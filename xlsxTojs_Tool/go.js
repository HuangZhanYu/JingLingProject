var xls_json = require("xls-to-json");
var fs = require("fs");
var path = require("path");
var exec = require('child_process').exec;
var NoKey = true; //判断是否有键值
var topStr = function (key) {
  return `'use strict';
  if (!window.i18n) {
      window.i18n = {};
  }
  
  if (!window.i18n.languages) {
      window.i18n.languages = {};
  }
  
  window.i18n.languages['${key}'] =`
}
var bottomStr = function () {
  return `\nlet loadMessage = function (language, dataID, isChange) {

  };
  exports.loadMessage = loadMessage;`
}

//遍历查询键值（有待优化）
var searchKey = function (obj, obj2, res) {
  if (typeof obj !== 'object') return;
  for (var key in obj) {
    if (typeof obj[key] === 'object') {
      searchKey(obj[key], obj2[key], res);
    } else {
      if (key == res['Key']) {
        obj[key] = res['EN'];
        obj2[key] = res['Chinese'];
        NoKey = false;
        break;
      }
    }
  }
}

//批处理命令函数
var exec_cb = function (err, stdout, stderr) {
  if (err) {
    console.log('cmd exec error:\n' + err);
  } else {
    if (stderr) {
      console.log('cmd exec stderr:\n' + stderr);
    } else {
      console.log('cmd exec success:\n' + stdout);
      HandleFiles(stdout.split('\n'));
    }
  }
}
//去首尾空格
var reEmpty = function(str){
  return String(str).replace(/(^\s*)|(\s*$)/g, "");
}

//操作文件
var HandleFiles = function(arr){
  var hasName = [];
  exec("dir result\\json\\*.*/b",(err, stdout, stderr) => {
    hasName = stdout.split('\n');
    for (let index = 0; index < arr.length; index++) {
      if(!arr[index] || isHasName(arr[index],hasName))continue;
      // xlsTojson(String(filename).replace(/(^\s*)|(\s*$)/g, ""),action_js);
      xlsTojson(reEmpty(arr[index]),action_json);
    }
  });
}
var isHasName = function(name,array){
  for (let index = 0; index < array.length; index++) {
    if(rehouzui(reEmpty(array[index])) == rehouzui(reEmpty(name))){
      return true;
    }
  }
  return false;
}

//生成js文件
var ToJsFile = function (name, str) {
  fs.writeFile(`./result/${name}`, str, function (err) {
    if (err) {
      console.log('writeFile error:' + err);
    } else {
      console.log(`${name} 转换完成 -> success !`);
    }
  });
}

//去掉文件后缀
var rehouzui = function(filename){
  return filename.replace(/(\.\w*$)/g,"");
}

//json转字符串文本
var jsonToStr = function(obj){
  var str = JSON.stringify(obj).replace(/:{/g, ":{\n").replace(/: {/g, ": {\n").replace(/\",/g, "\",\n").replace(/\"},\"/g, "\"},\n\"");
  return str;
}

//表格数据转json
var xlsTojson = function(name, fn_cb){
  
  xls_json({
    input:  path.join(__dirname ,`Tab_data/${name}`),
    output: path.join(__dirname , `result/defualt/${rehouzui(name)}.json`)
  }, function (err, res) {
    if (err) {
      console.error(err);
    } else {
      // console.log(res);
      if(fn_cb)fn_cb(res,rehouzui(name));
    }
  });
}

//执行函数（有待优化）
var action_js = function(res,filename){
  var EN_window = require("./languages/English").window;
  var CH_window = require("./languages/Chinese").window;
  var English = EN_window.i18n.languages['English'];
  var Chinese = CH_window.i18n.languages['Chinese'];
  if (!English.Common) {
    English.Common = {};
    Chinese.Common = {};
  }
  for (var index = 0; index < res.length; index++) {
    NoKey = true;
    searchKey(English, Chinese, res[index]);
    if (NoKey) {
      English.Common[res[index]['Key']] = res[index]['EN'];
      Chinese.Common[res[index]['Key']] = res[index]['Chinese'];
    }
  }
  var EN_midStr = jsonToStr(English);
  var CH_midStr = jsonToStr(Chinese);
  var EN_RES = topStr('English') + EN_midStr + bottomStr();
  var CH_RES = topStr('Chinese') + CH_midStr + bottomStr();
  console.log('转换中。。。');
  ToJsFile(`English.js`, EN_RES);
  ToJsFile(`Chinese.js`, CH_RES);
}
var action_json = function(res,filename){
  var keys = {};
  var resObj = {};
  var _id = "";
  var isId = 0;
  for (const key in res[0]) {
    if (key) {
      const element = res[0][key].split("|")[1];
      keys[element] = key+"|"+element;
    }
    if(isId === 1){
      _id = res[0][key].split("|")[1];
      break;
    }
    isId++;
  }

  for (let index = 3; index < res.length; index++) {
    let obj = res[index];
    let o = resObj[obj[keys[_id].split("|")[0]]] = {};
    for (const key in obj) {
      if(res[0][key].split("|")[1] == _id)continue;
      if (key) {
        if(!o[res[0][key].split("|")[1]]){
          o[res[0][key].split("|")[1]] = obj[key];         
        }else{
          o[res[0][key].split("|")[1]] += ";"+obj[key];         
        }
      }
    }
  }
  // console.log(resObj);
  ToJsFile('json/'+filename+'.json',jsonToStr(resObj));
}

/**主处理函数 */
var main_fun = function () {
  var cmdStr = "dir Tab_data\\*.*/b"//cmd命令语句(获取该目录下的所有文件)
  exec(cmdStr,exec_cb);
}
main_fun();
