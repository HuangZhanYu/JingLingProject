"use strict";

if (!window.i18n) {
    window.i18n = {};
}

if (!window.i18n.languages) {
    window.i18n.languages = {};
}

window.i18n.languages["English"] = {
    "LoginPanel_text":
    {
        "LoginButton":"Login",
        "GuestLoginButton":"Guest Login",
        "RegistButton":"Regist",
        "AccountName":"Account:",
        "PasswordName":"Password:",
        "ForgetPassword":"<u>Forget Password</u>",
        "AccountSafe":"<u>Account Safe</u>",
        "Account_PLACEHOLDER_LABEL":"请输入账号",
        "Password_PLACEHOLDER_LABEL":"请输入密码",
        "Landing":"登陆中",
    },
    "GamePanel_text":
    {
        "First_Pay_VIP":"FirstPayVIP",
        "Pay":"Pay",
        "Work":"Work",
        "Elf":"Elf",
        "Equip":"Equip",
        "Adventure":"Adventure",
        "Knapsack":"Knapsack",
        "Mission":"Mission",
    }
};
let loadMessage = function (language, dataID, isChange) {

};
exports.loadMessage = loadMessage;