'use strict';

if (!window.i18n) {
    window.i18n = {};
}

if (!window.i18n.languages) {
    window.i18n.languages = {};
}
window.i18n.languages['Chinese'] = {
    // write your key value pairs here
    "LoginPanel_text":
    {
        'LoginButton':'登 录',
        'GuestLoginButton':'游客登录',
        'RegistButton':'注册',
        'AccountName':'账  号:',
        'PasswordName':'密  码:',
        'ForgetPassword':'<u>忘记密码</u>',
        'AccountSafe':'<u>账号安全</u>',
        'Account_PLACEHOLDER_LABEL':'请输入账号',
        'Password_PLACEHOLDER_LABEL':'请输入密码',
        'Landing':'登陆中',
    },
    "GamePanel_text":
    {
        'First_Pay_VIP':'VIP首充',
        'Pay':'充值',
        'Work':'工作',
        'Elf':'精灵',
        'Equip':'装备',
        'Adventure':'冒险',
        'Knapsack':'背包',
        'Mission':'任务',
    },
    "TaskPopPanelText":
    {
        "LevelNotOpen":"等级【x】级"
    },
    "AlertText": {
        "Button_Common_Yes": "确认",
        "Button_Common_No": "取消",
    },
    "CopyeText":
    {
        "LevelLimit":"需要【x】级解琐",
        "Progress":"  通关进度：无",
        "Progress2":" 通关进度：第【x】层",
        "Progress3":"通关进度：全部通关",
        "JingJiAward":"您的排名可领取以下奖励：",
        "EnterTime":"入场时间：",
        "CopyLayer":"第【x】层",
        "HuaShiDiaoLuo":"化石掉落",
        "ChaoYueShidioaLuo":"超越石掉落"
    }
};
var pro = module.exports = {};

pro.refresh = function (ChangeValue) {
    this.notice = ChangeValue;
}