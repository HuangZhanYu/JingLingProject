/*
作者（Author）:    jason

描述（Describe）: 游戏内飘字处理类
*/
/*jshint esversion: 6 */

import PoolManager from "../manager/PoolManager";

class HUDText
{
    constructor(label, content,scaleTime, speed, duration, scaleSize = 2,textSize = 24)
    {
        this.mText = label;
        this.mText.fontSize = textSize;
        this.mText.string = content;
        this.mScaleTime = scaleTime*1000;
        this.mSpeed = speed;
        this.mDuration = duration*1000;//转成毫秒
        this.mScaleSize = scaleSize;
        this.mStartTime = (new Date()).getTime();
        this.mStartScale = 0;
        this.mEndFlag = false;
        this.mScaleFinished = false;
        this.mEndScale = 2;
        this.mNormalScale = 1;
    }
    Start()
    {
        this.mText.node.scale = this.mStartScale;
    }
    Update(dt)
    {
        if(!this.mText)
        {
            this.mEndFlag = true;
            return;
        }
        let passTime = (new Date()).getTime()- this.mStartTime;
        if (passTime > this.mDuration)
        {
            this.mEndFlag = true;
            return;
        }
        
        if (this.mScaleFinished)
        {
            //上移，透明，匀速上升
            let passTimeUp = passTime - this.mScaleTime;
            let lerp = passTimeUp / (this.mDuration - this.mScaleTime);
            let color = this.mText.node.color;
            color.a = 255*lerp;
            this.mText.color = color;
            //
            let pos = this.mText.node.position;
            pos.y += this.mSpeed * dt;
            this.mText.node.setPosition(pos);
        }
        else
        {
            //结束
            if (passTime > this.mScaleTime)
            {
                this.mScaleFinished = true;
                this.mText.node.scale = 1;
            }
            //先缩放
            let halfTime = this.mScaleTime / 2;
            if(halfTime == 0)
            {
                this.mScaleFinished = true;
                this.mText.node.scale = 1;
                return;
            }
            //先放大，后缩小
            if (passTime <= halfTime)
            {
                let lerp = passTime / halfTime;
                let scale = (this.mStartScale-this.mEndScale)*lerp;
                this.mText.node.scale = scale;
            }
            else
            {
                let lerp = passTime / halfTime;
                let scale = (this.mEndScale - this.mNormalScale) * lerp;
                this.mText.node.scale = scale;
            }

        }
    }

    IsEnd()
    {
        return this.mEndFlag;
    }

    GetNode()
    {
        if (this.mText)
        {
            return this.mText.node;
        }
        return null;
    }

    Up()
    {
        this.mStartTime -= 0.3*1000;
        let pos = this.mText.node.position;
        pos.y += this.mSpeed * 0.3;
        this.mText.node.setPosition(pos);
    }
}
let TOTAL_COUNT = 0;
let CreateCount = 0;
let ReleaseCount = 0;
cc.Class({
    extends: cc.Component,

    properties: {
        mPrefab:cc.Node
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.mTotalHUD = [];
        TOTAL_COUNT += 1;
        this.poolName = `HUDTextFactory${TOTAL_COUNT}`;
        console.log(`HUDTextFactory  poolName = ${this.poolName}`);
        PoolManager.CreatePool(this.poolName,2,2,this.mPrefab);
    },
    Create(content,scaleTime,speed,duration,scaleSize,textSize)
    {
        let trans = PoolManager.Get(this.poolName);
        trans.parent = this.node.parent;
        trans.active = true;
        trans.position = this.node.position;
        trans.scale = cc.Vec2.ONE;
        let hudText = new HUDText(trans.getComponent(cc.Label), content, scaleTime, speed, duration,scaleSize,textSize);
        this.AllUp();
        this.mTotalHUD.push(hudText);
        hudText.Start();
        ++CreateCount;
        //console.log(`HUDText  CreateCount = ${CreateCount} mTotalHUD.length = ${this.mTotalHUD.length}`);
    },
    AllUp()
    {
        for (let i = 0; i < this.mTotalHUD.length; i++)
        {
            this.mTotalHUD[i].Up();
        }
    },
    CreateGoldHUD(content)
    {
        this.Create(content,0, 50, 2, 1, 16);
    },
    update (dt) {
        for(let i = this.mTotalHUD.length - 1; i >=0 ; i--)
        {
            let hudText = this.mTotalHUD[i];
            if (hudText.IsEnd())
            {
                let t = hudText.GetNode();
                if(t)
                {
                    ++ReleaseCount;
                    console.log(`HUDText  ReleaseCount = ${ReleaseCount}  mTotalHUD.length = ${this.mTotalHUD.length}`);
                    PoolManager.Release(this.poolName,t);
                }
                else
                {
                    console.log(`HUDText  GetNode = null`);
                }
                this.mTotalHUD.splice(i,1);
            }else
            {
                hudText.Update(dt);
            }
        }
    },
    onDestroy()
    {
        PoolManager.ClearPoolByName(this.poolName);
    }
});
