/*
作者（Author）:    skyHuang

描述（Describe）:
*/
/*jshint esversion: 6 */
import DaoJuTool from '../../tool/DaoJuTool';
let Thor = require('Thor');

cc.Class({
    extends: Thor,

    properties: {

    },
    updateItemUI(data)
    {
        if(data.length < 1)
        {
            this.hideItemAll();
            //this.node.removeAllChildren();
            return;
        }
        if(data.length < this.node.childrenCount)
        {
            for(var i = 0; i < this.node.childrenCount; i ++)
            {
                let item = this.node.children[i];
                if(i < data.length)
                {
                    item.active = true;
                    //--设置道具数量 头像 
                    DaoJuTool.SetDaoJuInfo(item,data[i].ItemType, data[i].ItemID,data[i].ItemNum);
                }else
                {
                    item.active = false;
                }  
            }
        }else if(data.length > this.node.childrenCount)
        {
            for(var i = 0; i < data.length; i ++)
            {
                if(i < this.node.childrenCount)
                {
                    let item = this.node.children[i];
                    item.active = true;
                    //--设置道具数量 头像 
                    DaoJuTool.SetDaoJuInfo(item,data[i].ItemType, data[i].ItemID,data[i].ItemNum);
                }else
                {
                    let item = DaoJuTool.LoadDaoJu(this.node);
                    //--设置道具数量 头像 
                    DaoJuTool.SetDaoJuInfo(item,data[i].ItemType, data[i].ItemID,data[i].ItemNum);
                }  
            }
        }else
        {
            for(var i = 0; i < this.node.childrenCount; i ++)
            {
                let item = this.node.children[i];
                item.active = true;
                //--设置道具数量 头像 
                DaoJuTool.SetDaoJuInfo(item,data[i].ItemType, data[i].ItemID,data[i].ItemNum);
            }
        }
    },
    hideItemAll()
    {
        for(var i = 0; i < this.node.childrenCount; i ++)
        {
            let item = this.node.children[i];
            item.active = false;
        }
    },
});
