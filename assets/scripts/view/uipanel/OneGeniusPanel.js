/*
作者（Author）:    jason

描述（Describe）: 孵化单个精灵蛋
*/
/*jshint esversion: 6 */
import UIBase from '../UIBase';
import ModelManager from '../../manager/ModelManager';
import { ModelNames } from '../../common/ModelNames';
import MessageCenter from '../../tool/MessageCenter';
import { MessageID } from '../../common/MessageID';
import ViewManager from '../../manager/ViewManager';

cc.Class({
    extends: UIBase,

    properties: {
     
    },

    init: function () {
        this._isReused = false; //界面是否重复利用

        MessageCenter.addListen(this,this.onCreatePlayerSucceed,MessageID.MsgID_CreatePlayerSucceed);
    },
    onCreatePlayerSucceed()
    {
        ViewManager.hideLoading();
        this.closePanel();
    },
    _onConfirmButtonTouchEnd(sender, event)
    {
        console.log(`onConfirmButtonTouchEnd `);
        ViewManager.showLoading({});
        let playerInfoModel = ModelManager.getModel(ModelNames.PlayerInfoModel);
        LoginPart.CreatePlayer(playerInfoModel.playerName,playerInfoModel.roleType,0);
    },
    onDestroy() {
        MessageCenter.delListen(this);
    }
});
