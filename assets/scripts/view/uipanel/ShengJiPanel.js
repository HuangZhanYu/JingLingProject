/*
作者（Author）:    skyHuang

描述（Describe）:
*/
/*jshint esversion: 6 */

import CanShuTool from '../../tool/CanShuTool';
import { PlayerIntAttr } from '../../part/PlayerBasePart';
import PopupsController from '../PopupsController';
import UIBase from '../UIBase';

cc.Class({
    extends: UIBase,

    properties: {

    },
    onLoad() {
        this.m_Data = null;//服务器返回数据
        this.m_AwardData = null;
    },
    showPanel(resourcesData){
        this._super(resourcesData);
        this.m_Data = resourcesData;
        this.setInfo();
    },
    //点击登陆按钮
    _onMaskTouchEnd(sender, event)
    {
        this.hiddenPanel();
    },
    setInfo()
    {
        if(this.m_Data == null ){  
            console.error("请检查升级数据是否下发");
            return;
        }
        //--判断等级是否出现异常
        if((this.m_Data.NewLevel - this.m_Data.OldLevel) <=0 ){
            console.error("升级界面，等级出现异常");
            return;
        }
        
        //--当前训练上线
        let levelCur = null;
        var conf_Level = LoadConfig.getConfig(ConfigName.ZhuJue_Dengji);
        if(this.m_Data.OldLevel >= conf_Level.length ){ 
            levelCur = conf_Level.length;
        }
        else{ 
            levelCur = this.m_Data.OldLevel;
        }
        let curXLData = LoadConfig.getConfigData(ConfigName.ZhuJue_Dengji,levelCur);
        if(curXLData == null ){
            console.error("主角等级表数据为空，此ID："+levelCur);
            return;
        }
        //--下一阶训练上线
        let levelBefore = null;
        if(this.m_Data.NewLevel >= conf_Level.length ){
            levelBefore = conf_Level.length;
        }
        else{
            levelBefore = this.m_Data.NewLevel;
        }
        let beforeXLData = LoadConfig.getConfigData(ConfigName.ZhuJue_Dengji,levelBefore);
        if(beforeXLData == null ){
            console.error("主角等级表数据为空，此ID："+levelBefore);
            return;
        }

        //--设置前、后等级 ,以及训练上线
        this._ZJDJQText.$Label.string = this.m_Data.OldLevel;
        this._ZJDJHText.$Label.string = this.m_Data.NewLevel;
        this._XLSXQText.$Label.string = curXLData.MedalLevelMax;
        this._XLSXHText.$Label.string = beforeXLData.MedalLevelMax;
        //--强化上线
        //mLuaBehaviour:SetText(this.textQHQian,curXLData.GoldLevelMax);
        //mLuaBehaviour:SetText(this.textQHHou,beforeXLData.GoldLevelMax);

        //--金币加成
        let goldData = CanShuTool.GetCanShuData(CanShuTool.CanShuEnum.LevelUpJinBiAdd);
        let goldBuff = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_TaskGoldBuff]-goldData.Param[1];
        let beforeGold = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_TaskGoldBuff];
        if(goldBuff<= 0 ){
        goldBuff = 0;
        }
        this._JBJCQText.$Label.string = goldBuff+"%";
        if(beforeGold >= goldData.Param[2] ){
        beforeGold = goldData.Param[2];
        }
        this._JBJCHText.$Label.string = beforeGold+"%";
        //--纹章加成
        let medalData = CanShuTool.GetCanShuData(CanShuTool.CanShuEnum.LevelUpWenZhangAdd);
        let medalBuff = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_MedalBuff]-medalData.Param[1];
        let beforMmeda = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_MedalBuff];
        if(medalBuff <= 0 ){
        medalBuff = 0;
        }
        this._WZJCQText.$Label.string = medalBuff+"%";
        if(beforMmeda>= medalData.Param[2] ){
        beforMmeda = medalData.Param[2];
        }
        this._WZJCHText.$Label.string = beforMmeda+"%";
        this.showAwardInfo();
    },
    //显示奖励数据
    showAwardInfo(){
        if(!this.m_Data){
            return;
        }
        let resData = this.m_Data.Resources;
        let awardData = [];
        for (let i = 0; i < resData.length; i++) {
            if(resData[i].Type <= 0){
                break;
            }
            let data = {ItemNum : resData[i].Count,ItemType : resData[i].Type,ItemID : resData[i].ID};
            awardData.push(data);
        }
        this._AwardGrid.$AwardGrid.updateItemUI(awardData);
    },
    hiddenPanel(){
        this._super();
        this._AwardGrid.$AwardGrid.hideItemAll();
        PopupsController.getInstance().closeShowPopup();
    },
});
