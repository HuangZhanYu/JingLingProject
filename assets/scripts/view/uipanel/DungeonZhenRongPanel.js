/**
 * 副本玩家选择阵容面板
 */
import ViewManager, { PanelPos } from "../../manager/ViewManager";
import UIBase from "../UIBase";
cc.Class({
    extends: UIBase,

    properties: {
        closeBtn:cc.Node, //关闭按钮
        fightBtn:cc.Node, //挑战按钮
        zhenRongParent:cc.Node, //阵容父节点
        zhenRongItem:cc.Node, //阵容预设
        limitsParent:cc.Node, //禁止父节点
        limitItem:cc.Node, //禁止预设
        limitDesc:cc.Label, //禁止文本
        atlas:cc.SpriteAtlas
    },
    start () {
        
    },
    _addEvents()
    {
        this.closeBtn.on(cc.Node.EventType.TOUCH_END,this._onClickCloseBtn,this);
        this.fightBtn.on(cc.Node.EventType.TOUCH_END,this._onClickFightBtn,this);
    },
    _removeEvents()
    {
        this.closeBtn.off(cc.Node.EventType.TOUCH_END,this._onClickCloseBtn);
        this.fightBtn.off(cc.Node.EventType.TOUCH_END,this._onClickFightBtn);
    },
    /**
     * 点击关闭按钮事件
     */
    _onClickCloseBtn()
    {
        this.hiddenPanel();
    },

    hiddenPanel()
    {
        this._super();
        this._removeEvents();
    },
    /**
     * 点击战斗按钮事件
     */
    _onClickFightBtn()
    {
        DungeonPart.OnBeginFightVerifyRequest(this.dungeonId,this.currentShoudFightIndex);
    },

    showPanel(copyData)
    {
        this._addEvents();
        this.limitItem.active = false;
        this.dungeonId = copyData.curDungeonID; //地下城的ID
        this.currentShoudFightIndex = copyData.currentShoudFightIndex; //当前副本下标
        this._super();
        this._setZhenRong();
        this._setLimitPet();
    },
    /**
     * 设置限制进入的精灵
     */
    _setLimitPet()
    {
        let diXiaChengData = LoadConfig.getConfigData(ConfigName.DiXiaCheng,this.dungeonId);
        if (diXiaChengData == null) 
        {
            console.error(">>没有获取到地下城数据");
            return;
        }
        let limitData = diXiaChengData.Islimit;
        let hasLimit = false;
        if (limitData == 0) //无限制
        {
            hasLimit = false;
        }else
        {
            let objCount = limitData.length - this.limitsParent.childrenCount;
            for (let index = 0; index < objCount; index++) 
            {
                let limitItem = cc.instantiate(this.limitItem);
                this.limitsParent.addChild(limitItem);
                limitItem.position.y = 0;
            }
            for (let index = 0; index <  this.limitsParent.childrenCount; index++) 
            {
                this.limitsParent.children[index].active = false;
            }
            let count = 0;
            let conf_ShuXingIcon = LoadConfig.getConfig(ConfigName.ShuXingIcon);
            for (let index = 0; index < limitData.length; index++) 
            {
                let limieTips = limitData[index];
                if (limieTips == 0) 
                {
                    //TODO 获取item去设置图标
                    if (hasLimit == false) 
                    {
                        hasLimit = true;   
                    }
                    let limitItem = this.limitsParent.children[count];
                    limitItem.active = true;
                    let iconPath = conf_ShuXingIcon[count].Icon;
                    let sprite = limitItem.getComponent(cc.Sprite);
                    let spriteFram = this.atlas.getSpriteFrame(iconPath);
                    sprite.spriteFrame = spriteFram;
                    count+=1;
                }
            }
        }
        this.limitsParent.active = hasLimit; 
        this.limitDesc.node.active = !hasLimit;
    },
    /**
     * 设置目前已设置的阵容
     */
    _setZhenRong()
    {
        let canShuConfig = LoadConfig.getConfigData(ConfigName.CanShu,2)
        if (this.zhenRongParent.childrenCount == 0) 
        {
            let maxZhenRongCount = canShuConfig.Param[0];
            this.zhenRongItems = [];
            for (let index = 0; index < maxZhenRongCount; index++) 
            {
                let zhenRongItemObj = cc.instantiate(this.zhenRongItem);
                let zhenRongItem = zhenRongItemObj.getComponent("DungeonZhenRongItem");
                this.zhenRongParent.addChild(zhenRongItemObj);
                zhenRongItemObj.position.y = 0;
                this.zhenRongItems.push(zhenRongItem);
            } 
        }
        let mIndex = 0;
        let sum = LineupPart.CardCapacity - LineupPart.CardList.length;
        LineupPart.BackSort();
        for (let index = 0; index < this.zhenRongItems.length; index++) 
        {
            let cardData = LineupPart.CardList[index];
            let item = this.zhenRongItems[index]
            item.updateItem(cardData,sum,this.dungeonId);
            if (cardData == null) 
            {
                sum-=1;
            }
        }
    },
});
