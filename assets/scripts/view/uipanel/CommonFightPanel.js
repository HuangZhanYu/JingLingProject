import UIBase from "../UIBase";
import ViewManager from "../../manager/ViewManager";
import { PanelResName } from "../../common/PanelResName";
import { PlayerIntAttr, PlayerStrAttr } from "../../part/PlayerBasePart";
import Card from "../../logic/Card";
import FightTool from "../../logic/fightBehavior/FightTool";
import GFightManager from "../../logic/fight/GFightManager";
import DaoJuTool from "../../tool/DaoJuTool";
import StringTool from "../../tool/StringTool";
const i18n = require('LanguageData');
//战斗界面枚举  对应战斗配置表
export const FightType = 
{
    MianFight    : 1, //主线战斗
    Arena        : 2, //训练竞技场
    Dungeon      : 3, //地下城
    Dungeon2     : 4, //地下城
    Dungeon3     : 5, //地下城
    Dungeon4     : 6, //地下城
    Dungeon5     : 7, //地下城
    Dungeon6     : 8, //地下城
    Dungeon7     : 9, //地下城
    FightTower   : 10,//挑战塔
    TopArena     : 11,//巅峰竞技场
    SkillPreview : 12,//技能预览
    FriendBattle : 13,//好友切磋
    Plateau      : 14,//精灵高原
    ArenaRecord  : 15,//竞技场回放
    NewBie       : 16,//新手战斗
    GuildFight   : 17,//道馆战
}
//种族(1红2绿3黄4蓝)
export const SpriteType = 
{
    Red    : 1,
    Green  : 2,
    Yellow : 3,
    Blue   : 4,
}

cc.Class({
    extends: UIBase,

    properties: 
    {
        chatBtn:cc.Node, //聊天按钮
        emailBtn:cc.Node, //邮件按钮
        littleTipsBtn:cc.Node, //小技巧按钮
        speedBtn:cc.Node, //加速按钮
        jumpBtn:cc.Node, //跳过战斗按钮
        rankBtn:cc.Node, //排名按钮
        quitFightBtn:cc.Node, //退出战斗按钮
        level:cc.Label, //玩家等级
        honor:cc.Label, //玩家荣誉
        diamond:cc.Label, //玩家钻石
        gold:cc.Label, //玩家金币
        tiLi:cc.Label,//体力
        huPoBi:cc.Label, //琥珀币
        playerName:cc.Label, //玩家名字
        expPercent:cc.Label, //经验百分比
        eXPslider:cc.ProgressBar, //经验条
        headIcon:cc.Sprite, //头像
        skillParent:cc.Node, // 技能父级
        headAtlas:cc.SpriteAtlas,//头像图集

    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start () {

    },
    showPanel(openData)
    {
        //fightType,backFunc
        if (openData != null) 
        {
            this.mCurrentFightType = openData.fightType;
            this.mbackFunc = openData.backFunc;
        }
      
        this._super();
    },

    _OnShow()
    {
        if (this.mbackFunc != null) 
        {
            this.mbackFunc();
            this.mbackFunc = null;
        }
        this.jumpBtn.active = false;
        this.JumpText.active = false;
        if (this.skillControl == null) 
        {
            let skillControlObj = DaoJuTool.LoadSkillItem(this.skillParent);
            if (skillControlObj == null) 
            {
                console.error(">>>>>>没有获取到技能控制脚本");
                return;   
            }
            this.skillControl = skillControlObj.getComponent("SkillItemControl");
        }
        this.skillControl.initSkillItem(); //初始化技能信息
        this.updatePlayerInfo(); //刷新角色信息
        //TODO 这里显示玩家加速信息没有设置ShowSpeedUp;
        let conf_PeiZhiInfo = LoadConfig.getConfigData(ConfigName.ZhanDouPeiZhi,this.mCurrentFightType);
        if (conf_PeiZhiInfo == null) 
        {
            console.error("竞技场战斗配置ID为空"+this.mCurrentFightType)   
            return;
        }
        if (this.mCurrentFightType = FightType.Dungeon) 
        {
            mDungeon = DungeonDiffPopPanel.CurrentDungeonID;
            mRound = DungeonDiffPopPanel.DungeonRound;
            let conf_Dungeon = LoadConfig.getConfigData(ConfigName.DiXiaCheng,mDungeon)
            if (conf_Dungeon == null)
            {
                console.error("地下城ID错误:"+mDungeon);
                return
            }
            
            let contentStr = i18n.t('CopyeText.CopyLayer');
            let titleStr = StringTool.ZhuanHuan(contentStr,[conf_Dungeon.Name])
            //TODO 这里设置标题 第几层 
            console.log("TODO 这里设置标题 第几层");
            console.warn(">>>这里VIP的功能还没实现")
        }else if (this.mCurrentFightType == FightType.SkillPreview)
        {
            // mLuaBehaviour:SetText(this.Title,conf_PeiZhiInfo.FightTitle) 这段待翻译
            CommonFightPanel.StartTestFight();
        }else if (this.mCurrentFightType == FightType.FightTower)
        {
            
        }else if (this.mCurrentFightType == FightType.FriendBattle)
        {
            this.jumpBtn.active = true;
            this.JumpText.active = true;
            // mLuaBehaviour:SetText(this.Title,conf_PeiZhiInfo.FightTitle)这段待翻译
        }else
        {
            //mLuaBehaviour:SetText(this.Title,conf_PeiZhiInfo.FightTitle)这段待翻译
        }
        let isShowSkill = conf_PeiZhiInfo.UseSkill
        let isShowExit = conf_PeiZhiInfo.IsExit
        let isShowDamage = conf_PeiZhiInfo.CheckDamage
        let isShowSpeed = conf_PeiZhiInfo.UseSpeed
       
        //TODO 初始化伤害信息
        this.skillParent.active = false;
        this.quitFightBtn.active(isShowExit == 1 && !isDungeonGuide);

        //TOdO 待翻译
        /**
         * this.QuitText:SetActive(isShowExit == 1 and not isDungeonGuide)
    --设置伤害排行显隐
    this.ButtonRank:SetActive(isShowDamage == 1)
    this.ButtonRankText:SetActive(isShowDamage == 1)
    --设置加速显隐
    this.SpeedImage:SetActive(isShowSpeed == 1)
    this.SpeedText:SetActive(isShowSpeed == 1)
         */
    },
    SetPreviewCardIDAndOpenType(cardID,openType)
    {
        this.mPreviewID = cardID
        this.mPreviewOpenType = openType //1背包 2阵容
    },
    /**
     * 刷新玩家信息
     */
    updatePlayerInfo()
    {
        
        let level = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_DengJi]
        let honor = PlayerBasePart.StrAttr[PlayerStrAttr.PlayerStrAttr_BigNumberMedal]
        let diamond = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_ZuanShi]
        let gold = PlayerBasePart.StrAttr[PlayerStrAttr.PlayerStrAttr_BigNumberGold]
        let name = PlayerBasePart.StrAttr[PlayerStrAttr.PlayerStrAttr_MingZi]
        let exp = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_JingYan]
        let tili = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_TiLi]

        this.level.string = level;
        this.honor.string = honor;
        this.diamond.string = diamond;
        this.gold.string = gold;
        this.playerName.string = name;
       

        let canShu = LoadConfig.getConfig(ConfigName.CanShu);
        if (canShu == null) 
        {
            console.error(">>>没有获取到参数表");
            return;   
        }
        let maxTiLi = canShu["23"].Param[0]
        this.tiLi.string = tili+"/"+maxTiLi;

        let huPoBi = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_HuPoBi];
        this.huPoBi.string = huPoBi;

        let zhuJueDengjiConfig = LoadConfig.getConfig(ConfigName.ZhuJue_Dengji);
        let expMax = zhuJueDengjiConfig[level].NeedExp;
        let value = exp/expMax;
        value = (value > 1)?1:value;
        this.eXPslider.progress = value;
        this.expPercent = Math.ceil(value*100);

        this._setHeadIcon();
    
    },
    /**
     * 设置头像
     */
    _setHeadIcon()
    {
        let headId = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_TouXiangID];
        let headInfo = LoadConfig.getConfigData(ConfigName.ZhuJue_TouXiang,headId);
        if (headInfo == null) 
        {
            console.error(">>>没有获取到头像信息");
            return;   
        }
        let spriteFrame = this.headAtlas.getSpriteFrame(headInfo.Icon);
        if (spriteFrame == null) 
        {
            console.error(">>>头像图集中没有获取到头像资源:"+headInfo.Icon);
        }
        this.headIcon.spriteFrame = spriteFrame;
    },
    
    _ShowSpeedUp()
    {
        let speedData = SpeedUpPart.GetCurSpeedUpData();
        if (speedData == null) 
        {
            
        }
    },
    StartTestFight ()
    {
        this.OnDemoFightStart(this.mPreviewID, 1)
    },
    OnDemoFightStart(cardID, demeFightID)
    {
        this.demeFightID = 1;
        let conf_Demo_Data = LoadConfig.getConfigData(ConfigName.YanShiZhanDou,this.demeFightID);
        if (conf_Demo_Data == null) 
        {
            console.error(">>>演示战斗表错误："+this.demeFightID);
            return;   
        }
        let myCards = [];
        let card = Card.NewByID(cardID, 1, 0)
        card.SetNotRespawn()
        myCards.push(card)
        //获取我军boss
        let myBossCard = Card.NewByID(FightTool.GetMyBossID(),conf_Demo_Data.MyBossLevel, 0);
        let myTeam = GFightManager.CreateFightTeam(tostring(demeFightID), myBossCard, myCards, 1, 1)

        //获取敌军数据
        let enemyCards = [];
        for (let index = 0; index < conf_Demo_Data.GuaiWuID.length; index++) 
        {
            let guaiWuId = conf_Demo_Data.GuaiWuID[index];
            if (guaiWuId > 0) 
            {
                // 可能不会配满12个人
                let card = Card.NewByID(guaiWuID, conf_Demo_Data.GuaiWuLevel, 0)
                card.SetNotRespawn()
                enemyCards.push(card)
            }
        }
        //获取敌军boss
        let enemyBossCard = Card.NewByID(conf_Demo_Data.BossID, conf_Demo_Data.BossLevel, 0)
        let enemyTeam = GFightManager.CreateFightTeam(tostring(demeFightID), enemyBossCard, enemyCards, 1, 0)
        let scene = conf_Demo_Data.Sence;
        GFightManager.Demonstration(scene, 5000*1000, myTeam, enemyTeam, this._OnDemoFightEnd.bind(this))

    },
    _OnDemoFightEnd()
    {
        //     mPreviewID = cardID
        // mPreviewOpenType = openType --1背包 2阵容
        this.hiddenPanel();
        if (this.mPreviewOpenType == 1) 
        {
            // BackPackPopPanel.Show()
            // DigimonDetailPopPanel.ShowPreviewBack(mPreviewOpenType)
            console.log(">>>>>>功能未实现，因为面板还没有做")
        }
        else if (this.mPreviewOpenType == 2) 
        {
            console.log(">>>>>>功能未实现，因为面板还没有做")
            //LineupPopPanel.Show() 
        //DigimonDetailPopPanel.ShowPreviewBack(mPreviewOpenType)
        }
        else if (this.mPreviewOpenType == 3) 
        {
            
        }else{
            console.error(">>>>战斗预览类型不对："+this.mPreviewOpenType )
        }
        GuanQiaPart.Resume()
    }

    // update (dt) {},
});
