/*
作者（Author）:    Holmes

描述（Describe）:
*/
/*jshint esversion: 6 */
import UIBase from "../UIBase";
import ViewManager from "../../manager/ViewManager";

cc.Class({
    extends: UIBase,

    properties: {},
    //初始化item
    init: function (param) {


    },
    //刷新item显示
    showItem: function (param) {
        this._ItemIcon.$Sprite.spriteFrame = "";
        this._ItemTitle.$Label.string = "";
        this._ItemContent.$Label.string = "";

        this._PriceIcon.$Sprite.spriteFrame = "";
        this._Price.$Label.string = "";


        this._DayPrice.$Label.string = "";
        this._DayPriceLab.$Label.string = "";
    },
    //购买按钮
    _onBuyBtnTouchEnd(sender, event) {

    },


});
