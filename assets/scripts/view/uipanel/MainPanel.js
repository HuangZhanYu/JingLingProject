/*
作者（Author）:    jason

描述（Describe）: 游戏主界面
*/
/*jshint esversion: 6 */
import UIBase from '../UIBase';
import ViewManager, { PanelPos } from '../../manager/ViewManager';
import { PanelResName } from '../../common/PanelResName';
import FunctionOpenTool, { FuntionEnum } from '../../tool/FunctionOpenTool';
import LocalConfigLoader from '../../tool/LocalConfigLoader';
import BigNumber from '../../tool/BigNumber';
import { PlayerStrAttr, PlayerIntAttr } from '../../part/PlayerBasePart';
import MessageCenter from '../../tool/MessageCenter';
import { MessageID } from '../../common/MessageID';
import { GetPlayerCanBuyBool } from '../../common/PublicFunction';
import { ToggleType } from './BottomFunctionPanel';
let mCurrentRound = 0;

cc.Class({
    extends: UIBase,
    init: function () {
        MessageCenter.addListen(this,this.SetTaskRedPoint,MessageID.MsgID_AddGoldResponse);
        MessageCenter.addListen(this,this.ShowSpeedUp,MessageID.MsgID_ShowSpeedUp);
    },
    showPanel: function (param)
    {
        GuanQiaPart.SetMainInitFlag(true);
        GuanQiaPart.Init();
        this.UpdateInfo();
        //this.node.active = false;
        ViewManager.addPanel(PanelResName.BottomFunctionPanel,(panel)=>{
            panel.AfterPassRound();
            panel.AfterLevelUp();
        },PanelPos.PP_Midle);
    },
    

    UpdateCurrentRound(round)
    {
        mCurrentRound = round;
        this._CurLevelLabel.$RichText.string = `<color=#0d4fbc><outline color=#c4fffd width=1>第${round}关</outline></color>`;
        this.SetReviveEffect();
        this.ShowGuide();
    },
    IsPanDuanChengJiu()
    {

    },
    SetReviveEffect()
    {

    },
    ShowGuide()
    {

    },
    GetCurrentRound()
    {
        return mCurrentRound;
    },
    //历史最高关卡改变
    GuanKaPartUpateHistoryTop()
    {
        let BottomFunctionPanel = ViewManager.getPanel(PanelResName.BottomFunctionPanel);
        if(BottomFunctionPanel)
        {
            BottomFunctionPanel.AfterPassRound();
        }
        
    },
    //进入界面 刷新全部
    UpdateInfo()
    {
        if(PlayerBasePart.IntAttr.length == 0 || PlayerBasePart.StrAttr.length == 0)
        {
            return;
        }

        //MainPanel.UpdateHistoryTop();

        //角色等级  角色种族  跟重生有关
        let level = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_DengJi];
        let honor = PlayerBasePart.StrAttr[PlayerStrAttr.PlayerStrAttr_BigNumberMedal]; //纹章
        let diamond = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_ZuanShi];
        let gold = PlayerBasePart.StrAttr[PlayerStrAttr.PlayerStrAttr_BigNumberGold];  //金币
        let name = PlayerBasePart.StrAttr[PlayerStrAttr.PlayerStrAttr_MingZi];
        let exp = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_JingYan];
        let tili = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_TiLi];
        let hupobi = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_HuPoBi];
        let vipLv = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_Vip];

        this._HuPoBiLabel.$Label.string = hupobi;
        //mLuaBehaviour:SetText(this.PlayerLv,"Lv."..level);

        //mLuaBehaviour:SetText(this.VipLevel,vipLv)

        let resultMedal = BigNumber.getValue(honor);
        this._HonorLabel.$Label.string = resultMedal;

        this._DiamondLabel.$Label.string = diamond;
        let resultGold = BigNumber.getValue(gold);
        this._GoldLabel.$Label.string = resultGold;
        //mLuaBehaviour:SetText(this.Gold,resultGold);
        //mLuaBehaviour:SetText(this.PlayerName,name);

        let Conf_CanShu = LoadConfig.getConfigData(ConfigName.CanShu,23);
        if(Conf_CanShu == null)
        {
            console.error("参数表ID23为空")
            return;
        }
            
        

        //mLuaBehaviour:SetText(this.PlayerEnergy,tili+"/"+Conf_CanShu.Param[1]);
        //经验条
        let Conf_ZhuJue_Dengji = LoadConfig.getConfigData(ConfigName.ZhuJue_Dengji,level);
        let expMax =  Conf_ZhuJue_Dengji.NeedExp;
        let value = exp / expMax;

        if(value > 1)
            value = 1;
        

        // let v2temp = this.EXPslider.sizeDelta;
        // v2temp.x = 111 * value;
        // this.EXPslider.sizeDelta = v2temp;

        let intValue = Math.floor(value * 100);
        //mLuaBehaviour:SetText(this.Percent,intValue+"<size=13>%</size>")

        // if(GuanQiaPart.HistoryTop >= 5)
        // {
        //     MainPanel.ShowGuide();
        // }
            
        


        //设置头像
        let headID = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_TouXiangID];
        let headInfo = LoadConfig.getConfigData(ConfigName.ZhuJue_TouXiang,headID);
        if(headInfo == null)
        {
            console.error("头像ID 错误 , id "+headID)
            return;
        }
            
         

        // if(isHaiWai)
        //     mLuaBehaviour:SetImage(this.BtnPlayerHead,"headicon",headInfo.Icon+"_MoMoDa")
        // else
        //     mLuaBehaviour:SetImage(this.BtnPlayerHead,"headicon",headInfo.Icon)
        
    },
    //检测任务红点
    SetTaskRedPoint()
    {
        //如果没开放功能 不显示红点
        if(!FunctionOpenTool.IsFunctionOpen(FuntionEnum.MainTaskButton))
        {
            return;
        }
        let state = false
        let playerLv =  PlayerBasePart.GetIntValue(PlayerIntAttr.PlayerIntAttr_DengJi);
        //条件1  有可以开启的项目
        //条件2  有 请点击的项目
        for (let i = 0; i < TaskPart.DailyTaskList.length; ++i)
        {
            let taskInfo = TaskPart.GetTaskInfo(i);
            let conf_Task = LoadConfig.getConfigData(ConfigName.JinBiRenWu,i+1);
            //未开启,并且达到开启等级
            if (taskInfo.TaskLevel == 0 && playerLv >= conf_Task.PlayerLevel)
            {
                //并且金币足够
                let bigOpenNum = BigNumber.create(conf_Task.OpenCount);
                let bigNumPlayer = BigNumber.create(PlayerBasePart.StrAttr[PlayerStrAttr.PlayerStrAttr_BigNumberGold]);
                let tempBool = BigNumber.greaterThanOrEqualTo(bigNumPlayer,bigOpenNum);

                if (tempBool)
                {
                    state = true;
                    //SideBarPanel.ShowGuide()
                    break;
                }
                else
                    break;
                
            }
            else if (taskInfo.TaskLevel > 0 && taskInfo.TaskLevel < conf_Task.MaxLevel)  //没有满级 并且可升级
            {
                const [canOffer,costString] = GetPlayerCanBuyBool(1,i,taskInfo.TaskLevel);
                if (canOffer)
                {
                    state = true;
                    break;
                }
            }
        }

        for (let i = 0; i < TaskPart.DailyTaskList.length; ++i)
        {
            //有请点击    没有倒计时 并且 等级大于0  并且当前自动任务小于最大任务
            let taskInfo = TaskPart.GetTaskInfo(i)
            if (TaskPart.TaskInfo[i] == null)
            {
                //console.error("倒计时数据为空"..i)
                return;
            }

            if(taskInfo.TaskId > TaskPart.CurrentAutoId && !TaskPart.TaskInfo[i].IsCountDown && taskInfo.TaskLevel > 0)
            {
                state = true;
                //SideBarPanel.ShowGuide();
                break;
            }
                
        }
        let BottomFunctionPanel = ViewManager.getPanel(PanelResName.BottomFunctionPanel);
        if(BottomFunctionPanel)
        {
            BottomFunctionPanel.SetRedPoint(ToggleType.Task,state);
        }
    },
    //是否显示加速 由加速part控制  下发加速时  打开  直接设置为开启状态
    ShowSpeedUp()
    {

    },
    //刷新界面相关
    UpdateTeamStrength(zhanli)
    {
        
    },
    //刷新角色信息  其他界面刷新用  不刷新头像
    UpdatePlayerInfo()
    {
        //this.UpdateHistoryTop();
        //角色等级  角色种族  跟重生有关
        let level = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_DengJi];
        let honor = PlayerBasePart.StrAttr[PlayerStrAttr.PlayerStrAttr_BigNumberMedal]; //纹章
        let diamond = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_ZuanShi];
        let gold = PlayerBasePart.StrAttr[PlayerStrAttr.PlayerStrAttr_BigNumberGold];  //金币
        let name = PlayerBasePart.StrAttr[PlayerStrAttr.PlayerStrAttr_MingZi];
        let exp = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_JingYan];
        let tili = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_TiLi];
        let vipLv = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_Vip];

        let resultGold = BigNumber.getValue(gold);
        this._GoldLabel.$Label.string = resultGold;
    },
    //升级后相关操作
    OnPlayerLevelUp()
    {
        this.SetButtonsShowLevelUp();

        let BottomFunctionPanel = ViewManager.getPanel(PanelResName.BottomFunctionPanel);
        if(BottomFunctionPanel)
        {
            BottomFunctionPanel.AfterLevelUp();
        }
    },
    //更新升级相关的显隐按钮
    SetButtonsShowLevelUp()
    {

    },
    //设置飘字
    CreatHudText(contentStr)
    {
        // <param name="content">文字内容</param>
        //<param name="type">文字颜色枚举</param>
        // <param name="scaleTime">缩放时间</param>
        // <param name="speed">上升速度</param>
        // <param name="duration">持续时间</param>
        // <param name="scaleSize">伸缩大小</param>
        // <param name="textSize">文本大小</param>
        //(string content, SMFontColorType type, float scaleTime, float speed, float duration, float scaleSize,int textSize)
        //this.HUDtool:CreateGoldHUD(contentStr,SMFontColorType.GongJiLi)
        this._GodeHUD.$HUDTextFactory.CreateGoldHUD(contentStr);
    },

    //设置宝箱飘字
    CreateBoxHudText(contentStr)
    {
        //文字内容  文字颜色枚举  缩放时间  上升速度  持续时间  伸缩大小  文本大小
        //this.BoxHuDtool:Create(contentStr, SMFontColorType.XuanZeWanJia, 0, 70, 1, 1, 20);
    },

    //设置纹章飘字
    CreatMedalHudText(contentStr)
    {
        //this.MedalHUDtool:CreateGoldHUD(contentStr,SMFontColorType.GongJiLi)
        this._GodeHUD.$HUDTextFactory.CreateGoldHUD(contentStr);
    },
    
    //设置钻石飘字
    CreatDiamondHudText(contentStr)
    {
        //this.DiamondHUDtool:CreateGoldHUD(contentStr,SMFontColorType.GongJiLi)
    },
    //设置琥珀币飘字
    CreatHuPoHudText(contentStr)
    {
        //this.HuPoHUDtool:CreateGoldHUD(contentStr,SMFontColorType.GongJiLi)
    },
    //设置经验飘字   默认第一次不飘 
    CreateExpHudText(level, contentStr)
    {
        let showOne = "+"+contentStr;
        // this.ExpHUDtool:CreateGoldHUD(contentStr,SMFontColorType.GongJiLi)
        // //调用公用飘字
        // let strContent = TranslateTool.GetText(1016)
        // strContent = StringTool.ZhuanHuan(strContent,{level,contentStr})
        // CommonHUDPanel.Show(strContent)
    },
    //设置界面中心飘字
    //参数1  需要飘字的内容string
    CreatMainCenterHudText(contentStr)
    {
        //this.TipsHUDtool:CreateGoldHUD(contentStr,SMFontColorType.GongJiLi)
    },
    //单独刷新主界面纹章
    RefreshMedalText()
    {
        let honor = PlayerBasePart.StrAttr[PlayerStrAttr.PlayerStrAttr_BigNumberMedal];  //文章

        if (honor != null)
        {
            let resultMedal = BigNumber.getValue(honor);
            this._HonorLabel.$Label.string = resultMedal;
        }
    }
});