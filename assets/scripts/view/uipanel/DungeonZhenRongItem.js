import DaoJuTool from "../../tool/DaoJuTool";
/**
 * 阵容item脚本
 */
cc.Class({
    extends: cc.Component,

    properties: {
        add:cc.Node, //添加或者切换
        lock:cc.Node, //锁住
        daoJuParent:cc.Node, //道具父级
        limitItem:cc.Node, //禁止
    },
    start () 
    {
        this._addEvents();
    },
    onDestroy()
    {
        this._removeEvents();
    },
    /**
     * 刷新item
     * @param {*} cardData 卡牌数据
     * @param {*} sum  啥下标？？？ 好像是开启的阵位数的下标
     * @param {*} curDungeonID 当前的副本id 
     */
    updateItem(cardData,sum,curDungeonID)
    {
        this.curDungeonID = curDungeonID;
        this.cardData = cardData;
        this.sum = sum;
        let daoJuItem = null;
        if (this.daoJuParent.childrenCount != 0) 
        {
            daoJuItem = this.daoJuParent.children[0];
        }else
        {
            daoJuItem = DaoJuTool.LoadDaoJu(this.awardParent);
        }
        this.huanZhenBtn = cc.find("Frame",daoJuItem);
        daoJuItem.active = true;
        this.isLimit = false;
        this.add.active = false;
        this.lock.active = false;
        if (cardData != null) 
        {
            let cardInfo = LoadConfig.getConfigData(ConfigName.ShiBing,cardData.CardID);
            if (cardInfo == null) 
            {
                console.error(">>>>>获取数据为空，ID"+cardData.CardID);
                return;
            }
            let zhongZuType = cardInfo.Race;

            this.isLimit = this.isLimitSprite(zhongZuType);
            this.limitItem.active = (this.isLimit == true);
            //go,resourceType,resourceID,equipEvo
            DaoJuTool.SetDaoJuImage(daoJuItem,DaoJuTool.ResourceType.ShiBingBiao,cardData.CardID);
            if (cardData.EvolutionLevel <= 0) 
            {
                let level = LineupPart.GetCurrentEnhanceLevel();
                if (level <=0) 
                {
                    DaoJuTool.SetDaoJuText(daoJuItem,"","");
                }else
                {
                    DaoJuTool.SetDaoJuText(daoJuItem,"",("Lv."+level));
                }
            }else
            {
                DaoJuTool.SetDaoJuText(daoJuItem,"",("Lv."+cardData.EvolutionLevel));
            }
            this.add.active = true;
            this.huanZhenBtn.off(cc.Node.EventType.TOUCH_END,this._onClickChangePetEvent); //避免重复赋值点击事件
            this.limitItem.on(cc.Node.EventType.TOUCH_END,this._onClickChangePetEvent,this);
        }else
        {
            daoJuItem.active = false;
            this.add.active = (sum >= 0);
            this.lock.active = (sum < 0);
            this.limitItem.active = false;
        }
        
    },
    /**
     * 判断是否是禁止的宠物 
     * @param {*} zhongZuType 
     */
    isLimitSprite(zhongZuType)
    {
        let result = false;
        let diXiaChengData = LoadConfig.getConfigData(ConfigName.DiXiaCheng,this.curDungeonID);
        if (diXiaChengData == null) 
        {
            console.error("地下城表数据为空，此ID："+this.curDungeonID);
            return result;
        }
        let limitDatas = diXiaChengData.Islimit;
        if (limitDatas == 0 ) 
        {
            result = false;
        }else
        {
            for (let index = 0; index < limitDatas.length; index++) 
            {
                if (zhongZuType == index) 
                {
                    let limitTips = limitDatas[index];
                    result = (limitTips == 1);
                    break;
                }
            }
        }
        
        return result;
    },
    /**
     * 添加点击事件 
     */
    _addEvents()
    {
        this.add.on(cc.Node.EventType.TOUCH_END,this._onClickAddBtn,this);
        this.limitItem.on(cc.Node.EventType.TOUCH_END,this._onClickAddBtn,this);
    },
    /**
     * 移除点击事件
     */
    _removeEvents()
    {
        this.add.off(cc.Node.EventType.TOUCH_END,this._onClickAddBtn);
        if (this.huanZhenBtn!= null) {
            this.huanZhenBtn.off(cc.Node.EventType.TOUCH_END,this._onClickAddBtn);
        }
        this.limitItem.off(cc.Node.EventType.TOUCH_END,this._onClickAddBtn);
    },
    /**
     * 点击加号按钮事件
     */
    _onClickAddBtn()
    {
        console.log(">>点击了添加按钮事件>>>>>AddDigimonPopPanel类还没实现但是下面代码是对的 等实现")
        //AddDigimonPopPanel.Open(null,AddDigimonPopPanel.EnterPanel.Dungeon,this.curDungeonID);
    },
    /**
     * 点击更换宠物事件 
     */
    _onClickChangePetEvent()
    {
        console.log(">>点击更换宠物按钮事件>>>>>AddDigimonPopPanel类还没实现但是下面代码是对的 等实现")
        //AddDigimonPopPanel.Open(this.cardData.ObjID,AddDigimonPopPanel.EnterPanel.DungeonCardInfo,this.curDungeonID);
    },
    // update (dt) {},
});
