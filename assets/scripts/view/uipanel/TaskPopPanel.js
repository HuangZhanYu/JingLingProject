/*
作者（Author）:    jason

描述（Describe）: 工作界面
*/
/*jshint esversion: 6 */
import UIBase from '../UIBase';
import BigNumber from '../../tool/BigNumber';
import StringTool from '../../tool/StringTool';
import CanShuTool from '../../tool/CanShuTool';
import { PlayerIntAttr } from '../../part/PlayerBasePart';
import MessageCenter from '../../tool/MessageCenter';
import { MessageID } from '../../common/MessageID';
import { TimeTool_TimeTransformUpdate } from '../../tool/TimeTool';

let mCDTimeGroup = []; //计时器名称
let mIsInScreen = [];  //是否可见
let mTaskInfo = [];    //任务组件信息
let mIsShowLeftButton = []; //左边按钮组是否显示

let MAXTASKLEVEL = 2000000000;

let mtaskTotalCount = 0; //任务总数量 读表

cc.Class({
    extends: UIBase,

    properties: {
       TaskItemPrefab:cc.Prefab
    },

    onLoad () {
        this.taskObjList = [];

        MessageCenter.addListen(this,this.LevelUpResponse,MessageID.MsgID_AddGoldResponse);

        //读表拿到任务总数
        let Conf_JinBiRenWu = LoadConfig.getConfig(ConfigName.JinBiRenWu);
        for(let conf in Conf_JinBiRenWu)
        {
            ++mtaskTotalCount;
        }
        if(mtaskTotalCount <= 0 || mtaskTotalCount == null)
        {
            console.error("金币任务表错误");
            return;
        }
        for(let i = 0; i < mtaskTotalCount; ++i)
        {
            mCDTimeGroup[i] = 0;
            mIsInScreen[i] = false;
            mIsShowLeftButton[i] = false;
        }
        this.UpdateItemInfo();   
    },
    showPanel(param)
    {
        this._super();
        //设置刷新顶部信息
        this.UpdateTopBar();
        //显示预设
        this.UpdateCurrentTaskPrefab();
    },
    //判断当前显示几个
    UpdateCurrentTaskPrefab()
    {
        let count = 0;
        for(let i = 0;i < TaskPart.DailyTaskList.length; ++i)
        {
            if(TaskPart.DailyTaskList[i].TaskLevel == 0)
            {
                count = i + 1;
                break;
            }
        }
         //若全部都有等级 则为0
         if(count == 0)
         {
             count = mtaskTotalCount;
         }
         if(count > mtaskTotalCount)
         {
             count = mtaskTotalCount;
         }
         for(let i = 0; i < count; ++i)
         {
            this.taskObjList[i].node.active = true;
         }
         for(let i = count; i < mtaskTotalCount; ++i)
         {
            this.taskObjList[i].node.active = false;
         }
    },
    //刷新顶部信息
    UpdateTopBar()
    {
        this.SetPerSecond();

        let buyGoldCost = CanShuTool.GetCanShuData(CanShuTool.CanShuEnum.BuyGoldParams).Param[2];

        //判断是否没有重置
        let reviveTime = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_RevivalNumber];

        if(reviveTime < 1)
        {
            this._AutoDiamond.active = false;
        }
        else
        {
            this._DiamondNum.$Label.string = buyGoldCost;
            let goldStr = BigNumber.getValue(TaskPart.BuyGold);
            this._GoldNum.$Label.string = goldStr;
            if(PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_ZuanShi] >= buyGoldCost)
            {

            }
        }
    },

    //设置每秒金币（改为每分钟）
    SetPerSecond()
    {
        let result = BigNumber.mul(TaskPart.GetPerSecGold(),BigNumber.create("3600"));
        let showStr = StringTool.ZhuanHuan("【x】/小时",[BigNumber.getValue(result)]);

        this._GenGoldNum.$Label.string = showStr;
    },

    _onHelpBtnTouchEnd(sender, event)
    {
        
    },
    UpdateItemInfo()
    {
        let index = 0;
        for(let task of TaskPart.DailyTaskList)
        {
            if(index >= this.taskObjList.length)
            {
                this.addTask(task);
            }
           
            this.taskObjList[index].init(task,index);
            
            ++index;
        }
    },
    addTask(taskInfo) {
        let taskObj = cc.instantiate(this.TaskItemPrefab);
        taskObj.parent = this._ViewContent;
        let taskItemCom = taskObj.getComponent('TaskItem');
        //taskItemCom.init(taskInfo);

        this.taskObjList.push(taskItemCom);
    },
    //计时器刷新界面
    UpdateTimeInfo(seconds,index)
    {
        if(!this.taskObjList[index])
        {
            return;
        }
        //如果再当前显示区域,则刷新时间
        //if(mIsInScreen[index])
        {
            //seconds = parseInt(seconds);
            let realTaskTime = TaskPart.GetTaskInfo(index).BaseTime;
            this.taskObjList[index]._ProgressBar.$ProgressBar.progress = seconds / realTaskTime;
            let Integer =  Math.ceil(seconds);
            this.taskObjList[index]._Time.$Label.string = TimeTool_TimeTransformUpdate(Integer);
        }
    },
    //结束计时
    UpdateTimeInfoEnd(index)
    {
        
    },
    LevelUpResponse()
    {
        this.UpdateTopBar();
        //显示预设
        this.UpdateCurrentTaskPrefab();
        this.UpdateItemInfo();
       // MCLoopDragTool.UpdateCurrentPanel(mMCloopTool,count);
    },
    OnAutoClearTaskSuccess()
    {
        
    }
});
