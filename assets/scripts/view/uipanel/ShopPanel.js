/*
作者（Author）:    Holmes

描述（Describe）:
*/
/*jshint esversion: 6 */
import UIBase from "../UIBase";
import ViewManager from "../../manager/ViewManager";

cc.Class({
    extends: cc.Component,

    properties: {
        shopItemPrefab: cc.Prefab,
    },
    onLoad() {
        this.itemList = [];
        this.itemListObj = [];
        this.init();
    },
    //初始化商店界面
    init() {
        this.content =cc.find("ScrollView/view/content",this.node);
        let itemLength = 10;
        if (!itemLength) {
            console.log("ShopPanel itemLength is null");
            return;
        }
        for (let i = 0; i < itemLength; i++) {
            if (i > this.itemList.length) {
                this.addShopItem(i);
            }
            else {
                this.initShopItem(i);
            }
        }
    },
    addShopItem(ShopItemID) {
        let itemListObj = cc.instantiate(this.shopItemPrefab);
        //let PropsItem = ResourceManager.getInstance().getPrefab('PropsItem');
        this.content.addChild(itemListObj);
        this.itemList.push(itemListObj);
        this.itemListObj.push(itemListObj.getComponent("ShopItem"));
        this.initShopItem(ShopItemID);
    },
    initShopItem(ShopItemID) {
       //this.itemListObj[ShopItemID].showItem();
    },
    deleteShopItem() {

    },
});
