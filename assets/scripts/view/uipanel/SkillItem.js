// Learn cc.Class:
//  - [Chinese] http://docs.cocos.com/creator/manual/zh/scripting/class.html

cc.Class({
    extends: cc.Component,

    properties: {
        open:cc.Node, //开放
        lock:cc.Node, //锁住
        add:cc.Node, //添加
        openSkillIcon:cc.Sprite, //开放图标
        lockSkillIcon:cc.Sprite, //  锁住图标
        addSkillIcon:cc.Sprite, // 添加图标
        countDownBg:cc.Node, //倒计时父级
        countDown:cc.Sprite, //cd 圆圈
        countDownCount:cc.Label, //倒计时数字
        atlas:cc.SpriteAtlas, //技能图集
    },

    start () {
        this._addEvents();
    },
    onDestroy()
    {
        this.unschedule(updateCdTimre);
        this.skillData = null;
        this.index = null;
        this.removeEvents();
    },

    // update (dt) {},

    setSkillInfo(skillData,index)
    {
        let isOpen = skillData.IsOpen;
        this.skillData = skillData;
        this.index = index;
        this.lock.active = (isOpen == false);
        this.open.active = (isOpen == true && (isOpen == true && skillData.SkillID > 0))
        this.add.active = (isOpen == true && (isOpen == true && skillData.SkillID <= 0))
        if (isOpen) 
        {
            if (skillData.SkillID > 0) 
            {
                let skillInfo = LoadConfig.getConfigData(ConfigName.JiNeng_ZhuoChong,skillData.SkillID)
                let spriteFrame = this.atlas.getSpriteFrame(skillInfo.Icon)
                this.openSkillIcon.spriteFrame = spriteFrame;
            }
        }

        let cdTime = AbilityPart.GetCDTime(index);
        let isCd = (cdTime >= 0);
        this.countDownBg.active = isCd;
        if (isCd == true) 
        {
            
        }
    },
    _addEvents()
    {
        this.lock.on(cc.Node.EventType.TOUCH_END,this._onClickLockEvent,this);
        this.open.on(cc.Node.EventType.TOUCH_END,this._onClickLockEvent,this);
        this.add.on(cc.Node.EventType.TOUCH_END,this._onClickLockEvent,this);
    },
    removeEvents()
    {
        this.lock.off(cc.Node.EventType.TOUCH_END,this._onClickLockEvent,this);
        this.open.off(cc.Node.EventType.TOUCH_END,this._onClickLockEvent,this);
        this.add.off(cc.Node.EventType.TOUCH_END,this._onClickLockEvent,this);
    },
    //锁的按钮事件
    _onClickLockEvent()
    {
        console.log(">>>功能未实现c参考AbilityPrefabCreateor的OnLockClick方法");
        
        //SolveSkillPopPanel.Show(param.Index);
    },
    //开放的按钮事件
    _onClickOpenEvent()
    {
        AbilityPart.OnUseSkillRequest(this.index);
    },
    //添加的按钮事件
    _onClickAddEvent()
    {
        console.log(">>>功能未实现c参考AbilityPrefabCreateor的OnAddClick方法");
        //SkillSetPopPanel.Show();
    },
    //开始倒计时
    startCoolDown()
    {
        if (this.cdTime == null || this.cdTime > 0) 
        {
            return;   
        }
        this.cdTime = AbilityPart.GetCDTime(this.index);
        this.setCdTime();
        this.schedule(this.updateCdTimre,this,1,Math.ceil(this.cdTime),0);
    },
    //刷新倒计时
    updateCdTimre()
    {
        this.cdTime-=1;
        if (this.cdTime < 0) 
        {
            this.unschedule(updateCdTimre);
            this.countDown.active = false;
            return;   
        }else{
            this.setCdTime();
        }
        
    },
    //设置倒计时
    setCdTime()
    {
        if (AbilityPart.IsUsing(this.index)) 
        {
            this.countDownCount.string = "释放中";
            return;   
        }
        let skillInfo = LoadConfig.getConfigData(ConfigName.JiNeng_ZhuoChong,this.skillData.SkillID)
        this.countDownCount.string = TimeTool_TimeTransformUpdate(this.cdTime)
        let fillRange = this.cdTime / skillInfo.CDTime; //cd 进度
        this.countDown.fillRange = fillRange
    }
});
