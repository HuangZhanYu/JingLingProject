/*
作者（Author）:    skyHuang

描述（Describe）:
*/
/*jshint esversion: 6 */
import UIBase from '../UIBase';
import TimerManager from '../../tool/TimerManager';
import { PanelResName } from '../../common/PanelResName';
import PopupsController from '../PopupsController';

let HuoDeWuPinTiShiPanelData = { };
HuoDeWuPinTiShiPanelData.Tag = false;// --是否有奖励

//添加传一个资源table入口
HuoDeWuPinTiShiPanelData.ShowResources = function(resourcesData){
    PopupsController.getInstance().addShowPopup({urlOrPrefabOrNode : PanelResName.HuoDeWuPinTiShiPanel, panelPos : PanelPos.PP_Midle, param : resourcesData});
}
//显示界面
HuoDeWuPinTiShiPanelData.Show = function(itemTypeArray,itemIDArray,itemCountArray){
    if(!itemTypeArray){
        return;
    }
    let awardData = [];
    for (let i = 0; i < itemTypeArray.length; i++) {
        let data = {ItemNum : itemCountArray[i],ItemType : itemTypeArray[i],ItemID : itemIDArray[i]};
        awardData.push(data);
    }
    PopupsController.getInstance().addShowPopup({urlOrPrefabOrNode : PanelResName.HuoDeWuPinTiShiPanel, panelPos : PanelPos.PP_Midle, param : awardData});
}
window.HuoDeWuPinTiShiPanelData = HuoDeWuPinTiShiPanelData;

cc.Class({
    extends: UIBase,

    properties: {

    },
    onLoad() 
    {
        this.m_Data = null;
        this.delayTime = 1.2;
    },
    showPanel(resourcesData)
    {
        this._super(resourcesData);
        this.m_Data = resourcesData;
        this._AwardGrid.$AwardGrid.updateItemUI(resourcesData);
        this.delayClose();
    },
    //延迟关闭
    delayClose()
    {
        if(this.updateTimerKey && this.updateTimerKey >= 0)
        {
            TimerManager.removeTimeCallBack(this.updateTimerKey);
        }
        this.updateTimerKey = TimerManager.addTimeCallBack(this.delayTime,this.hiddenPanel.bind(this));
    },
    changeDelayTime(sec)
    {
        this.delayTime = sec;  
        TimerManager.removeTimeCallBack(this.updateTimerKey);
    },
    hiddenPanel()
    {
        this._super();
        this._AwardGrid.$AwardGrid.hideItemAll();
        PopupsController.getInstance().closeShowPopup();
    },
});
