/*
作者（Author）:    jason

描述（Describe）:
*/
/*jshint esversion: 6 */
import UIBase from "../UIBase";
import ViewManager from "../../manager/ViewManager";

class AlertInfo
{
    constructor()
    {
        this.clear();
    }
    clear()
    {
        this.alertMessage = "";
        this.confirmName = "确定";
        this.cancelName = "取消";
        this.singleName = "确定";
        this.confirmEvent = null;
        this.cancelEvent = null;
        this.singleEvent = null;
    }
}
cc.Class({
    extends: UIBase,

    properties: {
     
    },
    init: function () {
        this._super();
        this.alertInfo = new AlertInfo();
        this.node.active = false;
    },
    showPanel(param)
    {
        this._super();
        this.alertInfo.clear();
        this.alertInfo.alertMessage = param.mainText;
        this.alertInfo.singleName = param.singleText;
        this.alertInfo.confirmName = param.confirmText;
        this.alertInfo.cancelName = param.cancelText;
        this.alertInfo.confirmEvent = param.confirmCallback;
        this.alertInfo.cancelEvent = param.cancelCallback;
        this.alertInfo.singleEvent = param.singleCallBack;
        if(this.alertInfo.singleEvent)
        {
            this._SingleBtn.active = true;
            this._ConfirmBtn.active = false;
            this._CancelBtn.active = false;
        }
        else
        {
            this._SingleBtn.active = false;
            this._ConfirmBtn.active = true;
            this._CancelBtn.active = true;
        }
        this._messageLab.$Label.string = this.alertInfo.alertMessage;
    },
    //确定按钮的点击事件
    _onConfirmBtnTouchEnd(sender, event)
    {
        this.closePanel();
        if(this.alertInfo.confirmEvent && this.alertInfo.confirmEvent instanceof Function)
        {
            this.alertInfo.confirmEvent();
        }
    },
    //取消按钮的点击事件
    _onCancelBtnTouchEnd(sender, event)
    {
        this.closePanel();
        if(this.alertInfo.cancelEvent && this.alertInfo.cancelEvent instanceof Function)
        {
            this.alertInfo.cancelEvent();
        }
    },
    _onSingleBtnTouchEnd(sender, event)
    {
        this.closePanel();
        if(this.alertInfo.singleEvent && this.alertInfo.singleEvent instanceof Function)
        {
            this.alertInfo.singleEvent();
        }
    }
});
