/*
作者（Author）:    skyHuang

描述（Describe）:
*/
/*jshint esversion: 6 */
import UIBase from "../../view/UIBase";
import { TimeTool_TimeTransform } from '../../tool/TimeTool';
import PopupsController from '../PopupsController';
import { PanelResName } from '../../common/PanelResName';

let OffLinePopPanelData = { };
OffLinePopPanelData.Tag = false;// --是否有奖励

//--设置标记
OffLinePopPanelData.SetTag = function(tag){
    OffLinePopPanelData.Tag = tag;
}
//--显示界面
OffLinePopPanelData.Show = function(tag){
    //--审核模式不显示
    if(LoginPart.GetAuditFlag()){
        return;
    }
    //--没有离线信息 不显示
    if(!OffLinePopPanelData.Tag)
    {
        return;
    }
    GuanQiaPart.GetOfflineInfo(); //--请求离线奖励
}
OffLinePopPanelData.SetData = function(msg){
    PopupsController.getInstance().addShowPopup({urlOrPrefabOrNode : PanelResName.OffLinePopPanel, panelPos : PanelPos.PP_Midle, param : msg});
}
window.OffLinePopPanelData = OffLinePopPanelData;
cc.Class({
    extends: UIBase,

    properties: {

    },
    onLoad() {
        this.m_Data = null;//服务器返回数据
    },
    showPanel(resourcesData){
        this._super(resourcesData);
        this.m_Data = resourcesData;
        this.onLoadAfter();
    },
    //--界面加载完成后，设置界面信息
    onLoadAfter(){
        if(!this.m_Data)
        {
            this.hiddenPanel();
            return;
        }
        //设置离线时长
        this._OffLine.$Label.string = TimeTool_TimeTransform(this.m_Data.OffLineTime);
        //当前关卡
        this._OffLine.$Label.string = this.m_Data.CurLevel;
        //离线重置
        this._OffLine.$Label.string = this.m_Data.ReviveTime;
        //离线关卡
        this._OffLine.$Label.string = this.m_Data.LstLevel;
        //设置获得的道具
        let awardData = [];
        for (let i = 0; i < this.m_Data.Types.length; i++) {
            if(this.m_Data.Types <= 0){
                break;
            }
            let data = {ItemNum : this.m_Data.Counts[i],ItemType : this.m_Data.Types[i],ItemID : this.m_Data.IDs[i]};
            awardData.push(data);
        }
        this._AwardGrid.$AwardGrid.updateItemUI(awardData);
    },
    //确认按钮点击事件
    _onRewardBtnTouchEnd(sender, event)
    {
        this.hiddenPanel();
        GuanQiaPart.GetOfflineAwards();
    },
    //帮助按钮点击事件
    _onHelpBtnTouchEnd(sender, event)
    {
        HelpPanel.Show(13);
    },
    hiddenPanel(){
        this._super();
        this._AwardGrid.$AwardGrid.hideItemAll();
        PopupsController.getInstance().closeShowPopup();
    },
});
