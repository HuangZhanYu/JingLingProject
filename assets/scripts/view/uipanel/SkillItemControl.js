import FunctionOpenTool from "./FunctionOpenTool";

/**
 * 技能item控制类
 */
cc.Class({
    extends: cc.Component,

    properties: {
        skill1:cc.Node, //技能1
        skill2:cc.Node, //技能2
        skill3:cc.Node, //技能3
        buttonSet:cc.Node //设置??
    },

    start () 
    {
        this._addEvents();
    },
    onDestroy()
    {
        this._removeEvents();
    },

    _addEvents()
    {
    },
    _removeEvents()
    {
    },
	destroy()	
    {
        let result = this._super()
        this.node.destroy();  
        return result;
    },
    /**
     * 初始化技能信息
     */
    initSkillItem()
    {
        this.isOpen(); //判断是否开启
        if (AbilityPart.SkillGrooves == nil) 
        {
            console.error(">>>主动技信息错误")   ;
            return;
        }
        for (let index = 0; index < AbilityPart.SkillGrooves.length; index++) 
        {
            let skillData = AbilityPart.SkillGrooves[index];
            let skillItem = this._getSkillItemByIndex(index);
            if (skillItem == null) 
            {
                console.error(">>>>技能item脚本没有获取到")
                continue;   
            }
            skillItem.setSkillInfo(skillData,index);
        }
    },
    /**
     * 是否开放
     */
    isOpen()
    {
        let state = FunctionOpenTool.IsFunctionOpen(FunctionOpenTool.FuntionEnum.MainSkillButton)
        this.node.active = state;
        return state;
    },

    /**
     * 根据下标来获取技能item
     * @param {*} index 
     */
    _getSkillItemByIndex(index)
    {
        let skillItemObj = null;
        if (index == 0) 
        {
            skillItemObj = this.skill1
        }else if (index == 1)  
        {
            skillItemObj = this.skill2
        }else if (index == 2)
        {
            skillItemObj = this.skill3
        }else{
            console.error(">>>>下标不对")
            return null;
        }
        let skillItem = skillItemObj.getComponent("SkillItem");
        return skillItem;
    },

  
});
