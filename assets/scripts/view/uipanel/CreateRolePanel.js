/*
作者（Author）:    jason

描述（Describe）: 创建角色界面
*/
/*jshint esversion: 6 */
import UIBase from '../UIBase';
import ShieldedWordManager from '../../tool/ShieldedWordManager';
import ModelManager from '../../manager/ModelManager';
import { ModelNames } from '../../common/ModelNames';
import MessageCenter from '../../tool/MessageCenter';
import { MessageID } from '../../common/MessageID';
import { CreateRoleResult } from '../../part/LoginPart';
import ViewManager from '../../manager/ViewManager';
import { PanelResName } from '../../common/PanelResName';
const i18n = require('LanguageData');
cc.Class({
    extends: UIBase,

    properties: {
     
    },
    onLoad()
    {
        this.randomName();

        MessageCenter.addListen(this,this.onCreatePlayerError,MessageID.MsgID_CreatePlayerError);
        MessageCenter.addListen(this,this.onCheckNameSuccess,MessageID.MsgID_CheckNameSuccess);
    },
    init: function () {
        this._isReused = false; //界面是否重复利用
        this.playerInfoModel = ModelManager.getModel(ModelNames.PlayerInfoModel);
        this.mNameAList = [];
        this.mNameBList = [];
        this.mNameCList = [];
        let Conf_MingZiKu = LoadConfig.getConfig(ConfigName.MingZiKu);
        for(let key in Conf_MingZiKu)
        {
            let mingZi = Conf_MingZiKu[key];

            this.mNameAList.push(mingZi.NameA);
            this.mNameBList.push(mingZi.NameB);
            this.mNameCList.push(mingZi.NameC);
        }
    },
    randomName()
    {
        let nameAIndex = Math.floor(Math.random()*(this.mNameAList.length - 1));
        let nameBIndex = Math.floor(Math.random()*(this.mNameBList.length - 1));
        let nameCIndex = Math.floor(Math.random()*(this.mNameCList.length - 1));
        this._NameEditbox.$EditBox.string = this.mNameAList[nameAIndex]+this.mNameBList[nameBIndex]+this.mNameCList[nameCIndex];
    },
    _onConfirmButtonTouchEnd(sender, event)
    {
        let name = this._NameEditbox.$EditBox.string;
        if(name === '')
        {
            return;
        }
        let checkName = ShieldedWordManager.CheckShieldedWord(name);
        if(name != checkName)
        {
            console.log(`_nameEditBox  = ${name}  checkName = ${checkName}`);
            return;
        }
        this.playerInfoModel.playerName = name;
        console.log(`_nameEditBox  = ${name}`);
        LoginPart.CheckNameRequest(name);
    },
    _onBoyTouchEnd(sender, event)
    {
        this._SelectNode.x = event.target.x;
        console.log('_onBoyTouchEnd');
    },
    _onGirlTouchEnd(sender, event)
    {
        this._SelectNode.x = event.target.x;
        console.log('_onGirlTouchEnd');
    },
    _onRandNameButtonTouchEnd(sender, event)
    {
        this.randomName();
    },
    onCreatePlayerError(errID)
    {
        let alertInfo = {
            mainText: "",
            singleText: i18n.t('AlertText.Button_Common_Yes'),
            singleCallBack: function () {
                
            },
        };
        if ((errID == CreateRoleResult.Already)) {
            alertInfo.mainText = '该名称已被使用';//ToolTipPopPanel.Show(45);
        } else if ((errID == CreateRoleResult.Occupy) ){
            alertInfo.mainText = '该名称已被使用';//ToolTipPopPanel.Show(45);
        }
        ViewManager.alert(alertInfo);
    },
    //名字检查合格
    onCheckNameSuccess()
    {
        ViewManager.addPanel(PanelResName.OneGeniusPanel,(panel)=>{
            ViewManager.removePanelByUrl(PanelResName.CreateRoolPanel);
        });
    },
    onDestroy() {
        MessageCenter.delListen(this);
    }
    // update (dt) {},
});
