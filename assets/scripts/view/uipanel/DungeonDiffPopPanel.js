
import UIBase from "../UIBase";
const i18n = require('LanguageData');
import { PlayerIntAttr } from "../../part/PlayerBasePart";
import BigNumber from "../../tool/BigNumber";
import DaoJuTool from "../../tool/DaoJuTool";
import { ResourceTypeEnum } from "../../tool/ResourceTool";
import ViewManager, { PanelPos } from "../../manager/ViewManager";
import { PanelResName } from "../../common/PanelResName";
var CurrentDungeonID = 0;
var DungeonRound = 0;
/**
 * 副本信息面板
 */
cc.Class({
    extends: UIBase,

    properties: {
        closeBtn:cc.Node, //关闭按钮
        copyIcon:cc.Sprite, //副本的图标
        copyName:cc.Label, //副本名字
        remainCount:cc.Label, //副本剩余挑战次数
        headIcon:cc.Sprite, //boss头像
        fixAwardName:cc.Label, //固定奖励的道具名字
        diffcult:cc.Label, //副本难度描述
        bossfeture:cc.Label, //boss特点
        monsterFeture:cc.Label, //小怪特点
        fixAwardParent:cc.Node, // 固定奖励item的父级
        exteenAdd:cc.Label, //额外奖励添加的百分比
        randomAwardParent:cc.Node, //随机奖励item的父级
        fightBtn:cc.Node, // 挑战按钮
        unFightBtn:cc.Node, //不能挑战的按钮提示
        saoDangBtn:cc.Node, //扫荡按钮
        layerItemParents:cc.Node, //副本层数item的父级
        LayerItem:cc.Node, //副本层数item
        loopScrowObj:cc.Node, //无限滑动组件挂载节点
    },

    showPanel(dungeonID)
    {
        this.curDungeonID = dungeonID;
        this._super();
        this._initObj();
        this._getCurrentDungeonLayer();
        this._setCopyRemainCount();
        this._setCopyBaseInfo(this.currentShoudFightIndex);
        this._initLayerItem();
        this._addEvents();
        //TODO 定位到显需要挑战的副本层数
    },
    hiddenPanel()
    {
        this._super();
        this._removeEvents();
    },
    _addEvents()
    {
        this.fightBtn.on(cc.Node.EventType.TOUCH_END,this._onClickFightBtn,this);
        this.saoDangBtn.on(cc.Node.EventType.TOUCH_END,this._onClickSoaDangBtn,this);
        this.closeBtn.on(cc.Node.EventType.TOUCH_END,this._onClickCloseBtn,this);
    },
    _removeEvents()
    {
        this.fightBtn.off(cc.Node.EventType.TOUCH_END,this._onClickFightBtn);
        this.saoDangBtn.off(cc.Node.EventType.TOUCH_END,this._onClickSoaDangBtn);
        this.closeBtn.off(cc.Node.EventType.TOUCH_END,this._onClickCloseBtn);
    },
    /**
     * 点击挑战按钮事件
     * @param {*} e 
     */
    _onClickFightBtn(e)
    {
        let curDungeonID = this.curDungeonID;
        let currentShoudFightIndex = this.currentShoudFightIndex;
        CurrentDungeonID = this.curDungeonID;
        DungeonRound = this.currentShoudFightIndex;
        ViewManager.addPanel(PanelResName.DungeonZhenRongPanel,(panel)=>
        {

        },PanelPos.PP_Midle,{curDungeonID,currentShoudFightIndex});
        //DungeonPart.OnBeginFightVerifyRequest(this.curDungeonID,this.currentShoudFightIndex);
        this.hiddenPanel();
    },
    /**
     * 点击扫荡按钮事件
     * @param {*} e 
     */
    _onClickSoaDangBtn(e)
    {

    },
    /**
     * 点击关闭按钮事件
     * @param {*} e 
     */
    _onClickCloseBtn(e)
    {
        this.hiddenPanel();
    },
    /**
     * h
     */
    _getCurrentDungeonLayer()
    {
        let curRound = DungeonPart.GetTopRound(this.curDungeonID); //当前已经通关的最高层数
        this.curMaxRound = curRound;
        let curMaxRoundR = DungeonPart.GetCurDungeonMaxRound(this.curDungeonID); //副本最高层数
        this.currentShoudFightIndex = (curRound == -1)?1:curRound+1; //当前挑战的下标
        this.countRound = curRound + 3;
        this.countRound = (this.countRound >= curMaxRoundR)?curMaxRoundR:this.countRound;
        if (this.currentShoudFightIndex > curMaxRoundR) {
            this.currentShoudFightIndex = curMaxRoundR;
        }
        //this.loopScrow.initData(this.countRound,this._onRenderCopyLayerItem.bind(this));
    },
    /**
     * 初始化副本层数item
     */
    _initLayerItem()
    {
        if (this.layerItems == null) 
        {
            this.layerItems = [];
        }
        let count = this.countRound - this.layerItems.length;
        for (let index = 0; index < count; index++) 
        {
            let layerItemObj = cc.instantiate(this.LayerItem);
            let layerItem = layerItemObj.getComponent("CopyLayerItem");
            layerItemObj.y = 0;
            layerItem.onClickFunc = this._onClickCopyLayerItemEvent.bind(this);
            this.layerItemParents.addChild(layerItemObj);
            this.layerItems.push(layerItem);
        }
        for (let index = 0; index < this.layerItems.length; index++) 
        {
            let layerItem = this.layerItems[index];
            let hasData = (index < this.countRound);
            layerItem.node.active = hasData;
            if (hasData == true) 
            {
                let copyId = this.curDungeonID * 1000 + index+1;
                layerItem.updateItem(copyId,this.curMaxRound);
            }
            
        }
    },
    /**
     * 点击副本层数item事件 TODO 现在还没赋值， 应该要在无限滑动组件里面赋值点击事件
     */
    _onClickCopyLayerItemEvent(layerItemNode)
    {
        let layerItem = layerItemNode.getComponent("CopyLayerItem");
        let guanQiaId = layerItem.copyId%1000;
        this._setCopyBaseInfo(guanQiaId);
    },

    _initObj()
    {
       
        //this.loopScrow = this.loopScrowObj.getComponent("LoopScrow");
    },
    /**
     * 设置副本剩余次数信息
     */
    _setCopyRemainCount()
    {
        let canShuInfo = LoadConfig.getConfigData(ConfigName.CanShu,15);
        let totleCount = canShuInfo.Param[1]+PlayerAttrPart.GetSummaryAttr(PlayerAttrPart.Addition_Summary_Type.DungeonTimeAdd);
        this.remainCount.string = "可挑战次数："+DungeonPart.ChallengeCount+"/"+totleCount;
    },
    /**
     * 设置副本基础信息
     * @param {*} index 
     */
    _setCopyBaseInfo(index)
    {
        let diXiaChengData = LoadConfig.getConfigData(ConfigName.DiXiaCheng,this.curDungeonID);
        if (diXiaChengData == null) 
        {
            console.error(">>>没有获取到地下城的数据："+this.curDungeonID);
            return;   
        }
        this.copyName.string = diXiaChengData.Name.toString();
        this.copyIcon.spriteFrame = "";// dataCeng.Icon //TODO 现在副本的图片还没设置
        let guanQiaId =  this.curDungeonID * 1000 + index;
        let guanQiaData = LoadConfig.getConfigData(ConfigName.DiXiaCheng_GuanKa,guanQiaId);
        if (guanQiaData == null) 
        {
            console.error(">>>没有获取到地城关卡数据："+guanQiaId);
            return;
        }
        this.diffcult.string = guanQiaData.NanYiDu;
        this.bossfeture.string = guanQiaData.BossTeDian;
        this.monsterFeture.string = guanQiaData.GuaiWuTeDian;
        this._setFixAwardData(guanQiaData);
        this._setRandomAward(guanQiaData);
        this._setBossIcon(guanQiaData.BossID);
        this._updateFightBtnAndSaoDangBtnState(index);
    },
    /**
     * 刷新挑战按钮以及扫荡的按钮显示隐藏状态
     */
    _updateFightBtnAndSaoDangBtnState(index)
    {
        let hasTongGuan = index <= (this.curMaxRound+1);
        this.fightBtn.active = hasTongGuan;
        this.unFightBtn.active = !hasTongGuan;
       
        let canSaoDang = index <= this.curMaxRound; //TODO 这里还需要判断VIp的等级  因为现在VIP的数据还没有，所以暂时没有判断
        this.saoDangBtn.active = canSaoDang;
    },
    /**
     * 设置副本boss图标
     * @param {*} bossId 
     */
    _setBossIcon(bossId)
    {
        let shiBing = LoadConfig.getConfigData(ConfigName.ShiBing,bossId);
        if (shiBing == null) 
        {
            console.error(">>>没有获取到士兵的数据"+bossId); 
            return;
        }
        let modleData = LoadConfig.getConfigData(ConfigName.MoXing,shiBing.ModelID);
        if (modleData) 
        {
            console.error(">>>没有获取到模型数据"+shiBing.ModelID); 
            return;
        }
        //TODO 现在还不清楚是加载模型还是图片
    },
    /**
     * 设置固定奖励
     * @param {*} guanQiaData 
     */
    _setFixAwardData(guanQiaData)
    {
        let jiangLiData = LoadConfig.getConfigData(ConfigName.JiangChi,guanQiaData.ShengLiJiangChi);
        if (jiangLiData == null) 
        {
            console.error(">>>m没有获取到奖励信息")   
            return;
        }
        let externAdd  = DungeonPart.GetStoneBonus(); //额外加成
        
        if (jiangLiData.ResourceTypes[0] == ResourceTypeEnum.ResType_DaoJu && jiangLiData.ResourceIDs[0] == 110) 
        {
            externAdd = 0;
            this.exteenAdd.string = "0%";
            this.fixAwardName.string = i18n.t('CopyeText.ChaoYueShidioaLuo');
        } 
        else 
        {
            
            let matOne = BigNumber.div(externAdd,BigNumber.create("100"));
            this.exteenAdd.string = BigNumber.getValue(matOne)+"%";
            this.fixAwardName.string = i18n.t('CopyeText.HuaShiDiaoLuo');
        }
        let count = jiangLiData.MinCounts[0];
        if (externAdd != 0 && externAdd != "0") 
        {
            externAdd = BigNumber.add(10000,externAdd);
            let value = BigNumber.mul(BigNumber.create(count),externAdd);
            count = BigNumber.div(value,BigNumber.create(10000));
        }
        let awardItem = null;
        if (this.fixAwardParent.childrenCount == 0) 
        {
            awardItem = DaoJuTool.LoadDaoJu(this.fixAwardParent);
        }
        awardItem = this.fixAwardParent.children[0];
        DaoJuTool.SetDaoJuItemText(awardItem,count);
        DaoJuTool.SetDaoJuImage(awardItem,jiangLiData.ResourceTypes[0],jiangLiData.ResourceIDs[0]);
        let ResType = jiangLiData.ResourceTypes[0];
        let ResID = jiangLiData.ResourceIDs[0];
        DaoJuTool.SetDaoJuInfoClickEvent(awardItem,{ResType,ResID})
    },
    /**
     * 设置关卡奖励
     * @param {*} guanQiaData 
     */
    _setRandomAward(guanQiaData)
    {
        let jiangLiData = LoadConfig.getConfigData(ConfigName.JiangChi,guanQiaData.ShengLiJiangChi);
        if (jiangLiData == null) 
        {
            console.error(">>>m没有获取到奖励信息")   
            return;
        }
        for (let index = 0; index < this.randomAwardParent.childrenCount; index++) 
        {
            const child = this.randomAwardParent.children[index];
            child.active = false;
        }
        let awardItemChildIndex = -1;
        for (let index = 0; index < jiangLiData.ResourceTypes.length; index++) 
        {
            if (jiangLiData.ResourceTypes[index] == 0) 
            {
                break;   
            }
            if (index != 0) 
            {
                let awardItem = null;
                awardItemChildIndex+=1;
                if (awardItemChildIndex < this.randomAwardParent.childrenCount && 
                    this.randomAwardParent.childrenCount != 0) 
                {
                    awardItem = this.randomAwardParent.children[awardItemChildIndex];
                    awardItem.active = true;
                }else
                {
                    awardItem = DaoJuTool.LoadDaoJu(this.randomAwardParent);
                }
                
                DaoJuTool.SetDaoJuItemText(awardItem,jiangLiData.MinCounts[index]);
                DaoJuTool.SetDaoJuImage(awardItem,jiangLiData.ResourceTypes[index],jiangLiData.ResourceIDs[index]);
                let ResType = jiangLiData.ResourceTypes[index];
                let ResID = jiangLiData.ResourceIDs[index];
                DaoJuTool.SetDaoJuInfoClickEvent(awardItem,{ResType, ResID});
            }
            
        }
    },
    
    start () {

    },

    // update (dt) {},
});
