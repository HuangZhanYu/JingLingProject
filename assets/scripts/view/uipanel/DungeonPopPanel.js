import { PlayerIntAttr } from "../../part/PlayerBasePart";
import { TimeTool_GetIntPart } from "../../tool/TimeTool";
import UIBase from "../UIBase";

let MaxChallengeCount = 0;//最大挑战次数
let mSortList = []; //索引排序
let copyItemList = []; //缓存的副本item列表
let hasInit = false;
/**
 * 副本面板
 */
cc.Class({
    extends: UIBase,

    properties: {
        content:cc.Node, //滑动组件的内容
        daoJuFather:cc.Node, //道具父节点
        daoJuObj:cc.Node, //道具加载后对象
        copyItem:cc.Node, //地下城预设
        ChallengeCount:cc.Label,//挑战字数文字
        Time:cc.Node, //时间文字
        TimeText:cc.Label,
        mGuideRoot:cc.Node, //新手引导挂点
        times:cc.Node, //时间父级
        closeBtn:cc.node,

    },
    onLoad () 
    {
        this.initData();
        hasInit = true;
    },
    initData()
    {
        let diXiaCheng = LoadConfig.getConfig(ConfigName.DiXiaCheng);
        if (diXiaCheng == null ) {
            console.error(">>>>地下城表是空的");
            return;
        }
        let canShuInfo = LoadConfig.getConfigData(ConfigName.CanShu,15);
        if (canShuInfo == null) {
            console.error("参数表为空,ID:15 地下城免费挑战次数");
            return;
        }
        let param = canShuInfo.Param;
        MaxChallengeCount = param[0];
    },
        //
    showPanel()
    {
        if (hasInit == false) 
        {
            this.onLoad();   
        }
        this._super();
        this._sort(); //给副本信息排序
        this._setTopInfo(); //设置顶上信息
        this._setCopyInfo(); //设置副本信息
        this._showGuide();
        this._addEvents();
    },
    hiddenPanel()
    {
        this._super();
        this._removeTimeUpdate();
        this._removeEvents();
    },
    _addEvents()
    {

    },
    _removeEvents()
    {

    },

    /**
     * 创建副本item
     */
    _createCopyItem()
    {
        let itemObject = cc.instantiate(this.copyItem);
        this.content.addChild(itemObject);
        itemObject.position = cc.Vec2.ZERO;
        let copyItem = itemObject.getComponent("CopyItem");
        if (copyItem == null) {
            console.error(">>>没有获取到CopyItem脚本组件")
            return;
        }
        copyItemList.push(copyItem);

    },
    /***
     * 设置副本信息
     */
    _setCopyInfo()
    {
        let itemCount = copyItemList.length;
        let count = mSortList.length - itemCount;
        for (let index = 0; index < count; index++) 
        {
            this._createCopyItem();
        }
        
        let layerOut = this.content.getComponent(cc.Layout);
        layerOut.updateLayout();
        let diXiaCheng = LoadConfig.getConfig(ConfigName.DiXiaCheng);
        for (let key = 0; key < mSortList.length; key++) 
        {
            let copyItems = copyItemList[key];
            let copyIndex = mSortList[key];
            let copyData = diXiaCheng[copyIndex+""];
            if (copyData == null) 
            {
                console.error(">>>没有获取到副本数据"+copyIndex);
                continue;   
            }
            if (copyItems == null) 
            {
                console.error(">>>没有获取到副本item"+key);
                continue;  
            }   
            copyItems.updateItem(copyData,copyIndex);
        }
    },
    /***
     * 设置顶上信息
     */
    _setTopInfo() //设置顶上信息（副本剩余次数以及次数刷新时间）
    {
        let maxCount = MaxChallengeCount + Number(PlayerAttrPart.GetSummaryAttr(PlayerAttrPart.Addition_Summary_Type.DungeonTimeAdd));
        let mChallengeCount = DungeonPart.ChallengeCount//当前挑战剩余次数
       
        let canshuInfoTime = LoadConfig.getConfigData(ConfigName.CanShu,16) //取参数表的挑战次数刷新时间间隔
        if (canshuInfoTime == null )
        {
            console.error("参数表为空,ID:16 副本挑战次数恢复刷新间隔");
            return
        }
       
        this.ChallengeCount.string = mChallengeCount+"/"+maxCount;
        let hasRemainCount = (mChallengeCount < maxCount);
        this.times.active = hasRemainCount;
        if (hasRemainCount == true) 
        {
            this._removeTimeUpdate();
            this._updateTime();
            this.schedule(this._updateTime,this,1)
        }
    },
    /**
     * 刷新副本刷新的时间
     */
    _updateTime()//
    {
        let mLastRecoverTime = DungeonPart.LastRecoverTime //上次恢复时间
        let countDownTime = canshuInfoTime.Param[1] * 60; //分钟转秒
        let countDownSec = countDownTime - (TimePart.GetUnix() - mLastRecoverTime);
        let Integer =  math.ceil(countDownSec);
        let show = TimeTool_TimeTransformUpdate(Integer);
        this.Time.string = show;
    },
    _removeTimeUpdate()
    {
        this.unschedule(this._updateTime);
    },
    /***
     * 排序副本  已开放的在前面，未解锁在最后面
     */
    _sort()
    {
        mSortList = [];
        let diXiaCheng = LoadConfig.getConfig(ConfigName.DiXiaCheng);
        if (diXiaCheng == null ) {
            console.error(">>>>地下城表是空的");
            return;
        }
        let hasOpen = []; //已开放
        let hasUnLockNotOpen = []; //已解锁未开放
        let hasLock = []; // 未解锁
        let index = 1;
        for (const key in diXiaCheng) 
        {
            let copyData = diXiaCheng[key];
            index = Number(key);
            let functionID = 49 + index;
            let functionInfo = LoadConfig.getConfigData(ConfigName.GongNengKaiFang,functionID);
            if (functionInfo == null) 
            {
                console.error("功能开放表为空ID"+functionID)
                continue;   
            }
            let playerLevel = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_DengJi];
            if (playerLevel < functionInfo.ZhuJiaoDengJi) 
            {
                hasLock.push(index);
            }else
            {
                if (this._IsOpenToday(copyData) == true) 
                {
                    hasOpen.push(index);
                }else
                {
                    hasUnLockNotOpen.push(index);
                }
            }
            index+=1;
        }
        mSortList = mSortList.concat(hasOpen,hasUnLockNotOpen,hasLock);
    },
    _IsOpenToday(data) //今日该副本是否开放
    {
        let dayNum = new Date().getDay(); // -- 1  ---- 7
        let numTransform = Number(dayNum)
        //存下表格数据，用于判断今日周几
        let monday = data.IsMonday
        let tuesday = data.IsTuesday
        let wednesday = data.IsWednesday
        let thursday = data.IsThursday
        let friday = data.IsFriday
        let saturday = data.IsSaturday
        let sunday = data.IsSunday
    
        //根据今天是周几以及表格配置的周几开放，判断今天是否开启
        let result = (numTransform == 1 && monday == 1 ||
            numTransform == 2 && tuesday == 1 ||
            numTransform == 3 && wednesday == 1 ||
            numTransform == 4 && thursday == 1 ||
            numTransform == 5 && friday == 1 ||
            numTransform == 6 && saturday == 1 ||
            numTransform == 0 && sunday == 1 )
        return result;
    },
    _showGuide()
    {
        if (this.mGuideRoot != null) 
        {
            console.log(">>>设置了新手引导的root")
            GuideManager.NeedShowFinger(13, 1303, this.mGuideRoot);
        }
    },
    onDestroy()
    {
        super.onDestroy();
        copyItemList = [];
        hasInit = false;
    }
    // update (dt) {},
});
