/*
作者（Author）:    jason

描述（Describe）: 背包界面
*/
/*jshint esversion: 6 */
import UIBase from '../UIBase';
import ViewManager from "../../manager/ViewManager";
import MessageCenter from "../../tool/MessageCenter";
import {CommonPrefabNames, PanelResName} from "../../common/PanelResName";
import GOTool from "../../tool/GOTool";
import DaoJuTool from "../../tool/DaoJuTool";

let transform;
let gameObject;
let mLuaBehaviour;
let mMCloopTool_Hero;         //卡牌无限列表
let mMCloopTool_Treasure;     //装备无限列表
let mMCloopTool_Item;         //道具无限列表
let useID = null;             //道具ID

let heroList = {};             //用于缓存筛选后的卡牌列表
let mTreasureList = [];        //用于缓存整合后的装备列表

const HighLightType = {
    Prop: 1,        //道具
    Hero: 2,        //精灵
    Equip: 3        //装备
};

cc.Class({
    extends: UIBase,

    properties: {},

    onLoad() {
        this.currWindow = PanelResName.BackPackPopPanel;
    },
    showPanel() {
        this.initPanel(HighLightType.Prop);
        this._ItemsPanel.active = true;
        this._HerosPanel.active = false;
        this._EquipsPanel.active = false;
    },
    initPanel(uiType) {
        switch (uiType) {
            case HighLightType.Prop:
                break;
            case HighLightType.Hero:
                break;
            case HighLightType.Equip:
                break;
        }
    },
    //点击事件相关------------------------------------------------------------------------------------
    //点击筛选
    _OnFilterTouchEnd(){

    },
    //点击详情区域
    _OnAreaBtnClick(go,data){
        //DigimonDetailPopPanel.ShowBackPackCard(data.CardId)
    },
    //点击装备详情区域
    _OnTreausreAreaTouchEnd(go,data){
        //ViewManager.addPanel(PanelResName.ResType_ZhuangBeim data.TreasureID,data.ObjID);
    },
    //关闭
    _onCloseBtnTouchEnd(){
        ViewManager.closePanel(this.currWindow);
    },
    //道具按钮
    _onPropsBtnTouchEnd(sender, event) {
        //this.initPanel(HighLightType.Prop);
        this._ItemsPanel.active = true;
        this._HerosPanel.active = false;
        this._EquipsPanel.active = false;
    },
    //精灵按钮
    _onElfBtnTouchEnd(sender, event) {
        //this.initPanel(HighLightType.Hero);
        this._ItemsPanel.active = false;
        this._HerosPanel.active = true;
        this._EquipsPanel.active = false;
    },
    //装备按钮
    _onEquipBtnTouchEnd(sender, event) {
        //this.initPanel(HighLightType.Equip);
        this._ItemsPanel.active = false;
        this._HerosPanel.active = false;
        this._EquipsPanel.active = true;
    },
    //mTreasureList整合自己需要的装备列表，将装备数据完全相同（除了唯一ID不同）的整合再一起
    initMTreasureList() {
        mTreasureList = [];
        for (let i = 0; i < mTreasureList.length; i++) {
            let listMember = mTreasureList[i];
            if (listMember === null) {
                console.log('阵容数据为空');
                return;
            }
            let treasureInfo = LoadConfig.getConfig(ConfigName.Yiwu);
            if (treasureInfo === null) {
                console.log('取表数据为空,ID: ConfigName.Yiwu : ' + ConfigName.Yiwu);
                return;
            }
            if (this.mTreasureListContain(listMember)) {
                let index = mTreasureList.length + 1;
                mTreasureList[index] = {};
                mTreasureList[index].Count = 1;
                mTreasureList[index].ObjID = listMember.ObjID;
                mTreasureList[index].TreasureID = listMember.TreasureID;
                mTreasureList[index].Level = listMember.Level;
                mTreasureList[index].BeyondLevel = listMember.BeyondLevel;
            }
        }
    },
    //查看this.mTreasureList是否包含资源ID、强化等级、超级等级相同的元素，包含就放在一个元素里面，只需要将count+1，这里没有区分唯一ObjID
    mTreasureListContain(item) {
        for (let i = 1; i < mTreasureList.length; i++) {
            if (mTreasureList[i].TreasureID === item.TreasureID && mTreasureList[i].Level === item.Level && mTreasureList[i].BeyondLevel === item.BeyondLevel) {
                mTreasureList[i].Count = mTreasureList[i].Count + 1;
                return true;
            }
        }
        return false;
    },
    //--打开界面相关
    // --打开背包界面
    show() {
        ViewManager.addPanel(PanelResName.BackPackPopPanel, (panel) => {
            this.showAfterLoad();
        });
        //MainPanel.PlaySkillAnimationDown()
    },
    //从进化打开
    showEvo() {
        ViewManager.addPanel(PanelResName.BackPackPopPanel, () => {
            this.showAfterLoad()
        });
    },
    //从回收装备回到背包装备界面
    updateRecycleEquip() {
        this.initMTreasureList();
       // this.updateCurEquipInfo();
    },

    //刷新界面相关
    //打开背包默认开启道具切页并刷新道具界面
    showAfterLoad() {
        //整合装备列表
        this.initMTreasureList();
        //打开并刷新道具切页
        //this.UpdatePanelInfo(HighLightType.Prop);
        //初始化精灵页签
        //FilterPopPanel.InitState()
    },

    //进化调用
    showEvoAfterLoad() {
        //整合装备列表
        this.initMTreasureList();
        //打开并刷新道具切页
        //this.setHighLight(HighLightType.Hero);
        //初始化精灵页签
        // this.updateQualityList();
    },

    //根据枚举打开并刷新某个切页
    updatePanelInfo(panelType) {
        // //显示高亮
        // this.setHighLight(panelType);
        // //排序
        // this.sortList(panelType);
        // //显示数据
        // this.showListData(panelType);
    },
    //进化界面调用
    updatePanelInfoByEvo(panelType) {
        // //显示高亮
        // this.SetHighLight(panelType);
        // //排序
        // this.SortList(panelType);
    },
    //刷新列表数据
    ShowListData(panelType) {
        switch (panelType) {
            // case HighLightType.Hero:
            //     mMCloopTool_Hero.SetmaxIndex(this.GetCyCles(heroList.length));
            //     break;
            // case HighLightType.Equip:
            //     mMCloopTool_Treasure.SetmaxIndex(#BackPackPart.mTreasureList);
            //     break;
            // case HighLightType.Prop:
            //     mMCloopTool_Item.SetmaxIndex(#BackPackPart.mItemList);
            //     break;
        }
    },
    //刷新道具当前界面
    UpdateCurPropInfo() {
        if (GOTool.IsDestroy(gameObject)) {
            return;
        }
        if (!gameObject.activeInHierarchy) {
            return;
        }
        this.TreasureSort();

        mMCloopTool_Treasure.length = BackPackPart.mTreasureList.length;
        // mMCloopTool_Treasure:SetmaxIndex(BackPackPopPanel.GetCyCles(#mTreasureList));
    },

    //刷新卡牌当前界面
    UpdateCurCardInfo() {
        if (GOTool.IsDestroy(gameObject)) {
            return;
        }
        if (!gameObject.activeInHierarchy) {
            return;
        }
        //FilterPopPanel.UpdateQualityList();
    },
    //加载公用道具
    LoadDaoJu(obj) {
        //加载道具
        let daoJu = cc.instantiate(CommonPrefabNames.DaoJuPrefab);
        // 创建一个道具UI
        let daoJuObj = new Object(daoJu);
        //设置父节点
        GOTool.SetParent(daoJuObj, obj);
        //设置伸缩
        // GOTool.SetVector(daoJuObj, TransformOperateType.LocalScale, {x=1, y=1, z=1});
        //设置位置
        // GOTool.SetVector(daoJuObj, TransformOperateType.LocalPosition, {x=0, y=0, z=0});
        //设置名字
        daoJuObj.name = "DaoJuPrefab";
    },
    //初始化一次精灵列表预设ElfPrefab
    InitElfListPrefab() {
        for (let i = 1; i <= 5; i++) {
            // let daojuObjPos = PanleFindChild(gameObject,"ElfPrefab/pos"..i);
            // LoadDaoJu(daojuObjPos);
        }
    },
    //初始化一次装备列表预设EquipPrefab
    InitEquipListPrefab() {
        for (let i = 1; i <= 5; i++) {
            // let daojuObjPos = PanleFindChild(gameObject,'EquipPrefab/pos'..i);
            // LoadDaoJu(daojuObjPos);
        }
    },
    //获取循环列表行数：精灵和装备
    GetCyCles(count) {
        let cycles = 0;
        if (count % 5 === 0) {
            cycles = Math.Floor(count / 5);
        } else {
            cycles = Math.Floor(count / 5) + 1;
        }
        return cycles;
    },
    //
    GetDaojuPrefabButton(daojuObj) {
        if (daojuObj === null) {
            console.log('传入的对象为空');
        }
        let frame = PanleFindChild(daojuObj, "Frame");
        if (frame !== null) {
            return frame;
        } else {
            console.log('获取道具中拥有按钮组件的对象Frame失败');
            return daojuObj;
        }
    },
    //刷新单行卡牌数据
    UpdateItemInfo_Hero(go, index) {
        if (go === null || index === null || index <= 0) {
            console.log('回调数据错误，没有找到go,或者索引小于等于0');
            return;
        }
        let wuPinXiang = this.GetSoldierComponentPath(go);
        //index代表行数，一行5个，所以执行一次需要最多设置5个道具的信息
        for (let i = 1; i <= 5; i++) {
            let count = (index - 1) * 5 + i;
            if (count <= heroList.length) {
                let listMember = this.GetCardInfo(count);
                if (listMember === null) {
                    console.log('阵容数据为空');
                    return;
                }
                let cardID = listMember.CardID;
                let cardInfo = LoadConfig.getConfig(ConfigName.ShiBing)[cardID];
                if (cardInfo === null) {
                    console.log(`取表数据为空,ID: ${cardInfo}`);
                    return;
                }
                let modelInfo = LoadConfig.getConfig(ConfigName.MoXing)[cardInfo.ModelID];
                if (modelInfo === null) {
                    console.log(`取模型表数据为空,ID: ${cardInfo.ModelID}`);
                    return;
                }

                DaoJuTool.SetDaoJuItemText(wuPinXiang.daojuObjPosList[i], mLuaBehaviour, listMember.Count);
                DaoJuTool.SetDaoJuImage(wuPinXiang.daojuObjPosList[i], mLuaBehaviour, DaoJuTool.ResourceType.ShiBingBiao, cardID);
                // mLuaBehaviour:AddClick(BackPackPopPanel.GetDaojuPrefabButton(wuPinXiang.daojuObjPosList[i]),this.OnAreaBtnClick,{CardId = cardID});
                // wuPinXiang.daojuObjPosList[i]:SetActive(true);

            } else {
                wuPinXiang.daojuObjPosList[i].SetActive(false);

            }
        }
    },
    //根据所选品质刷新卡牌列表
    filterHeroList() {
        let count = heroList.length;
        heroList = [];
        let index = 1;
        for (let i = 1; i <= mCardList.length; i++) {
            let cardid = mCardList[i].CardID;
            let conf_card = LoadConfig.getConfig(ConfigName.ShiBing)[cardid];
            if (conf_card === null) {
                console.log(`卡牌不存在${cardid}`);
                return;
            }
            //判断品质是否在其中
            for (let j = 1; i <= qualityAttr.length; j++) {
                if (conf_card.Quality === qualityAttr[j]) {
                    heroList[index] = mCardList[i];
                    index++;
                }
            }
        }
        //table.sort(heroList,BackPackPart.CardBagCom)
        //界面不存在则不刷新
        if (GOTool.IsDestroy(gameObject)) {
            return;
        }
        if (!gameObject.activeInHierarchy) {
            return;
        }
        if (count !== heroList.length) {
            mMCloopTool_Hero.SetmaxIndex(this.GetCyCles(heroList.length));
            //刷新到指定ID
            let id = this.GetEvolutedCardID();
            if (id !== -1) {
                let index = 1;
                for (let i = 1; i <= heroList.length; i++) {
                    if (heroList[i].CardID === id) {
                        index = i;
                        break;
                    }
                }
                MCLoopDragTool.LocatedPos(mMCloopTool_Hero, this.GetCyCles(index), this.GetCyCles(heroList.length));
            }
        } else {
            //刷新到指定ID
            let id = this.GetEvolutedCardID();
            if (id !== -1) {
                MCLoopDragTool.UpdateCurrentPanel(mMCloopTool_Hero, this.GetCyCles(heroList.length));
            } else {
                let index = 1;
                for (let i = 1; i <= heroList.length; i++) {
                    if (heroList[i].CardID == id) {
                        index = i;
                        break;
                    }
                }
                MCLoopDragTool.UpdateCurrentPanel(mMCloopTool_Hero, this.GetCyCles(heroList.length));
            }
        }

    },

    //刷新单行装备数据
    UpdateItemInfo_Treasure(go, index) {
        if (go === null || index === null || index <= 0) {
            console.log('回调数据错误，没有找到go,或者索引小于等于0');
            return;
        }
        let wuPinXiang = this.GetTreasureComponentPath(go);
        //index代表行数，一行5个，所以执行一次需要最多设置5个道具的信息
        for (let i = 1; i <= 5; i++) {
            let count = (index - 1) * 5 + i;
            if (count <= mTreasureList.length) {
                let listMember = mTreasureList[count];
                if (listMember === null) {
                    console.log('阵容数据为空');
                    return;
                }
                let treasureInfo = LoadConfig.getConfig(ConfigName.Yiwu)[listMember.TreasureID];
                if (treasureInfo == null) {
                    console.log(`取表数据为空,ID ${listMember.TreasureID}`);
                    return;
                }
                DaoJuTool.SetDaoJuEquipText(wuPinXiang.daojuObjPosList[i], mLuaBehaviour, listMember.Count, listMember.Level);
                DaoJuTool.SetDaoJuImage(wuPinXiang.daojuObjPosList[i], mLuaBehaviour, DaoJuTool.ResourceType.ZhuangBeiBiao, listMember.TreasureID, listMember.BeyondLevel);
                // mLuaBehaviour.AddClick(this.GetDaojuPrefabButton(wuPinXiang.daojuObjPosList[i]), this.OnTreausreAreaClick, {
                //     TreasureID = listMember.TreasureID,
                //     ObjID = listMember.ObjID
                // });
                wuPinXiang.daojuObjPosList[i].SetActive(true);
            } else {
                wuPinXiang.daojuObjPosList[i].SetActive(false);
            }
        }
    },

    //刷新单个道具数据
    UpdateItemInfo_Item(go, index) {
        if (go === null || index == null || index <= 0) {
            console.log('回调数据错误，没有找到go,或者索引小于等于0');
            return;
        }
        let wuPinXiang = this.GetItemComponentPath(go);
        let listMember = BackPackPart.mItemList[index];
        if (listMember === null) {
            console.log('道具数据为空');
            return;
        }
        let itemInfo = LoadConfig.ZiYuan_DaoJu[listMember.ResourceID];
        //设置道具数据
        mLuaBehaviour.SetText(wuPinXiang.ItemName, itemInfo.Name);
        if (itemInfo.ResourceType === 10) {
            this.ShowSuiPianInfo(wuPinXiang, itemInfo, listMember); //精灵碎片
        } else if (itemInfo.ResourceType === 11) {
            this.ShowSuiPianInfo(wuPinXiang, itemInfo, listMember); //装备碎片
        } else {
            mLuaBehaviour.SetText(wuPinXiang.Des, itemInfo.Note)
            DaoJuTool.SetDaoJuItemText(wuPinXiang.DaoJuPrefab, mLuaBehaviour, listMember.ResourceCount)
            wuPinXiang.CombineDes.SetActive(false)
            wuPinXiang.Des.SetActive(true)
            wuPinXiang.CombineNum.SetActive(false)
            wuPinXiang.ButtonCombine.SetActive(false)
            PanleFindChild(wuPinXiang.DaoJuPrefab, "Count").SetActive(true);
        }
        //
        DaoJuTool.SetDaoJuImage(wuPinXiang.DaoJuPrefab, mLuaBehaviour, DaoJuTool.ResourceType.DaoJuBiao, listMember.ResourceID);
        DaoJuTool.AddDaoJuClick(wuPinXiang.DaoJuPrefab, mLuaBehaviour, DaoJuTool.ResourceType.DaoJuBiao, listMember.ResourceID);
        //设置按钮状态
        this.SetItemButtonState(itemInfo.UseType, wuPinXiang, listMember.ResourceID)
    },

    //计算及显示碎片的信息、按钮等
    ShowSuiPianInfo(go, itemInfo, rID) {
        mLuaBehaviour.SetText(go.$CombineDes, itemInfo.Note);
        go.$CombineDes.active = true;
        go.$Des.active = false;
        go.$CombineNum.active = true;
        PanleFindChild(go.DaoJuPrefab, "Count").SetActive(false);

        if (itemInfo === null) {
            console.log(`取合成表数据为空,ID: ${rID.ResourceID}`);
            return;
        }
        if (itemInfo.ResourceType === 10) {
            let index = -2;
            for (let key in LoadConfig.getConfig(ConfigName.HeCheng_JingLing)) {
                //
            }
            if (index !== -2) {
                // mLuaBehaviour:SetText(go.CombineNum,rID.ResourceCount.."/"..Conf_HeCheng_JingLing[index].NeedCount)
                // go.$ButtonCombine.active = true;
                // if (rID.ResourceCount >= Conf_HeCheng_JingLing[index].NeedCount){
                //
                // } else{
                //     --拥有足够材料，显示可合成的按钮
                // TextAlignTool.SetTextColor(go.CombineNum,2,mLuaBehaviour)
                // go.CombineOne:SetActive(true)
                // go.CombineGray:SetActive(false)
                // mLuaBehaviour:AddClick(go.CombineOne,this.OnCombineClick,{daojuID = Conf_HeCheng_JingLing[index],rType = itemInfo.ResourceType})
                // }
            } else {
                //TextAlignTool.SetTextColor(go.CombineNum,6,mLuaBehaviour);
                go.$CombineOne.active = false;
                go.$CombineGray.active = true;
            }
        } else if (itemInfo.ResourceType === 11){
            // for k, v in pairs(Conf_HeCheng_ZhuangBei) do
            //             if itemInfo.ID == Conf_HeCheng_ZhuangBei[k].DebrisID then--找到装备碎片所需的合成数
            //                 index = k
            //                 break
            //             end
            //         end
            //         if index ~= -2 then
            //             mLuaBehaviour:SetText(go.CombineNum,rID.ResourceCount.."/"..Conf_HeCheng_ZhuangBei[index].NeedCount)
            //             go.ButtonCombine:SetActive(true)
            //             if rID.ResourceCount >= Conf_HeCheng_ZhuangBei[index].NeedCount then--拥有足够材料，显示可合成的按钮
            //                 TextAlignTool.SetTextColor(go.CombineNum,2,mLuaBehaviour)--绿色字体显示
            //                 go.CombineOne:SetActive(true)
            //                 go.CombineGray:SetActive(false)
            //                 mLuaBehaviour:AddClick(go.CombineOne,this.OnCombineClick,{daojuID = Conf_HeCheng_ZhuangBei[index],rType = itemInfo.ResourceType})
            //             else
            //                 TextAlignTool.SetTextColor(go.CombineNum,6,mLuaBehaviour)--红色字体显示
            //                 go.CombineOne:SetActive(false)
            //                 go.CombineGray:SetActive(true)
            //             end
            //         end
        }
    },
    //--合成按钮事件
    OnCombineClick(go,data){
        if(data.rType === 10){
            //BackPackPart.Compose(DaoJuTool.ResourceType.ShiBingBiao, data.daojuID.ID)
        } else if (data.rType == 11){
            //BackPackPart.Compose(DaoJuTool.ResourceType.ZhuangBeiBiao, data.daojuID.ID)
        } else {
            //console.log(`此道具不是装备也不是精灵ID= ${data.daojuID.DebrisID}`);
        }

    },



    onDestroy() {
        MessageCenter.delListen(this);
    }
});
