/*
作者（Author）:    skyHuang

描述（Describe）:
*/
/*jshint esversion: 6 */
import TimerManager from '../../tool/TimerManager';
let Thor = require('Thor');

cc.Class({
    extends: Thor,

    properties: {
    },
    onLoad () 
    {
        cc.game.addPersistRootNode(this.node);
        console.log('Loading  onLoad');
    },
    init()
    {
        this.m_CountDownTime = 0;
        this.isCanClose = true;
        this.countDownTimerKey = -1;
    },
    //显示loading界面
    showLoading(value)
    {
        let {
            text = 'Loading', showDelay = 4, closeTime = 60,
            isText = true, isDian = true, maskOpacity = 178, CountDownTime = 0,
            canClose = true, //未达到关闭时间，不允许关闭
            timeCallBack = ()=>{}
        } = value;
        this.node.active = true;
        this._Content.$Label.string = text;
        this._LoadAnimation.active = false;
        this._Mask.active = true;
        this._Mask.opacity = 0;
        this._Content.active = isText;
        this._Dian.active = isDian;
        this.isCanClose = canClose;

        this.hideCountDown();
        if (closeTime == -1) {
            return;
        }
        //根据showDelay设置loading显示时间
        this.showTimerKey = TimerManager.addTimeCallBack(showDelay, () => {
            this._LoadAnimation.active = true;
            this._Mask.opacity = maskOpacity;
            if (CountDownTime > 0) {
                this.showCountDown(CountDownTime, timeCallBack);
            }
        });
        //根据CloseLoading设置loading关闭时间
        this.closeTimerKey = TimerManager.addTimeCallBack(closeTime, () => {
            this.hideLoading();
        });
    },
    //隐藏界面
    hideLoading(closed = false) {
        if (!this.isCanClose && !closed) {
            return;
        }
        this.node.active = false;
        this.hideCountDown();
        TimerManager.removeTimeCallBack(this.showTimerKey);
        TimerManager.removeTimeCallBack(this.closeTimerKey);
    },
    //显示loading里面倒计时
    showCountDown(time,callBack)
    {
        this.m_CountDownTime = this.m_CountDownTime == 0 ? time : this.m_CountDownTime;
        this._Time.active = true;
        this._Time.$Label.string = this.m_CountDownTime;
        //用定时器做的倒计时
        this.countDownTimerKey = TimerManager.addCountTimeCallBack(1, () => {
            this.m_CountDownTime --;
            this._Time.$Label.string = this.m_CountDownTime;
            if(this.m_CountDownTime <= 0)
            {
                callBack();
                this.isCanClose = true;
                this.node.active = false;
            }
        }, [parseInt(time)]);
    },
    //关闭倒计时
    hideCountDown()
    {
        this.m_CountDownTime = 0;
        this._Time.active = false;
        this._Time.$Label.string = '';
        if(this.countDownTimerKey > 0)
            TimerManager.removeTimeCallBack(this.countDownTimerKey);
    }
});
