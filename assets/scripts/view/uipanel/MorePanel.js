/*
作者（Author）:    jason

描述（Describe）: 更多界面
*/
/*jshint esversion: 6 */
import UIBase from '../UIBase';
import ViewManager from '../../manager/ViewManager';
import { PanelResName } from '../../common/PanelResName';
cc.Class({
    extends: UIBase,

    properties: {
    },
    _onMailBtnTouchEnd(sender, event)
    {
        ViewManager.addPanel(PanelResName.MailPrefab);
    },
    _onCloseBtnTouchEnd(sender, event)
    {
        this.hiddenPanel();
    }
});
