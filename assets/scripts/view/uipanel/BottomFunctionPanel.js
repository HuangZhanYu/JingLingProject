
/*
作者（Author）:    jason

描述（Describe）: 底部功能按钮界面
*/
/*jshint esversion: 6 */
import UIBase from '../UIBase';
import ViewManager from '../../manager/ViewManager';
import { PanelResName } from '../../common/PanelResName';
import FunctionOpenTool, { FuntionEnum } from '../../tool/FunctionOpenTool';

//切换键 类型  对应主界面下面的按钮
export const ToggleType = {
    Task       : 0, //任务
    Soldier    : 1, //阵容
    Treasure   : 2, //装备
    Dungeon    : 3, //地下城
    Bag        : 4, //背包
    More       : 5, //更多     
};

cc.Class({
    extends: UIBase,

    properties: {

    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.RedPoint = [];

        this.RedPoint.push(cc.find('redPoint',this._WorkBtnToggle));
        this.RedPoint.push(cc.find('redPoint',this._ElfBtnToggle));
        this.RedPoint.push(cc.find('redPoint',this._EquipBtnToggle));
        this.RedPoint.push(cc.find('redPoint',this._AdventureBtnToggle));
        this.RedPoint.push(cc.find('redPoint',this._KnapsackBtnToggle));
        this.RedPoint.push(cc.find('redPoint',this._MoreBtn));
        for(let red of this.RedPoint)
        {
            red.active = false;
        }

        this.FunctionLabel = [];
        this.FunctionLabel.push(cc.find('Label',this._WorkBtnToggle));
        this.FunctionLabel.push(cc.find('Label',this._ElfBtnToggle));
        this.FunctionLabel.push(cc.find('Label',this._EquipBtnToggle));
        this.FunctionLabel.push(cc.find('Label',this._AdventureBtnToggle));
        this.FunctionLabel.push(cc.find('Label',this._KnapsackBtnToggle));

        // this.FunctionLabel.forEach(element => {
        //     element.active = false;
        // });
        this.curShow = null;
        this.test();
        this.AfterPassRound();
    },
    SetRedPoint(index,state)
    {
        this.RedPoint[index].active = state;
    },
    //装备按钮点击
    _onEquipBtnToggleTouchEnd(sender, event)
    {
        this.ShowPanel(ToggleType.Treasure);
    },
    //工作按钮被点击
    _onWorkBtnToggleTouchEnd(sender, event)
    {
        this.ShowPanel(ToggleType.Task);
    },
    //精灵按钮被点击
    _onElfBtnToggleTouchEnd(sender, event)
    {
        this.ShowPanel(ToggleType.Soldier);
    },
    //冒险按钮被点击
    _onAdventureBtnToggleTouchEnd(sender, event)
    {
        this.ShowPanel(ToggleType.Dungeon);
    },
    //背包按钮被点击
    _onKnapsackBtnToggleTouchEnd(sender, event)
    {
        this.ShowPanel(ToggleType.Bag);
    },
    //更多按钮被点击
    _onMoreBtnTouchEnd(sender, event)
    {
        this.ShowPanel(ToggleType.More);
    },
    ShowPanel(type)
    {
        if(this.curShow)
        {
            this.curShow.hiddenPanel();
            this.curShow = null;
        }
        ViewManager.showLoading({});
        switch(type)
        {
            //    Soldier    : 1, //阵容
            //     Treasure   : 2, //装备
            //     Dungeon    : 3, //地下城
            //     Bag        : 4, //背包
            case ToggleType.Task:
                ViewManager.addPanel(PanelResName.TaskPopPanel,this.showTogglePanel.bind(this));
            break;
            case ToggleType.Soldier:
            case ToggleType.Treasure:
                ViewManager.hideLoading();
                break;
            case ToggleType.Dungeon:
                ViewManager.addPanel(PanelResName.DungeonPopPanel,this.showTogglePanel.bind(this));
                break;
            case ToggleType.Bag:
                ViewManager.addPanel(PanelResName.BackPackPopPanel,this.showTogglePanel.bind(this));
                break;
            case ToggleType.More:
            ViewManager.addPanel(PanelResName.MorePanel,this.showTogglePanel.bind(this));
                break;
        }
    },
    showTogglePanel(panel)
    {
        this.curShow = panel;
        ViewManager.hideLoading();
    },
    //方便测试
    test()
    {
        this._WorkBtnToggle.parent = this._ToggleContainer;
        this._WorkBtnToggle.$Toggle.isChecked = true;
        this._WorkBtnToggle.$Toggle.interactable = true;
        ViewManager.addPanel(PanelResName.TaskPopPanel,(panel)=>{
            this.curShow = panel;
        });

        this._ElfBtnToggle.parent = this._ToggleContainer;
        this._ElfBtnToggle.$Toggle.isChecked = false;
        this._ElfBtnToggle.$Toggle.interactable = true;

        this._EquipBtnToggle.parent = this._ToggleContainer;
        this._EquipBtnToggle.$Toggle.isChecked = false;
        this._EquipBtnToggle.$Toggle.interactable = true;

        this._KnapsackBtnToggle.parent = this._ToggleContainer;
        this._KnapsackBtnToggle.$Toggle.isChecked = false;
        this._KnapsackBtnToggle.$Toggle.interactable = true;

        this._AdventureBtnToggle.parent = this._ToggleContainer;
        this._AdventureBtnToggle.$Toggle.isChecked = false;
        this._AdventureBtnToggle.$Toggle.interactable = true;
    },
    //通关后设置相关按钮显隐
    AfterPassRound()
    {
        //工作，任务
        if(FunctionOpenTool.IsFunctionOpen(FuntionEnum.MainTaskButton) && this._WorkBtnToggle.parent != this._ToggleContainer)
        {
            this._WorkBtnToggle.parent = this._ToggleContainer;
            this._WorkBtnToggle.$Toggle.isChecked = true;
            this._WorkBtnToggle.$Toggle.interactable = true;
            ViewManager.addPanel(PanelResName.TaskPopPanel,(panel)=>{
                this.curShow = panel;
            });
        }
        //阵容，精灵
        if(FunctionOpenTool.IsFunctionOpen(FuntionEnum.MainLineUpButton) && this._ElfBtnToggle.parent != this._ToggleContainer)
        {
            this._ElfBtnToggle.parent = this._ToggleContainer;
            this._ElfBtnToggle.$Toggle.isChecked = false;
            this._ElfBtnToggle.$Toggle.interactable = true;
        }
        //装备
        if(FunctionOpenTool.IsFunctionOpen(FuntionEnum.MainEquipButton) && this._EquipBtnToggle.parent != this._ToggleContainer)
        {
            this._EquipBtnToggle.parent = this._ToggleContainer;
            this._EquipBtnToggle.$Toggle.isChecked = false;
            this._EquipBtnToggle.$Toggle.interactable = true;
        }
        //背包
        if(FunctionOpenTool.IsFunctionOpen(FuntionEnum.MainBagButton) && this._KnapsackBtnToggle.parent != this._ToggleContainer)
        {
            this._KnapsackBtnToggle.parent = this._ToggleContainer;
            this._KnapsackBtnToggle.$Toggle.isChecked = false;
            this._KnapsackBtnToggle.$Toggle.interactable = true;
        }
        //冒险
        if(FunctionOpenTool.IsFunctionOpen(FuntionEnum.MainDungeonButton) && this._AdventureBtnToggle.parent != this._ToggleContainer)
        {
            this._AdventureBtnToggle.parent = this._ToggleContainer;
            this._AdventureBtnToggle.$Toggle.isChecked = false;
            this._AdventureBtnToggle.$Toggle.interactable = true;
        }
    },
    //升级后设置相关按钮
    AfterLevelUp()
    {
        //阵容，精灵
        if(FunctionOpenTool.IsFunctionOpen(FuntionEnum.MainLineUpButton) && this._ElfBtnToggle.parent != this._ToggleContainer)
        {
            this._ElfBtnToggle.parent = this._ToggleContainer;
            this._ElfBtnToggle.$Toggle.isChecked = false;
            this._ElfBtnToggle.$Toggle.interactable = true;
        }
    }

});
