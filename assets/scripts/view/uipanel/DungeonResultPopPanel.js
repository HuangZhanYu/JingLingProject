/*
作者（Author）:    skyHuang

描述（Describe）:
*/
/*jshint esversion: 6 */

import CanShuTool from '../../tool/CanShuTool';
import { PlayerIntAttr } from '../../part/PlayerBasePart';
import StringTool from '../../tool/StringTool';
import { PanelResName } from '../../common/PanelResName';
import UIBase from '../UIBase';
import ViewManager, { PanelPos } from '../../manager/ViewManager';

let DungeonResultPopData = { };

DungeonResultPopData.EnterType = {
    GuildFightFirst    : 1, ////公会战首次占领
    GuildFightTake     : 2, ////公会战掠夺
    GuildFightPlayBack : 3, ////公会战战斗回放
}
//显示面板类型
DungeonResultPopData.DungeonShowType = {
    Normal : 1,//正常打开界面
    FightTower : 2,//战斗塔打开
    AfterFriend : 3,//好友切磋打开
    Plateau : 4,//精灵高原打开
    Common : 5,//公用打开接口
}
//显示结束界面
//type:显示面板类型
//isWin:是否是胜利
//typeData：界面类型数据
DungeonResultPopData.ShowPanel = function(type,isWin,typeData = null){
    let data = {
        type : type,
        isWin : isWin,
        typeData : typeData
    }
    ViewManager.addPanel(PanelResName.DungeonResultPopPanel, null, PanelPos.PP_Midle,data);
}

window.DungeonResultPopData = DungeonResultPopData;

cc.Class({
    extends: UIBase,

    properties: {

    },
    onLoad() {
        this.m_IsWin = null;
        this.m_IsFirstPass = null;
        this.m_plateauID = null
        this.m_PlateauStar = null;
        this.m_EnterType = null;
        this.m_Rewards = null;
        this.m_OpenType = null;
    },
    showPanel(data){
        this._super(data);
        this.showTypePanel(data);
    },
    showTypePanel(data){
        this.m_IsWin = data.isWin;
        this.m_OpenType = data.type;
        let typeData = data.typeData;//根据显示的类型获取数据
        this.showCommonLose();
        switch (data.type) {
            case DungeonResultPopData.DungeonShowType.Normal:
                this.afterLoad();
            break;
            case DungeonResultPopData.DungeonShowType.FightTower://战斗塔打开
                this.m_IsFirstPass = typeData.isFirstPass;
                this.afterTowerLoad();
            break;
            case DungeonResultPopData.DungeonShowType.AfterFriend://好友切磋打开
                this.afterFriendLoad();
            break;
            case DungeonResultPopData.DungeonShowType.Plateau://精灵高原打开
                this.m_plateauID = typeData.confid;
                this.m_PlateauStar = typeData.star;
                this.afterPlateauLoad();
            break;
            case DungeonResultPopData.DungeonShowType.Common://公用打开
                this.m_EnterType = typeData.enterType //进入枚举
                this.m_Rewards = {};
                this.m_Rewards = typeData.rewards;//奖励数组
                this.afterCommonLoad();
            break;
        
            default:
                break;
        }
    },
    //公用加载完毕回调
    afterCommonLoad(){
        if(this.m_IsWin){
            //播放胜利音效
            //DungeonResultPopPanel.PlaySound(true)
            this.showCommonWin()
        }
    },

    //公共胜利
    showCommonWin(){ 
        if(this.m_EnterType == DungeonResultPopData.EnterType.GuildFightFirst){
            this._WinDes1.$Label.string = '';
            this._WinDungeonName.$Label.string = '挑战成功';
            this._WinDungeonDiff.$Label.string = '';
        }else if(this.m_EnterType == DungeonResultPopData.EnterType.GuildFightTake){
            this._WinDes1.$Label.string = '';
            this._WinDungeonName.$Label.string = '掠夺成功';
            this._WinDungeonDiff.$Label.string = '';
        }else{
            this._WinDes1.$Label.string = '';
            this._WinDungeonName.$Label.string = '';
            this._WinDungeonDiff.$Label.string = '';
        }

        this._AwardGrid.$AwardGrid.updateItemUI(this.m_Rewards);
    },

    //公共失败
    showCommonLose(){
        this._Win.active = this.m_IsWin;
        this._Lose.active = !this.m_IsWin;
        this._AwardGrid.$AwardGrid.hideItemAll();
    },

    //加载完毕回调
    afterLoad(){
        DungeonResultPopPanel.ShowGuide();
        if(this.m_IsWin){
            //播放胜利音效
            //DungeonResultPopPanel.PlaySound(true);
            this.showWin();
        }else{
            //播放失败音效
            this.showLose();
        }
    },

    //战斗塔加载完毕回调
    afterTowerLoad(){
        if(this.m_IsWin){
            //播放胜利音效
            //DungeonResultPopPanel.PlaySound(true)
            this.showTowerWin();
        }
    },

    //好友切磋
    afterFriendLoad(){
        if(this.m_IsWin){
            //播放胜利音效
            //DungeonResultPopPanel.PlaySound(true)
            this.showFriendWin()
        }
    },

    //精灵高原
    afterPlateauLoad(){
        if(this.m_IsWin){
            //播放胜利音效
            //DungeonResultPopPanel.PlaySound(true)
            this.showPlateauWin();
        }
    },

    //刷新战斗塔胜利
    showTowerWin(){
        this._WinDungeonName.$Label.string = '战斗塔';
        this._WinDes1.$Label.string = '恭喜您通过了';
        
        let round = FightTowerPanel.GetCurrentRound();//获取当前章节的关卡数

        this._WinDungeonDiff.$Label.string = `第【${round}】层`;

        let conf_JiangLi = LoadConfig.getConfigData(ConfigName.ZhanDouTa_JiangLi,round);
        if(conf_JiangLi == null){
            return;
        }

        //如果第一次通关则
        if(this.m_IsFirstPass){
            let awardData = [];
            for(let i = 0; i < 6; i++){
                if(conf_JiangLi.ResourceType[i] == 0){
                    break;
                }
                let data = {ItemNum : `x`+conf_JiangLi.ResourceCount[i],ItemType : conf_JiangLi.ResourceType[i],ItemID : conf_JiangLi.ResourceID[i]};
                awardData.push(data);    
            }
            this._AwardGrid.$AwardGrid.updateItemUI(awardData);
        }
    },

    //刷新好友胜利
    showFriendWin(){
        this._WinDes1.$Label.string = '';
        this._WinDungeonName.$Label.string = '好友切磋';
        this._WinDungeonDiff.$Label.string = '';
    },

    //刷新地下城胜利
    showWin(){
        this._WinDes1.$Label.string = '恭喜您通过了';
        let dungeonID = DungeonDiffPopPanel.GetCurrentDungeonID();
        let roundID = DungeonDiffPopPanel.GetCurrentDungeonRound();
        let conf_ID = dungeonID * 1000 + roundID;

        let conf_GuanKa = LoadConfig.getConfigData(ConfigName.DiXiaCheng_GuanKa,conf_ID);
        if(conf_GuanKa == null){
            console.error("地下城关卡ID错误:"+conf_ID);
            return;
        }

        let conf_Dungeon = LoadConfig.getConfigData(ConfigName.DiXiaCheng,dungeonID);
        if(conf_Dungeon == null){
            console.error("地下城ID错误:"+dungeonID);
            return;
        }

        this._WinDungeonName.$Label.string = conf_Dungeon.Name;
        let str = '第【x】层';
        str = StringTool.ZhuanHuan(str,[roundI]);
        this._WinDungeonDiff.$Label.string = str;

        // let level = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_DengJi];
        // let exp = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_JingYan];
        
        // let expMax =  LoadConfig.getConfigData(ConfigName.ZhuJue_Dengji,level).NeedExp;

        // mLuaBehaviour:SetText(this.PlayerLvWin,"Lv."+level)

        // this.EXPsliderWin.value = exp / expMax

        //设置胜利奖励

        let countReward = DungeonPart.WinRewardList.length;

        if(countReward > 3){
            countReward = 3;
        }
        let awardData = [];
        for(let i = 1; i < countReward; i++){
            let rewardCount = BigNumber.GetValue(DungeonPart.WinRewardList[i].RewardCount)
            let data = {ItemNum : `x`+rewardCount,ItemType : DungeonPart.WinRewardList[i].RewardType,ItemID : DungeonPart.WinRewardList[i].RewardID};
            awardData.push(data);     
        }
        this._AwardGrid.$AwardGrid.updateItemUI(awardData);
    },

    //刷新失败
    showLose(){
        //根据后端的数据 刷新界面
        let dungeonID = DungeonDiffPopPanel.GetCurrentDungeonID();
        let roundID = DungeonDiffPopPanel.GetCurrentDungeonRound();
        let conf_ID = dungeonID * 1000 + roundID;

        let conf_GuanKa = LoadConfig.getConfigData(ConfigName.DiXiaCheng_GuanKa,conf_ID);
        if(conf_GuanKa == null){
            console.error("地下城关卡ID错误:"+conf_ID);
            return;
        }

        let conf_Dungeon = LoadConfig.getConfigData(ConfigName.DiXiaCheng_GuanKa,dungeonID);
        if(conf_Dungeon == null){
            console.error("地下城ID错误:"+dungeonID);
            return;
        }

        // let level = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_DengJi]
        // let exp = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_JingYan]

        // let expMax =  Conf_ZhuJue_Dengji[level].NeedExp

        // mLuaBehaviour:SetText(this.PlayerLvLose,"Lv."..level)

        // this.EXPsliderLose.value = exp / expMax

        //奖池为空
        if(conf_GuanKa.ShiBaiJiangChi == 0){
            return
        }

        //设置失败奖励
        let jiangChiInfo =LoadConfig.getConfigData(ConfigName.JiangChi,conf_GuanKa.ShiBaiJiangChi);
        if(jiangChiInfo == null){
            console.error("奖池ID错误,ID"+conf_GuanKa.ShiBaiJiangChi);
            return;
        }

        // for(let i = 0; i < 3; i++){
        //     if(jiangChiInfo.ResourceTypes[i] != 0){
        //         this.LoseDaojus[i].active = true;
        //         DaoJuTool.SetDaoJuImage(this.LoseDaojus[i],mLuaBehaviour,jiangChiInfo.ResourceTypes[i],jiangChiInfo.ResourceIDs[i])
        //         DaoJuTool.SetDaoJuText(this.LoseDaojus[i],mLuaBehaviour,"","x"..jiangChiInfo.MinCounts[i])
        //     }
        // }
    },

    //高原胜利
    showPlateauWin(){
        this._Stars.active = true;

        let conf_GaoYuan = LoadConfig.getConfigData(ConfigName.JingLingGaoYuan,this.m_plateauID);
        if(conf_GaoYuan == null){
            console.error("高原ID异常"+this.m_plateauID);
            return;
        }
        this._WinDungeonName.$Label.string = '';
        this._WinDungeonDiff.$Label.string = '';
        this._WinDes2.active = false;
        for(let i = 1; i <= 3; i++){
            this._Stars.children[i-1].children[0].active = this.m_PlateauStar >= i;
        }
        if(this.m_PlateauStar == 0){
            //这里没通关不会执行
            this._WinDes1.$Label.string = '';
        }else if(this.m_PlateauStar == 1){
            this._WinDes1.$Label.string = '挑战两次以上通关成功获得';
        }else if(this.m_PlateauStar == 2){
            this._WinDes1.$Label.string = '挑战两次通关成功获得';
        }else if(this.m_PlateauStar == 3){
            this._WinDes1.$Label.string = '挑战一次通关成功获得';
        }else{
            console.error("当前高原获得星星数量有误："+this.m_PlateauStar);
        }

        let conf_JiangLi = LoadConfig.getConfigData(ConfigName.JiangChi,conf_GaoYuan.RewardID);
        if(conf_JiangLi == null){
            console.error("奖池异常"+conf_GaoYuan.RewardID);
            return;
        }

        let awardData = [];
        for(let i = 1; i < countReward; i++){
            if(conf_JiangLi.ResourceTypes[i] == 0){
                break;
            }
            let data = {ItemNum : `x`+conf_JiangLi.MinCounts[i],ItemType : conf_JiangLi.ResourceTypes[i],ItemID : conf_JiangLi.ResourceIDs[i]};
            awardData.push(data);     
        }
        this._AwardGrid.$AwardGrid.updateItemUI(awardData);
    },

    //刷新界面end =============================================================


    //点击事件集合start >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
    //点击屏幕继续
    //点击登陆按钮
    _onMaskTouchEnd(sender, event)
    {
        switch (this.m_OpenType) {
            case DungeonResultPopData.DungeonShowType.Normal:
                this.onBtnContinueClick();
            break;
            case DungeonResultPopData.DungeonShowType.FightTower://战斗塔打开
                this.onBtnTowerContinueClick();
            break;
            case DungeonResultPopData.DungeonShowType.AfterFriend://好友切磋打开
                this.onBtnFriendContinueClick();
            break;
            case DungeonResultPopData.DungeonShowType.Plateau://精灵高原打开
                this.onBtnPlateauContinueClick();
            break;
            case DungeonResultPopData.DungeonShowType.Common://公用打开
                this.onCommonContinueClick();
            break;
        
            default:
                break;
        }
    },
    onBtnContinueClick(){
        //GuideManager.Pass(13,1306)

        this.hiddenPanel();

        //关闭竞技场战斗UI
        //GuideManager.IsSpecialPanel = true
        //UIManager.Close();

        ViewManager.addPanel(PanelResName.DungeonPopPanel);

        let dungeonID = DungeonDiffPopPanel.GetCurrentDungeonID();
        DungeonDiffPopPanel.Show(dungeonID);
        //开始主线战斗
        GuanQiaPart.Resume();

    },

    //战斗塔的继续
    onBtnTowerContinueClick(){
        //GuideManager.Pass(13,1306)
        //关闭自己
        this.hiddenPanel()
        //设置是从战斗塔返回
        //FightTowerPanel.SetTag()
        //关闭公共战斗界面
        //UIManager.Close();

        GuanQiaPart.Resume();
    },

    //好友的继续
    onBtnFriendContinueClick(){
        //GuideManager.Pass(13,1306)
        this.hiddenPanel();
        //UIManager.Close();
        GuanQiaPart.Resume();
    },

    //精灵高原的继续
    onBtnPlateauContinueClick(){
        //关闭自己
        this.hiddenPanel()
        //关闭公共战斗界面
        //UIManager.Close();

        GuanQiaPart.Resume();
    },

    //公用的继续
    onCommonContinueClick(){
        this.hiddenPanel();
        //UIManager.Close();
        GuanQiaPart.Resume();
    },
    hiddenPanel(){
        this._super();
        this._AwardGrid.$AwardGrid.hideItemAll();
        this._Stars.active = false;
    },
});
