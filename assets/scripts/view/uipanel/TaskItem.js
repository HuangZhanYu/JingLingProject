import BigNumber from '../../tool/BigNumber';
import { GetPlayerCanBuyBool } from '../../common/PublicFunction';
import { PlayerStrAttr, PlayerIntAttr } from '../../part/PlayerBasePart';
import StringTool from '../../tool/StringTool';
import { TimeTool_TimeTransformUpdate } from '../../tool/TimeTool';

let Thor = require('Thor');
const i18n = require('LanguageData');
cc.Class({
    extends: Thor,

    properties: {
        IconAtlas:cc.SpriteAtlas
    },
    init(taskInfo,index) {
        this.taskInfo = taskInfo;
        let conf_task = LoadConfig.getConfigData(ConfigName.JinBiRenWu,index+1);
        if(taskInfo.TaskLevel == 0)
        {
            this._NotOpen.active = true;
            let showText = i18n.t('TaskPopPanelText.LevelNotOpen');
            this._NotOpenLevel.$Label.string = StringTool.ZhuanHuan(showText,[conf_task.PlayerLevel]);
            //设置任务图标
            this._NotOpenTaskIcon.$Sprite.spriteFrame = this.IconAtlas.getSpriteFrame(conf_task.Icon);
            //设置任务名
            this._NotOpenName.$Label.string = conf_task.TaskDesc;
            //设置默认任务时间
            this._NotOpenTime.$Label.string = TimeTool_TimeTransformUpdate(conf_task.TaskTime);
            //设置任务收益
            this._NotOpenGetGold.$Label.string = BigNumber.getValue(BigNumber.create(conf_task.InitReward));

            //如果等级不够 显示等级开放
            if (PlayerBasePart.GetIntValue(PlayerIntAttr.PlayerIntAttr_DengJi) < conf_task.PlayerLevel)
            {
                this._NotOpenButtonUp.active = false;
            }
            else
            {
                this._NotOpenButtonUp.active = false;
            }
            return;
        }
        else if(taskInfo.TaskLevel >= conf_task.MaxLevel)
        {
            this._NotOpen.active = false;
            this._ButtonUp.active  = false;
            this._MaxLabel.active = true;

            let taskId = taskInfo.TaskId;
            let taskCon = LoadConfig.getConfigData(ConfigName.JinBiRenWu,taskId);
            //刷新等级
            let level = BigNumber.create(taskInfo.TaskLevel);
            this._TaskLv.$Label.string = "Lv."+BigNumber.getValue(level);
            this._GetGold.$Label.string = taskInfo.OnceRewardReal;
            this._Name.$Label.string = taskCon.TaskDesc;
            return;
        }
        this._NotOpen.active = false;

        let taskId = taskInfo.TaskId;
        let taskCon = LoadConfig.getConfigData(ConfigName.JinBiRenWu,taskId);
         //刷新等级
         let level = BigNumber.create(taskInfo.TaskLevel);
         this._TaskLv.$Label.string = "Lv."+BigNumber.getValue(level);
         this._GetGold.$Label.string = taskInfo.OnceRewardReal;
         this._Name.$Label.string = taskCon.TaskDesc;

         this._TaskIcon.$Sprite.spriteFrame = this.IconAtlas.getSpriteFrame(taskCon.Icon);

         //升一级增加 文字
        this._AddGold.$Label.string = taskInfo.UpLevelAddRewardReal;  
    
        //设置灰显文字 以及  不灰显文字
        this._Cost.$Label.string = taskInfo.UpLevelCostReal;  //升一级消耗 文字


        //判断1次10次和X次是否足够  不够灰显
        let [isOnceEnough,_] = GetPlayerCanBuyBool(1,taskInfo.TaskId,taskInfo.TaskLevel);
        this._ButtonUp.$Button.interactable = isOnceEnough;

        //如果任务满级  
        if(taskInfo.TaskLevel == taskCon.MaxLevel)
        {
            this._ButtonUp.active = false;
        }
    },
    //升级按钮点击
    _onButtonUpTouchEnd(sender, event){
        this.LevelUpRequest(1,this.taskInfo.TaskId,this.taskInfo.TaskLevel);
    },
    LevelUpRequest(count,taskId,taskLv)
    {
        //判断金币是否足够
        if(taskLv == 0)
        {
            let bigNumCost = TaskPart.GetTaskInfo(taskId).OpenTaskCost;
            let bigNumPlayer = BigNumber.create(PlayerBasePart.StrAttr[PlayerStrAttr.PlayerStrAttr_BigNumberGold]);
            let tempBool = BigNumber.greaterThanOrEqualTo(bigNumPlayer,bigNumCost);
            if (tempBool)
            {
                TaskPart.UpgradeTaskLvRequest(count,taskId);
            }
        }
        else if(taskLv > 0)
        {
            //判断当前金币是否足够升级
            let [tempBool,_] = GetPlayerCanBuyBool(count,taskId,taskLv);
            if (tempBool)
            {
                TaskPart.UpgradeTaskLvRequest(count,taskId);
            }
        }
    }
});
