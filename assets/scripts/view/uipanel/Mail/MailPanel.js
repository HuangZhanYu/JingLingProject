/*
作者（Author）:    skyHuang

描述（Describe）:
*/
/*jshint esversion: 6 */
import UIBase from '../../UIBase';
import TimerManager from '../../../tool/TimerManager';

//领取按钮状态枚举
let MailGetBtnState = {
	Hide          : 0,//隐藏
	Show          : 1,//显示
	Disabled      : 2,//置灰
}

cc.Class({
    extends: UIBase,

    properties: {
        MailBarPrefab : cc.Prefab,
    },
    onLoad() 
    {
        this.mMailBarList = [];       //邮件条预设列表
        this.mDaoJuList = [];         //奖励预设列表
        this.mCurSelectMailIndex = 0; //当前选择邮件索引
    },
    showPanel(param)
    {
        this._super();
        this.updateItemUI(MailPart.mTotalRewardData);
    },
    updateItemUI(data)
    {
        if(data.length < 1)
        {
            this.hideItemAll();
            //this._Content.removeAllChildren();
            return;
        }
        if(data.length < this._Content.childrenCount)
        {
            for(var i = 0; i < this._Content.childrenCount; i ++)
            {
                let item = this._Content.children[i];
                if(i < data.length)
                {
                    item.active = true;
                    let mailItem = item.getComponent('MailItem');
                    mailItem.init();
                    mailItem.updateUI(i);
                }else
                {
                    item.active = false;
                }  
            }
        }else if(data.length > this._Content.childrenCount)
        {
            for(var i = 0; i < data.length; i ++)
            {
                let item = null;
                if(i < this._Content.childrenCount)
                {
                    item = this._Content.children[i];
                    item.active = true;
                }else
                {
                    item = cc.instantiate(this.MailBarPrefab);
                    item.parent = this._Content;
                }
                let mailItem = item.getComponent('MailItem');
                mailItem.init();
                mailItem.updateUI(i);  
            }
        }else
        {
            for(var i = 0; i < this._Content.childrenCount; i ++)
            {
                let item = this._Content.children[i];
                item.active = true;
                let mailItem = item.getComponent('MailItem');
                mailItem.init();
                mailItem.updateUI(i); 
            }
        }
    },
    hideItemAll()
    {
        for(var i = 0; i < this._Content.childrenCount; i ++)
        {
            let item = this._Content.children[i];
            item.active = false;
        }
    },
    //取消按钮的点击事件
    _onCloseBtnTouchEnd(sender, event)
    {
        this._Content.removeAllChildren();
        this.closePanel();
    },
});
