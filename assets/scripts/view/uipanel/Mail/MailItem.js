/*
作者（Author）:    skyHuang

描述（Describe）:
*/
/*jshint esversion: 6 */
import DaoJuTool from '../../../tool/DaoJuTool';
import BigNumber from '../../../tool/BigNumber';
import UIBase from '../../UIBase';

//邮件状态枚举
let MailState = {
	NotRead       : 0,//未读
	NotGet        : 1,//已读未领取
	Got           : 2,//已读已领取
}

cc.Class({
    extends: UIBase,

    properties: {
        NotReadSprite : cc.SpriteFrame,
        GotSprite : cc.SpriteFrame,
    },
    init(){
        this.m_MailId = null;
        this.m_Award = null;
    },
    updateUI(id){
        this.m_MailId = id;
        let date = MailPart.mTotalRewardData[id];
        this.setMailState(MailPart.mTotalRewardData[id].State);
		//设置邮件标题和天数
		this._AwardName.$Label.string = MailPart.mTotalRewardData[i].Content[1];
		//设置删除天数
		if(MailPart.mTotalRewardData[i].DeleteTime <= 0){
			this._TimeText.$Label.string = '当天删除';
        }else{
			let deleteText = MailPart.mTotalRewardData[i].DeleteTime + '天后删除';
			this._TimeText.$Label.string = deleteText;
        }
        this.showAward();
    },
    setMailState(state){
        //根据状态显示背景
        if(state == MailState.NotRead){
            //未读
            this._ReadState.active = true;
            this._ReadState.$Sprite.SpriteFrame = NotReadSprite;
        }else if(state == MailState.NotGet){
            //已读未领取
            this._ReadState.active = false;
        }else{
            //以领取
            this._ReadState.active = true;
            this._ReadState.$Sprite.SpriteFrame = GotSprite;
        }
    },
    //--点击邮件条(传入邮件的索引)
    _onBgTouchEnd(sender, event){

        //--请求服务器阅读邮件,传入当前邮件索引-1(服务器下标从0开始)
        if(MailPart.mTotalRewardData[this.m_MailId].State == MailState.NotRead){
            //--转换索引
            let index = this.changeIndexForServer();
            MailPart.ReadRequest(index-1);
        }else{
            //--客户端调用查看邮件
            //this.ReadMailCallBack();
        }
    },
    changeIndexForServer(){
        return MailPart.mTotalRewardData[this.m_MailId].Index+1;
    },
    //显示第一个礼物
    showAward(){
        if(MailPart.mTotalRewardData[this.m_MailId].Rewards.length > 0){
            if(this.m_Award == null){
                this.m_Award = DaoJuTool.LoadDaoJu(this._AwardParent);
            }
            //设置物品信息
            this.m_Award.active = true;
			let rewardType = MailPart.mTotalRewardData[this.m_MailId].Rewards[0].Type;
			let rewardID = MailPart.mTotalRewardData[this.m_MailId].Rewards[0].ID;
			let rewardCount = MailPart.mTotalRewardData[this.m_MailId].Rewards[0].Count;
            DaoJuTool.SetDaoJuItemText(this.m_Award,'x'+BigNumber.getValue(rewardCount));
            DaoJuTool.SetDaoJuImage(this.m_Award,rewardType,rewardID);
        }else{
            this.m_Award.active = false;
        }
    },
});
