/*
作者（Author）:    jason

描述（Describe）: 登陆界面
*/

import UIBase from '../UIBase';
import SocketClient from '../../network/SocketClient';
import LocalStorage from '../../tool/LocalStorage';
import MessageCenter from '../../tool/MessageCenter';
import { MessageID } from '../../common/MessageID';
import { LoginError } from '../../part/LoginPart';
import ViewManager, { PanelPos } from '../../manager/ViewManager';
import { PanelResName } from '../../common/PanelResName';

const i18n = require('LanguageData');

cc.Class({
    extends: UIBase,

    onLoad () {
        console.log('LoginPanel ------------- onLoad ');

        let strAccount = LocalStorage.getString('LoginAccount');
        let strPassword = LocalStorage.getString('LoginPassword');
        if(strAccount)
        {
            this._EditboxAccount.$EditBox.string = strAccount;
        }
        else
        {
            this._EditboxAccount.$EditBox.string = '';
        }
        if(strPassword)
        {
            this._EditboxPassword.$EditBox.string = strPassword;
        }
        else
        {
            this._EditboxPassword.$EditBox.string = '';
        }
        this._InputAccount.active = false;
        this._StartBtn.active = false;
        this._ProgressBar.active = true;
        MessageCenter.addListen(this,this.onLoadConfigProcess,MessageID.MsgID_LoadConfigProcess);
        MessageCenter.addListen(this,this.onLoadConfigFinished,MessageID.MsgID_LoadConfigFinished);
        MessageCenter.addListen(this,this.onLoginError,MessageID.MsgID_LoginError);
    },
 
    onLoadConfigProcess(process)
    {
        this._ProgressBar.$ProgressBar.progress = process;
    },
    onLoadConfigFinished()
    {
        this._InputAccount.active = true;
        this._ProgressBar.active = false;
    },
    onLoginError(errorNo)
    {
        ViewManager.hideLoading(true);
        let alertInfo = {
            mainText: "",
            singleText: i18n.t('AlertText.Button_Common_Yes'),
            singleCallBack: function () {
                
            },
        };
        switch(errorNo)
        {
            case LoginError.Duplicate:
                alertInfo.mainText = '账号登陆中！';
            break;
            case LoginError.Password:
                alertInfo.mainText = '该账号被封了，如需申述请进群联系客服！';
            break;
            case LoginError.TryAgain:
                alertInfo.mainText = '账号已在其他设备登陆，继续登陆请再次点击';
            break;
            default:
                alertInfo.mainText = '登陆失败，继续登陆请再次点击';
            break;
        }
        ViewManager.alert(alertInfo);
    },
    _onStartBtnTouchEnd(sender, event)
    {
        console.log(sender.name);
        //SocketClient.connectServer();
    },
    //点击登陆按钮
    _onLoginButtonTouchEnd(sender, event)
    {
        cc.log(this._EditboxAccount.$EditBox.string);
        cc.log(this._EditboxPassword.$EditBox.string);
        let strAccount = this._EditboxAccount.$EditBox.string;
        let strPassword = this._EditboxPassword.$EditBox.string;

        if(strAccount == '' || strPassword == '')
        {
            console.log('账号密码不能等于空');
            return;
        }
        LocalStorage.setString('LoginAccount',strAccount);
        LocalStorage.setString('LoginPassword',strPassword);

        ViewManager.showLoading({text:i18n.t('LoginPanel_text.Landing'),showDelay:0});
        LoginPart.Login(strAccount,strPassword);
    },
    _onForgetPasswordTouchEnd(sender, event)
    {
        cc.log('_onForgetPasswordTouchEnd');
    },
    onDestroy() {
        ViewManager.hideLoading(true);
        MessageCenter.delListen(this);
    }
});
