/*
作者（Author）:    skyHuang

描述（Describe）:
*/
/*jshint esversion: 6 */

let Thor = require('Thor');

let BehaviorFlyItemManager = cc.Class({
    extends: Thor,

    properties: {
    },
    
    onLoad()
    {
        this.m_TatalFlyItem = [];
        this.mInstance = this;
    },
    onDestroy()
    {
        this.mInstance = null;
    },
    release()
    {
        //清理所有
        for(let i =0;i< m_TatalFlyItem.length; i++)
        {
            let bfi = this.m_TatalFlyItem[i];
            if(bfi == null)
            {
                continue;
            }

            let ad = bfi.getComponent('AutoDestroy');
            if(ad != null)
            {
                ad.release();
            }
            else
            {
                this.node.destroy();
            }
            
        }
    },
    // private static BehaviorFlyItemManager mInstance;
    // public static BehaviorFlyItemManager GetInstance()
    // {
    //     return mInstance;
    // }

    /// <summary>
    /// 创建一个直线飞行道具
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="id"></param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="z"></param>
    /// <param name="target"></param>
    /// <param name="duration"></param>
    createLineFlyItem( obj,  name,  id,  x,  y,  z,  target,  duration)
    {
        let item = obj.getComponent('LineFlyItem');
        if (item == null)
        {
            item = obj.AddComponent('LineFlyItem');
        }
        item.enabled = true;
        let item2 = obj.getComponent('ChainFlyItem');
        if (item2 != null)
        {
            item2.enabled = false;
        }
        let item3 = obj.getComponent('ParabolaFlyItem');
        if (item3 != null)
        {
            item3.enabled = false;
        }
        obj.position = cc.v2(x, y);
        this.setChildOrientation(obj);
        obj.active = true;
        item.create(id, target, duration, name);
        this.m_TatalFlyItem.push(item);
    },
    /// <summary>
    /// 创建一个闪电链飞行道具
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="id"></param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="z"></param>
    /// <param name="target"></param>
    /// <param name="duration"></param>
    createChainFlyItem( obj, name,  id,  x,  y,  z,  target,  duration)
    {
        let item = obj.getComponent('ChainFlyItem');
        if (item == null)
        {
            item = obj.AddComponent('ChainFlyItem');
        }
        item.enabled = true;
        let item2 = obj.getComponent('ParabolaFlyItem');
        if (item2 != null)
        {
            item2.enabled = false;
        }
        let item3 = obj.getComponent('LineFlyItem');
        if (item3 != null)
        {
            item3.enabled = false;
        }
        obj.position = cc.v2(x, y);
        this.setChildOrientation(obj);
        obj.active = true;
        item.create(id, target, duration, name);
        this.m_TatalFlyItem.push(item);
    },
    /// <summary>
    /// 创建一个抛物线飞行道具
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="id"></param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="z"></param>
    /// <param name="target"></param>
    /// <param name="duration"></param>
    createParabolaFlyItem( obj, name,  id,  x,  y,  z,  target,  duration)
    {
        let item = obj.getComponent('ParabolaFlyItem');
        if (item == null)
        {
            item = obj.AddComponent('ParabolaFlyItem');
        }
        item.enabled = true;
        let item2 = obj.getComponent('ChainFlyItem');
        if (item2 != null)
        {
            item2.enabled = false;
        }
        let item3 = obj.getComponent('LineFlyItem');
        if (item3 != null)
        {
            item3.enabled = false;
        }
        obj.position = cc.v2(x, y);
        this.setChildOrientation(obj);
        obj.active = true;
        item.create(id, target, duration, name);
        this.m_TatalFlyItem.push(item);
    },
    /// <summary>
    /// 创建一个穿透飞行道具
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="id"></param>
    /// <param name="master"></param>
    /// <param name="speed"></param>
    /// <param name="dist"></param>
    createCrossFlyItem( obj,  id,  x,  y,  z,  speed,  dist)
    {
        let item = obj.getComponent('CrossFlyItem');
        if (item == null)
        {
            item = obj.AddComponent('CrossFlyItem');
        }
        obj.position = cc.v2(x, y);
        this.setChildOrientation(obj);
        obj.active = true;
        item.create(id, speed, dist);
        this.m_TatalFlyItem.push(item);
    },
    createTrailFlyItem( obj,   target,  name,  id,  x,  y,  z,  duration,  hit,  time)
    {
        let item = obj.getComponent('TrailFlyItem');
        if (item == null)
        {
            item = obj.AddComponent('TrailFlyItem');
        }
        obj.position = cc.v2(x, y);
        this.setChildOrientation(obj);
        item.create(id, target, duration, hit,time , name);
        this.m_TatalFlyItem.push(item);
    },
    setChildOrientation(obj)
    {
        if (obj.childrenCount <= 0)
        {
            return;
        }
        let child = obj.children[0];
        if (child == null)
        {
            return;
        }
        // Vector3 lea = child.localEulerAngles;
        // lea.y = 180;
        // child.localEulerAngles = lea;
    }
});