/*
作者（Author）:    jason

描述（Describe）: 角色控制脚本
*/
/*jshint esversion: 6 */
import FightGameObjectManager from "../FightGameObjectManager";
import { FightDef } from "../../logic/fight/FightDef";

let SceneWidth = FightDef.BaseSceneLength/2;
cc.Class({
    extends: cc.Component,

    properties: {
       
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        //是否销毁，给异步加载回调对此RoleBehaviour是否还存在做判断
        this.mIsDestory = false;

        //移动操作标记
        this.mIsMoving = false;
        //移动起始位置
        this.mMovingStart = null;
        //移动目标位置
        this.mMovingTarget = null;
        //移动速度
        this.mMovingSpeed = 0;
        //击飞击退操作标记
        this.mIsRepeling = false;
        //移动起始位置
        this.mRepelStart = null;
        //移动半程位置
        this.mRepelMid = null;
        //移动终点位置
        this.mRepelTarget = null;
        //移动速度
        this.mRepelSpeed = 0;
        //时间
        this.mRepelDuration = 0;
        //计时时间
        this.mRepelStartTime = 0;

        //闪现
        this.mIsTeleport = false;
        //闪现开始时间
        this.mTeleportStartTime = 0;
        //闪现延迟时间
        this.mTeleportDelayTime = 0;
        //闪现位置
        this.mTeleportPosition = 0;
        //闪现光效名称
        this.mTeleportEffectName = '';
        //闪现光效时间
        this.mTeleportEffectDuration = 0;

        //旋转操作标记
        this.mIsRotating = false;
        //旋转起始位置
        this.mRotatingStart = null;
        //旋转目标位置
        this.mRotatingTarget = null;
        //旋转速度
        this.mRotatingSpeed = 0;

        //死亡操作标记
        this.mIsDead = false;
        //死亡开始时间
        this.mDeadStartTime = 0;
        //死亡延迟时间
        this.mDeadDelayTime = 0;
        //死亡角色类型
        this.mIsBossDead = false;
        //当前游戏速度
        this.mGameSpeed = 1;
    },
    update(deltatime)
    {
        //移动
        this.MoveToUpdate(deltatime);
        //旋转
        this.RotateToUpdate(deltatime);
        //击飞击退
        this.RepelUpdate(deltatime);
        //闪现
        this.TeleportUpdate(deltatime);
        //死亡表现
        this.DieUpdate(deltatime);
    },
    /// <summary>
    /// 移动到某个坐标点
    /// </summary>
    /// <param name="targetPos">目标位置</param>
    /// <param name="speed">速度</param>
    MoveTo(x,y, speed) {
        this.mMovingTarget = new cc.Vec2(x, y);
        this.mMovingSpeed = speed;
        this.mMovingStart = this.node.position;
       
        var dir = this.mMovingTarget.sub(this.mMovingStart); 
        var len = dir.mag(); // cc.pLength;
        if (len <= 0) {
            return;
        }
        this.walk_time = len / speed;
        this.now_time = 0;
        this.vx = speed * (dir.x / len);
        this.vy = speed * (dir.y / len);

        this.mIsMoving = true;
    },

    StopMove()
    {
        this.mIsMoving = false;
    },

    /// <summary>
    /// 移动是否结束
    /// </summary>
    /// <returns></returns>
    IsMoveEnd()
    {
        return this.mIsMoving;
    },

    /// <summary>
    /// 移动Update函数
    /// </summary>
    MoveToUpdate(deltatime)
    {
        if(!this.mIsMoving)
        {
            return;
        }


        // let step = this.mMovingSpeed * deltatime;
        // let pos = this.node.position;
        //  //计算玩家移动的时间
        //  var playTime = cc.pDistance(pos,this.mMovingTarget) / this.mMovingSpeed;
        //  //让玩家移动到点击位置
        //  var action = cc.moveTo(playTime,this.mMovingTarget);

        //  this.node.runAction(action);

        // let t =  cc.Vec2.MoveTowards(pos, this.mMovingTarget, step);
        // t.y = pos.y;

        //防止超出场景
        if (this.node.x > SceneWidth)
        {
            this.node.x = SceneWidth;
        }
        else if (this.node.x < -SceneWidth)
        {
            this.node.x = -SceneWidth;
        }


        // this.node.position = t;

        this.now_time += deltatime;
        if (this.now_time > this.walk_time) {
            deltatime -= (this.now_time - this.walk_time);
        }

         var sx = this.vx * deltatime;
         var sy = this.vy * deltatime;
         this.node.x += sx;
         this.node.y += sy;


        //移动结束
        if (this.now_time >= this.walk_time)
        {
            this.StopMove();
        }
    },

    Repel(height, length, duration)
    {
        //击飞击退操作标记
        this.mIsRepeling = true;
        //移动起始位置
        this.mRepelStart = this.node.position;
        //移动半程位置
        this.mRepelMid = new cc.Vec2(this.mRepelStart.x, this.mRepelStart.y + height);

        //防止超出场景
        if (this.mRepelMid.x > SceneWidth)
        {
            this.mRepelMid.x = SceneWidth;
        }
        else if (this.mRepelMid.x < -SceneWidth)
        {
            this.mRepelMid.x = -SceneWidth;
        }
        //移动终点位置
        this.mRepelTarget = new cc.Vec2(this.mRepelStart.x, this.mRepelStart.y);

        //防止超出场景
        if (this.mRepelTarget.x > SceneWidth)
        {
            this.mRepelTarget.x = SceneWidth;
        }
        else if (this.mRepelTarget.x < -SceneWidth)
        {
            this.mRepelTarget.x = -SceneWidth;
        }
        //时间
        this.mRepelDuration = duration;
        //计时时间
        this.mRepelStartTime = (new Date()).getTime();
        //速度
        //this.mRepelSpeed = Math.Abs(length) / duration;
        this.mRepelSpeed = Math.Sqrt(Math.Pow(length, 2) + Math.Pow(height, 2)) / duration;
    },

    RepelUpdate()
    {
        if (!this.mIsRepeling)
        {
            return;
        }
        let target = this.mRepelTarget;
        if (this.GetTimeGap(this.mRepelStartTime) < this.mRepelDuration/ 2)
        {
            target = this.mRepelMid;
        }
        let step = this.mRepelSpeed * deltatime;
        pos = this.node.position;
        t = cc.Vec2.MoveTowards(pos, target, step);
        this.node.position = t;
        //再往终点移动
        //移动结束
        if (this.node.position == this.mRepelTarget)
        {
            this.StopRepel();
        }
    },

    StopRepel()
    {
        if (this.mIsRepeling)
        {
            this.node.position = this.mRepelTarget;
        }
        this.mIsRepeling = false;
    },

    Teleport(delay, x, y, z, effectName, rootType, effectDuration)
    {
        this.mTeleportEffectDuration = effectDuration;
        this.mTeleportEffectName = effectName;
        this.mIsTeleport = true;
        this.mTeleportDelayTime = delay;
        this.mTeleportPosition = new cc.Vec2(x, y);
        this.mTeleportStartTime = (new Date()).getTime();
        FightGameObjectManager.getInstance().SpwanEffectInFightRole(this.mID, this.mTeleportEffectName, rootType, this.mTeleportEffectDuration, false, 0, 0, 0);

    },

    TeleportUpdate()
    {
        if (!this.mIsTeleport)
        {
            return;
        }
        if(this.GetTimeGap(this.mTeleportStartTime) >= this.mTeleportDelayTime)
        {
            this.node.position = mTeleportPosition;
            FightGameObjectManager.getInstance().SpwanEffectInFightRole(this.mID, this.mTeleportEffectName, EffectRootType.RootWorld, this.mTeleportEffectDuration, false, 0, 0, 0);
            this.mIsTeleport = false;
        }
    },

    StopTeleport()
    {
        this.mIsTeleport = false;
    },

    Die(effectName, isBoss, rootType, duration, missTime, 
        aniName, rolePath, speed, mode, rewardEffect)
    {
        //停止移动
        this.StopMove();
        this.StopRepel();
        this.StopTeleport();
        // let tm = this.getComponent('TransparentModel');
        // if (tm != null)
        // {
        //     tm.Resume();
        // }
        this.mIsDead = true;
        this.mDeadStartTime = (new Date()).getTime();
        this.mDeadDelayTime = missTime;
        this.mIsBossDead = isBoss;

        //播放光效
        if (effectName)
        {
            FightGameObjectManager.getInstance().SpwanEffectInFightRole(mID, effectName, rootType, duration, false, 0, 0, 0);
        }
        let rewardEffectID = -1;
        //播放掉落光效
        if (rewardEffect)
        {
            rewardEffectID = FightGameObjectManager.getInstance().SpwanEffectInFightRole(mID, rewardEffect, rootType, duration, false, 0, 0, 0);
        }


            //播放动画
        if (aniName)
        {
            this.PlayAni(aniName, mode, speed);
        }

        //播放子动画
        if (rolePath)
        {
        //     let modelRoot = this.node.FindChild(rolePath);
        //     if(modelRoot != null && modelRoot.childCount > 0)
        //     {
        //         let model = modelRoot.GetChild(0);
        //         if(model != null)
        //         {
        //             GameObjectTool.PlayAni(model.gameObject, aniName, speed, mode);
        //         }
        //     }
        }
        return rewardEffectID;
    },

    DieUpdate()
    {
        if (!this.mIsDead)
        {
            return;
        }

        if ((new Date()).getTime() - this.mDeadStartTime >= this.mDeadDelayTime)
        {
            this.StopDie();
        }
    },

    StopDie()
    {
        if (!this.mIsBossDead)
        {
            this.node.position = new cc.Vec2(-200, -200, -200);
        }
        
        this.mIsDead = false;
    },



    /// <summary>
    /// 旋转到某个角度
    /// </summary>
    /// <param name="targetPos"></param>
    /// <param name="speed"></param>
    RotateTo(targetPos, speed)
    {
        this.mRotatingTarget = targetPos;
        this.mRotatingSpeed = speed;
        this.mRotatingStart = this.node.eulerAngles;
        this.mIsRotating = true;
    },

    /// <summary>
    /// 旋转是否结束
    /// </summary>
    /// <returns></returns>
    IsRotateEnd()
    {
        return mIsRotating;
    },

    /// <summary>
    /// 旋转Update函数
    /// </summary>
    RotateToUpdate(deltatime)
    {
        if (!this.mIsRotating)
        {
            return;
        }
        let  x = Math.Lerp(this.mRotatingStart.x, this.mRotatingTarget.x, deltatime * mRotatingSpeed * this.mGameSpeed);
        let y = Math.Lerp(this.mRotatingStart.y, this.mRotatingTarget.y, deltatime * mRotatingSpeed * this.mGameSpeed);
        let z = Math.Lerp(this.mRotatingStart.z, this.mRotatingTarget.z, deltatime * mRotatingSpeed * this.mGameSpeed);
        this.node.eulerAngles = new cc.Vec2(x, y, z);

        //移动结束
        if (this.node.eulerAngles == this.mRotatingTarget)
        {
            this.mIsRotating = false;
        }
    },


    /// <summary>
    /// 播放动画
    /// </summary>
    /// <param name="aniName">动画名称</param>
    /// <param name="wrapMode">动画循环模式</param>
    /// <param name="fAniSpeed">动画播放速度</param>
    /// <returns>返回该动画时长</returns>
    PlayAni(aniName, wrapMode, fAniSpeed = 1)
    {
        let animation = this.getComponentInChildren(dragonBones.ArmatureDisplay);
        if (animation == null || animation[aniName] == null)
        {
            //技能召唤物没有动画
            return;
        }

        //animation.Stop();
        let aniStat = animation[aniName];
        //aniStat.wrapMode = wrapMode;
        //aniStat.speed = fAniSpeed;
        animation.playAnimation(aniName);
        
    },

    /// <summary>
    /// 动画是否播放结束
    /// </summary>
    /// <returns></returns>
    IsPlayAniEnd(aniName)
    {
        let animation = this.getComponentInChildren(dragonBones.ArmatureDisplay);
        if (animation == null)
        {
            return false;
        }
        return true;
    },

    /// <summary>
    /// 停止当前的动画
    /// </summary>
    StopAni()
    {
        let animation = this.getComponentInChildren(dragonBones.ArmatureDisplay);
        if (animation == null)
        {
            return;
        }
        animation.playAnimation('stand');
        //animation.Stop();
    },

    /// <summary>
    /// 获取动画时长
    /// </summary>
    /// <param name="aniName"></param>
    /// <returns></returns>
    GetAniTime(aniName)
    {
        let animation = this.getComponentInChildren(dragonBones.ArmatureDisplay);
        if (animation == null || animation[aniName] == null)
        {
            return 0;
        }

        let aniStat = animation[aniName];
        return aniStat.length;
    },
    
    /// <summary>
    /// 获取动画状态
    /// </summary>
    /// <param name="ani"></param>
    /// <param name="aniName"></param>
    /// <returns></returns>
    GetAnimationState(ani ,aniName)
    {
        if (ani == null)
        {
            return null;
        }
        
        return ani[aniName];
    },

    GetObjID()
    {
        return this.mID;
    },

    SetObjID(id)
    {
        this.mID = id;
    },
    //-----------------------------------------------------------------
    OnDestroy() {
        //Debug.Log("~" + name + " was destroy!");
        this.mIsDestory = true;
    },


    ReleaseEffect()
    {
        let ads = this.getComponentsInChildren('AutoDestroy');
        if (ads.length > 0)
        {
            for(let i = 0; i < ads.length; i++)
            {
                let ad = ads[i];
                ad.Release();
            }
        }
    },

    Reset()
    {
        this.mIsDead = false;
        this.mIsTeleport = false;
        this.mIsRotating = false;
        this.mIsMoving = false;
    },

    SetSpeed(speed)
    {
        this.mGameSpeed = speed;
    },

    GetTimeGap(time)
    {
        return ((new Date()).getTime() - time) * mGameSpeed;
    }
});
