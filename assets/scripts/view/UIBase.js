/*
作者（Author）:    jason

描述（Describe）: 界面基类
*/

import MessageCenter from "../tool/MessageCenter";
import Util from "../tool/Util";
import ViewManager from "../manager/ViewManager";
let Thor = require('Thor');

let UIBase = cc.Class({
    extends: Thor,

    properties: {
    },
 
    init: function () {
        this._isReused = true; //界面是否重复利用
    },
    // 取消监听事件
    onDestroy: function ()
    {
        MessageCenter.delListen(this);
        this.unscheduleAllCallbacks();
    },

    showPanel: function (param) {
        if (this.node) {
            this.node.active = true;
        } else {
            console.warn('Access invalid compoment object!');
        }
    },

    hiddenPanel: function () {
        if (this.node) {
            this.node.active = false;
        } else {
            console.warn('Access invalid compoment object!');
        }
    },
    playAimationOnLoad: function () {
        let animation = Util.searchComp(this.node, 'Panel_root', cc.Animation);
        if (animation) {
            animation.play();
        }
    },
     // 关闭面板
     closePanel: function (event)
     {
         if (!this._isReused) {
             if (this.node) {
                ViewManager.removePanel(this.node);
             } else {
                 console.warn('Access invalid compoment object!');
             }
         } else {
             this.hiddenPanel();
         }
     },
});
export default UIBase;