/*
作者（Author）:    skyHuang

描述（Describe）:
*/
/*jshint esversion: 6 */

import Util from '../../tool/Util';
import BaseFlyItem from './BaseFlyItem';

cc.Class({
    extends: BaseFlyItem,

    onLoad()
    {
        this._super();
        this.mSpeed = 0;
        this.mDistance = 0;
        this.mOrientation = false;
        this.mStartTime = 0;            //道具初始创建时间  
        this.mAutoDestroy = null;
        this.mHasGo = 0;
    },
    create(id, speed, dist)
    {
        this.mID = id;
        this.mSpeed = speed;

        this.mStartTime = Date.now();

        this.mHasGo = 0;
        this.mDistance = dist;
        //旋转角度
        // Vector3 lea = this.transform.localEulerAngles;
        // if (speed > 0)
        // {
        //     lea.y = 0;
        // }
        // else
        // {
        //     lea.y = 180;
        // }
        // this.transform.localEulerAngles = lea;
    },

    update(dt)
    {
        let step = mSpeed * dt;
        let pos = this.node.position;
        //pos.z += step;
        //transform.position = pos;

        //计算已经走过多少距离
        mHasGo += Math.abs(step);
        if (mHasGo >= mDistance)
        {

            let ad = this.node.getComponent('AutoDestroy');
            if (ad != null)
            {
                ad.release();
            }
            else
            {
                this.node.destroy();
            }

        }
    }
})