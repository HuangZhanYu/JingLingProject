/*
作者（Author）:    skyHuang

描述（Describe）:
*/
/*jshint esversion: 6 */

import Util from '../../tool/Util';
let Thor = require('Thor');

let BaseFlyItem = cc.Class({
    extends: Thor,

    onLoad()
    {
        this.mID = 0;
        this.mDuration = 0;
    },
    near(position)
    {
        myPos = this.node.position;

        let dist = Util.getDistanceVec2(myPos,position);

        if(dist <= 1)
        {
            return true;
        }
        return false;
    }
})
export default BaseFlyItem;