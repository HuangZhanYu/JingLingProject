/*
作者（Author）:    skyHuang

描述（Describe）:
*/
/*jshint esversion: 6 */

import Util from '../../tool/Util';
import BaseFlyItem from './BaseFlyItem';

cc.Class({
    extends: BaseFlyItem,

    onLoad()
    {
        this._super();
        this.mTargetObj = null;       //目标对象受击点
        this.mSpeed = 0;                //水平速度
        this.mStartTime = 0;            //道具初始创建时间  
        this.mAutoDestroy = null;
    },
    create( id,  targetObj,  duration,  effectName)
    {
        this.mID = id;
        this.mDuration = duration / 1000;
        this.mTargetObj = targetObj;
        this.mSpeed = Util.getDistanceVec2(this.node.position, mTargetObj.position) / mDuration;

        mStartTime = Date.now();

        //自动销毁
        mAutoDestroy = this.node.getComponent('AutoDestroy');
        mAutoDestroy.setDelayTime(effectName, mDuration);
    },

    update(dt)
    {
        if (mTargetObj == null ||
            !this.mTargetObj.activeInHierarchy ||
            Date.now() - this.mStartTime >= this.mDuration ||
            this.mTargetObj.transform.position.z > 900 ||
            this.Near(mTargetObj.transform.position))
        {
            this.mAutoDestroy.release();
            return;
        }
        // Vector3 targetPosition = mTargetObj.transform.position;
        // this.transform.LookAt(targetPosition);
        // float currentDis = Vector3.Distance(this.transform.position, mTargetObj.transform.position);

        // this.transform.Translate(Vector3.forward * Mathf.Min(mSpeed * Time.deltaTime, currentDis));
    }
})