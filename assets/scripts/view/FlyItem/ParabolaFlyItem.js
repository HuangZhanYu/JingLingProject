/*
作者（Author）:    skyHuang

描述（Describe）:
*/
/*jshint esversion: 6 */

import Util from '../../tool/Util';
import BaseFlyItem from './BaseFlyItem';

cc.Class({
    extends: BaseFlyItem,

    onLoad()
    {
        this._super();
        this.mSpeed = 0;                //水平速度
        this.mStartTime= 0;            //道具初始创建时间  
        this.mAutoDestroy = null;

        this.m_PointA = null;//点A
        this.m_PointB = null;//点B 目标对象受击点
        this.g = -10;//重力加速度
                             // Use this for initialization
        this.m_Gravity = null;//重力向量
        this.dTime = 0;
    },
    create( id,  targetObj,  duration,  effectName)
    {
        this.mID = id;
        this.mDuration = duration / 1000;
        this.m_PointB = this.node.transform;
        //通过一个式子计算初速度
        // mSpeed = new Vector3((transform.position.x - transform.position.x) / mDuration,
        //     (m_PointB.y - transform.position.y) / mDuration - 0.5f * g * mDuration, 
        //     (m_PointB.position.z - transform.position.z) / mDuration);
        // m_Gravity = Vector3.zero;//重力初始速度为0

        // this.transform.LookAt(m_PointB.transform.position);
        mStartTime = Time.time;
        
        //自动销毁
        mAutoDestroy = this.node.getComponent('AutoDestroy');
        mAutoDestroy.setDelayTime(effectName, mDuration);
    },

    // FixedUpdate()
    // {
    //     if (m_PointB == null ||
    //         !m_PointB.gameObject.activeInHierarchy ||
    //         Time.time - mStartTime >= mDuration ||
    //         m_PointB.transform.position.z > 900)
    //     {
    //         mAutoDestroy.Release();
    //         return;
    //     }
    //     this.transform.LookAt(m_PointB.transform.position);
    //     m_Gravity.y = g * (dTime += Time.fixedDeltaTime);//v=at
    //                                                     //模拟位移
    //     transform.Translate(mSpeed * Time.fixedDeltaTime);
    //     transform.Translate(m_Gravity * Time.fixedDeltaTime);
    // }
})