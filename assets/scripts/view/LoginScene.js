/*
作者（Author）:    jason

描述（Describe）: 
*/

import ViewManager from "../manager/ViewManager";
import { PanelResName } from "../common/PanelResName";
import Util from "../tool/Util";
import MessageCenter from "../tool/MessageCenter";
import { MessageID } from "../common/MessageID";
import BigNumber from "../tool/BigNumber";
import ResourceManager from "../tool/ResourceManager";

import SocketClient from "../network/SocketClient";
import PBHelper from "../network/PBHelper";

let Thor = require('Thor');

cc.Class({
    extends: Thor,

    properties: {
        //_bindHammer: false
    },
    
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    onLoad () {
       //this._bindHammer = false;
        console.log(this._button.name);
        let a = BigNumber.getBigInteger(1111111111);
        let b = BigNumber.getBigInteger(2222222222);
        console.log(`getBigInteger  = ${BigNumber.create("123.33a")}`);

        // let s = "1.2-345678ab";
        // let chars = s.split("");
        // for(let c of chars)
        // {
        //     console.log(c);
        //     console.log(`parseInt = ${parseInt(c)}`);
        // }
        // const [inte,unit] = BigNumber.parse(s);

        // console.log(`inte = ${inte} unit = ${unit}`);

        // ResourceManager.setLoadPrefabNames(["uiprefab/loginPanel"],function(){
        //     console.log('loginPanel  加载完成');
        // });
        //ResourceManager.loadPrefab();
        LoadConfig.loadAllConfigs();

        cc.macro.ENABLE_TRANSPARENT_CANVAS = true;
        this._button.active = false;
        MessageCenter.addListen(this,function(){
            let data = LoadConfig.getConfigData('ShiBing','1012010');
            if(data)
            {
                console.log(data.Name);
            }
            this._button.active = true;
        }.bind(this),MessageID.MsgID_LoadConfigFinished);

        //window.PB = PBHelper.loadFile('resources/proto', 'grace.proto.msg');
        
    },
    _onButtonTouchEnd(sender, event)
    {
        console.log(sender.name);
        SocketClient.connectServer();
    }
    // update (dt) {},
});
