/*
作者（Author）:    jason

描述（Describe）: 游戏管理类，游戏启动
*/
/*jshint esversion: 6 */
import ViewManager from "../manager/ViewManager";
import Util from "../tool/Util";
import MessageCenter from "../tool/MessageCenter";
import { MessageID } from "../common/MessageID";
import { PanelResName, CommonPrefabNames } from "../common/PanelResName";
import ExponentTool from "../tool/ExponentTool";
import SceneManager from "../manager/SceneManager";
import TimerManager from "../tool/TimerManager";
import ResourceManager from "../tool/ResourceManager";

const i18n = require('LanguageData');
let ConfigsLoadFinished = false;
let LoadCommonPrefabFinished = false;
cc.Class({
    extends: cc.Component,

    properties: {
      loginPrefab:cc.Prefab
    },

    onLoad () {
        //初始化多语言配置
        i18n.init('Chinese');
        i18n.updateSceneRenderers();
        //切换场景不销毁
        cc.game.addPersistRootNode(this.node);
        SceneManager.init();
        // this.loginPanel = cc.instantiate(this.loginPrefab);
        // let canvas = Util.getRootCanvas();
        // canvas.addChild(this.loginPanel);
        ViewManager.addPanel(PanelResName.loginPanel);
        
        LoadConfig.loadAllConfigs();

        this.addListeningMessage();
        this.loadCommonPrefab();
    },
    //添加监听的消息
    addListeningMessage()
    {
        MessageCenter.addListen(this,this.onLoginSucceed,MessageID.MsgID_LoginSucceed);
        MessageCenter.addListen(this,this.onCreatePlayerSucceed,MessageID.MsgID_CreatePlayerSucceed);
        MessageCenter.addListen(this,this.onLoadConfigFinished,MessageID.MsgID_LoadConfigFinished);
    },
    onLoadConfigFinished()
    {
        ConfigsLoadFinished = true;
        if(LoadCommonPrefabFinished)
        {
            //初始化指数工具
            ExponentTool.Init();
        }
        
    },
    onLoginSucceed(newPlayer)
    {
        if (newPlayer == 1)
        {
            //NewPlayer值为1说明需要创建角色
            //开启新手引导
            // NewbiePanel.Show(15)
            // Network.SetMissHeartTimes(0)
            ViewManager.addPanel(PanelResName.CreateRoolPanel,(comp)=>{
                ViewManager.removePanelByUrl(PanelResName.loginPanel);
            });
        }
        else
        {   
            //NewPlayer值为其他则直接进入游戏
            ViewManager.addPanel(PanelResName.MainPanel,(comp)=>{
               
                ViewManager.removePanelByUrl(PanelResName.loginPanel);
            });
        }
    },
    onCreatePlayerSucceed()
    {
        // --初始化自动任务
        TaskPart.AutoOnLogin();
        // --显示主界面
        ViewManager.addPanel(PanelResName.MainPanel);
    },
    //加载公共的预设资源
    loadCommonPrefab()
    {
        let prefabNames = [];
        for(let key in CommonPrefabNames)
        {
            let prefabName = CommonPrefabNames[key];
            prefabNames.push(prefabName);
            console.log(`loadCommonPrefab prefabName = ${prefabName}`);
        }
        //LoadCommonPrefabFinished = true;
        ResourceManager.setLoadPrefabNames(prefabNames,()=>{
            LoadCommonPrefabFinished = true;
            
            if(ConfigsLoadFinished)
            {
                //初始化指数工具
                ExponentTool.Init();
            }
        });
        ResourceManager.loadPrefab();
    },
    onDestroy() {
        MessageCenter.delListen(this);
    },
    update (dt) {
        TimerManager.update(dt);
    },
});
