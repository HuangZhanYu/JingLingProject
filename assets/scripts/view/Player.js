/*
作者（Author）:    jason

描述（Describe）: 角色动画测试类
*/
const AniNames = ["idl","attack","skill","walk"];
cc.Class({
    extends: cc.Component,
    
    onLoad:function(){
        //获取 ArmatureDisplay
        this._armatureDisPlay = this.getComponent(dragonBones.ArmatureDisplay)
        this.index = 0;
        this.schedule(function(){
            this._armatureDisPlay.playAnimation(AniNames[this.index], 0);
            ++this.index;
            if(this.index > AniNames.length)
            {
                this.index = 0;
            }
        }.bind(this),3,100);
        
    },

})
