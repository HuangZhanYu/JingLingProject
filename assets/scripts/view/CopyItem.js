/**
 * 副本item类
 */
import { PlayerIntAttr } from "../part/PlayerBasePart";
const i18n = require('LanguageData');
import StringTool from "../tool/StringTool";
import DaoJuTool from "../tool/DaoJuTool";
import ViewManager, { PanelPos } from "../manager/ViewManager";
import { PanelResName } from "../common/PanelResName";
cc.Class({
    extends: cc.Component,

    properties: {
        copyName:cc.Label, //副本的名字
        enterTime:cc.Label, //进入副本的时间
        lockInfo:cc.Node, //限制信息父级
        levelLimit:cc.Label, //等级限制
        tongguanProgress:cc.Label, //通关进度
        progressInfo:cc.Node, //进度信息父级
        icon:cc.Sprite, //副本图标
        noCanEnterTips:cc.Node,//不能进的标志
        awardParent:cc.Node, //奖励的父级
        limitParent:cc.Node, //限制携带的宠物
        notLimitParent:cc.Node,
        atlas:cc.SpriteAtlas,
    },
    start () {
       
    },
    onEnable()
    {
        this._addEvents();
    },
    onDisable()
    {
        this._removeEvents();
    },
    _addEvents()
    {
        this.node.on(cc.Node.EventType.TOUCH_END,this._onClickItemEvent,this);
    },
    _removeEvents()
    {
        this.node.off(cc.Node.EventType.TOUCH_END,this._onClickItemEvent);
    },
    _onClickItemEvent()
    {
        ViewManager.addPanel(PanelResName.DungeonDiffPopPanel,(panel)=>
        {

        },PanelPos.PP_Midle,this.index);
    },
    updateItem(copyData,index)
    {
        this.copyData = copyData;
        this.index = index;
        this._setBaseInfo();
        this._setAward();
        this._setPetLimit();
    },

    _setBaseInfo()
    {
        let functionID = 49 + this.index;
        let functionInfo = LoadConfig.getConfigData(ConfigName.GongNengKaiFang,functionID)
        if(functionInfo == null)
        {
            console.error("功能开放表为空ID"+functionID);
            return
        }
        //设置周几开放
        let content = this.getStringOpenDay(this.copyData);
        //拿到玩家等级
       let level = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_DengJi];
        //是否等级合格
        let isLevelEnough = level >= functionInfo.ZhuJiaoDengJi
        let isOpenToday = this.IsOpenToday(this.copyData)
        //判断等级是否达到地下城开放等级
        if( !isLevelEnough )
        {
            let contentStr = i18n.t('CopyeText.LevelLimit');
            this.levelLimit.string = StringTool.ZhuanHuan(contentStr,[functionInfo.ZhuJiaoDengJi])
            this._removeEvents();
        }
        else 
         {
                //如果今日开放,则设置开放相关信息
            if(isOpenToday == true)
            { 
                this._addEvents();
            }
        }
        //设置进度显隐
        this.progressInfo.active = isLevelEnough;
        this.noCanEnterTips.active = (!isLevelEnough || !isOpenToday);
        this.lockInfo.active = (!isLevelEnough);
        if (isLevelEnough == true) 
        {
            //根据进度设置文字
            let curRound = DungeonPart.GetTopRound(this.index);  //当前进度,第几层了
            let maxRound = DungeonPart.GetCurDungeonMaxRound(this.index); //最高关卡
            let showText = ""
            if (curRound == 0)
            {
                showText = i18n.t('CopyeText.Progress');
            }
            else if (curRound == maxRound)
            {
                showText = i18n.t('CopyeText.Progress3');
            }
            else
            {
                showText = StringTool.ZhuanHuan(i18n.t('CopyeText.Progress2'),[curRound])
            }
            this.tongguanProgress.string = showText;
        }
        this.copyName.string = this.copyData.Name;
        this.enterTime.string = content;
        let spriteFrame = this.atlas.getSpriteFrame(this.copyData.Icon);
        if (spriteFrame != null) 
        {
            this.icon.spriteFrame = spriteFrame;
        }
    },
    /**
     * 设置限制进去的精灵
     */
    _setPetLimit()
    {
        let limitData = this.copyData.Islimit;
        if (limitData == 0) 
        {
            limitData = [];
        }
        let petTypeTips = [];
        
        for (let index = 0; index < limitData.length; index++) 
        {
            const element = limitData[index];
            if (element == 0) 
            {
                petTypeTips.push(index);
            }
        }
        let shuXingIcon = LoadConfig.getConfig(ConfigName.ShuXingIcon);
        let hasLimit = false;
        for (let index = 0; index < this.limitParent.childrenCount; index++) 
        {
            let image = this.limitParent.children[index];
            let iconData = petTypeTips[index];
            iconData = shuXingIcon[iconData];
            let hasData = (iconData != null);
            image.active = hasData;
            if (hasData == true) 
            {
                let icon = image.getComponent(cc.Sprite);
                hasLimit = true;
                let spriteFrame = this.atlas.getSpriteFrame(iconData.Icon);
                if (spriteFrame == null) 
                {
                    console.warn(">>>没有加载到图素"+iconData.Icon)   
                }
                icon.spriteFrame = spriteFrame;
            }
        }

        this.notLimitParent.active = !hasLimit;
        this.limitParent.active = hasLimit;
    },

    _setAward()
    {
        let awardItem = null;
        if (this.awardParent.childrenCount == 0) 
        {
            awardItem = DaoJuTool.LoadDaoJu(this.awardParent);
        }
        awardItem = this.awardParent.children[0];
        let jiangChiInfo = LoadConfig.getConfigData(ConfigName.JiangChi,this.copyData.Reward);
        let itemType = jiangChiInfo.ResourceTypes[0];
        let itemId = jiangChiInfo.ResourceIDs[0];
        DaoJuTool.SetDaoJuImage(awardItem,itemType,itemId);
        DaoJuTool.SetDaoJuItemText(awardItem,"");
    },

    IsOpenToday(data) //今日该副本是否开放
    {
        let dayNum = new Date().getDay(); // -- 1  ---- 7
        let numTransform = Number(dayNum)
        //存下表格数据，用于判断今日周几
        let monday = data.IsMonday
        let tuesday = data.IsTuesday
        let wednesday = data.IsWednesday
        let thursday = data.IsThursday
        let friday = data.IsFriday
        let saturday = data.IsSaturday
        let sunday = data.IsSunday
    
        //根据今天是周几以及表格配置的周几开放，判断今天是否开启
        let result = (numTransform == 1 && monday == 1 ||
            numTransform == 2 && tuesday == 1 ||
            numTransform == 3 && wednesday == 1 ||
            numTransform == 4 && thursday == 1 ||
            numTransform == 5 && friday == 1 ||
            numTransform == 6 && saturday == 1 ||
            numTransform == 0 && sunday == 1 )
        return result;
    },
    
    getStringOpenDay(data) //获取副本开放的时间
    {
        //存下表格周几是否开放
        let monday = data.IsMonday
        let tuesday = data.IsTuesday
        let wednesday = data.IsWednesday
        let thursday = data.IsThursday
        let friday = data.IsFriday
        let saturday = data.IsSaturday
        let sunday = data.IsSunday

        let fanyiText = i18n.t('CopyeText.EnterTime');
        let des = fanyiText //初始化显示文本的字符串

        //如果全部为1，则每天都开放
        if (monday == 1 && tuesday == 1 && wednesday == 1 && thursday == 1 && friday == 1 && saturday == 1 && sunday == 1)
        {
            des = fanyiText+"每天" 
            return des;
        }

        //判断周几开放，并加入后缀
        if(monday == 1)
            des = des+"周一 "
        if(tuesday == 1)
            des = des+"周二 "
        if(wednesday == 1)
            des = des+"周三 "
        if (thursday == 1)
            des = des+"周四 "
        if(friday == 1)
            des = des+"周五 "
        if(saturday == 1)
            des = des+"周六 "
        if(sunday == 1)
            des = des+"周日 "
        return des
    }
    // update (dt) {},
});
