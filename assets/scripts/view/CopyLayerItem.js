import StringTool from "../tool/StringTool";
const i18n = require('LanguageData');
/**
 * 副本层数item
 */
cc.Class({
    extends: cc.Component,

    properties: {
        yiTongGuanTips:cc.Node, //已通关的标志
        layer:cc.Label, //层数
        icon:cc.Sprite, //怪物模型图标
        puTongItem:cc.Node, //普通的image（可选择）
        liangItem:cc.Node, //亮的image （已选择）
        AnImage:cc.Node,//暗的image (不可选)
        iconAtlas:cc.SpriteAtlas, //头像图集
        onClickFunc:null //点击item的事件
    },
    start () {
        this._addEvents();
    },
    onDestroy()
    {
        this._removeEvents();
    },
    _addEvents()
    {
        this.node.on(cc.Node.EventType.TOUCH_END,this._onClickNodeItemEvents,this);
    },
    _removeEvents()
    {
        this.node.off(cc.Node.EventType.TOUCH_END,this._onClickNodeItemEvents);
    },
    _onClickNodeItemEvents()
    {
        if (this.onClickFunc!= null) 
        {
            this.onClickFunc(this.node);
        }
    },
    updateLiangeItemState(state)
    {
        this.liangItem.active = state;
    },
    /***
     * 设置boss的头像
     */
    _setBossIcon()
    {
        let guanQiaData = LoadConfig.getConfigData(ConfigName.DiXiaCheng_GuanKa,this.copyId);
        if (guanQiaData == null) 
        {
            console.error(">>>没有获取到关卡数据："+this.copyId);
            return;   
        }
        let specialID = LoadConfig.getConfigData(ConfigName.ShiBing,guanQiaData.BossID);
        if (specialID == null) 
        {
            console.error(">>>卡牌ID错误，ID"+guanQiaData.BossID);
            return;  
        }
        let specialmodelInfo = LoadConfig.getConfigData(ConfigName.MoXing,specialID.ModelID);
        if (specialmodelInfo == null) 
        {
            console.error("模型ID错误，ID"+specialID.ModelID);
            return;
        }

        if (this.iconAtlas != null) 
        {
            let spriteFrame = this.iconAtlas.getSpriteFrame(specialmodelInfo.Icon);
            this.icon.spriteFrame = spriteFrame;
        }
        
    },
    updateItem(copyId,currentTongGuanIndex)
    {
        this.copyId = copyId;// 副本ID
        this.guanQiaID = copyId%1000; //关卡ID
        this.updateLiangeItemState(false);
        this.currentTongGuanIndex = currentTongGuanIndex; // 当前通关的关卡index
        this._setBossIcon();
        this._setBaseInfo();
    },
    /**
     * 设置基础信息
     */
    _setBaseInfo()
    {
        this.yiTongGuanTips.active = (this.guanQiaID <= this.currentTongGuanIndex);
        this.AnImage.active = (this.guanQiaID > this.currentTongGuanIndex+1);
        this.puTongItem.active = (this.guanQiaID <= this.currentTongGuanIndex+1);
        this.layer.string = StringTool.ZhuanHuan(i18n.t('CopyeText.CopyLayer'),[this.guanQiaID])
    }
    // update (dt) {},
});
