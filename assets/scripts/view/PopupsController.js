/*
作者（Author）:    skyHuang

描述（Describe）:
*/
/*jshint esversion: 6 */
import ViewManager, { PanelPos } from "../manager/ViewManager";

var PopupsController = cc.Class({
    properties: {

    },
    init: function () {
        this._popupList = [];//保存所有显示的弹窗界面
        this._showIndex = -1;//当前显示界面的_popupList下标
    },
    //添加弹窗显示的界面地址(显示界面的预设地址)
    addShowPopup(value) {
        let {urlOrPrefabOrNode = null, afterCreate = null, panelPos = PanelPos.PP_Bottom, param = null} = value;
        if (this._popupList.length == 0) {
            ViewManager.addPanel(urlOrPrefabOrNode,afterCreate,panelPos,param);
            this._showIndex = 0;
        }
        this._popupList.push({urlOrPrefabOrNode, afterCreate, panelPos, param});
    },

    //关闭弹窗(如果_popupList里面还有数据,就显示界面)
    closeShowPopup() {
        if (this._popupList.length < 1) {
            return;
        }
        if (this._popupList.length == 1) {
            this._popupList = [];
            this._showIndex--;
        } else {
            if (this._showIndex < this._popupList.length) {
                this._showIndex++;
                let data = this._popupList[this._showIndex];
                ViewManager.addPanel(data.urlOrPrefabOrNode,data.afterCreate,data.panelPos,data.param);
                if (this._showIndex == this._popupList.length - 1) {
                    this._showIndex = -1;
                    this._popupList = [];
                }
            } else {
                this._showIndex = -1;
                this._popupList = [];
            }
        }
    },
});
PopupsController.instance = null;
PopupsController.getInstance = function () {
    if (PopupsController.instance == null) {
        PopupsController.instance = new PopupsController();
        PopupsController.instance.init();
    }
    return PopupsController.instance;
};
export default PopupsController;