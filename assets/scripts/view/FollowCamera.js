/*
作者（Author）:    jason

描述（Describe）:
*/
/*jshint esversion: 6 */
/// <summary>
/// 屏幕拖动脚本
/// 设置一个目标点，进行碎碎
/// 当手指触摸屏幕时，获取第一个触摸点，根据拖动数值，拖动镜头
/// 回归路线，距离远时速度快，距离近时速度慢
/// </summary>

const NORMAL_SPEED = 100.0;    //误差范围
const ERROR_RANGE = 0.05;       //误差范围
const MAX_FOLLOW_TIME = 1000;    //最大跟随时间
cc.Class({
    extends: cc.Component,

    properties: {
       
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.moveSpeed = 10.0;     //滑动速度
        this.max             = 125;    //最大滑动位置
        this.min             = -125;   //最小滑动位置
        this.holdingTime     = 0;                //hold住开始时间
        this.maxHoldingTime  = 1;        //Hold住时间
        this.isHolding       = false;    //Hold住镜头
        this.isDrag          = false;    //当前是否正在拖动
        this.isTouch         = false;    //当前是否正在触摸
        this.dragXDistance   = 0;        //X轴的移动
        this.originZ = 0;        //初始的Z位置，用来给最大值最小值做便宜量
        this.originX = 0;        //初始的X位置，用来给最大值最小值做便宜量

        ///////////////////////////////////跟随部分/////////////////////////////////////////
        this.standardPos = cc.Vec2.ZERO;            //基准位置
        this.standardPosWorld = cc.Vec2.ZERO;       //基准位置世界坐标
        this.target = null;                 //目标对象
        this.followSpeed = 1.0;           //跟随速度
        this.isFollowed = false;            //是否在跟随中
        this.startPos = cc.Vec2.ZERO;                   //跟随初始位置
        this.followTime = 0;                   //跟随计算中的时间

        this.curRoot = null;                  //当前镜头的父对象
        this.emptyRoot = null;                //镜头原始对象
        this.beforeDragTarget = null;

        this.isShieldTouch = false;
        this.IsButtonDrag = false;
        this.IsButtonDown = false;
        this.IsButtonUp = false;
        this.node.on(cc.Node.EventType.TOUCH_START,function(event){
            this.IsButtonDown = true;
            this.isFollowed = false;
        },this);
        this.node.on(cc.Node.EventType.TOUCH_MOVE,function(event){
            this.IsButtonDrag = true;
            var delta = event.touch.getDelta();
            this.deltaX = -delta.x;

            //console.log(`TOUCH_MOVE   delta = ${delta}`);
        },this);
        this.node.on(cc.Node.EventType.TOUCH_END,function(event){
            this.IsButtonDown = false;
            this.IsButtonDrag = false;
            this.IsButtonUp = true;
        },this);
    },
    get MoveSpeed()
    {
        return this.moveSpeed;
    },
    start () {
        this.emptyRoot = this.node.parent;
        this.curRoot = this.emptyRoot;
        this.standardPos = this.node.position;
        this.originX = this.node.x;
    },
    /// <summary>
    /// 设置最大值
    /// </summary>
    /// <param name="newMax"></param>
    SetMax(newMax)
    {
        this.max = newMax;
    },

    /// <summary>
    /// 设置最小值
    /// </summary>
    /// <param name="newMin"></param>
    SetMin(newMin)
    {
        this.min = newMin;
    },

    /// <summary>
    /// 将镜头移回原点
    /// </summary>
    SetOriginRoot()
    {
        if (this.target == this.emptyRoot)
        {
            return;
        }
        this.beforeDragTarget = this.target;
        this.target = this.emptyRoot;
        this.node.x = this.target.x;
    },


    /// <summary>
    /// 设置当前为跟随状态，内部使用，在点击滑动结束后调用
    /// </summary>
    SetFollow()
    {
        let curPos = this.node.position;
        this.startPos = curPos;
        this.isFollowed = true;
        this.followTime = (new Date()).getTime();
    },

    /// <summary>
    /// 移动到某个位置
    /// </summary>
    /// <param name="obj"></param>
    MoveToTarget(obj)
    {
        this.target = obj;
        //this.node.parent = this.target;
    },

    /// <summary>
    /// 设置目标对象
    /// </summary>
    /// <param name="tar"></param>
    SetTarGet(tar)
    {
        
        if (tar == this.target)
        {
            return;
        }
        this.beforeDragTarget = tar;
        if (this.isTouch || this.isDrag)
        {
            return;
        }
        this.target = tar;
        //this.node.parent = this.target;
        this.SetFollow();
    },
    /// <summary>
    /// 设置镜头跟随速度
    /// </summary>
    /// <param name="rate"></param>
    SetSpeed(rate)
    {
        this.moveSpeed = NORMAL_SPEED * rate;
    },

    /// <summary>
    /// 是否屏蔽点击
    /// </summary>
    /// <param name="active"></param>
    ShieldTouch(active)
    {
        this.isShieldTouch = active;
        if (active)
        {
            this.SetTarGet(this.beforeDragTarget);
            this.isDrag = false;
            this.isTouch = false;
            this.isHolding = false;
        }
    },

    /// <summary>
    /// 给战斗停止时使用的
    /// </summary>
    /// <param name="active"></param>
    StopTouch(active)
    {
        this.isShieldTouch = active;
        if (active)
        {
            this.isDrag = false;
            this.isTouch = false;
            this.isHolding = false;
        }
    },

    /// <summary>
    /// 重置镜头
    /// </summary>
    Reset()
    {
        this.curRoot = this.emptyRoot;
        this.beforeDragTarget = curRoot;
        this.isFollowed = false;
        this.isDrag = false;
        this.isTouch = false;
        this.isHolding = false;
    },

    ///停留镜头1秒
    Hold(time)
    {
        this.maxHoldingTime = time;
        this.curRoot = emptyRoot;
        this.SetTarGet(emptyRoot);
        this.isFollowed = false;
        this.isTouch = false;
        this.isDrag = true;
        this.isHolding = true;
        this.holdingTime = (new Date()).getTime();
    },
    /// <summary>
    /// 镜头跟随
    /// </summary>
    Follow()
    {

        if (!this.isFollowed)
        {
            return;
        }
        //拖动时不跟随
        if (this.isDrag || this.isTouch)
        {
            return;
        }

        //目标对象不存在时，不跟随
        if (this.target == null)
        {
            return;
        }
        //对比当前的的位置与基准位置的差距，进行偏移
        let curPos = this.node.position;


        //设置距离速度，不同的距离速度不同，远的快，近的慢
        //和美术约定好只移动X轴

    //    if (Math.abs(this.target.x - curPos.x) <= 1)
    //     {
    //         this.isFollowed = false;
    //         return;
    //     }
       
        //开始计时
        let timeCounting = (new Date()).getTime() - this.followTime;

        // if(timeCounting >= MAX_FOLLOW_TIME) //固定1秒
        // {
        //     timeCounting = MAX_FOLLOW_TIME;
        //     this.isFollowed = false;
        // }
        //插值移动 
        //let speed = this.followSpeed * Time.deltaTime;
        let newPos = curPos.lerp(this.target.position, 0.1);
        //curPos.x = moveX;
        //curPos.y = moveY;
        // curPos.x = moveZ;
        // this.node.x = curPos.x;
        this.node.x = newPos.x;
    },

    update (dt) {
        if (this.isHolding)
        {
            if((new Date()).getTime() - this.holdingTime > this.maxHoldingTime)
            {
                this.isDrag = false;
                this.isHolding = false;
                let pos = this.beforeDragTarget.position;
                if(pos.x < 800)
                {
                    this.SetTarGet(this.beforeDragTarget);
                }
               
            }
        }


        if (!this.isShieldTouch)
        {

            //判断鼠标时候按下
            if (this.IsButtonDown)
            {
                //设置标记
                this.isTouch = true;
            }

            // if (this.isTouch)
            // {
            //     //判断是否拖动，如果是true，就等到Up再还原
            //     if (!this.isDrag)
            //     {
            //         this.isDrag = this.IsButtonDrag;
            //         this.SetOriginRoot();
            //     }
            // }

            //拖动时
            if (this.IsButtonDrag)
            {
                //let deltaX = this.GetDeltaX();

                this.dragXDistance += Math.abs(this.deltaX);


                let deltaVec = 0;
                let newVec = 0;

                //计算滑动
                deltaVec = this.deltaX * dt * this.moveSpeed;
                newVec = this.node.x + deltaVec;
                if (newVec > this.max)
                {
                    newVec = this.max;
                }

                if (newVec < this.min)
                {
                    newVec = this.min;
                }
                this.node.x = newVec;
                //目前还没有最大最小拖动范围
                //console.log(`update  this.node.x = ${this.node.x}`);
            }

            //手指抬起
            if (this.IsButtonUp)
            {
                this.dragXDistance = 0;
                this.isDrag = false;
                this.isTouch = false;
                this.SetTarGet(this.beforeDragTarget);
                this.SetFollow();
                this.IsButtonUp = false;
            }

            if (this.isTouch || this.isDrag)
            {
                return;
            }
        }


        //跟随
        this.Follow();
    },
    lateUpdate()
    {
        let curPosX = this.node.x;
        if (curPosX > this.max)
        {
            curPosX = this.max;
        }

        if (curPosX < this.min)
        {
            curPosX = this.min;
        }
        // curPosX = this.originX;
        this.node.x = curPosX;

        if (Math.abs(curPosX) > 900 )
        {
            //Util.CallFunc("FightBehaviorManager", "ResetCamera");
        }
    }
});
