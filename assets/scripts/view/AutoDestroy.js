/*
作者（Author）:    jason

描述（Describe）: 
/// 战斗光效销毁管理脚本
/// 1、静态HasHset存储着所有当前战斗创建的光效，战斗结束时释放
/// 2、可以定时销毁光效
/// 3、可以手动调用销毁光效
*/
let TotalDestroy = [];
let AutoDestroy = cc.Class({
    extends: cc.Component,

    properties: {
      
    },
    //定义静态成员
    statics:{
        ReleaseAll : function()
        {
            for(let des of TotalDestroy)
            {
                des.release();
            }
        }
    },
    onLoad () {
        this.index = TotalDestroy.push(this) - 1;
    },
    //不使用定时销毁功能的设置函数
    setName(effectName)
    {
        this.mEffectName = effectName;//光效名，用来释放光效
        this.mNeedFixedUpdate = false;//是否需要定时销毁功能
    },
    //使用定时销毁的设置函数
    setDelayTime(effectName,delay)
    {
        //this.mStartTime = (new Date()).getTime();;
        this.mEffectName = effectName;
        this.mDelayTime = delay;
        this.mHasDespawn = false;
        this.mNeedFixedUpdate = true;
    },
    onDestroy()
    {
        TotalDestroy.splice(this.index,this);
    },

    onEnable()
    {

    },
    onDisable()
    {

    },
    // update (dt) {},
    release()
    {

    },
    lateUpdate(dt)
    {
        if(!this.mNeedFixedUpdate)
        {
            return;
        }
    }
});

export default AutoDestroy;