/*
作者（Author）:    jason

描述（Describe）: 游戏内战斗对象管理类
*/
/*jshint esversion: 6 */
import Util from "../tool/Util";
import ResourceManager from "../tool/ResourceManager";
import SafeLoader from "../tool/SafeLoader";

let AutoDestroy = require("AutoDestroy");
let Thor = require('Thor');

const FlyItemType = 
{
    Line : 1,    //直线
    Parabola : 2,    //抛物线
    Cross : 3,    //穿透射击
    Chain : 4,    //连锁
}

const EffectRootType = {
    Head : 1,    //头顶
    Hit : 2,    //受击点
    Root : 3,    //0点
    HeadWorld : 4,    //头顶，世界坐标
    HitWorld : 5,    //受击点，世界坐标
    RootWorld : 6,    //0点，世界坐标
    RootWorldYZero : 7,    //0点，世界坐标，Y轴为0
}


const FightRolePathType = {
    Parent : 0,
    Role : 1,
    Hit : 2,
    Head : 3,
    UI : 4,
    Blood : 5,
    Hud : 6,
    Max : 7,
};

const CampType = 
{
    My : 0,
    Enemy : 1,
};
/// <summary>
/// 对象池中的光效对象
/// </summary>
class FightSpawnObj
{
    constructor(obj, use = false)
    {
        this.mObj = obj;
        this.mIsUse = use;
    }
}

const SpawnCount = 2;//对象池创建数量
/// <summary>
/// 光效对象池
/// </summary>
class FightSpawnList {
 
    constructor( name, cacheCount, obj, root)
    {
        this.mName = name;              //缓存的资源名
        this.mCacheCount = cacheCount; //缓存数量
        this.mObj = obj;                //原始对象
        this.mRoot = root;              //缓存的父对象
        this.mList = [];                //GameObject缓存
        for (let j = 0; j < SpawnCount; j++)
        {
            let spawnObj = cc.instantiate(this.mObj);
            spawnObj.name += "_" + j;
            let ad = spawnObj.addComponent("AutoDestroy");
            ad.setName(this.mName);

            spawnObj.parent = this.mRoot;
            let fso = new FightSpawnObj(spawnObj);
            this.mList.push(fso);
        }
    }

    

    Spawn()
    {
        for(let i = 0; i < mList.length; i++)
        {
            let fso = mList[i];
            if (!fso.mIsUse)
            {
                fso.mIsUse = true;
                return fso.mObj;
            }
        }
        newObj = cc.instantiate(mObj);
        newObj.name += "_" + mList.length;
        let ad = newObj.addComponent('AutoDestroy');
        ad.SetName(mName);
        newObj.SetParent(mRoot);
        let newFso = new FightSpawnObj(newObj, true);
        mList.add(newFso);
        return newObj;
    }

    Despawn(obj)
    {
        for(let i = 0; i < mList.length; i++)
        {
            let fso = mList[i];
            if(fso.mObj == obj)
            {
                fso.mObj.SetParent(mRoot);
                fso.mObj.active = false;
                fso.mIsUse = false;
                return;
            }
        }
    }

    Destroy()
    {
        for(let i = 0; i < mList.length; i++)
        {
            let fso = mList[i];
            fso.mObj.destroy();
            fso.mObj = null;
        }
        mList.Clear();
        mObj.destroy();
    }

    Release()
    {
        if(mCacheCount <= 0)
        {
            Destroy();
            return;
        }
        let hasCacheCount = mList.length;
        if (hasCacheCount > mCacheCount)
        {
            for (let i = hasCacheCount - 1; i >= mCacheCount -1; i--)
            {
                let fso = mList[i];
                fso.mObj.destroy();
                fso.mObj = null;
                mList.RemoveAt(i);
            }
        }
    }
}
/// <summary>
/// 战斗模型对象
/// </summary>
class FightRolePathObj
{
    constructor(id,obj, campType,rolePath,hitPath,headPath,uiPath)
    {
        this.mCampType = campType;
        //初始化数组长度
        this.mObjs = new Array(FightRolePathType.Max);

        if (obj == null)
        {
            return;
        }
        this.mObjs[FightRolePathType.Parent] = obj;
       
        
        this.mObjs[FightRolePathType.Role] = obj;
        this.mBehaviour = obj.getComponent('RoleBehaviour');

        if(!this.mBehaviour)
        {
            console.log(`FightRolePathObj  name = ${obj.name} 资源加载失败`);
            return;
        }
        this.mBehaviour.SetObjID(id);
        

        // let hitT = cc.find(hitPath,obj);
        // if (hitT != null)
        // {
        //     this.mObjs[FightRolePathType.Hit] = hitT;
        // }

        // let headT = cc.find(headPath,obj);
        // if (headT != null)
        // {
        //     head = headT;
        //     this.mObjs[FightRolePathType.Head] = head;
        //     this.mAutoSortEffect = head.addComponent('AutoSortEffect');
        // }

        let uiT = Util.searchNode(obj,'FightRoleUIPanel');
        if (uiT != null)
        {
            this.mObjs[FightRolePathType.UI] = uiT;
            uiT.active = true;
            
            //this.mHUDTextFactory = uiT._HUD.$HUDTextFactory;
            // if(this.mHUDTextFactory != null)
            //     this.mHUDTextFactory.InitFactory();
           
            let roleuicom = uiT.$FightRoleUIPanel;
            if(roleuicom)
            {
                roleuicom._Progress.active = campType == CampType.My;
                roleuicom._EnemyProgress.active = campType == CampType.Enemy;
                this.mObjs[FightRolePathType.Blood] = campType == CampType.My ? roleuicom._Progress : roleuicom._EnemyProgress;
            }
        }
        // let hudT = cc.find(hudPath,obj);
        // if (hudT != null)
        // {
        //     this.mObjs[FightRolePathType.Hud] = hudT;
        // }
    }

    Update(id,obj, campType,rolePath,hitPath,headPath,uiPath,bloodPath,enemyBloodPath,hudPath)
    {
        let parent = this.Get(FightRolePathType.Parent);
        if(!parent.isValid)
        {
            console.log(`Update  id = ${id}`);
            return;
        }
        let uiT = Util.searchNode(parent,'FightRoleUIPanel');
       
        if (uiT != null)
        {
            let roleuicom = uiT.$FightRoleUIPanel;
            if(roleuicom)
            {
                roleuicom._Progress.active = campType == CampType.My;
                roleuicom._EnemyProgress.active = campType == CampType.Enemy;
                this.mObjs[FightRolePathType.Blood] = campType == CampType.My ? roleuicom._Progress : roleuicom._EnemyProgress;
            }
        }
    }

    Get(type)
    {
        return this.mObjs[type];
    }

    GetBehaviour()
    {
        return this.mBehaviour;
    }

    GetHUDTextFactory()
    {
        return this.mHUDTextFactory;
    }

    GetAutoSortEffect()
    {
        return this.mAutoSortEffect;
    }
    
}
/// <summary>
/// 战斗模型缓存对象
/// </summary>
class FightRoleCacheObj
{
    constructor(id,name,model)
    {
        this.m_ID = id;
        this.m_Model = model;
        this.m_Name = name;
        this.m_Scale = model.scale;
        this.m_IsUse = true;
        this.m_FightRole = null;
    }

    AddFightRolePathObj(id,obj, campType,rolePath,hitPath,headPath,uiPath)
    {
        if(this.m_FightRole != null)
        {
            this.m_FightRole.Update(id, obj, campType, rolePath, hitPath, headPath, uiPath);
            return;
        }
        this.m_FightRole = new FightRolePathObj(id, obj, campType, rolePath, hitPath, headPath, uiPath);
    }

    SetUse(isUse)
    {
        this.m_IsUse = isUse;
        if (!this.m_IsUse)
        {
            this.m_Model.setPosition(-200, -200);
            if (this.m_FightRole == null)
            {
                return;
            }
            let rb = this.m_FightRole.mBehaviour;
            if (rb == null)
            {
                return;
            }
            rb.StopDie();
            rb.StopMove();
            rb.StopRepel();
            rb.StopTeleport();
            this.m_Model.scale  = this.m_Scale;
        }
        
    }
}

let mFightNeedModel = new Map();  //战斗需要的Model资源
let mLastFightModel = new Set(); //上次加载的Model资源
let mModelIDCounting = 0;//Model的ID的计数
let mTotalModelCache = new Map(); //新的模型缓存列表
////////////////////////////////////////模型相关
let mClearModelTime = 10;//清理模型次数，当战斗到达该次数清理一次模型
let mClearModelCounting = 0;

 ////////////////////////////////////////光效相关
let mFightNeedSpawnObj = new Map();//战斗需要的Effect对象池
let mLastFightEffect = new Map();   //上次加载的Effect资源
let mEffectIDCounting = 0;      //Effect的ID的计数
let mTotalEffect = new Map(); //存放所有已加载Effect的字典
   ////////////////////////////////////////战斗预设相关
//let mFightRoleUI;    //战斗角色UI，血条飘字
let FightGameObjectManager = cc.Class({
    extends: Thor,

    properties: {
        mFightRoleUI:cc.Prefab,
    },
    //定义静态成员
    statics:{
        _instance:null,
        getInstance : function()
        {
            return FightGameObjectManager._instance;
        }
    },
    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        FightGameObjectManager._instance = this;
        // cc.game.addPersistRootNode(this.node);
        // //游戏创建的Model的父对象
        // this.modelRoot = Util.searchNode(this.node,"ModelRoot");
        // this.this._EffectRoot = Util.searchNode(this.node,"EffectRoot");
        // //初始Model资源的父对象
        // this.sourceModelRoot = Util.searchNode(this.node,"SourceModelRoot");
    },

    Init() {
        // SafeLoader.safeLoadRes("uiprefab/FightRoleUIPanel",(err,prefab)=>{
        //     mFightRoleUI = prefab;
        // });
    },
  
    LoadModel(names,callback,loadID)
    {
        console.log(names.toString());
        let loadNames = new Set();
        names.forEach(([key, value]) => {
            loadNames.add(key);
        });
        //判断未加载的需要加载，获取loadNames和mFightNeedModel的差集
        let needLoad = new Set([...loadNames].filter(x => !mLastFightModel.has(x)));

        //判断已加载但是该场战斗不需要的就施放，获取mFightNeedModel和loadNames的差集
        let needDelete = new Set([...mLastFightModel].filter(x => !loadNames.has(x)));
        //清理本次战斗不需要的资源
        for(let name of needDelete)
        {
            if(mFightNeedModel.has(name))
            {
                let obj = mFightNeedModel.get(name);
                obj.destroy();
                mFightNeedModel.delete(name);
            }
            if(mLastFightModel.has(name))
            {
                mLastFightModel.delete(name);
            }
        }
        //清理模型资源
        mClearModelCounting++;
        if(mClearModelCounting > mClearModelTime)
        {
            mClearModelCounting = 0;
            let deleteModel = [];

            for(let obj of mTotalModelCache.values())
            {
                if(obj == null)
                {
                    continue;
                }
                if(obj.m_Model == null || !obj.m_Model.isValid)
                {
                    deleteModel.push(obj.m_ID);
                    continue;
                }
                // if (!needLoad.has(obj.m_Name))
                // {
                //     obj.m_FightRole = null;
                //     obj.m_Model.destroy();
                //     deleteModel.push(obj.m_ID);
                // }
            }

            for(let id of deleteModel)
            {
                mTotalModelCache.delete(id);
            }
        }
        ResourceManager.loadRole(needLoad,(objList)=>{
            for(let obj of objList)
            {
                obj.parent = this._SourceModelRoot;

                let uiRoot = cc.find("uiroot",obj);
                if(uiRoot)
                {
                    let newUI = cc.instantiate(this.mFightRoleUI);
                    newUI.parent = uiRoot;
                }

                //判断是否重复加载
                if (mFightNeedModel.has(obj.name))
                {
                    let o = mFightNeedModel.get(obj.name);
                    if (o != null)
                    {
                        o.destroy();
                    }
                    mFightNeedModel.delete(obj.name);
                }
                if (mLastFightModel.has(obj.name))
                {
                    mLastFightModel.delete(obj.name);
                }

                mFightNeedModel.set(obj.name, obj);
                mLastFightModel.add(obj.name);
            }
            if(callback)
            {
                callback(loadID);
            }
        });
    },
    //创建一个模型
    SpawnModel(name)
    {
        let cObj = this.GetFightRoleCache(name);
        if (cObj != null)
        {
            return cObj.m_ID;
        }
        let obj = this.CreateModel(name);
        obj.active = true;
        obj.parent = this._ModelRoot;
        obj.position = new cc.Vec2(-327, 250);
        obj.name = name;
        let id = ++mModelIDCounting;

        cObj = new FightRoleCacheObj(id, name, obj);
        if (mTotalModelCache.has(id))
        {
            mTotalModelCache.set(id,cObj);
        }else
        {
            mTotalModelCache.set(id, cObj);
        }
        return cObj.m_ID;
    },
    //从缓存中获取一个模型
    GetFightRoleCache(name)
    {
        for(let obj of mTotalModelCache.values())
        {
            if (obj.m_Name == name && !obj.m_IsUse && obj.m_Model != null)
            {
                obj.SetUse(true);
                return obj;
            }
        }
        return null;
    },
    //获取一个模型
    GetFightRole(id)
    {
        if (!mTotalModelCache.has(id))
        {
            return null;
        }
        return mTotalModelCache.get(id);
    },
    CreateModel(name)
    {
        let obj = null;
        if (mFightNeedModel.has(name))
        {
            obj = mFightNeedModel.get(name);
        }
        else
        {
            obj = new cc.Node();
        }
        return obj;
    },
    LoadEffect(names,callback,loadID)
    {
        if(callback)
        {
            callback(loadID);
        }
    },
    //追加加载光效
    LoadAppendEffect(names,luaFunc)
    {
        if (FightGameObjectManager.DebugEffect)
        {
            if (luaFunc != null)
                luaFunc.Call();
            return;
        }
        // List<string> needLoad = new List<string>();
        // for(let i = 0; i < names.Length; i++)
        // {
        //     name = names[i];
        //     if (mFightNeedSpawnObj.has(name))
        //     {
        //         continue;
        //     }
        //     needLoad.add(name);
        // }
        // needLoadNames = needLoad.ToArray();
        // LuaHelper.GetResManager().LoadEffect(needLoadNames, delegate (GameObject[] objs) {

        //     for(let i = 0; i < objs.Length; i++)
        //     {
        //         name = needLoadNames[i];
        //         let obj = objs[i] as GameObject;
        //         //光效特殊处理，需要加上动画和粒子的加速代码
        //         obj.AddComponent<ParticleSystemHelper>();
        //         obj.AddComponent<AnimationHelper>();
        //         let root = new cc.Node();
        //         root.name = name;
        //         obj.name = name;
        //         obj.transform.SetParent(root.transform);
        //         LuaHelper.GetResManager().SetDontDestroyOnLoad(root);
        //         obj.active = true;
        //         root.SetActive(false);
        //         root.transform.SetParent(this._SourceEffectRoot);

        //         //判断是否重复加载
        //         if (mFightNeedSpawnObj.has(name))
        //         {
        //             FightSpawnList fsl1 = mFightNeedSpawnObj[name];
        //             fsl1.Destroy();
        //             mFightNeedSpawnObj.Remove(name);
        //         }
        //         if (mLastFightEffect.has(name))
        //         {
        //             mLastFightEffect.Remove(name);
        //         }
        //         mLastFightEffect.add(name,true);
        //         FightSpawnList fsl2 = new FightSpawnList(name, SpawnCount, root, this._EffectRoot);
        //         mFightNeedSpawnObj.add(name, fsl2);
        //     }
        //     if (luaFunc != null)
        //         luaFunc.Call();
        // });
    },

    //创建一个光效
    SpawnEffect( name)
    {
        if ( name == "")
        {
            return -1;
        }
        let obj = this.CreateEffect(name);
        // Transform t = obj.transform;
        // t.localEulerAngles = Vector3.zero;
        // t.localScale = Vector3.one;
        // obj.active = true;
        // obj.transform.SetParent(this._EffectRoot);
        let id = ++mEffectIDCounting;
        mTotalEffect.set(id,obj);
        return id;
    },

    GetFightSpawnList(name)
    {
        if (FightGameObjectManager.DebugEffect)
        {
            return null;
        }
        if (!mFightNeedSpawnObj.has(name))
        {
            return null;
        }
        return mFightNeedSpawnObj[name];
    },

    CreateEffect(name)
    {
        let obj = null;
        if (FightGameObjectManager.DebugEffect)
        {
            obj = new cc.Node();
            obj.addComponent('AutoDestroy');
        }
        else if (!mFightNeedSpawnObj.has(name))
        {
            obj = new cc.Node();
            obj.addComponent('AutoDestroy');
        }
        else
        {
            let fsl = mFightNeedSpawnObj[name];
            obj = fsl.Spawn();
        }
       

        obj.active = true;
        return obj;
    },

    //创建一个在世界坐标的光效
    SpawnEffectInWorldPlace(name,x,y,z ,useEularAngles,eularAnglesX,eularAnglesY,eularAnglesZ)
    {
        if (name == "")
        {
            return -1;
        }
        let obj = this.CreateEffect(name);

        let id = ++mEffectIDCounting;
        mTotalEffect.set(id,obj);

        // Transform t = obj.transform;
        // t.position = new Vector3(x, y, z);
        // t.localEulerAngles = Vector3.zero;
        // t.localScale = Vector3.one;
        // if (useEularAngles)
        // {
        //     t.localEulerAngles = new Vector3(eularAnglesX, eularAnglesY, eularAnglesZ);
        // }

        // obj.active = true;
        return id;
    },

    //创建一个有父对象的光效
    SpawnEffectInParent(name,modelID,path, useEularAngles,eularAnglesX,eularAnglesY,eularAnglesZ)
    {
        if (name == "")
        {
            return -1;
        }
        let root = this.GetFightRoleObj(modelID, path);
        if(root == null)
        {
            return -1;
        }

        let obj = this.CreateEffect(name);
        id = ++mEffectIDCounting;
        mTotalEffect.set(id,obj);

        // Transform t = obj.transform;
        // t.SetParent(root.transform);
        // t.localPosition = Vector3.zero;
        // t.localEulerAngles = Vector3.zero;
        // t.localScale = Vector3.one;
        // if (useEularAngles)
        // {
        //     t.localEulerAngles = new Vector3(eularAnglesX, eularAnglesY, eularAnglesZ);
        // }
        // obj.active = true;
        return id;
    },

    //创建一个会自动销毁的光效
    SpawnEffectAutoDestroy(name,modelID,path,duration, useEularAngles,eularAnglesX,eularAnglesY,eularAnglesZ)
    {
        if (name == "")
        {
            return -1;
        }
        let root = this.GetFightRoleObj(modelID, path);
        if (root == null)
        {
            return -1;
        }

        let obj = this.CreateEffect(name);

        id = ++mEffectIDCounting;
        mTotalEffect.set(id,obj);

        // Transform t = obj.transform;
        // t.SetParent(root.transform);
        // t.localPosition = Vector3.zero;
        // t.localEulerAngles = Vector3.zero;
        // t.localScale = Vector3.one;
        // if (useEularAngles)
        // {
        //     t.localEulerAngles = new Vector3(eularAnglesX, eularAnglesY, eularAnglesZ);
        // }

        let ad = obj.getComponent('AutoDestroy');
        if (ad == null)
        {
            ad = obj.addComponent('AutoDestroy');
        }
        ad.SetDelayTime(name, duration);
        obj.active = true;
        return id;
    },

    //创建一个会自动销毁的光效，其位置在世界
    SpawnEffectAutoDestroyInWorldPlace(name,x,y,z,duration, useEularAngles,eularAnglesX,eularAnglesY,eularAnglesZ)
    {
        let obj = this.CreateEffect(name);

        name = ++mEffectIDCounting;
        mTotalEffect.set(name,obj);

        // Transform t = obj.transform;
        // t.position = new Vector3(x, y, z);
        // t.localEulerAngles = Vector3.zero;
        // t.localScale = Vector3.one;
        // if (useEularAngles)
        // {
        //     t.localEulerAngles = new Vector3(eularAnglesX, eularAnglesY, eularAnglesZ);
        // }
        let ad = obj.getComponent('AutoDestroy');
        if (ad == null)
        {
            ad = obj.addComponent('AutoDestroy');
        }
        if (ad == null) 
        {
            console.error(">>>没有添加脚本")
            return;
        }
        ad.SetDelayTime(name, duration);

        obj.active = true;
        return id;
    },

    //创建光效接口
   SpwanEffectInFightRoleByLua(id, name,type,duration, useEularAngles,eularAnglesX,eularAnglesY,eularAnglesZ)
    {
        return this.SpwanEffectInFightRole(id, name, type, duration, useEularAngles, eularAnglesX, eularAnglesY, eularAnglesZ);
    },

    //创建光效
    SpwanEffectInFightRole(id, name,type,duration, useEularAngles,eularAnglesX,eularAnglesY,eularAnglesZ)
    {
        if (name == "")
        {
            return -1;
        }
        switch (type)
        {
            case EffectRootType.Head:
                if (duration > 0)
                {
                    return this.SpawnEffectAutoDestroy(name, id, FightRolePathType.Head, duration, useEularAngles, eularAnglesX, eularAnglesY, eularAnglesZ);
                }
                else
                {
                    return this.SpawnEffectInParent(name, id, FightRolePathType.Head, useEularAngles, eularAnglesX, eularAnglesY, eularAnglesZ);
                }
                
            case EffectRootType.Hit:
                if (duration > 0)
                {
                    return this.SpawnEffectAutoDestroy(name, id, FightRolePathType.Hit, duration, useEularAngles, eularAnglesX, eularAnglesY, eularAnglesZ);
                }
                else
                {
                    return this.SpawnEffectInParent(name, id, FightRolePathType.Hit, useEularAngles, eularAnglesX, eularAnglesY, eularAnglesZ);
                }
            case EffectRootType.Root:
                if (duration > 0)
                {
                    return this.SpawnEffectAutoDestroy(name, id, FightRolePathType.Parent, duration, useEularAngles, eularAnglesX, eularAnglesY, eularAnglesZ);
                }
                else
                {
                    return this.SpawnEffectInParent(name, id, FightRolePathType.Parent, useEularAngles, eularAnglesX, eularAnglesY, eularAnglesZ);
                }

            case EffectRootType.HeadWorld:
                let head = this.GetFightRoleObj(id, FightRolePathType.Head);
                if(head == null)
                {
                    return -1;
                }
                let headPos = head.transform.position;
                if (duration > 0)
                {
                    return this.SpawnEffectAutoDestroyInWorldPlace(name, headPos.x, headPos.y, headPos.z, duration, useEularAngles, eularAnglesX, eularAnglesY, eularAnglesZ);
                }
                else
                {
                    return this.SpawnEffectInWorldPlace(name, headPos.x, headPos.y, headPos.z, useEularAngles, eularAnglesX, eularAnglesY, eularAnglesZ);
                }
            case EffectRootType.HitWorld:
                let hit = this.GetFightRoleObj(id, FightRolePathType.Hit);
                if (hit == null)
                {
                    return -1;
                }
                let hitPos = hit.transform.position;
                if (duration > 0)
                {
                    return this.SpawnEffectAutoDestroyInWorldPlace(name, hitPos.x, hitPos.y, hitPos.z, duration, useEularAngles, eularAnglesX, eularAnglesY, eularAnglesZ);
                }
                else
                {
                    return this.SpawnEffectInWorldPlace(name, hitPos.x, hitPos.y, hitPos.z, useEularAngles, eularAnglesX, eularAnglesY, eularAnglesZ);
                }
            case EffectRootType.RootWorld:
                let root = this.GetFightRoleObj(id, FightRolePathType.Parent);
                if (root == null)
                {
                    return -1;
                }
                let rootPos = root.transform.position;
                if (duration > 0)
                {
                    return this.SpawnEffectAutoDestroyInWorldPlace(name, rootPos.x, rootPos.y, rootPos.z, duration, useEularAngles, eularAnglesX, eularAnglesY, eularAnglesZ);
                }
                else
                {
                    return this.SpawnEffectInWorldPlace(name, rootPos.x, rootPos.y, rootPos.z, useEularAngles, eularAnglesX, eularAnglesY, eularAnglesZ);
                }
            case EffectRootType.RootWorldYZero:
                let root2 = this.GetFightRoleObj(id, FightRolePathType.Parent);
                if (root2 == null)
                {
                    return -1;
                }
                let rootPos2 = root2.position;
                if (duration > 0)
                {
                    return this.SpawnEffectAutoDestroyInWorldPlace(name, rootPos2.x, 0, rootPos2.z, duration, useEularAngles, eularAnglesX, eularAnglesY, eularAnglesZ);
                }
                else
                {
                    return this.SpawnEffectInWorldPlace(name, rootPos2.x, 0, rootPos2.z, useEularAngles, eularAnglesX, eularAnglesY, eularAnglesZ);
                }
        }
        return -1;
    },

    //销毁一个光效
    DespawnEffect(name,id)
    {
        if (!mTotalEffect.has(id))
        {
            return;
        }

        let obj = mTotalEffect[id];
        // if(obj != null)
        // {
        //     AutoDestroy ad = obj.getComponent('AutoDestroy');
        //     ad.Release();
        // }
        mTotalEffect.Remove(id);
    },
    
    //释放所有已加载的模型和光效
    ReleaseAll()
    {

        for (let [key,value] of mTotalModelCache)
        {
            let obj = value;
            if(obj.m_Model != null && obj.m_Model.isValid)
            {
                obj.SetUse(false);
            }
        }
        
        for (let value of mTotalEffect.values())
        {
            let obj = value;
            if (obj == null)
            {
                continue;
            }

            let ad = obj.getComponent('AutoDestroy');
            ad.release();
        }
        
        mTotalEffect.clear();

        for (let [key,value] of mFightNeedSpawnObj)
        {
            value.Release();
        }
    },

    //////////////////////////////////////////////////////主动技
    StartASkill(effectName,duration, 
        modelName, modelParent,modelParentAniName, aniName,modelEffectName,
        effectParent,modelEffectDuration, camera)
    {
        //播放主动技光效
        if (effectName != "")
        {
            id = this.SpawnEffect(effectName);
            let effect = this.GetEffect(id);
            if(effect != null)
            {
                // Transform effectT = effect.transform;
                // if (effectParent != null)
                // {
                //     effectT.SetParent(effectParent.transform);
                //     effectT.localPosition = Vector3.zero;
                //     effectT.localEulerAngles = Vector3.zero;
                //     effectT.localScale = Vector3.one;
                // }
                let ad = effect.getComponent('AutoDestroy');
                if (ad == null)
                {
                    ad = effect.addComponent('AutoDestroy');
                }
                ad.SetDelayTime(effectName, duration);
            }
           
        }
         
        //显示主动技模型
        if (modelName !="" && modelParent != null)
        {
            if (mFightNeedModel.has(modelName))
            {
                let obj = this.CreateModel(modelName);
                let ad = obj.getComponent('AutoDestroy');
                if (ad == null)
                {
                    ad = obj.addComponent('AutoDestroy');
                }
                ad.SetDelayTime(modelName, duration);
                // Transform t = obj.transform;
                // t.SetParent(modelParent.transform);
                // t.localScale = Vector3.one;
                // t.localEulerAngles = Vector3.zero;
                // t.localPosition = Vector3.zero;
                // Transform child = obj.transform.GetChild(0);
                // if(child != null)
                // {
                //     GameObjectTool.PlayAni(child.gameObject, aniName, 1, WrapMode.Once);
                // }
                // obj.active = true;

                // modelParent.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                // GameObjectTool.PlayAni(obj, modelParentAniName, 1, WrapMode.Once);
            }
            
        }
        //播放主动技模型光效
        if ( modelEffectName != "" && modelParent != null)
        {
            let obj = this.CreateEffect(modelEffectName);

            id = ++mEffectIDCounting;
            mTotalEffect.set(id,obj);

            // Transform t = obj.transform;
            // t.SetParent(modelParent.transform);
            // t.localPosition = Vector3.zero;
            // t.localEulerAngles = Vector3.zero;
            // t.localScale = Vector3.one;

            let ad = obj.getComponent('AutoDestroy');
            if (ad == null)
            {
                ad = obj.addComponent('AutoDestroy');
            }
            ad.SetDelayTime(modelEffectName, modelEffectDuration);
            obj.active = true;
        }

        if(camera != null)
        {
            // ShakeCamera sc = camera.getComponent<ShakeCamera>();
            // sc.SetCamera(camera);
            // sc.Shake(10, 10);
        }
    },


    ///////////////////////////////////////////////////////工具函数

    //设置模型次数
    SetClearModelTime(value)
    {
        mClearModelTime = value;
    },

    //根据ID获取GameObject的父对象
    GetModelParent(id)
    {
        return this.GetFightRoleObj(id, FightRolePathType.Parent);
    },
    
    //设置FightRole
    AddFightRoleObj(id,campType, rolePath, hitPath, headPath,uiPath)
    {
        let obj = this.GetFightRole(id);
        if (obj == null || obj.m_Model == null || !obj.m_Model.isValid)
        {
            return;
        }
        obj.AddFightRolePathObj(id, obj.m_Model, campType, rolePath, hitPath, headPath, uiPath);
    },

    GetModel(id)
    {
        if (!mTotalModelCache.has(id))
        {
            return null;
        }
        return mTotalModelCache[id].m_Model;
    },


    GetEffect(id)
    {
        if (!mTotalEffect.has(id))
        {
            return null;
        }
        return mTotalEffect[id];
    },
    //获取FightRole的对象
    GetFightRoleObj(id, type)
    {
        if (!mTotalModelCache.has(id))
        {
            return null;
        }
        let cObj = this.GetFightRole(id);
        if (cObj == null || cObj.m_Model == null || !cObj.m_Model.isValid)
        {
            return null;
        }

        let obj = cObj.m_FightRole;
        if(obj == null)
        {
            return null;
        }

        return obj.Get(type);
    },

    //获取FightRole的RoleBehaviour对象
    GetFightRoleBehaviour(id)
    {
        if (!mTotalModelCache.has(id))
        {
            return null;
        }
        let cObj = this.GetFightRole(id);
        if (cObj == null || cObj.m_Model == null || !cObj.m_Model.isValid)
        {
            return null;
        }

        let obj = cObj.m_FightRole;
        if (obj == null)
        {
            return null;
        }

        return obj.GetBehaviour();
    },

    GetFightRoleHUDTextFactory(id)
    {
        if (!mTotalModelCache.has(id))
        {
            return null;
        }
        let cObj = this.GetFightRole(id);
        if (cObj == null || cObj.m_Model == null || !cObj.m_Model.isValid)
        {
            return null;
        }

        let obj = cObj.m_FightRole;
        if (obj == null)
        {
            return null;
        }

        return obj.GetHUDTextFactory();
    },
    // 设置FightRole的Vector
    SetFightRoleVector(id, pathType, type,x,y,z,w)
    {

        let obj = this.GetFightRoleObj(id,pathType);
        if (obj == null)
        {
            return;
        }
        // let t = obj.transform;
        // RectTransform rt = null;
        // switch (type)
        // {
        //     case TransformOperateType.Position:
        //         t.position = new cc.Vec2(x, y, z);
        //         break;
        //     case TransformOperateType.Rotation:
        //         t.rotation = new Quaternion(x, y, z, w);
        //         break;
        //     case TransformOperateType.EulerAngles:
        //         t.eulerAngles = new cc.Vec2(x, y, z);
        //         break;
        //     case TransformOperateType.LocalPosition:
        //         t.localPosition = new cc.Vec2(x, y, z);
        //         break;
        //     case TransformOperateType.LocalRotation:
        //         t.localRotation = new Quaternion(x, y, z, w);
        //         break;
        //     case TransformOperateType.LocalEulerAngles:
        //         t.localEulerAngles = new cc.Vec2(x, y, z);
        //         break;
        //     case TransformOperateType.LocalScale:
        //         t.localScale = new cc.Vec2(x, y, z);
        //         break;
        //     case TransformOperateType.RTPosition:
        //         rt = t.getComponent<RectTransform>();
        //         rt.anchoredPosition3D = new cc.Vec2(x, y, z);
        //         break;
        //     case TransformOperateType.RTSizeDelta:
        //         rt = t.getComponent<RectTransform>();
        //         rt.sizeDelta = new Vector2(x, y);
        //         break;
        //     case TransformOperateType.RTPivot:
        //         rt = t.getComponent<RectTransform>();
        //         rt.pivot = new Vector2(x, y);
        //         break;
        // }
    },

    // 获取FightRole的Vector
    GetFightRoleVector(id, pathType, type)
    {
        let x = 0;
        let y = 0;
        
        let obj = this.GetFightRoleObj(id, pathType);
        if (obj == null)
        {
            return [x,y];
        }
        return [obj.x,obj.y];
        // let t = obj.transform;
        // RectTransform rt = null;
        // cc.Vec2 v = cc.Vec2.zero;
        // switch (type)
        // {
        //     case TransformOperateType.Position:
        //         v = t.position;
        //         x = v.x;
        //         y = v.y;
        //         z = v.z;
        //         break;
        //     case TransformOperateType.Rotation:
        //         Quaternion q = t.rotation;
        //         x = q.x;
        //         y = q.y;
        //         z = q.z;
        //         w = q.w;
        //         break;
        //     case TransformOperateType.EulerAngles:
        //         v = t.eulerAngles;
        //         x = v.x;
        //         y = v.y;
        //         z = v.z;
        //         break;
        //     case TransformOperateType.LocalPosition:
        //         v = t.localPosition;
        //         x = v.x;
        //         y = v.y;
        //         z = v.z;
        //         break;
        //     case TransformOperateType.LocalRotation:
        //         Quaternion q2 = t.localRotation;
        //         x = q2.x;
        //         y = q2.y;
        //         z = q2.z;
        //         w = q2.w;
        //         break;
        //     case TransformOperateType.LocalEulerAngles:
        //         v = t.localEulerAngles;
        //         x = v.x;
        //         y = v.y;
        //         z = v.z;
        //         break;
        //     case TransformOperateType.LocalScale:
        //         v = t.localScale;
        //         x = v.x;
        //         y = v.y;
        //         z = v.z;
        //         break;
        //     case TransformOperateType.RTPosition:
        //         rt = t.getComponent<RectTransform>();
        //         v = rt.anchoredPosition3D;
        //         x = v.x;
        //         y = v.y;
        //         z = v.z;
        //         break;
        //     case TransformOperateType.RTSizeDelta:
        //         rt = t.getComponent<RectTransform>();
        //         v = rt.sizeDelta;
        //         x = v.x;
        //         y = v.y;
        //         break;
        //     case TransformOperateType.RTPivot:
        //         rt = t.getComponent<RectTransform>();
        //         v = rt.pivot;
        //         x = v.x;
        //         y = v.y;
        //         break;
        // }
    },


    //设置战斗模型血量
    SetFightRoleHP(id,percent)
    {
        let obj = this.GetFightRoleObj(id, FightRolePathType.Blood);
        if (obj == null)
        {
            return;
        }
        obj.$Sprite.fillRange = percent;
        // Image image = obj.getComponent<Image>();
        // if (image == null)
        // {
        //     return;
        // }
        // image.fillAmount = percent;
    },

    //设置战斗模型出生
    SetFightRoleBorn(id, camp, x,y,z, camera,bornEffect, bornEffectParent,bornEffectDuration,bornScaleDutation)
    {
        //设置出生位置
        let parent = this.GetFightRoleObj(id, FightRolePathType.Parent);
        if (parent != null)
        {
            parent.position = new cc.Vec2(x, y);
            if (camp == CampType.Enemy)
            {
                parent.scaleX = -1;
            }
        }

        //设置方向
        //设置伸缩表现
        //let role = this.GetFightRoleObj(id, FightRolePathType.Role);
        // if (role != null)
        // {
        //     let roleT = role.transform;
        //     cc.Vec2 eulerAngles = roleT.eulerAngles;
        //     eulerAngles.y = 0;
        //     if (camp == (int)CampType.Enemy)
        //     {
        //         eulerAngles.y = 180;
        //     }
        //     roleT.eulerAngles = eulerAngles;

        //     cc.Vec2 scale = roleT.localScale;
        //     ScaleTool st = role.getComponent<ScaleTool>();
        //     if (st != null)
        //     {
        //         st.Show(role, 0, 0, 0, scale.x, scale.y, scale.z, bornScaleDutation);
        //     }
        // }

        //设置UI面对镜头
        //SetFightRoleLookAtCamera(id, camera);

        //设置出生光效
        //SpawnEffectAutoDestroy(bornEffect, id, (FightRolePathType)bornEffectParent, bornEffectDuration, false, 0, 0, 0);
    },

    //设置Boss战斗模型出生
    SetFightBossBorn(id, camp,x,y,z, camera,bornEffect, bornEffectParent,bornEffectDuration)
    {
        //设置出生位置
        let parent = this.GetFightRoleObj(id, FightRolePathType.Parent);
        if (parent != null)
        {
            parent.position = new cc.Vec2(x, y);

            if (camp == CampType.Enemy)
            {
                parent.scaleX = -1;
            }
        }

        //设置方向
        let role = this.GetFightRoleObj(id, FightRolePathType.Role);
        if (role != null)
        {
            // let roleT = role.transform;
            // cc.Vec2 eulerAngles = roleT.eulerAngles;
            // eulerAngles.y = 0;
            // if (camp == (int)CampType.Enemy)
            // {
            //     eulerAngles.y = 180;
            // }
            // roleT.eulerAngles = eulerAngles;
        }

        //设置UI面对镜头
        //SetFightRoleLookAtCamera(id, camera);

        //设置出生光效
        //SpawnEffectAutoDestroy(bornEffect, id, (FightRolePathType)bornEffectParent, bornEffectDuration, false, 0, 0, 0);
    },

    //设置战斗模型复活
    SetFightRoleRespawn(id, camp, x,y,z, camera,bornEffect, bornEffectParent,bornEffectDuration)
    {
        //设置出生位置
        let parent = this.GetFightRoleObj(id, FightRolePathType.Parent);
        if (parent != null)
        {
            parent.position = new cc.Vec2(x, y);
        }
        
        //设置方向
        let role = this.GetFightRoleObj(id, FightRolePathType.Role);
        if (role != null)
        {
            // let roleT = role.transform;
            // cc.Vec2 eulerAngles = roleT.eulerAngles;
            // eulerAngles.y = 0;
            // if (camp == (int)CampType.Enemy)
            // {
            //     eulerAngles.y = 180;
            // }
            // roleT.eulerAngles = eulerAngles;
        }

        //设置UI面对镜头
        //SetFightRoleLookAtCamera(id, camera);

        //播放复活光效
        //SpawnEffectAutoDestroy(bornEffect, id, (FightRolePathType)bornEffectParent, bornEffectDuration, false, 0, 0, 0);
    },

    //停止移动
    StopFightRoleMove(id)
    {
        let rb = this.GetFightRoleBehaviour(id);
        if(rb == null)
        {
            return;
        }
        rb.StopMove();
    },

    //设置击飞击退状态
    SetFightRoleRepel(id,height,length,duration)
    {
        let rb = this.GetFightRoleBehaviour(id);
        if (rb == null)
        {
            return;
        }
        rb.Repel(height, length, duration);
    },

    //停止击飞击退状态
    StopFightRoleRepel(id)
    {
        let rb = this.GetFightRoleBehaviour(id);
        if (rb == null)
        {
            return;
        }
        rb.StopRepel();
    },
    //播放受击光效和音效
    SetFightRolePlayHurt(id,hurtEffectName, hurtEffectParent,hurtEffectDuration,
       dieEffectName, dieEffectParent,dieEffectDuration,
       soundName,soundVolume,bloodProgress)
    {
        //播放受击光效
        //this.SpwanEffectInFightRole(id, hurtEffectName, hurtEffectParent, hurtEffectDuration, false, 0, 0, 0);
        //播放受击结束光效
        //this.SpwanEffectInFightRole(id, dieEffectName, dieEffectParent, dieEffectDuration, false, 0, 0, 0);
        //播放复活音效
        // if(soundName != "")
        //     LuaHelper.GetSoundManager().PlayEffect(soundName, soundVolume);
        //修改血条
        this.SetFightRoleHP(id, bloodProgress);
    },

    // 判断是否携带镜头
    IsFightRoleTakeCemera(id,cameraPath)
    {
        let parent = this.GetFightRoleObj(id, FightRolePathType.Parent);
        if(parent == null)
        {
            return false;
        }
        // if(parent.FindChild(cameraPath) == null)
        // {
        //     return false;
        // }
        return false;
    },

    //隐身
    SetFightRoleTransparent(id ,flag)
    {
        let role = this.GetFightRoleObj(id, FightRolePathType.Role);
        if(role == null)
        {
            return;
        }
        let tm = role.getComponent("TransparentModel");
        if (tm == null)
        {
            tm = role.AddComponent("TransparentModel");
        }

        if (flag)
        {
            tm.Transparent();
        }
        else
        {
            tm.Resume();
        }
    },

    //石化
    SetFightRolePetrifaction(id,flag)
    {
        let role = this.GetFightRoleObj(id, FightRolePathType.Role);
        if (role == null)
        {
            return;
        }
        let tm = role.getComponent("TransparentModel");
        if (tm == null)
        {
            tm = role.AddComponent("TransparentModel");
        }

        if (flag)
        {
            tm.Petrifaction();
        }
        else
        {
            tm.ResumePetrifaction();
        }
    },


    //播放动画
    SetFightRolePlayAni(id,aniName,speed,mode)
    {
        let role = this.GetFightRoleObj(id, FightRolePathType.Role);
        if(role == null)
        {
            return;
        }
        let ani = role.getComponentInChildren(dragonBones.ArmatureDisplay);
        if (ani == null)
        {
            return;
        }
        // AnimationState aniStat = ani[aniName];
        // if (aniStat == null)
        // {
        //     return;
        // }
        // aniStat.wrapMode = mode;
        // aniStat.speed = speed;
        ani.playAnimation(aniName);
    },

    //判断是否在播放动画
    IsFightRolePlaying(id,aniName)
    {
        let role = this.GetFightRoleObj(id, FightRolePathType.Role);
        if (role == null)
        {
            return false;
        }
        let ani = role.getComponentInChildren(dragonBones.ArmatureDisplay);
        if (ani == null)
        {
            return false;
        }
        return ani.animationName == aniName;
    },

    //停止播放动画
    StopFightRoleAni(id)
    {
        let rb = this.GetFightRoleBehaviour(id);
        if (rb == null)
        {
            return;
        }
        rb.StopAni();
    },

    //飘字
    HUDText(id,content, type,scaleTime,speed,duration)
    {
        let htf = this.GetFightRoleHUDTextFactory(id);
        if(htf == null)
        {
            return;
        }
        htf.Create(content, type, scaleTime, speed, duration);
    },

    //面对镜头
    SetFightRoleLookAtCamera(id,camera)
    {
        //设置UI面对镜头
        let ui = this.GetFightRoleObj(id, FightRolePathType.UI);
        if (ui == null)
        {
            return;
        }
        // LookAt la = ui.getComponent<LookAt>();
        // if (la == null)
        // {
        //     return;
        // }
        // la.target = camera;
    },

    SetFightRoleArrLookAtCamera( ids, camera)
    {
        for(let i = 0; i < ids.Length; i++)
        {
            id = ids[i];
            this.SetFightRoleLookAtCamera(id, camera);
        }
    },

    //开始闪现
    SetFightRoleTeleport(id,delay,x,y,z,effectName, rootType,effectDuration)
    {
        let rb = this.GetFightRoleBehaviour(id);
        if (rb == null)
        {
            return;
        }
        rb.Teleport(delay, x, y, z, effectName, rootType, effectDuration);
    },

    //结束闪现
    StopFightRoleTeleport(id)
    {
        let rb = this.GetFightRoleBehaviour(id);
        if(rb == null)
        {
            return;
        }
        rb.StopTeleport();
    },
    PlayFightBossBornAni(id, childPath, roleAniName,modelAniName, speed, mode,success, duration)
    {
        duration = 0;
        success = false;
        let role = this.GetFightRoleObj(id, FightRolePathType.Role);
        if(role == null)
        {
            return [success,duration];
        }
        return [success,duration];
        // let modelRootT = role.FindChild(childPath);
        // if(modelRootT == null)
        // {
        //     return [success,duration];
        // }
        // GameObjectTool.PlayAni(role, roleAniName, speed, mode);
        // duration = GameObjectTool.GetAniTime(role, roleAniName);
        // success = true;
        // if (modelRootT.childCount <= 0)
        // {
        //     return;
        // }
        // let model = modelRootT.GetChild(0);
        // if (model == null)
        // {
        //     return;
        // }
        // if (!GameObjectTool.IsPlayingAni(model.gameObject, modelAniName))
        // {
        //     GameObjectTool.PlayAni(model.gameObject, modelAniName, speed, mode);
        // }
        // duration = GameObjectTool.GetAniTime(model.gameObject, modelAniName);
    },

    /////////////////////////////////////////////////////状态
    //攻击表现
    SetFightRoleAttackBehavior(id,lookAtTarget, targetID, aniName,aniSpeed,mode, effectName,duration, rootType,soundName)
    {
        let masterRole = this.GetFightRoleObj(id, FightRolePathType.Role);
        if(masterRole == null)
        {
            return;
        }
        let masterRoleT = masterRole.transform;
        let targetRole = this.GetFightRoleObj(targetID, FightRolePathType.Role);
        if (targetRole == null)
        {
            return;
        }
        // let targetRoleT = targetRole.transform;
        // //设置攻击方的方向
        // if (lookAtTarget)
        // {
        //     GameObjectTool.LookAt(masterRole, targetRole);
        // }else
        // {
        //     let orienEulerAngles = masterRoleT.eulerAngles;
        //     let eY = orienEulerAngles.y % 360;
        //     if((eY >=0 && eY <90)||(eY >= 270 && eY <= 360))
        //     {
        //         orienEulerAngles.y = 0;
        //     }else
        //     {
        //         orienEulerAngles.y = 180;
        //     }
        //     masterRoleT.eulerAngles = orienEulerAngles;
        // }

        // //攻击动作
        this.SetFightRolePlayAni(id, aniName, aniSpeed, mode);

        // //攻击光效
        // //获取光效旋转值
        // let targetPos = targetRole.position;
        // let masterPos = masterRole.position;
        // let delta = targetPos - masterPos;
        // let eulerAngles = Quaternion.FromToRotation(Vector3.back, delta).eulerAngles;
        // //只旋转Y轴
        // eulerAngles.x = 0;
        // eulerAngles.z = 0;
        // SpwanEffectInFightRole(id, effectName,rootType, duration, true, eulerAngles.x, eulerAngles.y, eulerAngles.z);
        
        // //攻击音效
        // if (!string.IsNullOrEmpty(soundName))
        // {
        //     LuaHelper.GetSoundManager().PlayEffect(soundName);
        // }
        
    },

    //攻击表现 是否修改X轴的方向
    SetFightRoleAttackBehavior(id, lookAtTarget, targetID,aniName,aniSpeed,mode,effectName,duration, rootType,soundName, useX)
    {
        let masterRole = this.GetFightRoleObj(id, FightRolePathType.Role);
        if (masterRole == null)
        {
            return;
        }
        let masterRoleT = masterRole.transform;
        let targetRole = this.GetFightRoleObj(targetID, FightRolePathType.Role);
        if (targetRole == null)
        {
            return;
        }
        // let targetRoleT = targetRole.transform;
        // //设置攻击方的方向
        // if (lookAtTarget)
        // {
        //     GameObjectTool.LookAt(masterRole, targetRole);
        // }
        // else
        // {
        //     let orienEulerAngles = masterRoleT.eulerAngles;
        //    eY = orienEulerAngles.y % 360;
        //     if ((eY >= 0 && eY < 90) || (eY >= 270 && eY <= 360))
        //     {
        //         orienEulerAngles.y = 0;
        //     }
        //     else
        //     {
        //         orienEulerAngles.y = 180;
        //     }
        //     masterRoleT.eulerAngles = orienEulerAngles;
        // }

        // //攻击动作
        this.SetFightRolePlayAni(id, aniName, aniSpeed, mode);

        // //攻击光效
        // //获取光效旋转值
        // let targetPos = targetRole.position;
        // let masterPos = masterRole.position;
        // if (!useX)
        // {
        //     targetPos.x = masterPos.x;
        // }
        // let delta = targetPos - masterPos;
        // let eulerAngles = Quaternion.FromToRotation(Vector3.back, delta).eulerAngles;
        // //只旋转Y轴
        // eulerAngles.x = 0;
        // eulerAngles.z = 0;
        // SpwanEffectInFightRole(id, effectName,rootType, duration, true, eulerAngles.x, eulerAngles.y, eulerAngles.z);

        // //攻击音效
        // if (!string.IsNullOrEmpty(soundName))
        // {
        //     LuaHelper.GetSoundManager().PlayEffect(soundName);
        // }

    },
    SetFightRoleDieBehavior(id, effectName, isBoss, rootType,duration,missTime,aniName, rolePath,speed,mode, rewardEffect)
    {
        let rb = this.GetFightRoleBehaviour(id);
        if (rb == null)
        {
            return -1;
        }
       
        return rb.Die(effectName, isBoss, rootType, duration, missTime,
             aniName, rolePath, speed, mode, rewardEffect);
    },

    //死亡操作 弃用
    SetFightRoleDie(id)
    {

    },

    StopFightRoleDieBehavior(id)
    {
        let rb = this.GetFightRoleBehaviour(id);
        if (rb == null)
        {
            return;
        }
        rb.StopDie();
    },

    SetFightRoleStandBehavior(id, standAniName,speed,mode, childPath)
    {
       
        let rb = this.GetFightRoleBehaviour(id);
        if (rb == null)
        {
            return;
        }
        rb.StopMove();
        rb.StopRepel();

        this.SetFightRolePlayAni(id, standAniName, speed, mode);

        if (childPath == "")
        {
            return;
        }

        let role = this.GetFightRoleObj(id, FightRolePathType.Role);
        if (role == null)
        {
            return;
        }
        // let modelRoot = role.FindChild(childPath);
        // if (modelRoot == null || modelRoot.childCount <= 0)
        // {
        //     return;
        // }
        // let model = modelRoot.GetChild(0);
        // if(model != null)
        // {
        //     GameObjectTool.PlayAni(model.gameObject, standAniName, speed, mode);
        // }
    },

    SetFightRoleMoveBehavior(id, runAniName,speed,mode,x,y,z)
    {
     
        //设置方向
        let role = this.GetFightRoleObj(id, FightRolePathType.Role);
        if(role == null)
        {
            return;
        }

        // let roleT = role.transform;
        // let position = roleT.position;
        // let eulerAngles = roleT.eulerAngles;
        // eulerAngles.y = 0;
        // if (position.z >= z)
        // {
        //     eulerAngles.y = 180;
        // }
        // role.eulerAngles = eulerAngles;

        //播放动画
        if (!this.IsFightRolePlaying(id, runAniName))
        {
            this.SetFightRolePlayAni(id, runAniName, speed, mode);
        }
    },

    SetFightRoleMoveBehaviorRun(id,runSpeed,frequency,realSpeed,x,y)
    {
        let rb = this.GetFightRoleBehaviour(id);
        if (rb == null)
        {
            return;
        }
        //设置方向
        let role = this.GetFightRoleObj(id, FightRolePathType.Role);
        if (role == null)
        {
            return;
        }
        // let roleT = role.transform;
        // let position = roleT.position;
        // let eulerAngles = roleT.eulerAngles;
        // eulerAngles.y = 0;
        // if (position.z >= z)
        // {
        //     eulerAngles.y = 180;
        // }
        // role.eulerAngles = eulerAngles;

        // //移动
        // let masterPos = role.position;
        // //masterPos.y = 0;
        // let targetPos = new cc.Vec2(x, y);

        // let temp = targetPos.sub(masterPos);
        // temp.normalize();
        // temp *= runSpeed;

        // temp = temp * frequency / 1000;
        // masterPos += temp;
        //masterPos.y = role.y;
        
        rb.MoveTo(x, y,realSpeed);
    },

    StopFightRoleMoveBehavior(id)
    {
        let rb = this.GetFightRoleBehaviour(id);
        if (rb == null)
        {
            return;
        }
        rb.StopMove();
    },


    ////////////////////////////////////////////////////////飞行道具
    CreateFlyItem(id, targetID,  shotPosStr, flyItemID,  itemName, flyType,duration)
    {
        let masterParent = this.GetFightRoleObj(id, FightRolePathType.Parent);
        if(masterParent == null)
        {
            return;
        }
        let masterParentT = masterParent.transform;
        let targetHit = this.GetFightRoleObj(targetID, FightRolePathType.Hit);
        if (targetHit == null)
        {
            return;
        }

        //设置出手位置
        let shotPosArr = shotPosStr.split('|');
        let shotPos = new Array(3);
        if (shotPosArr.length != 3)
        {
            return;
        }

        for(let i = 0; i < 3; i++)
        {
            shotPos[i] = parseFloat(shotPosArr[i]);
        }

        let pos = new cc.Vec2(shotPos[0], shotPos[0], shotPos[1]);
        let pos1 = masterParentT.position;
        let pos2 = targetHit.position;
        if(pos1.z > pos2.z)
        {
            pos.z = pos.z * -1;
        }

        // let result = masterParentT.TransformPoint(pos);

        // flyItem = SpawnEffect(itemName);
        // let flyItemObj = GetEffect(flyItem);
        // switch (flyType)
        // {
        //     case FlyItemType.Line:
        //         BehaviorFlyItemManager.GetInstance().CreateLineFlyItem(flyItemObj, itemName, flyItemID,
        //             result.x, result.y, result.z, targetHit, duration);
        //         break;
        //     case FlyItemType.Parabola:
        //         BehaviorFlyItemManager.GetInstance().CreateParabolaFlyItem(flyItemObj, itemName, flyItemID,
        //             result.x, result.y, result.z, targetHit, duration);
        //         break;
        //     case FlyItemType.Chain:
        //         BehaviorFlyItemManager.GetInstance().CreateChainFlyItem(flyItemObj, itemName, flyItemID,
        //           result.x, result.y, result.z, targetHit, duration);
        //         break;
        //     default:
        //         BehaviorFlyItemManager.GetInstance().CreateLineFlyItem(flyItemObj, itemName, flyItemID,
        //             result.x, result.y, result.z, targetHit, duration);
        //         break;
        // }
    },

    /// <summary>
    /// 穿透飞行道具
    /// </summary>
    /// <param name="id"></param>
    /// <param name="flyItemID"></param>
    /// <param name="shotPosStr"></param>
    /// <param name="speed"></param>
    /// <param name="itemName"></param>
    /// <param name="flyType"></param>
    /// <param name="dist"></param>
    CreateCrossFlyItem(id, flyItemID, shotPosStr,speed, itemName, flyType,dist)
    {
        let masterParent = this.GetFightRoleObj(id, FightRolePathType.Parent);
        if (masterParent == null)
        {
            return;
        }
        let masterParentT = masterParent.transform;

        //设置出手位置
        let shotPosArr = shotPosStr.split('|');
        let shotPos = [];
        if (shotPosArr.length != 3)
        {
            return;
        }
        for (let i = 0; i < 3; i++)
        {
            shotPos[i] = parseFloat(shotPosArr[i]);
        }
        let pos = new cc.Vec2(shotPos[0], shotPos[0]);
        if (speed < 0)
        {
            pos.z = pos.z * -1;
        }
        // let result = masterParentT.TransformPoint(pos);

        // flyItem = SpawnEffect(itemName);
        // let flyItemObj = GetEffect(flyItem);
        // BehaviorFlyItemManager.GetInstance().CreateCrossFlyItem(flyItemObj, flyItemID, result.x, result.y, result.z, speed, dist);
    },

    CreateTrailFlyItem(fromID, toID, shotPosStr, flyItemName,duration,hit,time )
    {
        let masterParent = this.GetFightRoleObj(fromID, FightRolePathType.Parent);
        if (masterParent == null)
        {
            return;
        }
        let masterParentT = masterParent.transform;
        let targetHit = this.GetFightRoleObj(toID, FightRolePathType.Hit);
        if (targetHit == null)
        {
            return;
        }

        //设置出手位置
        let shotPosArr = shotPosStr.split('|');
        let shotPos = new float[2];
        if (shotPosArr.length != 3)
        {
            return;
        }

        for (let i = 0; i < 3; i++)
        {
            shotPos[i] = parseFloat(shotPosArr[i]);
        }

        let pos = new cc.Vec2(shotPos[0], shotPos[0]);
        let pos1 = masterParentT.position;
        let pos2 = targetHit.position;
        if (pos1.z > pos2.z)
        {
            pos.z = pos.z * -1;
        }

        // let result = masterParentT.TransformPoint(pos);

        // flyItem = SpawnEffect(flyItemName);
        // let flyItemObj = GetEffect(flyItem);
        // BehaviorFlyItemManager.GetInstance().CreateTrailFlyItem(flyItemObj, targetHit, flyItemName, flyItem, result.x, result.y, result.z, duration, hit, time);

    },

    ////////////////////////////////////////////////////////Buff
    AddFightRoleBuffEffect(id, effectName, rootType)
    {
        return this.SpwanEffectInFightRole(id, effectName,rootType, 0, false, 0, 0, 0);
    },

    RemoveFightRoleBuffEffect(effectObjID)
    {
        let effectObj = this.GetEffect(effectObjID);
        if(effectObj == null)
        {
            return;
        }
        // AutoDestroy ad = effectObj.getComponent('AutoDestroy');
        // if (ad != null)
        // {
        //     ad.Release();
        // }
        // else
        // {
        //     Destroy(effectObj);
        // }
    },
    

    /////////////////////////////////////////////////////////速度
    SetSpeed(speed)
    {
        // foreach(FightRoleCacheObj obj in mTotalModelCache.Values)
        // {

        //     if (obj == null)
        //     {
        //         continue;
        //     }

        //     FightRolePathObj frpo = obj.m_FightRole;
        //     if(frpo == null)
        //     {
        //         continue;
        //     }
        //     letrb = frpo.mBehaviour;
        //     if (rb == null)
        //     {
        //         continue;
        //     }
        //     rb.SetSpeed(speed);
        // }
    }

});
export default FightGameObjectManager;