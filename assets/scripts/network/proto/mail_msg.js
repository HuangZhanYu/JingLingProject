module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "MailResource",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Type",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Count",
                    "id": 3
                }
            ]
        },
        {
            "name": "MailData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "State",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "DeleteTime",
                    "id": 3
                },
                {
                    "rule": "repeated",
                    "type": "string",
                    "name": "Content",
                    "id": 4
                },
                {
                    "rule": "repeated",
                    "type": "MailResource",
                    "name": "Rewards",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Sign",
                    "id": 6
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Index",
                    "id": 7
                }
            ]
        },
        {
            "name": "MailGetDataRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "MailGetDataResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "MailData",
                    "name": "Data",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "IsShowPanel",
                    "id": 2
                }
            ]
        },
        {
            "name": "MailGetOneDataResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "MailData",
                    "name": "Data",
                    "id": 1
                }
            ]
        },
        {
            "name": "MailGetRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Index",
                    "id": 1
                }
            ]
        },
        {
            "name": "MailGetOneResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Success",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "MailResource",
                    "name": "Rewards",
                    "id": 2
                }
            ]
        },
        {
            "name": "MailGetTotalRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "MailGetResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Success",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "MailResource",
                    "name": "Rewards",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "MailData",
                    "name": "Data",
                    "id": 3
                }
            ]
        },
        {
            "name": "MailReadRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Index",
                    "id": 1
                }
            ]
        },
        {
            "name": "MailReadResponse",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "MailDeleteRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Index",
                    "id": 1
                }
            ]
        },
        {
            "name": "MailDeleteResponse",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "MailOneKeyDeleteRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "MailOneKeyDeleteResponse",
            "syntax": "proto2",
            "fields": []
        }
    ],
    "isNamespace": true
}).build();