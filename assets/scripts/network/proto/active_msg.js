module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "ActiveReward",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ItemType",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ItemID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ItemNum",
                    "id": 3
                }
            ]
        },
        {
            "name": "ActiveTaskItem",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Condition",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ConditionValue",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "ActiveReward",
                    "name": "Reward",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "GetSetae",
                    "id": 4
                }
            ]
        },
        {
            "name": "ActiveBuyTaskItem",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Condition",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ConditionValue",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Preice",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ShowPrice",
                    "id": 4
                },
                {
                    "rule": "repeated",
                    "type": "ActiveReward",
                    "name": "Reward",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "BuyState",
                    "id": 6
                }
            ]
        },
        {
            "name": "ActivityRewardList",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "ActiveReward",
                    "name": "RewardList",
                    "id": 1
                }
            ]
        },
        {
            "name": "ActiveLimitItem",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Name",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "RedPoint",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Type",
                    "id": 4
                }
            ]
        },
        {
            "name": "LimitListRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "LimitListResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "ActiveLimitItem",
                    "name": "List",
                    "id": 1
                }
            ]
        },
        {
            "name": "ActiveLimitDataRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ID",
                    "id": 1
                }
            ]
        },
        {
            "name": "SignActiveData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "SignProgress",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "HaveSign",
                    "id": 2
                }
            ]
        },
        {
            "name": "SignRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "SignResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "SignSusseed",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "SignProgress",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "HaveSign",
                    "id": 3
                }
            ]
        },
        {
            "name": "FirstRechargeActiveData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "ActiveReward",
                    "name": "Reward",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "GetSetae",
                    "id": 2
                }
            ]
        },
        {
            "name": "FirstRechargeActiveGetRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "FirstRechargeActiveGetResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "GetSetae",
                    "id": 1
                }
            ]
        },
        {
            "name": "MoonCardActiveData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "ActiveReward",
                    "name": "NormalReward",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "ActiveReward",
                    "name": "NormalOnceReward",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "ActiveReward",
                    "name": "AdvancedReward",
                    "id": 3
                },
                {
                    "rule": "repeated",
                    "type": "ActiveReward",
                    "name": "AdvancedOnceReward",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "NormalEndTime",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "AdvancedEndTime",
                    "id": 6
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "NormalGetState",
                    "id": 7
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "AdvancedGetState",
                    "id": 8
                }
            ]
        },
        {
            "name": "MoonCardActiveGetRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "IsAdvanced",
                    "id": 1
                }
            ]
        },
        {
            "name": "MoonCardActiveGetResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "GetSetae",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "IsAdvanced",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "NormalGetState",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "AdvancedGetState",
                    "id": 4
                }
            ]
        },
        {
            "name": "FundActiveData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "ActiveTaskItem",
                    "name": "NormalItem",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "ActiveTaskItem",
                    "name": "LuxuryItem",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "BuyNormal",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "BuyLuxury",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "NormalFundPrice",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "NormalFundVipNeed",
                    "id": 6
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "LuxuryFundPrice",
                    "id": 7
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "LuxuryFundVipNeed",
                    "id": 8
                }
            ]
        },
        {
            "name": "FundActiveOperateRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "OperateType",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Index",
                    "id": 2
                }
            ]
        },
        {
            "name": "FundActiveOperateResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "MsgCode",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "OperateType",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Index",
                    "id": 3
                }
            ]
        },
        {
            "name": "VipGiftData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Name",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "ActiveBuyTaskItem",
                    "name": "ConditionAndReward",
                    "id": 2
                }
            ]
        },
        {
            "name": "VipGiftRewardDataRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "VipGiftReward",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "ActiveReward",
                    "name": "RewardList",
                    "id": 1
                }
            ]
        },
        {
            "name": "VipGiftRewardDataReponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "VipGiftReward",
                    "name": "VipGiftRewardData",
                    "id": 1
                }
            ]
        },
        {
            "name": "VipGiftBuyRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "VipCondition",
                    "id": 1
                }
            ]
        },
        {
            "name": "VipGiftBuyResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "MsgCode",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "VipCondition",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "ActiveReward",
                    "name": "Reward",
                    "id": 3
                }
            ]
        },
        {
            "name": "ActiveRankItem",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Min",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Max",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "PlayerName",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "PlayerData",
                    "id": 4
                },
                {
                    "rule": "repeated",
                    "type": "ActiveReward",
                    "name": "Reward",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "PlayerLevel",
                    "id": 6
                }
            ]
        },
        {
            "name": "ActiveRankDataResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Type",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "MyRank",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int64",
                    "name": "FinishTime",
                    "id": 4
                },
                {
                    "rule": "repeated",
                    "type": "ActiveRankItem",
                    "name": "Data",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Introductions",
                    "id": 6
                }
            ]
        },
        {
            "name": "ActiveDailyWelfareFinishRechargeItem",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Index",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Stat",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Multi",
                    "id": 3
                },
                {
                    "rule": "repeated",
                    "type": "ActiveReward",
                    "name": "Rewards",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Time",
                    "id": 5
                }
            ]
        },
        {
            "name": "ActiveDailyWelfareCostItem",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Index",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "ActiveReward",
                    "name": "NeedCost",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "ActiveReward",
                    "name": "HasCost",
                    "id": 3
                },
                {
                    "rule": "repeated",
                    "type": "ActiveReward",
                    "name": "Rewards",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Stat",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "int64",
                    "name": "EndTime",
                    "id": 6
                }
            ]
        },
        {
            "name": "ActiveDailyWelfareRechargeItem",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Index",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "OrderID",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "ActiveReward",
                    "name": "Rewards",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Stat",
                    "id": 4
                }
            ]
        },
        {
            "name": "ActiveDailyWelfareDataRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 1
                }
            ]
        },
        {
            "name": "ActiveDailyWelfareDataResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "ActiveDailyWelfareFinishRechargeItem",
                    "name": "FinishRechargeData",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "ActiveDailyWelfareCostItem",
                    "name": "CostData",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "ActiveDailyWelfareRechargeItem",
                    "name": "RechargeData",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RechargeTime",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "LstTime",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "EndTime",
                    "id": 6
                }
            ]
        },
        {
            "name": "ActiveDailyWelfareGetRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Index",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 2
                }
            ]
        },
        {
            "name": "ActiveDailyWelfareGetResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Success",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "ActiveReward",
                    "name": "Rewards",
                    "id": 2
                }
            ]
        },
        {
            "name": "DayRechargeActiveData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "ActiveTaskItem",
                    "name": "TaskItem",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "AllIndex",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "LstTime",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "EndTime",
                    "id": 4
                }
            ]
        },
        {
            "name": "DayRechargeActiveGetRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Index",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 2
                }
            ]
        },
        {
            "name": "DayRechargeActiveGetResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "GetSetae",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Index",
                    "id": 2
                }
            ]
        },
        {
            "name": "ActiveTotalRechargeItem",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ConditionValue",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "ActiveReward",
                    "name": "Reward",
                    "id": 2
                }
            ]
        },
        {
            "name": "ActiveTotalRechargeDataResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int64",
                    "name": "FinishTime",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "ActiveTotalRechargeItem",
                    "name": "Data",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "CurrentCharge",
                    "id": 3
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "GetList",
                    "id": 4
                }
            ]
        },
        {
            "name": "ActiveTotalRechargeGetRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Condition",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ActiveID",
                    "id": 2
                }
            ]
        },
        {
            "name": "ActiveTotalRechargeGetResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Success",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "ActiveReward",
                    "name": "Rewards",
                    "id": 2
                }
            ]
        },
        {
            "name": "ActiveLevelGiftItem",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "ActiveReward",
                    "name": "CostInfo",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "ActiveReward",
                    "name": "RewardInfo",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RefreshType",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "LimitBuyCount",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "GiftID",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Discount",
                    "id": 6
                }
            ]
        },
        {
            "name": "ActiveLevelGiftDataResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Introduction",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "ActiveLevelGiftItem",
                    "name": "GiftDatas",
                    "id": 2
                }
            ]
        },
        {
            "name": "ActiveLevelGiftBuyRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "GiftID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Count",
                    "id": 2
                }
            ]
        },
        {
            "name": "ActiveLevelGiftBuyResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Success",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "ActiveReward",
                    "name": "Rewards",
                    "id": 2
                }
            ]
        },
        {
            "name": "ActivePointsShopItem",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "ActiveReward",
                    "name": "CostInfo",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "ActiveReward",
                    "name": "RewardInfo",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RefreshType",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "LimitBuyCount",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "GiftID",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Discount",
                    "id": 6
                }
            ]
        },
        {
            "name": "ActivePointsShopDataResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Introduction",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "ActivePointsShopItem",
                    "name": "GiftDatas",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "LstTime",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "EndTime",
                    "id": 4
                }
            ]
        },
        {
            "name": "ActivePointsShopBuyRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "GiftID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Count",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 3
                }
            ]
        },
        {
            "name": "ActivePointsShopBuyResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Success",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "ActiveReward",
                    "name": "Rewards",
                    "id": 2
                }
            ]
        }
    ],
    "isNamespace": true
}).build();