module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "ResetPart_LoginResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "GetPerMin",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "IsReseting",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int64",
                    "name": "LstResetTime",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int64",
                    "name": "LstStartTime",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "TodayCount",
                    "id": 5
                }
            ]
        },
        {
            "name": "ResetPart_StartRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "ResetPart_StartResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Success",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "ResetPart_LoginResponse",
                    "name": "Data",
                    "id": 2
                }
            ]
        },
        {
            "name": "ResetPart_FinishRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "Double",
                    "id": 1
                }
            ]
        },
        {
            "name": "ResetPart_FinishResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Success",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "ResetPart_LoginResponse",
                    "name": "Data",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "Reward",
                    "id": 3
                }
            ]
        },
        {
            "name": "ResetPart_ReduceTimeRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "ResetPart_ReduceTimeResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Success",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "ResetPart_LoginResponse",
                    "name": "Data",
                    "id": 2
                }
            ]
        }
    ],
    "isNamespace": true
}).build();