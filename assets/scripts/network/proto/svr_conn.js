module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "GS2Hub_Package",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ServerID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "FuncName",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "bytes",
                    "name": "Msg",
                    "id": 4
                }
            ]
        },
        {
            "name": "Hub2GS_Package",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "FuncName",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "bytes",
                    "name": "Msg",
                    "id": 3
                }
            ]
        },
        {
            "name": "PlayerOffline",
            "syntax": "proto2",
            "fields": []
        }
    ],
    "isNamespace": true
}).build();