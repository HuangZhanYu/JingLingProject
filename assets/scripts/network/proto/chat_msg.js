module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "ChatPart_Message",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ChatType",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "SenderObjID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Name",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Content",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Level",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Icon",
                    "id": 6
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "GuildName",
                    "id": 7
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "VipLevel",
                    "id": 8
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "GuildID",
                    "id": 9
                }
            ]
        },
        {
            "name": "ChatPart_SpeakRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ChatType",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Name",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Content",
                    "id": 3
                }
            ]
        },
        {
            "name": "ChatPart_SpeakResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                }
            ]
        },
        {
            "name": "ChatPart_FlauntNotice",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "NoticeID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Content",
                    "id": 2
                }
            ]
        },
        {
            "name": "ChatPart_MsgRecordRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ChatType",
                    "id": 1
                }
            ]
        },
        {
            "name": "ChatPart_MsgRecordResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "ChatPart_Message",
                    "name": "MsgList",
                    "id": 1
                }
            ]
        }
    ],
    "isNamespace": true
}).build();