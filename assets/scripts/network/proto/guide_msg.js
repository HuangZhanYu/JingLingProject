module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "GuidePart_TriggerResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "GuideID",
                    "id": 1
                }
            ]
        },
        {
            "name": "GuidePart_PassRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "GuideID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "StepID",
                    "id": 2
                }
            ]
        },
        {
            "name": "GuidePart_FinishRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "GuideID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "StepID",
                    "id": 2
                }
            ]
        },
        {
            "name": "GuidePart_UpdateGuideFightResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "GuideFight",
                    "id": 1
                }
            ]
        },
        {
            "name": "GuidePart_FinishGuideFightRequest",
            "syntax": "proto2",
            "fields": []
        }
    ],
    "isNamespace": true
}).build();