module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "TreasurePart_AddNewPositionRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "TreasurePart_ChangePopStateRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "IsNotShowToday",
                    "id": 1
                }
            ]
        },
        {
            "name": "TreasurePart_ChangePopStateResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "IsNotShowToday",
                    "id": 2
                }
            ]
        },
        {
            "name": "TreasurePart_AddNewPositionResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Capacity",
                    "id": 2
                }
            ]
        },
        {
            "name": "TreasurePart_AddTreasureRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Key",
                    "id": 1
                }
            ]
        },
        {
            "name": "TreasurePart_AddTreasureResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                }
            ]
        },
        {
            "name": "TreasurePart_ChangeTreasureRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "EquipKey",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "BagKey",
                    "id": 2
                }
            ]
        },
        {
            "name": "TreasurePart_ChangeTreasureResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                }
            ]
        },
        {
            "name": "TreasurePart_UpTreasureLevelRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Count",
                    "id": 2
                }
            ]
        },
        {
            "name": "TreasurePart_UpTreasureLevelResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                }
            ]
        },
        {
            "name": "TreasurePart_UpTreasureEvoLevelRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 1
                }
            ]
        },
        {
            "name": "TreasurePart_UpTreasureEvoLevelResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                }
            ]
        }
    ],
    "isNamespace": true
}).build();