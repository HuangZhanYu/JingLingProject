module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "GetRankDataRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RankType",
                    "id": 1
                }
            ]
        },
        {
            "name": "RankPlayerData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Name",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Head",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "RankValue",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Account",
                    "id": 4
                }
            ]
        },
        {
            "name": "RankData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "RankPlayerData",
                    "name": "Data",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RankType",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "MyRankIndex",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "MyRankValue",
                    "id": 4
                }
            ]
        }
    ],
    "isNamespace": true
}).build();