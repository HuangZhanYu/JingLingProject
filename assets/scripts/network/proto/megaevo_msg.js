module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "KeyStoneInfo",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "StoneID",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "EnhanceLevels",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "JumpLevel",
                    "id": 3
                }
            ]
        },
        {
            "name": "MegaEvoPart_StoneDataRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "MegaEvoPart_StoneDataResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "KeyStoneInfo",
                    "name": "StoneDatas",
                    "id": 1
                }
            ]
        },
        {
            "name": "MegaEvoPart_StoneSynthesizeRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "StoneID",
                    "id": 1
                }
            ]
        },
        {
            "name": "MegaEvoPart_StoneSynthesizeResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "KeyStoneInfo",
                    "name": "StoneData",
                    "id": 2
                }
            ]
        },
        {
            "name": "MegaEvoPart_EnhanceRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "StoneID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Index",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Count",
                    "id": 3
                }
            ]
        },
        {
            "name": "MegaEvoPart_EnhanceResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "KeyStoneInfo",
                    "name": "StoneData",
                    "id": 2
                }
            ]
        }
    ],
    "isNamespace": true
}).build();