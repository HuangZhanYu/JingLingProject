module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "FettersPart_Fetters",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "CardStateList",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "IsCollected",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "FettersID",
                    "id": 3
                }
            ]
        },
        {
            "name": "FettersPart_FettersQuality",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "FettersPart_Fetters",
                    "name": "FettersList",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "RewardList",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Process",
                    "id": 3
                }
            ]
        },
        {
            "name": "FettersPart_FettersData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "FettersIDArr",
                    "id": 1
                }
            ]
        },
        {
            "name": "FettersPart_FettersDataRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "FettersPart_FettersDataResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "FettersPart_FettersQuality",
                    "name": "FettersQualityList",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "FettersIDArr",
                    "id": 3
                }
            ]
        },
        {
            "name": "FettersPart_SetCardRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "CardIndex",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "FettersID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Quality",
                    "id": 3
                }
            ]
        },
        {
            "name": "FettersPart_SetCardResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                }
            ]
        },
        {
            "name": "FettersPart_UnloadCardRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "CardIndex",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "FettersID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Quality",
                    "id": 3
                }
            ]
        },
        {
            "name": "FettersPart_UnloadCardResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                }
            ]
        },
        {
            "name": "FettersPart_GetRewardRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Quality",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Index",
                    "id": 2
                }
            ]
        },
        {
            "name": "FettersPart_GetRewardResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                }
            ]
        }
    ],
    "isNamespace": true
}).build();