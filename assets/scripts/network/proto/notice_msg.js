module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "Notice",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "IsXuanYao",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "string",
                    "name": "Content",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "StartTime",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "EndTime",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "Insert",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "IsRoll",
                    "id": 6
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "IntervalTime",
                    "id": 7
                }
            ]
        },
        {
            "name": "NoticeResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "Notice",
                    "name": "Datas",
                    "id": 1
                }
            ]
        }
    ],
    "isNamespace": true
}).build();