module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "FriendData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "PlayerObjectID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "PlayerName",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "PlayerLevel",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "GuildName",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int64",
                    "name": "LeaveTime",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "HeadIconID",
                    "id": 6
                }
            ]
        },
        {
            "name": "FriendRecommendData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "PlayerObjectID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "VIPLevel",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Level",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Name",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ZhanLi",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "JunTuan",
                    "id": 6
                },
                {
                    "rule": "required",
                    "type": "int64",
                    "name": "LeaveTime",
                    "id": 7
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Icon",
                    "id": 8
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Quality",
                    "id": 9
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Frame",
                    "id": 13
                }
            ]
        },
        {
            "name": "FriendCardInfo",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "CardID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Level",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "EvolutionLevel",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 4
                }
            ]
        },
        {
            "name": "FriendPart_ChatMessage",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Name",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Content",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Level",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "HeadIconID",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ReceiverName",
                    "id": 5
                }
            ]
        },
        {
            "name": "FriendRecommendRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "FriendRecommendResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "FriendRecommendData",
                    "name": "RecommendData",
                    "id": 1
                }
            ]
        },
        {
            "name": "FriendAddByIDRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "PlayerObjectID",
                    "id": 1
                }
            ]
        },
        {
            "name": "FriendAddByNameRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "PlayerName",
                    "id": 1
                }
            ]
        },
        {
            "name": "FriendAddByIDResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "PlayerObjectID",
                    "id": 2
                }
            ]
        },
        {
            "name": "FriendAddByNameResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                }
            ]
        },
        {
            "name": "FriendBlackListResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "FriendData",
                    "name": "BlackList",
                    "id": 1
                }
            ]
        },
        {
            "name": "FriendUpdateTotalResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "FriendData",
                    "name": "FriendDatas",
                    "id": 1
                }
            ]
        },
        {
            "name": "FriendShowRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "FriendShowResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                }
            ]
        },
        {
            "name": "FriendDeleteRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "PlayerObjectID",
                    "id": 1
                }
            ]
        },
        {
            "name": "FriendDeleteResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Name",
                    "id": 2
                }
            ]
        },
        {
            "name": "FriendBlackRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "PlayerObjectID",
                    "id": 1
                }
            ]
        },
        {
            "name": "FriendBlackResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                }
            ]
        },
        {
            "name": "FriendWhiteRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "PlayerObjectID",
                    "id": 1
                }
            ]
        },
        {
            "name": "FriendWhiteResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                }
            ]
        },
        {
            "name": "FriendAcceptRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "PlayerObjectID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Operate",
                    "id": 2
                }
            ]
        },
        {
            "name": "FriendAcceptResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                }
            ]
        },
        {
            "name": "FriendNewRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "FriendNewResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "FriendData",
                    "name": "RequestDatas",
                    "id": 1
                }
            ]
        },
        {
            "name": "FriendPart_SpeakRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "PlayerObjectID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Content",
                    "id": 2
                }
            ]
        },
        {
            "name": "FriendPart_SpeakResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                }
            ]
        },
        {
            "name": "FriendQieChuoRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "PlayerObjectID",
                    "id": 1
                }
            ]
        },
        {
            "name": "FriendQieChuoResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "FriendCardInfo",
                    "name": "CardInfoList",
                    "id": 2
                }
            ]
        },
        {
            "name": "JumpFriendFightResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "IsWin",
                    "id": 1
                }
            ]
        }
    ],
    "isNamespace": true
}).build();