module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "SDKLoginRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "AccountID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "IsIphone",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "SdkType",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "Sessionid",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "Gameid",
                    "id": 5
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "Appkey",
                    "id": 6
                }
            ]
        },
        {
            "name": "SDKLoginResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "MsgCode",
                    "id": 1
                }
            ]
        }
    ],
    "isNamespace": true
}).build();