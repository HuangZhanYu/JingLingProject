module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "CDKeyPart_Reward",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ResourceType",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ResourceID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ResourceCount",
                    "id": 3
                }
            ]
        },
        {
            "name": "CDKeyPart_UseCodeRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Code",
                    "id": 1
                }
            ]
        },
        {
            "name": "CDKeyPart_UseCodeResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "CDKeyPart_Reward",
                    "name": "RewardList",
                    "id": 2
                }
            ]
        }
    ],
    "isNamespace": true
}).build();