module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "LineupPart_LevelUpCardRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjId",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Count",
                    "id": 2
                }
            ]
        },
        {
            "name": "LineupPart_LevelUpCardResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                }
            ]
        },
        {
            "name": "LineupPart_ChangePopStateRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "IsNotShowToday",
                    "id": 1
                }
            ]
        },
        {
            "name": "LineupPart_ChangePopStateResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "IsNotShowToday",
                    "id": 2
                }
            ]
        },
        {
            "name": "LineupPart_EnhanceLevelUpRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjId",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Count",
                    "id": 2
                }
            ]
        },
        {
            "name": "LineupPart_EnhanceLevelUpResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                }
            ]
        },
        {
            "name": "LineupPart_AddNewPositionRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "LineupPart_AddNewPositionResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Capacity",
                    "id": 2
                }
            ]
        },
        {
            "name": "LineupPart_AddCardRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "CardID",
                    "id": 1
                }
            ]
        },
        {
            "name": "LineupPart_AddCardResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                }
            ]
        },
        {
            "name": "LineupPart_ChangeCardRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "LineUpCardKey",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "BackPackCardID",
                    "id": 2
                }
            ]
        },
        {
            "name": "LineupPart_ChangeCardResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                }
            ]
        },
        {
            "name": "LineupPart_EvolutionCardRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjId",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "EvoType",
                    "id": 2
                }
            ]
        },
        {
            "name": "LineupPart_EvolutionCardResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 2
                }
            ]
        },
        {
            "name": "LineupPart_OnTianFuOpenResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "CardID",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "Indexs",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "string",
                    "name": "Values",
                    "id": 3
                }
            ]
        },
        {
            "name": "LineupPart_BounsInfo",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "Levels",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "int64",
                    "name": "TriggerTime",
                    "id": 2
                }
            ]
        },
        {
            "name": "LineupPart_ZhanliUp",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Zhanli",
                    "id": 1
                }
            ]
        }
    ],
    "isNamespace": true
}).build();