module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "GuildPart_Sync",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "GuildID",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "DoubleCards",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "BossAlive",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "GuildLevel",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "TotalFightTimes",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "HistoryHighestBossKill",
                    "id": 6
                }
            ]
        },
        {
            "name": "GuildPart_GuildListRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "GuildPart_Guild",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "GuildID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Name",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Icon",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Level",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "MemberCount",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Capacity",
                    "id": 6
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "HasApply",
                    "id": 7
                }
            ]
        },
        {
            "name": "GuildPart_GuildListResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "GuildPart_Guild",
                    "name": "GuildList",
                    "id": 1
                }
            ]
        },
        {
            "name": "GuildPart_SearchRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Name",
                    "id": 1
                }
            ]
        },
        {
            "name": "GuildPart_SearchResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "GuildPart_Guild",
                    "name": "GuildList",
                    "id": 2
                }
            ]
        },
        {
            "name": "GuildPart_GuildInfoRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "GuildPart_Boss",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Area",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Diff",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "BossID",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "MaxHP",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "HP",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "LastTime",
                    "id": 6
                }
            ]
        },
        {
            "name": "GuildPart_Build",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Level",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "Active",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "LastTime",
                    "id": 3
                }
            ]
        },
        {
            "name": "GuildPart_Cloth",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "CardList",
                    "id": 1
                }
            ]
        },
        {
            "name": "GuildPart_GuildInfoResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Name",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Icon",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Level",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Exp",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "HistoryHighestBossKill",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "MemberCount",
                    "id": 6
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Capacity",
                    "id": 7
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "GuildCoin",
                    "id": 8
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Contribution",
                    "id": 9
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Position",
                    "id": 10
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Notice",
                    "id": 11
                },
                {
                    "rule": "repeated",
                    "type": "GuildPart_Boss",
                    "name": "OpenedBoss",
                    "id": 12
                },
                {
                    "rule": "repeated",
                    "type": "GuildPart_Build",
                    "name": "Buildings",
                    "id": 13
                },
                {
                    "rule": "repeated",
                    "type": "GuildPart_Cloth",
                    "name": "ClothArray",
                    "id": 14
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "GainElves",
                    "id": 15
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "SignT",
                    "id": 16
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Status",
                    "id": 17
                }
            ]
        },
        {
            "name": "GuildPart_MembersRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "GuildPart_Member",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Name",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Icon",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Position",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "TopGuanQia",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Contribution",
                    "id": 6
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "OwnCard",
                    "id": 7
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Level",
                    "id": 8
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "Online",
                    "id": 9
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "OfflineTime",
                    "id": 10
                }
            ]
        },
        {
            "name": "GuildPart_MembersResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "GuildCoin",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Contribution",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "GuildPart_Member",
                    "name": "Members",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "OwnPosition",
                    "id": 4
                }
            ]
        },
        {
            "name": "GuildPart_AppliesRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "GuildPart_Apply",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Name",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "TopGuanQia",
                    "id": 3
                }
            ]
        },
        {
            "name": "GuildPart_AppliesResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "GuildCoin",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Contribution",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "JoinCheck",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "MemberCount",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Capacity",
                    "id": 5
                },
                {
                    "rule": "repeated",
                    "type": "GuildPart_Apply",
                    "name": "ApplyList",
                    "id": 6
                }
            ]
        },
        {
            "name": "GuildPart_LogsRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "GuildPart_Log",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "LogTm",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "LogT",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "string",
                    "name": "Params",
                    "id": 3
                }
            ]
        },
        {
            "name": "GuildPart_LogsResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "GuildCoin",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Contribution",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "GuildPart_Log",
                    "name": "LogList",
                    "id": 3
                }
            ]
        },
        {
            "name": "GuildPart_CreateRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Name",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Icon",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Notice",
                    "id": 3
                }
            ]
        },
        {
            "name": "GuildPart_CreateResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "GuildID",
                    "id": 2
                }
            ]
        },
        {
            "name": "GuildPart_ApplyRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "GuildID",
                    "id": 1
                }
            ]
        },
        {
            "name": "GuildPart_ApplyResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "GuildID",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "MyGuildID",
                    "id": 3
                }
            ]
        },
        {
            "name": "GuildPart_CancelApplyRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "GuildID",
                    "id": 1
                }
            ]
        },
        {
            "name": "GuildPart_CancelApplyResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "GuildID",
                    "id": 2
                }
            ]
        },
        {
            "name": "GuildPart_AgreeRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 1
                }
            ]
        },
        {
            "name": "GuildPart_AgreeResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 2
                }
            ]
        },
        {
            "name": "GuildPart_RefuseRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 1
                }
            ]
        },
        {
            "name": "GuildPart_RefuseResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 2
                }
            ]
        },
        {
            "name": "GuildPart_QuitRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "GuildPart_QuitResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                }
            ]
        },
        {
            "name": "GuildPart_KickRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 1
                }
            ]
        },
        {
            "name": "GuildPart_KickResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 2
                }
            ]
        },
        {
            "name": "GuildPart_AppointRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Position",
                    "id": 2
                }
            ]
        },
        {
            "name": "GuildPart_AppointResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "ObjID",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "Position",
                    "id": 3
                }
            ]
        },
        {
            "name": "GuildPart_ImpeachRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "GuildPart_ImpeachResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                }
            ]
        },
        {
            "name": "GuildPart_SetCheckRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Open",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "LimitV",
                    "id": 2
                }
            ]
        },
        {
            "name": "GuildPart_SetCheckResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                }
            ]
        },
        {
            "name": "GuildPart_SetIconRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Icon",
                    "id": 1
                }
            ]
        },
        {
            "name": "GuildPart_SetIconResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "Icon",
                    "id": 2
                }
            ]
        },
        {
            "name": "GuildPart_SetNoticeRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Notice",
                    "id": 1
                }
            ]
        },
        {
            "name": "GuildPart_SetNoticeResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "Notice",
                    "id": 2
                }
            ]
        },
        {
            "name": "GuildPart_BuildingInfoRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "BuildID",
                    "id": 1
                }
            ]
        },
        {
            "name": "GuildPart_DonateRecord",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Times",
                    "id": 2
                }
            ]
        },
        {
            "name": "GuildPart_BuildingInfoResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "Active",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Level",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "ActiveExp",
                    "id": 3
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "SpecNeeds",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "LastTime",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "GuildCoin",
                    "id": 6
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Contribution",
                    "id": 7
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "SelfLevel",
                    "id": 8
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "GuildLevel",
                    "id": 9
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Position",
                    "id": 10
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "LevelAtZero",
                    "id": 11
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "GuildDonateTimes",
                    "id": 12
                },
                {
                    "rule": "repeated",
                    "type": "GuildPart_DonateRecord",
                    "name": "Records",
                    "id": 13
                }
            ]
        },
        {
            "name": "GuildPart_DonateRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "BuildID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "DonateT",
                    "id": 2
                }
            ]
        },
        {
            "name": "GuildPart_DonateResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "bool",
                    "name": "Active",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "ActiveExp",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "Level",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "DonateT",
                    "id": 5
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "GuildDonateTimes",
                    "id": 6
                },
                {
                    "rule": "repeated",
                    "type": "GuildPart_DonateRecord",
                    "name": "Records",
                    "id": 7
                }
            ]
        },
        {
            "name": "GuildPart_UpBuildingRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "BuildID",
                    "id": 1
                }
            ]
        },
        {
            "name": "GuildPart_UpBuildingResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "Level",
                    "id": 2
                }
            ]
        },
        {
            "name": "GuildPart_UpSelfBuildRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "BuildID",
                    "id": 1
                }
            ]
        },
        {
            "name": "GuildPart_UpSelfBuildResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "Level",
                    "id": 2
                }
            ]
        }
    ],
    "isNamespace": true
}).build();