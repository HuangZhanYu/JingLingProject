module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "CostDiamondReward",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ItemType",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ItemID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ItemNum",
                    "id": 3
                }
            ]
        },
        {
            "name": "NormalCostItem",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Min",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Max",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "PlayerName",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "PlayerData",
                    "id": 4
                },
                {
                    "rule": "repeated",
                    "type": "CostDiamondReward",
                    "name": "Reward",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "PlayerLevel",
                    "id": 6
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "HeadIconID",
                    "id": 7
                }
            ]
        },
        {
            "name": "HighCostItem",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Min",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Max",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "PlayerName",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "PlayerData",
                    "id": 4
                },
                {
                    "rule": "repeated",
                    "type": "CostDiamondReward",
                    "name": "Reward",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "PlayerLevel",
                    "id": 6
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "AtLeast",
                    "id": 7
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "HeadIconID",
                    "id": 8
                }
            ]
        },
        {
            "name": "ActiveCostDiamondDataResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ID",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "MyRank",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int64",
                    "name": "FinishTime",
                    "id": 3
                },
                {
                    "rule": "repeated",
                    "type": "NormalCostItem",
                    "name": "NormalData",
                    "id": 4
                },
                {
                    "rule": "repeated",
                    "type": "HighCostItem",
                    "name": "HighData",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Introductions",
                    "id": 6
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "MyCost",
                    "id": 7
                }
            ]
        },
        {
            "name": "CostDiamondPlayerData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Rank",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Name",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Cost",
                    "id": 3
                }
            ]
        },
        {
            "name": "ActiveCostDiamondRankListRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Index",
                    "id": 2
                }
            ]
        },
        {
            "name": "ActiveCostDiamondRankListResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "CostDiamondPlayerData",
                    "name": "RankList",
                    "id": 1
                }
            ]
        },
        {
            "name": "OnceRechargeItem",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ID",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "CostDiamondReward",
                    "name": "Rewards",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "GetCount",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RechargeCount",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "MaxCount",
                    "id": 5
                }
            ]
        },
        {
            "name": "OnceRechargeData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int64",
                    "name": "StartTime",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int64",
                    "name": "FinishTime",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "OnceRechargeItem",
                    "name": "Datas",
                    "id": 3
                }
            ]
        },
        {
            "name": "OnceRechargeGetRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ActiveID",
                    "id": 2
                }
            ]
        },
        {
            "name": "OnceRechargeGetResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Success",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "CostDiamondReward",
                    "name": "Rewards",
                    "id": 2
                }
            ]
        },
        {
            "name": "CostRewardItem",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Index",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "CostDiamondReward",
                    "name": "Rewards",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "CostNum",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Stat",
                    "id": 4
                }
            ]
        },
        {
            "name": "CostRewardData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int64",
                    "name": "StartTime",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int64",
                    "name": "FinishTime",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "CostRewardItem",
                    "name": "Datas",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "CostCount",
                    "id": 4
                }
            ]
        },
        {
            "name": "CostRewardGetRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Index",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ActiveID",
                    "id": 2
                }
            ]
        },
        {
            "name": "CostRewardGetResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Success",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "CostDiamondReward",
                    "name": "Rewards",
                    "id": 2
                }
            ]
        },
        {
            "name": "ActiveRedPointResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "List",
                    "id": 1
                }
            ]
        },
        {
            "name": "ActiveRedPointChangeRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Index",
                    "id": 1
                }
            ]
        },
        {
            "name": "ActiveLimitRedpointChange",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Id",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "State",
                    "id": 2
                }
            ]
        },
        {
            "name": "ActiveLimitRankRedpoint",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Id",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RankType",
                    "id": 2
                }
            ]
        },
        {
            "name": "PartnerRankAward",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RankBegin",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RankEnd",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "CostDiamondReward",
                    "name": "Rewards",
                    "id": 3
                }
            ]
        },
        {
            "name": "PartnerPointAward",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Point",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Status",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "CostDiamondReward",
                    "name": "Rewards",
                    "id": 3
                }
            ]
        },
        {
            "name": "ActivePartnerResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ShowModeID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Rank",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Point",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "FreeTimes",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "PreCost",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "TenCost",
                    "id": 6
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "GotPointAwards",
                    "id": 7
                },
                {
                    "rule": "repeated",
                    "type": "PartnerRankAward",
                    "name": "RankAwards",
                    "id": 8
                },
                {
                    "rule": "repeated",
                    "type": "PartnerPointAward",
                    "name": "PointAwards",
                    "id": 9
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "LstTime",
                    "id": 10
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "EndTime",
                    "id": 11
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "FreeCD",
                    "id": 12
                }
            ]
        },
        {
            "name": "ActivePartner_LotteryRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Times",
                    "id": 2
                }
            ]
        },
        {
            "name": "ActivePartner_LotteryResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "CostDiamondReward",
                    "name": "Awards",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "GetPoint",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "Point",
                    "id": 4
                },
                {
                    "rule": "repeated",
                    "type": "PartnerPointAward",
                    "name": "PointAwards",
                    "id": 5
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "Rank",
                    "id": 6
                },
                {
                    "rule": "optional",
                    "type": "bool",
                    "name": "FreeTimes",
                    "id": 7
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "FreeCD",
                    "id": 8
                }
            ]
        },
        {
            "name": "ActivePartner_PointAwardRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Point",
                    "id": 2
                }
            ]
        },
        {
            "name": "ActivePartner_PointAwardResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "CostDiamondReward",
                    "name": "Awards",
                    "id": 2
                }
            ]
        },
        {
            "name": "ActivePartner_RankItem",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Account",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Nick",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Level",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Icon",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Point",
                    "id": 5
                }
            ]
        },
        {
            "name": "ActivePartner_RankRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 1
                }
            ]
        },
        {
            "name": "ActivePartner_RankResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "ActivePartner_RankItem",
                    "name": "RankList",
                    "id": 1
                }
            ]
        },
        {
            "name": "ActiveContractResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "CostDiamondReward",
                    "name": "TargetAwards",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "LstTimes",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "BadgeStatus",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "TotalTimes",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "PreNeedCNY",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "MaxTimes",
                    "id": 6
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "FreeTimes",
                    "id": 7
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RechargeCNY",
                    "id": 8
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "BadgeCNY",
                    "id": 9
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "LstTime",
                    "id": 10
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "EndTime",
                    "id": 11
                }
            ]
        },
        {
            "name": "ActiveContract_LotteryRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Index",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Times",
                    "id": 3
                }
            ]
        },
        {
            "name": "ActiveContract_LotteryResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "CostDiamondReward",
                    "name": "Awards",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "LstTimes",
                    "id": 3
                }
            ]
        },
        {
            "name": "ActiveContract_BadgeAwardInfoRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 1
                }
            ]
        },
        {
            "name": "ActiveContract_BadgeAwardInfoResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "CostDiamondReward",
                    "name": "Awards",
                    "id": 1
                }
            ]
        },
        {
            "name": "ActiveContract_BadgeAwardRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Index",
                    "id": 2
                }
            ]
        },
        {
            "name": "ActiveContract_BadgeAwardResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "CostDiamondReward",
                    "name": "Awards",
                    "id": 2
                }
            ]
        },
        {
            "name": "BarbecuePartyActiveData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Name",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Instruction",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "TimeDes",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "IsGetLunch",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "IsGetDinner",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "LStartTime",
                    "id": 6
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "LEndTime",
                    "id": 7
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "DStartTime",
                    "id": 8
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "DEndTime",
                    "id": 9
                }
            ]
        },
        {
            "name": "BarbecuePartyGetRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "BarbecuePartyGetResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Success",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "CostDiamondReward",
                    "name": "Rewards",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "IsGetLunch",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "IsGetDinner",
                    "id": 4
                }
            ]
        },
        {
            "name": "TotalCostDiamondItem",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ConditionV",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "CostDiamondReward",
                    "name": "Rewards",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "Got",
                    "id": 3
                }
            ]
        },
        {
            "name": "TotalCostDiamondActiveData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "TotalCost",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "TotalCostDiamondItem",
                    "name": "TargetAwards",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "LstTime",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "EndTime",
                    "id": 4
                }
            ]
        },
        {
            "name": "TotalCostDiamondRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ConditionV",
                    "id": 2
                }
            ]
        },
        {
            "name": "TotalCostDiamondResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "ConditionV",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "CostDiamondReward",
                    "name": "Rewards",
                    "id": 3
                }
            ]
        }
    ],
    "isNamespace": true
}).build();