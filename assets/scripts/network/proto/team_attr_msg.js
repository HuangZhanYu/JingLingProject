module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "TeamAttrPart_Data",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "IsTrigger",
                    "id": 2
                }
            ]
        },
        {
            "name": "TeamAttrPart_TriggerResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "TeamAttrPart_Data",
                    "name": "Datas",
                    "id": 1
                }
            ]
        },
        {
            "name": "TeamAttrPart_NewTriggerResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "IDs",
                    "id": 1
                }
            ]
        }
    ],
    "isNamespace": true
}).build();