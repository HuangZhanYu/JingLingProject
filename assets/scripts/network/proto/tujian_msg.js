module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "TuJianPart_TuJianDataRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "TuJianPart_TuJianDataResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "TotalCardIDs",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "TotalEquipIDs",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "CardRewardGetList",
                    "id": 3
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "EquipRewardGetList",
                    "id": 4
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "CollectCards",
                    "id": 5
                }
            ]
        },
        {
            "name": "TuJianPart_GetRewardRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RewardType",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RewardID",
                    "id": 2
                }
            ]
        },
        {
            "name": "TuJianPart_GetRewardResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                }
            ]
        },
        {
            "name": "TuJianPart_LoginDatas",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "TotalCardIDs",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "TotalEquipIDs",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "CollectCards",
                    "id": 3
                }
            ]
        }
    ],
    "isNamespace": true
}).build();