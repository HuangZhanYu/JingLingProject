module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "ShopInfo",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "GoodsResType",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "GoodsID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "GoodsCount",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "IsRecommend",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "BuyResType",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "BuyResId",
                    "id": 6
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "BuyCostCount",
                    "id": 7
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "BuyResType2",
                    "id": 8
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "BuyResId2",
                    "id": 9
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "BuyCostCount2",
                    "id": 10
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "IsHadBuy",
                    "id": 11
                }
            ]
        },
        {
            "name": "ShopPart_SendShopInfo",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "BuyInfoList",
                    "id": 1
                }
            ]
        },
        {
            "name": "ShopPart_SendCommonShopInfo",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "ShopInfo",
                    "name": "ShopList",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "FreeRefreshCount",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "LastRecoverTime",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "HasUsedFreshCard",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ShopEnum",
                    "id": 5
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "DailyFreshCount",
                    "id": 6
                }
            ]
        },
        {
            "name": "ShopPart_BuyItemRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Index",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ShopEnum",
                    "id": 2
                }
            ]
        },
        {
            "name": "ShopPart_BuyItemResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ShopEnum",
                    "id": 2
                }
            ]
        },
        {
            "name": "ShopPart_RefreshItemRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ShopEnum",
                    "id": 1
                }
            ]
        },
        {
            "name": "ShopPart_RefreshItemResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ShopEnum",
                    "id": 2
                }
            ]
        },
        {
            "name": "ShopPart_ArenaInfoRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "ShopPart_ArenaInfoResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "ShopInfo",
                    "name": "ArenaGoodsList",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "HasFreshHonorCount",
                    "id": 2
                }
            ]
        },
        {
            "name": "ShopPart_GuildInfoRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "ShopPart_GuildInfoResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "ShopInfo",
                    "name": "GuildGoodsList",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "HasFreshContriCount",
                    "id": 2
                }
            ]
        },
        {
            "name": "ShopPart_SingleShopInfoRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ShopEnum",
                    "id": 1
                }
            ]
        },
        {
            "name": "ShopPart_SingleShopInfoResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "ShopInfo",
                    "name": "ShopList",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "FreeRefreshCount",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "LastRecoverTime",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "HasUsedFreshCard",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ShopEnum",
                    "id": 5
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "DailyFreshCount",
                    "id": 6
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 7
                }
            ]
        },
        {
            "name": "ShopPart_BuyMainRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ItemID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Count",
                    "id": 2
                }
            ]
        },
        {
            "name": "ShopPart_BuyMainResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                }
            ]
        }
    ],
    "isNamespace": true
}).build();