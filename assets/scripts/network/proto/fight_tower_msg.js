module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "FightTowerPart_FightTowerData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "LineUpCardList",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "OpenCount",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int64",
                    "name": "StartTime",
                    "id": 3
                }
            ]
        },
        {
            "name": "FightTowerPart_CommentData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "PlayerName",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "PlayerLevel",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "HeadIcon",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Likes",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Content",
                    "id": 6
                },
                {
                    "rule": "required",
                    "type": "int64",
                    "name": "CommentTime",
                    "id": 7
                }
            ]
        },
        {
            "name": "FightTowerPart_AddLineUpRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "CardID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Round",
                    "id": 2
                }
            ]
        },
        {
            "name": "FightTowerPart_AddLineUpResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                }
            ]
        },
        {
            "name": "FightTowerPart_SubLineDownRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Index",
                    "id": 1
                }
            ]
        },
        {
            "name": "FightTowerPart_SubLineDownResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Index",
                    "id": 2
                }
            ]
        },
        {
            "name": "FightTowerPart_ChangeLineRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "IndexOne",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "IndexTwo",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Round",
                    "id": 3
                }
            ]
        },
        {
            "name": "FightTowerPart_ChangeLineResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                }
            ]
        },
        {
            "name": "FightTowerPart_FightRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Round",
                    "id": 1
                }
            ]
        },
        {
            "name": "FightTowerPart_FightResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "IsFirtPass",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "bytes",
                    "name": "Content",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "IsWin",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "Done",
                    "id": 5
                }
            ]
        },
        {
            "name": "FightTowerPart_FightAppendResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "bytes",
                    "name": "Content",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "Done",
                    "id": 2
                }
            ]
        },
        {
            "name": "FightTowerPart_CheckLineRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Round",
                    "id": 1
                }
            ]
        },
        {
            "name": "FightTowerPart_CheckLineResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                }
            ]
        },
        {
            "name": "FightTowerPart_BuyChallengeCountRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Count",
                    "id": 1
                }
            ]
        },
        {
            "name": "FightTowerPart_BuyChallengeCountResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                }
            ]
        },
        {
            "name": "FightTowerPart_OnBeginFightVerifyRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ZhenRongID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RoundID",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "LineCardList",
                    "id": 3
                }
            ]
        },
        {
            "name": "FightTowerPart_BeginFightVerifyResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "FightID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RandSeed",
                    "id": 2
                }
            ]
        },
        {
            "name": "FightTowerPart_StartFightVerifyRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "FightID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "FightResult",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "bytes",
                    "name": "Input",
                    "id": 3
                }
            ]
        },
        {
            "name": "FightTowerPart_StartFightVerifyResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "IsWin",
                    "id": 1
                }
            ]
        },
        {
            "name": "FightTowerPart_CommentDataRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Round",
                    "id": 1
                }
            ]
        },
        {
            "name": "FightTowerPart_CommentDataResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "FightTowerPart_CommentData",
                    "name": "CommentData",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "string",
                    "name": "HasLiked",
                    "id": 3
                }
            ]
        },
        {
            "name": "FightTowerPart_CommentRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Content",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Round",
                    "id": 2
                }
            ]
        },
        {
            "name": "FightTowerPart_CommentResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "FightTowerPart_CommentData",
                    "name": "CommentData",
                    "id": 2
                }
            ]
        },
        {
            "name": "FightTowerPart_LikeRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Round",
                    "id": 2
                }
            ]
        },
        {
            "name": "FightTowerPart_LikeResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                }
            ]
        }
    ],
    "isNamespace": true
}).build();