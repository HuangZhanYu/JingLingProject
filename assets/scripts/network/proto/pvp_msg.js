module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "PVPPart_StatRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "PVPPart_StatResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "PeriodNum",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "StartTime",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "OpenStat",
                    "id": 3
                }
            ]
        },
        {
            "name": "PVPPart_InfoRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "PVPPart_InfoResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Point",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "WinTimes",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "WinRate",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "TopPoint",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "TopWinTimes",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "TopWinRate",
                    "id": 6
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "LstMatchTimes",
                    "id": 7
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "TotalMatchTimes",
                    "id": 8
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "LstDays",
                    "id": 9
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "MatchTimesToday",
                    "id": 10
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "WinTimesToday",
                    "id": 11
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "CWinTimesToday",
                    "id": 12
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "GotDailyRecord",
                    "id": 13
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "GotSeasonRecord",
                    "id": 14
                }
            ]
        },
        {
            "name": "PVPCardItem",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "CardID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Count",
                    "id": 2
                }
            ]
        },
        {
            "name": "PVPMatchPlayerItem",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ServerName",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Nick",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Icon",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Point",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "BossID",
                    "id": 6
                },
                {
                    "rule": "repeated",
                    "type": "PVPCardItem",
                    "name": "CardList",
                    "id": 7
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "IsRobot",
                    "id": 8
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ServerID",
                    "id": 9
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Level",
                    "id": 10
                }
            ]
        },
        {
            "name": "PVPTest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "PVPMatchRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Nick",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Icon",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Point",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "BossCardID",
                    "id": 4
                },
                {
                    "rule": "repeated",
                    "type": "PVPCardItem",
                    "name": "CardList",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ServerID",
                    "id": 6
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "MatchPoint",
                    "id": 7
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Level",
                    "id": 8
                }
            ]
        },
        {
            "name": "PVPMatchResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "PVPMatchPlayerItem",
                    "name": "Players",
                    "id": 1
                }
            ]
        },
        {
            "name": "PVPPart_MatchRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "PVPPart_MatchRet",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                }
            ]
        },
        {
            "name": "PVPPart_MatchResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "PVPMatchPlayerItem",
                    "name": "Players",
                    "id": 1
                }
            ]
        },
        {
            "name": "PVPPart_RoomStateChange",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "State",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Seconds",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "bytes",
                    "name": "Content",
                    "id": 3
                }
            ]
        },
        {
            "name": "PVPPart_BanRaceRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "Races",
                    "id": 1
                }
            ]
        },
        {
            "name": "PVPPart_BanRaceResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "Races",
                    "id": 2
                }
            ]
        },
        {
            "name": "PVPPart_BanCardsRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "Cards",
                    "id": 1
                }
            ]
        },
        {
            "name": "PVPPart_BanCardsResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "Cards",
                    "id": 2
                }
            ]
        },
        {
            "name": "PVPPart_ClothArrayRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "Cards",
                    "id": 1
                }
            ]
        },
        {
            "name": "PVPPart_ClothArrayResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "Cards",
                    "id": 2
                }
            ]
        },
        {
            "name": "PVPPart_FightRecord",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "bytes",
                    "name": "Content",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "Done",
                    "id": 2
                }
            ]
        },
        {
            "name": "PVPFightRet",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Winner",
                    "id": 1
                }
            ]
        },
        {
            "name": "PVPPart_FightRet",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "Win",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Point",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "WinTimesPoint",
                    "id": 3
                }
            ]
        },
        {
            "name": "PVPPart_Award",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ItemType",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ItemID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ItemCount",
                    "id": 3
                }
            ]
        },
        {
            "name": "PVPPart_DailyAwardRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ID",
                    "id": 1
                }
            ]
        },
        {
            "name": "PVPPart_DailyAwardResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "ID",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "PVPPart_Award",
                    "name": "Awards",
                    "id": 3
                }
            ]
        },
        {
            "name": "PVPPart_SeasonAwardRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ID",
                    "id": 1
                }
            ]
        },
        {
            "name": "PVPPart_SeasonAwardResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "ID",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "PVPPart_Award",
                    "name": "Awards",
                    "id": 3
                }
            ]
        }
    ],
    "isNamespace": true
}).build();