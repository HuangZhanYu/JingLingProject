module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "CardAddition",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "string",
                    "name": "Indexs",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "string",
                    "name": "Values",
                    "id": 2
                }
            ]
        },
        {
            "name": "CardSaveData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "string",
                    "name": "FightAttr",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "string",
                    "name": "FightSpecialAttr",
                    "id": 2
                }
            ]
        },
        {
            "name": "CardInfo",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "CardID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Level",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "EvolutionLevel",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "MaxLevel",
                    "id": 5
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "LevelUpRealCost",
                    "id": 6
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "LevelUpBaseCost",
                    "id": 7
                },
                {
                    "rule": "repeated",
                    "type": "string",
                    "name": "LevelAdditionAttr",
                    "id": 8
                },
                {
                    "rule": "repeated",
                    "type": "string",
                    "name": "QiangHuaLevelAdditionAttr",
                    "id": 9
                },
                {
                    "rule": "optional",
                    "type": "CardAddition",
                    "name": "FightAdditionAttr",
                    "id": 10
                },
                {
                    "rule": "optional",
                    "type": "CardAddition",
                    "name": "JingLingAdditionAttr",
                    "id": 12
                },
                {
                    "rule": "optional",
                    "type": "CardAddition",
                    "name": "ZhuangBeiAdditionAttr",
                    "id": 13
                },
                {
                    "rule": "repeated",
                    "type": "string",
                    "name": "BeforeAddAttr",
                    "id": 14
                },
                {
                    "rule": "repeated",
                    "type": "string",
                    "name": "FightAttr",
                    "id": 15
                },
                {
                    "rule": "repeated",
                    "type": "string",
                    "name": "FightSpecialAttr",
                    "id": 16
                },
                {
                    "rule": "repeated",
                    "type": "string",
                    "name": "WenZhangFightAttr",
                    "id": 17
                },
                {
                    "rule": "repeated",
                    "type": "string",
                    "name": "WenZhangFightSpecialAttr",
                    "id": 18
                }
            ]
        },
        {
            "name": "TreasureInfo",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "TreasureID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Level",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "BeyondLevel",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "EnhanceCostOne",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "EnhanceCostTwo",
                    "id": 6
                }
            ]
        },
        {
            "name": "ItemInfo",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ResourceType",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ResourceID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ResourceCount",
                    "id": 3
                }
            ]
        },
        {
            "name": "SendAllCardInfo",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "CardInfo",
                    "name": "CardInfoList",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Group",
                    "id": 2
                }
            ]
        },
        {
            "name": "SendAllTreasureInfo",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "TreasureInfo",
                    "name": "TreasureInfoList",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Group",
                    "id": 2
                }
            ]
        },
        {
            "name": "SendAllItemInfo",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "ItemInfo",
                    "name": "ItemInfoList",
                    "id": 1
                }
            ]
        },
        {
            "name": "UpdataCardInfo",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "CardInfo",
                    "name": "Info",
                    "id": 1
                }
            ]
        },
        {
            "name": "UpdataTreasureInfo",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "TreasureInfo",
                    "name": "Info",
                    "id": 1
                }
            ]
        },
        {
            "name": "SendLineupAllCardInfo",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "CardInfo",
                    "name": "CardInfoList",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Group",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "CurrentCapacity",
                    "id": 3
                }
            ]
        },
        {
            "name": "SendTreasureAllInfo",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "TreasureInfo",
                    "name": "TreasureInfoList",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Group",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "CurrentCapacity",
                    "id": 3
                }
            ]
        }
    ],
    "isNamespace": true
}).build();