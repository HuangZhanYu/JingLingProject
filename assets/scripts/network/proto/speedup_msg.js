module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "SpeedUpPart_PauseRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "SpeedUpPart_ResumeRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "SpeedUpPart_SpeedUpData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int64",
                    "name": "StartTime",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int64",
                    "name": "Duration",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Speed",
                    "id": 3
                }
            ]
        },
        {
            "name": "SpeedUpPart_SpeedUpDataUpdate",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "SpeedUpPart_SpeedUpData",
                    "name": "Datas",
                    "id": 1
                }
            ]
        },
        {
            "name": "SpeedUpPart_BuyRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "SpeedUpPart_BuyResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Success",
                    "id": 1
                }
            ]
        },
        {
            "name": "SpeedUpPart_ControlTimeRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "Control",
                    "id": 1
                }
            ]
        }
    ],
    "isNamespace": true
}).build();