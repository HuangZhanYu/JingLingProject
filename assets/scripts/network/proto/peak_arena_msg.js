module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "PeakArenaPart_SingleLine",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "CardList",
                    "id": 1
                }
            ]
        },
        {
            "name": "PeakArenaPart_Reward",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Type",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Count",
                    "id": 3
                }
            ]
        },
        {
            "name": "PeakArenaPart_PosChangeRecord",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "Initiative",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "bool",
                    "name": "IsWin",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "PositionChange",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "RivalObjID",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "RivalName",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RivalLevel",
                    "id": 6
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RivalIconID",
                    "id": 7
                },
                {
                    "rule": "required",
                    "type": "int64",
                    "name": "RecordTime",
                    "id": 8
                },
                {
                    "rule": "repeated",
                    "type": "string",
                    "name": "FightRecordID",
                    "id": 9
                },
                {
                    "rule": "repeated",
                    "type": "PeakArenaPart_SingleLine",
                    "name": "MyLines",
                    "id": 10
                },
                {
                    "rule": "repeated",
                    "type": "PeakArenaPart_SingleLine",
                    "name": "RivalLines",
                    "id": 11
                }
            ]
        },
        {
            "name": "PeakArenaPart_DefendRecord",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "Initiative",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "bool",
                    "name": "IsWin",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "RivalObjID",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "RivalName",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RivalLevel",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RivalIconID",
                    "id": 6
                },
                {
                    "rule": "required",
                    "type": "int64",
                    "name": "RecordTime",
                    "id": 7
                },
                {
                    "rule": "repeated",
                    "type": "string",
                    "name": "FightRecordID",
                    "id": 8
                }
            ]
        },
        {
            "name": "PeakArenaPart_PlayerArenaData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Name",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Level",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "IconID",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "MaxWenZhang",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Position",
                    "id": 6
                }
            ]
        },
        {
            "name": "PeakArenaPart_GetPointRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "PeakArenaPart_GetPointResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "AccumulatedPoint",
                    "id": 2
                }
            ]
        },
        {
            "name": "PeakArenaPart_PeakDataRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "PeakArenaPart_PeakDataResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "PeakArenaPart_SingleLine",
                    "name": "DefendLines",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "PeakArenaPart_SingleLine",
                    "name": "FightLines",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "AccumulatedPoint",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "MyPosition",
                    "id": 4
                },
                {
                    "rule": "repeated",
                    "type": "PeakArenaPart_PlayerArenaData",
                    "name": "Rivals",
                    "id": 5
                }
            ]
        },
        {
            "name": "PeakArenaPart_BattlePointData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "AccumulatedPoint",
                    "id": 1
                }
            ]
        },
        {
            "name": "PeakArenaPart_SaveLinesRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "DefendLinesOne",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "DefendLinesTwo",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "DefendLinesThree",
                    "id": 3
                }
            ]
        },
        {
            "name": "PeakArenaPart_SaveLinesResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                }
            ]
        },
        {
            "name": "PeakArenaPart_GetRivalRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Position",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 2
                }
            ]
        },
        {
            "name": "PeakArenaPart_GetRivalResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Success",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "PeakArenaPart_SingleLine",
                    "name": "DefendLines",
                    "id": 2
                }
            ]
        },
        {
            "name": "PeakArenaPart_FightRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Position",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "FightLinesOne",
                    "id": 3
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "FightLinesTwo",
                    "id": 4
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "FightLinesThree",
                    "id": 5
                }
            ]
        },
        {
            "name": "PeakArenaPart_FightResultResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Success",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "bool",
                    "name": "IsWin",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "OldPosition",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "NewPosition",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "RivalName",
                    "id": 5
                },
                {
                    "rule": "repeated",
                    "type": "bytes",
                    "name": "FightRecord",
                    "id": 6
                },
                {
                    "rule": "repeated",
                    "type": "PeakArenaPart_Reward",
                    "name": "Rewards",
                    "id": 7
                }
            ]
        },
        {
            "name": "PeakArenaPart_RecordRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "PeakArenaPart_RecordResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "PeakArenaPart_PosChangeRecord",
                    "name": "PosChanges",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "PeakArenaPart_DefendRecord",
                    "name": "Defends",
                    "id": 2
                }
            ]
        },
        {
            "name": "PeakArenaPart_BuyChallengeCountRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Count",
                    "id": 1
                }
            ]
        },
        {
            "name": "PeakArenaPart_BuyChallengeCountResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                }
            ]
        }
    ],
    "isNamespace": true
}).build();