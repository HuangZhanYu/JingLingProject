module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "SingleArenaPart_Reward",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Type",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Count",
                    "id": 3
                }
            ]
        },
        {
            "name": "SingleArenaPart_PlayerArenaData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Name",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Level",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "IconID",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "MaxWenZhang",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Position",
                    "id": 6
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "XunLianLevel",
                    "id": 7
                }
            ]
        },
        {
            "name": "SingleArenaPart_PosChangeRecord",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "Initiative",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "IsWin",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "PositionChange",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "RivalObjID",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "RivalName",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RivalLevel",
                    "id": 6
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RivalIconID",
                    "id": 7
                },
                {
                    "rule": "required",
                    "type": "int64",
                    "name": "RecordTime",
                    "id": 8
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "FightRecordID",
                    "id": 9
                }
            ]
        },
        {
            "name": "SingleArenaPart_DefendRecord",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "Initiative",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "IsWin",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "RivalObjID",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "RivalName",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RivalLevel",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RivalIconID",
                    "id": 6
                },
                {
                    "rule": "required",
                    "type": "int64",
                    "name": "RecordTime",
                    "id": 7
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "FightRecordID",
                    "id": 8
                }
            ]
        },
        {
            "name": "SingleArenaPart_TopPlayer",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Name",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Level",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "IconID",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "LikesNum",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "LikeToday",
                    "id": 6
                }
            ]
        },
        {
            "name": "SingleArenaPart_MainDataRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "SingleArenaPart_MainDataResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Position",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int64",
                    "name": "NextTime",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "SingleArenaPart_PlayerArenaData",
                    "name": "Rivals",
                    "id": 3
                },
                {
                    "rule": "repeated",
                    "type": "SingleArenaPart_Reward",
                    "name": "Rewards",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "RewardTime",
                    "id": 5
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "RewardRank",
                    "id": 6
                },
                {
                    "rule": "repeated",
                    "type": "SingleArenaPart_Reward",
                    "name": "NextRewards",
                    "id": 7
                },
                {
                    "rule": "repeated",
                    "type": "SingleArenaPart_Reward",
                    "name": "RankRewards",
                    "id": 8
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "Stat",
                    "id": 9
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "NextRewardRank",
                    "id": 10
                },
                {
                    "rule": "optional",
                    "type": "bool",
                    "name": "AwardSameDay",
                    "id": 11
                },
                {
                    "rule": "repeated",
                    "type": "SingleArenaPart_TopPlayer",
                    "name": "TopPlayers",
                    "id": 12
                }
            ]
        },
        {
            "name": "SingleArenaPart_RecordRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "SingleArenaPart_RecordResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "SingleArenaPart_PosChangeRecord",
                    "name": "PosChanges",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "SingleArenaPart_DefendRecord",
                    "name": "Defends",
                    "id": 2
                }
            ]
        },
        {
            "name": "SingleArenaPart_PosRankRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "SingleArenaPart_PosRankReSponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Position",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "SingleArenaPart_PlayerArenaData",
                    "name": "Datas",
                    "id": 2
                }
            ]
        },
        {
            "name": "SingleArenaPart_GetRivalRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Position",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 2
                }
            ]
        },
        {
            "name": "SingleArenaPart_GetRivalResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Success",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "SingleArenaPart_PlayerArenaData",
                    "name": "Rival",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "Cards",
                    "id": 3
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "QuangHuaLevel",
                    "id": 4
                }
            ]
        },
        {
            "name": "SingleArenaPart_FightRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Position",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 2
                }
            ]
        },
        {
            "name": "SingleArenaPart_FightResultResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Success",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "bool",
                    "name": "IsWin",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "SingleArenaPart_Reward",
                    "name": "Rewards",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "OldPosition",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "NewPosition",
                    "id": 5
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "RivalName",
                    "id": 6
                },
                {
                    "rule": "optional",
                    "type": "bytes",
                    "name": "FightRecord",
                    "id": 7
                },
                {
                    "rule": "optional",
                    "type": "bool",
                    "name": "Done",
                    "id": 8
                }
            ]
        },
        {
            "name": "SingleArenaPart_FightResultFollowResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "bytes",
                    "name": "FightRecord",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "Done",
                    "id": 2
                }
            ]
        },
        {
            "name": "SingleArenaPart_BuyFightTimeRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "SingleArenaPart_BuyFightTimeResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Success",
                    "id": 1
                }
            ]
        },
        {
            "name": "SingleArenaPart_FightRecordRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "RecordID",
                    "id": 1
                }
            ]
        },
        {
            "name": "SingleArenaPart_FightRecordResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Success",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "bytes",
                    "name": "FightRecord",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "bool",
                    "name": "Done",
                    "id": 3
                }
            ]
        },
        {
            "name": "SingleArenaPart_FightRecordFollowResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "bytes",
                    "name": "FightRecord",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "Done",
                    "id": 2
                }
            ]
        },
        {
            "name": "SingleArenaPart_RewardRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "SingleArenaPart_RewardResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "SingleArenaPart_Reward",
                    "name": "Rewards",
                    "id": 2
                }
            ]
        },
        {
            "name": "SingleArenaPart_LikeRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 1
                }
            ]
        },
        {
            "name": "SingleArenaPart_LikeResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "ObjID",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "LikesNum",
                    "id": 3
                }
            ]
        }
    ],
    "isNamespace": true
}).build();