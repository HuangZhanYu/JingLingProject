module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "DungeonInfo",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "DungeonID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "DungeonTopRound",
                    "id": 2
                }
            ]
        },
        {
            "name": "DungeonPart_Reward",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RewardType",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RewardID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "RewardCount",
                    "id": 3
                }
            ]
        },
        {
            "name": "DungeonPart_SendDungeonInfo",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "DungeonInfo",
                    "name": "DungeonList",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ChallengCount",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int64",
                    "name": "LastRecoverTime",
                    "id": 3
                }
            ]
        },
        {
            "name": "DungeonPart_FightRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "DungeonID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Round",
                    "id": 2
                }
            ]
        },
        {
            "name": "DungeonPart_FightResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "DungeonPart_Reward",
                    "name": "RewardList",
                    "id": 2
                }
            ]
        },
        {
            "name": "DungeonPart_SwapRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "DungeonID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Round",
                    "id": 2
                }
            ]
        },
        {
            "name": "DungeonPart_SwapResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "DungeonPart_Reward",
                    "name": "RewardList",
                    "id": 2
                }
            ]
        },
        {
            "name": "DungeonPart_OnBeginFightVerifyRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "DungeonID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RoundID",
                    "id": 2
                }
            ]
        },
        {
            "name": "DungeonPart_BeginFightVerifyResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "FightID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RandSeed",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "DungeonPart_Reward",
                    "name": "RewardList",
                    "id": 3
                }
            ]
        },
        {
            "name": "DungeonPart_StartFightVerifyRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "FightID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "FightResult",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "bytes",
                    "name": "Input",
                    "id": 3
                }
            ]
        },
        {
            "name": "DungeonPart_StartFightVerifyResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "IsWin",
                    "id": 1
                }
            ]
        },
        {
            "name": "DungeonPart_JumpRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "DungeonID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Round",
                    "id": 2
                }
            ]
        },
        {
            "name": "DungeonPart_JumpResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                }
            ]
        }
    ],
    "isNamespace": true
}).build();