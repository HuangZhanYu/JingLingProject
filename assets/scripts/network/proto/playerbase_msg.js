module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "PlayerBaseResource",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Type",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Count",
                    "id": 3
                }
            ]
        },
        {
            "name": "PlayerBase",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "int64",
                    "name": "IntAttr",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "string",
                    "name": "StrAttr",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "int64",
                    "name": "TimeAttr",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 4
                }
            ]
        },
        {
            "name": "PlayerBase_IntChange",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "IntAttrIndex",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "int64",
                    "name": "IntAttr",
                    "id": 2
                }
            ]
        },
        {
            "name": "PlayerBase_StrChange",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "StrAttrIndex",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "string",
                    "name": "StrAttr",
                    "id": 2
                }
            ]
        },
        {
            "name": "PlayerBase_TimeChange",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "TimeAttrIndex",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "int64",
                    "name": "TimeAttr",
                    "id": 2
                }
            ]
        },
        {
            "name": "PlayerBase_AttrChange",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "optional",
                    "type": "PlayerBase_IntChange",
                    "name": "IntAttr",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "PlayerBase_StrChange",
                    "name": "StrAttr",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "PlayerBase_TimeChange",
                    "name": "TimeAttr",
                    "id": 3
                }
            ]
        },
        {
            "name": "PlayerBase_HeadIconInfo",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "IconID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "LockState",
                    "id": 2
                }
            ]
        },
        {
            "name": "PlayerBase_LevelUp",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "OldLevel",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "NewLevel",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "PlayerBaseResource",
                    "name": "Resources",
                    "id": 3
                }
            ]
        },
        {
            "name": "PlayerBase_ChangePortraitRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "PortraitIndex",
                    "id": 1
                }
            ]
        },
        {
            "name": "PlayerBase_ChangePortraitResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "PortraitIndex",
                    "id": 2
                }
            ]
        },
        {
            "name": "PlayerBase_ChangeNameRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Name",
                    "id": 1
                }
            ]
        },
        {
            "name": "PlayerBase_ChangeNameResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Name",
                    "id": 2
                }
            ]
        },
        {
            "name": "PlayerBase_ChangeMainCardRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "PlayerBase_ChangeMainCardResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                }
            ]
        },
        {
            "name": "PlayerBase_HeadIconResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "PlayerBase_HeadIconInfo",
                    "name": "HeadIcons",
                    "id": 2
                }
            ]
        },
        {
            "name": "PlayerBase_OffLineResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int64",
                    "name": "OffLineTime",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "OffLineRoundBefore",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "OffLineRound",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "OffLineGold",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 5
                }
            ]
        },
        {
            "name": "PlayerBase_OnAppPauseRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "PlayerBase_OnAppResumeRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "PlayerBase_TeamUpRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Count",
                    "id": 1
                }
            ]
        },
        {
            "name": "PlayerBase_TeamUpResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                }
            ]
        }
    ],
    "isNamespace": true
}).build();