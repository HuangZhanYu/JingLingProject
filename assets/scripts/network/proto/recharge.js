module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "RechargeData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "OrderNo",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "OrderState",
                    "id": 2
                }
            ]
        },
        {
            "name": "SyncRechargeData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "RechargeData",
                    "name": "RechargeDataList",
                    "id": 1
                }
            ]
        },
        {
            "name": "RechargeReward",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ResourceType",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ResourceID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ResourceCount",
                    "id": 3
                }
            ]
        },
        {
            "name": "RechargeReponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "MsgCode",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "RechargeReward",
                    "name": "RewardList",
                    "id": 2
                }
            ]
        }
    ],
    "isNamespace": true
}).build();