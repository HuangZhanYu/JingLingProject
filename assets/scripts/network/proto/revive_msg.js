module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "RevivePart_InfoRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "RevivePart_LoginResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "IDs",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "AutoReviveFlag",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "GotIDs",
                    "id": 3
                }
            ]
        },
        {
            "name": "RevivePart_ReviveRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ReviveType",
                    "id": 1
                }
            ]
        },
        {
            "name": "RevivePart_ReviveResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "Types",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "IDs",
                    "id": 3
                },
                {
                    "rule": "repeated",
                    "type": "string",
                    "name": "Counts",
                    "id": 4
                }
            ]
        },
        {
            "name": "RevivePart_AutoReviveRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "Flag",
                    "id": 1
                }
            ]
        },
        {
            "name": "RevivePart_AutoReviveResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "Flag",
                    "id": 1
                }
            ]
        },
        {
            "name": "RevivePart_FastRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "ReviveType",
                    "id": 1
                }
            ]
        },
        {
            "name": "RevivePart_FastResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Success",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "Types",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "IDs",
                    "id": 3
                },
                {
                    "rule": "repeated",
                    "type": "string",
                    "name": "Counts",
                    "id": 4
                }
            ]
        },
        {
            "name": "RevivePart_GetAwardRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ID",
                    "id": 1
                }
            ]
        },
        {
            "name": "RevivePart_GetAwardResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "Types",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "IDs",
                    "id": 3
                },
                {
                    "rule": "repeated",
                    "type": "string",
                    "name": "Counts",
                    "id": 4
                }
            ]
        }
    ],
    "isNamespace": true
}).build();