module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "TaskPart_DailyTask",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "TaskId",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "TaskLevel",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "IsOpenAutoClear",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "OnceReward",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "OnceRewardAfter",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "OnceRewardReal",
                    "id": 6
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "UpLevelCost",
                    "id": 7
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "UpLevelCostReal",
                    "id": 8
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "OpenTaskCost",
                    "id": 9
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "UpLevelAddReward",
                    "id": 10
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "UpLevelAddRewardAfter",
                    "id": 11
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "UpLevelAddRewardReal",
                    "id": 12
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "BaseTime",
                    "id": 13
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "UpLevelTenCost",
                    "id": 14
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "UpLevelTenCostReal",
                    "id": 15
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "UpLevelHunCost",
                    "id": 16
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "UpLevelHunCostReal",
                    "id": 17
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "UpLevelHunCostNum",
                    "id": 18
                }
            ]
        },
        {
            "name": "TaskPart_Reward",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ResourceType",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ResourceID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ResourceCount",
                    "id": 3
                }
            ]
        },
        {
            "name": "TaskPart_TaskList",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "TaskPart_DailyTask",
                    "name": "DailyTaskList",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "CurrentAutoId",
                    "id": 2
                }
            ]
        },
        {
            "name": "TaskPart_TaskLessTime",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "LessTimeList",
                    "id": 1
                }
            ]
        },
        {
            "name": "TaskPart_TaskLessTimeRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "LessTimeList",
                    "id": 1
                }
            ]
        },
        {
            "name": "TaskPart_TaskCanBuyGold",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Gold",
                    "id": 1
                }
            ]
        },
        {
            "name": "TaskPart_CurrentRealTime",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "TimeList",
                    "id": 1
                }
            ]
        },
        {
            "name": "TaskPart_AddGoldRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "TaskId",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Count",
                    "id": 2
                }
            ]
        },
        {
            "name": "TaskPart_AddGoldResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Gold",
                    "id": 2
                }
            ]
        },
        {
            "name": "TaskPart_UpgradeTaskLvRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "TaskId",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Count",
                    "id": 2
                }
            ]
        },
        {
            "name": "TaskPart_UpgradeTaskLvResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "TaskId",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "TaskPart_DailyTask",
                    "name": "TaskData",
                    "id": 3
                }
            ]
        },
        {
            "name": "TaskPart_AutoTaskRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Type",
                    "id": 1
                }
            ]
        },
        {
            "name": "TaskPart_AutoTaskResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "CurrentAutoID",
                    "id": 2
                }
            ]
        },
        {
            "name": "TaskPart_BuyGoldRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "TaskPart_BuyGoldResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                }
            ]
        }
    ],
    "isNamespace": true
}).build();