module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "PlayerAttr_CardAddItem",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Index",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Value",
                    "id": 2
                }
            ]
        },
        {
            "name": "PlayerAttr_CardAddition",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "PlayerAttr_CardAddItem",
                    "name": "Values",
                    "id": 1
                }
            ]
        },
        {
            "name": "PlayerAttr_CampData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "PlayerAttr_CardAddition",
                    "name": "FightAttr",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "PlayerAttr_CardAddItem",
                    "name": "MasterAttr",
                    "id": 2
                }
            ]
        },
        {
            "name": "PlayerAttr_Update",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "Indexs",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "PlayerAttr_CampData",
                    "name": "Datas",
                    "id": 2
                }
            ]
        },
        {
            "name": "PlayerAttr_All",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "PlayerAttr_CampData",
                    "name": "Updates",
                    "id": 1
                }
            ]
        },
        {
            "name": "PlayerAttr_SendAddAttrTips",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Type",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "Params",
                    "id": 2
                }
            ]
        },
        {
            "name": "PlayerAttr_StoneData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "StoneID",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "PlayerAttr_CardAddition",
                    "name": "Attrs",
                    "id": 2
                }
            ]
        },
        {
            "name": "PlayerAttr_StoneUpdate",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "PlayerAttr_StoneData",
                    "name": "Datas",
                    "id": 1
                }
            ]
        }
    ],
    "isNamespace": true
}).build();