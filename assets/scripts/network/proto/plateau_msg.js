module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "PlateauPart_RoundData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Stars",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "IsTodayPass",
                    "id": 2
                }
            ]
        },
        {
            "name": "PlateauPart_ChapterData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "PlateauPart_RoundData",
                    "name": "RoundList",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "BoxIndex",
                    "id": 2
                }
            ]
        },
        {
            "name": "PlateauPart_DeadCardData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "CardID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Count",
                    "id": 2
                }
            ]
        },
        {
            "name": "PlateauPart_ChapterDataRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ChapterIndex",
                    "id": 1
                }
            ]
        },
        {
            "name": "PlateauPart_ChapterDataResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "PlateauPart_ChapterData",
                    "name": "ChapterData",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "bool",
                    "name": "ChapterRedArr",
                    "id": 3
                }
            ]
        },
        {
            "name": "PlateauPart_SendLineData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "CardList",
                    "id": 1
                }
            ]
        },
        {
            "name": "PlateauPart_SendDeadCardData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "PlateauPart_DeadCardData",
                    "name": "DeadCardList",
                    "id": 1
                }
            ]
        },
        {
            "name": "PlateauPart_SendUnPassData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "string",
                    "name": "HpList",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "PlateauID",
                    "id": 2
                }
            ]
        },
        {
            "name": "PlateauPart_CheckLineRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "PlateauID",
                    "id": 1
                }
            ]
        },
        {
            "name": "PlateauPart_CheckLineResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                }
            ]
        },
        {
            "name": "PlateauPart_FightResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "bytes",
                    "name": "Content",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "IsWin",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "Done",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Stars",
                    "id": 5
                }
            ]
        },
        {
            "name": "PlateauPart_FightAppendResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "bytes",
                    "name": "Content",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "Done",
                    "id": 2
                }
            ]
        },
        {
            "name": "PlateauPart_SendRoundData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Round",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Stars",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "IsTodayPass",
                    "id": 3
                }
            ]
        },
        {
            "name": "PlateauPart_BuyChallengeCountRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Count",
                    "id": 1
                }
            ]
        },
        {
            "name": "PlateauPart_BuyChallengeCountResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                }
            ]
        },
        {
            "name": "PlateauPart_SwapRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ChapterID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RoundID",
                    "id": 2
                }
            ]
        },
        {
            "name": "PlateauPart_SwapResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                }
            ]
        },
        {
            "name": "PlateauPart_GetWaysDataRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ConfID",
                    "id": 1
                }
            ]
        },
        {
            "name": "PlateauPart_GetWaysDataResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "PlateauPart_RoundData",
                    "name": "DataList",
                    "id": 2
                }
            ]
        },
        {
            "name": "PlateauPart_GetRewardRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Chapter",
                    "id": 1
                }
            ]
        },
        {
            "name": "PlateauPart_GetRewardResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RewardID",
                    "id": 2
                }
            ]
        },
        {
            "name": "PlateauPart_OnBeginFightVerifyRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ChapterID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RoundID",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "LineCardList",
                    "id": 3
                }
            ]
        },
        {
            "name": "PlateauPart_BeginFightVerifyResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "FightID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RandSeed",
                    "id": 2
                }
            ]
        },
        {
            "name": "PlateauPart_StartFightVerifyRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "FightID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "FightResult",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "bytes",
                    "name": "Input",
                    "id": 3
                }
            ]
        },
        {
            "name": "PlateauPart_StartFightVerifyResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "IsWin",
                    "id": 1
                }
            ]
        }
    ],
    "isNamespace": true
}).build();