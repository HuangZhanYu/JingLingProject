module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "CatchPetPart_PetStateInfo",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "EffectID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "LeftRound",
                    "id": 2
                }
            ]
        },
        {
            "name": "CatchPetPart_CatchInfo",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "PetID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "PetHP",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 3
                },
                {
                    "rule": "repeated",
                    "type": "CatchPetPart_PetStateInfo",
                    "name": "StateList",
                    "id": 4
                }
            ]
        },
        {
            "name": "CatchPetPart_SceneInfo",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ScenePosition",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "SceneID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "PetOneID",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "PetTwoID",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "NormalSearchCount",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "HighSearchCount",
                    "id": 6
                }
            ]
        },
        {
            "name": "CatchPetPart_Reward",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RewardType",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RewardID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "RewardCount",
                    "id": 3
                }
            ]
        },
        {
            "name": "CatchPetPart_RequestAllData",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "CatchPetPart_ResponseAllData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int64",
                    "name": "LastRecoveryTime",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RefreshCount",
                    "id": 3
                },
                {
                    "rule": "repeated",
                    "type": "CatchPetPart_SceneInfo",
                    "name": "CatchScenesList",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "HasUsedFreshCard",
                    "id": 5
                }
            ]
        },
        {
            "name": "CatchPetPart_RefreshRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RefreshType",
                    "id": 1
                }
            ]
        },
        {
            "name": "CatchPetPart_RefreshResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                }
            ]
        },
        {
            "name": "CatchPetPart_SearchRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "SearchType",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Position",
                    "id": 2
                }
            ]
        },
        {
            "name": "CatchPetPart_SearchResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "CatchPetPart_Reward",
                    "name": "RewardList",
                    "id": 2
                }
            ]
        },
        {
            "name": "CatchPetPart_EnterCatchFightRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ScenePosition",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "HeroPosition",
                    "id": 2
                }
            ]
        },
        {
            "name": "CatchPetPart_EnterCatchFightResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "CatchPetPart_CatchInfo",
                    "name": "EnemyPet",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "CatchPetPart_CatchInfo",
                    "name": "CurePet",
                    "id": 3
                },
                {
                    "rule": "repeated",
                    "type": "CatchPetPart_CatchInfo",
                    "name": "MyTeam",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "CatchRound",
                    "id": 5
                }
            ]
        },
        {
            "name": "CatchPetPart_UseCatchSkillRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "SkillID",
                    "id": 1
                }
            ]
        },
        {
            "name": "CatchPetPart_UseCatchSkillResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                }
            ]
        },
        {
            "name": "CatchPetPart_ChangePetRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 1
                }
            ]
        },
        {
            "name": "CatchPetPart_ChangePetResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                }
            ]
        },
        {
            "name": "CatchPetPart_UseBallRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "BallID",
                    "id": 1
                }
            ]
        },
        {
            "name": "CatchPetPart_UseBallResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "IsRun",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "CatchPetPart_Reward",
                    "name": "RewardList",
                    "id": 3
                }
            ]
        },
        {
            "name": "CatchPetPart_RunRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "CatchPetPart_RunResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                }
            ]
        },
        {
            "name": "CatchPetPart_ResponseTimeData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int64",
                    "name": "LastRecoveryTime",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RefreshCount",
                    "id": 3
                }
            ]
        },
        {
            "name": "CatchPetPart_BuyBallRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ResourceID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Count",
                    "id": 2
                }
            ]
        },
        {
            "name": "CatchPetPart_BuyBallResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                }
            ]
        },
        {
            "name": "CatchPetPart_OneKeyBuyPetRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ScenePosition",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "HeroPosition",
                    "id": 2
                }
            ]
        },
        {
            "name": "CatchPetPart_OneKeyBuyPetResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RealCostBall",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "CardID",
                    "id": 3
                },
                {
                    "rule": "repeated",
                    "type": "CatchPetPart_Reward",
                    "name": "RewardList",
                    "id": 4
                }
            ]
        },
        {
            "name": "CatchPetPart_SendCatchTag",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "IsInCatch",
                    "id": 1
                }
            ]
        }
    ],
    "isNamespace": true
}).build();