module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "XinShouRenWuInfo",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "CureRenWuID",
                    "id": 1
                }
            ]
        },
        {
            "name": "XinShouRenWuRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "XinShouRenWuResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "CureRenWuID",
                    "id": 1
                }
            ]
        },
        {
            "name": "ResetRenWuInfo",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "CureRenWuID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "TargetNum",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "RenWuState",
                    "id": 3
                }
            ]
        },
        {
            "name": "ResetRenWuRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RenWuID",
                    "id": 1
                }
            ]
        },
        {
            "name": "ResetRenWuResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "CureRenWuID",
                    "id": 1
                }
            ]
        },
        {
            "name": "DayTaskInfo",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Progress",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "State",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "TaskID",
                    "id": 3
                }
            ]
        },
        {
            "name": "AllDayTaskInfo",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "DayTaskInfo",
                    "name": "Info",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "GotTimesAward",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Times",
                    "id": 3
                }
            ]
        },
        {
            "name": "DayTaskInfoRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "DayTaskRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "DayTaskID",
                    "id": 1
                }
            ]
        },
        {
            "name": "DayTaskResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "DayTaskID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "State",
                    "id": 2
                }
            ]
        },
        {
            "name": "TimesAwardRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Times",
                    "id": 1
                }
            ]
        },
        {
            "name": "TimesAwardResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "Times",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "GetTimes",
                    "id": 3
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "GotTimesAward",
                    "id": 4
                }
            ]
        }
    ],
    "isNamespace": true
}).build();