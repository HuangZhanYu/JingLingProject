module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "LandgravePart_Sync",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "TotalRobTimes",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "MaxSeizeNum",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "UnlockLands",
                    "id": 3
                }
            ]
        },
        {
            "name": "LandgravePart_Vector2",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "X",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Y",
                    "id": 2
                }
            ]
        },
        {
            "name": "LandgravePart_Award",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ItemType",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ItemID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ItemNum",
                    "id": 3
                }
            ]
        },
        {
            "name": "LandgravePart_MegaData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "AttrID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "TargetID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Value",
                    "id": 3
                }
            ]
        },
        {
            "name": "LandgravePart_SeizeGrid",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "LandgravePart_Vector2",
                    "name": "Pos",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "Lineup",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Level",
                    "id": 3
                },
                {
                    "rule": "repeated",
                    "type": "string",
                    "name": "HourOutput",
                    "id": 4
                },
                {
                    "rule": "repeated",
                    "type": "string",
                    "name": "TotalOutput",
                    "id": 5
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "SeizeTime",
                    "id": 6
                }
            ]
        },
        {
            "name": "LandgravePart_Lineup",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "Lineup",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Status",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "LstTime",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "TotalTime",
                    "id": 4
                }
            ]
        },
        {
            "name": "LandgravePart_InfoRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "Appoint",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "LandgravePart_Vector2",
                    "name": "Pos",
                    "id": 2
                }
            ]
        },
        {
            "name": "LandgravePart_GridLord",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "ObjID",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "Nick",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "GuildID",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "GuildName",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "GuildIcon",
                    "id": 5
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "GuildLevel",
                    "id": 6
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "Lineup",
                    "id": 7
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "PlayerLevel",
                    "id": 8
                },
                {
                    "rule": "repeated",
                    "type": "LandgravePart_MegaData",
                    "name": "MegaAttrList",
                    "id": 9
                }
            ]
        },
        {
            "name": "LandgravePart_Grid",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "LandgravePart_Vector2",
                    "name": "Pos",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Level",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "LandgravePart_GridLord",
                    "name": "Lord",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "LineupID",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "LineupLevel",
                    "id": 5
                },
                {
                    "rule": "optional",
                    "type": "bool",
                    "name": "HasAward",
                    "id": 6
                },
                {
                    "rule": "repeated",
                    "type": "LandgravePart_Award",
                    "name": "Awards",
                    "id": 7
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "IconID",
                    "id": 8
                },
                {
                    "rule": "repeated",
                    "type": "LandgravePart_Award",
                    "name": "OutputAwards",
                    "id": 9
                }
            ]
        },
        {
            "name": "LandgravePart_InfoResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Point",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "GuildPoint",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Weight",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Height",
                    "id": 4
                },
                {
                    "rule": "repeated",
                    "type": "LandgravePart_SeizeGrid",
                    "name": "SeizeGrids",
                    "id": 5
                },
                {
                    "rule": "repeated",
                    "type": "LandgravePart_Grid",
                    "name": "CatGrids",
                    "id": 6
                },
                {
                    "rule": "repeated",
                    "type": "LandgravePart_Lineup",
                    "name": "Lineup",
                    "id": 7
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "GvCDEndTime",
                    "id": 8
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "GuildLevel",
                    "id": 9
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "EndTime",
                    "id": 10
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "HasOutput",
                    "id": 11
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "LstRobTimes",
                    "id": 12
                }
            ]
        },
        {
            "name": "LandgravePart_CatGridRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "LandgravePart_Vector2",
                    "name": "Pos",
                    "id": 1
                }
            ]
        },
        {
            "name": "LandgravePart_CatGridResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "LandgravePart_Grid",
                    "name": "Grid",
                    "id": 1
                }
            ]
        },
        {
            "name": "LandgravePart_InfoNoGridsRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "LandgravePart_InfoNoGridsResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Point",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "GuildPoint",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "LandgravePart_SeizeGrid",
                    "name": "SeizeGrids",
                    "id": 3
                },
                {
                    "rule": "repeated",
                    "type": "LandgravePart_Lineup",
                    "name": "Lineup",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "GvCDEndTime",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "GuildLevel",
                    "id": 6
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "EndTime",
                    "id": 7
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "HasOutput",
                    "id": 8
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "LstRobTimes",
                    "id": 9
                }
            ]
        },
        {
            "name": "LandgravePart_AbleSORequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "LandgravePart_Vector2",
                    "name": "Pos",
                    "id": 1
                }
            ]
        },
        {
            "name": "LandgravePart_AbleSOResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                }
            ]
        },
        {
            "name": "LandgravePart_FightRecord",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "bytes",
                    "name": "Content",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "Done",
                    "id": 2
                }
            ]
        },
        {
            "name": "LandgravePart_SeizeRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "LandgravePart_Vector2",
                    "name": "Pos",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "Lineup",
                    "id": 2
                }
            ]
        },
        {
            "name": "LandgravePart_SeizeResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "LandgravePart_Award",
                    "name": "Awards",
                    "id": 2
                }
            ]
        },
        {
            "name": "LandgravePart_RobRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "LandgravePart_Vector2",
                    "name": "Pos",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "Lineup",
                    "id": 2
                }
            ]
        },
        {
            "name": "LandgravePart_RobResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "LandgravePart_Award",
                    "name": "Awards",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "LstRobTimes",
                    "id": 3
                }
            ]
        },
        {
            "name": "LandgravePart_GiveupRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "LandgravePart_Vector2",
                    "name": "Pos",
                    "id": 1
                }
            ]
        },
        {
            "name": "LandgravePart_GiveupResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "LandgravePart_Grid",
                    "name": "Grid",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "LandgravePart_Award",
                    "name": "Awards",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "GvCDEndTime",
                    "id": 4
                }
            ]
        },
        {
            "name": "LandgravePart_UnlockRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "LandID",
                    "id": 1
                }
            ]
        },
        {
            "name": "LandgravePart_UnlockResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "UnlockLands",
                    "id": 2
                }
            ]
        },
        {
            "name": "LandgravePart_GetAwardRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "LandgravePart_Vector2",
                    "name": "Pos",
                    "id": 1
                }
            ]
        },
        {
            "name": "LandgravePart_GetAwardResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "LandgravePart_Award",
                    "name": "Awards",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "Point",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "GuildPoint",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "bool",
                    "name": "HasOutput",
                    "id": 5
                }
            ]
        },
        {
            "name": "LandgravePart_PointRankRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "LandgravePart_PointRankItem",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Nick",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Icon",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Level",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Point",
                    "id": 5
                }
            ]
        },
        {
            "name": "LandgravePart_PointRankResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "LandgravePart_PointRankItem",
                    "name": "RankList",
                    "id": 1
                }
            ]
        },
        {
            "name": "LandgravePart_GPointRankRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "LandgravePart_GPointRankItem",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "GuildID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "GuildName",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "GuildIcon",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "GuildLevel",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "GuildPoint",
                    "id": 5
                }
            ]
        },
        {
            "name": "LandgravePart_GPointRankResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "LandgravePart_GPointRankItem",
                    "name": "RankList",
                    "id": 1
                }
            ]
        },
        {
            "name": "LandgravePart_ReviveRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ItemID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ItemNum",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Index",
                    "id": 3
                }
            ]
        },
        {
            "name": "LandgravePart_ReviveResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "LandgravePart_Lineup",
                    "name": "Lineup",
                    "id": 2
                }
            ]
        },
        {
            "name": "LandgravePart_RecordsRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "Win",
                    "id": 1
                }
            ]
        },
        {
            "name": "LandgravePart_Record",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "EnemyID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "EnemyName",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "EnemyIcon",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "EnemyLevel",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "IsAttacker",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "FightT",
                    "id": 6
                },
                {
                    "rule": "repeated",
                    "type": "LandgravePart_Award",
                    "name": "Awards",
                    "id": 7
                },
                {
                    "rule": "required",
                    "type": "LandgravePart_Vector2",
                    "name": "Pos",
                    "id": 8
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "PassedSeconds",
                    "id": 9
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "BattleID",
                    "id": 10
                }
            ]
        },
        {
            "name": "LandgravePart_RecordsResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "LandgravePart_Record",
                    "name": "Records",
                    "id": 1
                }
            ]
        },
        {
            "name": "LandgravePart_ReviewFightRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "BattleID",
                    "id": 1
                }
            ]
        },
        {
            "name": "LandgravePart_AwardGridRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "LandgravePart_AwardGridResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "LandgravePart_Vector2",
                    "name": "Pos",
                    "id": 2
                }
            ]
        },
        {
            "name": "LandgravePart_StatRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "LandgravePart_StatResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Status",
                    "id": 1
                }
            ]
        },
        {
            "name": "LandgravePart_ErrCodeSync",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                }
            ]
        }
    ],
    "isNamespace": true
}).build();