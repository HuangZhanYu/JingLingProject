module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "WizardCollPart_Award",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ItemType",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ItemID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ItemNum",
                    "id": 3
                }
            ]
        },
        {
            "name": "WizardCollPart_InfoRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "WizardCollPart_Item",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "SubID",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "bool",
                    "name": "Stats",
                    "id": 3
                }
            ]
        },
        {
            "name": "WizardCollPart_InfoResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "WizardCollPart_Item",
                    "name": "List",
                    "id": 1
                }
            ]
        },
        {
            "name": "WizardCollPart_AwardRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "SubID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Index",
                    "id": 3
                }
            ]
        },
        {
            "name": "WizardCollPart_AwardResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "ID",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "SubID",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "Index",
                    "id": 4
                },
                {
                    "rule": "repeated",
                    "type": "WizardCollPart_Award",
                    "name": "Awards",
                    "id": 5
                }
            ]
        },
        {
            "name": "WizardCollPart_NewAward",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Index",
                    "id": 2
                }
            ]
        }
    ],
    "isNamespace": true
}).build();