module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "GuanQiaPart_LoginResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "HistoryTopLevel",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "CurLevel",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "CurTopLevel",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "FightID",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "RandSeed",
                    "id": 5
                },
                {
                    "rule": "optional",
                    "type": "bool",
                    "name": "IsReseting",
                    "id": 6
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "AutoOpenCount",
                    "id": 7
                },
                {
                    "rule": "optional",
                    "type": "bool",
                    "name": "OfflineReward",
                    "id": 8
                }
            ]
        },
        {
            "name": "GuanQiaPart_BaoXiangInfoResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "HasBaoXiangLevel",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "bool",
                    "name": "BoxAward",
                    "id": 2
                }
            ]
        },
        {
            "name": "GuanQiaPart_GetBaoXiangRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Level",
                    "id": 1
                }
            ]
        },
        {
            "name": "GuanQiaPart_GetBaoXiangResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Success",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "Level",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "ResourceID",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "ResourceType",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "ResourceCount",
                    "id": 5
                },
                {
                    "rule": "optional",
                    "type": "bool",
                    "name": "BoxAward",
                    "id": 6
                }
            ]
        },
        {
            "name": "GuanQiaPart_XiaoGuaiJinBiRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "KillID",
                    "id": 1
                }
            ]
        },
        {
            "name": "GuanQiaPart_FinishFightRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "GuanQiaID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "FightResult",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "FightID",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "bytes",
                    "name": "Input",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "FrameCount",
                    "id": 5
                }
            ]
        },
        {
            "name": "GuanQiaPart_FinishFightResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "CurLevel",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "FightID",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "RandSeed",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "bool",
                    "name": "BoxAward",
                    "id": 4
                }
            ]
        },
        {
            "name": "GuanQiaPart_HistoryTopLevelChangeResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Level",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Exp",
                    "id": 2
                }
            ]
        },
        {
            "name": "GuanQiaPart_CurTopLevelChangeResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "CurTopLevel",
                    "id": 1
                }
            ]
        },
        {
            "name": "GuanQiaPart_TestRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Level",
                    "id": 1
                }
            ]
        },
        {
            "name": "GuanQiaPart_TriggerResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "IDs",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "Types",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "string",
                    "name": "Counts",
                    "id": 3
                }
            ]
        },
        {
            "name": "GuanQiaPart_OfflineRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "GuanQiaPart_OffLineResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int64",
                    "name": "OffLineTime",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "LstLevel",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "IDs",
                    "id": 3
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "Types",
                    "id": 4
                },
                {
                    "rule": "repeated",
                    "type": "string",
                    "name": "Counts",
                    "id": 5
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "ExtraID",
                    "id": 6
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "ExtraType",
                    "id": 7
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "ExtraCount",
                    "id": 8
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "CurLevel",
                    "id": 9
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ReviveTime",
                    "id": 10
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "ReviveWenZhang",
                    "id": 11
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "GuanQiaWenZhang",
                    "id": 13
                }
            ]
        },
        {
            "name": "GuanQiaPart_OfflineAwardRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "GuanQiaPart_OfflineAwardResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "IDs",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "Types",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "string",
                    "name": "Counts",
                    "id": 3
                }
            ]
        },
        {
            "name": "GuanQiaPart_OffLineYuLanRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "GuanQiaPart_AutoOpenCountResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "AutoOpenCount",
                    "id": 1
                }
            ]
        },
        {
            "name": "GuanQiaPart_OffLineYuLanResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "MaxOffLineLevel",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Speed",
                    "id": 2
                }
            ]
        },
        {
            "name": "GuanQiaPart_PassLevelResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "Level",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "Pass",
                    "id": 2
                }
            ]
        }
    ],
    "isNamespace": true
}).build();