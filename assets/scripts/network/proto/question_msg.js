module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "PlayerAnswerRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "Answer",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "QuestionID",
                    "id": 2
                }
            ]
        },
        {
            "name": "PlayerQuestionData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "CureQuestion",
                    "id": 1
                }
            ]
        }
    ],
    "isNamespace": true
}).build();