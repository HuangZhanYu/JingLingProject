module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "HongDianRelateData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Enum",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "RelateEnum",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "bool",
                    "name": "Active",
                    "id": 3
                }
            ]
        },
        {
            "name": "HongDianRelateResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "HongDianRelateData",
                    "name": "Data",
                    "id": 1
                }
            ]
        }
    ],
    "isNamespace": true
}).build();