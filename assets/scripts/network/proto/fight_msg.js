module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "Vector2D",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "float",
                    "name": "x",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "float",
                    "name": "y",
                    "id": 2
                }
            ]
        },
        {
            "name": "BornRecord",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "UUID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Camp",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "Vector2D",
                    "name": "Pos",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "MaxHP",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Type",
                    "id": 6
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "HP",
                    "id": 7
                }
            ]
        },
        {
            "name": "ReviveRecord",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "UUID",
                    "id": 1
                }
            ]
        },
        {
            "name": "DieRecord",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "UUID",
                    "id": 1
                }
            ]
        },
        {
            "name": "MoveRecord",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "UUID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "TargetUUID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "float",
                    "name": "Speed",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "Vector2D",
                    "name": "Pos",
                    "id": 4
                }
            ]
        },
        {
            "name": "AttackRecord",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "UUID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "TargetUUID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "SkillID",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "float",
                    "name": "ASPD",
                    "id": 4
                }
            ]
        },
        {
            "name": "StandRecord",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "UUID",
                    "id": 1
                }
            ]
        },
        {
            "name": "AddHPRecord",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "UUID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "HP",
                    "id": 2
                }
            ]
        },
        {
            "name": "HurtRecord",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "UUID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Hurt",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "HurtSrc",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "HurtSrcID",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "MasterUUID",
                    "id": 5
                }
            ]
        },
        {
            "name": "AddBuffRecord",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "UUID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "BuffID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "LstTime",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "EffectV",
                    "id": 4
                }
            ]
        },
        {
            "name": "RmvBuffRecord",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "UUID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "BuffID",
                    "id": 2
                }
            ]
        },
        {
            "name": "HudRecord",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "UUID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Type",
                    "id": 2
                }
            ]
        },
        {
            "name": "FlyItemBornRecord",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "UUID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "TargetUUID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "FlyItemID",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "FlyTime",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "SkillID",
                    "id": 5
                }
            ]
        },
        {
            "name": "TeleportRecord",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "UUID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "Vector2D",
                    "name": "Pos",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Time",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "SkillID",
                    "id": 4
                }
            ]
        },
        {
            "name": "UseASkillRecord",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "SkillID",
                    "id": 1
                }
            ]
        },
        {
            "name": "RepelRecord",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "UUID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "BuffID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "float",
                    "name": "Height",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "float",
                    "name": "Length",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "LstTime",
                    "id": 5
                }
            ]
        },
        {
            "name": "CrossFlyItemRecord",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "UUID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "FlyItemID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "float",
                    "name": "Speed",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "float",
                    "name": "Distance",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "SkillID",
                    "id": 5
                }
            ]
        },
        {
            "name": "SprintRecord",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "UUID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "TargetUUID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "SkillID",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "float",
                    "name": "ASPD",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "float",
                    "name": "Speed",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "Vector2D",
                    "name": "Pos",
                    "id": 6
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "UsePos",
                    "id": 7
                }
            ]
        },
        {
            "name": "SprintFinishRecord",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "UUID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "Vector2D",
                    "name": "Pos",
                    "id": 2
                }
            ]
        },
        {
            "name": "PullRecord",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "UUID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "TargetUUID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "float",
                    "name": "Speed",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "Vector2D",
                    "name": "Pos",
                    "id": 4
                }
            ]
        },
        {
            "name": "PullFinishRecord",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "UUID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "Vector2D",
                    "name": "Pos",
                    "id": 2
                }
            ]
        },
        {
            "name": "PreHurtRecord",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "UUID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "SkillID",
                    "id": 2
                }
            ]
        },
        {
            "name": "FightRecord",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RecordT",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "bytes",
                    "name": "Content",
                    "id": 2
                }
            ]
        },
        {
            "name": "FrameRecords",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Frame",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "FightRecord",
                    "name": "Records",
                    "id": 2
                }
            ]
        },
        {
            "name": "FightTeam",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "RoleList",
                    "id": 2
                }
            ]
        },
        {
            "name": "FightRecordsFollow",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "FrameRecords",
                    "name": "FrameList",
                    "id": 1
                }
            ]
        },
        {
            "name": "FightRecords",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "FightTeam",
                    "name": "TeamList",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Duration",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RandSeed",
                    "id": 3
                },
                {
                    "rule": "repeated",
                    "type": "FrameRecords",
                    "name": "FrameList",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "WinTeam",
                    "id": 5
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "PackageNum",
                    "id": 6
                }
            ]
        },
        {
            "name": "InputItem",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Camp",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "SkillID",
                    "id": 2
                }
            ]
        },
        {
            "name": "FrameInputList",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Frame",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "InputItem",
                    "name": "FrameList",
                    "id": 2
                }
            ]
        },
        {
            "name": "BornBuffItem",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "BuffID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "LstTm",
                    "id": 3
                },
                {
                    "rule": "repeated",
                    "type": "string",
                    "name": "Params",
                    "id": 4
                }
            ]
        },
        {
            "name": "FightInput",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RandSeed",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "FrameInputList",
                    "name": "InputList",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "BornBuffItem",
                    "name": "BornBuffTab",
                    "id": 3
                }
            ]
        }
    ],
    "isNamespace": true
}).build();