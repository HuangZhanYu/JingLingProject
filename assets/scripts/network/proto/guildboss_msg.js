module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "GuildPart_Award",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ItemType",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ItemID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ItemNum",
                    "id": 3
                }
            ]
        },
        {
            "name": "GuildPart_SignInfoRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "GuildPart_SignTimesAward",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Times",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "GuildPart_Award",
                    "name": "Awards",
                    "id": 2
                }
            ]
        },
        {
            "name": "GuildPart_SignInfoResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "SignTimes",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "GetRecords",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "GuildPart_SignTimesAward",
                    "name": "RewardList",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "LevelAtZero",
                    "id": 4
                }
            ]
        },
        {
            "name": "GuildPart_SignRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "SignT",
                    "id": 1
                }
            ]
        },
        {
            "name": "GuildPart_SignResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "GuildLevel",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "Contribution",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "GuildCoin",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "SignT",
                    "id": 5
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "SignTimes",
                    "id": 6
                }
            ]
        },
        {
            "name": "GuildPart_SignTimesAwardRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Times",
                    "id": 1
                }
            ]
        },
        {
            "name": "GuildPart_SignTimesAwardResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "GuildPart_Award",
                    "name": "Awards",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "GetRecords",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "SignTimes",
                    "id": 4
                }
            ]
        },
        {
            "name": "GuildPart_ClearApplyRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "GuildPart_ClearApplyResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                }
            ]
        },
        {
            "name": "GuildPart_CardInfoRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "GuildPart_SendLog",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Time",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "SendNick",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "RecvNick",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "LogT",
                    "id": 4
                }
            ]
        },
        {
            "name": "GuildPart_CardInfoResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "GuildCoin",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Contribution",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "OwnCard",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "LastDay",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "GetToday",
                    "id": 5
                },
                {
                    "rule": "repeated",
                    "type": "GuildPart_SendLog",
                    "name": "SendLogs",
                    "id": 6
                },
                {
                    "rule": "repeated",
                    "type": "GuildPart_Award",
                    "name": "Awards",
                    "id": 7
                }
            ]
        },
        {
            "name": "GuildPart_CaSMembersRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "GuildPart_CaSMember",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Name",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Icon",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Position",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "TopGuanQia",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Contribution",
                    "id": 6
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Level",
                    "id": 7
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "Online",
                    "id": 8
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "OfflineTime",
                    "id": 9
                }
            ]
        },
        {
            "name": "GuildPart_CaSMembersResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "GuildCoin",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Contribution",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "GuildPart_CaSMember",
                    "name": "Members",
                    "id": 3
                }
            ]
        },
        {
            "name": "GuildPart_SendCardRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 1
                }
            ]
        },
        {
            "name": "GuildPart_SendCardResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "ObjID",
                    "id": 2
                }
            ]
        },
        {
            "name": "GuildPart_SendCardRet",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 1
                }
            ]
        },
        {
            "name": "GuildPart_CardAwardsRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "GuildPart_CardAwardsResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "GuildPart_Award",
                    "name": "Awards",
                    "id": 2
                }
            ]
        },
        {
            "name": "GuildPart_BossInfoRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "GuildPart_SBoss",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Area",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Diff",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "BossID",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "MaxHP",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "HP",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "LastTime",
                    "id": 6
                }
            ]
        },
        {
            "name": "GuildPart_BossInfoResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "GuildCoin",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Contribution",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "FightTimes",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Position",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "GuildLevel",
                    "id": 5
                },
                {
                    "rule": "repeated",
                    "type": "GuildPart_SBoss",
                    "name": "OpenedBoss",
                    "id": 6
                }
            ]
        },
        {
            "name": "GuildPart_OpenBossRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Area",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Diff",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "BossID",
                    "id": 3
                }
            ]
        },
        {
            "name": "GuildPart_OpenBossResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "GuildPart_SBoss",
                    "name": "OpenedBoss",
                    "id": 2
                }
            ]
        },
        {
            "name": "GuildPart_FightBossRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Area",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Diff",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "BossID",
                    "id": 3
                }
            ]
        },
        {
            "name": "GuildPart_FightBossResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "GuildPart_Award",
                    "name": "Awards",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "HP",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "Hurt",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "TotalFightTimes",
                    "id": 5
                }
            ]
        },
        {
            "name": "GuildPart_FightBossRecord",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "bytes",
                    "name": "Content",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "Done",
                    "id": 2
                }
            ]
        },
        {
            "name": "GuildPart_FightRecordsRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "TeamIndex",
                    "id": 1
                }
            ]
        },
        {
            "name": "GuildPart_FightRecordsResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "Done",
                    "id": 1
                }
            ]
        },
        {
            "name": "GuildPart_DamageRankRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Area",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Diff",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "BossID",
                    "id": 3
                }
            ]
        },
        {
            "name": "GuildPart_DamageItem",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Nick",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Icon",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Damage",
                    "id": 4
                }
            ]
        },
        {
            "name": "GuildPart_DamageRankResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "GuildPart_DamageItem",
                    "name": "DamageList",
                    "id": 2
                }
            ]
        },
        {
            "name": "GuildPart_ClothInfoRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "GuildPart_Cloths",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "CardList",
                    "id": 1
                }
            ]
        },
        {
            "name": "GuildPart_ClothInfoResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "GuildCoin",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Contribution",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "GuildPart_Cloths",
                    "name": "Lineup",
                    "id": 3
                }
            ]
        },
        {
            "name": "GuildPart_ClothArrayRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Index",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "WizardList",
                    "id": 2
                }
            ]
        },
        {
            "name": "GuildPart_ClothArrayResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "Index",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "WizardList",
                    "id": 3
                }
            ]
        },
        {
            "name": "GuildPart_ClothElvesRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "ElvesList",
                    "id": 1
                }
            ]
        },
        {
            "name": "GuildPart_ClothElvesResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "ElvesList",
                    "id": 2
                }
            ]
        },
        {
            "name": "GuildPart_TodayDamageRankRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "GuildPart_TodayDamageRankResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "GuildPart_DamageItem",
                    "name": "DamageList",
                    "id": 1
                }
            ]
        },
        {
            "name": "GuildPart_KilledRankRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "GuildPart_KilledRankItem",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "GuildID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Name",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Icon",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Level",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "MemberCount",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Capacity",
                    "id": 6
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "KilledCount",
                    "id": 7
                }
            ]
        },
        {
            "name": "GuildPart_KilledRankResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "KilledCount",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "Rank",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "GuildPart_KilledRankItem",
                    "name": "RankList",
                    "id": 3
                }
            ]
        },
        {
            "name": "GuildPart_DoubleCardsRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "GuildPart_DoubleCardsResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "DoubleCards",
                    "id": 1
                }
            ]
        },
        {
            "name": "GuildPart_DonateLogRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "BuildID",
                    "id": 1
                }
            ]
        },
        {
            "name": "GuildPart_DonateLog",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "LogTm",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Name",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "GetExp",
                    "id": 3
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "Params",
                    "id": 4
                }
            ]
        },
        {
            "name": "GuildPart_DonateLogResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "GuildPart_DonateLog",
                    "name": "LogList",
                    "id": 1
                }
            ]
        },
        {
            "name": "GuildPart_WeekDamageRankRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "GuildPart_WeekDamageItem",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "Nick",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "GuildName",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "GuildIcon",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "Icon",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "GuildLevel",
                    "id": 5
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "Level",
                    "id": 6
                },
                {
                    "rule": "repeated",
                    "type": "GuildPart_Award",
                    "name": "Awards",
                    "id": 7
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "Damage",
                    "id": 8
                }
            ]
        },
        {
            "name": "GuildPart_WeekDamageRankResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "GuildPart_WeekDamageItem",
                    "name": "RankList",
                    "id": 1
                }
            ]
        }
    ],
    "isNamespace": true
}).build();