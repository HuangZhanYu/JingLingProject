module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "LoginRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Account",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "PassWord",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ServerId",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "DeviceId",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ChannelId",
                    "id": 5
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "Reconnect",
                    "id": 6
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "PackageName",
                    "id": 7
                }
            ]
        },
        {
            "name": "LoginResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Susseed",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "NewPlayer",
                    "id": 2
                }
            ]
        },
        {
            "name": "LoginErrorResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Susseed",
                    "id": 1
                }
            ]
        },
        {
            "name": "CheckPlayerNameRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Name",
                    "id": 1
                }
            ]
        },
        {
            "name": "CheckPlayerNameResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Susseed",
                    "id": 1
                }
            ]
        },
        {
            "name": "CreatePlayerRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Name",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "PlayerType",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "BornPet",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "DeviceId",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ChannelId",
                    "id": 5
                }
            ]
        },
        {
            "name": "CreatePlayerResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Susseed",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "Error",
                    "id": 2
                }
            ]
        },
        {
            "name": "HeartPkgRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "Connect",
                    "id": 1
                }
            ]
        },
        {
            "name": "HeartPkgResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "Connect",
                    "id": 1
                }
            ]
        },
        {
            "name": "KickMessage",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Type",
                    "id": 1
                }
            ]
        },
        {
            "name": "KickBackMessage",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "ServerCloseMessage",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "ViewCardInfo",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "CardID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Level",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "EvolutionLevel",
                    "id": 3
                }
            ]
        },
        {
            "name": "ViewEquipInfo",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "TreasureID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Level",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "BeyondLevel",
                    "id": 3
                }
            ]
        },
        {
            "name": "ViewOtherPlayerRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjectID",
                    "id": 1
                }
            ]
        },
        {
            "name": "ViewOtherPlayerByAccountRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Account",
                    "id": 1
                }
            ]
        },
        {
            "name": "ViewOtherPlayerResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Name",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Lv",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "WenZhang",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "TopQuanQia",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ResetCount",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "HeadID",
                    "id": 6
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjectID",
                    "id": 7
                },
                {
                    "rule": "repeated",
                    "type": "ViewCardInfo",
                    "name": "CardList",
                    "id": 8
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "SkillList",
                    "id": 9
                },
                {
                    "rule": "repeated",
                    "type": "ViewEquipInfo",
                    "name": "EquipList",
                    "id": 10
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "GuildName",
                    "id": 11
                }
            ]
        },
        {
            "name": "ReLoginOnDailyUpdateResponse",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "SendPushUIDRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "PushUID",
                    "id": 1
                }
            ]
        },
        {
            "name": "CollectAdviceRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Advice",
                    "id": 1
                }
            ]
        }
    ],
    "isNamespace": true
}).build();