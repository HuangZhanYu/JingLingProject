/*
作者（Author）:    jason

描述（Describe）: protobuf文件加载类
*/
/*jshint esversion: 6 */
var protobuf = require("protobufjs");
var builder = protobuf.newBuilder();

var protoFiles = [
    'ability_msg.proto',
    'achievement_msg.proto',
    'LoginRequest.proto',
    'active_msg.proto',
    'activity_msg.proto',
    'background_activity_msg.proto',
    'backpack_msg.proto',
    'time_msg.proto'
];
export default class PBHelper
{
    static loadFile(path, packageName)
    {
        if(typeof cc != 'undefined')
        {
            path = true ? cc.url.raw(path) : `res/raw-assets/${path}`;
            protobuf.Util.IS_NODE = true;
            cc.log('>>>>>>>>' + path);
        }

        builder.importRoot = path;
        protoFiles.forEach(function (fileName) {
            let filePath = `${path}/${fileName}`;
            protobuf.protoFromFile(filePath, builder);
        });

        return builder.build(packageName);
    }
}