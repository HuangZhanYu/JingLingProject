/*
作者（Author）:    jason

描述（Describe）: 消息回调处理类
*/
/*jshint esversion: 6 */
//

window.resBuf = null;
export default class MessageResponse
{
    static onResponse(name,buff)
    {
        console.log(`onResponse funcName = ${name}`);
        resBuf = buff;
        (new Function('', name + '(resBuf)'))();
        // try
        // {
            
        // }
        // catch(err)
        // {
        //     console.error(`onResponse err = ${err}`);
        // }
    }
}

