/*
作者（Author）:    jason

描述（Describe）: 客户端网络模块
*/
/*jshint esversion: 6 */
// host = 120.77.106.78 port = 8091
import Util from "../tool/Util";
import MessageResponse from "./MessageResponse";

let webSocket = null;
export default class SocketClient
{

    //链接到服务器
    static connectServer()
    {
        webSocket = new WebSocket("ws://120.24.169.210:8089/game");
        webSocket.binaryType = "arraybuffer";
        // 建立 web socket 连接成功触发事件
        webSocket.onopen = SocketClient.onOpen;

        // 接收服务端数据时触发事件
        webSocket.onmessage = SocketClient.onMessage;
        webSocket.onerror = SocketClient.onError;
        webSocket.onclose = SocketClient.onClose;
    }
    static onOpen()
    {
        console.log("与服务器 链接成功 ############");

        Network.OnConnect();
    }
    
    static callServerFun(funName,data)
    {
        
        let dataArray = data.toArrayBuffer();
        var buffer = new ArrayBuffer(6 + funName.length + dataArray.byteLength);
        //SocketClient.sendMessage(base);
        var view = new DataView(buffer); //,11, 48); // 4 + funName.length,dataArray.byteLength);
        view.setInt16(0,2 + funName.length + dataArray.byteLength,true);
        view.setInt16(2,funName.length,true);
        for (let i = 0; i < funName.length; ++i)
        {
            view.setUint8(i + 4,funName.charCodeAt(i));
        }
        var dataView = new DataView(dataArray);
        view.setInt16(4+funName.length,dataView.byteLength,true);
        for (let i = 0; i < dataView.byteLength; ++i)
        {
            view.setUint8(i + 6 + funName.length , dataView.getUint8(i));
        }
        SocketClient.sendMessage(view);
    }
    //发送消息
    static sendMessage(buff)
    {
        webSocket.send(buff);
    }
    
    //接收服务器发来的消息
    static onMessage(evt)
    {
        let received_msg = evt.data;
     
        let receive = Array.from(new Uint8Array(received_msg));
        let dataView = new DataView(new Uint8Array(receive).buffer);
        if (dataView.byteLength == 0)
        {
            return;
        }
        let readPos = 0; //
        dataView.getInt16(readPos,true); readPos += 2;
        let funcLength = dataView.getInt32(readPos,true); readPos += 4;
        let funcName = receive.slice(readPos , funcLength + 6); readPos += funcLength;
      
        let bufLength = dataView.getInt32(readPos,true); readPos += 4;
        let resBytes = receive.slice(readPos);
       
        MessageResponse.onResponse(Util.Utf8ArrayToStr(funcName),resBytes);
    }
    
    static onError(evt)
    {
        let received_msg = evt.data;
        console.log("发生错误..." + received_msg);
    }

    static onClose(evt)
    {
        let received_msg = evt.data;
        console.log("链接关闭" + received_msg);
    }

    

}
