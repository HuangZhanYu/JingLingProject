//import { BigInteger } from "./biginteger";

/*
作者（Author）:    jason

描述（Describe）: 大数字处理类
*/
/*jshint esversion: 6 */
import { typeOf, jsType } from "../common/PublicFunction";
let Enable = true;
let CacheFlag = true;

let ClearCount = 0;
let TotalTimes = 0;
let CacheTimes = 0;

const ClearAllTime = 3;

//大数字缓存数量
const CacheCount = 500;
 //每一位的最大值
 const MAX_VALUE = 1000;
//大数字缓存字典
let CacheMap = new Map();

let FormulaCacheMap = new Map();

class BigNumberCache
{
    constructor()
    {
        this.mValue = null;
        this.mCount = 0;
    }
}
class FormulaCache {

    constructor(a, b, op, r, br)
    {
        this.valueA = a;
        this.valueB = b;
        this.oper = op;
        this.ret = r;
        this.bret = br;
    }
}
//货币单位
const TotalUnit = [
    "",
    "a",
    "b",
    "c",
    "d",
    "e",
    "f",
    "g",
    "h",
    "i",
    "j",
    "k",
    "l",
    "m",
    "n",
    "o",
    "p",
    "q",
    "r",
    "s",
    "t",
    "u",
    "v",
    "w",
    "x",
    "y",
    "z",
    "aa",
    "ab",
    "ac",
    "ad",
    "ae",
    "af",
    "ag",
    "ah",
    "ai",
    "aj",
    "ak",
    "al",
    "am",
    "an",
    "ao",
    "ap",
    "aq",
    "ar",
    "as",
    "at",
    "au",
    "av",
    "aw",
    "ax",
    "ay",
    "az",
    "ba",
    "bb",
    "bc",
    "bd",
    "be",
    "bf",
    "bg",
    "bh",
    "bi",
    "bj",
    "bk",
    "bl",
    "bm",
    "bn",
    "bo",
    "bp",
    "bq",
    "br",
    "bs",
    "bt",
    "bu",
    "bv",
    "bw",
    "bx",
    "by",
    "bz",
    "da",
    "db",
    "dc",
    "dd",
    "de",
    "df",
    "dg",
    "dh",
    "di",
    "dj",
    "dk",
    "dl",
    "dm",
    "dn",
    "do",
    "dp",
    "dq",
    "dr",
    "ds",
    "dt",
    "du",
    "dv",
    "dw",
    "dx",
    "dy",
    "dz",
    "ea",
    "eb",
    "ec",
    "ed",
    "ee",
    "ef",
    "eg",
    "eh",
    "ei",
    "ej",
    "ek",
    "el",
    "em",
    "en",
    "eo",
    "ep",
    "eq",
    "er",
    "es",
    "et",
    "eu",
    "ev",
    "ew",
    "ex",
    "ey",
    "ez",
    "fa",
    "fb",
    "fc",
    "fd",
    "fe",
    "ff",
    "fg",
    "fh",
    "fi",
    "fj",
    "fk",
    "fl",
    "fm",
    "fn",
    "fo",
    "fp",
    "fq",
    "fr",
    "fs",
    "ft",
    "fu",
    "fv",
    "fw",
    "fx",
    "fy",
    "fz",
    "ga",
    "gb",
    "gc",
    "gd",
    "ge",
    "gf",
    "gg",
    "gh",
    "gi",
    "gj",
    "gk",
    "gl",
    "gm",
    "gn",
    "go",
    "gp",
    "gq",
    "gr",
    "gs",
    "gt",
    "gu",
    "gv",
    "gw",
    "gx",
    "gy",
    "gz",
    "ha",
    "hb",
    "hc",
    "hd",
    "he",
    "hf",
    "hg",
    "hh",
    "hi",
    "hj",
    "hk",
    "hl",
    "hm",
    "hn",
    "ho",
    "hp",
    "hq",
    "hr",
    "hs",
    "ht",
    "hu",
    "hv",
    "hw",
    "hx",
    "hy",
    "hz",
    "ia",
    "ib",
    "ic",
    "id",
    "ie",
    "if",
    "ig",
    "ih",
    "ii",
    "ij",
    "ik",
    "il",
    "im",
    "in",
    "io",
    "ip",
    "iq",
    "ir",
    "is",
    "it",
    "iu",
    "iv",
    "iw",
    "ix",
    "iy",
    "iz",
    "ja",
    "jb",
    "jc",
    "jd",
    "je",
    "jf",
    "jg",
    "jh",
    "ji",
    "jj",
    "jk",
    "jl",
    "jm",
    "jn",
    "jo",
    "jp",
    "jq",
    "jr",
    "js",
    "jt",
    "ju",
    "jv",
    "jw",
    "jx",
    "jy",
    "jz",
]
let DigitIndexMap = new Map();
  /// <summary>
/// 通过下标获取单位
/// </summary>
/// <param name="index"></param>
/// <returns></returns>
function getUnitString (index) {
    if (index >= 0) {
        return TotalUnit[index];
    }

    return "";
}

/// <summary>
/// 通过单位查找是那个位的
/// </summary>
/// <param name="unit"></param>
/// <returns></returns>
function getUnitIndex (unit) {
    if (DigitIndexMap.size == 0) {
        for (let i = 0; i < TotalUnit.length; i++) {
            let unitStr = TotalUnit[i];

            DigitIndexMap.set(unitStr,i);
        }
    }

    //找不到，返回-1代表错误
    if (!DigitIndexMap.has (unit)) {
        return -1;
    }
    return DigitIndexMap.get(unit);
}

//大数字运算类型
export const BigNumberCalType = {
        Add : 1, //加
        Sub : 2, //减
        Mul : 3, //乘
        Div : 4, //除
};

export default class BigNumber
{
    static getBigInteger(value)
    {
        if(CacheMap.has(value))
        {
            let c = CacheMap.get(value);
            c.mCount++;
            return c.mValue;
        }
        else
        {
            if(CacheMap.size > CacheCount)
            {
                BigNumber.clear();
            }
            let c = new BigNumberCache ();
            c.mValue = BigInteger.parse(value);
            c.mCount++;
            CacheMap.set(value, c);
            return c.mValue;
        }
    }
    static clear()
    {
        ClearCount++;
        if (ClearCount >= ClearAllTime) {
            ClearCount = 0;
            BigNumber.clearAll();
            return;
        }

        let needClear = CacheMap.size - CacheCount;
        let needDelete = [];
        for(let [key, value] of CacheMap) {
            if (value.mCount < 100) {
                needDelete.push(key);
                needClear--;
            }
            if (needClear <= 0) {
                break;
            }
        }

        for (let key of needDelete) {
            CacheMap.delete(key);
        }
    }
    static clearAll()
    {
        CacheMap.clear();
        FormulaCacheMap.clear();
    }
    static clearFormulaCache()
    {
        FormulaCacheMap.clear ();
        TotalTimes = 0;
        CacheTimes = 0;
    }
    static addFormulaCache (va, vb, op, v, bret) {
        FormulaCacheMap.set(BigNumber.formulaKey (va, vb, op),new FormulaCache (va, vb, op, v, bret));
    }

    static formulaKey (va, vb, op) {
        return va + '|' + vb + '|' + op;
    }
    static formulaRet (va, vb, op) {
        let bret = false;
        let cache;
        let syno = false;
        TotalTimes++;
        let key = BigNumber.formulaKey (va, vb, op);
        if (FormulaCacheMap.has(key)) {
            cache = FormulaCacheMap.get(key);
        } else {
            key = BigNumber.formulaKey (vb, va, op);
            if (FormulaCacheMap.has (key)) {
                cache = FormulaCacheMap.get(key);
                syno = true;
            } else {
                return ["",bret];
            }
        }

        if (!syno) {
            CacheTimes++;
            bret = cache.bret;
            return [cache.ret,bret];
        }

        bret = cache.bret;
        switch (op) {
            case "-":
                CacheTimes++;
                if (cache.ret[0] == '-') {
                    return [cache.ret.substring(1, cache.ret.length - 1),bret];
                } else {
                    return ['-' + cache.ret,bret];
                }
            case ">":
            case ">=":
            case "<":
            case "<=":
                bret = !cache.bret;
                break;
            case "/":
            case "%":
                return ["",bret];
            default:
                return ["",bret];
        }
        CacheTimes++;
        return [cache.ret,bret];
    }
    /// <summary>
    /// 获取当前的值
    /// </summary>
    /// <returns></returns>
    static getIntValue (value) {
        let result = BigNumber.getBigInteger(value);
        return result.toJSValue();
    }
    static parse (value) {
        let inte = "";
        let unit = "";
        //首先截取整数部分
        //其次，如果有截取小数部分
        //最后获取单位
        let chars = value.split("");
        for (let i = 0; i < chars.length; i++) {
            let cha = chars[i];
            if (!isNaN(parseInt(cha))) //数字
            {
                inte += cha;
            } else if (cha === '.') //小数点
            {
                inte += cha;
            } else if (cha === '-') //负数
            {
                inte += cha;
            } 
            else //单位
            {
                unit += cha;
            }
        }
        return [inte,unit];
    }

    //加法
    static add ( a, b)
    {
        if (!Enable)
        {
            return a;
        }
        const [v,bret] = BigNumber.formulaRet (a, b, "+");
        if (v != "") {
            return v;
        }

        let ret = BigNumber.getBigInteger(a).add(BigNumber.getBigInteger(b)).toString();
        BigNumber.addFormulaCache (a, b, "+", ret, false);
        return ret;
    }
    //减法
    static sub ( a, b)
    {
        if (!Enable)
        {
            return a;
        }
        const [v,bret] = BigNumber.formulaRet (a, b, "-");
        if (v != "") {
            return v;
        }

        let ret = BigNumber.getBigInteger(a).subtract(BigNumber.getBigInteger(b)).toString();
        BigNumber.addFormulaCache (a, b, "-", ret, false);
        return ret;
    }
    //乘法
    static mul ( a, b)
    {
        if (!Enable)
        {
            return a;
        }
        const [v,bret] = BigNumber.formulaRet (a, b, "*");
        if (v != "") {
            return v;
        }

        let ret = BigNumber.getBigInteger(a).multiply(BigNumber.getBigInteger(b)).toJSValue();
        ret = ret.toString();
        BigNumber.addFormulaCache (a, b, "*", ret, false);
        return ret;
    }
    //除法
    static div ( a, b)
    {
        if (!Enable)
        {
            return a;
        }
        const [v,bret] = BigNumber.formulaRet (a, b, "/");
        if (v != "") {
            return v;
        }

        let ret = BigNumber.getBigInteger(a).divide(BigNumber.getBigInteger(b)).toJSValue();
        ret = ret.toString();
        BigNumber.addFormulaCache (a, b, "/", ret, false);
        return ret;
    }
    //多项式计算，按照从左到右的顺序计算
    static cal (values, calTypes) {
        let ret = values[0];
        for (let i = 0; i < calTypes.length; i++) {
            let typ = calTypes[i];
            let op = "";
            switch (typ) {
                case BigNumberCalType.Add:
                    op = "+";
                    break;
                case BigNumberCalType.Sub:
                    op = "-";
                    break;
                case BigNumberCalType.Mul:
                    op = "*";
                    break;
                case BigNumberCalType.Div:
                    op = "/";
                    break;
            }

            const [v,bret] = BigNumber.formulaRet (ret, values[i + 1], op);
            if (v != "") {
                ret = v;
                continue;
            }

            let biret = BigNumber.getBigInteger (ret);
            switch (typ) {
                case BigNumberCalType.Add:
                    biret += BigNumber.getBigInteger (values[i + 1]);
                    break;
                case BigNumberCalType.Sub:
                    biret -= BigNumber.getBigInteger (values[i + 1]);
                    break;
                case BigNumberCalType.Mul:
                    biret *= BigNumber.getBigInteger (values[i + 1]);
                    break;
                case BigNumberCalType.Div:
                    biret /= BigNumber.getBigInteger (values[i + 1]);
                    break;
            }
            let newret = biret.toString ();
            BigNumber.addFormulaCache (ret, values[i + 1], op, newret, false);
            ret = newret;
        }

        return ret;
    }
    /// <summary>
    /// 获取浮点值的除法
    /// 获取0-1之间的数量
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <returns></returns>
    static floatDivision ( a, b)
    {
        if (!Enable)
        {
            return 1;
        }
        //获取单位
        const [aNum,aUnit] = BigNumber.parse (BigNumber.getValue (a));
        let aFloat = parseFloat(aNum);
        if(isNaN(aFloat))
        {
            return 0;
        }
        let aUnitIndex = getUnitIndex (aUnit);

        const [bNum,bUnit] = BigNumber.parse (BigNumber.getValue (b));
        let bFloat = parseFloat(bNum);
        if(isNaN(bFloat))
        {
            return 0;
        }
        let bUnitIndex = getUnitIndex (bUnit);

        //相差过大，直接返回0或者1
        if (Math.abs (aUnitIndex - bUnitIndex) >= 3) {
            if (aUnitIndex >= bUnitIndex) {
                return 1;
            } else {
                return 0;
            }
        }

        //判断大小
        return aFloat / bFloat * Math.pow (MAX_VALUE, aUnitIndex - bUnitIndex);
    }
    /// <summary>
    /// 获取当前的值，只保留最大单位和第二大单位
    /// </summary>
    /// <returns></returns>
    static getValue (strValue) {
        if(typeOf(strValue) != jsType.string)
        {
            strValue = strValue.toString();
        }
        let docIndex = strValue.indexOf(".");
        if (docIndex >= 0) {
            strValue = strValue.substring (0, docIndex);
        }

        let isNegative = false;
        //计算有多少个三位
        if (strValue.indexOf ('-') >= 0) {
            isNegative = true;
        }
        let numStr = strValue.replace (/-/,"");
        let len = numStr.length;

        //计算单位
        let digit = Math.floor((len - 1) / 3);
        let unit = TotalUnit[digit];
        //如果位数大于等于1，会给小数
        if (digit >= 1) {
            //余数就是要显示的单位数值，加上1，代表保留一位小数
            let remainder = len % 3;
            if (remainder == 0) {
                remainder = 3;
            }
            let show = numStr.substring (0, remainder + 1);
            let tem = show.substring(0,remainder);
            show = show.replace(tem,tem+".");
            
            // show = show.TrimEnd ('0');
            // show = show.TrimEnd ('.');
            if (isNegative) {
                show = "-" + show;
            }

            return show + unit;
        } else {
            //去掉多余位数
            if (isNegative) {
                numStr = "-" + numStr;
            }

            return numStr;
        }
    }
    //判断字符是否为空的方法
    static isEmpty(obj){
        if(typeof obj == "undefined" || obj == null || obj == ""){
            return true;
        }else{
            return false;
        }
    }
    /// <summary>
    /// 通过一个字符串创建大数字
    /// 该字符串满足xx.xx加单位
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    static create (value) {

        if(typeOf(value) != jsType.number && typeOf(value) != jsType.string)
        {
            return "0";
        }
        if (BigNumber.isEmpty(value)) {
            return "0";
        }
       
        value = value.toString();
        
        //首先判断是否是科学计数法
        value = value.toLowerCase ();
        let eIndex = value.indexOf ("e+");
        if (eIndex >= 0) {
            value = value.replace ("e", "");
            let contents = value.split ('+');
            let number = contents[0];
            let digit = parseInt(contents[1]);
            let dotIndex = number.indexOf (".");
            let needSub = 0;
            if (dotIndex >= 0) {
                //下标从0开始算的，所以要减1
                //长度是从1开始算的
                needSub = number.length - dotIndex - 1;
                number = number.replace (".", "");
            }
            digit = digit - needSub;

            for (let i = 0; i < digit; i++) {
                number += "0";
            }
            return number;
        }

        //首先截取整数部分
        //其次，如果有截取小数部分
        //最后获取单位
        let inte = "";
        let deci = "";
        let unit = "";
        let isDecimal = false;
        let isNegative = false;
        let chars = value.split("");
        for (let i = 0; i < chars.length; i++) {
            let cha = chars[i];
            if (!isNaN(parseInt (cha))) //数字
            {
                if (isDecimal) {
                    deci += cha;
                } else {
                    inte += cha;
                }
            } else if (cha == '.') //小数点
            {
                isDecimal = true;
            } else if (cha == '-') //负数
            {
                isNegative = true;
            } else //单位
            {
                unit += cha;
            }
        }

        //获取单位位置
        let digitIndex = getUnitIndex (unit);

        if (digitIndex == -1) {
            console.log ("大数字参数单位错误:" + value);
            return "0";
        }
        //设置整数
        if (!isDecimal) {
            for (let i = 0; i < digitIndex; i++) {
                inte += "000";
            }
        } else {
            //获取小数点位数
            for (let i = 0; i < digitIndex; i++) {
                if (deci.length >= 3) {
                    let add = deci.substring (0, 3);
                    deci = deci.substring (3);
                    inte += add;
                } else if (deci.length > 0) {
                    inte += deci;
                    for (let j = 0; j < (3 - deci.length); j++) {
                        inte += "0";
                    }
                    deci = "";
                } else {
                    inte += "000";
                }
            }
        }
        if (isNegative) {
            inte = "-" + inte;
        }
        return inte;
    }
    //等于函数
    static equalTo(a,b)
    {
       let aa = BigNumber.getBigInteger(a);
       let bb = BigNumber.getBigInteger(b);

       return aa === bb;
    }
    //不等于
    static notEqualTo(a,b)
    {
        let aa = BigNumber.getBigInteger(a);
        let bb = BigNumber.getBigInteger(b);
 
        return aa !== bb;
    }

    //大于函数
    static greaterThan(a,b)
    {
        let aa = BigNumber.getBigInteger(a);
        let bb = BigNumber.getBigInteger(b);
 
        return aa > bb;
    }
    //小于等于函数 
    static lessThanOrEqualTo(a,b)
    {
        let aa = BigNumber.getBigInteger(a);
        let bb = BigNumber.getBigInteger(b);
 
        return aa <= bb;
    }
    
    //小于函数
    static lessThan(a,b)
    {
        let aa = BigNumber.getBigInteger(a);
        let bb = BigNumber.getBigInteger(b);

        return aa < bb;
    }
    //大于等于函数
    static greaterThanOrEqualTo(a,b)
    {
        let aa = BigNumber.getBigInteger(a);
        let bb = BigNumber.getBigInteger(b);

        return aa >= bb;
    }
    //传入任务和时间 返回每秒获得金币
     static GetPerSecGold(getGold,doTimes)
     {
        let gold = "0";
        let result = [];
        for (let i = 0; i < getGold.length; i++) {
            result[i] = BigNumber.div(getGold[i], BigNumber.create (doTimes[i]));

        }

        for (let i = 0; i < result.length; i++) {
            gold = BigNumber.add (gold, result[i]);
        }

        return gold;
     }
}
 
/// <summary>
/// 常用数 10000
/// </summary>
BigNumber.tenThousand = "10000";
BigNumber.zero = "0";
BigNumber.one = "1";