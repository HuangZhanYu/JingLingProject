/*jshint esversion: 6 */

export const ResourceTypeEnum = {
	ResType_HuoBi     : 1,         // 货币表
	ResType_DaoJu     : 2,         // 道具表
	ResType_JingLing  : 3,         // 士兵表
	ResType_ZhuangBei : 4,         // 装备表
	ResType_JiaSu     : 5,         // 加速表
	ResType_ZhuDongJi : 6,         // 主动技表
    ResType_Guild     : 7,         // 公会
};

export const HuoBiTypeEnum = {
	HuoBiType_ZuanShi                    : 1, //钻石
	HuoBiType_HuangJin					 : 2, //黄金
	HuoBiType_WenZhang                   : 3, //纹章
	HuoBiType_JinBi                      : 4, //金币
	HuoBiType_HuPoBi                     : 5, //琥珀币
	HuoBiType_JingYan                    : 6, //经验
	HuoBiType_QiangHuaCaiLiao1           : 7, //装备强化材料1（贝壳化石）
	HuoBiType_QiangHuaCaiLiao2           : 8, //装备强化材料2（甲壳化石）
	HuoBiType_QiangHuaCaiLiao3           : 9, //装备强化材料3（秘密琥珀）
	HuoBiType_QiangHuaCaiLiao4           : 10,//装备强化材料4（盾甲化石）
	HuoBiType_QiangHuaCaiLiao5           : 11,//装备强化材料5（根状化石）
	HuoBiType_QiangHuaCaiLiao6           : 12,//装备强化材料6（爪子化石）
	HuoBiType_QiangHuaCaiLiao7           : 13,//装备强化材料7（背盖化石）
	HuoBiType_RongYuBi                   : 14,//荣誉币
	HuoBiType_GongHuiBi                  : 15,//公会币
	HuoBiType_JingJiDian   				 : 16,//竞技点
	HuoBiType_TiLi                       : 17,//体力
	HuoBiType_VipExp                     : 18,//VIP经验
	HuoBiType_ChongZhiJiFen              : 19,//充值积分 
	HuoBiType_DaoGuanJingYan             : 20,//道馆经验
	HuoBiType_JingTongShi                : 21,//精通石
	HuoBiType_FightTowerTimes            : 22,//战斗塔次数
	HuoBiType_PlateauTimes               : 23,//精通塔次数
	HuoBiType_LandgraveMedal 			 : 24,//道馆战勋章
};