/*
作者（Author）:    skyHuang

描述（Describe）:
*/
/*jshint esversion: 6 */

import BigNumber from "./BigNumber";

let ExponentTool = (function () 
{
    let ErrorLoopCount = 20;    //--错误循环计数
    let TotalResult = new Map();       //--缓存
    let Indexs = new Map();
    return {
        Init()
        {
            let conf_CanShu_Data = LoadConfig.getConfigData(ConfigName.CanShu,58);
            
            if(conf_CanShu_Data == null)
            {
                //--默认设置
                Indexs.set(1.01,0);    //--表格下标
            }
            else
            {
                let value = conf_CanShu_Data.Param[0] / 10000;
                Indexs.set(value,0);    //--表格下标
            }
            
            conf_CanShu_Data = LoadConfig.getConfigData(ConfigName.CanShu,59);

            if(conf_CanShu_Data == null)
            {
                //--默认设置
                Indexs.set(1.05,1);    //--表格下标
            }
            else
            {
                let value = conf_CanShu_Data.Param[0] / 10000;
                Indexs.set(value,19);    //--表格下标
            }
            
            conf_CanShu_Data = LoadConfig.getConfigData(ConfigName.CanShu,60);

            if(conf_CanShu_Data == null)
            {
                //--默认设置
                Indexs.set(1.05,2);    //--表格下标
            }
            else
            {
                let value = conf_CanShu_Data.Param[0] / 10000;
                Indexs.set(value,2);
            }
        },

        //--获取以param为底数，exponent为指数的值，为确保精度，需要除以10000
        //--先从表中获取
        //--再从缓存中获取
        //--最后将指数不断地除以2，找到在表中的数据位置，再计算出结果，缓存
        //--param 底数
        //--exponent 指数
        GetResult(param, exponent)
        {
            let index = Indexs.get(param);
            if(index == null)
            {
                console.error("该列不存在，param:"+param);
                return BigNumber.create(1);
            }
            //--先判断是否在表里
            let conf_ZhiShuShuZhi_Data = LoadConfig.getConfigData(ConfigName.ZhiShuShuZhi,exponent);
            if(conf_ZhiShuShuZhi_Data != null)
            {
                return BigNumber.create(conf_ZhiShuShuZhi_Data.Value[index]);
            }

            //--表里没有先从缓存取
            let set = TotalResult.get(param);
            if(set != null)
            {
                let result = set.get(exponent);
                if(result != null)
                {
                    return result;
                }
            }

            let calExponent = exponent;
            let counting = 0;
            let mods = [];
            while (conf_ZhiShuShuZhi_Data == null )
            {
                let mod = calExponent % 2;
                mods.push(mod);
                calExponent = Math.floor(calExponent / 2);
                conf_ZhiShuShuZhi_Data = Conf_ZhiShuShuZhi[calExponent];
                counting = counting + 1;
                if(counting > ExponentTool.ErrorLoopCount)
                {
                    console.error("指数参数错误param:"+param+"|exponent:"+exponent);
                    return BigNumber.create(1);
                }
            }
            let value = BigNumber.create(conf_ZhiShuShuZhi_Data.Value[index]);
            let unit = BigNumber.create(math.floor(param * 10000));
            for(let i = mods.length-1; i > 0; i--)
            {
                let pow = BigNumber.Power(value, 2) ;  //--由于本身乘了10000，两个本身就10000*10000，要还原回去，所以这里要除10000
                pow = BigNumber.div(pow, BigNumber.tenThousand);
                let mod = mods[i];
                if(mod == 1)
                {
                    value = BigNumber.mul(pow, unit);
                    value = BigNumber.div(value, BigNumber.tenThousand);
                }
                else
                {
                    value = pow;
                }
            }

            //--缓存数据
            if(set == null)
            {
                set = new Map();
            }
            set.set(exponent, value);
            TotalResult.set(param,set);

            //--返回数值
            return value;
        }
    };
})();

export default ExponentTool;