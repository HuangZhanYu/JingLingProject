/*
作者（Author）:    jason

描述（Describe）: 资源加载类
*/
const retryCount = 6;
const retryInterval = 500;

export default class SafeLoader
{
    static safeLoadResWithType (assets, type, callback, initTry = 0) {
        cc.loader.loadRes(assets, type, (error, res) => {
            if (error) {
                if (initTry < retryCount) {
                    initTry++;
                    setTimeout(() => {
                        SafeLoader.safeLoadResWithType(assets, type, callback, initTry);
                    }, retryInterval);
                    console.warn('try reload res: ' + assets);
                } else {
                    console.error("safeLoadResWithType: load resource failed, ret="+ error);
                   callback && callback(error, res);
                }
            } else {
                callback && callback(error, res);
            }
        })
    }
    static safeLoadRes(assets, callback, initTry = 0)
    {
        cc.loader.loadRes(assets, (error, prefab) => {
            if (error) {
                if (initTry < retryCount) {
                    initTry++;
                    setTimeout(() => {
                        SafeLoader.safeLoadRes(assets, callback, initTry);
                    }, retryInterval);
                    console.warn('try reload res: ' + assets);
                } else {
                    console.error(`safeLoadRes: load resource failed, ret = ${error}  assets = ${assets}`);
                    callback && callback(error, prefab);
                }
            } else {
                callback && callback(error, prefab);
            }
        })
    }
}
