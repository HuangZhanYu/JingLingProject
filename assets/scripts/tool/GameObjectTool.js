/*
作者（Author）:    skyHuang

描述（Describe）:
*/
/*jshint esversion: 6 */

export default class GameObjectTool
{
    /// <summary>
    /// 设置跟随镜头速度
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="speed"></param>
    static SetFollowCameraSpeed(obj,speed)
    {
        let fc = obj.getComponent('FollowCamera');
        if (fc == null)
        {
            return;
        }
        fc.SetSpeed(speed);
    }

    /// <summary>
    /// 设置跟随镜头最小移动位置
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="min"></param>
    static SetFollowCameraMin(obj,min)
    {
        let fc = obj.getComponent('FollowCamera');
        if (fc == null)
        {
            return;
        }
        fc.SetMin(min);
    }

    /// <summary>
    /// 设置跟随镜头最大移动位置
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="max"></param>
    static SetFollowCameraMax(obj,max)
    {
        let fc = obj.getComponent('FollowCamera');
        if (fc == null)
        {
            return;
        }
        fc.SetMax(max);
    }


    /// <summary>
    /// 重设镜头
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="enable"></param>
    static SetFollowCameraReset(obj)
    {
        let fc = obj.getComponent('FollowCamera');
        if (fc == null)
        {
            return;
        }
        fc.Reset();
    }

    /// <summary>
    /// 跟随镜头开关
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="enable"></param>
    static SetFollowCameraEnable(obj,enable)
    {
        let fc = obj.getComponent('FollowCamera');
        if (fc == null)
        {
            return;
        }
        fc.enabled = enable;
    }

    /// <summary>
    /// 设置跟随镜头目标
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="target"></param>
    static SetFollowCameraTarget(obj, target)
    {
        let fc = obj.getComponent('FollowCamera');
        if (fc == null)
        {
            return;
        }
        fc.SetTarGet(target);
    }

    /// <summary>
    /// 设置跟随镜头屏蔽触摸
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="shield"></param>
    static SetFollowCameraShieldTouch(obj,shield)
    {

        let fc = obj.getComponent('FollowCamera');
        if (fc == null)
        {
            return;
        }
        fc.ShieldTouch(shield);
    }

    /// <summary>
    /// 设置跟随镜头停止触摸
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="stop"></param>
    static SetFollowCameraStopTouch(obj,stop)
    {
        let fc = obj.getComponent('FollowCamera');
        if (fc == null)
        {
            return;
        }
        fc.StopTouch(stop);
    }

    /// <summary>
    /// 设置跟随镜头停止触摸
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="stop"></param>
    static SetFollowCameraHold(obj,time)
    {
        let fc = obj.getComponent('FollowCamera');
        if (fc == null)
        {
            return;
        }
        fc.Hold(time);
    }
    /// <summary>
    /// 检测一个模型是否存在某个光效
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="effectName"></param>
    /// <returns></returns>
    static CheckExistEffectInFightRole(obj,effectName)
    {
        if (obj == null)
        {
            return false;
        }
        let root = obj.transform;
        //遍历模型子对象，原点位置
        let rootChildCount = root.childCount;
        for (let i = 0; i < rootChildCount; i++)
        {
            let child = root.GetChild(i);
            if (child.name.Equals(effectName))
            {
                return true;
            }
        }

        //遍历血条位置
        let head = root.FindChild(obj.name+"/root/Boold");
        if (head!= null)
        {
            let childCount = head.childCount;
            for(let i = 0; i < childCount; i++)
            {
                let child = head.GetChild(i);
                if (child.name.Equals(effectName))
                {
                    return true;
                }
            }
        }

        //遍历受击点位置
        let qua = root.FindChild(obj.name + "root/Bip01/Qua");
        if (qua != null)
        {
            let childCount = qua.childCount;
            for (let i = 0; i < childCount; i++)
            {
                let child = qua.GetChild(i);
                if (child.name.Equals(effectName))
                {
                    return true;
                }
            }
        }

        return false;
    }

    /////////////////////////////////////////RoleBehaviour函数////////////////////////////////////////////////

    /// <summary>
    /// 设置Role移动
    /// </summary>
    /// <param name="obj"></param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <param name="z"></param>
    /// <param name="speed"></param>
    static SetRoleBehaviourMoveTo(obj,x,y,speed)
    {
        if (obj == null)
        {
            return;
        }
        let rb = obj.obj.getComponent('RoleBehaviour');
        if (rb == null)
        {
            return;
        }
        rb.MoveTo(x, y, z, speed);
    }

    /// <summary>
    /// 停止移动
    /// </summary>
    /// <param name="obj"></param>
    static SetRoleBehaviourStopMove(obj)
    {
        if(obj == null)
        {
            return;
        }
        let rb = obj.obj.getComponent('RoleBehaviour');
        if (rb == null)
        {
            return;
        }
        rb.StopMove();
    }


    /// <summary>
    /// 停止移动
    /// </summary>
    /// <param name="obj"></param>
    static SetRoleBehaviourReleaseEffect(obj)
    {
        if (obj == null)
        {
            return;
        }
        let rb = obj.obj.getComponent('RoleBehaviour');
        if (rb == null)
        {
            return;
        }
        rb.ReleaseEffect();
    }
}
