/*
作者（Author）:    skyHuang

描述（Describe）:
*/
/*jshint esversion: 6 */

import { PlayerIntAttr } from "../part/PlayerBasePart";

export const FuntionEnum = 
{
    PokenMonPosOne : 1,//--精灵阵位1
    PokenMonPosTwo : 2,//--精灵阵位2
    PokenMonPosThree : 3,//--精灵阵位3
    PokenMonPosFour : 4,//--精灵阵位4
    PokenMonPosFive : 5,//--精灵阵位5
    PokenMonPosSix : 6,//--精灵阵位6
    PokenMonPosSeven : 7,//--精灵阵位7
    PokenMonPosEight : 8,//--精灵阵位8
    PokenMonPoseNight : 9,//--精灵阵位9
    PokenMonPosTen : 10,//--精灵阵位10
    PokenMonPosEleven : 11,//--精灵阵位11
    PokenMonPosTwelve : 12,//--精灵阵位12
    EquipPos1 : 13,//--装备位置1
    EquipPos2 : 14,//--装备位置2
    EquipPos3 : 15,//--装备位置3
    EquipPos4 : 16,//--装备位置4
    EquipPos5 : 17,//--装备位置5
    EquipPos6 : 18,//--装备位置6
    EquipPos7 : 19,//--装备位置7
    EquipPos8 : 20,//--装备位置8
    EquipPos9 : 21,//--装备位置9
    EquipPos10 : 22,//--装备位置10
    EquipPos11 : 23,//--装备位置11
    EquipPos12 : 24,//--装备位置12
    EquipPos13 : 25,//--装备位置13
    EquipPos14 : 26,//--装备位置14
    EquipPos15 : 27,//--装备位置15
    EquipPos16 : 28,//--装备位置16
    EquipPos17 : 29,//--装备位置17
    EquipPos18 : 30,//--装备位置18
    EquipPos19 : 31,//--装备位置19
    EquipPos20 : 32,//--装备位置20
    EquipPos21 : 33,//--装备位置21
    EquipPos22 : 34,//--装备位置22
    EquipPos23 : 35,//--装备位置23
    EquipPos24 : 36,//--装备位置24
    EquipPos25 : 37,//--装备位置25
    EquipPos26 : 38,//--装备位置26
    UnLimitDungeon : 50,//--无限制地下城
    RedLimitDugeon : 51,//--限制红地下城
    GreenLimitDugeon : 52,//--限制绿地下城
    YellowLimitDugeon : 53,//--限制黄地下城
    BlueLimitDugeon : 54,//--限制蓝地下城
    GoldDugeon : 55,//--金币地下城
    ResourceDugeon : 56,//--资源地下城
    MainCatchButton : 57,//--主界面捉宠按钮
    MainTaskButton : 58,//--主界面任务
    MainLineUpButton : 59,//--主界面阵容
    MainEquipButton : 60,//--主界面装备
    MainDungeonButton : 61,//--主界面地下城
    MainBagButton : 62,//--主界面背包
    MainShopButton : 63,//--主界面商店
    MainSkillButton : 64,//--主界面技能开放系统
    MainPokedexButton : 65,//--主界面图鉴
    MainResetButton : 66,//--主界面重置
    MainBoxButton : 67,//--主界面宝箱
    DanMu : 68,//--弹幕解锁
    Chat : 69,//--聊天解锁
    MailSystem : 70, //--邮件
    Rank : 71,//--排行榜
    ChongZhiDaRen : 72,//--重置达人
    FightTower : 73,//--挑战塔
    SingleArena : 74,//--训练竞技场
    Friend      : 75,//--好友
    FettersSystem : 76,//--羁绊系统
    Adventure    : 77,//--冒险
    ChengJiu     : 78,//--成就
    MainSetSkillButton : 79,//--主界面技能设置按钮
    JingLingGaoYuan  : 80,//--精灵高原
    HuFu             : 81,//--护符
    DianFengJJC      : 82,//--巅峰竞技场
    GongHuiZhan      : 83,//--公会战
    KuaFuZhan        : 84,//--跨服战
    Rest             : 85,//--灵魂休息
    DaoGuan          : 86,//--道馆
    TaoZhuangTiShi   : 87,//--套装提示
    XiaoJiQiao       : 88,//--小技巧
    YiJianZhuoChong  : 89,//--一键捉宠
    SearchPetPos1    : 90,//--搜寻场景1
    SearchPetPos2    : 91,//--搜寻场景2
    SearchPetPos3    : 92,//--搜寻场景3
    SearchPetPos4    : 93,//--搜寻场景4
    SearchPetPos5    : 94,//--搜寻场景5
    SearchPetPos6    : 95,//--搜寻场景6
    SeniorRefresh    : 96,//--高级刷新
    VIPButton        : 97,//--VIP按钮
    ChargeButton     : 98,//--充值按钮
    WonderfulActivities : 99,//--精彩活动
    SignButton       : 100,//--签到
    FirstChargeButton : 101,//--首充豪礼
    AdventureShopButton : 102,//--冒险商店按钮开放等级
    LimitTimeActButton : 103,//--限时活动功能开放
    FastResetButton    : 104,//--快速重置
    OffLinePassText    : 105,//--离线通关显示
	TeamAttrMain       : 106,//--队伍羁绊
	TeamAttrCount      : 107,//--队伍羁绊-上阵人数
	TeamAttrQuality    : 108,//--队伍羁绊-上阵品质
    TeamAttrJinHua     : 109,//--队伍羁绊-上阵进化
    CollectProperty    : 110,//--收集属性
    MegaEvolution      : 111,//--百万进化
    FBLikeActivity     : 112,//--FB点赞
    FBCommentActivity  : 113,//--FB评论
    ReviveOneKeyGuanQia : 114,//--重置一键领取关卡限制
    TeamStrength        : 116,//--队伍战斗力的功能开放
    PokemonEvoOne       : 117,//--精灵进化+1
    PokemonEvoTwo       : 118,//--精灵进化+2
    PokemonEvoThree     : 119,//--精灵进化+3
    PokemonEvoFour      : 120,//--精灵进化+4
    PokemonEvoFive      : 121,//--精灵进化+5
    PokemonEvoSix       : 122,//--精灵进化+6
    PokemonEvoSeven     : 123,//--精灵进化+7
};

let FunctionOpenTool = (function () 
{
    return {
        //--开放条件类型
        //--1，表示主角等级。
        //--2，表示VIP等级。
        //--3，表示主角等级或VIP等级。
        //--4，表示主角等级和VIP等级。
        //--5，表示公会等级
        //--6,表示主角等级和钻石解锁
        //--7,首次通关第X关。
        //--8，首次通关第x关或者消耗x钻石解锁）


        //--判断某个功能是否开放  根据等级


        //-- 判断某个模块是否开启
        //-- 参数1，FunctionOpenTool.FunctionEnum枚举
        //-- 返回 bool值
        IsFunctionOpen(functionEnum)
        {   
            let funcData = LoadConfig.getConfigData(ConfigName.GongNengKaiFang,functionEnum);
            if(funcData == null){
                return;
            }
            let level = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_DengJi];
            let vipLevel = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_Vip];
            if(level == null || vipLevel == null){
                return false;
            }
            if(funcData.KaiFangType == 1){
                //-- 仅使用等级判断
                return level >= funcData.ZhuJiaoDengJi;
            }else if( funcData.KaiFangType == 2){
                //-- 仅使用VIP等级判断
                return vipLevel >= funcData.VIPDengJi;
            }else if( funcData.KaiFangType == 3){
                //-- 使用等级或者VIP等级判断，两者只要满足其一便可
                return level >= funcData.ZhuJiaoDengJi || vipLevel >= funcData.VIPDengJi;
            }else if( funcData.KaiFangType == 4){
                //-- 同时使用等级和VIP等级判断，必须满足这两者
                return level >= funcData.ZhuJiaoDengJi && vipLevel >= funcData.VIPDengJi;
            }else if( funcData.KaiFangType == 7){
                //-- 同时使用首次通关关卡判断
                return GuanQiaPart.HistoryTop >= funcData.ZhuJiaoDengJi;
            }else if( funcData.KaiFangType == 9){
                //--判断重置次数
                return PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_RevivalNumber] >= funcData.ZhuJiaoDengJi;
            }
        },

        //-- 获取功能开放条件
        //-- 参数1，FunctionOpenTool.FunctionEnum枚举
        //-- 返回值1，开放条件类型
        //-- 1：代表仅使用等级判断
        //-- 2：代表仅使用VIP等级判断
        //-- 3：代表只需要满足等级或者VIP等级判断便可
        //-- 4：代表需要同时满足等级和VIP等级
        //-- 返回值2，开放的等级数值，如果是仅使用vip等级判断的条件类型，该值可能为nil
        //-- 返回值3，开放的VIP等级数值，如果是仅使用等级判断的条件类型，该值可能为nil
        GetFunctionOpenLimit(functionEnum)
        {
            let funcData = LoadConfig.getConfigData(ConfigName.GongNengKaiFang,functionEnum);
            if(funcData == null){
                return;
            }
            if(funcData.KaiFangType == 1){
                //-- 仅使用等级判断
                return [funcData.KaiFangType, funcData.ZhuJiaoDengJi, null];
            }else if( funcData.KaiFangType == 2){
                //-- 仅使用VIP等级判断
                return [funcData.KaiFangType, null, funcData.VIPDengJi];
            }else if( funcData.KaiFangType == 3){
                //-- 使用等级或者VIP等级判断，两者只要满足其一便可
                return [funcData.KaiFangType, funcData.ZhuJiaoDengJi, funcData.VIPDengJi];
            }else if( funcData.KaiFangType == 4){
                //-- 同时使用等级和VIP等级判断，必须满足这两者
                return [funcData.KaiFangType, funcData.ZhuJiaoDengJi, funcData.VIPDengJi];
            }else if( funcData.KaiFangType == 7){ //--首次达到X关
                return [funcData.KaiFangType,funcData.ZhuJiaoDengJi,null];
            }else if( funcData.KaiFangType == 9){ //--重置x次
                return [funcData.KaiFangType,funcData.ZhuJiaoDengJi,null];
            }
        },

        //--获取该功能按钮是否显示
        GetBtnIsShow(functionEnum)
        {
            let funcData = LoadConfig.getConfigData(ConfigName.GongNengKaiFang,functionEnum);
            if(funcData == null){
                console.error("功能开放枚举错误>>>"+functionEnum);
                return false;
            }
            let level = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_DengJi];
            let vipLevel = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_Vip];

            if(funcData.XianShiDengJi == null){
                console.error("功能开放枚举错误>>>"+functionEnum);
                return false;
            }

            if(funcData.ButtonShowType == 1){
                //-- 仅使用等级判断
                return level >= funcData.XianShiDengJi;
            }else if( funcData.ButtonShowType == 2){
                //-- 仅使用VIP等级判断
                return vipLevel >= funcData.XianShiDengJi;
            }else if( funcData.ButtonShowType == 3){
                //-- 使用等级或者VIP等级判断，两者只要满足其一便可
                return level >= funcData.XianShiDengJi || vipLevel >= funcData.XianShiDengJi;
            }else if( funcData.ButtonShowType == 4){
                //-- 同时使用等级和VIP等级判断，必须满足这两者
                return level >= funcData.XianShiDengJi && vipLevel >= funcData.XianShiDengJi;
            }else if( funcData.ButtonShowType == 7){
                //-- 同时使用首次通关关卡判断
                return GuanQiaPart.HistoryTop >= funcData.XianShiDengJi;
            }else if( funcData.ButtonShowType == 9){
                //--判断重置次数
                return PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_RevivalNumber] >= funcData.XianShiDengJi;
            }else{
                //--如果功能开放了，按钮必定显示(容错)
                return this.IsFunctionOpen(functionEnum);
            }
        },

        //--功能开放通用提示
        //--functionEnum:功能枚举
        //--返回bool 如果开放了返回true，没有开放则返回false并弹出提示
        ShowTips(functionEnum)
        {
            //--先判断功能有没开放
            if(this.IsFunctionOpen(functionEnum)){
                return true;
            }
            //--获取功能开放条件
            let limit =[];
            limit = this.GetFunctionOpenLimit(functionEnum);

            let tipsParam = [];
            if(limit[0] == 1){
                //-- 仅使用等级判断
                tipsParam[0] = limit[1];
                ToolTipPopPanel.Show(138,tipsParam);
            }else if( limit[0] == 2){
                //-- 仅使用VIP等级判断
                tipsParam[0] = limit[2];
                ToolTipPopPanel.Show(139,tipsParam);  
            }else if( limit[0] == 3){
                //-- 使用等级或者VIP等级判断，两者只要满足其一便可
                tipsParam[0] = limit[1];
                tipsParam[1] = limit[2];
                ToolTipPopPanel.Show(140,tipsParam);  
            }else if( limit[0] == 4){
                //-- 同时使用等级和VIP等级判断，必须满足这两者
                tipsParam[0] = limit[1];
                tipsParam[1] = limit[2];
                ToolTipPopPanel.Show(141,tipsParam);  
            }else if( limit[0] == 7){
                //--仅使用最高关卡判断
                tipsParam[0] = limit[1];
                ToolTipPopPanel.Show(146,tipsParam);
            }else if( limit[0] == 9){
                //--使用重置次数判断
                tipsParam[0] = limit[1];
                ToolTipPopPanel.Show(260,tipsParam);
            }
            return false;
        }
    };
})();



export default FunctionOpenTool;