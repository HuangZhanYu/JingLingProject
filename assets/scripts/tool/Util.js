/*
作者（Author）:    jason

描述（Describe）: 工具类
*/
/*jshint esversion: 6 */
import { PanelPos } from "../manager/ViewManager";
export default class Util
{
    //搜索节点
    static searchNode(parent, name1, name2) {
        //广搜
        function search(node, name) {
            let queue = [
                [node]
            ];
            for (let i = 0; i < queue.length; ++i) {
                let list = queue[i];
                for (let j = 0; j < list.length; ++j) {
                    let node = list[j];
                    if (node.name === name) {
                        return node;
                    } else {
                        queue.push(node.children);
                    }
                }
            }
            return null;
        }

        let node = parent;
        for (let i = 1; i < arguments.length && node; ++i) {
            node = search(node, arguments[i]);
        }
        return node;
    }
    static searchComp(parent, name, component)
    {
        let node = Util.searchNode(parent, name);
        if (!node) {
            return null;
        }
        let comp = node.getComponent(component);
        if (!comp) {
            return null;
        }
        return comp;
    }
    static Utf8ArrayToStr(array) {
        let out, i, len, c;
        let char2, char3;

        out = "";
        len = array.length;
        i = 0;
        while(i < len) {
            c = array[i++];
            switch(c >> 4)
            {
                case 0: case 1: case 2: case 3: case 4: case 5: case 6: case 7:
                // 0xxxxxxx
                out += String.fromCharCode(c);
                break;
                case 12: case 13:
                // 110x xxxx   10xx xxxx
                char2 = array[i++];
                out += String.fromCharCode(((c & 0x1F) << 6) | (char2 & 0x3F));
                break;
                case 14:
                    // 1110 xxxx  10xx xxxx  10xx xxxx
                    char2 = array[i++];
                    char3 = array[i++];
                    out += String.fromCharCode(((c & 0x0F) << 12) |
                    ((char2 & 0x3F) << 6) |
                    ((char3 & 0x3F) << 0));
                    break;
            }
        }

        return out;
    }
    static getRootCanvas(parent,panelPos) {
        let scene = cc.director.getScene();
        let canvas = null;
        if (parent) {
            canvas = parent;
        }
        else {
            canvas = scene.getChildByName('Canvas');
            switch(panelPos)
            {
                case PanelPos.PP_Bottom:
                    return this.searchNode(canvas,'UINodeBottom');
                case PanelPos.PP_Midle:
                    return this.searchNode(canvas,'UINodeMidle');
                case PanelPos.PP_Top:
                    return this.searchNode(canvas,'UINodeTop');
                default:
                    return canvas;
            }
        }
        return canvas;
    }
    //获取两点距离
    static getDistanceVec2(pos1,pos2) {
        if(pos1 != typeof(cc.Vec2) || pos2 != typeof(cc.Vec2))
        {
            return null;
        }
        return pos1.divSelf(pos2).mag();
    }

}
