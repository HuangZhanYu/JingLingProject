/*
作者（Author）:    jason

描述（Describe）: 游戏内事件处理类
*/
//当前事件所引用到的对象
class MessageRecvObj {
    constructor()
    {
        this._tObj = null;
    }
    dtor()
    {
        this._tObj = null;
    }
    exeMessage(messageID, func, param)
    {
        func.call(this._tObj,param);
    }
};

let MessageCenter = (function(){
    
    let _tMessageDict = {};			//记录监听消息ID的对象
    let _bIsGlobalCaching = false;	//是否缓冲消息
    let _tMessageBuffer = [];			//全局消息缓存区
    let _tObjectList = [];			//记录消息接受者的一些数据
    let getRecvObjIdx = function(obj)
    {
        for (let i = 0; i < _tObjectList.length; ++i)
		{
			if (_tObjectList[i]._tObj === obj)
			{
				return i;
			}
		}
		return -1;
    }
    let getObjArr = function(messageID)
	{
		let arr = _tMessageDict[messageID];
		if (arr === undefined)
		{
			arr = [];
			_tMessageDict[messageID] = arr;
		}
		return arr;
    }
    let exeMessage = function(messageID,param)
    {
        let arr = getObjArr(messageID);
        for (let i = 0;i < arr.length;i += 2)
        {
            let recvObj = arr[i];
            let func = arr[i + 1];
            recvObj.exeMessage(messageID,func,param);
        }
    }
    return {
        //注册监听
        /*
        @obj 当前监听事件的对象
        @func 当前对象接受事件的函数
        @messageID 消息ID
        */
        addListen: function(obj,func,messageID)
        {
            let recvObj = null;
            let idx = getRecvObjIdx(obj);
            if (idx < 0)
            {
                recvObj = new MessageRecvObj();
                recvObj._tObj = obj;
                _tObjectList.push(recvObj);
            }
            else
            {
                recvObj = _tObjectList[idx];
            }

            let arr = getObjArr(messageID);
            if (arr.indexOf(obj) === -1)
            {
                arr.push(recvObj,func);
            }
        },
        //反注册监听
        delListen: function(obj)
        {
            let idx = getRecvObjIdx(obj);
            if (idx >= 0)
            {
                let recvObj = _tObjectList[idx];

                for (let k in _tMessageDict)
                {
                    let arr = _tMessageDict[k];
                    for (let i = 0;i < arr.length;i += 2)
                    {
                        if (arr[i] === recvObj)
                        {
                            arr.splice(i,2);
                            break;
                        }
                    }
                }

                recvObj.dtor();
                _tObjectList.splice(idx,1);
            }
        },
        //广播消息
        sendMessage: function(messageID,param)
        {
            if (_bIsGlobalCaching)
            {
                _tMessageBuffer.push([messageID,param]);
            }
            else
            {
                exeMessage(messageID,param);
            }
        },
        //设置是否全局cache消息
        setGlobalCacheMessage: function(isGlobalCaching)
        {
            _bIsGlobalCaching = isGlobalCaching;
            if (!_bIsGlobalCaching)
            {
                for (let i = 0;i < _tMessageBuffer.length;++i)
                {
                    let message = _tMessageBuffer[i];
                    let messageID = message[0];
                    let param = message[1];
                    exeMessage(messageID,param);
                }
                _tMessageBuffer.length = 0;
            }
        }
    };
})();

export default MessageCenter;