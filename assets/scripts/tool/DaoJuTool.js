/*
作者（Author）:    skyHuang

描述（Describe）:
*/
/*jshint esversion: 6 */
import AtlasTool from "./AtlasTool";
import Util from "./Util";
import BigNumber from "./BigNumber";
import ResourceManager from "./ResourceManager";
import { CommonPrefabNames } from "../common/PanelResName";

let DaoJuTool = (function () 
{
    return {
        //--设置边框品质
        //--白 绿 蓝 紫 红 金
        SetQualityImage(go,quality)
        {
            if(quality == DaoJuTool.Quality.Green)
            {
                AtlasTool.setAtlasImage(go,"BianKuangAtlas","GY_0066_lvpinzhi");
            }else if(quality == DaoJuTool.Quality.Blue)
            {
                AtlasTool.setAtlasImage(go,"BianKuangAtlas","GY_0068_lanpinzhi");
            }else if(quality == DaoJuTool.Quality.Purple)
            {
                AtlasTool.setAtlasImage(go,"BianKuangAtlas","GY_0067_zipinzhi");
            }else if(quality == DaoJuTool.Quality.Orange)
            {
                AtlasTool.setAtlasImage(go,"BianKuangAtlas","GY_0065_chengpinzhi");
            }else if(quality == DaoJuTool.Quality.Red)
            {
                AtlasTool.setAtlasImage(go,"BianKuangAtlas","GY_0064_hongpinzhi");
            }else if(quality == DaoJuTool.Quality.White)
            {
                AtlasTool.setAtlasImage(go,"BianKuangAtlas","GY_0069_baipinzhi");
            }else if(quality == DaoJuTool.Quality.Color)
            {
                AtlasTool.setAtlasImage(go,"BianKuangAtlas","GY_0062_caisepinzhi");
            }
            else
            {
                console.error("设置边框品质 数据错误,quality=="+quality);
            }
        },

        //--设置角标品质
        SetConerQualityImage(go,quality)
        {
            if(quality == DaoJuTool.Quality.Green)
            {
                AtlasTool.setAtlasImage(go,"BianKuangAtlas","GY_0066_lvpinzhi-biao");
            }else if(quality == DaoJuTool.Quality.Blue)
            {
                AtlasTool.setAtlasImage(go,"BianKuangAtlas","GY_0068_lanpinzhi-biao");
            }else if(quality == DaoJuTool.Quality.Purple)
            {
                AtlasTool.setAtlasImage(go,"BianKuangAtlas","GY_0067_zipinzhi-biao");
            }else if(quality == DaoJuTool.Quality.Orange)
            {
                AtlasTool.setAtlasImage(go,"BianKuangAtlas","GY_0065_chengpinzhi-biao");
            }else if(quality == DaoJuTool.Quality.Red)
            {
                AtlasTool.setAtlasImage(go,"BianKuangAtlas","GY_0064_hongpinzhi-biao");
            }else if(quality == DaoJuTool.Quality.White)
            {
                AtlasTool.setAtlasImage(go,"BianKuangAtlas","GY_0069_baipinzhi-biao");
            }else if(quality == DaoJuTool.Quality.Color)
            {
                AtlasTool.setAtlasImage(go,"BianKuangAtlas","GY_0062_caisepinzhi-biao");
            }
            else
            {
                console.error("设置角标品质 数据错误,quality=="+quality);
            }
        },

        //--设置角表图标
        SetConerImage(go,evoLevel)
        {
            if(evoLevel == 1)
            {
                AtlasTool.setAtlasImage(go,"JiaoBiaoAtlas","JH-+1");
            }else if(evoLevel == 2)
            {
                AtlasTool.setAtlasImage(go,"JiaoBiaoAtlas","JH-+2");
            }else if(evoLevel == 3)
            {
                AtlasTool.setAtlasImage(go,"JiaoBiaoAtlas","JH-+3");
            }else if(evoLevel == 4)
            {
                AtlasTool.setAtlasImage(go,"JiaoBiaoAtlas","JH-+4");
            }else if(evoLevel == 5)
            {
                AtlasTool.setAtlasImage(go,"JiaoBiaoAtlas","JH-+5");
            }else if(evoLevel == 6)
            {
                AtlasTool.setAtlasImage(go,"JiaoBiaoAtlas","JH-+6");
            }else if(evoLevel == 7)
            {
                AtlasTool.setAtlasImage(go,"JiaoBiaoAtlas","JH-+7");
            }
            else
            {
                console.error("设置角表图标 数据错误,进化等级"+evoLevel)
            }
        },

        //--设置属性角标，参数：道具，属性类别，mLuabehaviour
        SetConerAttrImage(go,attrType)
        {
            if(attrType == 1)
            {
                AtlasTool.setAtlasImage(go,"BonusIconAtlas","SX-Bing"); //--冰
            }else if(attrType == 2)
            {
                AtlasTool.setAtlasImage(go,"BonusIconAtlas","SX-Long"); //--龙
            }else if(attrType == 3)
            {
                AtlasTool.setAtlasImage(go,"BonusIconAtlas","SX-Huo"); //--火
            }else if(attrType == 4)
            {
                AtlasTool.setAtlasImage(go,"BonusIconAtlas","SX-Cao"); //--草
            }else if(attrType == 5)
            {
                AtlasTool.setAtlasImage(go,"BonusIconAtlas","SX-Shui"); //--水
            }else if(attrType == 6)
            {
                AtlasTool.setAtlasImage(go,"BonusIconAtlas","SX-DiMian"); //--地面
            }else if(attrType == 7)
            {
                AtlasTool.setAtlasImage(go,"BonusIconAtlas","SX-Dian"); //--电
            }else if(attrType == 8)
            {
                AtlasTool.setAtlasImage(go,"BonusIconAtlas","SX-ChaoNengLi"); //--超能
            }else if(attrType == 9)
            {
                AtlasTool.setAtlasImage(go,"BonusIconAtlas","SX-Du"); //--毒
            }else if(attrType == 10)
            {
                AtlasTool.setAtlasImage(go,"BonusIconAtlas","SX-YaoJing"); //--妖精
            }else if(attrType == 11)
            {
                AtlasTool.setAtlasImage(go,"BonusIconAtlas","SX-GeDou"); //--格斗
            }else if(attrType == 12)
            {
                AtlasTool.setAtlasImage(go,"BonusIconAtlas","SX-Gang"); //--钢
            }else if(attrType == 13)
            {
                AtlasTool.setAtlasImage(go,"BonusIconAtlas","SX-YanShi"); //--岩石
            }else if(attrType == 14)
            {
                AtlasTool.setAtlasImage(go,"BonusIconAtlas","SX-Chong"); //--虫
            }else if(attrType == 15)
            {
                AtlasTool.setAtlasImage(go,"BonusIconAtlas","SX-E"); //--恶
            }else if(attrType == 16)
            {
                AtlasTool.setAtlasImage(go,"BonusIconAtlas","SX-YouLing"); //--幽灵
            }else if(attrType == 17)
            {
                AtlasTool.setAtlasImage(go,"BonusIconAtlas","SX-YiBan"); //--一般
            }
        },

        //--设置道具图标
        SetIcon(go,resourceType,resourceID)
        {
            if(resourceType == DaoJuTool.ResourceType.HuoBiBiao)
            {
                let huobiInfo = LoadConfig.getConfigData(ConfigName.ZiYuan_HuoBi,resourceID);
                if(huobiInfo == null)
                {
                    console.error("货币表ID错误"+resourceID);
                    return;
                }
                AtlasTool.setAtlasImage(go,"PropIconAutoAtlas",huobiInfo.Icon);

            }else if(resourceType == DaoJuTool.ResourceType.DaoJuBiao)
            {
                let daojuInfo = LoadConfig.getConfigData(ConfigName.ZiYuan_DaoJu,resourceID);
                if(daojuInfo == null)
                {
                    console.error("道具表ID错误"+resourceID);
                    return;
                }

                if(daojuInfo.ResourceType == 10)
                {
                    AtlasTool.setAtlasImage(go,"HeadIconAutoAtlas",daojuInfo.Icon);
                }else if(daojuInfo.ResourceType == 11)
                {
                    AtlasTool.setAtlasImage(go,"EquipIconAutoAtlas",daojuInfo.Icon);
                }
                else
                {
                    AtlasTool.setAtlasImage(go,"PropIconAutoAtlas",daojuInfo.Icon);
                }

            }else if(resourceType == DaoJuTool.ResourceType.ShiBingBiao)
            {
                let shiBingInfo = LoadConfig.getConfigData(ConfigName.ShiBing,resourceID);
                if(shiBingInfo == null)
                {
                    console.error("士兵表ID错误"+resourceID);
                    return;
                }
                let shibingIcon = LoadConfig.getConfigData(ConfigName.MoXing,shiBingInfo.ModelID);
                if(shibingIcon == null)
                {
                    console.error("士兵模型表ID错误"+shiBingInfo.ModelID);
                    return;
                }
                AtlasTool.setAtlasImage(go,"HeadIconAutoAtlas",shibingIcon.Icon);

            }else if(resourceType == DaoJuTool.ResourceType.ZhuangBeiBiao)
            {
                let zhuangbeiInfo = LoadConfig.getConfigData(ConfigName.Yiwu,resourceID);
                if(zhuangbeiInfo == null)
                {
                    console.error("遗物表ID错误"+resourceID);
                    return;
                }

                AtlasTool.setAtlasImage(go,"EquipIconAutoAtlas",zhuangbeiInfo.Icon);
            }else if(resourceType == DaoJuTool.ResourceType.Guild)
            {
                let daoguanInfo = LoadConfig.getConfigData(ConfigName.ZiYuan_DaoGuan,resourceID);
                if(daoguanInfo == null)
                {
                    console.error("道馆表ID错误"+resourceID);
                    return;
                }
                AtlasTool.setAtlasImage(go,"PropIconAutoAtlas",daoguanInfo.Icon);
            }
        },

        //--设置道具的所有图片
        SetDaoJuImage(go,resourceType,resourceID,equipEvo)
        {
            //--角表品质 
            let conerQuality = cc.find("icon",go);
            let quality = null;
            if(conerQuality == null)
            {
                return;
            }
            //--角表图标
            let conerIcon = cc.find("icon/daojuIcon",go).getComponent(cc.Sprite);

            if(resourceType == DaoJuTool.ResourceType.HuoBiBiao)   
            { 
                let huobiInfo = LoadConfig.getConfigData(ConfigName.ZiYuan_HuoBi,resourceID);
                if(huobiInfo == null)
                {
                    console.error("货币表ID错误"+resourceID);
                    return;
                }
                quality = huobiInfo.Quality;
                conerQuality.active = false;

            }else if(resourceType == DaoJuTool.ResourceType.DaoJuBiao)
            {
                let daojuInfo = LoadConfig.getConfigData(ConfigName.ZiYuan_DaoJu,resourceID);
                if(daojuInfo == null)
                {
                    console.error("道具表ID错误"+resourceID);
                    return;
                }
                quality = daojuInfo.Quality;

                let isSuiPian = daojuInfo.ResourceType == 9 || daojuInfo.ResourceType == 10 || daojuInfo.ResourceType == 11
                conerQuality.active = isSuiPian;
                if(isSuiPian)
                {
                    AtlasTool.setAtlasImage(conerIcon,"TuBiaoAtlas",'GY-tubiao-suipian');
                }
            }else if(resourceType == DaoJuTool.ResourceType.ShiBingBiao)
            {
                let shiBingInfo = LoadConfig.getConfigData(ConfigName.ShiBing,resourceID);
                if(shiBingInfo == null)
                {
                    console.error("士兵表ID错误"+resourceID)
                    return;
                }
                conerQuality.active = true;
                quality = shiBingInfo.Quality;
                //--设置角表 图标
                if(shiBingInfo.EVO > 0)
                {
                    DaoJuTool.SetConerImage(conerIcon,shiBingInfo.EVO);
                }
                else
                {
                    conerQuality.active = false; 
                }
            }else if(resourceType == DaoJuTool.ResourceType.ZhuangBeiBiao)
            {
                let zhuangbeiInfo = LoadConfig.getConfigData(ConfigName.Yiwu,resourceID);
                if(zhuangbeiInfo == null)
                {
                    console.error("遗物表ID错误"+resourceID);
                    return;
                }
                conerQuality.active = true 
                quality = zhuangbeiInfo.Quality
                //--设置角表 图标
                if(equipEvo != null && equipEvo != "")
                {
                    if(equipEvo > 0)
                    {
                        DaoJuTool.SetConerImage(conerIcon,equipEvo)
                    }
                    else
                    {
                        conerQuality.active = false 
                    }
                }
                else 
                {
                    conerQuality.active = false 
                }
            }else if(resourceType == DaoJuTool.ResourceType.Guild)
            {
                let daoguanInfo = LoadConfig.getConfigData(ConfigName.ZiYuan_DaoGuan,resourceID);
                if(daoguanInfo == null)
                {
                    console.error("道馆表ID错误"+resourceID)
                    return
                }
                quality = daoguanInfo.Quality
                conerQuality.active = false
            }
            else
            {
                return;
            }

            //--图标
            let icon = cc.find("Frame/icon",go).getComponent(cc.Sprite);
            //--边框
            let frame = cc.find("Frame",go).getComponent(cc.Sprite);

            //--设置图标
            DaoJuTool.SetIcon(icon,resourceType,resourceID)
            //--设置边框
            DaoJuTool.SetQualityImage(frame,quality)
            //--设置角标 品质
            DaoJuTool.SetConerQualityImage(conerQuality.getComponent(cc.Sprite),quality)
        },

        //--士兵专用，特别显示：右上角属性类型图标；其它显示：士兵图标、边框
        SetSoldierAttrTypeIcon(go,resourceID)
        {
            //--只有士兵才设置
            let shiBingInfo = LoadConfig.getConfigData(ConfigName.ShiBing,resourceID);
            if(shiBingInfo == null)
            {
                console.error("士兵表ID错误"+resourceID);
                return;
            }
            //--角表品质
            let conerQuality = cc.find("icon",go);
            conerQuality.active = false;
            //--右上角属性类型角标
            let attrTypeImg = cc.find("Frame/attrType",go);
            attrTypeImg.active = true;
            DaoJuTool.SetConerAttrImage(attrTypeImg.getComponent(cc.Sprite),shiBingInfo.Race);
            //--图标
            let icon = cc.find("Frame/icon",go).getComponent(cc.Sprite);
            //--边框
            let frame = cc.find("Frame",go).getComponent(cc.Sprite);
            //--设置图标
            DaoJuTool.SetIcon(icon,DaoJuTool.ResourceType.ShiBingBiao,resourceID);
            //--设置边框
            DaoJuTool.SetQualityImage(frame,shiBingInfo.Quality);
        },

        //--设置道具数字显示 (阵容用 非道具公用)
        //--设置上面和下面的文字
        SetDaoJuText(go,textOne,textTwo)
        {
            let contentOne = cc.find("QiangHuaLv",go).getComponent(cc.Label);
            let contentTwo = cc.find("TianFuLv",go).getComponent(cc.Label);

            contentOne.string = textOne;
            contentTwo.string = textTwo;
        },

        //--设置道具点击事件
        SetClickEvent(go,func,params)
        {
            let frame = cc.find("Frame",go)
            let button = frame.getComponent(cc.Button)
            button.enabled = true
            frame.on('click', func.bind(this, params));
        },


        //--统一给道具加上详情事件
        SetDaoJuInfoClickEvent(go,params)
        {
            let frame = cc.find("Frame",go)
            if(params.ResType == DaoJuTool.ResourceType.HuoBiBiao || params.ResType == DaoJuTool.ResourceType.DaoJuBiao ||
            params.ResType == DaoJuTool.ResourceType.Guild)
            {
                frame.on('click', DaoJuTool.OnHuoBiOrDaoJuClick.bind(this, {ResType : params.ResType,ResID : params.ResID}));
            }else if(params.ResType == DaoJuTool.ResourceType.ShiBingBiao)
            {
                frame.on('click', DaoJuTool.OnCardInfoClick.bind(this, {ResType : params.ResType,ResID : params.ResID}));
            }else if(params.ResType == DaoJuTool.ResourceType.ZhuangBeiBiao)
            {
                frame.on('click', DaoJuTool.OnEquipInfoClick.bind(this, {ResType : params.ResType,ResID : params.ResID}));
            }else
            {
                console.error("未添加类型params.ResType"+params.ResType)
            }
        },


        SetPressClickEvent(go,params)
        {
            // let mPress = Util.searchNode(go, "Frame");
            // if(params.ResType == DaoJuTool.ResourceType.HuoBiBiao || params.ResType == DaoJuTool.ResourceType.DaoJuBiao ||
            // params.ResType == DaoJuTool.ResourceType.Guild)
            // {
            //     mPress.AddPressClick(go,null,DaoJuTool.OnHuoBiOrDaoJuClick,null,{ResType : params.ResType,ResID : params.ResID})
            // }else if(params.ResType == DaoJuTool.ResourceType.ShiBingBiao)
            // {
            //     mPress.AddPressClick(go,null,DaoJuTool.OnCardInfoClick,null,{ResType : params.ResType,ResID : params.ResID})
            // }else if(params.ResType == DaoJuTool.ResourceType.ZhuangBeiBiao)
            // {
            //     mPress.AddPressClick(go,null,DaoJuTool.OnEquipInfoClick,null,{ResType : params.ResType,ResID : params.ResID}) 
            // }else
            // {
            //     console.error("未添加类型params.ResType"+params.ResType)
            // }
        },

        //--给道具添加点击事件
        AddDaoJuClick(go,resourceType,resourceID)
        {
            let frame = cc.find("Frame",go);

            let button = frame.getComponent(cc.Button);
            button.enabled = true;

            if(resourceType == DaoJuTool.ResourceType.HuoBiBiao || resourceType == DaoJuTool.ResourceType.DaoJuBiao ||
            resourceType == DaoJuTool.ResourceType.Guild)
            {
                frame.on('click', DaoJuTool.OnHuoBiOrDaoJuClick.bind(this, {ResType : resourceType,ResID : resourceID}));
            }else if(resourceType == DaoJuTool.ResourceType.ShiBingBiao)
            {
                frame.on('click', DaoJuTool.OnCardInfoClick.bind(this, {ResID : resourceID}));
            }else if(resourceType == DaoJuTool.ResourceType.ZhuangBeiBiao)
            {
                frame.on('click', DaoJuTool.OnEquipInfoClick.bind(this, {ResID : resourceID}));
            }else
            {
                console.error("添加道具点击事件  数据错误resourceType."+resourceType+"|resourceID."+resourceID);
            }
        },

        //--道具公用显示界面
        ShowPopPanel(resourceType,resourceID)
        {
            if(resourceType == DaoJuTool.ResourceType.HuoBiBiao || resourceType == DaoJuTool.ResourceType.DaoJuBiao ||
            resourceType == DaoJuTool.ResourceType.Guild)
            {
                this.OnHuoBiOrDaoJuClick(null,{ResType : resourceType,ResID : resourceID});
            }else if(resourceType == DaoJuTool.ResourceType.ShiBingBiao)
            {
                this.OnCardInfoClick(null,{ResID : resourceID});
            }else if(resourceType == DaoJuTool.ResourceType.ZhuangBeiBiao)
            {
                this.OnEquipInfoClick(null,{ResID : resourceID});
            }else
            {
                console.error("数据错误resourceType."+resourceType+"|resourceID."+resourceID);
            }
        },

        //--道具 货币 详情
        OnHuoBiOrDaoJuClick(go,data)
        {
            //--如果是礼包 限时特殊界面
            if(data.ResType == ResourceTypeEnum.ResType_DaoJu)
            {
                let daojuData = LoadConfig.getConfigData(ConfigName.ZiYuan_DaoJu,resourceID);;
                if(daojuData == null)
                {
                    console.error("道具不存在,ID"+data.ResID);
                    return;
                }

                if(daojuData.Peek == 1)
                {
                    //LiBaoYuLanPopPanel.Show(data.ResType,data.ResID);
                    return;
                }
            }

            //ToolTipPopPanel.ShowDaoJuInfo(data.ResType,data.ResID,data.ResCount);
        },

        //--卡牌详情
        OnCardInfoClick(go,data)
        {
            //DigimonDetailPopPanel.ShowCardInfo(data.ResID);
        },

        //--装备详情
        OnEquipInfoClick(go,data)
        {
            //EquipInfoPanel.Show(ResourceTypeEnum.ResType_ZhuangBei,data.ResID,3,null);
        },

        //--设置道具数量  (公用)
        SetDaoJuItemText(go,count)
        {
            let contentOne = cc.find("QiangHuaLv",go);
            if(contentOne == null)
            {
                return;
            }
            let contentTwo = cc.find("TianFuLv",go);
            let contentThree = cc.find("Count",go);

            contentOne.active = false;
            contentTwo.active = false;
            contentThree.active = true;

            if(count == null)
            {
                contentOne.active = false
                contentTwo.active = false
                contentThree.active = false
                return
            }

            //--有可能是数字类型  或者字符串类型   string有可能是12121212    12a
            let bigNumCount = BigNumber.create(count);
            bigNumCount = BigNumber.getValue(bigNumCount);
            contentThree.getComponent(cc.Label).string = "x"+bigNumCount;
        },

        //--背包装备显示专用：同时显示强化等级和个数
        SetDaoJuEquipText(go,count,level)
        {
            let contentOne = cc.find("QiangHuaLv",go);
            if(contentOne == null)
            {
                return
            }
            let contentTwo = cc.find("TianFuLv",go);
            let contentThree = cc.find("Count",go);

            contentOne.active = true
            contentTwo.active = false
            contentThree.active = true

            if(count == null)
            {
                contentOne.active = false
                contentTwo.active = false
                contentThree.active = false
                return
            }
            
            //--有可能是数字类型  或者字符串类型   string有可能是12121212    12a
            let bigNumCount = BigNumber.create(count);
            bigNumCount = BigNumber.getValue(bigNumCount);
            let contentThreeLabel = contentThree.getComponent(cc.Label);
            contentThreeLabel.string = "x"+bigNumCount;
            let contentOneLabel = contentOne.getComponent(cc.Label);
            //--装备等级  
            if(level == 0)
            {
                contentOneLabel.string = "";
            }else if(level > 0)
            {
                contentOneLabel.string = "+"+level;
            }else
            {
                console.error("数据错误，装备等级小于0");
                return;
            }
        },

        //--设置道具不可点击
        DisableClickEvent(go)
        {
            let button = cc.find("Frame",go).getComponent(cc.Button);
            button.enabled = false;
        },

        //--设置道具不可长按
        DisablePressEvent(go)
        {
            //let buttonPress = Util.searchNode(go, "Frame").getComponent("ButtonPressTool")
            //buttonPress.enabled = false
        },

        //--对外接口，设置公共道具信息,如果不需要显示数量，传空
        SetDaoJuInfo(go,resourceType,resourceID,resourceCount)
        {
            DaoJuTool.SetDaoJuImage(go,resourceType,resourceID);
            DaoJuTool.SetDaoJuItemText(go,resourceCount);
        },

        //--挑战塔用，设置精灵进化等级
        SetFightTowerDaoJuInfo(go,resourceType,resourceID,resourceCount)
        {
            DaoJuTool.SetDaoJuImage(go,resourceType,resourceID);
            //--角表品质
            let conerQuality = cc.find("icon",go);
            conerQuality.active = true;
            //--角表图标
            let conerIcon = cc.find("icon/daojuIcon",go);

            let shibing = LoadConfig.getConfigData(ConfigName.ShiBing,resourceID);
            if(shibing == null)
            {
                console.error("士兵表为空"+resourceID);
                return;
            }
            //--设置角表 图标
            if(shibing.EVO > 0)
            {
                DaoJuTool.SetConerImage(conerIcon,shibing.EVO);
            }else
            {
                conerQuality.active = false ;
            }

            //--设置物理还是魔法攻击
            let atkType = cc.find("Frame/attType",go);
            atkType.active = false;//--新需求，物理还是魔法不显示了，在那里显示属性
            //-- if(shibing.HurtType == 1)
            //--     luabehaviour.SetImage(atkType,"common","GY-tubiao-wugong")
            //-- }else if(shibing.HurtType == 2)
            //--     luabehaviour.SetImage(atkType,"common","GY-tubiao-fagong")
            //-- else
            //--     console.error("攻击类型错误，士兵"+resourceID+"攻击类型"+shibing.HurtType)
            //-- }

            //--右上角属性类型角标
            let attrTypeImg = cc.find("Frame/attrType",go);
            attrTypeImg.active = true;
            DaoJuTool.SetConerAttrImage(attrTypeImg,shibing.Race);

            //--显示数量
            if(resourceCount != null)
            {
                let contentOne = cc.find("QiangHuaLv",go);
                let contentTwo = cc.find("TianFuLv",go);
                let contentThree = cc.find("Count",go);

                contentOne.active = false;
                contentTwo.active = false;
                contentThree.active = true;

                contentThree.getComponent(cc.Label).string = resourceCount;
            }
        },

        //--单独打开物免魔免
        SetDefend(go,defendType)
        {

            let defend = cc.find("Frame/defend",go).getComponent(cc.Sprite);

            if(defendType == 1) //--物免
            {
                defend.node.active = true
                AtlasTool.setAtlasImage(defend,"BonusIconAtlas","BJ_WuMian")
            }else if(defendType == 2) //--魔免
            {
                defend.node.active = true
                AtlasTool.setAtlasImage(defend,"BonusIconAtlas","BJ_MoMian")
            }else //--不免疫
            {
                defend.node.active = false
            }
        },

        //--加载公用道具到一个父物体
        LoadDaoJu(obj)
        {
            //--加载道具
            let daoJu = this.getPrefabs(CommonPrefabNames.DaoJuPrefab);
            if(!daoJu)
            {
                return null;
            }
            //-- 创建一个道具UI
            let daoJuObj = cc.instantiate(daoJu.obj);
            //--设置父节点
            daoJuObj.parent = obj;
            //--设置伸缩
            daoJuObj.scale = 1;
            //--设置位置
            daoJuObj.position = cc.p(0,0);
            //--设置名字
            daoJuObj.name = "DaoJuPrefab";
            return daoJuObj;
        },
        /**
         * 加载一个技能预制体
         * @param {*} parent 
         */
        LoadSkillItem(parent)
        {
            let skillPrefab = this.getPrefabs(CommonPrefabNames.SkillPrefabs);
            if(!skillPrefab)
            {
                return null;
            }
            //-- 创建一个道具UI
            let itemObj = cc.instantiate(skillPrefab.obj);
            //--设置父节点
            itemObj.parent = obj;
            //--设置伸缩
            itemObj.scale = 1;
            //--设置位置
            itemObj.position = cc.p(0,0);
            //--设置名字
            itemObj.name = "SkillPrefabs";
            return itemObj;
        },

        getPrefabs(name)
        {
            let index = name.lastIndexOf("/");
            if(index != -1)
            {
                let prafabName = name.substr(index+1);
                return ResourceManager.getPrefab(prafabName);
            }
            return null;
        }
    };
})();


DaoJuTool.ResourceType = {
    HuoBiBiao     : 1,  ////--货币表
    DaoJuBiao     : 2,  ////--道具表
    ShiBingBiao   : 3,  ////--士兵表
    ZhuangBeiBiao : 4,  ////--装备表
    JiaSu         : 5,  ////--加速表
    ZhuDongJi     : 6,  ////--主动技表
    Guild         : 7,  ////--公会资源，实际上也在货币表
},

DaoJuTool.Quality = {
    White  : 1,  ////--白色品质
    Green  : 2,  ////--绿色品质
    Blue   : 3,  ////--蓝色品质
    Purple : 4,  ////--紫色品质
    Orange : 5,  ////--橙色品质
    Red    : 6,  ////--红色品质
    Color  : 7,  ////--彩色品质
    Max    : 7,  ////--最大品质
},

DaoJuTool.AttackType = {
    Physic : 1,    ////--物理
    Magic  : 2,    ////--魔法
},

////--道具标记
DaoJuTool.Flag = {
    ContainEquip : 1////--标记为1代表这个道具可以开出装备
}
export default DaoJuTool;