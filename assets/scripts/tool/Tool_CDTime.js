/*jshint esversion: 6 */
import TimerManager from "./TimerManager";

//region *.lua
//Date
//此文件由[BabeLua]插件自动生成

class CDTimer{
    constructor()
    {
        this.mCDSeconds       = 0;    //倒计时总时间（秒）
        this.mHour            = 0;    //小时
        this.mMinute          = 0;    //分
        this.mSecond          = 0;    //秒
        this.mRefreshFunc     = null;  //倒计时每一秒回调 回调格式 funcname(args) 其中args就是类似 1:20:30时间字符串
        this.mRefreshFuncName = '';   //倒计时刷新回调函数名字
        this.mCdTimeEndFunc   = null;  //倒计时结束回调
    }
    //启动倒计时
    StartCDLHTime(refreshFunc,refreshFuncName,CDSeconds,cdTimeEndFunc)
    {
        this.mCDSeconds    = CDSeconds;   
        this.Hour     = parseInt(CDSeconds/3600);
        this.Minute   = parseInt((CDSeconds%3600)/60);
        this.Second        = CDSeconds%60;
        this.mRefreshFunc = refreshFunc;
        this.mRefreshFuncName = refreshFuncName;
        this.mCdTimeEndFunc   = cdTimeEndFunc;
        
        TimerManager.addTimeCallBack(1,this.RefreshLHCDTime.bind(this));
    }

    //刷新倒计时
    RefreshLHCDTime()
    {
        this.Second = this.Second - 1

        //这里就不用总秒数计算时分秒，减少计算量
        if(this.Second < 0)
        {
            //秒数为0的时候 分钟数-1
            this.Second = 59;
            this.Minute = this.Minute - 1;
            if(this.Minute < 0)
            {
                //分钟数为0的时候，小时数-1
                this.Minute = 59;
                this.Hour = this.Hour - 1;
                if(this.Hour < 0)
                {
                    //小时数也为0的时候则表示倒计时结束
                    this.Hour   = 0;
                    this.Minute = 0;
                    this.Second = 0;
                }
            }
        }
        //刷新倒计时
        let cdtime = "";
        if(this.Second < 10)
        {
            if(this.Minute < 10)
                cdtime = `${this.Hour}:`+`00${this.Minute}:`.substr(this.Minute.toString().length)+`00${this.Second}`.substr(this.Second.toString().length);
            else
                cdtime = `${this.Hour}:${this.Minute}:`+`00${this.Second}`.substr(this.Second.toString().length);
        }     
        else
        {
            if(this.Minute < 10)     
                cdtime = `${this.Hour}:`+`00${this.Minute}:`.substr(this.Minute.toString().length)+`${this.Second}`;
            else
                cdtime = `${this.Hour}:${this.Minute}:${this.Second}`;
        }
        let seconds = this.Second + this.Minute * 60;
        this.mRefreshFunc(cdtime,this.mRefreshFuncName,seconds);
        this.mCDSeconds = this.mCDSeconds - 1;

        //倒计时结束
        if(this.mCDSeconds <= 0)
        {
            if(null != this.mCdTimeEndFunc)
                this.mCdTimeEndFunc();
                    
            Tool_CDTime_RemoveTimer(this.mRefreshFuncName);
        }
        else
        {
            TimerManager.addTimeCallBack(1,this.RefreshLHCDTime.bind(this));
        }
    }    
    //启动倒计时
    StartCDTime(refreshFunc,refreshFuncName,CDSeconds,cdTimeEndFunc)
    {
        let temp = 0
        this.mCDSeconds    = CDSeconds;   
        this.Hour,temp     = parseInt(CDSeconds/3600);
        this.Minute,temp   = parseInt((CDSeconds%3600)/60);
        this.Second        = CDSeconds%60;
        this.mRefreshFunc = refreshFunc;
        this.mRefreshFuncName = refreshFuncName;
        this.mCdTimeEndFunc   = cdTimeEndFunc;
        TimerManager.addTimeCallBack(1,this.RefreshCDTime.bind(this));
    }   

    //刷新倒计时
    RefreshCDTime()
    {
        this.Second = this.Second - 1;

        //这里就不用总秒数计算时分秒，减少计算量
        if(this.Second < 0)
        {
            //秒数为0的时候 分钟数-1
            this.Second = 59;
            this.Minute = this.Minute - 1;
            if(this.Minute < 0)
            {
                //分钟数为0的时候，小时数-1
                this.Minute = 59;
                this.Hour = this.Hour - 1;
                if(this.Hour < 0)
                {
                    //小时数也为0的时候则表示倒计时结束
                    this.Hour   = 0;
                    this.Minute = 0;
                    this.Second = 0;
                }
            }
        }
         
        //刷新倒计时
       
        let cdtime = this.Second + this.Minute * 60;
        this.mRefreshFunc(cdtime,this.mRefreshFuncName);
        this.mCDSeconds = this.mCDSeconds - 1;

        //倒计时结束
        if(this.mCDSeconds <= 0)
        {
            if(null != this.mCdTimeEndFunc)
            {
                this.mCdTimeEndFunc(this.mRefreshFuncName);
            }
                
            Tool_CDTime_RemoveTimer(this.mRefreshFuncName);
        }
        else
        {
            TimerManager.addTimeCallBack(1,this.RefreshCDTime.bind(this));
        }
    }  

}

let mCDTimerList = {} //倒计时列表

//开启一个倒计时
//refreshFunc：刷新回调函数,函数必须有一个接收 类似于 1:05:09 的字符串的参数
//refreshFuncName：刷新回调函数名
//CDSeconds：倒计时总秒数
//cdTimeEndFunc：倒计时结束回调
export function Tool_CDTime_StartTimer(refreshFunc,refreshFuncName,CDSeconds,cdTimeEndFunc)
{
    if(null == refreshFuncName)
    {
        console.error('启动倒计时失败，回调函数为空');
        return;
    }

    //检测是否重复开启定时器
    if(null != mCDTimerList[refreshFuncName])
    {
        console.error('启动倒计时失败，上一个定时器没有结束，请先手动调用Tool_CDTime_RemoveTimer再开启定时器，回调函数名：'+refreshFuncName)
        return; 
    }

    let cdTimer = new CDTimer();
    mCDTimerList[refreshFuncName] = cdTimer;
    cdTimer.StartCDLHTime(refreshFunc,refreshFuncName,CDSeconds,cdTimeEndFunc);
}
       


//移除一个倒计时
function Tool_CDTime_RemoveTimer(refreshFuncName)
{
    mCDTimerList[refreshFuncName] = null;
}
    


let  mCDLerpTimerList = {}; //帧数倒计时列表
//开启一个帧数倒计时
export function Tool_CDTime_StartLerpTimer(refreshFunc,refreshFuncName,CDSeconds,cdTimeEndFunc)
{
    if(null == refreshFuncName)
    {
        console.error('启动倒计时失败，回调函数为空')
        return;
    }

    //检测是否重复开启定时器
    if(null != mCDLerpTimerList[refreshFuncName])
    {
        console.error('启动倒计时失败，上一个定时器没有结束，请先手动调用Tool_CDTime_RemoveTimer再开启定时器，回调函数名：'+refreshFuncName)
        return;
    }

    let cdTimer = new CDTimer();
    mCDLerpTimerList[refreshFuncName] = cdTimer;
    //timerMgr:AddLerpTimerEvent(refreshFuncName,CDSeconds,refreshFunc, cdTimeEndFunc,false)
    TimerManager.addLerpTimerEvent(CDSeconds,refreshFunc,cdTimeEndFunc,refreshFuncName);
}

//判断一个倒计时是否存在
export function Tool_CDTime_IsLerpTimerExist(refreshFuncName)
{
    if(null != mCDLerpTimerList[refreshFuncName])
        return true;
 
    return false;
}

//移除一个倒计时
export function Tool_CDTime_RemoveLerpTimer(refreshFuncName)
{
    mCDLerpTimerList[refreshFuncName] = null;
}
    

//endregion
