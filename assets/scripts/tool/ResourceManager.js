/*
作者（Author）:    jason

描述（Describe）: 预设资源预加载管理类
*/
/*jshint esversion: 6 */
import SafeLoader from "./SafeLoader";
let RoleBehaviour = require("RoleBehaviour");
let ResourceManager = (function(){

    let _prefabList = new Map();   //加载的预设链表
    let _prefabNames = [];
    let _finishedCallback = null;   //加载完成后的回掉
    let _needLoadCount = 0;
    let _totalNeedLoadCount  = 0;
    let _curLoadCount = 0;

    let finishedCall = function()
    {
        if( _curLoadCount === _totalNeedLoadCount)
        {
            if(_finishedCallback)
            {
                _finishedCallback();
            }
            // console.log(`############finishedCall totalNeedLoadCount = ${_totalNeedLoadCount},  curLoadCount = ${_curLoadCount}`);
            return true;
        }
        return false;
    };
    return {
        loadRole(roleNames,callback)
        {
            let objList = [];
            let namesUrl = [];
            if(roleNames.size <= 0)
            {
                if(callback)
                {
                    callback(objList);
                }
                return;
            }
            roleNames.forEach((key) => {
                namesUrl.push(`role/${key}`);
            });
            
            this.setLoadPrefabNames(namesUrl,function(){
                for(let name of roleNames)
                {
                    let res = this.getPrefab(name);
                    if(!res)
                    {
                        continue;
                    }
                    let go = cc.instantiate(res.obj);
                    go.addComponent("RoleBehaviour");
                    go.active = false;
                    objList.push(go);
                }
                if(callback)
                {
                    callback(objList);
                }
            }.bind(this));
            this.loadPrefab();
        },
        //设置需要加载的预设的名字
        setLoadPrefabNames:function(names,finishedCall)
        {
            _prefabNames = names;
            _finishedCallback = finishedCall;
            _needLoadCount = _prefabNames.length;
            _totalNeedLoadCount  = _prefabNames.length;
            _curLoadCount = 0;
            //console.log(`##############setLoadPrefabNames totalNeedLoadCount = ${_totalNeedLoadCount}`);
        },
        loadPrefab:function()
        {
            if(_needLoadCount > 0)
            {   
                --_needLoadCount;
                let name = _prefabNames[_needLoadCount];
                let index = name.lastIndexOf("/");
                if(index != -1)
                {
                    let prafabName = name.substr(index+1);

                    //console.log(`############loadPrefab  prafabName = ${prafabName}`);
                    //是否已经加载过
                    if(this.getPrefab(prafabName))
                    {
                        //console.log(`loadPrefab  prafabName = ${prafabName}  isLoaded`);
                        this.isLoaded();
                        return;
                    }
                }
                SafeLoader.safeLoadRes(name,(err, prefab)=>{
                    if(err)
                    {
                        //错误处理，即使有加载失败的也不要影响后面的流程
                        this.isLoaded();
                    }
                    else
                    {
                        this.onLoadFinished(prefab,name);
                    }
                    
                });
            }
            else
            {
                finishedCall();
            }
        },
        //存储加载成功的预设
        onLoadFinished:function(prefab,name)
        {   
            _prefabList.set(prefab.name,{obj:prefab,url:name});
            this.isLoaded();
        },
        //已经加载过
        isLoaded:function()
        {
            ++_curLoadCount;
            if( finishedCall() )
            {
                return;
            }
            this.loadPrefab();
        },
        
        //获取已经加载的预设
        getPrefab:function(name)
        {
            if(_prefabList.has(name))
            {
                return _prefabList.get(name);
            }
            return null;
        },
        //清除已经加载的预设
        clearPrefab:function(names)
        {
            for(let name of names)
            {
                if(_prefabList.has(name))
                {
                    cc.loader.releaseRes(_prefabList.get(name).url);
                    _prefabList.delete(name);
                }
            }
        }
    }
})();

export default ResourceManager;