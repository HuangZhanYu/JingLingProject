/*
作者（Author）:    skyHuang

描述（Describe）:
*/
/*jshint esversion: 6 */

let GOTool = (function () 
{

    return {
        //-- 判断对象是否存在
        //-- 参数1，一个GameObject对象或者一个组件对象
        //-- 返回值，bool
        IsDestroy(obj)
        {
            if(obj == null){
                return true;
            }
            return obj == null || obj == undefined;
        },

        //-- 设置一个GameObject的Transform的数值
        //-- obj GameObject
        //-- typ 操作数枚举，类型如下
        //-- public enum TransformOperateType
        //-- {
        //--    Position,       //世界坐标
        //--    Rotation,       //世界旋转
        //--    EulerAngles,    //世界欧拉角
        //--    LocalPosition,  //自身坐标
        //--    LocalRotation,  //自身旋转
        //--    LocalEulerAngles,//自身欧拉角
        //--    LocalScale,     //自身拉伸
        //--    RTPosition,     //RectTransform anchoredPosition3D
        //--    RTSizeDelta,    //RectTransform RTSizeDelta
        //--    RTPivot,        //RectTransform Pivot
        //-- }
        SetVector(obj, typ, vec)
        {
            // if(obj == null || typ == null){
            //     return;
            // }

            // vec.x = vec.x || 0;
            // vec.y = vec.y || 0;
            // vec.z = vec.z || 0;
            // vec.w = vec.w || 0;

            // GameObjectTool.SetVector(obj, typ, vec.x, vec.y, vec.z, vec.w);
        },

        //-- 获取一个GameObject的Transform的数值
        //-- obj GameObject
        //-- typ 操作数枚举，类型如下
        //-- public enum TransformOperateType
        //-- {
        //--    Position,       //世界坐标
        //--    Rotation,       //世界旋转
        //--    EulerAngles,    //世界欧拉角
        //--    LocalPosition,  //自身坐标
        //--    LocalRotation,  //自身旋转
        //--    LocalEulerAngles,//自身欧拉角
        //--    LocalScale,     //自身拉伸
        //--    RTPosition,     //RectTransform anchoredPosition3D
        //--    RTSizeDelta,    //RectTransform RTSizeDelta
        //--    RTPivot,        //RectTransform Pivot
        //-- }
        GetVector(obj, typ)
        {
            // let xx = 0;
            // let yy = 0;
            // let zz = 0;
            // let ww = 0;

            // if(obj == null || typ == null){
            //     return { x = xx, y = yy, z = zz, w = ww }
            // }

            // xx, yy, zz, ww = GameObjectTool.GetVector(obj, typ, xx, yy, zz, ww)
            // return { x = xx, y = yy, z = zz, w = ww }
        },

        //-- 设置父对象
        //-- obj 需要设置的GameObject
        //-- parent 父对象GameObject
        SetParent(obj, parent)
        {
            obj.parent = parent;
        },

        //-- 旋转一个物体
        Rotate(obj, x, y, z)
        {
            //GameObjectTool.Rotate(obj, x, y, z);
        },

        //--移动一个物体
        Translate(obj, x, y)
        {
            obj.setPosition(obj, x, y);
        },

        //--望向一个物体
        LookAt(obj, target)
        {
            //GameObjectTool.LookAt(obj, target);
        },

        //--获取从某个点到某个点的旋转角度
        FromToRotation(from, to)
        {
            // from.x = from.x || 0
            // from.y = from.y || 0
            // from.z = from.z || 0
            // to.x = to.x || 0
            // to.y = to.y || 0
            // to.z = to.z || 0

            // let xx = 0
            // let yy = 0
            // let zz = 0
            // xx, yy, zz = GameObjectTool.FromToRotation(from.x, from.y, from.z, to.x, to.y, to.z, xx, yy, zz)
            // return { x = xx, y = yy, z = zz }
        },

        //--获取物体的子对象，参数index是下标
        GetChild(obj, index)
        {
            return obj.children[index];
        },

        //--获取物体的子对象，参数path是路径
        FindChild(obj, path)
        {
            return GameObjectTool.FindChild(obj, path);
        },

        //--获取两点距离
        //--目前无视点的高度
        Distance(posA,posB)
        {
            if(posA != typeof(cc.Vec2) || posB != typeof(cc.Vec2))
            {
                return null;
            }
            return posA.divSelf(posB).mag();
        },


        //--获取Text实际宽高
        GetTextPreferre(obj)
        {
            let t = obj.getComponent(cc.Label);
            if(!t)
            {
                return;
            }
            return [obj.width,obj.height];
        },

        //--播放一个动画
        PlayAni(obj,name,speed,mode)
        {
            // speed = speed || FightBehaviorManager.GetSpeed();
            // mode = mode || UnityEngine.WrapMode.Once
            // GameObjectTool.PlayAni(obj,name,speed,mode)
        },

        //--是否在播放一个动画
        IsPlayingAni(obj,name)
        {
            //return GameObjectTool.IsPlayingAni(obj,name)
        },


        //--是否在播放一个动画
        GetAniTime(obj,name)
        {
            //return GameObjectTool.GetAniTime(obj,name)
        },


        //--停止一个动画
        StopAni(obj)
        {
            //GameObjectTool.StopAni(obj)
        },

        //--播放动画
        PlayMovie(movieName)
        {
            //GameObjectTool.PlayMovie(movieName)
        },

        //--退出游戏
        Quit()
        {
            Game.end();
        },

        //--创建一个自动销毁的光效
        SpawnEffectAutoDestroy(effectName, effect, duration, parent)
        {
            //GameObjectTool.SpawnEffectAutoDestroy(effectName, effect, duration, parent);
        },

        //--创建一个在世界坐标上的自动销毁的光效
        SpawnEffectAutoDestroyInWorldPlace(effectName, effect, duration, pos)
        {
            //GameObjectTool.SpawnEffectAutoDestroyInWorldPlace(effectName, effect, duration, pos.x, pos.y, pos.z)
        },

        //--创建一个战斗光效
        CreateFightEffect(effectName, effect)
        {
            //return GameObjectTool.CreateFightEffect(effectName, effect)
        },

        //--将字符串转换成Vector3
        SplitStringToVector(str)
        {
            resultX = 0;
            resultY = 0;
            let aa = str.split('|');
            if(aa.Length != 2)
            {
                return;
            }
            
            return [aa[0], aa[1]];
        },
        //--切换数字
        SplitTime(str)
        {
            if(!str)
            {
                return;
            }
            let aa = str.split('|');
            return aa;
        },

        //--望向对象
        FightRoleUILookAt(obj, target)
        {
            //GameObjectTool.FightRoleUILookAt(obj, target)
        },

        //--战斗飘字
        CreateHUDText(obj, content, typ, scaleTime, speed, duration)
        {
            //GameObjectTool.CreateHUDText(obj, content, typ, scaleTime, speed, duration)
        },

        //--释放战斗飘字
        ReleaseHUDText(obj)
        {
            //GameObjectTool.ReleaseHUDText(obj)
        },


        //--设置一个RectTransform的长度
        SetRectTransformLength(obj,length)
        {
            //GameObjectTool.SetRectTransformLength(obj,length)
        },

        //--获取一个RectTransform的长度
        GetRectTransformLength(obj)
        {
            //return GameObjectTool.GetRectTransformLength(obj)
        },

        //--重设镜头
        SetFollowCameraReset(obj)
        {
            //GameObjectTool.SetFollowCameraReset(obj)
        },

        //--设置跟随镜头的速度
        SetFollowCameraEnable(obj, enable)
        {
            //GameObjectTool.SetFollowCameraEnable(obj, enable)
        },

        //--设置跟随镜头的速度
        SetFollowCameraSpeed(obj, speed)
        {
            //GameObjectTool.SetFollowCameraSpeed(obj, speed)
        },

        //--设置跟随镜头的最小值
        SetFollowCameraMin(obj, min)
        {
            //GameObjectTool.SetFollowCameraMin(obj, min)
        },

        //--设置跟随镜头的最大值
        SetFollowCameraMax(obj, max)
        {
            //GameObjectTool.SetFollowCameraMax(obj, max)
        },

        //--设置跟随镜头目标
        SetFollowCameraTarget(obj, target)
        {
            //GameObjectTool.SetFollowCameraTarget(obj, target)
        },


        //--屏蔽跟随镜头的触摸
        SetFollowCameraShieldTouch(obj, shield)
        {
            //GameObjectTool.SetFollowCameraShieldTouch(obj, shield)
        },

        //--停止跟随镜头的触摸，和屏蔽不一样
        SetFollowCameraStopTouch(obj, stop)
        {
            //GameObjectTool.SetFollowCameraStopTouch(obj, stop)
        },

        //--实例化一个战斗模型
        InstantiateFightModel(obj)
        {
            //return GameObjectTool.InstantiateFightModel(obj)
        },

        //--判断战斗模型上是否存在光效
        CheckExistEffectInFightRole(obj,effectName)
        {
            //return GameObjectTool.CheckExistEffectInFightRole(obj,effectName)
        },


        GuideStartMove(obj, originX, originY, originZ, targetX, targetY, targetZ)
        {
            //GameObjectTool.GuideStartMove(obj, originX, originY, originZ, targetX, targetY, targetZ)
        },


        GuideStopMove(obj)
        {
            //GameObjectTool.GuideStopMove(obj)
        },

        //--设置一个带有粒子的GameObject的缩放
        ParticleScaler(obj, scale)
        {
            //GameObjectTool.ParticleScaler(obj,scale)
        },

        //--设置图片
        SetImage(obj,abName, sTextureName)
        {
            //GameObjectTool.SetImage(obj,abName, sTextureName)
        },

        //--添加一个面相镜头的脚本
        AddLookAt(obj,camera)
        {
            //GameObjectTool.AddLookAt(obj,camera)
        },
        //--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--战斗有关

        //-- 设置一个GameObject的Transform的数值
        //-- obj GameObject
        //-- typ 操作数枚举，类型如下
        //-- public enum TransformOperateType
        //-- {
        //--    Position,       //世界坐标
        //--    Rotation,       //世界旋转
        //--    EulerAngles,    //世界欧拉角
        //--    LocalPosition,  //自身坐标
        //--    LocalRotation,  //自身旋转
        //--    LocalEulerAngles,//自身欧拉角
        //--    LocalScale,     //自身拉伸
        //--    RTPosition,     //RectTransform anchoredPosition3D
        //--    RTSizeDelta,    //RectTransform RTSizeDelta
        //--    RTPivot,        //RectTransform Pivot
        //-- }
        SetFightRoleVector(id, path, typ, vec)
        {

            // vec.x = vec.x || 0
            // vec.y = vec.y || 0
            // vec.z = vec.z || 0
            // vec.w = vec.w || 0

            // FightGameObjectManager.getInstance():SetFightRoleVector(id, path, typ, vec.x, vec.y, vec.z, vec.w)
        },

        //-- 获取一个GameObject的Transform的数值
        //-- obj GameObject
        //-- typ 操作数枚举，类型如下
        //-- public enum TransformOperateType
        //-- {
        //--    Position,       //世界坐标
        //--    Rotation,       //世界旋转
        //--    EulerAngles,    //世界欧拉角
        //--    LocalPosition,  //自身坐标
        //--    LocalRotation,  //自身旋转
        //--    LocalEulerAngles,//自身欧拉角
        //--    LocalScale,     //自身拉伸
        //--    RTPosition,     //RectTransform anchoredPosition3D
        //--    RTSizeDelta,    //RectTransform RTSizeDelta
        //--    RTPivot,        //RectTransform Pivot
        //-- }
        GetFightRoleVector(id, path, typ)
        {
            // let xx = 0
            // let yy = 0
            // let zz = 0
            // let ww = 0

            // xx, yy, zz, ww = FightGameObjectManager.getInstance():GetFightRoleVector(obj, typ, xx, yy, zz, ww)
            // return { x = xx, y = yy, z = zz, w = ww }
        },

    };
})();

export default GOTool;