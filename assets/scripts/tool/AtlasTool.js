/*
作者（Author）:    skyHuang

描述（Describe）:
*/
/*jshint esversion: 6 */

import SafeLoader from "./SafeLoader";

let AtlasTool = (function(){

    let AtlasMap = new Map();
    return {

        setAtlasImage(image,atlasName,imageName)
        {
            if(!image)
            {
                return;
            }
            if(AtlasMap.has(atlasName))
            {
                image.spriteFrame = AtlasMap.get(atlasName).getSpriteFrame(imageName);
            }
            else
            {
                SafeLoader.safeLoadResWithType("image/"+atlasName, cc.SpriteAtlas, (err, spriteAtlas) => {
                    if(err)
                    {
                        return;
                    }
                    AtlasMap.set(atlasName,spriteAtlas);

                    image.spriteFrame =  spriteAtlas.getSpriteFrame(imageName);
                });
            }
        },

        getAtlasMap(){
            return QualityAtlas;
        }
    };
})();

export default AtlasTool;