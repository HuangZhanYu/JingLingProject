/*jshint esversion: 6 */

export default class StringTool
{
    //转化文本特殊符号【x】【x1】【x2】...【xn】
    //参数1：需替换的字符串
    //参数2：需替换的参数 （数组）
    static ZhuanHuan(str,param)
    {
        if(!param || param.length == 0)
        {
            return;
        }
        for(let i = 0; i < param.length; ++i)
        {
            if(i == 0)
            {
                str = str.replace(/【x】/,param[i]);
            }
            else
            {
                str = str.replace(`【x${i}】`,param[i]);
            }
        }

        return str;
    }
    //替换string中的字符或字符串成其他字符或字符串
    //参数1，源字符
    //参数2，需要替换的字符
    //参数3，替换成的字符
    //返回，一个新的字符串
    static Replace(str,old,newStr)
    {
        str = str.replace(`${old}`,newStr);
        return str;
    }
}