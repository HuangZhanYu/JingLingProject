/*
作者（Author）:    jason

描述（Describe）: 定时器管理类
*/
/*jshint esversion: 6 */
const TIME_DESTORY = {
    DESTORY : 0,
    ALWAY : 1,  //一直执行
    COUNT : 2,  //执行指定的次数
    FRAME: 3    //帧定时器
};
class TimeCallBack
{
    constructor(k, length, speed, callback, args, td, endFunc = null)
    {
        this.type = td;
        this.Key = k;
        this.callBack = callback;
        this.args = args;
        this._speed = speed;
        if (this._speed <= 0) this._speed = 1;
        this.length = (length / this._speed)*1000;
        this.beginTime = (new Date()).getTime();
        this.endTime = this.beginTime + this.length;
        this.count = 0;
        this.endfunc = endFunc;
        this.callTime = length;
    }
    
    get speed()
    {
        return this._speed;
    }
    set speed(value)
    {
        let old = this._speed;
        this._speed = value;
        let scale = this._speed / old;
        let surplus1 = this.surplusTime;
        let surplus2 = surplus1 / scale;
        this.endTime += (surplus2 - surplus1);
    }
    //剩余时间（毫秒）
    get surplusTime()
    {
        return (this.endTime - (new Date()).getTime());
    }

    update(dt)
    {
        if (this.type == TIME_DESTORY.FRAME)
        {
            this.callTime -= dt;
            this.callBack(this.callTime,this.args);
            if (this.callTime <= 0)
            {
                if(this.endfunc)
                {
                    this.endfunc(this.args);
                }
                return true;
            }
        }
        else if(this.surplusTime <= 0)
        {
            if (this.callBack != null)
            {
                ++this.count;
                this.callBack(this.args);
            }
           
            if(this.type == TIME_DESTORY.ALWAY)
            {
                this.beginTime = (new Date()).getTime();
                this.endTime = this.beginTime + this.length;
                return false;
            }
            else if (this.type == TIME_DESTORY.COUNT && this.count < this.args[0])
            {
                this.beginTime = (new Date()).getTime();
                this.endTime = this.beginTime + this.length;
                return false;
            }
            return true;
        }
        return false;
    }
}
let TimerManager = (function(){
    let _timeCallBack = new Map();	//计时器数组
    let _freeTimeKey = [];			//空闲的计时器ID
    let  _addCallBack = [];			//要添加的计时器数组 下一帧统一添加
    let _delCallBack = [];			//要删除的计时器数组 下一帧统一删除
    let _validKey = 0;
    let GetFreeKey = function()
    {
        let Key = -1;
        if (_freeTimeKey.length > 0)
        {
            Key = _freeTimeKey[0];
            _freeTimeKey.shift();
        }
        else
        {
            Key = (_validKey++);
        }
        return Key;
    };
    
    return{
        addTimeCallBack(length, callBack, args)
        {
            let Key = GetFreeKey();
            _addCallBack.push(new TimeCallBack(Key, length, 1, callBack, args,TIME_DESTORY.DESTORY));
            return Key;
        },
        addAlwayTimeCallBack(length, callBack, args)
        {
            let Key = GetFreeKey();
            _addCallBack.push(new TimeCallBack(Key, length, 1, callBack, args, TIME_DESTORY.ALWAY));
            return Key;
        },
        addCountTimeCallBack(length, callBack, args)
        {
            let Key = GetFreeKey();
            _addCallBack.push(new TimeCallBack(Key, length, 1, callBack, args, TIME_DESTORY.COUNT));
            return Key;
        },
        addLerpTimerEvent(length, callBack, endFunc, args)
        {
            let Key = GetFreeKey();
            _addCallBack.push(new TimeCallBack(Key, length, 1, callBack, args,TIME_DESTORY.FRAME,endFunc));
            return Key;
        },
        removeTimeCallBack(Key)
        {
            if (_timeCallBack.has(Key))
                _delCallBack.push(Key);
        },
        update(deltaTime)
        {
            for (let i = 0; i < _addCallBack.length; ++i)
                _timeCallBack.set(_addCallBack[i].Key, _addCallBack[i]);
            for (let i = 0; i < _delCallBack.length; ++i)
            {
                if (_timeCallBack.has(_delCallBack[i]))
                {
                    _timeCallBack.delete(_delCallBack[i]);
                    _freeTimeKey.push(_delCallBack[i]);
                }
            }
            _addCallBack.length = 0;
            _delCallBack.length = 0;
            let endKey = [];
            for (let [key, value] of _timeCallBack)
            {
                if (value.update(deltaTime))
                {
                    endKey.push(key);
                }
            }
            for (let i = 0; i < endKey.length; ++i)
            {
                this.removeTimeCallBack(endKey[i]);
            }
        }
    };
})();

export default TimerManager;
