/*
作者（Author）:    jason

描述（Describe）: 本地配置加载处理类
*/
/*jshint esversion: 6 */

import SafeLoader from "./SafeLoader";


let LocalConfigLoader = (function () 
{
    return {
        loadConfig: function (url) {
            //执行一个承诺，必须实现then方法
            return new Promise(function (resolve, reject) {
                SafeLoader.safeLoadRes(url, (err, data) => {
                    if (err) {
                        reject(err);    //拒绝
                    } else {
                        resolve(data.json);  //完成
                    }
                });
            });
        },
    };
})();

export default LocalConfigLoader;
