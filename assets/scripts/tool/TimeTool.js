/*
作者（Author）:    jason

描述（Describe）: 时间计算工具类
*/
/*jshint esversion: 6 */

import TimerManager from "./TimerManager";

let TimerKey = 0;
//通过秒数获取时间信息，如25分，1小时32分等,一天，一周，最小单位分钟
//参数1，秒数
export function TimeTool_GetTimeDetailBySecond(second)
{
    if(second == null)
		return '';
	
	// if(type(second) != 'number')
	// 	second = tonumber(second);
	
    //周数 604800
    let week = second / 604800;
    let weekRemainder = second % 604800;
    //天数 86400
    let day = weekRemainder / 86400;
    let dayRemainder = weekRemainder % 86400;
    //小时数 3600
    let hour = dayRemainder / 3600
    let hourRemainder = dayRemainder % 3600
    //分钟数 60
    let minute = hourRemainder / 60

    if(week >= 1)
        return TranslateTool.GetText(40617)
    else if( day >= 1)
        return TimeTool_GetIntPart(day)+TranslateTool.GetText(40618)
    else if( hour >= 1)
        return TimeTool_GetIntPart(hour)+TranslateTool.GetText(40619)
    else if( minute >= 1)
        return TimeTool_GetIntPart(minute)+TranslateTool.GetText(40620)
    else
        return "";
}
	

//获取整数部分
export function TimeTool_GetIntPart(x)
{
    if(x <= 0)
        return Math.ceil(x);
    if(Math.ceil(x) == x)
        x = Math.ceil(x);
    else
        x = Math.ceil(x) -1;

    return x;
}

export function TimeTool_TimeTransform(seconds)
{
    let time = "";
    let hour = Math.floor(seconds/3600);
    if(hour < 10)
        hour = "0"+hour
    
    let minute = Math.floor((seconds%3600)/60);
    if(minute < 10)
        minute = "0"+minute
    
    let second = seconds%60;
    if(second < 10)
        second = "0"+second
    
	
	time = hour+":"+minute+":"+second;
	return time;
}

//传入秒  转换为00：00格式
export function TimeTool_TimeTransformUpdate(seconds)
{
    let time = "";
	let Hour = Math.floor(seconds/3600);
    let Minute = Math.floor((seconds%3600)/60);
    let Second = seconds%60;
	
    if(Hour == 0 && Minute != 0)
    {
        time =  TimeTool_ChangeOneNumber(Minute)+":"+TimeTool_ChangeOneNumber(Second);
        return time;
    }
    else if( Hour == 0 && Minute == 0)
    {
        time =  "00:"+TimeTool_ChangeOneNumber(Second);
        return time;
    }

    Minute = Hour * 60 + Minute

	time = TimeTool_ChangeOneNumber(Minute)+":"+TimeTool_ChangeOneNumber(Second); // TimeTool_ChangeOneNumber(Hour)+":"+
	return time;
}

export function TimeTool_ChangeOneNumber(number)
{
    if(number < 10)
        return "0"+number;
    else if( number >= 10)
        return number;
}

//传入秒  转为xxHxxM   XX小时XX分钟的形式
export function TimeTool_TransformActivityTime(seconds)
{
    let time = "";
	let Hour = Math.floor(seconds/3600);
    let Minute = Math.floor((seconds%3600)/60);
    let Second = seconds%60;

    if(Hour == 0 && Minute != 0)
    {
        time =  "00H"+TimeTool_ChangeOneNumber(Minute)+"M"
        return time;
    }
    else if( Hour == 0 && Minute == 0)
    {
        time =  "00M"+TimeTool_ChangeOneNumber(Second)+"S"
        return time;
    }

    time = TimeTool_ChangeOneNumber(Hour)+"H"+TimeTool_ChangeOneNumber(Minute)+"M"; 
	return time;
}
    

//传入秒  转为xx:xx   XX小时XX分钟的形式
export function TimeTool_TransformTime(seconds)
{
    let time = "";
	let Hour = Math.floor(seconds/3600);
    let Minute = Math.floor((seconds%3600)/60);
    let Second = seconds%60;

    if(Hour == 0 && Minute != 0)
    {
        time =  "00:"+TimeTool_ChangeOneNumber(Minute)
        return time;
    }
    else if( Hour == 0 && Minute == 0)
    {
        time =  "00:"+TimeTool_ChangeOneNumber(Second)
        return time;
    }

    time = TimeTool_ChangeOneNumber(Hour)+":"+TimeTool_ChangeOneNumber(Minute); 
	return time;
}
    



//传入秒  转为xx天xx小时xx分xx秒的形式
export function TimeTool_TransformStringTime(seconds)
{
    let time = "";
	let Hour = Math.floor((seconds%86400)/3600);
    let Minute = Math.floor(((seconds%86400)%3600)/60);
    let Second = seconds%60;

    time = Day+"天"+Hour+"小时"+Minute+"分钟"+Second+"秒";
	return time;
}

//传入秒  xx:xx:xx 小时：分钟：秒   用来获取倒计时形式
export function TimeTool_TransformDaoJiShiTime(seconds)
{
    let time = "";
    let Day = Math.floor(seconds/86400)
	let Hour = Math.floor((seconds%86400)/3600);
    let Minute = Math.floor(((seconds%86400)%3600)/60);
    let Second = seconds%60;

    time = TimeTool_ChangeOneNumber(Hour)+":"+TimeTool_ChangeOneNumber(Minute)+":"+TimeTool_ChangeOneNumber(Second);
	return time;
}

//////////////////////////////////////数字滚动工具////////////////////////////////////////////-
//滚动前准备工作  参数1 滚动数字(数组) 参数2 注册滚动时的回调函数 参数3 滚动结束的回调函数
export function TimeTool_StartNumberScroll(updateFunc,endFunc)
{
    updateFunction = updateFunc;
    endFunction = endFunc;
    //timerMgr:AddLerpTimerEvent('TimeTool_NumberScrollUpdate', 2, TimeTool_NumberScrollUpdate,TimeTool_StopNumberScroll, false)
    TimerKey = TimerManager.addTimeCallBack(2,TimeTool_StopNumberScroll);
}

//0.02秒调用一次 共调用150次
// function TimeTool_NumberScrollUpdate()
// {
//     if(updateFunction != null)
//         updateFunction();
// }

export function TimeTool_StopNumberScroll()
{
    if(updateFunction != null)
        updateFunction();
    TimerManager.removeTimeCallBack(TimerKey);
    if(endFunction != null)
        endFunction()
}

export function TimeTool_ClearTimer()
{
    TimerManager.removeTimeCallBack(TimerKey);
}
    

