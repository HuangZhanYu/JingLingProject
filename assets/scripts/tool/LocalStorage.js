/*
作者（Author）:    jason

描述（Describe）: 本地存储管理类
*/
/*jshint esversion: 6 */

let LocalStorage = (function(){

    return{
        setString(key, value) {
            cc.sys.localStorage.setItem(key, value);
        },
        getString(key) {
            return cc.sys.localStorage.getItem(key);
        },
    };
})();

export default LocalStorage;
