/*
作者（Author）:    jason

描述（Describe）: 对象池
*/
/*jshint esversion: 6 */

export default class
{
    constructor(poolName, poolObjectPrefab, initCount, maxSize, pool){
        this.poolName = poolName;
		this.poolSize = initCount;
        this.maxSize = maxSize;
        this.poolRoot = pool;
        this.poolObjectPrefab = poolObjectPrefab;
        this.availableObjStack = [];
        for(let index = 0; index < initCount; index++) {
			this.AddObjectToPool(this.NewObjectInstance());
		}

    }
    AddObjectToPool(go)
    {
        go.active = false;
        this.availableObjStack.push(go);
        //this.poolRoot.addChild(go);
        //go.parent = this.poolRoot;
        go.setPosition(0,0);
    }
    //实例化一个对象
    NewObjectInstance()
    {
        return cc.instantiate(this.poolObjectPrefab);
    }
    NextAvailableObject()
    {
        var go = null;
        if(this.availableObjStack.length > 0)
        {
            go = this.availableObjStack.shift();
        }
        else
        {
            this.Extension();
            go = this.availableObjStack.shift();
        }
        return go;
    }
    //扩展池，默认扩大一倍
    Extension()
    {
        for (let index = 0; index < this.poolSize; index++)
        {
            this.AddObjectToPool(this.NewObjectInstance());
        }
    }
    ClearStack()
    {
        for(let obj of this.availableObjStack)
        {
            obj.destroy();
        }
        this.availableObjStack.length = 0;
    }
    //回收对象到池链表
    ReturnObjectToPool(poolName, po) 
    {
        if (this.poolName === poolName)
        {
            this.AddObjectToPool(po);
        } 
    }
}
