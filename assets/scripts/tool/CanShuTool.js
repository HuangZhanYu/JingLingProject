/*
作者（Author）:    skyHuang

描述（Describe）:
*/
/*jshint esversion: 6 */




let CanShuTool = (function () 
{
    return {
        //-- 获取参数数据
        //-- 参数1，FunctionOpenTool.FunctionEnum枚举
        //-- 返回 所有参数
        GetCanShuData(functionEnum)
        {
            let funcData =  LoadConfig.getConfigData(ConfigName.CanShu,functionEnum);
            if(funcData == null)
            {
                console.error("参数表ID异常，ID"+functionEnum);
                return null;
            }
            return funcData;
        }
    };
})();

CanShuTool.CanShuEnum = {
    ZuiDaJinHuaDengJi           : 1, //--最大进化等级
    ZuiDaShangZhenJingLing      : 2, //--最大上阵精灵数
    ZuiDaShangZhenZhuangBei     : 3, //--最大上阵装备数
    BuyAbilityCost              : 4, //--主动技购买消耗钻石
	AbilityConvertResource      : 5, //--主动技购转化资源
	UnUse3                      : 6, //--6可填充
	UnUse4                      : 7, //--7可填充
	UnUse5                      : 8, //--8可填充
    ChuangJianJunTuan           : 9, //--第一层复活奖励的勋章值  
    MinReviveRound              : 10,//--至少到X层才可以复活
    BuySpeed                    : 11,//--购买加速，1，金额，2，加速倍数，3加速时间（秒）
    OneBoxGetMedal              : 12,//--获得一个宝箱奖励的勋章值
    OffLineRoundPerMinute       : 13,//--离线每分钟闯关数
    OffLineTopRound             : 14,//--离线最高关卡:历史闯的最高关卡*X(X值配置在表)
    DungeonInitChallengeCount   : 15,//--地下城每天初始挑战次数
    DungeonRecoverTime          : 16,//--地下城每隔x分钟恢复1次挑战次数
    DungeonTimeBetween          : 17,//--地下城可以恢复的时间X~Y
    CatchPetFreeFreshCount      : 18,//--捉宠:捉宠免费刷新次数最大值
    CatchPetRecoverTime         : 19,//--捉宠:捉宠每x分钟恢复一次免费刷新次数
	CatchPetNormalSearchCost    : 20,//--捉宠:捉宠普通搜寻消耗的体力数量
    CatchPetHighSearchCost      : 21,//--捉宠:捉宠高级搜寻消耗的钻石数量
    CatchPetTimeBetween         : 22,//--捉宠:捉宠可以恢复的时间X~Y
    PlayerMaxTiLi               : 23,//--角色体力最大值
    RecoverTiLiPerMinute        : 24,//--每x分钟恢复角色1点体力
    DiamondResetCost            : 25,//--钻石复活需要消耗的钻石
    MedalDropParam              : 26,//--关卡掉落：纹章掉落系数。需要程序除以10000
    Unknown3                    : 27,//--不用的
    RoundLineUpTime             : 28,//--关卡上阵间隔时间s毫秒。
    RoundGetTreasureRate        : 29,//--关卡宝箱：每关出现宝箱的概率(万分比)
    BaoXiangJiangChiID          : 30,//--关卡宝箱:宝箱奖池id
    BaoXiangJinBiXiShu          : 31,//--关卡宝箱:金币掉落系数(万分比)公式：金币:最大任务每秒金币*系数
    EquipMaxEvoLevel            : 32,//--装备最大进化等级
    ZhuangBeiFenJie             : 33,//--装备分解消耗钻石数量
    ZhuangBeiChongZhi           : 34,//--装备重置消耗钻石数量
    MeiShuChangJingWeiZhi       : 35,//--美术场景底部位置，美术场景顶部位置，所需数量
    Unknown1                    : 36,//--不用的
    Unknown2                    : 37,//--不用的
    Unknown4                    : 38,//--不用的
    TuJianJingLingShuLiang      : 39,//--图鉴：精灵图鉴最大数量
    TuJianEquipShuLiang         : 40,//--图鉴：装备图鉴最大数量
    MaxShopSaoHuoKaFreshCount   : 41,//--商店：扫货卡每日刷新上限
    ShopSaoHuoKaResumeHour      : 42,//--商店:商店免费次数恢复的时间X~Y
    ShopSaoHuoKaResumeTimeGap   : 43,//--商店：商店免费次数恢复间隔时间(单位:分钟)
    MaxShopFreeFreshCount       : 44,//--商店：每天免费刷新次数
    ShopJinBiCanShu             : 45,//--商店：商店出售的金币:参数值1*最大已开启任务每秒获得金币值
    ReviveJinBiAddtition        : 46,//--重置：每次重置金币加成百分比。（填50表示50%)
    MaxReviveJinBiAddtition     : 47,//--重置：重置最多增加金币加成百分比。（填50表示50%)
    ReviveWenZhangAddtition     : 48,//--重置：每次重置纹章加成百分比。（填50表示50%)
    MaxReviveWenZhangAddtition  : 49,//--重置：重置最多增加纹章加成百分比。（填50表示50%)
    CatchPetFirstGuideSceneID   : 50,//--新手引导：引导捉宠第1次刷新的场景ID
    CatchPetSecondGuideSceneID  : 51,//--新手引导：引导捉宠第2次刷新的场景ID(引导修改暂时去掉)
    CatchPetFirstGuideJingLingID: 52,//--新手引导：引导捉宠第2次刷新出的非稀有精灵ID(引导修改暂时去掉)
    AutoClearMailDay            : 53,//--邮件系统：过了x天邮件自动删除。
    BornJingLing                : 54,//--出生系统：出生需要选择的三个精灵(小火龙、妙蛙种子、杰尼龟)
    MaxCatchPetFreshCardFreshCount  : 55,//--捉宠系统：刷新卡每日刷新上限
    CalBossKillTimeGuanQiaCount : 56,//--每多少关计算一次关卡BOSS击杀次数
    JingLingJinBiLevelUpCost    : 57,//--参数1：宠物金币等级消耗公比(万分率)。参数2：橙宠初始值。参数3：紫宠初始值。参数4，白绿蓝宠初始值不用了，现在用（ShiBing_TianFuDengJi）
    JingLingXunZhangLevelUpCost : 58,//--参数1：宠物勋章等级消耗公比去q(万分率)。参数2：勋章消耗初始值
    JinBiLevelATKDEFHPAdd       : 59,//--金币等级提升攻、防、血的百分比(万分率)
    QiangHuaLevelATKDEFHPAdd    : 60,//--强化提升等级时候提升攻、防、血的百分比(万分率)
    ActiveSkillGuanQiaHurt      : 61,//--主动技能按层数伤害：伤害:1.05^(主线关卡最大记录层数-参数1)*参数2+参数3。。(万分率)
    ActiveSkillLevelHurt        : 62,//--主动技能按等级伤害：伤害:1.05^(角色等级*参数1)*参数2+参数3。。(万分率)
    FirstPassTimeExp            : 63,//--首次通关经验公式：第x关经验:x*参数1+参数2
    CalFightCount               : 64,//--【攻击*参数1+生命*参数2+（物防+法防）*参数3】*（1+暴击几率）*（1+暴伤）*（攻速/600000）
    LevelUpPopPanelTime         : 65,//--主角升级：主角升级弹窗停留时间(单位:s)不能少于0.5
    SpecailSkillTimeGap         : 66,//--特殊技能：受击技能触发冷却时间。
    CatchPetMinSceneTime        : 67,//--捉宠刷新保底：前X次刷新场景有保底机制
    CatchPetMinOriginTime       : 68,//--捉宠刷新保底：每x次刷新必定出橙场景
    CatchPetMinAdvancedSearchTime  : 69,//--捉宠搜寻保底：高级搜寻每x次必定出橙
    CatchPetMinNormalSearchTime : 70,//--捉宠搜寻保底：普通搜寻每x次必定出橙
    CatchPetRandomScene         : 71,//--捉宠刷新保底：如果随机时候没有，则随机一个配置的ID场景
    HuanZhenXiaoHao             : 72,//--换阵需要消耗钻石
    HuanZhuangXiaoHao           : 73,//--更换装备需要消耗钻石
    LiXianZuiDaJiaCheng         : 74,//--离线行军最大加成
    TrainArenaMaxCount          : 75,//--训练竞技场挑战次数
    TrainArenaTime              : 76,//--训练竞技场挑战次数恢复时间（单位：分钟）
    PKTrainPaiMing              : 77,//--训练竞技场对战记录-排名变化页签保存记录数量
    PKTrainFangShou             : 78,//--训练竞技场对战记录-防守战报页签保存记录数量
    PKTrainZhanDouTime          : 79,//--训练竞技场战斗时间（单位：分钟）
    TrainArenaXHZS              : 80,//--训练竞技场购买挑战次数消耗钻石
    FightTowerChallengeCount    : 81,//--战斗塔每天初始挑战次数
    FightTowerRecoverTime       : 82,//--战斗塔每隔x分钟恢复1次挑战次数
    FightTowerTime              : 83,//--战斗塔可以恢复的时间X~Y
    ZhanDouTaDanChangTime       : 84,//--战斗塔单场战斗时间（单位：分钟）
    FightTowerOpenTime          : 85,//--战斗塔开放时间参数（参数1为开服第几天开放；参数2为开服后几点开放；参数3为持续时间，单位：小时；参数4为下次开放间隔，单位：小时）
    FightTowerMaxRound          : 86,//--战斗塔最高层数
    ArenaRewardTime             : 87,//--训练竞技场排名奖励发放时间（晚上10点）参数1：小时，参数2：分钟
    FightTowerLoopCount         : 88,//--战斗塔最大循环周期
    DFArenaTZCount              : 89,//--巅峰竞技场挑战次数
    DFArenaHFTimer              : 90,//--巅峰竞技场挑战次数恢复时间（单位：秒）
    DFArenaPMBH                 : 91,//--巅峰竞技场对战记录-排名变化页签保存记录数量
    DFArenaFS                   : 92,//--巅峰竞技场对战记录-防守战报页签保存记录数量
    DFArenaZDTimer              : 93,//--巅峰竞技场战斗时间（单位：秒）
    DFArenaXHZS                 : 94,//--巅峰竞技场购买挑战次数消耗钻石
    DFArenaLJTimer              : 95,//--巅峰竞技场排名累计奖励时间间隔（单位：小时）
    DFArenaMaxTimer             : 96,//--巅峰竞技场最大累计奖励时间（单位：小时）
    DFArenaSDXH                 : 97,//--巅峰竞技场商店刷新消耗竞技点数
    MaxFriend                   : 98,//--初始最大好友数量
    MaxHeiMingDan               : 99,//--黑名单列表最大数量
    MaxShenQing                 : 100,//--好友申请列表最大数量
    MaxFriendChat               : 101,//--好友聊天最大信息数量
    FettersAutoID               : 102,//--羁绊系统给的默认ID
    BuyGoldParams               : 103,//--购买可获得金币:当前所有已经开启的任务（不管是否已经开启自动任务）每秒金币*参数1。购买消耗钻石为参数2
    ShuaXinKaGouMai             : 104,//--刷新卡购买钻石价格
    YinLiaoGM                   : 105,//--饮料购买钻石价格
    JingLingQiuGouMai           : 106,//--精灵球购买钻石价格
    ChaoJiQiuGouMai             : 107,//--超级球购买钻石价格
    GaoJiQiuGouMai              : 108,//--高级球购买钻石价格
    DaShiQiuGouMai              : 109,//--大师球购买钻石价格
    VipLiBaoCur                 : 110,//--玩家可看到的VIP礼包当前VIP等级+参数1
    ShuaXinRongYuCiShuMax       : 111,//--荣誉商店：每天刷新次数上限
    FuFeiMinHuanZhen            : 112,//--付费换阵最低精灵训练等级
    GengHuanEquipQH             : 113,//--付费更换装备最低强化等级
    BuyEquipJinHua              : 114,//--付费更换装备最低进化等级
	ResetMinTime                : 115,//--灵魂休息最低奖励时间（单位/秒）
	ResetMaxTime                : 116,//--灵魂休息最大奖励时间（单位/秒）
	ResetCDTime                 : 117,//--灵魂休息冷却时间（单位/秒）
	ResetCalParam               : 118,//--灵魂休息分获得纹章公式参数
	ResetDoubleCost             : 119,//--消耗X钻石获得双倍灵魂休息奖励
	ResetGetMinTime             : 120,//--计算每分钟获得纹章最小重置时间间隔（单位/秒）
	ResetReduceTime             : 121,//--灵魂休息缩短时间（单位/秒）
    DaoGuanTiaoZhanBoss         : 122,//--每人每天可挑战道馆BOSS次数
    CreateDaoGuanZuanShi        : 123,//--创建道馆消耗X钻石       
	GuanQiaTimeReward 		    : 124,//--间隔一定时间关卡概率掉落礼盒的相关参数：参数1，间隔时间/秒。参数2，离线累积超过多少时间后不再触发/秒。参数3，掉落礼盒的奖池ID。参数4，命中掉落礼盒奖池的概率/万分率
    DaoGuanXinBingZengSongMax   : 125,//--道馆新兵营最多展示X条赠送记录
    DaoGuanDiaoXiangMaxLv       : 126,//--道馆雕像最大等级
    DaoGuanDiaoXiangTeXu        : 127,//--雕像特需开启需要的道馆等级。参数1为特需1开启等级，参数2为特需2开启等级，参数3为特需3开启等级
    DaoGuanZhiYuanDui           : 128,//--支援队开启的最高通关层数。参数1为志愿队1开启条件，参数2为支援队2开启条件，参数3为支援队3开启条件，参数4为支援队4开启条件
    TeXuBeiShu                  : 129,//--特需精灵可获得5倍激活进度
    FeiFaKaID                   : 130,//--参数1表示大于该值的卡牌ID为非法卡牌
    DaoGuanPositionLimit        : 131,//--道馆职位上限。参数1为副馆长上限，参数2为精英上限
    ShiYongLimit                : 132,//--一键使用最大使用数量
    BornJingLingWithPet         : 133,//--不选择宠物的方式创建角色的，赠送的宠物
    DaoGuanDaKa                 : 134,//--钻石打卡消耗
    ReviveSpeedUp               : 135,//--每次重置之后给予一段时间加速效果。参数1表示几倍加速，参数2表示加速时间（单位/秒）
    LevelUpJinBiAdd             : 136,//--参数1表示每次升级提升的金币加成（为百分比），参数2表示金币加成上限
    LevelUpWenZhangAdd          : 137,//--参数1表示每次升级提升的纹章加成（为百分比），参数2表示纹章加成上限
    OffLineMaxTime              : 138,//--离线能获取奖励的最大时长
    SeniorCostCard              : 139,//--高级刷新消耗刷新卡数量
    SeniorCostDiamond           : 140,//--高级刷新消耗钻石
    GiveSpeedPet                : 141,//--每次获得精灵给与5倍加速。参数1为精灵ID，参数2为几倍加速，参数3位加速时长（单位/秒）
    OffLineParam                : 142,//--离线时长
    NotAdditionRangeSkillType   : 143,//--不进行技能射程，攻击范围加成的技能表现类型
    TaskAutoParams              : 144,//--自动任务相关参数（参数值为主角等级条件）
    ReviveParams                : 145,//--参数1：填1表示工作系统在重置关卡的时候不会被重置。填0表示会被重置。参数2，玩家当前金币是否保留，填1保留，填0不保留
    FastReviveCost              : 146,//--快速重置消耗资源。参数1~3分别表示表ID、资源ID、消耗数量
    EquipBagMaxCount            : 147,//--参数1表示装备背包可容纳的最大装备数量
    RechargeGetPoint            :148,//-- 参数1表示每次充值可以获得的积分参数。获得的积分:充值RMB金额*参数1
	LandgraveWH 				:149,//-- 参数1 道馆战长 参数2 道馆战宽
	LandgravePVERvTime 			:150,//-- 道馆战pve复活时间
	LandgravePVPRvTime 			:151,//-- 道馆战pvp复活时间 
	LandgraveRobRvTime 			:152,//-- 道馆战掠夺复活时间
	LandgraveGiveupCD 			:153,//-- 放弃领地CD时间
    LandgraveOutputMax			:154,//-- 领地产出上限
    RevivePromptParams          :155,//-- 重置提示参数
    ZhanliParam                 :156,//-- 战力参数
}
export default CanShuTool;
