/*
作者（Author）:    jason

描述（Describe）: 对象池管理类
*/
/*jshint esversion: 6 */
import GameObjectPool from "../tool/GameObjectPool";
let PoolManager = (function(){

    let mGameObjectPools = new Map();
    return {
        //创建一个池
        CreatePool:function( poolName, initSize, maxSize, prefab)
        {
            var pool = new GameObjectPool(poolName, prefab, initSize, maxSize, this.node);
            mGameObjectPools.set(poolName,pool);
            return pool;
        },
        //返回poolName池里面的一个对象
        Get:function(poolName)
        {
            let result = null;
            if(mGameObjectPools.has(poolName))
            {
                let pool = mGameObjectPools.get(poolName);
                result = pool.NextAvailableObject();
            }
            return result;
        },
         //回收池对象
        Release:function(poolName, go)
        {
            if(mGameObjectPools.has(poolName))
            {
                let pool = mGameObjectPools.get(poolName);
                pool.ReturnObjectToPool(poolName, go);
            }
        },
        ClearPoolByName(poolName)
        {
            if(mGameObjectPools.has(poolName))
            {
                let pool = mGameObjectPools.get(poolName);
                pool.ClearStack();
                mGameObjectPools.delete(poolName);
            }
        },
        ClearPool:function()
        {
            for(let [key,value] of mGameObjectPools)
            {
                value.ClearStack();
            }
            mGameObjectPools.clear();
        }
    };
})();

export default PoolManager;