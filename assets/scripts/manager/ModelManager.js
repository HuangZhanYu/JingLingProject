/*
作者（Author）:    jason

描述（Describe）: 游戏内数据管理类
*/
/*jshint esversion: 6 */

import PlayerInfoModel from "../model/PlayerInfoModel";
import { ModelNames } from "../common/ModelNames";
import GameInfoModel from "../model/GameInfoModel";

let ModelManager = (function () {
    var _modelMap = {};
    var _modelConsturctFun = {};
    var registerModelContruct = function (modelName, constructFun) {
        _modelConsturctFun[modelName] = constructFun;
    };

    registerModelContruct(ModelNames.PlayerInfoModel,PlayerInfoModel);
    registerModelContruct(ModelNames.GameModel,GameInfoModel);
    return {
        getModel(modelName) {
            if (_modelMap[modelName]) {

                return _modelMap[modelName];
            }
            else {
                if (_modelConsturctFun[modelName]) {

                    _modelMap[modelName] = new _modelConsturctFun[modelName];
                    return _modelMap[modelName];
                }
                else {

                }
            }
        },

        destroyModel(modelName) {
            if (_modelMap[modelName]) {
                _modelMap[modelName].destroy();
                delete _modelMap[modelName];
                console.log("ModelManager: destroyModel:" + _modelMap[modelName]);
            }
        }

    };
})();

export default ModelManager;
