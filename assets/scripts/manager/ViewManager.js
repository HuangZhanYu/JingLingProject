/*
作者（Author）:    jason

描述（Describe）: 界面弹窗管理类
*/
/*jshint esversion: 6 */
import SafeLoader from '../tool/SafeLoader';
import Util from '../tool/Util';
import { PanelResName } from '../common/PanelResName';
let UIBase = require('UIBase');

//界面显示位置
export const PanelPos = 
{
    PP_Bottom : 0,
    PP_Midle : 1,
    PP_Top : 2,
};
let ViewManager = (function(){
    let _curAlertDialog = null;
    let _popupList = [];
    let _beforPoupCallBack = []; //弹窗之前的回调函数链
    let _loadingUI = null;
    let getPanelByUrl = function (url)
    {
        for (let i = 0; i < _popupList.length; ++i) {
            let popup = _popupList[i];
            if (popup.url === url) {
                return popup;
            }
        }
        return null;
    };
    
    let getPanelIndexByNode = function (node)
    {
        for (let i = 0; i < _popupList.length; ++i) {
            let popup = _popupList[i];
            if (popup.node === node) {
                return i;
            }
        }
        return -1;
    };
    let implementBeforPanelCallBack = function ()
    {
        let cb;
        for (let i = 0; i < _beforPoupCallBack.length; ++i) {
            cb = _beforPoupCallBack[i];
            if (cb) {
                cb();
            }
        }
        _beforPoupCallBack.length = 0;
    };
    //实例化一个弹窗
    let instantiatePanel = function ({error,url,res,afterCreate,comp}, panelPos)
    {
        let popup = getPanelByUrl(url);
        // 如果没有 url 就不创建
        if (!popup) {
            
            return null;
        }

        let panel = cc.instantiate(res);
        popup.node = panel;

        // 插入切面 并 初始化
        let popupComp = panel.getComponent(UIBase);
        popupComp.init && popupComp.init();
        
        let canvas = Util.getRootCanvas(null,panelPos);

        if (!canvas) {
            let index = getPanelIndexByNode(panel);
            if (index !== -1) {
                _popupList.splice(index, 1);
            }
        }
        canvas && canvas.addChild(panel);

        afterCreate && afterCreate(popupComp);
        
        return popupComp;
    };
    //释放所有资源
    let releaseAllDeps = function (deps) {
        deps.forEach((item) => {
            cc.loader.release(item);
        });
    };
   
    return {
         // 这里在添加回调函数会在 addPanel 到时候调用一次
        addBeforPanelCallBack: function (callback)
        {
            _beforPoupCallBack.push(callback);
        },
        /**
         * 弹出窗口方法
         * @param urlOrPrefabOrNode 可以是Prefab，或Prefab的资源路径，或Node节点
         * @param afterCreate Prefab等创建成功了立马插入一个切面，通常对新创建的实例做一些数据上的操作
         */
        addPanel: function (urlOrPrefabOrNode, afterCreate, panelPos = PanelPos.PP_Bottom,param = null)
        {
            implementBeforPanelCallBack();
            let prefabCreated = getPanelByUrl(urlOrPrefabOrNode);
            //这个prefab缓存在内存中，被隐藏了
            if ( prefabCreated && prefabCreated.comp ) {
                afterCreate && afterCreate(prefabCreated.comp);

                prefabCreated.comp.node.active = true;
                prefabCreated.comp.showPanel(param);
                return prefabCreated.comp;
            }
            let popupInfo = {
                error: null,
                url: urlOrPrefabOrNode,
                res: null,
                afterCreate: afterCreate,
                comp: null
            };
    
            _popupList.push(popupInfo);

            if (cc.js.isString(urlOrPrefabOrNode))
            {
                let res = cc.loader.getRes(urlOrPrefabOrNode, cc.Prefab);
                if (res && res._uuid.length > 0)
                {
                    popupInfo.res = res;
                    popupInfo.comp = instantiatePanel(popupInfo, panelPos);
                    popupInfo.comp.showPanel(param);
                    return popupInfo.comp;
                }

                SafeLoader.safeLoadRes(urlOrPrefabOrNode, (err, res) =>
                {
                    if (err) {
                        let index = getPanelIndexByNode(popupInfo.url);
                        if (index !== -1) {
                            _popupList.splice(index, 1);
                        }
                        ViewManager.alert({mainText: '您的网络不稳定，请稍后再试',singleText:"确定"});
                        return;
                    }
                    popupInfo.error = err;
                    popupInfo.res = res;
                    popupInfo.comp = instantiatePanel(popupInfo, panelPos);
                    popupInfo.comp.showPanel(param);
                    return popupInfo.comp;
                });
            }
            else
            {
                popupInfo.res = urlOrPrefabOrNode;
                popupInfo.comp = instantiatePanel(popupInfo, panelPos);
                popupInfo.comp.showPanel(param);
                return popupInfo.comp;
            }
        },
        //获取一个已经加载的界面
        getPanel(panelName)
        {
            let prefabCreated = getPanelByUrl(panelName);
            //这个prefab缓存在内存中，被隐藏了
            if ( prefabCreated && prefabCreated.comp ) {
                return prefabCreated.comp;
            }
            return null;
        },
         /**
         * 移除弹窗
         */
        removePanel(node,isDestroy = true) {
            if (_curAlertDialog)
            {
                _curAlertDialog = null;
            }
            if (node)
            {
                // 移除
                let index = getPanelIndexByNode(node);
                if (index !== -1) {
                    _popupList.splice(index, 1);
                    node.active = false;
                }
                if(isDestroy)
                {
                    node.destroy();
                }
            }
        },
        /**
         * 移除指定url的弹窗
         */
        removePanelByUrl:function(url)
        {
            let popup = getPanelByUrl(url);
            if (popup)
            {
                this.removePanel(popup.node);
            }
        },
         /**
         * 暂时清除列表，后面可能要移除控件了！！！！
         */
        removeAll: function() {
            for (let i = 0; i < this._popupList.length; ++i) {
                let popupInfo = this._popupList[i];
                if (popupInfo.node)
                {
                    popupInfo.node.destroy();
                }
            }
            this._popupList.length = 0;
        },
        //获取窗口依赖的资源
        getDependsRecursively: function (url) {
            let popup = getPanelByUrl(url);
            if (popup && popup.url) {
                let deps = cc.loader.getDependsRecursively(popup.url);
                return deps;
            }
            return null;
        },
        //删除窗口
        releasePanel: function (url) {
            let deps = this.getDependsRecursively(url);
            if (deps) {
                releaseAllDeps(deps);
            } else {
                console.warn('releasePanel error!');
            }
        },
        // 通用用对话框格式
        // let alertInfo = {
        //    mainText: '提示信息',
        //    singleText: '单个按钮',
        //    confirmText: '确定按钮',
        //    cancelText: '取消按钮',
        //    confirmCallback: ()=> {}, 确定回调
        //    cancelCallback: ()=> {},  取消按钮回调
        //    singleCallBack: ()=> {},  单个按钮回调
        //};
        alert: function (alertInfo)
        {
            ViewManager.addPanel(PanelResName.AlertPanel,(panel)=>{},PanelPos.PP_Top,alertInfo);
        },
        /**
        * 通用显示Loading菊花，全局只显示一个
        * let {
            text = 'Loading', 提示信息
            showDelay = 4, 延迟显示时间
            closeTime = 60,关闭时间
            isText = true, 是否显示提示信息
            isDian = true,是否显示后面的点动画
            maskOpacity = 178, mask透明度
            CountDownTime = 0, 显示倒计时的时间
            canClose = true, //未达到关闭时间，不允许关闭
            timeCallBack = ()=>{}倒计时结束回调
        } = value;
        */
        showLoading(value) {
            if (this._loadingUI && this._loadingUI.node.active) {
                return;
            }
            if (!this._loadingUI) {

                let canvas = cc.director.getScene();
                this._loadingUI = Util.searchComp(canvas,'LoadingPanel','LoadingPanel');
                if (!this._loadingUI) {
                    return;
                }
                this._loadingUI.init();
            }
            this._loadingUI.showLoading(value);
        },
        /**
         * 隐藏菊花
         * closed 强制关闭
         */
        hideLoading(closed = false) {
            if ((!this._loadingUI || !this._loadingUI.isCanClose) && !closed) {
                return;
            }
            this._loadingUI.hideLoading(closed);
        },
    };
})();

export default ViewManager;