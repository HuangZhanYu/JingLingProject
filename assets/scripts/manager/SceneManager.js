/*
作者（Author）:    jason

描述（Describe）:
*/
/*jshint esversion: 6 */
import Util from "../tool/Util";
import GameObjectTool from "../tool/GameObjectTool";

let SceneManager = (function(){
    let SceneName = ['CaoCong','SenLin','ShaMo','XueDi','HuoShan','XiaGu','HaiBian','CaoCong-ZhuaChong'];
    let SceneLogic = null;
    let Camera1 = null;
    let FollowCamera = null;
    return{
        //游戏内场景的名字
        Login_Scene_Name:'login', //登陆场景
        Game_Scene_Name: 'game', //游戏场景
        SceneType : {
            CaoCong : 1,//草丛
            SenLin  : 2,//森林
            ShaMo   : 3,//沙漠
            XueDi   : 4,//雪地
            HuoShan : 5,//火山
            XiaGu   : 6,//峡谷
            HaiBian : 7,//海边
            CaoCongZhuaChong : 8,//草地抓宠
        },
        SceneOrigin:null,
        init()
        {
            let scene = cc.director.getScene();
            let canvas = scene.getChildByName('Canvas');
            this.SceneOrigin = Util.searchNode(canvas,'Origin');
            SceneLogic = Util.searchNode(this.SceneOrigin,'OriginLogic');
            Camera1 = Util.searchNode(SceneLogic,'Camera');
            FollowCamera = SceneLogic.getComponent('FollowCamera');
            SceneLogic.active = false;
        },
        //获取当前场景的名字
        getcurSceneName:function()
        {
            return cc.director.getScene().name;
        },
        //加载场景资源
        loadSceneResource: function(name, done)
        {
            //TsdkFacade.getLogger().info(`loadSceneResource setGlobalCacheMessage true`);
            //场景需要时间加载，期间缓存消息
            //MessageCenter.getInstance().setGlobalCacheMessage(true);

            // 加载场景开始计时
            const __start = Date.now();
            let preSceneName = cc.director.getScene().name;
            console.log(`loadSceneResource preSceneName = ${preSceneName}`);
            let curSceneName = name;
            function loadSceneCB()
            {
                done && done();
                // 加载场景结束计时
                const __end = Date.now();
                // 打印场景加载的时间，单位：毫秒
                console.log(`loadSceneResource  curSceneName = ${curSceneName}  耗时：${parseInt(__end - __start)} 毫秒`);
                
                //不再缓存消息
                //MessageCenter.getInstance().setGlobalCacheMessage(false);

                //MessageCenter.getInstance().sendMessage(MessageIDS.MsgID_ChangeSceneEnd,[preSceneName,curSceneName]);
            }

            cc.director.loadScene(name,loadSceneCB.bind(this));
            //UIManager.getInstance().ClearAllWindow();
        },
        getRandomScene()
        {
            return SceneName[0];
        },
        SetLogicActive(flag)
        {
            SceneLogic.active = flag;
        },
        //设置跟随目标
        SetFollowCameraTarget(target)
        {
            if(!FollowCamera)
            {
                return;
            }
            FollowCamera.SetTarGet(target);
        },
        //设置跟随镜头屏蔽触摸
        SetFollowCameraShieldTouch(shield)
        {
            if(!FollowCamera)
            {
                return;
            }
            FollowCamera.ShieldTouch(shield);
        },
        //设置跟随镜头停止触摸
        SetFollowCameraStopTouch(stop)
        {
            if(!FollowCamera)
            {
                return;
            }
            FollowCamera.StopTouch(stop);
        },
        //Hold住镜头
        HoldCamera(time)
        {
            if(!FollowCamera)
            {
                return;
            }
            FollowCamera.Hold(time);
        },
        //重设镜头
        SetFollowCameraReset()
        {
            if(!FollowCamera)
            {
                return;
            }
            FollowCamera.Reset();
        },
        //设置相机跟随速度
        SetFollowCameraSpeed(speed)
        {
            if(!FollowCamera)
            {
                return;
            }
            FollowCamera.SetSpeed(speed);
        },
        //获取当前镜头的坐标
        GetLogicPos()
        {
            if(!SceneLogic)
            {
                return;
            }
            return SceneLogic.position;
        }
    };
})();

export default SceneManager;