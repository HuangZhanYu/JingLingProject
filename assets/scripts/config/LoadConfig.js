/*
作者（Author）:    jason

描述（Describe）: 配置表加载类
*/

import LocalConfigLoader from "../tool/LocalConfigLoader";
import MessageCenter from "../tool/MessageCenter";
import { MessageID } from "../common/MessageID";



let LoadConfig = (function(){
    let _cfgData = null;
    let _dataMap = new Map();
    let _needLoadCount = 0;         
    let _totalNeedLoadCount  = 0;
    let _curLoadCount = 0;
    let _configNames = [];

    let loadConfig = function()
    {
        if(_needLoadCount > 0)
        {   
            --_needLoadCount;
            let name = _configNames[_needLoadCount];

            let url = `config/${name}`;
            LocalConfigLoader.loadConfig(url).then( (data) => {
                _cfgData = data;
               
                console.log(`配置文件名字：${name}`)
                onLoadFinished(name);
                
            }).catch ( (err) => {
                //Log.Error(err);
            });
        }
        
    };
    let onLoadFinished = function(configName)
    {
        _dataMap.set(configName,_cfgData);
        ++_curLoadCount;

        if(_curLoadCount < _totalNeedLoadCount)
        {
            MessageCenter.sendMessage(MessageID.MsgID_LoadConfigProcess,_curLoadCount/_totalNeedLoadCount);
            loadConfig();
        }
        else
        {
            console.log(`配置全部加载完毕`);
            MessageCenter.sendMessage(MessageID.MsgID_LoadConfigFinished);
        }
    };
    return {
        loadAllConfigs:function()
        {
            _configNames = Object.keys(ConfigName);
            _needLoadCount = _configNames.length;
            _totalNeedLoadCount = _needLoadCount;
            _curLoadCount = 0;
            
            loadConfig();
        },
        getConfigData(configName,id)
        {
            if(_dataMap.has(configName))
            {
                let data = _dataMap.get(configName);
                let conData = data[id];
                if(conData)
                {
                    conData["ID"] = id;
                }
                return conData;
            }
            else
            {
                console.log(`没找到配置：${configName}`);
            }
            return null;
        },
        getConfig(configName)
        {
            if(_dataMap.has(configName))
            {
                let data = _dataMap.get(configName);
                return data;
            }
            else
            {
                console.log(`没找到配置：${configName}`);
            }
            return null;
        }
    }
})();

window.LoadConfig = LoadConfig;