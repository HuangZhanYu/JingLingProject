/*
作者（Author）:    jason

描述（Describe）: 玩家信息数据管理类
*/
/*jshint esversion: 6 */

export default class PlayerInfoModel
{
    constructor()
    {
        this.playerName = '';
        this.roleType = 1;
        this.PlayerAttrs = [];  //玩家属性
        this.mIsPlayerAttrInit = false;
        this.StoneBaseAttrs = []; 
        this.ObjID = 0;
        this.StrAttr = '';
        this.IntAttr = [];
        this.TimeAttr = [];

    }
    OnLogout()
    {
        this.mIsPlayerAttrInit = false;
        this.PlayerAttrs.length = 0;
    }
}

