/*
作者（Author）.    skyHuang

描述（Describe）.
*/
/*jshint esversion: 6 */

import BigNumber from "../tool/BigNumber";
import FightTool from "./fightBehavior/FightTool";
import { FightAttr, FightSpecialAttr } from "./fight/FightDef";
import CanShuTool from "../tool/CanShuTool";
import ExponentTool from "../tool/ExponentTool";

////--卡牌加成信息
class CardAddition 
{
    constructor()
    {
        this.Values = [];
        for(let i = PlayerAttrPart.Card_Fight_Type.ATK; i < PlayerAttrPart.Card_Fight_Type.Max - 1; i++)
        {
            this.Values[i] = BigNumber.zero;
        }
    }
    Set(card_Fight_Type, value)
    {
        this.Values[card_Fight_Type] = value;
    }

    Get(card_Fight_Type)
    {
        return this.Values[card_Fight_Type];
    }

    AddCardAddition(cardAddition)
    {
        if(cardAddition == null)
        {
            return;
        }
    
        for(let i = PlayerAttrPart.Card_Fight_Type.ATK; i < PlayerAttrPart.Card_Fight_Type.Max - 1; i++)
        {
            this.Values[i] = BigNumber.add(this.Values[i], cardAddition.Values[i]);
        }
    }
}

//--卡牌信息
export default class Card
{
    constructor(msgCard)
    {
        this.ObjID                      = null; //--卡牌唯一ID 
        this.CardID                     = null; //--卡牌ID
        this.Level                      = null; //--金币等级
        this.EvolutionLevel             = null; //--强化等级
        this.MaxLevel                   = null; //--最高金币等级
        this.LevelUpRealCost            = null; //--升级真实消耗
        this.LevelUpBaseCost            = null; //--升级未加成前消耗
        this.FightAttr                  = []; //--战斗属性
        this.FightSpecialAttr           = []; //--战斗特殊属性
        this.LevelAdditionAttr          = []; //--金币，强化等级总属性
        this.QiangHuaLevelAdditionAttr  = []; //--金币，强化等级总属性
        this.FightAdditionAttr          = null; //--精灵天赋，装备，套装总属性
        this.JingLingAdditionAttr       = null; //--自身天赋
        this.ZhuangBeiAdditionAttr      = null; //--自身天赋
        this.BeforeAddAttr              = []; //--战斗加成前属性
        this.OrginFightAttr             = null; //--阵容卡牌的初始属性，保留大数字单位
        this.WenZhangFightAttr          = null; //--纹章加成战斗属性
        this.WenZhangFightSpecialAttr   = null; //--纹章加成战斗特殊属性
        if(!msgCard)
        {
            return;
        }
        this.SetCardInfo(msgCard);
    }
    //--通过卡牌ID新建一张卡
    static NewByID(cardID, level, qiangHuaLevel, kind)
    {
        let cardObj = new Card(null);
        
        cardObj.ObjID              = FightTool.GetGUID();           //--卡牌唯一ID 
        cardObj.CardID             = cardID;             //--卡牌ID
        cardObj.Level              = level || 1;              //--金币等级
        cardObj.EvolutionLevel     = qiangHuaLevel || 0;     //--强化等级
        //--初始化
        for(let i = FightAttr.ATK; i < FightAttr.Max - 1; i++)
        {
            cardObj.FightAttr[i] = BigNumber.zero;
            cardObj.BeforeAddAttr[i] = BigNumber.zero;
            cardObj.LevelAdditionAttr[i] = BigNumber.tenThousand;
            cardObj.QiangHuaLevelAdditionAttr[i] = BigNumber.tenThousand;
        }

        //--初始化特殊属性
        for(let i = 0; i < FightSpecialAttr.Max - 1; i++)
        {
            cardObj.FightSpecialAttr[i] = BigNumber.zero;
        }

        cardObj.FightAdditionAttr  = new CardAddition(); //--精灵天赋，装备，套装总属性

        cardObj.CalInitAttr();
        cardObj.CalLevelAttr(0);

        if(kind != null)
        {
            let cardAddition = PlayerAttrPart.GetOtherAttr(PlayerAttrPart.Source.Total, kind);
            cardObj.CalTeamAttr(cardAddition);
        }

        cardObj.CalResult();
        return cardObj;
    }

//--复制一张使用强化等级属性的卡
    CloneWenZhangCard()
    {
        let card = this.CloneCard();
        card.FightAttr = card.WenZhangFightAttr;
        card.FightSpecialAttr = card.WenZhangFightSpecialAttr;
        return card;
    }

//--根据一张卡的属性复制一张卡，同时拷贝钥石等基础属性加成
    Clone(level, qiangHuaLevel, kind)
    {
        let card = this.CloneCard();

        card.Level              = level || 1;              //--金币等级
        card.EvolutionLevel     = qiangHuaLevel || 0;     //--强化等级
        card.FightAttr          = {}; //--战斗属性
        card.FightSpecialAttr   = {}; //--战斗特殊属性
        card.BeforeAddAttr          = {}; //--战斗属性
        card.LevelAdditionAttr  = {}; //--金币等级总属性
        card.QiangHuaLevelAdditionAttr = {}; //--强化等级总属性
        //--初始化
        for(let i = FightAttr.ATK; i < FightAttr.Max - 1;i++ )
        {
            card.FightAttr[i] = BigNumber.zero;
            card.BeforeAddAttr[i] = BigNumber.zero;
            card.LevelAdditionAttr[i] = BigNumber.tenThousand;
            card.QiangHuaLevelAdditionAttr[i] = BigNumber.tenThousand;
        }

        //--初始化特殊属性
        for(let i = FightSpecialAttr.Ice_Hurt; i < FightSpecialAttr.Max - 1; i++ )
        {
            card.FightSpecialAttr[i] = BigNumber.zero;
        }

        card.FightAdditionAttr  = new CardAddition(); //--精灵天赋，装备，套装总属性

        card.CalInitAttr();
        card.CalLevelAttr(0);

        if(kind != null)
        {
            let cardAddition = PlayerAttrPart.GetOtherAttr(PlayerAttrPart.Source.Total, kind);
            card.CalTeamAttr(cardAddition);
        }
        
        if(card.BaseAttr != null)
        {
            card.CalBaseAttr(card.BaseAttr);
        }

        card.CalResult();
        return card;
    }

//--获取特殊属性
    GetFightSpecialAttr()
    {
        let result = [];
        
        for(let i = 0; i < FightSpecialAttr.Max-1;i++ )
        {
            if (this.FightSpecialAttr[i] == null || this.FightSpecialAttr[i]=="undefine") 
            {
                console.error(">>>>下标越界了") 
                break;  
            }
            let attr = BigNumber.getIntValue(this.FightSpecialAttr[i]);
            result.push(attr);
        }

        return result;
    }

//--初始战斗属性
    CalInitAttr()
    {
        this.FightAttr          = []; //--战斗属性
        this.FightSpecialAttr   = []; //--战斗特殊属性
        let conf_ShiBing_Data = LoadConfig.getConfigData(ConfigName.ShiBing,this.CardID);
        if(conf_ShiBing_Data == null)
        {
            for(let i = 0; i < FightAttr.Max; i++)
            {
                this.FightAttr[i] = "0";
            }
            console.error("CalInitAttr卡牌不存在, Card ."+this.CardID);
            return;
        }
        
        //--获取战斗属性
        this.FightAttr[FightAttr.ATK] = BigNumber.create(conf_ShiBing_Data.ATK);
        this.FightAttr[FightAttr.HP] = BigNumber.create(conf_ShiBing_Data.HP);
        this.FightAttr[FightAttr.DEF] = BigNumber.create(conf_ShiBing_Data.DEF);
        this.FightAttr[FightAttr.MDEF] = BigNumber.create(conf_ShiBing_Data.MDEF);
        this.FightAttr[FightAttr.ASPD] = BigNumber.create(conf_ShiBing_Data.ASPD);
        this.FightAttr[FightAttr.Speed] = BigNumber.create(conf_ShiBing_Data.Speed);
        this.FightAttr[FightAttr.HitRate] = BigNumber.create(conf_ShiBing_Data.HitRate);
        this.FightAttr[FightAttr.DodgeRate] = BigNumber.create(conf_ShiBing_Data.DodgeRate);
        this.FightAttr[FightAttr.CRI] = BigNumber.create(conf_ShiBing_Data.CRI);
        this.FightAttr[FightAttr.CritDamage] = BigNumber.create(conf_ShiBing_Data.CritDamage);
        this.FightAttr[FightAttr.Respawn] = BigNumber.create(conf_ShiBing_Data.Respawn);
        this.FightAttr[FightAttr.MaxHP] = BigNumber.create(conf_ShiBing_Data.HP);

        let conf_Skill1_Data = LoadConfig.getConfigData(ConfigName.JiNeng,conf_ShiBing_Data.NormalSkillID);
        let conf_Skill2_Data = LoadConfig.getConfigData(ConfigName.JiNeng,conf_ShiBing_Data.BigSkillID);
        if(conf_Skill1_Data == null )
        {
            this.FightAttr[FightAttr.Range] = BigNumber.zero;
            this.FightAttr[FightAttr.AttackRange] = BigNumber.zero;
        }
        else
        {
            this.FightAttr[FightAttr.Range] = BigNumber.create(conf_Skill1_Data.Range);
            this.FightAttr[FightAttr.AttackRange] = BigNumber.create(conf_Skill1_Data.AttackRange);
        }
        
        if(conf_Skill2_Data == null)
        {
            this.FightAttr[FightAttr.SkillRange] = BigNumber.zero;
            this.FightAttr[FightAttr.SkillAttackRange] = BigNumber.zero;
        }
        else
        {
            this.FightAttr[FightAttr.SkillRange] = BigNumber.create(conf_Skill2_Data.Range);
            this.FightAttr[FightAttr.SkillAttackRange] = BigNumber.create(conf_Skill2_Data.AttackRange);
        }

        //--初始化特殊属性
        for(let i = 0; i < FightSpecialAttr.Max - 1; i++ )
        {
            if(i == FightSpecialAttr.RestoreHP)
            {
                this.FightSpecialAttr[i] = BigNumber.create(conf_ShiBing_Data.RestoreHP);
            }
            else if(i == FightSpecialAttr.EasyHurt)
            {
                this.FightSpecialAttr[i] = BigNumber.create(conf_ShiBing_Data.EasyHurt);
            }
            else
            {
                let add = BigNumber.create(conf_ShiBing_Data.HurtAdd[i]);
                let reduce = BigNumber.create(conf_ShiBing_Data.HurtReduce[i]);

                this.FightSpecialAttr[i] = BigNumber.sub(add, reduce);
            }
        }
    }

//--计算等级属性
//--天赋提升的等级
    CalLevelAttr(addLevel)
    {
        addLevel = addLevel || 0;

        let exponent = 1.05;
        let conf_CanShu_Data = CanShuTool.GetCanShuData(CanShuTool.CanShuEnum.JinBiLevelATKDEFHPAdd);
        if(conf_CanShu_Data != null)
        {
            exponent = conf_CanShu_Data.Param[0] / 10000;
        }
        //--攻击 血量 物防 法防
        let additionLevel = ExponentTool.GetResult(exponent, this.Level - 1 + addLevel);
        this.LevelAdditionAttr[FightAttr.ATK] = additionLevel;
        this.LevelAdditionAttr[FightAttr.HP] = additionLevel;
        this.LevelAdditionAttr[FightAttr.DEF] = additionLevel;
        this.LevelAdditionAttr[FightAttr.MDEF] = additionLevel;

        //--攻击 血量 物防 法防
        let additionEvolutionLevel = ExponentTool.GetResult(exponent, this.EvolutionLevel);
        this.QiangHuaLevelAdditionAttr[FightAttr.ATK] = additionEvolutionLevel;
        this.QiangHuaLevelAdditionAttr[FightAttr.HP] = additionEvolutionLevel;
        this.QiangHuaLevelAdditionAttr[FightAttr.DEF] = additionEvolutionLevel;
        this.QiangHuaLevelAdditionAttr[FightAttr.MDEF] = additionEvolutionLevel;

    }

//--计算队伍属性
    CalTeamAttr(cardAddition)
    {
        this.FightAdditionAttr.AddCardAddition(cardAddition);
    }

//--计算基础属性加成
    CalBaseAttr(cardAddition)
    {
        this.FightAdditionAttr.AddCardAddition(cardAddition);
    }

//-- 计算最终战斗力
    CalResult()
    {
        let totalAddition = new CardAddition();
        totalAddition.AddCardAddition(this.FightAdditionAttr);

        for(let i = FightAttr.ATK; i < FightAttr.Max - 1;i++ )
        {
            //--这三类不用加成
            if(i != FightAttr.MaxHP && i != FightAttr.Vision && i != FightAttr.Anger)
            {
                let [add, addRate, reduceRate, coef, baseAdd] = PlayerAttrPart.ChangeFightAttrToCard_Fight_Type(i);
                let addBig = totalAddition.Get(add);
                let baseAddBig = totalAddition.Get(baseAdd);
                let addRateBig = totalAddition.Get(addRate);
                let reduceRateBig = totalAddition.Get(reduceRate);
                let coefBig = totalAddition.Get(coef);
                let rateBig = BigNumber.sub(addRateBig, reduceRateBig);

                //-- 双防御
                if(i == FightAttr.DEF || i == FightAttr.MDEF)
                {
                    let doubleDefAddBig = totalAddition.Get(PlayerAttrPart.Card_Fight_Type.DoubleDef);
                    let doubleDefBaseAddBig = totalAddition.Get(PlayerAttrPart.Card_Fight_Type.DoubleDef_Base);
                    let doubleDefAddRateBig = totalAddition.Get(PlayerAttrPart.Card_Fight_Type.DoubleDef_Add);
                    let doubleDefReduceRateBig = totalAddition.Get(PlayerAttrPart.Card_Fight_Type.DoubleDef_Reduce);
                    let doubleDefCoefBig = totalAddition.Get(PlayerAttrPart.Card_Fight_Type.DoubleDef_Coef);
                    let doubleDefRate = BigNumber.sub(doubleDefAddRateBig, doubleDefReduceRateBig);

                    addBig = BigNumber.add(addBig, doubleDefAddBig);
                    baseAddBig = BigNumber.add(baseAddBig, doubleDefBaseAddBig);
                    rateBig = BigNumber.add(rateBig, doubleDefRate);
                    coefBig = BigNumber.add(coefBig, doubleDefCoefBig);
                }

                //--攻防血
                if(i == FightAttr.DEF || i == FightAttr.MDEF || 
                i == FightAttr.ATK || i == FightAttr.HP)
                {
                    let adhAddBig = totalAddition.Get(PlayerAttrPart.Card_Fight_Type.ADH);
                    let adhBaseAddBig = totalAddition.Get(PlayerAttrPart.Card_Fight_Type.ADH_Base);
                    let adhAddRateBig = totalAddition.Get(PlayerAttrPart.Card_Fight_Type.ADH_Add);
                    let adhReduceRateBig = totalAddition.Get(PlayerAttrPart.Card_Fight_Type.ADH_Reduce);
                    let adhCoefBig = totalAddition.Get(PlayerAttrPart.Card_Fight_Type.ADH_Coef);
                    let adhRate = BigNumber.sub(adhAddRateBig, adhReduceRateBig);

                    addBig = BigNumber.add(addBig, adhAddBig);
                    baseAddBig = BigNumber.add(baseAddBig, adhBaseAddBig);
                    rateBig = BigNumber.add(rateBig, adhRate);
                    coefBig = BigNumber.add(coefBig, adhCoefBig);

                }
                //-- 固定值
                this.FightAttr[i] = BigNumber.add(this.FightAttr[i], addBig);
                //--固定值加成
                let rate = BigNumber.add(baseAddBig, BigNumber.tenThousand);
                let result = BigNumber.mul(this.FightAttr[i], rate);
                this.FightAttr[i] = BigNumber.div(result, BigNumber.tenThousand);
                //-- 计算总数
                if(i == FightAttr.ATK || i == FightAttr.HP || i == FightAttr.DEF || i == FightAttr.MDEF)
                {
                
                    //-- 金币等级，this.LevelAdditionAttr的值需要除以10000
                    if(! BigNumber.equalTo(this.LevelAdditionAttr[i], BigNumber.tenThousand))
                    {
                        this.FightAttr[i] = BigNumber.mul(this.FightAttr[i], this.LevelAdditionAttr[i]);
                        this.FightAttr[i] = BigNumber.div(this.FightAttr[i], BigNumber.tenThousand);
                    }
                    //-- 强化等级，this.QiangHuaLevelAdditionAttr的值需要除以10000
                    if(! BigNumber.equalTo(this.QiangHuaLevelAdditionAttr[i], BigNumber.tenThousand))
                    {
                        this.FightAttr[i] = BigNumber.mul(this.FightAttr[i], this.QiangHuaLevelAdditionAttr[i]);
                        this.FightAttr[i] = BigNumber.div(this.FightAttr[i], BigNumber.tenThousand);
                    }

                }
            
                //-- 加成值
                let rateAddition = BigNumber.mul(this.FightAttr[i], rateBig);
                rateAddition = BigNumber.div(rateAddition, BigNumber.tenThousand);
                this.FightAttr[i] = BigNumber.add(this.FightAttr[i], rateAddition);
                //-- 系数
                let coefAddition = BigNumber.mul(this.FightAttr[i], coefBig);
                coefAddition = BigNumber.div(coefAddition, BigNumber.tenThousand);
                this.FightAttr[i] = BigNumber.add(this.FightAttr[i], coefAddition);

                //--省略其他
                if(i == FightAttr.ATK || i == FightAttr.HP || i == FightAttr.DEF || i == FightAttr.MDEF)
                {
                    let bigValue = BigNumber.getValue(this.FightAttr[i]);
                    this.FightAttr[i] = BigNumber.create(bigValue);
                }
                //--设置最大血量
                if(i == FightAttr.HP)
                {
                    this.FightAttr[FightAttr.MaxHP] = this.FightAttr[i];
                }

            }
        }

    //--计算特殊战斗属性
    for(let i = 0; i < FightSpecialAttr.Max - 1; i++ )
    {
        if(i == FightSpecialAttr.RestoreHP || i == FightSpecialAttr.EasyHurt)
        {
            let [addRate , _] = PlayerAttrPart.ChangeFightSpecialAttrToCard_Fight_Type(i);
            let addRateBig = totalAddition.Get(addRate);
            //--暂时没有判断最大值;
            this.FightSpecialAttr[i] = BigNumber.add(this.FightSpecialAttr[i], addRateBig);
        }
        else
        {
            let [addRate , reduceRate] = PlayerAttrPart.ChangeFightSpecialAttrToCard_Fight_Type(i);
            let addRateBig = totalAddition.Get(addRate);
            let reduceRateBig = totalAddition.Get(reduceRate);
            let addition = BigNumber.sub(addRateBig, reduceRateBig);
            //--暂时没有判断最大值
            this.FightSpecialAttr[i] = BigNumber.add(this.FightSpecialAttr[i], addition);
        }
    }

}

//-- 设置没有复活时间，给不会复活的怪物使用
    SetNotRespawn()
    {
        this.FightAttr[FightAttr.Respawn] = BigNumber.zero;
    }

//--初始化卡牌对象
    SetCardInfo(msgCard)
    {
        if(msgCard == null)
        {
            return;
        }

        this.ObjID                     = msgCard.ObjID;                    //--卡牌唯一ID 
        this.CardID                    = msgCard.CardID;                   //--卡牌ID
        this.Level                     = msgCard.Level;                    //--金币等级
        this.EvolutionLevel            = msgCard.EvolutionLevel;           //--强化等级
        this.MaxLevel                  = msgCard.MaxLevel;                 //--强化等级
        this.LevelUpRealCost           = msgCard.LevelUpRealCost;          //--强化等级
        this.LevelUpBaseCost           = msgCard.LevelUpBaseCost;          //--强化等级
        this.LevelAdditionAttr         = msgCard.LevelAdditionAttr;        //--金币等级总属性
        this.QiangHuaLevelAdditionAttr = msgCard.QiangHuaLevelAdditionAttr; //--等级总属性
        this.FightAdditionAttr         = msgCard.FightAdditionAttr;        //--精灵天赋，装备，套装总属性
        this.JingLingAdditionAttr      = msgCard.JingLingAdditionAttr;     //--精灵汇总
        this.ZhuangBeiAdditionAttr     = msgCard.ZhuangBeiAdditionAttr;    //--装备汇总
        this.OrginFightAttr            = msgCard.FightAttr;

        this.FightAttr = [];    //--战斗属性，要从123.33a的形式转成完整的大数字
        for(let i = 0; i < msgCard.FightAttr.length; i++ )
        {
            this.FightAttr[i] = BigNumber.create(msgCard.FightAttr[i]);
        }
        
        this.FightSpecialAttr = [];//--战斗属性特殊下发
        let length = msgCard.FightSpecialAttr.length
        for(let i = 0; i < length; i++ )
        {
            this.FightSpecialAttr[i] = BigNumber.create(msgCard.FightSpecialAttr[i]);
        }
        
        this.WenZhangFightAttr = [];    //--战斗属性，要从123.33a的形式转成完整的大数字
        for(let i = 0; i < msgCard.WenZhangFightAttr.length; i++  )
        {
            this.WenZhangFightAttr[i] = BigNumber.create(msgCard.WenZhangFightAttr[i]);
        }
        
        this.WenZhangFightSpecialAttr = [];//--战斗属性特殊下发
        for(let i = 0; i < msgCard.WenZhangFightSpecialAttr.length; i++ )
        {
            this.WenZhangFightSpecialAttr[i] = BigNumber.create(msgCard.WenZhangFightSpecialAttr[i]);
        }

        this.BeforeAddAttr = [];    //--战斗加成之前的属性
        for(let i = 0; i < msgCard.BeforeAddAttr.length; i++ )
        {
            this.BeforeAddAttr[i] = BigNumber.create(msgCard.BeforeAddAttr[i]);
        }
    }

//--获取战力公式
    GetZhanli(param)
    {
        //--log(this.OrginFightAttr[FightAttr.ATK]+" "+param[0])
        let atk = BigNumber.cal([this.FightAttr[FightAttr.ATK], BigNumber.create(param[0]), "10000"], [BigNumberCalType.Mul, BigNumberCalType.Div]);
        let hp  = BigNumber.cal([this.FightAttr[FightAttr.HP], BigNumber.create(param[1]), "10000"], [BigNumberCalType.Mul, BigNumberCalType.Div]);
        let def = BigNumber.cal([BigNumber.add(this.FightAttr[FightAttr.DEF], this.FightAttr[FightAttr.MDEF]), BigNumber.create(param[2]), "10000"],[BigNumberCalType.Mul, BigNumberCalType.Div]);
        //--let cri = BigNumber.cal([this.FightAttr[FightAttr.CRI], "10000", "10000"],[BigNumberCalType.Add, BigNumberCalType.Div]);
        let dmg = BigNumber.cal([this.FightAttr[FightAttr.CritDamage], "10000", "10000"],[BigNumberCalType.Add, BigNumberCalType.Div]);
        //--let asp = BigNumber.div(this.FightAttr[FightAttr.ASPD], "600000")
        let val = BigNumber.cal([atk, hp, def],[BigNumberCalType.Add, BigNumberCalType.Add]);
        let li = BigNumber.add(val, this.FightAttr[FightAttr.CRI]);
        let zhanli = BigNumber.mul(li, dmg);
        //--log("id."+this.CardID+" atk."+this.FightAttr[FightAttr.ATK]+" hp."+this.FightAttr[FightAttr.HP]+" zhanli."+zhanli)
        return zhanli;
    }

//--参数1 FightAttr
    GetFightAttr(fightAttr)
    {
        return this.OrginFightAttr[fightAttr];
    }

//--获取某战斗数值的具体信息
    GetFightAttrDetail(fightAttr)
    {
    
        //--未加成的值
        let total = this.FightAttr[fightAttr];
        let noAdd = this.BeforeAddAttr[fightAttr];
        let jingLing = BigNumber.zero;
        let zhuangBei = BigNumber.zero;
        let addition = BigNumber.sub(total, noAdd);


        //--攻速，移速，范围，复活时间都要除以10000
        if(fightAttr == FightAttr.ASPD || fightAttr == FightAttr.Speed || 
        fightAttr == FightAttr.Range  || fightAttr == FightAttr.Respawn)
        {
            noAdd = BigNumber.div(noAdd, BigNumber.tenThousand);
            addition = BigNumber.div(addition, BigNumber.tenThousand);
        }

        //--暴伤，暴率要除以100
        if(fightAttr == FightAttr.CRI || fightAttr == FightAttr.CritDamage)
        {
            noAdd = BigNumber.div(noAdd, BigNumber.create(100));
            addition = BigNumber.div(addition, BigNumber.create(100));
        }
        
        //--普攻 近战的射程不加成
        if(fightAttr == FightAttr.Range || fightAttr == FightAttr.AttackRange)
        {
            if(this.CheckIsJinZhan(false))
            {
                return BigNumber.getValue(noAdd), BigNumber.getValue(addition), BigNumber.getValue(jingLing), BigNumber.getValue(zhuangBei);
            }
        }

        //--技能 近战的射程不加成
        if(fightAttr == FightAttr.SkillRange || fightAttr == FightAttr.SkillAttackRange)
        {
            if(this.CheckIsJinZhan(true))
            {
                return BigNumber.getValue(noAdd), BigNumber.getValue(addition), BigNumber.getValue(jingLing), BigNumber.getValue(zhuangBei);
            }
        }

        let [add, addType, reduceType, coef] = PlayerAttrPart.ChangeFightAttrToCard_Fight_Type(fightAttr);


        //--暴伤暴率算法不一样，取增加值
        if(fightAttr == FightAttr.CRI || fightAttr == FightAttr.CritDamage)
        {
            jingLing = this.JingLingAdditionAttr.Values[add];
            zhuangBei = this.ZhuangBeiAdditionAttr.Values[add];
        
        }
        else
        {
            let jingLingAdd = this.JingLingAdditionAttr.Values[addType];
            let jingLingReduce = this.JingLingAdditionAttr.Values[reduceType];
            jingLing = BigNumber.sub(jingLingAdd, jingLingReduce);
            let zhuangBeiAdd = this.ZhuangBeiAdditionAttr.Values[addType];
            let zhuangBeiReduce = this.ZhuangBeiAdditionAttr.Values[reduceType];
            zhuangBei = BigNumber.sub(zhuangBeiAdd, zhuangBeiReduce);
        } 

        //-- 双防御
        if(fightAttr == FightAttr.DEF || fightAttr == FightAttr.MDEF)
        {   
            let doubleDEFJingLingAdd = this.JingLingAdditionAttr.Values[PlayerAttrPart.Card_Fight_Type.DoubleDef_Add];
            let doubleDEFJingLingReduce = this.JingLingAdditionAttr.Values[PlayerAttrPart.Card_Fight_Type.DoubleDef_Reduce];
            let doubleDEFJingLing = BigNumber.sub(doubleDEFJingLingAdd, doubleDEFJingLingReduce);
            jingLing = BigNumber.add(jingLing, doubleDEFJingLing);
            let doubleDEFZhuangBeiAdd = this.ZhuangBeiAdditionAttr.Values[PlayerAttrPart.Card_Fight_Type.DoubleDef_Add];
            let doubleDEFZhuangBeiReduce = this.ZhuangBeiAdditionAttr.Values[PlayerAttrPart.Card_Fight_Type.DoubleDef_Reduce];
            let doubleDEFZhuangBei = BigNumber.sub(doubleDEFZhuangBeiAdd, doubleDEFZhuangBeiReduce);
            zhuangBei = BigNumber.add(zhuangBei, doubleDEFZhuangBei);
        }

        //--攻防血
        if(fightAttr == FightAttr.DEF || fightAttr == FightAttr.MDEF || 
            fightAttr == FightAttr.ATK || fightAttr == FightAttr.HP)
        {
            let adhJingLingAdd = this.JingLingAdditionAttr.Values[PlayerAttrPart.Card_Fight_Type.ADH_Add];
            let adhJingLingReduce = this.JingLingAdditionAttr.Values[PlayerAttrPart.Card_Fight_Type.ADH_Reduce];
            let adhJingLing = BigNumber.sub(adhJingLingAdd, adhJingLingReduce);
            jingLing = BigNumber.add(jingLing, adhJingLing);
            let adhZhuangBeiAdd = this.ZhuangBeiAdditionAttr.Values[PlayerAttrPart.Card_Fight_Type.ADH_Add];
            let adhZhuangBeiReduce = this.ZhuangBeiAdditionAttr.Values[PlayerAttrPart.Card_Fight_Type.ADH_Reduce];
            let adhZhuangBei = BigNumber.sub(adhZhuangBeiAdd, adhZhuangBeiReduce);
            zhuangBei = BigNumber.add(zhuangBei, adhZhuangBei);
        }

        //--都是加成，算百分比
        jingLing = BigNumber.div(jingLing, BigNumber.create(100));
        zhuangBei = BigNumber.div(zhuangBei, BigNumber.create(100));

        return [BigNumber.getValue(noAdd), BigNumber.getValue(addition), BigNumber.getValue(jingLing), BigNumber.getValue(zhuangBei)];
    }

//--判断是否是近战技能不需要加成信息
    CheckIsJinZhan(isSkill)
    {
        let conf_Card_Data = LoadConfig.getConfigData(ConfigName.ShiBing,this.CardID);
        if(conf_Card_Data == null)
        {
            return true;
        }
        let skillID = conf_Card_Data.NormalSkillID;
        if(isSkill)
        {
            skillID = conf_Card_Data.BigSkillID;
        }
        let conf_Skill1_Data = LoadConfig.getConfigData(ConfigName.JiNeng,skillID);
        if(conf_Skill1_Data == null)
        {
            return true;
        }
        let conf_CanShu_Data = CanShuTool.GetCanShuData(CanShuTool.CanShuEnum.NotAdditionRangeSkillType);
        if(conf_CanShu_Data == null)
        {
            return true;
        }
        for(let i = 1; i < conf_CanShu_Data.Param.length;i++ )
        {
            let typ = conf_CanShu_Data.Param[i];
            if(typ != 0 && typ == conf_Skill1_Data.ShowType)
            {
                return true;
            }
        }

    }

//--根据进化等级获取当前形态id
    GetCurrentXingTaiID()
    {
        return Card.GetXingTaiID(this.CardID, this.EvolutionLevel);
    }

//--通过卡牌ID和进化等级获取形态ID
    GetXingTaiID(cardID, evoLevel)
    {
        //--获取卡牌表格信息
        let conf_CardInfo = LoadConfig.getConfigData('KaPai_XinXi',cardID);
        if(conf_CardInfo == null)
        {
            console.error('Card.GetCurrentXingTaiID，获取卡牌表格信息是吧，cadid.'+this.CardID);
            return 0;
        }

        //--根据进化等级获取当前形态id
        if(evoLevel <= 0)
        {
            return conf_CardInfo.XingTaiID[0];
        }
        else if (evoLevel <= 3)
        {
            return conf_CardInfo.XingTaiID[1];
        }
        else if(evoLevel <= 6)
        {
            return conf_CardInfo.XingTaiID[2];
        }
        else
        {
            return conf_CardInfo.XingTaiID[3];
        }
    }

//--传入进化等级，根据进化等级获取等级属性
    GetLevelAttr(evoLevel, level)
    {
        //--取数据表
        let conf_XinXi = LoadConfig.getConfigData('KaPai_XinXi',this.CardID);
        if(conf_XinXi == null)
        {
            return null;
        } 
        
        let attrStoreId = conf_XinXi.ShuXingKu[this.Quality];//--这里要根据卡牌的品质计算
        let conf_PinZhi = LoadConfig.getConfigData(ConfigName.KaPai_PinZhiKu,attrStoreId);
        if(conf_PinZhi == null)
        {
            return null;
        }
        
        //--计算进化等级幂次方和升级差值
        let evoPow =(math.pow(1.15, evoLevel)) * 0.04;
        let decLevel = level - 1;
    
        //--初始化属性
        let attr = 
        {
            attack : 0,
            hp : 0,
            phyDef : 0,
            magDef : 0
        };
        
        attr.attack = (math.floor(conf_PinZhi.InitialAttack * evoPow + 0.5)) * decLevel;
        attr.hp = (math.floor(conf_PinZhi.InitialLife * evoPow + 0.5)) * decLevel;
        attr.phyDef = (math.floor(conf_PinZhi.InitialPhyDef * evoPow + 0.5)) * decLevel;
        attr.magDef = (math.floor(conf_PinZhi.InitialMagDef * evoPow + 0.5)) * decLevel;
        
        return attr;
    }

    //--传入进化等级，根据进化等级获取进化属性
        GetEvoAttr(evoLevel, level)
        {
            //--取数据表
            let conf_XinXi = LoadConfig.getConfigData('KaPai_XinXi',this.CardID);
            if(conf_XinXi == null)
            {
                return null;
            } 
            
            let attrStoreId = conf_XinXi.ShuXingKu[this.Quality];//--这里要根据卡牌的品质计算
            let conf_PinZhi = LoadConfig.getConfigData(ConfigName.KaPai_PinZhiKu,attrStoreId);
            if(conf_PinZhi == null)
            {
                return null;
            }
            
            //--计算进化等级幂次方和升级差值
            let evoPow = (math.pow(1.15, evoLevel));
            
            //--初始化属性
            let attr = 
            {
                attack : 0,
                hp : 0,
                phyDef : 0,
                magDef : 0
            };
            
            attr.attack = math.floor(conf_PinZhi.InitialAttack * evoPow + 0.5);
            attr.hp 	= math.floor(conf_PinZhi.InitialLife * evoPow + 0.5);
            attr.phyDef = math.floor(conf_PinZhi.InitialPhyDef * evoPow + 0.5);
            attr.magDef = math.floor(conf_PinZhi.InitialMagDef * evoPow + 0.5);
            
            return attr;
        }

//--获取自身基础属性 （等级属性 + 进化属性）
    GetBaseAttr(evoLevel, level)
    {
        //--获取等级属性
        let levelAttr = this.GetLevelAttr(evoLevel,level);
        let evoAttr = this.GetEvoAttr(evoLevel,level);
        
        if(levelAttr == null || evoAttr == null)
        {
            return null;
        }
        
        //--初始化属性
        let attr = 
        {
            attack : 0,
            hp : 0,
            phyDef : 0,
            magDef : 0
        }
                    
        attr.attack = levelAttr.attack +  evoAttr.attack;
        attr.hp = levelAttr.hp +  evoAttr.hp;
        attr.phyDef = levelAttr.phyDef +  evoAttr.phyDef;
        attr.magDef = levelAttr.magDef +  evoAttr.magDef;
        
        return attr;
    }

//--克隆卡牌
    CloneCard()
    {
        let cloneCard = new Card(this);
        return cloneCard;
    }

//--获取空卡
    GetBlankCard()
    {
        let card = new Card(
        {
            ObjID              : '',//--卡牌唯一ID 
            CardID             : 0, //--卡牌ID
            Level              : 0, //--等级   
            Exp                : 0, //--经验
            Quality            : 0, //--品质
            EvolutionLevel     : 0, //--进化等级(少三的突破)
            CurrentTuPoValue   : 0, //--当前突破值
            TuPoLevel          : 0, //--突破等级(少三的天命等级)
            ChaJianLevel	    : 0, //--插件等级(少三的觉醒)
            YuQianLevel        : 0, //--跃迁等级(少三的化神)

            ChaJianEquip       : 0, //--插件装备数组(大小为4的int数组，记录是否装备该位置)

            JianShen_NewAttr   : 0, //--健身出来的攻击
            JianShen_NewPhyDef : 0, //--健身出来的物防
            JianShen_NewMagDef : 0, //--健身出来的法防
            JianShen_NewHP     : 0, //--健身出来的生命
            JianShen_Attr      : 0, //--当前健身的攻击
            JianShen_PhyDef    : 0, //--当前健身的物防
            JianShen_MagDef    : 0, //--当前健身的法防
            JianShen_HP        : 0, //--当前健身的生命

            Attack             : 0, //--攻击
            HP                 : 0, //--生命
            PhyDef             : 0, //--物防
            MagDef             : 0, //--法防
            AttackAdd          : 0, //--攻击加成百分比
            HPAdd              : 0, //--生命加成百分比
            PhyDefAdd          : 0, //--物防加成百分比
            MagDefAdd          : 0, //--法防加成百分比

            Anger              : 0, //--初始怒气值
            AddAnger           : 0, //--每回合怒气恢复值

            HitRate            : 0, //--命中率
            DodgeRate          : 0, //--闪避率
            CritRate           : 0, //--暴击率
            ResistCritRate     : 0, //--抗爆率
            HurtPlus           : 0, //--伤害加成
            HurtReduce         : 0, //--伤害减免
            BigCritRate        : 0, //--致命一击

            IngoreDefRate      : 0, //--忽视防御概率
            IngoreDefPercent   : 0, //--忽视防御比例
            BloodRate          : 0, //--吸血概率
            BloodPercent       : 0, //--吸血比例
            PhyBackRate        : 0, //--物理反弹概率
            PhyBackPercent     : 0, //--物理反弹比例
            MagBackRate        : 0, //--魔法反弹概率
            MagBackPercent     : 0, //--魔法反弹比例
            HpRecoverRate      : 0, //--生命恢复概率
            HpRecoverPercent   : 0, //--生命恢复比例

            PVPHurtPlus        : 0, //--PVP增伤
            PVPHurtReduce      : 0, //--PVP减伤

            ResVaccine         : 0, //--克制疫苗种
            ResData            : 0, //--克制数据种
            ResViruses         : 0, //--克制病毒种
            ResFree            : 0, //--克制自由种
            DefVaccine         : 0, //--抵抗疫苗种
            DefData            : 0, //--抵抗数据种
            DefViruses         : 0, //--抵抗病毒种
            DefFree            : 0, //--抵抗自由种   
        });
        return card;
    }
}