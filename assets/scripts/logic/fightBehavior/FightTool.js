/*
作者（Auth||）:    skyHuang

描述（Describe）:
*/
/*jshint esversion: 6 */

import FightBehaviorManager from "./FightBehaviorManager";
import { PlayerIntAttr } from "../../part/PlayerBasePart";
import { FightAttr } from "../fight/FightDef";
import BigNumber from "../../tool/BigNumber";


let FightTool = (function(){
    let GUIDCount = 0;
    return{
        GetGUID()
        {
            GUIDCount = GUIDCount + 1;
            return GUIDCount.toString();
        },

        //获取时间间隔，如果加上加速功能之后，需要用到
        GetTimeGap(time)
        {
            let d = new Date();
            return (d.getTime()/1000 - time/1000) * FightBehaviorManager.GetSpeed();
        },

        ReduceFightAttr(myCards, myBossCard, enemyCards, enemyBossCard)
        {
            let minLength = 10000;
            for(let i = 0; i < myCards.length; ++i)
            {
                let card = myCards[i];
                let len = this.GetMinLengthFightAttrByCard(card);
                if( len < minLength){
                    minLength = len;
                }
            }
            let len = this.GetMinLengthFightAttrByCard(myBossCard);
            if( len < minLength){
                minLength = len;
            }
            for(let i = 0; i < enemyCards.length; ++i){
                let card = enemyCards[i];
                let len = this.GetMinLengthFightAttrByCard(card);
                if( len < minLength){
                    minLength = len;
                }
            }
            len = this.GetMinLengthFightAttrByCard(enemyBossCard);
            if( len < minLength){
                minLength = len;
            }
        
            if( minLength <= 6){
                //不约分
                //还原战斗管理器的约分长度
                FightBehaviorManager.mCutCount = 0;
                return [myCards, myBossCard, enemyCards, enemyBossCard];
            }
        
            let needCut = minLength - 6;
            //设置战斗管理器的约分长度
            FightBehaviorManager.mCutCount = needCut;
            for(let i = 0; i < myCards.length; ++i){
                let card = myCards[i];
                myCards[i] = this.ReduceCard(card, needCut);
            }
            myBossCard = this.ReduceCard(myBossCard, needCut);
            for(let i = 0; i < enemyCards.length; ++i){
                let card = enemyCards[i];
                enemyCards[i] = this.ReduceCard(card, needCut);
            }
            enemyBossCard = this.ReduceCard(enemyBossCard, needCut);
            return [myCards, myBossCard, enemyCards, enemyBossCard];
        },
        GetMinLengthFightAttrByCard(card)
        {
            let minLength = 10000;
            for(let i = FightAttr.ATK; i < FightAttr.Max; ++i){
                //只约分 攻击，物防，法防，血量，最大血量
                if( i == FightAttr.ATK || i == FightAttr.HP || i == FightAttr.DEF || i == FightAttr.MDEF || i == FightAttr.MaxHP){
                    let len = card.FightAttr[i].length;
                    //0不约分
                    if( len < minLength && BigNumber.notEqualTo(card.FightAttr[i], BigNumber.zero)){
                        minLength = len;
                    }
                }
            }

            return minLength;
        },
        ReduceCard(card, cut)
        {
            for(let i = FightAttr.ATK; i < FightAttr.Max; i++){
                //只约分 攻击，物防，法防，血量，最大血量
                if( i == FightAttr.ATK || i == FightAttr.HP || i == FightAttr.DEF || i == FightAttr.MDEF || i == FightAttr.MaxHP){
                    let attr = card.FightAttr[i]
                    //0不约分
                    if( BigNumber.notEqualTo(attr, BigNumber.zero)){
                        let len = card.FightAttr[i].length;
                        card.FightAttr[i]  = string.sub( attr, 1, len - cut);
                    }
                }
            }

            return card;
        },
        //获取卡牌高度
        GetModelHeight(cardID)
        {
            let conf_Card_Data = LoadConfig.getConfigData(ConfigName.ShiBing,cardID);
            if (!conf_Card_Data)
                return 0;
            
            let conf_MoXing_Data = LoadConfig.getConfigData(ConfigName.MoXing,conf_Card_Data.ModelID);
            if (conf_MoXing_Data == null)
                return 0;
            
            return conf_MoXing_Data.Height;
        },
        //获取主角卡牌
        GetMyBossID()
        {
            let sex = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_Sex];
            if (sex == 1)
                return 1900001;
            else
                return 1900002;
        },
        SplitTime(str)
        {
            let strArry = str.toString().split("|");
            let num = [];
            strArry.forEach(element => {
                num.push(parseFloat(element));
            });
            return num;
        }
    };
})();

export default FightTool;
