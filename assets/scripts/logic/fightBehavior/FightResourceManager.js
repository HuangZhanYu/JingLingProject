/*
作者（Author）:    jason

描述（Describe）:   战斗资源管理器 预加载战斗资源
*/
/*jshint esversion: 6 */

import FightGameObjectManager from "../../view/FightGameObjectManager";
import { SpecialSkillType, FightBuffType } from "../fight/FightDef";


let FightResourceManager = (function(){
    
    let mCurLoadModel = [];    //当前需要加载的模型数组
    let mCurLoadEffect = [];  //当前需要加载的光效数组
    let mCurLoadSound = [];   //当前需要加载的音效数组
    let mCurLoadFinishFunc = null;  //加载完成回调
    let mCurLoadFinishParam = null;  //加载完成回调
    let mCurLoadID = 0;        //每次资源加载的ID

    return{
        //创建一个模型
        SpawnModel(modelName)
        {
            return FightGameObjectManager.getInstance().SpawnModel(modelName);
        },
        //创建一个光效
        //参数1，光效名 effectName
        //返回值，光效对象
        SpawnEffect(effectName)
        {
            return FightGameObjectManager.getInstance().SpawnEffect(effectName);
        },

        //创建一个会自动销毁的光效
        //参数1，effectName 光效名
        //参数2，position 世界坐标
        //参数4，eulerAngles 欧拉角 不传就用默认角度
        SpawnEffectInWorldPlace(effectName, position, eulerAngles)
        {
            let x = position.x;
            let y = position.y;
            let z = position.z;

            let useEulerAngles = eulerAngles != null;
            let eulerAngleX = 0;
            let eulerAngleY = 0;
            let eulerAngleZ = 0;

            if (useEulerAngles)
            {
                eulerAngleX = eulerAngles.x;
                eulerAngleY = eulerAngles.y;
                eulerAngleZ = eulerAngles.z;
            }

            return FightGameObjectManager.getInstance().SpawnEffectInWorldPlace(effectName, x, y, z,
            useEulerAngles, eulerAngleX, eulerAngleY, eulerAngleZ);
        },

        //创建一个会自动销毁的光效
        //参数1，effectName 光效名
        //参数2，parent 光效父对象 GameObject
        //参数3，duration 持续时长
        //参数4，eulerAngles 欧拉角 不传就用默认角度
        SpawnEffectInParent(effectName, parent, path, eulerAngles)
        {
            let useEulerAngles = eulerAngles != null;
            let eulerAngleX = 0;
            let eulerAngleY = 0;
            let eulerAngleZ = 0;

            if (useEulerAngles)
            {
                eulerAngleX = eulerAngles.x;
                eulerAngleY = eulerAngles.y;
                eulerAngleZ = eulerAngles.z;
            }

            return FightGameObjectManager.getInstance().SpawnEffectInParent(effectName, parent, path,
            useEulerAngles, eulerAngleX, eulerAngleY, eulerAngleZ);
        },

        //创建一个会自动销毁的光效
        //参数1，effectName 光效名
        //参数2，parent 光效父对象 GameObject
        //参数3，duration 持续时长
        //参数4，eulerAngles 欧拉角 不传就用默认角度
        SpawnEffectAutoDestroy(effectName, parent, path, duration, eulerAngles)
        {
            let useEulerAngles = eulerAngles != null;
            let eulerAngleX = 0;
            let eulerAngleY = 0;
            let eulerAngleZ = 0;

            if (useEulerAngles)
            {
                eulerAngleX = eulerAngles.x;
                eulerAngleY = eulerAngles.y;
                eulerAngleZ = eulerAngles.z;
            }

            let realDuration = duration / FightBehaviorManager.GetSpeed();

            return FightGameObjectManager.getInstance().SpawnEffectAutoDestroy(effectName, parent, path, realDuration,
            useEulerAngles, eulerAngleX, eulerAngleY, eulerAngleZ);
        },

        //创建一个会自动销毁的光效，其位置在世界
        //参数1，effectName 光效名
        //参数2，position 光效创建位置
        //参数3，duration 持续时长
        //参数4，eulerAngles 欧拉角 不传就用默认角度
        //返回一个光效对象
        SpawnEffectAutoDestroyInWorldPlace(effectName, position, duration, eulerAngles)
        {
            let x = position.x;
            let y = position.y;
            let z = position.z;

            let useEulerAngles = eulerAngles != null;
            let eulerAngleX = 0;
            let eulerAngleY = 0;
            let eulerAngleZ = 0;

            if (useEulerAngles)
            {
                eulerAngleX = eulerAngles.x;
                eulerAngleY = eulerAngles.y;
                eulerAngleZ = eulerAngles.z;
            }

            let realDuration = duration / FightBehaviorManager.GetSpeed();

            return FightGameObjectManager.getInstance().SpawnEffectAutoDestroyInWorldPlace(effectName, x, y, z, realDuration,
            useEulerAngles, eulerAngleX, eulerAngleY, eulerAngleZ);
        },
        //endregion

        //加载两个队伍的资源
        //cardIDLis1t 卡牌ID列表1
        //cardIDList2 卡牌ID列表2
        //func 资源加载完毕回调函数
        LoadAllTeam(cardIDList1, cardIDList2, func, param)
        {
            mCurLoadID = mCurLoadID + 1;
            let allCardIDList = [];
            for(let i = 0; i <  cardIDList1.length; ++i)
            {   
                let cardID = cardIDList1[i];
                allCardIDList.push(cardID);
            }
            for(let i = 0; i <  cardIDList2.length; ++i)
            {
                let cardID = cardIDList2[i];
                allCardIDList.push(cardID);
            }

            let [models, effecs, sounds] = this.GetResourceName(allCardIDList);
            mCurLoadModel = [];
            mCurLoadEffect = [];
            mCurLoadSound = [];

            for (let name of models)
            {
                if (name)
                    mCurLoadModel.push(name);
            }
            for (let name of effecs)
            {
                if (name)
                    mCurLoadEffect.push(name);
            }

            for (let name of sounds)
            {
                if (name)
                    mCurLoadSound.push(name);
            }

            mCurLoadFinishFunc = func;
            mCurLoadFinishParam = param;

            FightGameObjectManager.getInstance().LoadModel(mCurLoadModel, this.OnModelLoad.bind(this), mCurLoadID);
        },

        OnModelLoad(loadID)
        {
            FightGameObjectManager.getInstance().LoadEffect(mCurLoadEffect, this.OnEffectLoad.bind(this), loadID);
        },

        OnEffectLoad(loadID)
        {
            // for(let i = 1; i < mCurLoadSound.length; ++i)
            // {
            //     let name = mCurLoadSound[i];
            //     soundMgr.LoadEffect(name);
            // }

            if (loadID != mCurLoadID)
            {
                console.log("还有其他战斗的资源正在加载中");
            }
                
            

            //加载完毕，回调
            if (mCurLoadFinishFunc != null)
                mCurLoadFinishFunc(mCurLoadFinishParam);
            
        },

        //加载一个主动技光效
        LoadActiveSkill(skillID)
        {
            let needModel = new Map();
            let needEffects = new Map();
            let needSounds = new Map();
            let conf_JiNeng_ZhuDong_BiaoXian_Data = LoadConfig.getConfigData(ConfigName.JiNeng_ZhuDong_BiaoXian,skillID);
            if (conf_JiNeng_ZhuDong_BiaoXian_Data != null)
            {
                //主动技模型
                needModel.set(conf_JiNeng_ZhuDong_BiaoXian_Data.SkillModel,true);
                //主动技光效
                needEffects.set(conf_JiNeng_ZhuDong_BiaoXian_Data.SkillEffect,true);
                needEffects.set(conf_JiNeng_ZhuDong_BiaoXian_Data.HurtEffect,true);
                needEffects.set(conf_JiNeng_ZhuDong_BiaoXian_Data.SkillModelEffect,true);
                //主动技音效
                needSounds.set(conf_JiNeng_ZhuDong_BiaoXian_Data.SkillSound,true);
            }
            
            //Buff光效
            let conf_JiNeng_ZhuDong_Data = LoadConfig.getConfigData(ConfigName.JiNeng_ZhuDong,skillID);
            if (conf_JiNeng_ZhuDong_Data != null)
            {
                for(let i = 0; i < conf_JiNeng_ZhuDong_Data.EffectID.length; ++i)
                {
                    let buffID = conf_JiNeng_ZhuDong_Data.EffectID[i];
                    if (buffID > 0)
                    {
                        let conf_Buff_Data = LoadConfig.getConfigData(ConfigName.Buff,buffID);
                        if (conf_Buff_Data != null)
                        {
                            //添加Buff光效
                            needEffects.set(conf_Buff_Data.Effect,true);
                        }
                    }
                }
            }
            
                
            //加载模型
            let models = [];
            for (let name of needModel.keys())
            {
                if (name)
                    models.push(name);
            }
                
            FightGameObjectManager.getInstance().LoadAppendModel(models, null);

            //加载光效
            let effects = [];
            for (let name of needEffects.keys())
            {
                if (name)
                    effects.push(name);
            }
            
            FightGameObjectManager.getInstance().LoadAppendEffect(effects, null);

            //加载音效
            let sounds = [];
            for (let name of needSounds.keys())
            {
                if (name)
                    sounds.push(name);
            }

            for(let i = 0; i < sounds.length; ++i)
            {
                let name = sounds[i];
                soundMgr.LoadEffect(name);
            }
        },


        //获取士兵ID
        //参数1，士兵ID
        //返回值，士兵模型名称或''
        GetShiBingModelNameByShiBingID(id)
        {
            let shiBingData = LoadConfig.getConfigData(ConfigName.ShiBing,id);
            if (shiBingData == null)
                return '';

            let moXingID = shiBingData.ModelID;
            let modelData = LoadConfig.getConfigData(ConfigName.MoXing,moXingID);
            if (modelData == null)
                return '';

            return modelData.Model;
        },
        // 获取一个队伍的所有资源名
        // cardIDList 卡牌ID列表
        // isMyTeam 我方队伍标记
        // 返回modelArr，模型名数组，Key是资源名，Value是一个bool
        // 返回effectArr，光效名数组，Key是资源名，Value是一个bool
        // 返回soundArr，音效名数组，Key是资源名，Value是一个bool
        GetResourceName(cardIDList)
        {
            let needModels = new Map();
            let needEffects = new Map();
            let needSounds = new Map();

            for(let i = 0; i < cardIDList.length; ++i)
            {
                let cardID = cardIDList[i];
                // 遍历士兵表
                let conf_ShiBing_Data = LoadConfig.getConfigData(ConfigName.ShiBing,cardID);
                if (conf_ShiBing_Data != null)
                {
                    // 获取自身模型
                    let modelID = conf_ShiBing_Data.ModelID;
                    let conf_MoXing_Data = LoadConfig.getConfigData(ConfigName.MoXing,modelID);
                    if (conf_MoXing_Data != null)
                        needModels.set(conf_MoXing_Data.Model,true);
                    
                    // 获取技能召唤物模型
                    // 技能1
                    let skillID1 = conf_ShiBing_Data.NormalSkillID;
                    let [skill1Model, skill1Effect, skill1Sound] = this.GetSkillResourceName(skillID1);
                    for (let name of skill1Model.keys())
                        needModels.set(name,true);
                    
                    for(let name of skill1Effect.keys())
                        needEffects.set(name,true);
                    
                    for(let name of skill1Sound.keys())
                        needSounds.set(name,true);
                    
                    // 技能2
                    let skillID2 = conf_ShiBing_Data.BigSkillID;
                    let [skill2Model, skill2Effect, skill2Sound] = this.GetSkillResourceName(skillID2);
                    for(let name of skill2Model.keys())
                        needModels.set(name,true);
                    
                    for(let name of skill2Effect.keys())
                        needEffects.set(name,true);
                    
                    for(let name of skill2Sound.keys())
                        needSounds.set(name,true);
                    

                    // 特殊技能
                    for(let i = 0; i < conf_ShiBing_Data.SpecialType.length; ++i)
                    {
                        let specialSkillType = conf_ShiBing_Data.SpecialType[i];
                        // 获取类型为死亡，受击，出场的特殊技能，这些才有资源需要加载
                        if (specialSkillType == SpecialSkillType.Die ||
                            specialSkillType == SpecialSkillType.Hurt ||
                            specialSkillType == SpecialSkillType.InFight)
                            {
                                let specialSkillID = conf_ShiBing_Data.SpecialID[i];
                                let conf_SpecialSkill_Data = LoadConfig.getConfigData(ConfigName.TeShuJiNeng,specialSkillID);

                                if (conf_SpecialSkill_Data != null && conf_SpecialSkill_Data.SkillID > 0)
                                {
                                    let [skillModel, skillEffect, skillSound] = this.GetSkillResourceName(conf_SpecialSkill_Data.SkillID);
                                    for(let name of skillModel.keys())
                                        needModels.set(name,true);
                                    
                                    for(let name of skillEffect.keys())
                                        needEffects.set(name,true);
                                    
                                    for(let name of skillSound.keys())
                                        needSounds.set(name,true);
                                    
                                }
                            }
                    }

                    //死亡表现
                    let conf_SiWangBiaoXian_Data = LoadConfig.getConfigData(ConfigName.SiWangBiaoXian,modelID);
                    if (conf_SiWangBiaoXian_Data != null && conf_SiWangBiaoXian_Data.Ename != '')
                        needModels.set(conf_SiWangBiaoXian_Data.Ename,true);
                    
                }
            }


            // 获取主动技光效
            for(let i = 0;i < AbilityPart.SkillGrooves.length; ++i)
            {
                // 技能光效
                let skillID = AbilityPart.SkillGrooves[i].SkillID;
                let conf_JiNeng_ZhuDong_BiaoXian_Data = LoadConfig.getConfigData(ConfigName.JiNeng_ZhuDong_BiaoXian,skillID);
                if (conf_JiNeng_ZhuDong_BiaoXian_Data != null)
                {   
                    needModels.set(conf_JiNeng_ZhuDong_BiaoXian_Data.SkillModel,true);
                    needEffects.set(conf_JiNeng_ZhuDong_BiaoXian_Data.SkillEffect,true);
                    needEffects.set(conf_JiNeng_ZhuDong_BiaoXian_Data.HurtEffect,true);
                    needEffects.set(conf_JiNeng_ZhuDong_BiaoXian_Data.SkillModelEffect,true);
                    needSounds.set(conf_JiNeng_ZhuDong_BiaoXian_Data.SkillSound,true);
                }
                // Buff光效
                let conf_JiNeng_ZhuDong_Data = LoadConfig.getConfigData(ConfigName.JiNeng_ZhuDong,skillID);
                if (conf_JiNeng_ZhuDong_Data != null)
                {
                    for(let i = 0; i < conf_JiNeng_ZhuDong_Data.EffectID.length; ++i)
                    {
                        let buffID = conf_JiNeng_ZhuDong_Data.EffectID[i];
                        if (buffID > 0)
                        {
                            let conf_Buff_Data = LoadConfig.getConfigData(ConfigName.Buff,buffID);
                            if (conf_Buff_Data != null)
                            {
                                // 添加Buff光效
                                needEffects.set(conf_Buff_Data.Effect,true);
                            }
                        }
                    }
                }
            }

            // 加载出生死亡闪现光效
            needEffects.set("ChuSheng",true);
            needEffects.set("SiWang",true);
            needEffects.set("SiWang_2",true);
            needEffects.set("SiWang_3",true);
            needEffects.set("SiWang_4",true);
            needEffects.set("ShanXian",true);
            return [needModels, needEffects, needSounds];
        },
 
        // 获取一个技能所涉及的所有资源
        // skillID 技能ID
        // 返回needModels，模型名数组，Key是资源名，Value是一个bool
        // 返回needEffects，光效名数组，Key是资源名，Value是一个bool
        // 返回needSounds，音效名数组，Key是资源名，Value是一个bool
        GetSkillResourceName(skillID)
        {
            let needModels = new Map();
            let needEffects = new Map();
            let needSounds = new Map();
            // 技能召唤物模型
            // 攻击光效 受击光效 buff光效
            let conf_JiNeng_Data = LoadConfig.getConfigData(ConfigName.JiNeng,skillID);
            if (!conf_JiNeng_Data)
            {
                return [needModels, needEffects, needSounds];
            }
            // 敌方buff光效
            for(let i = 0; i < conf_JiNeng_Data.HurtEffectID.length; ++i)
            {
                let id = conf_JiNeng_Data.HurtEffectID[i];
                if (id > 0 )
                {
                    let conf_BuffPool_Data = LoadConfig.getConfigData(ConfigName.BuffPool,id);
                    if (conf_BuffPool_Data != null)
                    {
                        for (let j = 0; j < conf_BuffPool_Data.BuffID.length; ++j)
                        {
                            let buffID = conf_BuffPool_Data.BuffID[j];
                            if (buffID > 0)
                            {
                                let conf_Buff_Data = LoadConfig.getConfigData(ConfigName.Buff,buffID);
                                if (conf_Buff_Data != null)
                                {
                                    // 召唤buff会有模型信息
                                    if (conf_Buff_Data.Type == FightBuffType.Summon)
                                    {
                                        let shiBingID = conf_BuffPool_Data.Params[(j - 1) * 4 + 1];
                                        let conf_Summon_ShiBing_Data = LoadConfig.getConfigData(ConfigName.ShiBing,shiBingID);
                                        if (conf_Summon_ShiBing_Data != null)
                                        {
                                            // 获取召唤物模型
                                            let modelSummonID = conf_Summon_ShiBing_Data.ModelID;
                                            let conf_Summon_MoXing_Data = LoadConfig.getConfigData(ConfigName.MoXing,modelSummonID);
                                            if (conf_Summon_MoXing_Data != null)
                                                needModels.set(conf_Summon_MoXing_Data.Model,true);

                                            //获取召唤物光效
                                            let [childModel1, childEffect1, childSound1] = this.GetSkillResourceName(conf_Summon_ShiBing_Data.NormalSkillID);
                                            let [childModel2, childEffect2, childSound2] = this.GetSkillResourceName(conf_Summon_ShiBing_Data.BigSkillID);
                                        
                                            for(let name of childModel1.keys())
                                                needModels.set(name,true);
                                            
                                            for(let name of childEffect1.keys())
                                                needEffects.set(name,true);
                                            
                                            for(let name of childSound1.keys())
                                                needSounds.set(name,true);
                                            
                                            for(let name of childModel2.keys())
                                                needModels.set(name,true);
                                            
                                            for(let name of childEffect2.keys())
                                                needEffects.set(name,true);
                                            
                                            for(let name of childSound2.keys())
                                                needSounds.set(name,true);
                                            
                                        }
                                    }
                                    // 添加Buff光效
                                    needEffects.set(conf_Buff_Data.Effect,true);
                                }
                            }
                        }
                    }
                }
            }
                
            // 我方buff光效
            for(let i = 0; i < conf_JiNeng_Data.SelftEffectID.length; ++i)
            {
                let id = conf_JiNeng_Data.SelftEffectID[i];
                if (id > 0)
                {
                    let conf_BuffPool_Data = LoadConfig.getConfigData(ConfigName.BuffPool,id);
                    if (!conf_BuffPool_Data)
                    {
                        continue;
                    }
                    for(let j = 0; j < conf_BuffPool_Data.BuffID.length; ++j)
                    {
                        let buffID = conf_BuffPool_Data.BuffID[j];
                        if (buffID > 0)
                        {
                            let conf_Buff_Data = LoadConfig.getConfigData(ConfigName.Buff,buffID);
                            if (!conf_Buff_Data)
                                continue;
                            // 召唤buff会有模型信息
                            if (conf_Buff_Data.Type == FightBuffType.Summon)
                            {
                                let shiBingID = conf_BuffPool_Data.Params[(j - 1) * 4 + 1];
                                let conf_Summon_ShiBing_Data = LoadConfig.getConfigData(ConfigName.ShiBing,shiBingID);
                                if (conf_Summon_ShiBing_Data != null)
                                {
                                    // 获取召唤物模型
                                    let modelSummonID = conf_Summon_ShiBing_Data.ModelID;
                                    let conf_Summon_MoXing_Data = LoadConfig.getConfigData(ConfigName.MoXing,modelSummonID);
                                    if (conf_Summon_MoXing_Data != null)
                                        needModels.set(conf_Summon_MoXing_Data.Model,true);
                                    
                                    //获取召唤物光效
                                    let [childModel1, childEffect1, childSound1] = this.GetSkillResourceName(conf_Summon_ShiBing_Data.NormalSkillID);
                                    let [childModel2, childEffect2, childSound2] = this.GetSkillResourceName(conf_Summon_ShiBing_Data.BigSkillID);
                                
                                    for(let name of childModel1.keys())
                                        needModels.set(name,true);
                                    
                                    for(let name of childEffect1.keys())
                                        needEffects.set(name,true);
                                    
                                    for(let name of childSound1.keys())
                                        needSounds.set(name,true);
                                    
                                    for(let name of childModel2.keys())
                                        needModels.set(name,true);
                                    
                                    for(let name of childEffect2.keys())
                                        needEffects.set(name,true);
                                    
                                    for(let name of childSound2.keys())
                                        needSounds.set(name,true);
                                    
                                }
                                    
                            }
                            // 添加Buff光效
                            needEffects.set(conf_Buff_Data.Effect,true);
                        }
                    }
                }
            }
            // 我方团队Buff光效
            for(let i = 0; i < conf_JiNeng_Data.TeamEffectID.length; ++i)
            {
                let id = conf_JiNeng_Data.TeamEffectID[i];
                if (id > 0)
                {
                    let conf_BuffPool_Data = LoadConfig.getConfigData(ConfigName.BuffPool,id);
                    if (conf_BuffPool_Data != null)
                    {
                        for(let j = 0; j < conf_BuffPool_Data.BuffID.length; ++j)
                        {
                            let buffID = conf_BuffPool_Data.BuffID[j];
                            if (buffID > 0 )
                            {
                                let conf_Buff_Data = LoadConfig.getConfigData(ConfigName.Buff,buffID);
                                if (conf_Buff_Data != null)
                                {
                                    // 召唤buff会有模型信息
                                    if (conf_Buff_Data.Type == FightBuffType.Summon)
                                    {
                                        let shiBingID = conf_BuffPool_Data.Params[(j - 1) * 4 + 1];
                                        let conf_Summon_ShiBing_Data = LoadConfig.getConfigData(ConfigName.ShiBing,shiBingID);
                                        if (conf_Summon_ShiBing_Data != null)
                                        {
                                            // 获取召唤物模型
                                            let modelSummonID = conf_Summon_ShiBing_Data.ModelID;
                                            let conf_Summon_MoXing_Data = LoadConfig.getConfigData(ConfigName.MoXing,modelSummonID);
                                            if (conf_Summon_MoXing_Data != null)
                                                needModels.set(conf_Summon_MoXing_Data.Model,true);
                                            
                                            //获取召唤物光效
                                            let [childModel1, childEffect1, childSound1] = this.GetSkillResourceName(conf_Summon_ShiBing_Data.NormalSkillID);
                                            let [childModel2, childEffect2, childSound2] = this.GetSkillResourceName(conf_Summon_ShiBing_Data.BigSkillID);
                                        
                                            for(let name of childModel1.keys())
                                                needModels.set(name,true);
                                            
                                            for(let name of childEffect1.keys())
                                                needEffects.set(name,true);
                                            
                                            for(let name of childSound1.keys())
                                                needSounds.set(name,true);
                                            
                                            for(let name of childModel2.keys())
                                                needModels.set(name,true);
                                            
                                            for(let name of childEffect2.keys())
                                                needEffects.set(name,true);
                                            
                                            for(let name of childSound2.keys())
                                                needSounds.set(name,true);
                                            
                                        }
                                    }
                                    // 添加Buff光效
                                    needEffects.set(conf_Buff_Data.Effect,true);
                                }
                                
                            }
                        }
                    }
                }
                
            }

            //技能Buff
            let conf_Skill_BiaoXian_Data = LoadConfig.getConfigData(ConfigName.JiNeng_BiaoXian,skillID);
            if (conf_Skill_BiaoXian_Data != null)
            {
                // 技能特效
                needEffects.set(conf_Skill_BiaoXian_Data.SkillEffect,true);
                // 受击特效
                needEffects.set(conf_Skill_BiaoXian_Data.HurtEffect,true);
                // 前置受击特效
                needEffects.set(conf_Skill_BiaoXian_Data.PreHurtEffect,true);

                // 飞行道具特效
                let flyItemID = conf_Skill_BiaoXian_Data.FlyItemID;
                let conf_FlyItem_Data = LoadConfig.getConfigData(ConfigName.FeiXingDaoJu,flyItemID);
                if (conf_FlyItem_Data != null)
                {   
                    needEffects.set(conf_FlyItem_Data.SkillName,true);
                    needEffects.set(conf_FlyItem_Data.HurtEffect,true);
                    needEffects.set(conf_FlyItem_Data.DieEffect,true);
                }

                // 受击音效
                needSounds.set(conf_Skill_BiaoXian_Data.SkillSound,true);
            }

            //闪现技能特殊光效
            let conf_ShanXianBiaoXian_Data = LoadConfig.getConfigData(ConfigName.ShanXianBiaoXian,skillID);
            if (conf_ShanXianBiaoXian_Data != null)
            {   
                if (conf_ShanXianBiaoXian_Data.BEffect != '')
                    needEffects.set(conf_ShanXianBiaoXian_Data.BEffect,true);
                
                if (conf_ShanXianBiaoXian_Data.AEffect != '')
                    needEffects.set(conf_ShanXianBiaoXian_Data.AEffect,true);
            }

            //溅射技能特殊光效
            let conf_JianSheBiaoXian_Data = LoadConfig.getConfigData(ConfigName.JianSheBiaoXian,skillID);
            if (conf_JianSheBiaoXian_Data != null)
            {
                if (conf_JianSheBiaoXian_Data.HurtEffect != '')
                    needEffects.set(conf_JianSheBiaoXian_Data.HurtEffect,true);
            }

            return [needModels, needEffects, needSounds];
        }
    
    };
})();

export default FightResourceManager;