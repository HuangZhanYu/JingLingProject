/*
作者（Author）:    skyHuang

描述（Describe）.
*/
/*jshint esversion: 6 */
import FightBehaviorManager from "../FightBehaviorManager";
import FightGameObjectManager from "../../../view/FightGameObjectManager";
import { FightFlyItemType } from "../../fight/FightDef";

export default class FlyItemBehaviorManager
{
    static getInstance()
    {
        if(FlyItemBehaviorManager.instance == null)
        {
            FlyItemBehaviorManager.instance = new FlyItemBehaviorManager();
        }
        return FlyItemBehaviorManager.instance;
    }
    constructor()
    {
        console.log('FlyItemBehaviorManager  constructor');
    }
    //--处理飞行道具记录
    ProcessFlyItemBornRecord(record)
    {
        let masterUUID = record.UUID;          //--飞行道具创建者
        let targetUUID = record.TargetUUID;    //--飞行道具目标
        let flyItemID = record.FlyItemID;      //--飞行道具ID
        let flyTime = record.FlyTime;          //--飞行时间
        let skillID = record.SkillID;          //--技能ID
    
        let conf_FlyItem_Data = LoadConfig.getConfigData(ConfigName.FeiXingDaoJu,flyItemID);
        if(conf_FlyItem_Data == null)
        {
            return;
        }
        
        let fltItemName = conf_FlyItem_Data.SkillName;
        if(fltItemName == null || fltItemName == '')
        {
            return;
        }
    
        let conf_JiNengBiaoXian_Data = LoadConfig.getConfigData(ConfigName.JiNeng_BiaoXian,skillID);
        if(conf_JiNengBiaoXian_Data == null)
        {
            return;
        }
        
        let masterRole = FightBehaviorManager.GetRole(masterUUID);
        if(masterRole == null)
        {
            return;
        }
        let targetRole = FightBehaviorManager.GetRole(targetUUID);
        if(targetRole == null)
        {
            return;
        }
        let id = masterRole.mGameObjectID;
        let targetID = targetRole.mGameObjectID;
        let shotPosStr = conf_JiNengBiaoXian_Data.FlyItemShotPos;
        let flyType = conf_FlyItem_Data.FlyType;
        if(flyType == FightFlyItemType.Trail)
        {
            FightGameObjectManager.getInstance().CreateTrailFlyItem(id, targetID, shotPosStr, fltItemName, flyTime, conf_FlyItem_Data.TrailHit, conf_FlyItem_Data.TrailDuration);
        
        }else
        {
            if(flyType == FightFlyItemType.Parabola)
            {
                flyType = FightFlyItemType.Line;
            }
            FightGameObjectManager.getInstance().CreateFlyItem(id, targetID, shotPosStr, flyItemID, fltItemName, flyType, flyTime);
        }
    
    
    }
    
    //--处理穿透攻击飞行道具记录
    ProcessCrossFlyItemBornRecord(record)
    {
        let masterUUID = record.UUID;          //--飞行道具创建者
        let flyItemID = record.FlyItemID;      //--飞行道具ID
        let speed = record.Speed;              //--飞行速度
        let distance = record.Distance;        //--飞行距离
        let skillID = record.SkillID;          //--技能ID
        
        let masterRole = FightBehaviorManager.GetRole(masterUUID);
        if(masterRole == null)
        {
            return;
        }
    
        let conf_FlyItem_Data = LoadConfig.getConfigData(ConfigName.FeiXingDaoJu,flyItemID);
        if(conf_FlyItem_Data == null)
        {
            return;
        }
        
        let fltItemName = conf_FlyItem_Data.SkillName;
        if(fltItemName == null || fltItemName == '')
        {
            return;
        }
    
        let conf_JiNengBiaoXian_Data = LoadConfig.getConfigData(ConfigName.JiNeng_BiaoXian,skillID);
        if(conf_JiNengBiaoXian_Data == null)
        {
            return;
        }
    
        let id = masterRole.mGameObjectID; 
        let shotPosStr = conf_JiNengBiaoXian_Data.FlyItemShotPos;
        let flyType = conf_FlyItem_Data.FlyType; 
        FightGameObjectManager.getInstance().CreateCrossFlyItem(id, flyItemID, shotPosStr, speed, fltItemName, flyType, distance);
    }
}
FlyItemBehaviorManager.instance = null;
