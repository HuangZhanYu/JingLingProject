/*
作者（Author）:    skyHuang

描述（Describe）.
*/
/*jshint esversion: 6 */

import FightBehaviorRole from "./FightBehaviorRole";
import { StateDef } from "../state/StateDef";
import DieBossBehaviorState from "../state/DieBossBehaviorState";
import StandBossBehaviorState from "../state/StandBossBehaviorState";
import MoveBehaviorState from "../state/MoveBehaviorState";
import AttackBehaviorState from "../state/AttackBehaviorState";
import SprintBehaviorState from "../state/SprintBehaviorState";
import PullBehaviorState from "../state/PullBehaviorState";
import FightGameObjectManager from "../../../view/FightGameObjectManager";

export default class FightNormalRole extends FightBehaviorRole
{
    constructor(uuid, id, campType, gameObjectID, hp, maxhp, roleType, modelName)
    {
        super();
        this.mUUID          = uuid;
        this.mCardID        = id;
        this.mCampType      = campType;
        this.mRoleType      = roleType;
        this.mGameObjectID  = gameObjectID;
        this.mModelName     = modelName;
        this.mHP            = hp;
        this.mMaxHP         = maxhp;
        this.IsDead         = false;
        //--设置普通士兵的状态机
        this.mStates = [];
        this.mStates[StateDef.Die] = new DieBossBehaviorState(this);
        this.mStates[StateDef.Stand] = new StandBossBehaviorState(this);
        this.mStates[StateDef.Move] = new MoveBehaviorState(this);
        this.mStates[StateDef.Attack] = new AttackBehaviorState(this);
        this.mStates[StateDef.Sprint] = new SprintBehaviorState(this);
        this.mStates[StateDef.Pull] = new PullBehaviorState(this);
        this.mCurStateEnum = StateDef.Stand;
        this.mCurState = this.mStates[this.mCurStateEnum];
        //--Buff集合
        this.mBuffColloect   = new Map();
        this.mBuffEffectName = new Map();

        this.mIsRepel        = false;//-- 击飞击退标记
        this.mRepelStartTime = 0;    //-- 击飞击退开始时间
        this.mRepelDuration  = 0;    //-- 击飞击退持续时间

        this.mHUDContents    = {};   //-- 飘字
        this.mHUDTimeGap     = 1;    //-- 飘字间隔

        this.mShakeStartTime = 0;    //-- 震动开始时间
        this.mShakeTimeGaps  = {};   //-- 震动时间
        this.mShakeIndex     = 0;    //-- 震动下标

        this.mTeleportStartTime = 0; //--闪现开始时间
        this.mTeleportDuration = 0;  //--闪现持续时间
        //--初始化模型有关对象
        this.Init(uuid);
    }
    //-- 初始化
    Init(uuid)
    {
        //--设置FightGameObjectManager相应的GameObject对象
        let hitPath           = this.mModelName+'/root/Bip01/Qua';
        let headPath          = this.mModelName+'/root/Boold';
        let uiPath            = this.mModelName+'/root/Boold/FightRoleUIPanel';
        let bloodPath         = this.mModelName+'/root/Boold/FightRoleUIPanel/Blood/Progress';
        let enemyBloodPath    = this.mModelName+'/root/Boold/FightRoleUIPanel/Blood/EnemyProgress';
        let hud               = this.mModelName+'/root/Boold/FightRoleUIPanel/HUD';
        FightGameObjectManager.getInstance().AddFightRoleObj(this.mGameObjectID, this.mCampType, 
        this.mModelName, hitPath, headPath, uiPath, bloodPath, enemyBloodPath, hud);
    }

}
