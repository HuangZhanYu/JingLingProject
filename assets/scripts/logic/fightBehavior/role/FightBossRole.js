/*
作者（Author）:    skyHuang

描述（Describe）.
*/
/*jshint esversion: 6 */

import FightBehaviorRole from "./FightBehaviorRole";
import { StateDef } from "../state/StateDef";
import DieBossBehaviorState from "../state/DieBossBehaviorState";
import StandBossBehaviorState from "../state/StandBossBehaviorState";
import MoveBehaviorState from "../state/MoveBehaviorState";
import AttackBehaviorState from "../state/AttackBehaviorState";
import SprintBehaviorState from "../state/SprintBehaviorState";
import PullBehaviorState from "../state/PullBehaviorState";
import FightGameObjectManager from "../../../view/FightGameObjectManager";
import FightBehaviorManager from "../FightBehaviorManager";
import { FightPathType } from "../../fight/FightDef";
import BigNumber from "../../../tool/BigNumber";

export default class FightBossRole extends FightBehaviorRole
{
    constructor(uuid, id, campType, gameObjectID, hp, maxhp, roleType, modelName)
    {
        super();
        this.mUUID          = uuid;
        this.mCardID        = id;
        this.mCampType      = campType;
        this.mRoleType      = roleType;
        this.mGameObjectID  = gameObjectID;
        this.mModelName     = modelName;
        this.mHP            = hp;
        this.mMaxHP         = maxhp;
        this.IsDead         = false;
        //--设置普通士兵的状态机
        this.mStates = [];
        this.mStates[StateDef.Die] = new DieBossBehaviorState(this);
        this.mStates[StateDef.Stand] = new StandBossBehaviorState(this);
        this.mStates[StateDef.Move] = new MoveBehaviorState(this);
        this.mStates[StateDef.Attack] = new AttackBehaviorState(this);
        this.mStates[StateDef.Sprint] = new SprintBehaviorState(this);
        this.mStates[StateDef.Pull] = new PullBehaviorState(this);
        this.mCurStateEnum = StateDef.Stand;
        this.mCurState = this.mStates[this.mCurStateEnum];
        //--Buff集合
        this.mBuffColloect   = new Map();
        this.mBuffEffectName = new Map();

        this.mIsRepel        = false;//-- 击飞击退标记
        this.mRepelStartTime = 0;    //-- 击飞击退开始时间
        this.mRepelDuration  = 0;    //-- 击飞击退持续时间

        this.mHUDContents    = {};   //-- 飘字
        this.mHUDTimeGap     = 1;    //-- 飘字间隔

        this.mShakeStartTime = 0;    //-- 震动开始时间
        this.mShakeTimeGaps  = {};   //-- 震动时间
        this.mShakeIndex     = 0;    //-- 震动下标

        this.mTeleportStartTime = 0; //--闪现开始时间
        this.mTeleportDuration = 0;  //--闪现持续时间
        //--初始化模型有关对象
        this.Init(uuid);
        this.mBornAniStart = 0;      //--开始出生
        this.mBornAniFlag  = false;  //--出生标记
        this.mBornAniTime  = 0;      //--出生动画时长

        this.mHurtAniStart = 0;      //--开始受击
        this.mHurtAniFlag  = false;  //--受击标记
        this.mHurtAniTime  = 0;      //--受击动画时长
    }
    //-- 初始化
    Init(uuid)
    {
        //--设置FightGameObjectManager相应的GameObject对象
        let hitPath           = this.mModelName+'/root/Bip01/Qua';
        let headPath          = this.mModelName+'/root/Boold';
        let uiPath            = this.mModelName+'/root/Boold/FightRoleUIPanel';
        // let bloodPath         = this.mModelName+'/root/Boold/FightRoleUIPanel/Blood/Progress';
        // let enemyBloodPath    = this.mModelName+'/root/Boold/FightRoleUIPanel/Blood/EnemyProgress';
        // let hud               = this.mModelName+'/root/Boold/FightRoleUIPanel/HUD';
        FightGameObjectManager.getInstance().AddFightRoleObj(this.mGameObjectID, this.mCampType, 
        this.mModelName, hitPath, headPath, uiPath);
    }

    //--出生
    Born(position)
    {

        //--设置位置
        //--设置方向
        //--设置UI对准镜头
        //--设置出生光效
        let curCamera = FightBehaviorManager.GetCamera();
        FightGameObjectManager.getInstance().SetFightBossBorn(this.mGameObjectID, this.mCampType,   //--ID和阵营类型
        position.x, position.y, position.z, curCamera,                                              //--位置和跟随镜头
        'ChuSheng', FightPathType.Parent, 1);                                                       //--出生光效相关

        this.IsDead = false;
        //--播放出生动画
        this.PlayBornAni();
    }

    //--播放出生动画
    PlayBornAni()
    {

        //--Boss分两种，一种是站在盘子上的，一种是精灵，
        //--站在盘子上的是人类，出生的时候要播放丢球动画
        //--精灵出生不需要丢球
        //--判断条件是是否存在root/role路径

        let [success, duration] = FightGameObjectManager.getInstance().PlayFightBossBornAni(this.mGameObjectID, 'root/role',
        'stand', 'skill01', FightBehaviorManager.GetSpeed(), 0, false, 0);

        if(! success)
        {
            //--设置状态
            this.ChangeState(StateDef.Stand);
            return;
        }

        this.mBornAniFlag = true;
        this.mBornAniStart = (new Date()).getTime();
        this.mBornAniTime = duration;
    }

    //--受击
    //--hurtSrc 1 攻击
    //--hurtSrc 2 Buff  
    //--hurtSrc 3 主动技  
    Hurt(hurtSrc, hurtSrcID)
    {
        //-- 全屏界面不播放受击
        if(FightBehaviorManager.IsFullPanel)
        {
            return;
        }
        let [hurtEffectName, hurtEffectParent, hurtEffectDuration, dieEffectName, dieEffectParent, dieEffectDuration] = 
        this.GetHurtEffect(hurtSrc, hurtSrcID);
        let soundName = this.GetHurtSound(hurtSrc, hurtSrcID);
        soundName = soundName || "";
        //--参数调整
        hurtEffectName = hurtEffectName || "";
        hurtEffectParent = hurtEffectParent || 1;
        hurtEffectDuration = hurtEffectDuration || 0;
        dieEffectName = dieEffectName || "";
        dieEffectParent = dieEffectParent || 1;
        dieEffectDuration = dieEffectDuration || 0;
        //--血量
        let progress = BigNumber.floatDivision(this.mHP , this.mMaxHP);
        //--播放受击光效，飞行道具结束光效，受击音效，修改血条
        FightGameObjectManager.getInstance().SetFightRolePlayHurt(this.mGameObjectID,hurtEffectName,hurtEffectParent,hurtEffectDuration,
        dieEffectName, dieEffectParent, dieEffectDuration,
        soundName, 0.3, progress);

        //--如果存在路径root/role，需要播放动画

        let [success, duration] = FightGameObjectManager.getInstance().PlayFightBossBornAni(this.mGameObjectID, 'root/role',
        'stand', 'wound', FightBehaviorManager.GetSpeed(), null, false, 0);

        if(success)
        {
            this.mHurtAniStart = (new Date()).getTime();      //--开始受击
            this.mHurtAniFlag  = true;           //--受击标记
            this.mHurtAniTime  = duration;       //--受击动画时长
        }
    }

    //--Update
    Update()
    {
        //--状态刷新
        if(this.mCurState != null && ! this.mCurState.CanStop() && this.mCurStateEnum != StateDef.Move)
        {
            this.mCurState.Run();
            if(this.mCurState.CanStop())
            {
                this.mCurState.Stop();
            }
        }

        //--刷新Buff
        for(var [id,buffList] of this.mBuffColloect) 
        {
            //let conf_Buff_Data = LoadConfig.getConfigData(ConfigName.Buff,id);
            let newBuffList = [];
            for(let i = 0; i < this.buffList.length;i++)
            { 
                let buff = buffList[i];
                buff.Run();
                if(buff.CanStop())
                {
                    buff.Stop();
                }
                else
                {
                    newBuffList.push(buff);
                }
            }
            this.mBuffColloect.set(id,newBuffList);
        }

        //--丢球动画结束后要进入待机状态
        if(this.mBornAniFlag && (new Date()).getTime() - this.mBornAniStart > this.mBornAniTime)
        {
            this.mBornAniFlag = false;
            //--设置状态
            this.ChangeState(StateDef.Stand);
        }
        
        //--丢球动画结束后要进入待机状态
        if(this.mHurtAniFlag && (new Date()).getTime() - this.mHurtAniStart > this.mHurtAniTime)
        {
            this.mHurtAniFlag = false;
            //--播放站立动画
            let standAniName = 'stand';
            let speed = FightBehaviorManager.GetSpeed();
            let mode = 0;
            let childPath = 'root/role';
            FightGameObjectManager.getInstance().SetFightRoleStandBehavior(this.mGameObjectID, standAniName, speed, mode, childPath);
        }
    }

}
