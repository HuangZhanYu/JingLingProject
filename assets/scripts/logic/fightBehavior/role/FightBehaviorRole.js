/*
作者（Author）:    skyHuang

描述（Describe）:
*/
/*jshint esversion: 6 */
import { StateDef } from "../state/StateDef";
import { RecordT, HurtT, HudT } from "../../fight/FightRecord";
import FightTool from "../FightTool";
import BigNumber from "../../../tool/BigNumber";
import FightGameObjectManager from "../../../view/FightGameObjectManager";
import BaseAddEffectBuffBehavior from "../buff/BaseAddEffectBuffBehavior";
import SummonBuffBehavior from "../buff/SummonBuffBehavior";
import FightBehaviorManager from "../FightBehaviorManager";
import { FightPathType, RoleT, FightCampType } from "../../fight/FightDef";

export default class FightBehaviorRole
{
    //-- 创建角色
    constructor () 
    {
        this.InitField();
    }
    //初始化创建角色所有字段信息
    InitField()
    {
        this.mUUID           = 0;    //-- 唯一ID
        this.mCardID         = 0;    //-- 卡牌ID
        this.mCampType       = null;  //-- 阵营
        this.mRoleType       = 0;    //-- 士兵类型
        this.mGameObjectID   = 0;    //-- GameObjectID
        this.mModelName      = null;  //-- 模型名称
        this.mHP             = 0;    //-- 血量
        this.mMaxHP          = 0;    //-- 最大血量
        this.IsDead          = false;//-- 是否死亡
        this.mStates         = [];   //-- 状态机
        this.mCurState       = [];   //-- 当前的状态
        this.mCurStateEnum   = null;  //-- 当前状态枚举
        this.mBuffColloect   = new Map();   //-- Buff集合
        this.mBuffEffectName = new Map();   //-- Buff光效集合    
    
        this.mIsRepel        = false;//-- 击飞击退标记
        this.mRepelStartTime = 0;    //-- 击飞击退开始时间
        this.mRepelDuration  = 0;    //-- 击飞击退持续时间
    
        this.mHUDContents    = [];   //-- 飘字
        this.mHUDTimeGap     = 1;    //-- 飘字间隔
    
        this.mShakeStartTime = 0;    //-- 震动开始时间
        this.mShakeTimeGaps  = [];   //-- 震动时间
        this.mShakeIndex     = 0;    //-- 震动下标
    
        this.mIsTeleport     = false;//-- 闪现标记
        this.mTeleportStartTime = 0; //-- 闪现开始时间
        this.mTeleportDuration = 0;  //-- 闪现持续时间
        this.mTeleportSkillID  = 0;  //-- 闪现技能ID
        this.mTeleportX  = 0;
        this.mTeleportY  = 0;
        this.mTeleportZ  = 0;
    
        this.mTeleportBEffectFlag = false;
        this.mTeleportBEffectStartTime = 0;
        this.mTeleportBEffectTime = 0;
        this.mTeleportBEffectName = '';
    
        this.mTeleportAEffectFlag = false;
        this.mTeleportAEffectStartTime = 0;
        this.mTeleportAEffectTime = 0;
        this.mTeleportAEffectName = '';
    }
    //--更新信息
    UpdateData(uuid, id, campType, hp, maxhp, roleType)
    {
        this.mUUID          = uuid;
        this.mCardID        = id;
        this.mCampType      = campType;
        this.mRoleType      = roleType;
        this.mHP            = hp;
        this.mMaxHP         = maxhp;
        this.mHUDContents   = {};
        this.mHUDTimeGap    = 1;
    }
    //-- 初始化
    Init(uuid, gameObject)
    {
    }
    //-- 切换状态
    ChangeState(state, param)
    {    //--先停止旧的状态
        if(this.mCurState != null && !this.mCurState.CanStop())
        {
            this.mCurState.Stop();
        }
        this.mCurStateEnum = state;
        //--开启新的状态
        this.mCurState = this.mStates[this.mCurStateEnum];
        if(this.mCurState == null)
        {
            return;
        }
        this.mCurState.Start(param);
    }
    Update() 
    {
        //--状态刷新
        if(this.mCurState != null && ! this.mCurState.CanStop())
        {
            if(this.mCurStateEnum != StateDef.Move &&
                this.mCurStateEnum != StateDef.Sprint &&
                this.mCurStateEnum != StateDef.Pull)
            {
    
                this.mCurState.Run();
                if(this.mCurState.CanStop())
                {
                    this.mCurState.Stop();
                }
    
            }
        }
    
        //--刷新Buff
        // for(var [id,buffList] of this.mBuffColloect) 
        // {
        //     //let conf_Buff_Data = LoadConfig.getConfigData(ConfigName.Buff,id);
        //     let newBuffList = [];
        //     for(let i = 0; i < this.buffList.length;i++)
        //     { 
        //         let buff = buffList[i];
        //         buff.Run();
        //         if(buff.CanStop())
        //         {
        //             buff.Stop();
        //         }
        //         else
        //         {
        //             newBuffList.push(buff);
        //         }
        //     }
        //     this.mBuffColloect.set(id,newBuffList);
        // }
        
        //--检测是否击飞击退完毕
        if(this.mIsRepel && FightTool.GetTimeGap(this.mRepelStartTime) >= this.mRepelDuration)
        {
            this.mIsRepel = false;
        }
    
        //--检测是否震动完毕
        if(this.mShakeTimeGaps.length > 0)
        {
            //--到达一次震动时间
            if(FightTool.GetTimeGap(this.mShakeStartTime) >= this.mShakeTimeGaps[this.mShakeIndex])
            {
                this.mShakeIndex = this.mShakeIndex + 1;
                //--震动
                //let camera = FightBehaviorManager.GetCamera();
                //let sc = camera.getComponent('ShakeCamera');
                // if(! GOTool.IsDestroy(sc))
                // {
                //     sc.SetCamera(camera);
                //     sc.Shake(10, 10);
                // }
            }
    
            //--震动完毕
            if(this.mShakeIndex >= this.mShakeTimeGaps.length)
            {
                this.mShakeTimeGaps = [];
                this.mShakeIndex = 0;
            }
        }
    
        //--检测是否闪现完毕
        if(this.mIsTeleport && FightTool.GetTimeGap(this.mTeleportStartTime) >= this.mTeleportDuration)
        {
            this.mIsTeleport = false;
    
            //--播放闪现结束动作，光效
            this.TeleportEnd();
        }
    
        //--检测闪现前光效是否播放完毕
        if(this.mTeleportBEffectFlag && FightTool.GetTimeGap(this.mTeleportBEffectStartTime) >= this.mTeleportBEffectTime)
        {
            this.mTeleportBEffectFlag = false;
            if(this.mTeleportBEffectName != null && this.mTeleportBEffectName != '')
            {
                this.RemoveBuffEffect(this.mTeleportBEffectName);
                this.mTeleportBEffectName = '';
            }
        }
    
        //--检测闪现后光效是否播放完毕
        if(this.mTeleportAEffectFlag && FightTool.GetTimeGap(this.mTeleportAEffectStartTime) >= this.mTeleportAEffectTime)
        {
            this.mTeleportAEffectFlag = false;
            if(this.mTeleportAEffectName != null && this.mTeleportAEffectName != '')
            {
                this.RemoveBuffEffect(this.mTeleportAEffectName);
                this.mTeleportAEffectName = '';
            }
        }
    }
    //--出生
    Born(position)
    {
        //--回收所有Buff
        this.ReleaseBuff();
        //--设置位置
        //--设置方向
        //--设置UI对准镜头
        //--设置出生光效
        //let curCamera = FightBehaviorManager.GetCamera();
        FightGameObjectManager.getInstance().SetFightRoleBorn(this.mGameObjectID, this.mCampType,   //--ID和阵营类型
        position.x, position.y, position.z, 0,                                              //--位置和跟随镜头
        'ChuSheng', FightPathType.Parent, 1,                                                       //--出生光效相关
        0.17);                                                                                       //--出生缩放动画时长
        //--死亡标记
        this.IsDead = false;
        //--设置状态
        this.ChangeState(StateDef.Stand);
    }
    //--复活
    Respawn(position)
    {
        //--回收所有Buff
        this.ReleaseBuff();
        //--设置默认状态为站立
        this.mCurStateEnum = StateDef.Stand;
        this.mCurState = this.mStates[this.mCurStateEnum];

        //--死亡标记
        this.IsDead = false;
        this.ChangeState(StateDef.Stand);
        //let curCamera = FightBehaviorManager.GetCamera();
        FightGameObjectManager.getInstance().SetFightRoleRespawn(this.mGameObjectID, this.mCampType,    //--ID和阵营类型
        position.x, position.y, position.z, 0,                                                  //--位置和跟随镜头
        'ChuSheng', FightPathType.Parent, 1);                                                        //--出生光效相关
    }
    //--攻击
    Attack(targetRole, skillID, aspd)
    {
        let param = {};
        param.TargetRole = targetRole;
        param.SkillID = skillID;
        param.ASPD = aspd;
        this.ChangeState(StateDef.Attack, param);
        
        let conf_JiNengBiaoXian = LoadConfig.getConfigData(ConfigName.JiNeng_BiaoXian,skillID);
        //--判断是否震屏
        if(conf_JiNengBiaoXian != null && conf_JiNengBiaoXian.IsShake == 1)
        {
            this.mShakeStartTime = (new Date()).getTime();
            this.mShakeIndex = 1;
            this.mShakeTimeGaps = FightTool.SplitTime(conf_JiNengBiaoXian.DelayShake);
        }
    }
    //-- 移动到
    Move(position, speed)
    {
        let param = {};
        param.Speed = speed;
        param.Position = position;
        this.ChangeState(StateDef.Move, param);
    }
    //-- 被拉
    Pull(position, speed)
    {
        let param = {};
        param.Speed = speed;
        param.Position = position;
        this.ChangeState(StateDef.Pull, param);
    }
    //-- 冲刺
    Sprint(position, speed, targetRole, skillID, aspd)
    {
        //--回收镜头
        this.HoldCamera(1);
        let param = {};
        param.Speed = speed;
        param.Position = position;
        param.TargetRole = targetRole;
        param.SkillID = skillID;
        param.ASPD = aspd;
        this.ChangeState(StateDef.Sprint, param);
        
        let conf_JiNengBiaoXian = LoadConfig.getConfigData(ConfigName.JiNeng_BiaoXian,skillID);
        //--判断是否震屏
        if(conf_JiNengBiaoXian != null && conf_JiNengBiaoXian.IsShake == 1)
        {
            this.mShakeStartTime = (new Date()).getTime();
            this.mShakeIndex = 1;
            this.mShakeTimeGaps = FightTool.SplitTime(conf_JiNengBiaoXian.DelayShake);
        }
    }
    //--移动特殊的Update
    UpdateMove()
    {

        if(this.mCurState == null)
        {
            return;
        }
        if(this.mCurStateEnum != StateDef.Sprint && 
        this.mCurStateEnum != StateDef.Move && 
        this.mCurStateEnum != StateDef.Pull)
        {
            return;
        }
        
        this.mCurState.Run();
        if(this.mCurState.CanStop())
        {
            this.mCurState.Stop();
        }
        
    }
    //-- 停止移动
    StopMove()
    {
        FightGameObjectManager.getInstance().StopFightRoleMove(this.mGameObjectID);
    }
    //--击飞击退
    Repel(height, length, duration)
    {
        this.mIsRepel = true;
        this.mRepelDuration = duration;
        this.mRepelStartTime = (new Date()).getTime();
        FightGameObjectManager.getInstance().SetFightRoleRepel(this.mGameObjectID, height, length, duration);
    }
    //--停止击飞击退
    StopRepel()
    {
        this.mIsRepel = false;
        FightGameObjectManager.getInstance().StopFightRoleRepel(this.mGameObjectID);
    }
    //--站立
    Stand()
    {
        this.ChangeState(StateDef.Stand);
    }
    //--受击
    //--hurtSrc 1 攻击
    //--hurtSrc 2 Buff  
    //--hurtSrc 3 主动技  
    Hurt(hurtSrc, hurtSrcID)
    {
        //-- 全屏界面不播放受击
        if(FightBehaviorManager.IsFullPanel)
        {
            return;
        }
        let [hurtEffectName, hurtEffectParent, hurtEffectDuration, dieEffectName, dieEffectParent, dieEffectDuration]
        = this.GetHurtEffect(hurtSrc, hurtSrcID);
        let soundName = this.GetHurtSound(hurtSrc, hurtSrcID);
        soundName = soundName || "";
        //--参数调整
        hurtEffectName = hurtEffectName || "";
        hurtEffectParent = hurtEffectParent || 1;
        hurtEffectDuration = hurtEffectDuration || 0;
        dieEffectName = dieEffectName || "";
        dieEffectParent = dieEffectParent || 1;
        dieEffectDuration = dieEffectDuration || 0;
        //--血量
        let progress = BigNumber.floatDivision(this.mHP , this.mMaxHP);
        //--播放受击光效，飞行道具结束光效，受击音效，修改血条
        FightGameObjectManager.getInstance().SetFightRolePlayHurt(this.mGameObjectID,hurtEffectName,hurtEffectParent,hurtEffectDuration,
        dieEffectName, dieEffectParent, dieEffectDuration,
        soundName, 0.3, progress);
    }
    //--前置受击
    PreHurt(hurtSkillID)
    {
        let conf_JiNeng_BiaoXian_Data = LoadConfig.getConfigData(ConfigName.JiNeng_BiaoXian,hurtSkillID);
        if(conf_JiNeng_BiaoXian_Data == null)
        {
            return;
        }
        
        let effect    = conf_JiNeng_BiaoXian_Data.PreHurtEffect;
        if(effect == null || effect == "")
        {
            return;
        }
        let place     = conf_JiNeng_BiaoXian_Data.PreHurtPlace;
        let duration  = conf_JiNeng_BiaoXian_Data.PreHurtDuration;

        //--如果是敌人受击，光效要翻转180度
        let angle = 0;
        if(this.mCampType == FightCampType.Enemy)
        {
            angle = 180;
        }

        FightGameObjectManager.getInstance().SpwanEffectInFightRoleByLua(this.mGameObjectID, effect, place, duration, true, 0,angle,0);
    }
    //--获取受击光效
    GetHurtEffect(hurtSrc, hurtSrcID)
    {
        //-- 加载受击光效
        if(hurtSrc == HurtT.Attack)
        {
            //-- 近战 飞行道具手机光效
            let conf_JiNeng_BiaoXian_Data = LoadConfig.getConfigData(ConfigName.JiNeng_BiaoXian,hurtSrcID);
            if(conf_JiNeng_BiaoXian_Data == null)
            {
                return[];
            }
            if(conf_JiNeng_BiaoXian_Data.ShowType == 1)
            {
                //-- 近战受击光效
                let hitSkillName = conf_JiNeng_BiaoXian_Data.HurtEffect;
                if(hitSkillName == null || hitSkillName == '')
                {
                    return[];
                }
                let hitSkillDuration = conf_JiNeng_BiaoXian_Data.HurtDuration;
                return  [hitSkillName, conf_JiNeng_BiaoXian_Data.HurtPlace, hitSkillDuration];
            }else if(conf_JiNeng_BiaoXian_Data.ShowType == 2)
            {
                //-- 飞行道具受击光效
                let flyItemID = conf_JiNeng_BiaoXian_Data.FlyItemID;
                let conf_FlyItem_Data = LoadConfig.getConfigData(ConfigName.FeiXingDaoJu,flyItemID);
                if(conf_FlyItem_Data == null)
                {
                    return[];
                }
                let hitSkillName = conf_FlyItem_Data.HurtEffect;
                let hitSkillDuration = conf_FlyItem_Data.HurtDuration;

                let dieSkillName = conf_FlyItem_Data.DieEffect;
                let dieSkillDuration = conf_FlyItem_Data.DieDuration;

                return [hitSkillName, conf_FlyItem_Data.HurtPlace, hitSkillDuration, dieSkillName, conf_FlyItem_Data.DiePlace, dieSkillDuration];
            }
        }else if(hurtSrc == HurtT.Buff)
        {
            //-- Buff受击光效
        }else if(hurtSrc == HurtT.ASkill)
        {
            //-- 主动技受击光效
            let conf_Skill_BiaoXian_Data = LoadConfig.getConfigData(ConfigName.JiNeng_ZhuDong_BiaoXian,hurtSrcID);
            if(conf_Skill_BiaoXian_Data == null)
            {
                return[];
            }
            let hurtEffect = conf_Skill_BiaoXian_Data.HurtEffect;
            if(hurtEffect == null || hurtEffect == '')
            {
                return[];
            }
            return [hurtEffect, conf_Skill_BiaoXian_Data.HurtPlace, conf_Skill_BiaoXian_Data.HurtDuration];
        }else if(hurtSrc == HurtT.Splash)
        {
            let conf_JianShe_BiaoXian_Data = LoadConfig.getConfigData(ConfigName.JianSheBiaoXian,hurtSrcID);
            if(conf_JianShe_BiaoXian_Data == null)
            {
                return[];
            }
            let jianSheEffect = conf_JianShe_BiaoXian_Data.HurtEffect;
            if(jianSheEffect == null || jianSheEffect == '')
            {
                return;
            }
            return [jianSheEffect, conf_JianShe_BiaoXian_Data.HurtPlace, conf_JianShe_BiaoXian_Data.HurtDuration];
        }
    }
    //--获取受击音效
    GetHurtSound(hurtSrc, hurtSrcID)
    {
        if(hurtSrc == 1)
        {
            //-- 近战，飞行道具受击音效
            let conf_JiNeng_BiaoXian_Data = LoadConfig.getConfigData(ConfigName.JiNeng_BiaoXian,hurtSrcID);
            if(conf_JiNeng_BiaoXian_Data == null || conf_JiNeng_BiaoXian_Data.SkillSound == '')
            {
                return;
            }
            return conf_JiNeng_BiaoXian_Data.SkillSound;
        }else if(hurtSrc == 2)
        {
            //-- Buff受击音效

        }else if(hurtSrc == 3)
        {
            //-- 主动技受击音效
            let conf_Skill_BiaoXian_Data = LoadConfig.getConfigData(ConfigName.JiNeng_ZhuDong_BiaoXian,hurtSrcID);
            if(conf_Skill_BiaoXian_Data == null)
            {
                return;
            }
            let soundName = conf_Skill_BiaoXian_Data.SkillSound;
            if(soundName == null || soundName == '')
            {
                return;
            }
            return conf_Skill_BiaoXian_Data.SkillSound;
        }
    }
    //--死亡
    Die()
    {
        //--死亡标记
        this.IsDead = true;
        //--震动标记
        this.mShakeTimeGaps = { };
        this.mShakeIndex = 0;
        //--闪现标记
        this.mIsRepel = false;
        this.mIsTeleport     = false;
        this.mTeleportBEffectFlag = false;
        this.mTeleportAEffectFlag = false;

        //--回收镜头
        this.ResumeCamera();
        //--进入死亡状态
        this.ChangeState(StateDef.Die);

    }
    //--刷新血量
    UpdateBlood()
    {
        let progress = BigNumber.floatDivision(this.mHP,this.mMaxHP);
        FightGameObjectManager.getInstance().SetFightRoleHP(this.mGameObjectID, progress);
    }
    //-- 是否隐身
    SetTransparent(flag)
    {
        FightGameObjectManager.getInstance().SetFightRoleTransparent(this.mGameObjectID, flag);
    }
    //-- 是否石化
    SetPetrifaction(flag)
    {
        FightGameObjectManager.getInstance().SetFightRolePetrifaction(this.mGameObjectID, flag);
    }

//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//---通用功能函数
    //--获取位置
    GetPosition()
    {
        //--return GOTool.GetVector(this.mParentObj, TransformOperateType.Position)
        // var xx = 0;
        // var yy = 0;
        // var zz = 0;
        // var ww = 0;

        let[xx, yy] = FightGameObjectManager.getInstance().GetFightRoleVector(
            this.mGameObjectID,FightPathType.Parent,0);
        return { x:xx, y:yy};
    }
    //--回收镜头
    ResumeCamera()
    {
        if(FightGameObjectManager.getInstance().IsFightRoleTakeCemera(this.mGameObjectID  ,'OriginLogic'))
        {
            FightBehaviorManager.ResetCamera();
        }
    }
    //--Hold住镜头
    HoldCamera(time)
    {

        //--如果同一个人，又需要hold住镜头的话，延续Hold住镜头的时间
        if(FightBehaviorManager.mHoldRoleID == this.mUUID && FightBehaviorManager.mHoldCamera)
        {
            SceneManager.HoldCamera(time);
            FightBehaviorManager.mHoldRoleID = this.mUUID;
            FightBehaviorManager.mHoldCamera = true;
            FightBehaviorManager.mHoldTime = time ;         //--Hold住镜头时间
            FightBehaviorManager.mHoldStartTime = (new Date()).getTime();;
        }

        if(FightGameObjectManager.getInstance().IsFightRoleTakeCemera(this.mGameObjectID  ,'OriginLogic'))
        {
            SceneManager.HoldCamera(time);
            FightBehaviorManager.mHoldRoleID = this.mUUID;
            FightBehaviorManager.mHoldCamera = true;
            FightBehaviorManager.mHoldTime = time;         //--Hold住镜头时间
            FightBehaviorManager.mHoldStartTime = (new Date()).getTime();;
        }
    }
//-- //--Buff禁用镜头中，海外临时处理
//-- HoldCamera(time)
    
//--     if(FightGameObjectManager.getInstance().IsFightRoleTakeCemera(this.mGameObjectID  ,'OriginLogic'))
//--         FightBehaviorManager.ResetCamera()
//--         SceneManager.SetFollowCameraReset()
//--         FightBehaviorManager.mHoldCamera = true
//--         FightBehaviorManager.mHoldTime = time          //--Hold住镜头时间
//--         FightBehaviorManager.mHoldStartTime = (new Date()).getTime();
//--         
//--     }
    
//-- }

    //--播放动画
    PlayAni(aniName, speed, wrapMode)
    {
        FightGameObjectManager.getInstance().SetFightRolePlayAni(this.mGameObjectID, aniName, speed, wrapMode);
    }
    //--是否在播放动画
    IsPlayingAni(aniName)
    {
        return FightGameObjectManager.getInstance().SetFightRolePlayAni(this.mGameObjectID, aniName);
    }
    //--停止动画
    StopAni()
    {
        FightGameObjectManager.getInstance().StopFightRoleAni(this.mGameObjectID);
    }
    //--播放音效
    PlaySound(soundName)
    {
        soundMgr.PlayEffect(soundName, 0.3);
    }
    //--飘字
    HUD(content, fontColorType)
    {
        let lstTime = this.mHUDContents[content];
        if(lstTime == null)
        {
            lstTime = -1;
            this.mHUDContents[content] = lstTime;
        }

        if((new Date()).getTime() - lstTime < this.mHUDTimeGap)
        {
            return;
        }
        
        this.mHUDContents[content] = (new Date()).getTime();;

        let scaleTime = 0.5 / FightBehaviorManager.GetSpeed();
        let speed = 10 * FightBehaviorManager.GetSpeed();
        let duration = 3 / FightBehaviorManager.GetSpeed();
        FightGameObjectManager.getInstance().HUDText(this.mGameObjectID, content, fontColorType, scaleTime, speed, duration);
    }
    //--设置血量
    SetBlood(progress)
    {
        FightGameObjectManager.getInstance().SetFightRoleHP(this.mGameObjectID, progress);
    }

//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--Buff相关
    //--添加一个Buff
    AddBuff(record)
    {
        let id = record.BuffID;
        let conf_Buff_Data = LoadConfig.getConfigData(ConfigName.Buff,id);
        if(conf_Buff_Data == null)
        {
            console.error("BuffID错误."+id);
            return ;
        }
        
        let buff = null;
        if(conf_Buff_Data.Type == FightBuffType.Coma)   //--昏迷
        {
            buff = new  BaseAddEffectBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.Summon)    //--召唤
        {
            buff = new SummonBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.PI) //--物免
        {
            buff = new  BaseAddEffectBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.MI) //--魔免
        {
            buff = new  BaseAddEffectBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.AddHP)  //--一次加血
        {
            buff = new  BaseAddEffectBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.SlowDown)  //--减速
        {
            buff = new  BaseAddEffectBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.Repel)  //--击退
        {
            return;
        }else if( conf_Buff_Data.Type == FightBuffType.Off)    //--击飞
        {
            return;
        }else if( conf_Buff_Data.Type == FightBuffType.Splash) //--溅射
        {
            buff = new  BaseAddEffectBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.Frozen)    //--冰冻
        {
            buff = new  BaseAddEffectBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.Paralysis)    //--麻痹
        {
            buff = new  BaseAddEffectBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.Silence)    //--沉默
        {
            buff = new  BaseAddEffectBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.Curse)    //--诅咒
        {
            buff = new  BaseAddEffectBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.Invincible)    //--无敌
        {
            buff = new  BaseAddEffectBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.Dodge)    //--闪避
        {
            buff = new  BaseAddEffectBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.Hiding)    //--隐身
        {
            buff = new HidingBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.DropBlood)    //--持续扣血
        {
            buff = new  BaseAddEffectBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.Shield)    //--护盾
        {
            buff = new  BaseAddEffectBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.AddATK)    //--增加物理攻击
        {
            buff = new  BaseAddEffectBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.AddMATK)    //--增加法术攻击
        {
            buff = new  BaseAddEffectBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.AddASPD)    //--增加攻击速度
        {
            buff = new  BaseAddEffectBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.AddSpeed)    //--增加移动速度
        {
            buff = new  BaseAddEffectBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.ImmComa)    //--免疫晕
        {
            buff = new  BaseAddEffectBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.ImmFrozen)    //--免疫冰冻
        {
            buff = new  BaseAddEffectBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.ImmRepel)    //--免疫击退
        {
            buff = new  BaseAddEffectBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.ImmOff)    //--免疫击飞
        {
            buff = new  BaseAddEffectBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.Winding)    //--缠绕
        {
            buff = new  BaseAddEffectBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.Shock)    //--震荡
        {
            buff = new  BaseAddEffectBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.Interfere)    //--干扰
        {
            buff = new  BaseAddEffectBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.Poisoning)    //--中毒
        {
            buff = new  BaseAddEffectBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.Vampire)    //--吸血
        {
            buff = new  BaseAddEffectBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.Intimidate)    //--恐吓
        {
            buff = new  BaseAddEffectBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.GhostCurse)    //--鬼咒
        {
            buff = new  BaseAddEffectBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.Deter)    //--威慑
        {
            buff = new  BaseAddEffectBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.Rebound)    //--反弹
        {
            buff = new  BaseAddEffectBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.Charm)    //--魅惑
        {
            buff = new  BaseAddEffectBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.Petrifaction)    //--石化
        {
            buff = new PetrifactionBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.Ridicule)    //--嘲讽
        {
            buff = new  BaseAddEffectBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.IntoVoid)    //--遁入虚空
        {
            buff = new  BaseAddEffectBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.Purify)    //--净化
        {
            buff = new  BaseAddEffectBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.AddCrit)    //--增加暴击率
        {
            buff = new  BaseAddEffectBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.AddCritDmg)    //--增加暴击伤害
        {
            buff = new  BaseAddEffectBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.AddDEF)    //--增加防御值
        {
            buff = new  BaseAddEffectBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.AddMDEF)    //--增加法术防御值
        {
            buff = new  BaseAddEffectBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.ImmControl)    //--免疫控制
        {
            buff = new  BaseAddEffectBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.DecHit)    //--减命中
        {
            buff = new  BaseAddEffectBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.ImmSlow)    //--免疫减速
        {
            buff = new  BaseAddEffectBuffBehavior(id, this, record);
        }

        if(buff == null)
        {
            console.error("Buff类型错误ID."+id+"|Type."+conf_Buff_Data.Type);
            return ;
        }
        buff.Start();

        let buffList = this.mBuffColloect[id];
        if(buffList == null)
        {
            buffList = {};
        }

        buffList.push(buff);
        this.mBuffColloect[id] = buffList;

        //--buff飘字
        let buffType = LoadConfig.getConfigData(ConfigName.Buff,id).Type;
        if(buffType == null)
        { 
            return;
        }
        //--console.error("buffType."+buffType);
        if(buffType == 17) 
        {
            //--流血：闪避红
            this.HUD(LoadConfig.getConfigData(ConfigName.Buff,id).TipContent, SMFontColorType.ShanBi);
        }else if( buffType == 5) 
        {
            //--治疗：攻击力
            this.HUD(LoadConfig.getConfigData(ConfigName.Buff,id).TipContent, SMFontColorType.GongJiLi);
        }else if( buffType == 30 || buffType == 6 || buffType == 11 || buffType == 13 || buffType == 36) 
        {
            //--中毒、中毒减速、诅咒、麻痹、魅惑：品质紫边
            this.HUD(LoadConfig.getConfigData(ConfigName.Buff,id).TipContent, SMFontColorType.PinZhiZiBian);
        }else 
        {
            //--其余有可能没有飘字
            if(LoadConfig.getConfigData(ConfigName.Buff,id).TipContent != null && LoadConfig.getConfigData(ConfigName.Buff,id).TipContent != "")
            {
                //--其余如眩晕：品质橙边
                this.HUD(LoadConfig.getConfigData(ConfigName.Buff,id).TipContent, SMFontColorType.PinZhiChengBian);
            }
        }
    }
    //--添加击飞击退Buff
    AddRepelBuff(record)
    {
        let id = record.BuffID;
        let conf_Buff_Data = LoadConfig.getConfigData(ConfigName.Buff,id);
        if(conf_Buff_Data == null)
        {
            console.error("BuffID错误."+id);
            return;
        }
        if(conf_Buff_Data.Type == FightBuffType.Repel)  //--击退
        {
            buff = new RepelBuffBehavior(id, this, record);
        }else if( conf_Buff_Data.Type == FightBuffType.Off)    //--击飞
        {
            buff = new OffBuffBehavior(id, this, record);
        }
        if(buff == null)
        {
            console.error("Buff类型错误ID."+id+"|Type."+conf_Buff_Data.Type);
            return;
        }
        //--回收镜头
        this.HoldCamera((record.LstTime / 1000 + 2)/ FightBehaviorManager.GetSpeed());

        //--开始BUFF
        buff.Start();

        let buffList = this.mBuffColloect[id];
        if(buffList == null)
        {
            buffList = [];
        }

        buffList.push(buff);
        this.mBuffColloect[id] = buffList;
    }
    //--移除一个Buff
    RemoveBuff(id)
    {
        let buffList = this.mBuffColloect[id];
        if(buffList == null)
        {
            return;
        }
        table.remove(buffList, 1);
    }
    AddBuffEffect(name, rootType)
    {
        let effect = this.mBuffEffectName.get(name);
        if(effect == null)
        {
            effect = {};
            effect.Count = 1;
            effect.ID = FightGameObjectManager.getInstance().AddFightRoleBuffEffect(this.mGameObjectID, name, rootType);
        }
        else
        {
            effect.Count = effect.Count + 1;
        }
        this.mBuffEffectName.set(name,effect);
    }
    RemoveBuffEffect(name)
    {
        let effect = this.mBuffEffectName.get(name);
        if(effect == null)
        {
            effect = { };
            effect.Count = 0;
            effect.ID = 0;
        }
        else
        {
            effect.Count = effect.Count - 1;
        }
        if(effect.Count == 0)
        {
            if(effect.ID > 0)
            {
                FightGameObjectManager.getInstance().RemoveFightRoleBuffEffect(effect.ID);
            }
            this.mBuffEffectName.set(name,null);
        }
        else
        {
            this.mBuffEffectName.set(name,effect);
        }
    }
    //--回收所有Buff
    ReleaseBuff()
    {

        for(var [id, buffList] of this.mBuffColloect)
        {
            for(let i = 0; i < buffList.length; i++)
            {
                let buff = buffList[i];
                buff.Stop();
            }
        }
        this.mBuffColloect.clear();
    }
//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//---处理Record函数
    //--接受帧信息
    ReceiveRecord(recordT, record)
    {
        if(recordT == RecordT.RecordT_Born)
        {
            this.ProcessBornRecord(record);
        }else if( recordT == RecordT.RecordT_Revive)
        {
            this.ProcessRespawnRecord(record);
        }else if( recordT == RecordT.RecordT_Die)
        {
            this.ProcessDieRecord(record);
        }else if( recordT == RecordT.RecordT_Move)
        {
            this.ProcessMoveRecord(record);
        }else if( recordT == RecordT.RecordT_Attack)
        {
            this.ProcessAttackRecord(record);
        }else if( recordT == RecordT.RecordT_Stand)
        {
            this.ProcessStandRecord(record);
        }else if( recordT == RecordT.RecordT_IncHP)
        {
            this.ProcessAddHPRecord(record);
        }else if( recordT == RecordT.RecordT_Hurt)
        {
            this.ProcessHurtRecord(record);
        }else if( recordT == RecordT.RecordT_PreHurt)
        {
            this.ProcessPreHurtRecord(record);
        }else if( recordT == RecordT.RecordT_AddBuff)
        {
            this.ProcessAddBuffRecord(record);
        }else if( recordT == RecordT.RecordT_RmvBuff)
        {
            this.ProcessRmvBuffRecord(record);
        }else if( recordT == RecordT.RecordT_Hud)
        {
            this.ProcessHudRecord(record);
        }else if( recordT == RecordT.RecordT_Teleport)
        {
            this.ProcessTeleportRecord(record);
        }else if( recordT == RecordT.RecordT_Repel)
        {
            this.ProcessRepelRecord(record);
        }else if( recordT == RecordT.RecordT_Sprint)
        {
            this.ProcessSprintRecord(record);
        }else if( recordT == RecordT.RecordT_SprintFinish)
        {
            this.ProcessSprintFinishRecord(record);
        }else if( recordT == RecordT.RecordT_Pull)
        {
            this.ProcessPullRecord(record);
        }else if( recordT == RecordT.RecordT_PullFinish)
        {
            this.ProcessPullFinishRecord(record);
        }
    }

    //--处理出生记录
    ProcessBornRecord(record)
    {
        let pos = record.Pos;
        // let usePos = {};
        // usePos.x = pos.y;
        // usePos.z = pos.x;
        // usePos.y = FightTool.GetModelHeight(this.mCardID);
        
        //--未做血量超出判断
        this.UpdateBlood();

        this.Born(pos);
    }
    //--处理复活记录
    ProcessRespawnRecord(record)
    {
        
        FightGameObjectManager.getInstance().StopFightRoleDieBehavior(this.mGameObjectID);

        let pos = record.Pos;
        // let usePos = {};
        // usePos.x = pos.y;
        // usePos.z = pos.x;
        // usePos.y = FightTool.GetModelHeight(this.mCardID);

        //--血量
        this.UUID           = record.UUID;
        this.mCardID        = record.ID;
        this.mCampType      = record.Camp;
        this.mRoleType      = record.Type;
        this.mHP            = record.HP;
        this.mMaxHP         = record.MaxHP;
        //--未做血量超出判断
        this.UpdateBlood();

        this.Respawn(pos);
    }

    //--处理死亡记录
    ProcessDieRecord(record)
    {
        this.Die();
    }
    //--处理移动记录
    ProcessMoveRecord(record)
    {
        //--根据UUID获取对象，获取对象的位置
        let targetUUID = record.TargetUUID;

        //--技能演示特殊
        if(targetUUID == 0)
        {
            // let targetPosition = {};
            // targetPosition.x = record.Pos.x;
            // targetPosition.y = record.Pos.y;
            let speed = record.Speed;
            this.Move(record.Pos, speed);
        }
        else
        {
            let targetRole = FightBehaviorManager.GetRole(targetUUID);
            if(targetRole == null)
            {
                console.error("移动对象不存在targetUUID." + targetUUID);
                return;
            }
            let targetPosition = targetRole.GetPosition();
            let speed = record.Speed;
            this.Move(targetPosition, speed);
        }

    }
    //--处理冲刺记录
    ProcessSprintRecord(record)
    {    
        let targetUUID = record.TargetUUID;
        let targetRole = FightBehaviorManager.GetRole(targetUUID);
        if(targetRole == null)
        {
            console.error("移动对象不存在targetUUID." + targetUUID);
            return ;
        }
        let pos = null;
        let usePos = record.UsePos;
        //--如果位置为空，说明要移动到目标对象上
        if(usePos)
        {
            pos = record.Pos;
            // pos.x = record.Pos.y;
            // pos.z = record.Pos.x;
            // pos.y = FightTool.GetModelHeight(this.mCardID);
        }
        else
        {
            pos = targetRole.GetPosition();
        }

        let speed = record.Speed;
        let skillID = record.SkillID;
        let aspd = record.ASPD;
        this.Sprint(pos, speed, targetRole, skillID, aspd);
    }
    //--处理冲刺结束记录
    ProcessSprintFinishRecord(record)   
    { 
        FightGameObjectManager.getInstance().StopFightRoleMoveBehavior(this.mGameObjectID);
        let pos = {};
        // pos.x = record.Pos.y;
        // pos.z = record.Pos.x;
        // pos.y = FightTool.GetModelHeight(this.mCardID);
        let model = FightGameObjectManager.getInstance().GetModelParent(this.mGameObjectID);
        //GOTool.SetVector(model, TransformOperateType.Position, {x:pos.x,y:pos.y,z:pos.z});
    }
    ProcessPullRecord(record)
    {
        //--根据UUID获取对象，获取对象的位置
        let targetUUID = record.TargetUUID;
            
        let targetRole = FightBehaviorManager.GetRole(targetUUID);
        if(targetRole == null)
        {
            console.error("移动对象不存在targetUUID." + targetUUID);
            return;
        }
        // let pos = {};
        // pos.x = record.Pos.y;
        // pos.z = record.Pos.x;
        // pos.y = FightTool.GetModelHeight(this.mCardID);
        let speed = record.Speed;
        this.Pull(record.Pos, speed);        
    }
    //--处理冲刺结束记录
    ProcessPullFinishRecord(record)   
    { 
        if(this.IsDead)
        {
            return;
        }

        FightGameObjectManager.getInstance().StopFightRoleMoveBehavior(this.mGameObjectID);

        let pos = {};
        // pos.x = record.Pos.y;
        // pos.z = record.Pos.x;
        // pos.y = FightTool.GetModelHeight(this.mCardID);
        let model = FightGameObjectManager.getInstance().GetModelParent(this.mGameObjectID);
        //GOTool.SetVector(model, TransformOperateType.Position, {x:pos.x,y:pos.y,z:pos.z});
    }
    //--处理攻击记录
    ProcessAttackRecord(record)
    {
        if(this.mCardID == 1145040)
        {
            if(record.SkillID == 11450402)
            {
                return;
            }
        }
        let targetUUID = record.TargetUUID;
        let targetRole = FightBehaviorManager.GetRole(targetUUID);
        if(targetRole == null)
        {
            console.error("处理攻击对象不存在targetUUID." + targetUUID);
            return;
        }
        let skillID = record.SkillID;
        let aspd = record.ASPD;
        this.Attack(targetRole, skillID, aspd);
    }
    //--处理站立记录
    ProcessStandRecord(record)
    {
        this.Stand();
    }
    //--处理加血记录
    ProcessAddHPRecord(record)
    {
        let addHP = record.HP;
        this.mHP = BigNumber.add(this.mHP, addHP);
        //--未做血量超出判断
        this.UpdateBlood();
    }
    //--处理受击记录
    ProcessHurtRecord(record)
    {
        let reduceHP = record.Hurt;
        this.mHP = BigNumber.sub(this.mHP, reduceHP);
        if(BigNumber.lessThan(this.mHP, BigNumber.zero))
        {
            this.mHP = BigNumber.zero;
        }

        let hurtSrc    = record.HurtSrc;
        let hurtSrcID  = record.HurtSrcID;

        this.Hurt(hurtSrc, hurtSrcID);
    }
    //--处理前置受击记录
    ProcessPreHurtRecord(record)
    {
        let skillID = record.SkillID;
        this.PreHurt(skillID);
    }
    //--处理添加buff记录
    ProcessAddBuffRecord(record)
    {
        this.AddBuff(record);
    }
    //--处理删除buff记录
    ProcessRmvBuffRecord(record)
    {
        this.RemoveBuff(record);
    }
    //--处理击飞击退Buff记录
    ProcessRepelRecord(record)
    {
        this.AddRepelBuff(record);
    }
    //-- 处理飘字记录
    ProcessHudRecord(record)
    {
        let hudType = record.Type;
        // if(hudType == HudT.MISS)  //-- 闪避
        // {
        //     this.HUD('miss', SMFontColorType.ShanBi);
        // }else if( hudType == HudT.CRIT) //-- 暴击
        // {
        //     this.HUD(TranslateTool.GetText(200107), SMFontColorType.BaoJi);
        // }else if( hudType == HudT.PIMM)  //-- 物免
        // {
        //     this.HUD(TranslateTool.GetText(200640), SMFontColorType.WuLiMianYi);
        // }else if( hudType == HudT.MIMM)  //-- 魔免
        // {
        //     this.HUD(TranslateTool.GetText(200641), SMFontColorType.FaShuMianYi);
        // }
    }
    //--处理闪现记录
    ProcessTeleportRecord(record)
    {
        this.TeleportStart(record);
    }
    //--开始闪现
    TeleportStart(record)
    {
        //--闪现
        let pos   = record.Pos;
        let x = pos.y;
        let z = pos.x;
        let y = FightTool.GetModelHeight(this.mCardID);

        let delay = record.Time / 1000;
        let effectTime = 0.25 ;
        this.mTeleportX  = x;
        this.mTeleportY  = y;
        this.mTeleportZ  = z;
        this.mTeleportSkillID   = record.SkillID;
        //--Hold住镜头
        this.HoldCamera(delay);

        let conf_Teleport_Data = LoadConfig.getConfigData(ConfigName.ShanXianBiaoXian,this.mTeleportSkillID);
        //--表现表不存在，返回
        if(conf_Teleport_Data == null)
        {
            //--旧的闪现表现
            FightGameObjectManager.getInstance().SetFightRoleTeleport(this.mGameObjectID, delay,x, y, z,
            'ShanXian',FightEffectRootType.RootWorld, effectTime);
            return;
        }

        //--开启闪现计时
        this.mTeleportStartTime = (new Date()).getTime();
        this.mTeleportDuration  = delay;
        this.mIsTeleport        = true;

        //--播放闪现前动作
        if(conf_Teleport_Data.BAction != null && conf_Teleport_Data.BAction != '')
        {
            FightGameObjectManager.getInstance().SetFightRolePlayAni(this.mGameObjectID, conf_Teleport_Data.BAction, FightBehaviorManager.GetSpeed(), null);
        }

        //--播放闪现前光效
        if(conf_Teleport_Data.BEffect != null && conf_Teleport_Data.BEffect != '')
        {
            //--临时用加buff光效的代码
            this.AddBuffEffect(conf_Teleport_Data.BEffect, conf_Teleport_Data.BPlace);
            this.mTeleportBEffectFlag = true;
            this.mTeleportBEffectTime = conf_Teleport_Data.BDuration;
            this.mTeleportBEffectStartTime = (new Date()).getTime();;
            this.mTeleportBEffectName = conf_Teleport_Data.BEffect;
        }

    }
    //--闪现结束
    TeleportEnd()
    {

        // GOTool.SetFightRoleVector(this.mGameObjectID, 0, TransformOperateType.Position, 
        //     {x:this.mTeleportX,y:this.mTeleportY,z:this.mTeleportZ});

        let conf_Teleport_Data = LoadConfig.getConfigData(ConfigName.ShanXianBiaoXian,this.mTeleportSkillID);
        //--表现表不存在，返回
        if(conf_Teleport_Data == null)
        {
            return;
        }
        //--播放闪现前动作
        if(conf_Teleport_Data.AAction != null && conf_Teleport_Data.AAction != '')
        {
            FightGameObjectManager.getInstance().SetFightRolePlayAni(this.mGameObjectID, conf_Teleport_Data.Action, FightBehaviorManager.GetSpeed(), WrapMode.Once);
        }

        //--播放闪现光效
        if(conf_Teleport_Data.AEffect != null && conf_Teleport_Data.AEffect != '')
        {
            //--临时用加buff光效的代码
            this.AddBuffEffect(conf_Teleport_Data.AEffect, conf_Teleport_Data.APlace);
            this.mTeleportAEffectFlag = true;
            this.mTeleportAEffectTime = conf_Teleport_Data.ADuration;
            this.mTeleportAEffectStartTime = (new Date()).getTime();
            this.mTeleportAEffectName = conf_Teleport_Data.AEffect;
        }

    }
    //--设置当前显示的镜头
    LookAtCamera(camera)
    {
        FightGameObjectManager.getInstance().SetFightRoleLookAtCamera(this.mGameObjectID, camera);
    }
    //--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--//--战斗角色的镜头跟随变量
    //--是否死亡
    IsDeadFlag()
    {
        return this.IsDead;
    }
    //--是否是士兵
    IsShiBing()
    {
        return this.mRoleType != RoleT.Castle;
    }
    //--是否击飞击退中
    IsRepelFlag()
    {
        return this.mIsRepel;
    }
    //--是否闪现中
    IsTeleportFlag()
    {
        return this.mIsTeleport;
    }

    //--是否冲锋中
    IsSprintFlag()
    {
        return this.mCurStateEnum == StateDef.Sprint;
    }
}
