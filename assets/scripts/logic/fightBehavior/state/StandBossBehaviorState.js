/*
作者（Author）:    skyHuang

描述（Describe）:
*/
/*jshint esversion: 6 */

import StandBehaviorState from "./StandBehaviorState";
import FightBehaviorManager from "../FightBehaviorManager";
import FightGameObjectManager from "../../../view/FightGameObjectManager";

export default class StandBossBehaviorState extends StandBehaviorState
{
    
    //--状态创建
    constructor(role)
    {
        super(role);
    }
    //--状态开启
    Start(param)
    {
        //--Boss分两种，一种是站在盘子上的，一种是精灵，
        //--站在盘子上的是人类，出生的时候要播放丢球动画
        //--精灵站立直接播放站立动画就可以了
        //--人类需要盘子和人物模型都播放站立动画
        //--判断条件是是否存在root/role路径
        let standAniName = 'stand';
        let speed = FightBehaviorManager.GetSpeed();
        let mode = 0;
        let childPath = 'root/role';
        FightGameObjectManager.getInstance().SetFightRoleStandBehavior(this.mRole.mGameObjectID, standAniName, speed, mode, childPath);
    }
}
