/*
作者（Author）:    skyHuang

描述（Describe）.
*/
/*jshint esversion: 6 */

import FightBehaviorManager from "../FightBehaviorManager";
import DieBehaviorState from "./DieBehaviorState";
import { FightSystemType, FightCampType, FightEffectRootType } from "../../fight/FightDef";
import FightGameObjectManager from "../../../view/FightGameObjectManager";
import FightTool from "../FightTool";

export default class DieBossBehaviorState extends DieBehaviorState
{
    //--状态创建
    constructor(role)
    {
        super(role);
    }
    //--状态开启
    Start(param)
    {
        this.mStop = false;
        this.mDieEffectFlag = false;
        this.mDieEffectShow = false;

        //--停止动作
        this.mRole.StopAni();

        this.mIsTriggerTimeReward = false;
        this.mIsTriggerPassReward = false;
        this.mIsTriggerDungeonReward = false;
        //--掉落判断，小兵不用了，只有boss需要
        let rewardEffectName = "";

        if(FightBehaviorManager.mFightType == FightSystemType.GuanQia && this.mRole.mCampType == FightCampType.Enemy )
        {
            if(GuanQiaPart.IsTriggerTimeReward())
            {
                if(this.UseOldStyle())
                {
                    rewardEffectName = "SiWang_4";
                }
                this.mIsTriggerTimeReward = true;
                GuanQiaPart.FinishTimeReward();
            }
            if(GuanQiaPart.IsTriggerPassReward())
            {
                if(this.UseOldStyle())
                {
                    rewardEffectName = "SiWang_4";
                }
                this.mIsTriggerPassReward = true;
                GuanQiaPart.FinishPassReward();
            }
        }else if(FightBehaviorManager.mFightType == FightSystemType.DiXiaCheng && this.mRole.mCampType == FightCampType.Enemy)
        {
            if(DungeonPart.IsTriggerPassReward())
            {
                if(this.UseOldStyle())
                {
                    rewardEffectName = "SiWang_4";
                }
                this.mIsTriggerDungeonReward = true;
            }
        }

        //--普通怪播放死亡光效1.2秒，0.3秒后消失
        let id = this.mRole.mGameObjectID;
        let effectName = this.GetDieEffect();
        let isBoss = false;
        let rootType = FightEffectRootType.RootWorld;
        let duration = 1.1 / FightBehaviorManager.GetSpeed();
        let missTime = 0.9 / FightBehaviorManager.GetSpeed();
        let aniName = 'die';
        let rolePath = 'root/role';
        let speed = FightBehaviorManager.GetSpeed();
        let mode = null;
        let effectID = FightGameObjectManager.getInstance().SetFightRoleDieBehavior(id, effectName, isBoss, rootType, duration, missTime,
                aniName, rolePath, speed, mode, rewardEffectName);

        let conf_ShiBing_Data = LoadConfig.getConfigData(ConfigName.ShiBing,this.mRole.mCardID);
        let moXingID = conf_ShiBing_Data.ModelID;
        this.mDieEffectData = LoadConfig.getConfigData(ConfigName.SiWangBiaoXian,moXingID);
        if(this.mDieEffectData != null)
        {
            this.mDieEffectFlag = true;
            let d = new Date();
            this.mDieEffectEndTime = d.getTime();
            if(this.mDieEffectData.Action != '')
            {
                this.mRole.PlayAni(this.mDieEffectData.Action, FightBehaviorManager.GetSpeed(), null);
            }
        }
        
        //------------------------关卡掉落相关-------------------------
        if(this.UseOldStyle() && (this.mIsTriggerTimeReward || this.mIsTriggerPassReward))
        {
            //--旧版飘道具
            //this.ShowReward(effectID);
        }
        else if(!this.UseOldStyle() && (this.mIsTriggerTimeReward || this.mIsTriggerPassReward))
        {
            //--新版飘道具
            //let types, ids, counts = this.GetGuanQiaReward();
            //MoveItemPanel.Show(MoveItemPanel.TargetType.Item, id, 0.5 / FightBehaviorManager.GetSpeed(), 0.5 / FightBehaviorManager.GetSpeed(), types, ids, counts);
        }

        //------------------------副本掉落相关-------------------------
        if(this.UseOldStyle() && this.mIsTriggerDungeonReward )
        {
            //--旧版飘道具
            //this.ShowReward(effectID);
        }
        else if(!this.UseOldStyle() && this.mIsTriggerDungeonReward)
        {
            //--新版飘道具
            //let types, ids, counts = this.GetDungeonReward();
            //MoveItemPanel.Show(MoveItemPanel.TargetType.Item, id, 0.5 / FightBehaviorManager.GetSpeed(), 0.5 / FightBehaviorManager.GetSpeed(), types, ids, counts);
        }
    }
    
    //--状态运行
    Run()
    {
        if(! this.mDieEffectFlag || this.mDieEffectData == null)
        {
            return;
        }

        if(!this.mDieEffectShow && 
        FightTool.GetTimeGap(this.mDieEffectStartTime) > this.mDieEffectData.EShowTime && 
        this.mDieEffectData.Ename != '')
        {
            this.mRole.AddBuffEffect(this.mDieEffectData.Ename, this.mDieEffectData.Eposition);
            this.mDieEffectShow = true;
            let d = new Date();
            this.mDieEffectEndTime = d.getTime();
        }

        if(this.mDieEffectShow && 
        FightTool.GetTimeGap(this.mDieEffectEndTime) > this.mDieEffectData.Eduration && 
        this.mDieEffectData.Ename != '')
        {
            this.mRole.RemoveBuffEffect(this.mDieEffectData.Ename);
        }
    } 
    GetDieEffect()
    {
        return '';
    }
}
