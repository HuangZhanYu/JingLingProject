/*
作者（Author）:    skyHuang

描述（Describe）.
*/
/*jshint esversion: 6 */

import BehaviorState from "./BehaviorState";
import FightBehaviorManager from "../FightBehaviorManager";
import { FightEffectRootType } from "../../fight/FightDef";
import FightGameObjectManager from "../../../view/FightGameObjectManager";
import FightTool from "../FightTool";

export default class DieBehaviorState extends BehaviorState
{
    //--状态创建
    constructor(role)
    {
        super(role);
        this.mDieEffectData = null;          //--死亡光效数据
        this.mDieEffectFlag = false;        //--死亡光效是否需要  
        this.mDieEffectStartTime = 0;       //--死亡光效开始时间
        this.mDieEffectShow = false;        //--死亡光效是否显示
        this.mDieEffectEndTime = 0;         //--死亡光效结束时间
        this.mIsTriggerTimeReward = false;  //--是否触发时间奖励
        this.mIsTriggerPassReward = false;  //--是否触发通关奖励
        this.mUseOldStyle = true;           //--使用旧的道具显示模式
        this.mIsTriggerDungeonReward = false;//--是否触发副本通关奖励
    }
    //--状态开启
    Start(param)
    {
        this.mStop = false;
        this.mDieEffectFlag = false;
        this.mDieEffectShow = false;

        //--停止动作
        this.mRole.StopAni();

        this.mIsTriggerTimeReward = false;
        this.mIsTriggerPassReward = false;

        //--掉落判断，小兵不用了，只有boss需要
        let rewardEffectName = "";

        //--普通怪播放死亡光效1.2秒，0.3秒后消失
        let id = this.mRole.mGameObjectID;
        let effectName = this.GetDieEffect();
        let isBoss = false;
        let rootType = FightEffectRootType.RootWorld;
        let duration = 0.5 / FightBehaviorManager.getInstance().GetSpeed();
        let missTime = 0.3 / FightBehaviorManager.getInstance().GetSpeed();
        let aniName = '';
        let rolePath = '';
        let speed = 1;
        let mode = null;
        let effectID = FightGameObjectManager.getInstance().SetFightRoleDieBehavior(id, effectName, isBoss, rootType, duration, missTime, aniName, rolePath, speed, mode, rewardEffectName);
        
        let conf_ShiBing_Data = LoadConfig.getConfigData(ConfigName.ShiBing,this.mRole.mCardID);
        let moXingID = conf_ShiBing_Data.ModelID;
        this.mDieEffectData = LoadConfig.getConfigData(ConfigName.SiWangBiaoXian,moXingID);
        if(this.mDieEffectData != null)
        {
            this.mDieEffectFlag = true;
            this.mDieEffectStartTime = (new Date()).getTime();
            if(this.mDieEffectData.Action != '')
            {
                this.mRole.PlayAni(this.mDieEffectData.Action, FightBehaviorManager.getInstance().GetSpeed(), null);
            }
        }

        //--判断是否需要飘金币，新版
        if(!this.UseOldStyle() && FightBehaviorManager.getInstance().mFightType == FightSystemType.GuanQia && this.mRole.mCampType == FightCampType.Enemy)
        {
            //MoveItemPanel.Show(MoveItemPanel.TargetType.Gold, id, 0.4 / FightBehaviorManager.getInstance().GetSpeed(), 0.5 / FightBehaviorManager.getInstance().GetSpeed());
        }
    }
    //--设置攻击模型的方向是否面相被攻击对象
    GetOrientation()
    {
        if(this.mConf_JiNeng == null)
        {
            return true;
        }

        //--设置面相目标，如果是穿透射击则面相攻击方向的正前方
        if(this.mConf_JiNeng.ShowType == FightShowType.Shot)
        {
            let conf_FlyItem_Data = LoadConfig.getConfigData(ConfigName.FeiXingDaoJu,this.mConf_BiaoXian.FlyItemID);
            //-- 穿透射击
            if(conf_FlyItem_Data != null && conf_FlyItem_Data.FlyType == FightFlyItemType.Cross)
            {
                return false;
            }else
            {
                return true;
            }
        }

        if(this.mConf_JiNeng.ShowType == FightShowType.Beam)
        {
            return false;
        }

        return true;
    }
    //--状态运行
    Run()
    {
        if(! this.mDieEffectFlag || this.mDieEffectData == null)
        {
            return;
        }

        if(!this.mDieEffectShow && 
        FightTool.GetTimeGap(this.mDieEffectStartTime) > this.mDieEffectData.EShowTime && 
        this.mDieEffectData.Ename != '')
        {
            this.mRole.AddBuffEffect(this.mDieEffectData.Ename, this.mDieEffectData.Eposition);
            this.mDieEffectShow = true;
            let d = new Date();
            this.mDieEffectEndTime = d.getTime();
        }

        if(this.mDieEffectShow && 
        FightTool.GetTimeGap(this.mDieEffectEndTime) > this.mDieEffectData.Eduration && 
        this.mDieEffectData.Ename != '')
        {
            this.mRole.RemoveBuffEffect(this.mDieEffectData.Ename);
        }
    } 
    //--旧有方式，现在留给没有更新的iphone版本
    ShowReward(effectID)
    {

        let rewardEffectObj = FightGameObjectManager.getInstance().GetEffect(effectID);

        if(!rewardEffectObj)
        {
            return;
        }

        //-- 掉落道具
        if(FightBehaviorManager.mFightType == FightSystemType.DiXiaCheng)
        {
            let[types, ids, counts] = this.GetDungeonReward();
        }
        else if(FightBehaviorManager.mFightType == FightSystemType.GuanQia)
        {
            let[types, ids, counts]  = this.GetGuanQiaReward();
        }
        
        let arr = R&&omTool.GetArr(6);
        let use = [];  //-- 已经使用的下标数组

        //-- 显示需要的
        for (let i = 0; i < types.length; i++) 
        {
            if(i > 6)
            {
                break;
            }
            let typ = types[i];
            let id = ids[i];
            let count = counts[i];
            let index = arr[i];
            // let root = this.PanleFindChild(rewardEffectObj, "SiWang_4/baoxiang_diaoluo/root/Bip0" + index + "/baoxiang0" + index, false);
            // if(GOTool.IsDestroy(root))
            // {
            //     let daoJu = GOTool.GetChild(root, index);
            //     if(GOTool.IsDestroy(daoJu))
            //     {
            //         daoJu = newObject(UIManager.GetCommonPrefabs(CommonPrefabEnum.DaoJuPrefab));
            //         LayerTool_ChangeTotalLayer(daoJu, 'Role');
            //         GOTool.SetParent(daoJu, root);
            //         GOTool.SetVector(daoJu, TransformOperateType.LocalPosition, { x : 0, y : 0, z : 0 });
            //         GOTool.SetVector(daoJu, TransformOperateType.LocalEulerAngles, { x : 0, y : 0, z : 0 });
            //         GOTool.SetVector(daoJu, TransformOperateType.LocalScale, { x : 0.015, y : 0.015, z : 0.015 });
            //     }
            //     DaoJuTool.SetDaoJuInfo(daoJu, MainPanel.GetLuaBehvaiour(), typ, id, count);
            //     root.active = true;
            //     //-- 面相镜头
            //     GOTool.AddLookAt(daoJu, FightBehaviorManager.GetCamera());
            // }

            //-- 获取已经使用的下标
            use[index] = true;
        }

        //-- 隐藏不需要的
        for(let j = 1; j <= 6 ; j++)
        {
            if(use[j] == null)
            {
                // let root = PanleFindChild(rewardEffectObj, "SiWang_4/baoxiang_diaoluo/root/Bip0" + j + "/baoxiang0" + j, false);
                // if(! GOTool.IsDestroy(root))
                // {
                //     root.active = false;
                // }
            }
        }
    }
    GetDieEffect()
    {
        //--死亡光效
        let effectName = 'SiWang_2';

        //--新版的修改了死亡光效，只有苹果需要保留
        if(!this.UseOldStyle())
        {
            return effectName;
        }

        //-- 获取对象位置
        let pos = this.mRole.GetPosition();

        if(FightBehaviorManager.mFightType == FightSystemType.GuanQia && this.mRole.mCampType == FightCampType.Enemy)
        {
            //--主线战斗，毕竟并且是敌军，用掉落金币光效
            if(pos.y > 1 )
            {
                effectName = 'SiWang_3';//--空中兵用_3;
            }else
            {
                effectName = 'SiWang';
            }
        }
        return effectName;
    }
    //--状态停止
    Stop()
    {
        let id = this.mRole.mGameObjectID;
        FightGameObjectManager.getInstance().StopFightRoleDieBehavior(id);
        this.mStop = true;
    }

    //--获取掉落奖励
    GetGuanQiaReward()
    {
        if(this.mIsTriggerTimeReward)
        {
            let[types, ids, counts]  = GuanQiaPart.GetTimeReward();
        }
        else if(this.mIsTriggerPassReward)
        {
            let[types, ids, counts]  = GuanQiaPart.GetPassReward();
        }
        else
        {
            let[types, ids, counts]  = GuanQiaPart.GetTimeReward();
            let type2s, id2s, count2s = GuanQiaPart.GetPassReward();

            for(let i = 0; i < type2s.length; i++)
            {
                types.push(type2s[i]);
                ids.push(id2s[i]);
                counts.push(count2s[i]);
            }
        }
        return [types, ids, counts] ;
    }

    //--获取副本奖励
    GetDungeonReward()
    {
        let[types, ids, counts]  = DungeonPart.GetPassReward();
        return [types, ids, counts] ;
    }

    //--是否使用旧的显示效果
    UseOldStyle()
    {
        return this.mUseOldStyle;
    }
}
