/*
作者（Author）:    skyHuang

描述（Describe）.
*/
/*jshint esversion: 6 */

import BehaviorState from "./BehaviorState";
import FightBehaviorManager from "../FightBehaviorManager";
import FightGameObjectManager from "../../../view/FightGameObjectManager";
import { FightDef, FightShowType, FightFlyItemType } from "../../fight/FightDef";

export default class SprintBehaviorState extends BehaviorState
{
    
    //--状态创建
    constructor(role)
    {
        super(role);
        this.mPosition       = null;  //--移动位置
        this.mSpeed          = 0;    //--移动速度
        this.mSkillID        = 0;    //--技能
        this.mASPD           = 0;
        this.mConf_BiaoXian  = null;  //--技能表现配置
        this.mConf_JiNeng    = null;  //--技能配置
        this.mEffect         = null;  //--技能光效
        this.mEffectName     = null;  //--技能光效名
    }
    //--状态开启
    Start(param)
    {
        this.mStop = false;
        this.mPosition  = param.Position;
        this.mSpeed     = param.Speed;

        this.mTargetRole    = param.TargetRole;
        this.mSkillID       = param.SkillID;
        this.mASPD          = param.ASPD;
        //--获取技能配置
        this.mConf_BiaoXian = LoadConfig.getConfigData(ConfigName.JiNeng_BiaoXian,this.mSkillID);
        if(this.mConf_BiaoXian == null)
        {
            console.log("AttackBehaviorState this.mSkillID."+this.mSkillID);
            return;
        }
        this.mConf_JiNeng = LoadConfig.getConfigData(ConfigName.JiNeng,this.mSkillID);

        //--设置攻击方向
        //--攻击动作
        //--攻击光效
        //--攻击音效

        let id = this.mRole.mGameObjectID;
        let target = this.mTargetRole.mGameObjectID;
        let lookAtTarget = this.GetOrientation();
        let useX = this.GetUseX();

        //--动作
        let aniName = this.mConf_BiaoXian.Action;
        let aniSpeed = FightBehaviorManager.GetSpeed();
        let mode = 0;

        //--光效
        let effectName = this.mConf_BiaoXian.SkillEffect;
        let duration = this.mConf_BiaoXian.Duration;
        let rootType = this.mConf_BiaoXian.SkillEffectPlace;
        //--音效
        let soundName = "";
        //--播放攻击
        FightGameObjectManager.getInstance().SetFightRoleAttackBehavior(id,lookAtTarget, target, 
        effectName, aniSpeed, mode,
        effectName, duration, rootType,
        soundName, useX);
        
        //--播放移动动画
        let x = this.mPosition.x;
        let y = this.mPosition.y;
        let z = this.mPosition.z;
        FightGameObjectManager.getInstance().SetFightRoleMoveBehavior(id, effectName, aniSpeed, mode, x, y, z);
    }
    //--状态运行
    Run()
    {
        //let rolePosition = this.mRole.GetPosition();

        //--旋转
        //--设置移动
        let id = this.mRole.mGameObjectID;
        let realSpeed = this.mSpeed * FightBehaviorManager.GetSpeed();
        let x = this.mPosition.x;
        let y = this.mPosition.y;
        let z = this.mPosition.z;
        FightGameObjectManager.getInstance().SetFightRoleMoveBehaviorRun(id, this.mSpeed, FightDef.FREQUENCY, realSpeed, x, y, z);
    }

    //--状态停止
    Stop()
    {
        //--停止移动
        let id = this.mRole.mGameObjectID;
        FightGameObjectManager.getInstance().StopFightRoleMoveBehavior(id);
    }
    //--设置方向是否面相对象
    GetOrientation()
    {
        if(this.mConf_JiNeng == null)
        {
            return true;
        }

        //--设置面相目标，如果是穿透射击则面相攻击方向的正前方
        if(this.mConf_JiNeng.ShowType == FightShowType.Shot)
        {
            let conf_FlyItem_Data = LoadConfig.getConfigData(ConfigName.FeiXingDaoJu,this.mConf_BiaoXian.FlyItemID);
            //-- 穿透射击
            if(conf_FlyItem_Data != null && conf_FlyItem_Data.FlyType == FightFlyItemType.Cross)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        if(this.mConf_JiNeng.ShowType == FightShowType.Beam )
        {
            return false;
        }

        return true;
    }

    //--攻击光效是否要面相被攻击对象还是面相正前方
    GetUseX()
    {
        if(this.mConf_JiNeng == null)
        {
            return true;
        }

        if(this.mConf_JiNeng.ShowType == FightShowType.Beam)
        {
            return false;
        }

        return true;
    }
}
