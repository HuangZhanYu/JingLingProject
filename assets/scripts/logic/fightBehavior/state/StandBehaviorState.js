/*
作者（Author）:    skyHuang

描述（Describe）.
*/
/*jshint esversion: 6 */

import BehaviorState from "./BehaviorState";
import FightGameObjectManager from "../../../view/FightGameObjectManager";
import FightBehaviorManager from "../FightBehaviorManager";

export default class StandBehaviorState extends BehaviorState
{
    
    //--状态创建
    constructor(role)
    {
        super(role);
    }
    //--状态开启
    Start(param)
    {
        //--播放站立动画
        let standAniName = 'stand';
        let speed = FightBehaviorManager.GetSpeed();
        let mode = 0;
        let childPath = '';
        FightGameObjectManager.getInstance().SetFightRoleStandBehavior(this.mRole.mGameObjectID, standAniName, speed, mode, childPath);
    }
}
