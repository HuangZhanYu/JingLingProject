/*
作者（Author）:    skyHuang

描述（Describe）.
*/
/*jshint esversion: 6 */

import BehaviorState from "./BehaviorState";
import FightGameObjectManager from "../../../view/FightGameObjectManager";
import FightBehaviorManager from "../FightBehaviorManager";
import { FightDef } from "../../fight/FightDef";

export default class PullBehaviorState extends BehaviorState
{
    
    //--状态创建
    constructor(role)
    {
        super(role);
        this.mPosition   = null;  //--移动位置
        this.mSpeed      = 0;   //--移动速度
    }
    Start(param)
    {
        //--先停止移动
        let id = this.mRole.mGameObjectID;
        FightGameObjectManager.getInstance().StopFightRoleMoveBehavior(id);

        this.mStop = false;
        this.mPosition  = param.Position;
        this.mSpeed     = param.Speed;
        //--旋转方向
        //--播放移动动画
        let runAniName = 'stand';
        let speed = FightBehaviorManager.GetSpeed();
        let mode = 0;
        let x = this.mPosition.x;
        let y = this.mPosition.y;
        let z = this.mPosition.z;
        FightGameObjectManager.getInstance().SetFightRoleMoveBehavior(id, runAniName, speed, mode, x, y, z);
    }
    //--状态运行
    Run()
    {
        //--旋转
        //--设置移动
        let id = this.mRole.mGameObjectID;
        let realSpeed = this.mSpeed * FightBehaviorManager.GetSpeed();
        let x = this.mPosition.x;
        let y = this.mPosition.y;
        let z = this.mPosition.z;
        FightGameObjectManager.getInstance().SetFightRoleMoveBehaviorRun(id, this.mSpeed, FightDef.FREQUENCY, realSpeed, x, y, z);
    }

    //--状态停止
    Stop()
    {
        //--停止移动
        let id = this.mRole.mGameObjectID;
        FightGameObjectManager.getInstance().StopFightRoleMoveBehavior(id);
    } 

}
