/*
作者（Author）:    skyHuang

描述（Describe）:
*/
/*jshint esversion: 6 */
export default class BehaviorState
{
    constructor(role)
    {
        this.mRole = role;//--角色
        this.mStop = false;//--是否停止
    }
    //--状态开启
    Start(param)
    {  
    }
    // --状态运行
    Run()
    {
    }
    // --是否能够停止
    CanStop()
    {
    }
    // --状态停止
    Stop()
    {
    }
    
}
