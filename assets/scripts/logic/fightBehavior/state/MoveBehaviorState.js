/*
作者（Author）:    skyHuang

描述（Describe）.
*/
/*jshint esversion: 6 */

import BehaviorState from "./BehaviorState";
import FightGameObjectManager from "../../../view/FightGameObjectManager";
import FightBehaviorManager from "../FightBehaviorManager";
import { FightDef } from "../../fight/FightDef";

export default class MoveBehaviorState extends BehaviorState
{
    
    //--状态创建
    constructor(role)
    {
        super(role);
        this.mPosition   = null;  //--移动位置
        this.mSpeed      = 0;   //--移动速度
    }
    Start(param)
    {
        this.mStop = false;
        this.mPosition  = param.Position;
        this.mSpeed     = param.Speed;
        //--旋转方向
        //--播放移动动画
        let id = this.mRole.mGameObjectID;
        let runAniName = 'run';
        let speed = FightBehaviorManager.GetSpeed();
        let mode = 0;

        FightGameObjectManager.getInstance().SetFightRoleMoveBehavior(id, runAniName, speed, mode, this.mPosition.x, this.mPosition.y);
    }
    //--状态运行
    Run()
    {
        //--离开战场的判定
        let rolePosition = this.mRole.GetPosition();
        if(rolePosition.x >= 325 && this.mPosition.x > rolePosition.x)
        {
            this.mStop = true;
            return;
        }
        if(rolePosition.x <= -325 && this.mPosition.x < rolePosition.x)
        {
            this.mStop = true;
            return;
        }
        //--旋转
        //--设置移动
        let id = this.mRole.mGameObjectID;
        let realSpeed = this.mSpeed * FightBehaviorManager.GetSpeed();
   
        FightGameObjectManager.getInstance().SetFightRoleMoveBehaviorRun(id, this.mSpeed, FightDef.FREQUENCY, realSpeed, this.mPosition.x, this.mPosition.y);
    }

    //--状态停止
    Stop()
    {
        //--停止移动
        let id = this.mRole.mGameObjectID;
        FightGameObjectManager.getInstance().StopFightRoleMoveBehavior(id);
    } 

}
