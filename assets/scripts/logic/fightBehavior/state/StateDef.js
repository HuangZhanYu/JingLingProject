/*
作者（Author）:    skyHuang

描述（Describe）:
*/
/*jshint esversion: 6 */

export const StateDef = {
    Attack  : 1,    //--攻击
    Die     : 2,    //--死亡
    Move    : 3,    //--移动
    Stand   : 4,    //--站立
    Sprint  : 5,    //--冲刺
    Pull    : 6,    //--被拉扯
};

