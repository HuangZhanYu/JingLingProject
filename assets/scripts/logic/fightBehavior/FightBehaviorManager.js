/*
作者（Author）:    jason

描述（Describe）: 战斗表现管理器，处理所有的战斗表现

设计要点：1.对战斗所需的资源进行加载，模型，音效，预设等
         2.根据战斗逻辑管理器的指令，控制资源的行为，如模型攻击、行走，飞行道具的飞行，音效播放，光效显示
         3.战斗镜头的切换和点击以及屏蔽也由战斗表现管理器负责
*/
/*jshint esversion: 6 */
import FightGameObjectManager from "../../view/FightGameObjectManager";
import FightResourceManager from "./FightResourceManager";
import { FightSystemType, RoleT, FightCampType, FightDef } from "../fight/FightDef";
import FightBossRole from "./role/FightBossRole";
import SceneManager from "../../manager/SceneManager";
import FightNormalRole from "./role/FightNormalRole";
import FightTool from "./FightTool";
import GFightManager from "../fight/GFightManager";
import BigNumber from "../../tool/BigNumber";
import TimerManager from "../../tool/TimerManager";
import { RecordT } from "../fight/FightRecord";
import AutoDestroy from "../../view/AutoDestroy";
import FlyItemBehaviorManager from "./FlyItem/FlyItemBehaviorManager";

let FightBehaviorManager = (function(){

    return{
        // 初始化战斗表现管理器
        Init()
        {
            // 初始化资源加载
            FightGameObjectManager.getInstance().Init();
            //设置模型缓存关卡数，50关的话，iphone6以下的机子容易内存不足
            //内存1024M以下的设置
            // if(UnityEngine.SystemInfo.systemMemorySize <= 1024)
            //     FightBehaviorManager.mCacheModelFightCount = 1      //不缓存模型
            //     FightBehaviorManager.mUnloadResourceFightCount = 10 //10关释放一次内存
            // 
            FightGameObjectManager.getInstance().SetClearModelTime(FightBehaviorManager.mCacheModelFightCount);
            // 注册界面变化回调
            //UIManager.RegisterPanelChangeFunc(FightBehaviorManager.OnPanelChange)

            // 公共战斗界面的统计弹窗回调
            // 这个是由于公共战斗界面的统计弹窗不在UIManager内，而是公共战斗界面的一个子界面，
            // 所以没法用FightBehaviorManager.OnPanelChange控制，另外使用了一个控制函数注册进去
            //CommonFightPanel.RegistDamageOpenCloseFunction("FightBehaviorManager.OnCommonFightChange", FightBehaviorManager.OnCommonFightChange)
        },
        SetGuanQiaInit()
        {
            FightBehaviorManager.mIsInitGuanQia = true;
            FightBehaviorManager.mLastUpdateTime = (new Date()).getTime(); + FightBehaviorManager.mErrorTimeGap;
            //LoadingPanel.Show()
        },
        //界面变换回调
        OnPanelChange(panelType, panelEnum)
        {

        },
        // 加载战斗资源
        // sceneName 场景名称
        // myTeam 我方队伍
        // enemyTeam 敌方队伍
        // func 加载完资源回调
        LoadFightResource(fightType, sceneName, duration, myTeam, enemyTeam, func)
        {
            if(!FightBehaviorManager.IsEnd)
                this.StopFight();
            //一场战斗是否开启
            FightBehaviorManager.mIsFighting = true
            //是否在加载资源中
            FightBehaviorManager.mIsLoadingRes = true
            //当前战斗标记
            FightBehaviorManager.mFightingSystemType = fightType
            //检测不加速的战斗，暂停加速
            //this.StopSpeed()
            
            //检测光效屏蔽
            //this.ChangeShieldEffectFlagOnFight()
            
            //战斗下标+1，该数值用来避免短时间内多次进行战斗资源加载，导致错误的资源进行一场战斗
            FightBehaviorManager.mFightIndex = FightBehaviorManager.mFightIndex + 1
            //设置战斗类型
            FightBehaviorManager.mFightType = fightType
            //设置战斗时长
            FightBehaviorManager.mDuration = duration / 1000
            // 我军阵容信息
            FightBehaviorManager.mMyTeam = myTeam
            // 敌军阵容信息
            FightBehaviorManager.mEnemyTeam = enemyTeam
            // 资源加载完毕
            FightBehaviorManager.mLoadFinishFunc = func
            // 加载场景
            // 如果当前战斗界面被全屏界面挡住，不加载场景，退出场景会调用恢复场景
            // if(!FightBehaviorManager.IsFullPanel)
            //     // 播放战斗背景音乐
            //     FightBehaviorManager.PlayMusic()
            //     // 显示加载黑幕加载场景
            //     FightShelterPanel.Show( function()
            //         SceneManager.ShowScene(sceneName, function()
            //             FightBehaviorManager.OnSceneLoad(FightBehaviorManager.mFightIndex)
            //          )
            //      )
            // else
                //不显示黑幕加载场景
                this.OnSceneLoad(FightBehaviorManager.mFightIndex);
            //
        },
        StopFight()
        {
            TimerManager.removeTimeCallBack(this.timeKey);
            //战斗后还原加速信息
            this.ResumeSpeed();
        
            //停止战斗逻辑
            GFightManager.Pause();
        
            //回收镜头
            //SceneManager.RecycleOrigin()
            
            //回收角色所有buff光效
            for(let key in FightBehaviorManager.mAllRole)
            {
                let role = FightBehaviorManager.mAllRole[key];
                //回收所有Buff
                role.ReleaseBuff();
            }
        
            //回收所有飞行道具
            //LuaFramework.BehaviorFlyItemManager.GetInstance().Release()
            //回收所有光效
            AutoDestroy.ReleaseAll();
        
            //回收所有模型
            FightGameObjectManager.getInstance().ReleaseAll();
            FightBehaviorManager.mAllRole = {};
            
            //清理时间
            FightBehaviorManager.mTimePass = 0;
        
            // // 关闭宝箱  //新需求，宝箱没有领取的时候，战斗结束不关闭宝箱
            // if(FightBehaviorManager.mFightType == FightSystemType.GuanQia)
            //     GuanQiaPart.CloseBaoXiang()
            // 
        
            //新手战斗关闭
            FightBehaviorManager.mIsNewbieFight = false;
            //清理当前战斗系统标记
            FightBehaviorManager.mFightingSystemType = null;
            //清理有人死亡的标记
            FightBehaviorManager.mDeadFlag = false;
            //一场战斗停止
            FightBehaviorManager.mIsFighting = false;
        },

        //战斗前检测当前战斗类型是否能使用加速 竞技场。战斗塔。精通塔不能加速
        StopSpeed()
        {
            //如果不能加速，则不加速
            if(!this.GetStopSpeedState())
                SpeedUpPart.StopOnFighting();
            
        },
        //战斗后还原加速信息
        ResumeSpeed()
        {
            //如果不能加速，则不加速
            if(!this.GetStopSpeedState())
                SpeedUpPart.StartAfterFighting();
            
        },
        //根据当前战斗类型读表判断是否可以使用加速
        GetStopSpeedState()
        {
            let peiZhiID = 0;
            if(FightBehaviorManager.mFightingSystemType == FightSystemType.Arena)
                peiZhiID = 2;
            else if(FightBehaviorManager.mFightingSystemType == FightSystemType.FightTower)
                peiZhiID = 10;
            else if(FightBehaviorManager.mFightingSystemType == FightSystemType.Plateau)
                peiZhiID = 14;
            else if(FightBehaviorManager.mFightingSystemType == FightSystemType.DiXiaCheng)
                peiZhiID = 3;
            else
                peiZhiID = 1;
            
            let setData = LoadConfig.getConfigData(ConfigName.ZhanDouPeiZhi,peiZhiID);//Conf_ZhanDouPeiZhi[peiZhiID]
            if(setData == null)
                return false;
            
            return setData.UseSpeed == 1;
        },
        //加载场景回调
        OnSceneLoad(fightIndex)
        {
            if(FightBehaviorManager.DebugBehavior)
            {
                FightBehaviorManager.LoadResourceFinished(fightIndex);
                return;
            }

            // 初始化场景信息
            this.InitScene();
            // 我方队伍卡牌ID
            let myTeamCardIDs = FightBehaviorManager.mMyTeam;
            // 敌方队伍卡牌ID
            let enemyTeamCardIDs = FightBehaviorManager.mEnemyTeam;

            // 当我方阵容改变时，重新加载两队的资源
            FightResourceManager.LoadAllTeam(myTeamCardIDs, enemyTeamCardIDs,FightBehaviorManager.LoadResourceFinished, fightIndex);
        },
        InitScene()
        {
            // // 获取我军出生点 用于控制我军全体人员阵亡时，镜头返回
            FightBehaviorManager.mMyBornObj = SceneManager.SceneOrigin;
            // // 设置镜头可拖动的最小值，由于场景是越远数值越负，所以用0去减，30是一个偏移量
            // SceneManager.SetFollowCameraMin(-40)
            // SceneManager.SetFollowCameraMax(40)
            // 根据当前加速信息设置镜头移动速度
            SceneManager.SetFollowCameraSpeed(FightBehaviorManager.GetSpeed());
        },
        //资源完成加载回调
        LoadResourceFinished(fightIndex)
        {
            //登陆场景判断
            // let curPanelData = UIManager.GetCurPanelData()
            // if(curPanelData == null || (curPanelData.Type == PanelType.Panel && curPanelData.Enum == PanelEnum.Login))
            //     return;
            
            // // 战斗下标不对，不进行战斗
            // if(fightIndex != FightBehaviorManager.mFightIndex)
            // {
            //     // 如果在捉宠，不需要加载过场动画
            //     if(!FightBehaviorManager.IsFullPanel)
            //         FightShelterPanel.Close()

            //     return;
            // }
                
            

            FightBehaviorManager.mIsLoadingRes = false;

            // 资源初始化完成
            // 打开战斗场景
            //SceneManager.ShowFightOrigin()
            // 重置镜头
            FightBehaviorManager.ResetCamera();

            // 如果在捉宠，不需要加载过场动画
            //if(FightBehaviorManager.IsFullPanel)
                // 开启一场战斗
                FightBehaviorManager.StartFight(fightIndex);
            // else
            // {
            //     // 关闭过场动画
            //     FightShelterPanel.Close( function(){
            //         // 打开战斗镜头
            //         SceneManager.SetLogicActive(true)
            //         FightBehaviorManager.StartFight(fightIndex)
            //     })
            // }
            SceneManager.SetLogicActive(true);   
            
            
            FightBehaviorManager.mFightCount = FightBehaviorManager.mFightCount + 1
            // 战斗次数到达一定次数就施放
            // if(FightBehaviorManager.mFightCount >= FightBehaviorManager.mUnloadResourceFightCount)
            // {
            //     FightBehaviorManager.mFightCount = 0
            //     // 资源加载完毕，清理内存，该函数五分钟内只能调用一次，在主界面才释放资源
            //     resMgr:UnloadResource()
            // }
            
        },
        StartFight(fightIndex)
        {
            //如果有loading就关掉，避免挡住
            // if(LoadingPanel.IsShow())
            // {
            //     FightBehaviorManager.mIsInitGuanQia = false
            //     LoadingPanel.Close()
            // }
                
            
        
            // 战斗下标不对，不进行战斗
            if(fightIndex != FightBehaviorManager.mFightIndex)
                return;
            
            
            //设置战斗开始;
            FightBehaviorManager.IsEnd = false;
            //置0帧数
            FightBehaviorManager.mFrameCount = 0;
            //清理大数字
            BigNumber.clearFormulaCache();
            // 开启一场战斗
            // if(NewbiePanel.IsNewbieFight())
            //     // 新手战斗特殊判断，先进行新手引导展示
            //     FightBehaviorManager.GuideFightStep1_1()
            // else
            {
                //开始战斗
                if(FightBehaviorManager.mLoadFinishFunc != null)
                {
                    FightBehaviorManager.mLoadFinishFunc();
                }
                    

                //AbilityPart.CheckNotEffectAbility();
            }
        
            //timerMgr:AddUpdateFunc('FightBehaviorManager.Update',FightBehaviorManager.Update);
            this.timeKey = TimerManager.addAlwayTimeCallBack(0.1, FightBehaviorManager.Update);
            //主线战斗特殊处理
            if(FightBehaviorManager.mFightType == FightSystemType.GuanQia)
            {
                // // // 检查是否有宝箱显示
                // // GuanQiaPart.ShowBaoXiang()  //以移到了GFightManager的GFightManager.Start中去了，静默行军状态下不会加载战斗相关的资源
                // // 检查是否有主动技buff
                // AbilityPart.CheckBuff()
            }
        },
        Update()
        {
            for(let key in FightBehaviorManager.mAllRole)
            {
                let role = FightBehaviorManager.mAllRole[key];
                role.Update();
            }
        },
        //检测当前镜头需要追踪的对象
        SetCameraFollow()
        {
            //新手战斗介绍时禁止镜头移动
            if(FightBehaviorManager.mNewbieStop)
                return;
            

            //force = force || false;
            //一定时间内不修改
            if((new Date()).getTime() - FightBehaviorManager.mLastFollowTime < FightBehaviorManager.mFollowTimeGap)
                return;
            
            FightBehaviorManager.mLastFollowTime = (new Date()).getTime();
            //Hold住镜头定时
            if(FightBehaviorManager.mHoldCamera && (new Date()).getTime() - FightBehaviorManager.mHoldStartTime < FightBehaviorManager.mHoldTime)
                return;
            
            if(FightBehaviorManager.mHoldCamera)
            {
                FightBehaviorManager.mHoldCamera = false;
                FightBehaviorManager.mHoldStartTime = 0;
                FightBehaviorManager.mHoldTime = 0;
                FightBehaviorManager.mHoldRoleID = 0;
            }
            

            //判断镜头是否位置错误
            let pos = SceneManager.GetLogicPos();
            if(pos.x > 700)
            {
                //跟随我方塔
                FightBehaviorManager.ResetCamera();
                return;
            }
                
            

            let myTeam = FightBehaviorManager.GetBehaviorTeam(FightCampType.My);

            //最前面的模型
            let mostFrontRole = null;
            //最前面的模型的z轴坐标
            let mostFrontCount = -999;
            // 战斗队员Update
            for(let key in myTeam)
            {
                let role = myTeam[key];
                // 查找最前面的模型进行跟随
                // 全部死亡就跟随水晶
                // 不跟随闪现对象
                if(role.IsShiBing() && !role.IsDeadFlag() && !role.IsRepelFlag() && !role.IsSprintFlag() && !role.IsTeleportFlag())
                {
                    let pos = role.GetPosition();
                    if(pos.x > mostFrontCount && pos.y <= 800)
                    {
                        mostFrontCount = pos.x;
                        mostFrontRole = role;
                    }
                }
                    
            }

            // 无法找到镜头跟随对象，说明所有队员均已阵亡
            if(mostFrontRole == null)
            {
                //跟随我方塔
                FightBehaviorManager.ResetCamera();
                return;
            }
                
            

            // 镜头跟随对象与上次相同
            if(FightBehaviorManager.mCurFollowTarget != null && FightBehaviorManager.mCurFollowTarget.mUUID == mostFrontRole.mUUID)
                return;
            
            
            //设置跟随对象
            let obj = FightGameObjectManager.getInstance().GetModelParent(mostFrontRole.mGameObjectID);
            if(obj == null)
            {
                FightBehaviorManager.ResetCamera()
                return;
            }
                
            
            SceneManager.SetFollowCameraTarget(obj);
            FightBehaviorManager.mCurFollowTarget = mostFrontRole;
        },
        // 获取一个阵营的所有人
        GetBehaviorTeam(campType)
        {
            let result = [];
            for(let key in FightBehaviorManager.mAllRole)
            {
                let role = FightBehaviorManager.mAllRole[key];
                if(role.mCampType == campType && !role.IsDead)
                    result.push(role);
            }
                
                
            
            return result;
        },
    

        //重置镜头，跟随我方塔
        ResetCamera()
        {
            SceneManager.SetFollowCameraTarget(FightBehaviorManager.mMyBornObj);
            FightBehaviorManager.mCurFollowTarget = null;
        },
        GetCamera()
        {
            return null;
        },

        //约分数量获取函数
        GetCutCount()
        {
            if(FightBehaviorManager.mFightingSystemType == FightSystemType.GuanQia)
                return FightBehaviorManager.mCutCount;
        
            return 0;
        },
        //接受战斗帧信息
        ReceiveRecordlist(recordlist)
        {
            //增加一帧
            FightBehaviorManager.mFrameCount = FightBehaviorManager.mFrameCount + 1

            for(let i in recordlist)
            {
                let fightRecord = recordlist[i]
                FightBehaviorManager.ProcessRecord(fightRecord)
            }
                
             
            
            //检测是否有人死亡，有的话进行检测
            let totalCount = 0
            let deadCount = 0
            let needCheckDead = false
            if(FightBehaviorManager.mDeadFlag && (new Date()).getTime() - FightBehaviorManager.mTipsTime > FightBehaviorManager.mTipsTimeGap)
            {
                FightBehaviorManager.mTipsTime = (new Date()).getTime();
                needCheckDead = true
            }
            
            //战斗角色移动一定要在这里运行，不然移动会有偏移
            for(let key in FightBehaviorManager.mAllRole)
            {
                let role = FightBehaviorManager.mAllRole[key];
                //Update战斗角色移动
                role.UpdateMove();
                //检测是否全队死亡
                if(needCheckDead)
                {
                    if(role.mCampType == FightCampType.My && role.mRoleType == RoleT.Normal)
                    {
                        totalCount = totalCount + 1
                        if(role.IsDeadFlag())
                            deadCount = deadCount + 1
                    }
                }
                    
            }
                
                        
                    
                
            
            
            //检测完毕，判断是否需要弹出提示
            if(needCheckDead)
            {
                // if( totalCount == deadCount)
                //     CommonHUDPanel.Show(TranslateTool.GetText(200427))
            }
                
                
            

            //刷新镜头跟随
            FightBehaviorManager.SetCameraFollow();

            //设置接收时间
            FightBehaviorManager.mLastUpdateTime = (new Date()).getTime();

            //设置定时器
            //timerMgr:RemoveTimerEvent('FightBehaviorManager.CheckGuanQiaErrorTimer');
            //timerMgr:AddTimerEvent('FightBehaviorManager.CheckGuanQiaErrorTimer', FightBehaviorManager.mErrorTimeGap + 1, FightBehaviorManager.CheckGuanQiaErrorTimer, null, false);
            
            //计算时间流逝
            FightBehaviorManager.mTimePass = FightBehaviorManager.mTimePass + FightDef.FREQUENCY / 1000;
            this.OnTimeChange(FightBehaviorManager.mDuration - FightBehaviorManager.mTimePass);
        },
        
        //时间改变
        //参数1，remain 剩余时间
        OnTimeChange(remain)
        {
            for(let key in FightBehaviorManager.mTimeCallBackFunc)
            {
                let func = FightBehaviorManager.mTimeCallBackFunc[key];
                if(func)
                {
                    func(remain);
                }
            }
        },
        //时间改变
        //uuid 唯一ID
        //cardID 卡牌ID
        //hurt 伤害值 BigNumber对象
        OnHurtChange(uuid, cardID, hurt)
        {
            for(let key in FightBehaviorManager.mHurtCallBackFunc)
            {   
                let func = FightBehaviorManager.mHurtCallBackFunc[key];
                if(func)
                    func(uuid, cardID, hurt);
            }
        },
        ProcessRecord(fightRecord)
        {
            let record = fightRecord.Content
            // N种指令，战斗角色有关，飞行道具有关，主动技有关
            if(fightRecord.RecordT == RecordT.RecordT_Born)
            {
                //新手战斗特殊操作，这些模型在新手战斗开始已经出生过，在出生记录执行时只需要刷新信息
                if(FightBehaviorManager.mIsNewbieFight)
                {
                    for(let id in FightBehaviorManager.mAllRole)
                    {
                        let role = FightBehaviorManager.mAllRole[id];
                        if(role.mCardID == record.ID && role.mCampType == record.Camp && role.mRoleType != RoleT.Summoner)
                        {
                            let uuid = record.UUID;
                            let campType = record.Camp;
                            let hp = record.HP;
                            let maxhp = record.MaxHP;
                            let roleType = record.Type;
                            role.UpdateData(uuid, record.ID, campType, hp, maxhp, roleType);
                            FightBehaviorManager.mAllRole[uuid] = role;
                            FightBehaviorManager.mAllRole[id] = null;
                            return;
                        }
                    }
                }

                // 出生，创建一个角色到指定位置
                let id = record.ID;

                // 获取模型名称
                let modelName = FightResourceManager.GetShiBingModelNameByShiBingID(id);
                if(modelName == null || modelName == '')
                    return;

                // 获取模型
                let gameObjID = FightResourceManager.SpawnModel(modelName);

                let uuid = record.UUID;
                let campType = record.Camp;
                let pos = record.Pos;
                let maxhp = record.MaxHP;
                let hp = record.HP;
                let roleType = record.Type;
                let role = null;
                if(roleType == RoleT.Normal)
                    role = new FightNormalRole(uuid, id, campType, gameObjID, hp, maxhp, roleType, modelName);
                else
                    role = new FightBossRole(uuid, id, campType, gameObjID, hp, maxhp, roleType, modelName);

                // 加入战斗模型table中
                FightBehaviorManager.mAllRole[uuid] = role;
                // 接受指令
                role.ReceiveRecord(fightRecord.RecordT, record);

                //如果是敌方Boss，返回最大血量回调
                if(roleType == RoleT.Castle && campType == FightCampType.Enemy)
                {
                    for(let key in FightBehaviorManager.mEnemyBossMaxBloodCallBackFunc)
                    {   let func = FightBehaviorManager.mEnemyBossMaxBloodCallBackFunc[key];
                        if(func != null)
                            func(maxhp, hp);
                    }
                    
                }
            }
            else if(fightRecord.RecordT == RecordT.RecordT_FlyItem)
                // 飞行道具有关
                FlyItemBehaviorManager.getInstance().ProcessFlyItemBornRecord(record);
            else if(fightRecord.RecordT == RecordT.RecordT_CrossFly)
                // 穿透飞行道具
                FlyItemBehaviorManager.getInstance().ProcessCrossFlyItemBornRecord(record);
            else if(fightRecord.RecordT == RecordT.RecordT_UseASkill)
                // 主动技
                FightBehaviorManager.ProcessASkillRecord(record);
            else
            {
                // 其他指令战斗角色指令
                if(!record)
                    console.error("play null record:"+fightRecord.RecordT);
                
                let uuid = record.UUID;
                let role = FightBehaviorManager.mAllRole[uuid];
                if(role == null)
                {
                    console.error("指令对象不存在"+uuid);
                    return;
                }
                // 接受指令
                role.ReceiveRecord(fightRecord.RecordT, record);

                //受击血量接口调用
                if(fightRecord.RecordT == RecordT.RecordT_Hurt)
                {
                    // 受击指令
                    for(let id in FightBehaviorManager.mAllRole)
                    {
                        let role = FightBehaviorManager.mAllRole[id];
                        if(role.mCampType == FightCampType.My && role.mUUID == record.MasterUUID)
                            this.OnHurtChange(role.mUUID, role.mCardID, record.Hurt);
                    }
                        
                }
                //如果是敌方Boss，返回最大血量回调
                if(role.mRoleType == RoleT.Castle && role.mCampType == FightCampType.Enemy)
                {
                    for(let key in FightBehaviorManager.mEnemyBossBloodCallBackFunc)
                    {
                        let func = FightBehaviorManager.mEnemyBossBloodCallBackFunc[key];
                        if(func != null)
                            func(record.Hurt)
                    }
                }
                
                //新手引导战斗步骤判断
                if(FightBehaviorManager.mIsNewbieFight && fightRecord.RecordT == RecordT.RecordT_Die)
                {
                    if(role.mRoleType == RoleT.Normal && role.mCampType == FightCampType.Enemy)
                        FightBehaviorManager.GuideFightStep3_1();
                
                
                    if(role.mRoleType == RoleT.Castle && role.mCampType == FightCampType.Enemy)
                        FightBehaviorManager.GuideFightStep4_1();
                }

                //关卡战斗我方是否有人死亡标记
                if(FightBehaviorManager.mFightType == FightSystemType.GuanQia || FightBehaviorManager.mFightType == FightSystemType.DiXiaCheng)
                {
                    for(let id in FightBehaviorManager.mAllRole)
                    {
                        let role = FightBehaviorManager.mAllRole[id];
                        if(role.mCampType == FightCampType.My && role.mUUID == record.MasterUUID)
                            FightBehaviorManager.mDeadFlag = true;
                    }
                }
            }
        },
        //获取当前的速度
        GetSpeed()
        {
            return FightBehaviorManager.mCurSpeed;
        },
        //获取当前的速度
        SetSpeed(speed)
        {
            FightBehaviorManager.mCurSpeed = speed;
            //AnimationManager.GetInstance().SetSpeed(speed)
            //ParticleSystemManager.GetInstance().SetSpeed(speed)
        },
        //获取某个对象
        GetRole(uuid)
        {
            return FightBehaviorManager.mAllRole[uuid];
        },
        // 获取敌方Boss
        GetEnemyBoss(campType)
        {
            let oppCampType = FightBehaviorManager.GetOppCampType(campType);
            return FightBehaviorManager.GetBoss(oppCampType);
        },
        // 获取Boss
        GetBoss(campType)
        {
            for(let id in FightBehaviorManager.mAllRole)
            {
                let role = FightBehaviorManager.mAllRole[id];
                if(role.mRoleType == RoleT.Castle && role.mCampType == campType)
                    return role;
            }
            return null;
        },
        //获取敌对阵营枚举
        //参数1，自身枚举
        //返回值，对方枚举
        GetOppCampType(campType)
        {
            if(campType == FightCampType.My)
                return FightCampType.Enemy;
            else
                return FightCampType.My;
            
        },
        //判断是否在非关卡战斗中
        IsInOtherFightSystem()
        {
            if(FightBehaviorManager.mFightingSystemType == null)
                return false;

            if(FightBehaviorManager.mFightingSystemType == FightSystemType.GuanQia)
                return false;

            return true;
        },

        //设置战斗类型，提供给需要先请求再加载战斗的，比如竞技场，战斗塔，挑战塔
        SetFightingSystem(typ)
        {
            FightBehaviorManager.mFightingSystemType = typ;
        },

        //屏蔽点击
        OnShieldTouch(shield)
        {
            if(shield)
                FightBehaviorManager.mCurFollowTarget = null;
        },

        /****************************新手战斗 相关的步骤函数-***********************/
        //引导步骤1，一开始显示到对方的塔，然后显示1901对白，对方逐个扔出小精灵
        //1-1 一开始显示到对方的塔，然后显示1901对白
        GuideFightStep1_1()
        {
            if(FightBehaviorManager.mGuideFightStep1)
                return;
            
            FightBehaviorManager.mGuideFightStep1 = true;
            //新手战斗标记 
            FightBehaviorManager.mIsNewbieFight = true;
            FightBehaviorManager.mNewbieStop = true;
            GFightManager.Pause();

            // 获取我方模型
            let conf_ZhanDouPeiZhi_Data = LoadConfig.getConfigData(ConfigName.ZhanDouPeiZhi,16);
            let myBossID = conf_ZhanDouPeiZhi_Data.MyBossID;
            // 获取模型名称
            let modelName = FightResourceManager.GetShiBingModelNameByShiBingID(myBossID);
            if(modelName == null || modelName == '')
                return;
            
            // 获取模型
            let root = FightGameObjectManager.getInstance().SpawnModel(modelName);
            let uuid = 10001;
            let campType = FightCampType.My;
            let pos = { x : 70, y : 0 };
            let hp = 100;
            let maxhp = 100;
            let roleType = RoleT.Castle;
            let role = new FightBossRole(uuid, myBossID, campType, root, hp, maxhp, roleType, modelName);
            // 加入战斗模型table中
            FightBehaviorManager.mAllRole[uuid] = role;
            role.Born( { x : 0, y : 0, z : - 70 });

            // 获取敌方模型
            let enemyBossID = conf_ZhanDouPeiZhi_Data.EnemyBossID;
            // 获取模型名称
            modelName = FightResourceManager.GetShiBingModelNameByShiBingID(enemyBossID);
            if(modelName == null || modelName == '')
                return;
            
            // 获取模型
            root = FightGameObjectManager.getInstance().SpawnModel(modelName);
            uuid = 10002;
            campType = FightCampType.Enemy;
            pos = { x : 70, y : 0 };
            hp = 100;
            maxhp = 100;
            roleType = RoleT.Castle;
            role = new FightBossRole(uuid, enemyBossID, campType, root, hp, maxhp, roleType, modelName);
            // 加入战斗模型table中
            FightBehaviorManager.mAllRole[uuid] = role;
            role.Born( { x: 0, y : 0, z : 70 });

            //镜头跟随敌方boss
            let obj = FightGameObjectManager.getInstance().GetModelParent(role.mGameObjectID);
            if(obj)
            {
                SceneManager.SetFollowCameraTarget(obj);
                FightBehaviorManager.mCurFollowTarget = role;
            }
                
            

            // GuidePanel.ShowDialog(1901, function(){
            //     this.GuideFightStep1_2()
            // });
        },

        //1-2对象逐个扔出小精灵
        GuideFightStep1_2()
        {
            // 敌方boss丢球
            FightBehaviorManager.GetBoss(FightCampType.Enemy).PlayBornAni();
            //敌人逐个扔出小精灵
            let conf_XinShoUZhanDou_Data = LoadConfig.getConfigData(ConfigName.XinShouZhanDou,1);
            
            // 获取敌方模型
            FightBehaviorManager.mEnemyCount = 0;
            for( i = 0; i < conf_XinShoUZhanDou_Data.EnemyTeam; ++i)
            {
                let enemyID = conf_XinShoUZhanDou_Data.EnemyTeam[i]
                if(enemyID > 0)
                {
                    // 获取模型名称
                    let modelName = FightResourceManager.GetShiBingModelNameByShiBingID(enemyID)
                    if(modelName == null || modelName == '')
                        return;
                    
                    // 获取模型
                    let root = FightResourceManager.SpawnModel(modelName);
                    let uuid = 10003 + FightBehaviorManager.mEnemyCount;
                    let campType = FightCampType.Enemy;
                    let pos = { x : 60, y : 0 };
                    let hp = 100;
                    let roleType = RoleT.Normal;
                    let role = new FightNormalRole(uuid, enemyID, campType, root, hp, hp, roleType, modelName);
                    // 加入战斗模型table中
                    FightBehaviorManager.mAllRole[uuid] = role;
                        
                    //timerMgr:AddTimerEvent('FightBehaviorManager.GuideFightBornEnemy'..FightBehaviorManager.mEnemyCount,0.5 + 0.3 * (FightBehaviorManager.mEnemyCount + 1),FightBehaviorManager.GuideFightBornEnemy,{Role = role},false);
                    FightBehaviorManager.mEnemyCount = FightBehaviorManager.mEnemyCount + 1;
                }
                    
            }
        
        },
        
        

        //敌人出生定时器回调
        GuideFightBornEnemy(params)
        {
            FightBehaviorManager.mEnemyBornCount = FightBehaviorManager.mEnemyBornCount + 1;
            let conf_XinShoUZhanDou_Data = LoadConfig.getConfigData(ConfigName.XinShouZhanDou,1);
            let posStr = conf_XinShoUZhanDou_Data.EnemyPos[FightBehaviorManager.mEnemyBornCount];
            let pos = GOTool.SplitStringToVector(posStr);

            let role = params.Role;
            role.Born( { x : pos.x, y : FightTool.GetModelHeight(role.mCardID), z : pos.z });

            if(FightBehaviorManager.mEnemyBornCount == FightBehaviorManager.mEnemyCount)
            {   
                this.GuideFightStep2_1();
            }
        },

        //引导步骤2转到我方的塔，显示1902,我方逐个扔出小精灵,显示1903对白
        //2-1转到我方的塔
        GuideFightStep2_1()
        {
            let myBoss = FightBehaviorManager.GetBoss(FightCampType.My);
            
            //镜头跟随
            let obj = FightGameObjectManager.getInstance().GetModelParent(myBoss.mGameObjectID);
            if(obj != null)
            {
                SceneManager.SetFollowCameraTarget(obj);
                FightBehaviorManager.mCurFollowTarget = myBoss;
            }
            
            //timerMgr:AddTimerEvent('FightBehaviorManager.GuideFightStep2_2',1,FightBehaviorManager.GuideFightStep2_2,null,false);
        },
            
        //2-2显示1902
        GuideFightStep2_2()
        {
            //显示1902
            // GuidePanel.ShowDialog(1902, function(){
            //     this.GuideFightStep2_3();
            // })
        },
            
        //2-3我方逐个扔出小精灵
        GuideFightStep2_3()
        {
            // 我方boss丢球
            this.GetBoss(FightCampType.My).PlayBornAni()
            //我方逐个扔出小精灵
            let conf_XinShoUZhanDou_Data = LoadConfig.getConfigData(ConfigName.XinShouZhanDou,1);
            // 获取模型
            for(let i = 0; i < conf_XinShoUZhanDou_Data.MyTeam; ++i)
            {
                let myID = conf_XinShoUZhanDou_Data.MyTeam[i];
                if(myID > 0)
                {
                    let modelName = FightResourceManager.GetShiBingModelNameByShiBingID(myID);
                    if(modelName == null || modelName == '')
                        return;
                    
                    // 获取模型
                    let root = FightResourceManager.SpawnModel(modelName)
                    let uuid = 20003 + FightBehaviorManager.mMyCount
                    let campType = FightCampType.My;
                    let pos = { x : - 60, y : 0 };
                    let hp = 100;
                    let roleType = RoleT.Normal;
                    let role = new FightNormalRole(uuid, myID, campType, root, hp, hp, roleType, modelName);
                    // 加入战斗模型table中
                    FightBehaviorManager.mAllRole[uuid] = role;    
                    //timerMgr:AddTimerEvent('FightBehaviorManager.GuideFightBornMy'..FightBehaviorManager.mMyCount,0.5 + 0.3 * (FightBehaviorManager.mMyCount + 1),FightBehaviorManager.GuideFightBornMy,{Role = role},false);
        
                    FightBehaviorManager.mMyCount = FightBehaviorManager.mMyCount + 1;
                }
            }
        },

        //我方出生定时器回调，显示1903
        GuideFightBornMy(params)
        {
            FightBehaviorManager.mMyBornCount = FightBehaviorManager.mMyBornCount + 1;
            let conf_XinShoUZhanDou_Data = LoadConfig.getConfigData(ConfigName.XinShouZhanDou,1);
            let posStr = conf_XinShoUZhanDou_Data.MyPos[FightBehaviorManager.mMyBornCount];
            let pos = GOTool.SplitStringToVector(posStr);
            let role = params.Role;
            role.Born( { x : pos.x, y : FightTool.GetModelHeight(role.mCardID), z : pos. z});

            if(FightBehaviorManager.mMyBornCount == FightBehaviorManager.mMyCount)
            {
                //显示1903
                // GuidePanel.ShowDialog(1903, function(){
                //     FightBehaviorManager.mNewbieStop = false
                //     GFightManager.Continue()
                // })
            }    
        },
        
        //引导步骤3 打死对方所有怪物之后，转到对方塔下，显示1904对白
        //3-1打死对方所有怪物之后，转到对方塔下
        GuideFightStep3_1()
        {
            FightBehaviorManager.mEnemyDieCount = FightBehaviorManager.mEnemyDieCount + 1;
            if(FightBehaviorManager.mEnemyCount > FightBehaviorManager.mEnemyDieCount)
                return;
            
            
            if(FightBehaviorManager.mGuideFightStep3)
                return;
            
            FightBehaviorManager.mGuideFightStep3 = true;
            //镜头跟随敌方boss
            let role = FightBehaviorManager.GetBoss(FightCampType.Enemy);
            let obj = FightGameObjectManager.getInstance().GetModelParent(role.mGameObjectID);
            if(obj)
            {
                SceneManager.SetFollowCameraTarget(obj);
                FightBehaviorManager.mCurFollowTarget = role;
            }
            FightBehaviorManager.mNewbieStop = true;
            GFightManager.Pause();
            //timerMgr:AddTimerEvent('FightBehaviorManager.GuideFightStep3_2',0.5,FightBehaviorManager.GuideFightStep3_2,null,false);
        },
            

        //3-2显示1904对白
        GuideFightStep3_2()
        {
            GuidePanel.ShowDialog(1904, function(){
                FightBehaviorManager.mNewbieStop = false;
                GFightManager.Continue();
            });
        },

        //引导步骤4 摧毁对方塔之后显示1905，转到我方塔下，显示1906
        //4-1 摧毁对方塔之后显示1905
        GuideFightStep4_1()
        {
            if(FightBehaviorManager.mGuideFightStep4)
                return;
        
            FightBehaviorManager.mGuideFightStep4 = true;
            FightBehaviorManager.mNewbieStop = true;
            GFightManager.Pause();
            GuidePanel.ShowDialog(1905, function(){
                let myBoss = FightBehaviorManager.GetBoss(FightCampType.My);
                
                //镜头跟随
                let obj = FightGameObjectManager.getInstance().GetModelParent(myBoss.mGameObjectID);
                if(obj != null)
                {
                    SceneManager.SetFollowCameraTarget(obj);
                    FightBehaviorManager.mCurFollowTarget = myBoss;
                }
                
                //timerMgr:AddTimerEvent('FightBehaviorManager.GuideFightStep4_2',1,FightBehaviorManager.GuideFightStep4_2,null,false);
            });
        },

        //4-2 转到我方塔下，显示1906
        GuideFightStep4_2()
        {
            GuidePanel.ShowDialog(1906, function(){
                NewbiePanel.OnNewbieFightFinished();
                FightBehaviorManager.mIsNewbieFight = false;
                FightBehaviorManager.mNewbieStop = false;
                GFightManager.Continue();
            });
        },

        //移除所有定时器，跳过用
        RemoveAllGuideTimer()
        {
            // for(let i = 0; i < FightBehaviorManager.mEnemyCount; ++i)
            // {
            //     let index = i - 1
            //     //timerMgr:RemoveTimerEvent('FightBehaviorManager.GuideFightBornEnemy'..index)
            // }
            
        
            // for(let i = 0; i < FightBehaviorManager.mMyCount; ++i)
            // {
            //     let index = i - 1
            //     //timerMgr:RemoveTimerEvent('FightBehaviorManager.GuideFightBornMy'..index)
            // }
                
            
            // timerMgr:RemoveTimerEvent('FightBehaviorManager.GuideFightStep2_2');
            // timerMgr:RemoveTimerEvent('FightBehaviorManager.GuideFightStep3_2');
            // timerMgr:RemoveTimerEvent('FightBehaviorManager.GuideFightStep4_2');
        }
        
    
    };
})();
FightBehaviorManager.mHurtCallBackFunc = {}; //伤害接口
FightBehaviorManager.mTimeCallBackFunc = {}; //计时接口
FightBehaviorManager.mEnemyBossBloodCallBackFunc = {}; //Boss血量接口
FightBehaviorManager.mEnemyBossMaxBloodCallBackFunc = {}; //Boss最大血量接口
FightBehaviorManager.mTimeCounting = 0;     //计时用的变量
FightBehaviorManager.mTimePass = 0;          //已经走过的时间
FightBehaviorManager.mDuration = 0;          //本场战斗的战斗时长
FightBehaviorManager.mFrameCount = 0;        //帧数量

FightBehaviorManager.mMyBornObj = null;       //我军出生点对象
FightBehaviorManager.mLogicObj = null;        //场景的逻辑对象，包含镜头，镜头跟随等脚本

FightBehaviorManager.mCurFollowTarget = null; //当前镜头跟随对象
FightBehaviorManager.mLastFollowTime = 0;    //上次设置镜头对象的时间
FightBehaviorManager.mFollowTimeGap = 100;     //镜头设置间隔
FightBehaviorManager.IsFullPanel = false;    //战斗是否被一个全屏界面挡住，如果是，关闭战斗显示
FightBehaviorManager.IsCamera1 = true  ;     //战斗镜头标记，true则当前使用的是Camera1，false则当前使用的是Camera2

FightBehaviorManager.mMyTeam = {};           //我军阵容信息
FightBehaviorManager.mEnemyTeam = {};        //敌军阵容信息
FightBehaviorManager.mFightIndex = 0;        //战斗下标
FightBehaviorManager.mFightType = null;       //战斗类型枚举
FightBehaviorManager.mLoadFinishFunc = null;  //资源加载回调

FightBehaviorManager.mAllRole = {};          //当前创建的对象
FightBehaviorManager.mCurSpeed = 1;          //当前速度
FightBehaviorManager.IsEnd = true;           //是否停止
FightBehaviorManager.mIsLoadingRes = false;  //加载资源次数
FightBehaviorManager.mIsFighting   = false;  //是否在一场战斗中，包括该战斗是否在加载中

FightBehaviorManager.mFightingSystemType = null;       //战斗类型枚举
FightBehaviorManager.mIsInitGuanQia = false;     //关卡战斗是否初始化中
FightBehaviorManager.mLastUpdateTime = 0;    //最后一次接收战斗指令时间
FightBehaviorManager.mErrorTimeGap = 10;     //错误检测时间
FightBehaviorManager.mFightCount = 0;

FightBehaviorManager.mDeadFlag = false;      //当前是否有人死亡
FightBehaviorManager.mTipsTime = 0;          //全队死亡提示
FightBehaviorManager.mTipsTimeGap = 2 ;      //全队死亡提示间隔时间

FightBehaviorManager.mHoldCamera = false;    //Hold住镜头标记
FightBehaviorManager.mHoldTime = 0;          //Hold住镜头时间
FightBehaviorManager.mHoldStartTime = 0;     //Hold住镜头开始时间
FightBehaviorManager.mHoldRoleID = 0;        //Hold住镜头的模型ID

//////////////////////////////////////////////-释放资源战斗次数////////////////////////////////////////////////
FightBehaviorManager.mUnloadResourceFightCount = 50; //卸载资源的战斗次数，苹果的要改成10
FightBehaviorManager.mCacheModelFightCount = 50;     //缓存模型的战斗次数，苹果不缓存，要改成1

//////////////////////////////////////////////-新手战斗////////////////////////////////////////////////////////
FightBehaviorManager.mIsNewbieFight     = false;     //是否是新手战斗
FightBehaviorManager.mNewbieStop        = false;     //是否战斗暂停中
FightBehaviorManager.mNewBieRole        = {};        //新手战斗模型
FightBehaviorManager.mMyCount           = 0 ;        //我方队伍人数
FightBehaviorManager.mEnemyCount        = 0;         //敌方队伍人数
FightBehaviorManager.mEnemyDieCount     = 0;         //敌方死亡人数
FightBehaviorManager.mMyBornCount       = 0;         //我方出生人数
FightBehaviorManager.mEnemyBornCount    = 0;         //敌方出生人数
FightBehaviorManager.mGuideFightStep1   = false;     //引导步骤一是否开始
FightBehaviorManager.mGuideFightStep3   = false;     //引导步骤二是否开始
FightBehaviorManager.mGuideFightStep4   = false;     //引导步骤三是否开始

//////////////////////////////////////////////-测试代码////////////////////////////////////
FightBehaviorManager.DebugLogic     = false; //是否运行逻辑
FightBehaviorManager.DebugBehavior  = false; //是否运行表现
FightBehaviorManager.DebugGuanQia   = false; //是否执行特殊的关卡代码
FightBehaviorManager.DebugFight     = false; //是否战斗

//////////////////////////////////////////////-光效屏蔽////////////////////////////////////
FightBehaviorManager.NeedShield = false;     //是否屏蔽光效

//////////////////////////////////////////////-约掉的位数，0是不约//////////////////////////
//战斗属性约掉的位数
FightBehaviorManager.mCutCount = 0;
export default FightBehaviorManager;