/*
作者（Author）:    skyHuang

描述（Describe）:
*/
/*jshint esversion: 6 */

import FightBehaviorManager from "../FightBehaviorManager";
import FightTool from "../FightTool";

export default class BaseBuffBehavior
{
    constructor(id, role, record)
    {
        this.mID        = id;
        this.mRole      = role;//--角色
        this.mBuffRecord = record;// --记录
        this.mStop      = false;
        let d = new Date();
        this.mStartTime = d.getTime();//--开始时间
    }
    Start()
    {

    }
    Run()
    {
        if(this.mStop)
        {
            return;
        }
        let realLstTime = (this.mBuffRecord.LstTime / 1000) / FightBehaviorManager.GetSpeed();
        if(FightTool.GetTimeGap(this.mStartTime) >= realLstTim)
        {
            this.mStop = true;
        }
    }
    Stop()
    {

    }
    CanStop()
    {
        return this.mStop;
    }
}
