/*
作者（Author）:    skyHuang

描述（Describe）:
*/
/*jshint esversion: 6 */

import BaseBuffBehavior from "./BaseBuffBehavior";

export default class SummonBuffBehavior extends BaseBuffBehavior
{
    constructor(id, role, record)
    {
        super(id, role, record);
        this.mEffectObj  = null;//--光效
        this.mEffectName = null;//--光效名
    }
    Start()
    {
    }
    Run()
    {
    }
    Stop()
    {
    }
    
}
