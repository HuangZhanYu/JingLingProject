/*
作者（Author）:    skyHuang

描述（Describe）:
*/
/*jshint esversion: 6 */

import BaseBuffBehavior from "./BaseBuffBehavior";

export default class HidingBuffBehavior extends BaseBuffBehavior
{
    constructor(id, role, record)
    {
        super(id, role, record);
    }
    Start()
    {
        //--设置隐身
        this.mRole.SetTransparent(true);
    }
    Stop()
    {
        //--设置不隐身
        this.mRole.SetTransparent(false);
    }
    
}
