/*
作者（Author）:    skyHuang

描述（Describe）:
*/
/*jshint esversion: 6 */


import BaseBuffBehavior from "./BaseBuffBehavior";

export default class BaseAddEffectBuffBehavior extends BaseBuffBehavior
{
    constructor(id, role, record)
    {
        super(id, role, record);
        this.mEffectObj  = null;//--光效
        this.mEffectName = null;//--光效名
    }
    Start()
    {
        let d = new Date();
        this.mStartTime = d.getTime();//--开始时间
        this.mStop = false;
        let conf_Buff_Data = LoadConfig.getConfigData(ConfigName.Buff,buff.buffid);
        this.mEffectName =  conf_Buff_Data.Effect;
        if(this.mEffectName == null || this.mEffectName == '')
        {
            return;
        }
        this.mRole.AddBuffEffect(this.mEffectName, conf_Buff_Data.Path);
    }
    Stop()
    {
        if(this.mEffectName == null || this.mEffectName == '')
        {
            return;
        }
        this.mRole.RemoveBuffEffect(this.mEffectName);
    }
    
}
