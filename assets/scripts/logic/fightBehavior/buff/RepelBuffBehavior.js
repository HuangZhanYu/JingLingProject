/*
作者（Author）:    skyHuang

描述（Describe）:
*/
/*jshint esversion: 6 */


import BaseBuffBehavior from "./BaseBuffBehavior";

export default class RepelBuffBehavior extends BaseBuffBehavior
{
    constructor(id, role, record)
    {
        super(id, role, record);
        this.mEffectObj  = null;//--光效
        this.mEffectName = null;//--光效名
        this.mVerticalSpeed      = 0;
        this.mHorizontalSpeed    = 0;
        this.mHorizontalOrient   = 0;
    }
    Start()
    {
        let conf_Buff_Data = LoadConfig.getConfigData(ConfigName.Buff,buff.buffid);
        this.mEffectName =  conf_Buff_Data.Effect;
        if(this.mEffectName != null && this.mEffectName != '')
        {
            this.mRole.AddBuffEffect(this.mEffectName, conf_Buff_Data.Path);
        }
        let height = this.mBuffRecord.Height;
        let length = this.mBuffRecord.Length;
        let duration = ((this.mBuffRecord.LstTime / 1000) - 0.25) / FightBehaviorManager.GetSpeed();
        let d = new Date();
        this.mStartTime = d.getTime();//--开始时间
        this.mStop       = false;
        //--横向移动用MoveBehaviorState的方式
        this.mRole.Repel(height, length, duration);
        
    }
    Run()
    {
        if(this.mStop)
        {
            return;
        }
        //-- buff添加0.5秒不能移动
        if(FightTool.GetTimeGap(this.mStartTime) >=(this.mBuffRecord.LstTime / 1000)/ FightBehaviorManager.GetSpeed())
        {
            this.mStop = true;
            return;
        }
    }
    Stop()
    {
        this.mRole.StopRepel();
        if(this.mEffectName == null || this.mEffectName == '')
        {
            return;
        }
        this.mRole.RemoveBuffEffect(this.mEffectName);
    }
    
}
