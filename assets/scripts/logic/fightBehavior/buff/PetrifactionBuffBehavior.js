/*
作者（Author）:    skyHuang

描述（Describe）:
*/
/*jshint esversion: 6 */


import BaseAddEffectBuffBehavior from "./BaseAddEffectBuffBehavior";

export default class PetrifactionBuffBehavior extends BaseAddEffectBuffBehavior
{
    constructor(id, role, record)
    {
        super(id, role, record);
    }
    Start()
    {
        let d = new Date();
        this.mStartTime = d.getTime();//--开始时间
        this.mStop = false;
        let conf_Buff_Data = LoadConfig.getConfigData(ConfigName.Buff,buff.buffid);
        this.mEffectName =  conf_Buff_Data.Effect;
        //--石化
        this.mRole.SetPetrifaction(true);
        //--停止动作播放
        this.mRole.StopAni();
        if(this.mEffectName == null || this.mEffectName == '')
        {
            return;
        }
        this.mRole.AddBuffEffect(this.mEffectName, conf_Buff_Data.Path);
    }
    Stop()
    {
        //--移除石化效果
        this.mRole.SetPetrifaction(false);
        if(this.mEffectName == null || this.mEffectName == '')
        {
            return;
        }
        this.mRole.RemoveBuffEffect(this.mEffectName);
    }
    
}
