/*
作者（Author）:    skyHuang

描述（Describe）.
*/
/*jshint esversion: 6 */

export default class FightRoleUI
{
    constructor()
    {
        this.mObj                        = null;  //--自身对象
        this.mHUD                        = null;  //--飘字组件
        this.mHP                         = null;  //--血条UI          //--显示血量百分比
        this.mHPLength                   = 0;    //--血条总长
        this.mWuMianTime                 = 0;
        this.mMoMianTime                 = 0;
    }
    //--创建角色UI
    //--参数1，obj UI的GameObject
    //--参数2，parentObj UI的挂点，一般来说是FightRole的某个位置
    Create(roleObj, campType)
    {
        this.mObj       = PanleFindChild(roleObj, 'root/Boold/FightRoleUIPanel', false);                  
        if(this.mObj != null)    //--召唤怪不一定有血条，如喇叭芽大招龙卷风
        {
            this.mObj.active = true;
            let enemyHP = PanleFindChild(this.mObj,'Blood/EnemyProgress', false); 
            let myHP = PanleFindChild(this.mObj,'Blood/Progress', false); 
            if(campType == FightCampType.My)
            {
                this.mHP =   myHP;
                myHP.active = true;
                enemyHP.SetActive(false);
            }
            else
            {
                this.mHP =   enemyHP;
                myHP.SetActive(false);
                enemyHP.active = true;
            }
            this.mHUD = PanleFindChild(this.mObj,'HUD', false); 
            this.mHPLength  = GOTool.GetRectTransformLength(this.mHP);
        }
    }

    //--设置HP
    //--参数1，percent 当前的血量百分比
    SetHP(percent)
    {
        if(percent > 1)
        {
            console.error("血量错误"+percent);
            percent = 1;
        }
        if(percent < 0)
        {
            console.error("血量错误"+percent);
            percent = 0;
        }
        if(this.mHP != null)//--召唤怪不一定有血条，如喇叭芽大招龙卷风
        {
            let curLength = this.mHPLength * percent;
            GOTool.SetRectTransformLength(this.mHP,curLength);
        }
    }

    SetLookAtTarget(camera)
    {
        //--召唤怪不一定有mLookAtScript，如喇叭芽大招龙卷风
        if(this.mObj != null)
        {
            GOTool.FightRoleUILookAt(this.mObj, camera);
        }
    }

    CreateHUD(content, typ)
    {
        if(FightManager.DebugHUD)
        {
            return;
        }
        let d = new Date();
        //--物免时间判断
        if(content == '物免' && d.getTime() - this.mWuMianTime < 1)
        {
            return;
        }
        else
        {
            this.mWuMianTime = d.getTime();
        }
        
        //--魔免时间判断
        if(content == '魔免' && d.getTime() - this.mMoMianTime < 1)
        {
            return;
        }
        else
        {
            this.mMoMianTime = d.getTime();
        }

        if(this.mHUD != null)
        {
            GOTool.CreateHUDText(this.mHUD, content, typ, 0.5 / FightBehaviorManager.GetSpeed(), 10 * FightBehaviorManager.GetSpeed(), 3 / FightBehaviorManager.GetSpeed());
        }
    }

    Release()
    {
        //--召唤怪不一定有mHUDTextFactory，如喇叭芽大招龙卷风
        if(this.mObj != null)
        {
            GOTool.ReleaseHUDText(this.mObj);
        }
    }

}
