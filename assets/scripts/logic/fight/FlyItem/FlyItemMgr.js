/*
作者（Author）:    jason

描述（Describe）:   飞行道具管理器
*/
/*jshint esversion: 6 */



import { FightFlyItemType } from "../FightDef";
import CrossFlyItem from "./CrossFlyItem";
import ChainFlyItem from "./ChainFlyItem";
import FlyItem from "./FlyItem";

export default class FlyItemMgr
{
    constructor(mgr)
    {
        this.FlyList = [];
        this.Manager = mgr;
    }
    AddFlyItem(item)
    {
        this.FlyList.push(item);
    }
    CreateFlyItem(master, target, skillid, tm)
    {
        if(!target)
        {
            return;
        }
        let show_conf = LoadConfig.getConfigData(ConfigName.JiNeng_BiaoXian,skillid);
        if (show_conf == null)
            return;

        let fly_conf = LoadConfig.getConfigData(ConfigName.FeiXingDaoJu,show_conf.FlyItemID);
        if (fly_conf == null)
            return;

        let item = null;
        if (fly_conf.FlyType == FightFlyItemType.Cross)
            item = new CrossFlyItem(show_conf.FlyItemID, master, target, skillid, tm);
        else if (fly_conf.FlyType == FightFlyItemType.Chain)
            item = new ChainFlyItem(show_conf.FlyItemID, master, target, skillid, tm);
        else
        {
            item = new FlyItem(show_conf.FlyItemID, master, target, skillid, tm);
        } 
            

        if (item == null)
            return;

        this.AddFlyItem(item);
    }
    Run(tm)
    {
        let list = [];
        for(let item of this.FlyList)
        {
            if (!item.Run(tm))
            {
                list.push(item);
            }
        }
        this.FlyList = list;
    }
}
