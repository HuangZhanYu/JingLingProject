/*
作者（Author）:    jason

描述（Describe）:
*/
/*jshint esversion: 6 */
import { HurtT } from "../FightRecord";

export default class CrossFlyItem
{
    constructor(id, master, target, skillid, tm)
    {
        this.Master  = master;
        this.Target  = target;
        this.StartTime = tm;
        this.FlyTime = 0;
        this.Conf    = LoadConfig.getConfigData(ConfigName.JiNeng,skillid);
        this.Height  = this.Conf.Param[1] / 10000;
        this.Distance= this.Conf.Param[0] / 10000 + master.GetRangeAddition(this.Conf);
        this.Speed   = this.Conf.FlyItemSpeed / 10000;
        this.Direct  = 1;
        if (master.Pos.x > target.Pos.x)
            this.Direct = -1;
        this.BeginPos= master.Pos;
        this.CurX    = this.BeginPos.x;
        this.UpdateTm= tm;
        this.HitList = [];

        master.Manager.AddCrossFlyItemRecord(master, skillid, id, this.Direct*this.Speed, this.Distance);
    }
    HasHit(id)
    {
        for(let uuid of this.HitList)
        {
            if (uuid == id)
            {
                return true;
            }
        }
        return false;
    }
    Run(tm)
    {
        let len = Math.abs(this.CurX - this.BeginPos.x);
        if (len >= this.Distance)
            return true;

        let dis = this.Speed * (tm - this.UpdateTm) / 1000;
        if (len + dis > this.Distance)
            dis = this.Distance - len;
        
        let pos = null;
        if (this.Direct == 1)
            pos = new cc.Vec2(this.CurX+dis/2, this.BeginPos.y);
        else
            pos = new cc.Vec2(this.CurX-dis/2, this.BeginPos.y);
        
        let targets = this.Master.Manager.RectSearch(this.Master, pos, dis/2, this.Height/2, this.Conf);
        for (let target of targets)
        {
            if (!this.HasHit(target.UUID))
            {
                target.Wound(this.Master, this.Conf, HurtT.Attack);
                this.HitList.push(target.UUID);
            }
            if (this.HitList.length >= this.Conf.WoundCount)
                return true;
        }
            
        //log("cross hit count:"..#targets.." x:"..this.CurX.." dis:"..dis.." y:"..this.BeginPos.y)

        this.UpdateTm = tm;
        this.CurX = this.CurX + dis * this.Direct;
        return false;
    }
}

