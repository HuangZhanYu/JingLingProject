/*
作者（Author）:    jason

描述（Describe）:
*/
/*jshint esversion: 6 */
import { FightRoleState, FightSkillHurtType } from "../FightDef";
import { HurtT } from "../FightRecord";


export default class ChainFlyItem
{
    constructor(id, master, target, skillid, tm)
    {
        this.ID      = id;
        this.Master  = master;
        this.Target  = target;
        this.StartTime = tm;
        this.Conf    = LoadConfig.getConfigData(ConfigName.JiNeng,skillid);
        this.FlyTime = this.Conf.FlyTime;
        this.HitCount = this.Conf.Param[0];
        this.HitIndex = 0;
    
        master.Manager.AddFlyItemBornRecord(master, target.UUID, skillid, id, this.FlyTime);
    }

    Run(tm)
    {
        if (tm - this.StartTime < this.FlyTime)
            return false;

        this.HitIndex = this.HitIndex + 1;
        if (this.Conf.SkillHurtType == FightSkillHurtType.Single)
            this.Target.Wound(this.Master, this.Conf, HurtT.Attack);
        else if (this.Conf.SkillHurtType == FightSkillHurtType.Range)
        {
            let targets  = this.Master.Manager.SearchPos(this.Target.Pos, this.Master, this.Conf);
            for(let target of targets)
                target.Wound(this.Master, this.Conf, HurtT.Attack);
        }
            

        if (this.HitIndex >= this.HitCount)
            return true;

        return this.StartNextFight(tm);
    }
    StartNextFight(tm)
    {
        this.StartTime = tm;
        let manager  = this.Master.Manager;
        let oldtarget= this.Target;
        this.Target = manager.TargetBehind(this.Master, this.Target, this.Conf);
        if (this.Target != null) //判断目标是否在攻击范围内
        {
            let dis = this.Target.Pos;
            dis = dis.sub(oldtarget.Pos);
            let reach = this.Conf.Param[1]/10000;
            //log("find target behind id"..this.Target.UUID.." reach:"..reach * reach.." dis:"..dis:magSqr())
            if (dis.magSqr() > reach * reach)
                this.Target = null;
            else
                manager.AddFlyItemBornRecord(oldtarget,  this.Target.UUID, this.Conf.ID, this.ID, this.FlyTime);
        }
   
        return this.Target == null;
    }
}

