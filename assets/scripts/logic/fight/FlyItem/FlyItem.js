/*
作者（Author）:    jason

描述（Describe）:
*/
/*jshint esversion: 6 */


import { FightSkillHurtType, FightShowType } from "../FightDef";
import { HurtT } from "../FightRecord";

export default class FlyItem
{
    constructor(id, master, target, skillid, tm)
    {
        this.Master  = master;
        this.Target  = target;
        this.StartTime = tm;
        this.Conf    = LoadConfig.getConfigData(ConfigName.JiNeng,skillid);
        this.FlyTime = this.Conf.FlyTime;

        master.Manager.AddFlyItemBornRecord(master, target.UUID, skillid, id, this.FlyTime);
    }

    Run(tm)
    {
        if (tm - this.StartTime < this.FlyTime)
            return false;

        if (this.Conf.SkillHurtType == FightSkillHurtType.Single)
            this.Target.Wound(this.Master, this.Conf, HurtT.Attack);
        else if (this.Conf.SkillHurtType == FightSkillHurtType.Range)
        {
            let targets  = this.Master.Manager.SearchPos(this.Target.Pos, this.Master, this.Conf);
            for (let target of targets)
            {
                if (this.Conf.ShowType == FightShowType.Splash && this.Target != target)
                    target.Wound(this.Master, this.Conf, HurtT.Splash);
                else
                    target.Wound(this.Master, this.Conf, HurtT.Attack);
            }
        }

        return true;
    }
}

