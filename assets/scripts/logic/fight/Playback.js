/*
作者（Author）:    jason

描述（Describe）: 战斗回放控制器
*/
/*jshint esversion: 6 */

export default class Playback
{
    constructor()
    {
        this.Record = null;    //战斗记录
        this.Frame  = 0;      //当前帧数
        this.Pause  = false;  //暂停标记
        this.TotalFrame  = 0; //战斗总帧数
        this.RecordIndex = 1;
    }
    //初始化
    Init(record)
    {
        this.Record = record;
        let len = this.Record.FrameList.length;
        this.TotalFrame = this.Record.FrameList[len].Frame;
    }

    Run()
    {

    }

    //暂停
    Pause(pause){
        this.Pause = pause;
    }
        


    //更新函数
    Update()
    {
        if (this.Pause)
            return false;
        

        this.Frame = this.Frame + 1;
        return this.Frame > this.TotalFrame;
    }
        


    UseActiveSkill(id){}
        // body


    BuffNotDone()
    {
        return [];
    }

    //获取战斗详情
    GetFightInfo()
    {
        return this.Record;
    }

    //获取当前帧的记录
    GetRecords()
    {
        let frame = this.Record.FrameList[this.RecordIndex];
        if (frame.Frame == this.Frame - 1)
        {
            this.RecordIndex = this.RecordIndex + 1;
            return frame.Records;
        }
        return [];
    }

    GetInput()
    {
        return [];
    }
        


    //战斗结果
    Ret()
    {
        return this.Record.WinTeam;
    }
        


    //判断战斗是否结束
    End()
    {
        return this.Frame >= this.TotalFrame.length;
    }

}