/*
作者（Author）:    jason

描述（Describe）: 战斗记录
*/
/*jshint esversion: 6 */
let fight_msg_pb = require('fight_msg').msg;

export const RecordT = {
    RecordT_Born        : 1	,//出生
	RecordT_Revive      : 2	,//复活
	RecordT_Die	        : 3	,//死亡
	RecordT_Move        : 4	,//移动
	RecordT_Attack      : 5	,//攻击
	RecordT_Stand       : 6	,//战力
	RecordT_IncHP       : 7	,//血量增加
	RecordT_Hurt        : 8	,//受击
	RecordT_AddBuff     : 9	,//添加buff
	RecordT_RmvBuff     : 10 ,//移除buff
	RecordT_Hud	        : 11 ,//飘字
	RecordT_FlyItem     : 12 ,//飞行道具
	RecordT_Teleport    : 13 ,//闪现
    RecordT_UseASkill   : 14 ,//使用主动技
    RecordT_Repel       : 15 ,//击退
    RecordT_CrossFly    : 16 ,
    RecordT_Sprint      : 17 ,//冲刺
    RecordT_SprintFinish: 18 ,//冲刺结束
    RecordT_Pull        : 19 ,//拉人
    RecordT_PullFinish  : 20 ,//拉人结束
    RecordT_PreHurt     : 21 ,//受击前置光效
};
let RecordFactory = {};
export function RegisterRecord(type, record)
{
    if (RecordFactory[type] != null)
        return;
    RecordFactory[type] = record;
}
    
export function RecordDecode(type, buffer)
{
    let record = RecordFactory[type];
    if (record == null)
        return null;
    
    record.Decode(buffer);
    return record;
}

//单条战斗记录
export class FightRecord
{
    constructor(recordt, cxt)
    {
        this.RecordT = recordt;
        this.Content = cxt;
    }
}
//战斗每帧记录
export class FrameRecords
{
    constructor()
    {
        this.Frame = 0;
        this.Records = [];
    }
}
//战斗记录信息
export class FightRecords
{
    constructor()
    {
        this.TeamList = [];
        this.Duration = 0;
        this.RandSeed = 0;
        this.FrameList= [];
        this.WinTeam  = 0;
        this.RealTime = 0;
    }
    Decode(buffer)
    {
        let record = fight_msg_pb.FightRecords.decode(buffer);


        this.Duration = record.Duration;
        this.RandSeed = record.RandSeed;
        this.WinTeam  = record.WinTeam;

        for(let team of record.TeamList)
        {
            let teaminfo = [];
            teaminfo.ObjID = team.ObjID;
            teaminfo.RoleList = [];
            for (let id of team.RoleList)
                teaminfo.RoleList.push(id);
            
            this.TeamList.push(teaminfo);
        }
        
        for(let data of record.FrameList)
        {
            let frame = new FrameRecords();
            frame.Frame = data.Frame;
            for(let item of data.Records)
            {
                let o = new FightRecord(item.RecordT, RecordDecode(item.RecordT, item.Content));
                frame.Records.push(o);
            }
                
            this.FrameList.push(frame);
        }
    }
    AppendFollow(buffer)
    {
        let record = fight_msg_pb.FightRecordsFollow.decode(buffer);

        
        for(let data of record.FrameList)
        {
            let frame = new FrameRecords();
            frame.Frame = data.Frame;
            for(let item of data.Records)
            {
                let o = new FightRecord(item.RecordT, RecordDecode(item.RecordT, item.Content));
                frame.Records.push(o);
            }
            this.FrameList.push(frame);
        }
    }
    Debug()
    {
        // for _, data in ipairs(this.FrameList) do
        //     log("frame:"..data.Frame)
        //     for _, item in ipairs(data.Records) do
        //         item.Content:Debug()
        //     end
        // end
    }
}

//出生记录
export class BornRecord
{
    constructor()
    {
        this.UUID  = 0;
        this.ID    = 0;
        this.Camp  = 0;
        this.Pos   = null;
        this.MaxHP = '';
        this.Type  = 0;
        this.HP    = '';
    }
    Decode(buffer)
    {
        let record = fight_msg_pb.BornRecord.decode(buffer);


        this.UUID = record.UUID;
        this.ID   = record.ID;
        this.Camp = record.Camp;
        this.Pos  = new cc.Vec2(record.Pos.x, record.Pos.y);
        this.MaxHP= record.MaxHP;
        this.Type = record.Type;
        this.HP   = record.HP;
    }
    Flip()
    {
        this.Pos.x = -this.Pos.x;
    }
    Debug()
    {

    }
}
RegisterRecord(RecordT.RecordT_Born, new BornRecord());
RegisterRecord(RecordT.RecordT_Revive, new BornRecord());

//死亡记录
export class DieRecord
{
    constructor()
    {
        this.UUID  = 0;
    }
    Decode(buffer)
    {
        let record = fight_msg_pb.DieRecord.decode(buffer);


        this.UUID = record.UUID;
    }
    Flip()
    {
    }
    Debug()
    {
    }
}
RegisterRecord(RecordT.RecordT_Die, new DieRecord());

//移动记录
export class MoveRecord
{
    constructor()
    {
        this.UUID  = 0;
        this.TargetUUID = 0;
        this.Speed = 0;
        this.Pos   = null;
    }
    Decode(buffer)
    {
        let record = fight_msg_pb.MoveRecord.decode(buffer);


        this.UUID  = record.UUID;
        this.TargetUUID = record.TargetUUID;
        this.Speed = record.Speed;
        if (record.Pos != null)
            this.Pos = new cc.Vec2(record.Pos.x, record.Pos.y);
        
    }
    Flip()
    {
        this.Speed = this.Speed;
    }
    Debug()
    {
    }
}
RegisterRecord(RecordT.RecordT_Move, new MoveRecord());

//攻击记录
export class AttackRecord
{
    constructor()
    {
        this.UUID  = 0;
        this.TargetUUID = 0;
        this.SkillID = 0;
        this.ASPD  = 0;
    }
    Decode(buffer)
    {
        let record = fight_msg_pb.AttackRecord.decode(buffer);


        this.UUID  = record.UUID;
        this.TargetUUID = record.TargetUUID;
        this.SkillID = record.SkillID;
        this.ASPD  = record.ASPD;
    }
    Flip()
    {
    }
    Debug()
    {
    }
}
RegisterRecord(RecordT.RecordT_Attack, new AttackRecord());

//站立记录
export class StandRecord
{
    constructor()
    {
        this.UUID  = 0;
    }
    Decode(buffer)
    {
        let record = fight_msg_pb.StandRecord.decode(buffer);


        this.UUID  = record.UUID;
    }
    Flip()
    {
    }
    Debug()
    {
    }
}
RegisterRecord(RecordT.RecordT_Stand, new StandRecord());

//加血记录
export class AddHPRecord
{
    constructor()
    {
        this.UUID  = 0;
        this.HP    = '0';
    }
    Decode(buffer)
    {
        let record = fight_msg_pb.AddHPRecord.decode(buffer);

    
        this.UUID  = record.UUID;
        this.HP    = record.HP;
    }
    Flip()
    {
    }
    Debug()
    {
    }
}


export const HurtT = {
	Attack  : 1, //普攻
	Buff    : 2, //buff
    ASkill  : 3, //主动技
    Splash  : 4, //溅射 
};
//受击记录
export class HurtRecord
{
    constructor()
    {
        this.UUID  = 0;
        this.Hurt  = '0';
        this.HurtSrc = 0;
        this.HurtSrcID = 0;
        this.MasterUUID = 0;
    }
    Decode(buffer)
    {
        let record = fight_msg_pb.HurtRecord.decode(buffer);


        this.UUID  = record.UUID;
        this.Hurt  = record.Hurt;
        this.HurtSrc   = record.HurtSrc;
        this.HurtSrcID = record.HurtSrcID;
        this.MasterUUID= record.MasterUUID;
    }
    Flip()
    {
    }
    Debug()
    {
    }
}

//添加buff记录
export class AddBuffRecord
{
    constructor()
    {
        this.UUID  = 0;
        this.BuffID= 0;
        this.LstTime = 0;
        this.EffectV = 0;
    }
    Decode(buffer)
    {
        let record = fight_msg_pb.AddBuffRecord.decode(buffer);


        this.UUID   = record.UUID;
        this.BuffID = record.BuffID;
        this.LstTime = record.LstTime;
        this.EffectV = record.EffectV;
    }
    Flip()
    {
    }
    Debug()
    {
    }
}
RegisterRecord(RecordT.RecordT_AddBuff, new AddBuffRecord());

//删除buff记录
export class RmvBuffRecord
{
    constructor()
    {
        this.UUID  = 0;
        this.BuffID= 0;
    }
    Decode(buffer)
    {
        let record = fight_msg_pb.RmvBuffRecord.decode(buffer);


        this.UUID   = record.UUID;
        this.BuffID = record.BuffID;
    }
    Flip()
    {
    }
    Debug()
    {
    }
}
RegisterRecord(RecordT.RecordT_RmvBuff, new RmvBuffRecord());

export const HudT = {
    MISS  : 1,
	CRIT  : 2,
	PIMM  : 3,
	MIMM  : 4,
};

//飘字记录
export class HudRecord
{
    constructor()
    {
        this.UUID  = 0;
        this.Type  = 0;
    }
    Decode(buffer)
    {
        let record = fight_msg_pb.HudRecord.decode(buffer);


        this.UUID = record.UUID;
        this.Type = record.Type;
    }
    Flip()
    {
    }
    Debug()
    {
    }
}
RegisterRecord(RecordT.RecordT_Hud, new HudRecord());
//飞行道具记录
export class FlyItemBornRecord
{
    constructor()
    {
        this.UUID  = 0;
        this.TargetUUID = 0;
        this.FlyItemID  = 0;
        this.FlyTime    = 0;
        this.SkillID    = 0;
    }
    Decode(buffer)
    {
        let record = fight_msg_pb.FlyItemBornRecord.decode(buffer);


        this.UUID = record.UUID;
        this.TargetUUID = record.TargetUUID;
        this.FlyItemID  = record.FlyItemID;
        this.FlyTime    = record.FlyTime;
        this.SkillID    = record.SkillID;
    }
    Flip()
    {
    }
    Debug()
    {
    }
}
RegisterRecord(RecordT.RecordT_FlyItem, new FlyItemBornRecord());
//闪现记录
export class TeleportRecord
{
    constructor()
    {
        this.UUID = 0;
        this.Pos  = null;
        this.Time = 0;
        this.SkillID = 0;
    }
    Decode(buffer)
    {
        let record = fight_msg_pb.TeleportRecord.decode(buffer);


        this.UUID = record.UUID;
        this.Pos  = new cc.Vec2(record.Pos.x, record.Pos.y);
        this.Time = record.Time;
        this.SkillID = record.SkillID;
    }
    Flip()
    {
        this.Pos.x = -this.Pos.x;
    }
    Debug()
    {
    }
}
RegisterRecord(RecordT.RecordT_Teleport, new TeleportRecord());
//主动技使用记录
export class UseASkillRecord
{
    constructor()
    {
        this.SkillID = 0;
    }
    Decode(buffer)
    {
        let record = fight_msg_pb.UseASkillRecord.decode(buffer);


        this.SkillID = record.SkillID;
    }
    Flip()
    {
        this.Pos.x = -this.Pos.x;
    }
    Debug()
    {
    }
}
RegisterRecord(RecordT.RecordT_UseASkill, new UseASkillRecord());
//击退使用记录
export class RepelRecord
{
    constructor()
    {
        this.UUID  = 0;
        this.BuffID = 0;
        this.Height = 0;
        this.Length = 0;
        this.LstTime= 0;
    }
    Decode(buffer)
    {
        let record = fight_msg_pb.RepelRecord.decode(buffer);

    
        this.UUID = record.UUID;
        this.BuffID = record.BuffID;
        this.Height = record.Height;
        this.Length = record.Length;
        this.LstTime = record.LstTime;
    }
    Flip()
    {
    }
    Debug()
    {
    }
}
RegisterRecord(RecordT.RecordT_Repel, new RepelRecord());

export class CrossFlyItemRecord
{
    constructor()
    {
        this.UUID  = 0;
        this.FlyItemID = 0;
        this.Speed = 0;
        this.Distance = 0;
        this.SkillID  = 0;
    }
    Decode(buffer)
    {
        let record = fight_msg_pb.CrossFlyItemRecord.decode(buffer);


        this.UUID = record.UUID;
        this.FlyItemID = record.FlyItemID;
        this.Speed = record.Speed;
        this.Distance = record.Distance;
        this.SkillID = record.SkillID;
    }
    Flip()
    {
    }
    Debug()
    {
    }
}
RegisterRecord(RecordT.RecordT_CrossFly, new CrossFlyItemRecord());
//冲刺记录
export class SprintRecord
{
    constructor()
    {
        this.UUID  = 0;       
        this.TargetUUID = 0;  //需要移动到的对象，如果是贯穿或者多重就传入当前方向的水晶ID
        this.SkillID = 0;     //当前的攻击ID
        this.ASPD  = 0;       //当前的攻速
        this.Speed = 0;         //移动速度
        this.Pos   = null;       //移动到的位置，如果是贯穿或者多重就需要传入
        this.UsePos = false;
    }
    Decode(buffer)
    {
        let record = fight_msg_pb.SprintRecord.decode(buffer);


        this.UUID  = record.UUID;
        this.TargetUUID = record.TargetUUID;
        this.SkillID = record.SkillID;
        this.ASPD  = record.ASPD;
        this.Speed = record.Speed;
        this.Pos  = new cc.Vec2(record.Pos.x, record.Pos.y);
        this.UsePos= record.UsePos;
    }
    Flip()
    {
        this.Pos.x = -this.Pos.x;
    }
    Debug()
    {
    }
}
RegisterRecord(RecordT.RecordT_Sprint, new SprintRecord());

//冲锋结束记录
export class SprintFinishRecord
{
    constructor()
    {
        this.UUID  = 0;
        this.Pos   = null;
    }
    Decode(buffer)
    {
        let record = fight_msg_pb.SprintFinishRecord.decode(buffer);


        this.UUID  = record.UUID;
        this.Pos   = new cc.Vec2(record.Pos.x, record.Pos.y);
    }
    Flip()
    {
        this.Pos.x = -this.Pos.x;
    }
    Debug()
    {
    }
}
RegisterRecord(RecordT.RecordT_SprintFinish, new SprintFinishRecord());
//拉人记录
export class PullRecord
{
    constructor()
    {
        this.UUID  = 0;
        this.TargetUUID = 0;
        this.Speed = 0;
        this.Pos   = null;
    }
    Decode(buffer)
    {
        let record = fight_msg_pb.PullRecord.decode(buffer);


        this.UUID  = record.UUID;
        this.TargetUUID = record.TargetUUID;
        this.Speed = record.Speed;
        this.Pos   = new cc.Vec2(record.Pos.x, record.Pos.y);
    }
    Flip()
    {
        this.Pos.x = -this.Pos.x;
    }
    Debug()
    {
    }
}
RegisterRecord(RecordT.RecordT_Pull, new PullRecord());

//拉人结束记录
export class PullFinishRecord
{
    constructor()
    {
        this.UUID  = 0;
        this.Pos   = null;
    }
    Decode(buffer)
    {
        let record = fight_msg_pb.PullFinishRecord.decode(buffer);


        this.UUID  = record.UUID;
        this.Pos   = new cc.Vec2(record.Pos.x, record.Pos.y);
    }
    Flip()
    {
        this.Pos.x = -this.Pos.x;
    }
    Debug()
    {
    }
}
RegisterRecord(RecordT.RecordT_PullFinish, new PullFinishRecord());

export class PreHurtRecord
{
    constructor()
    {
        this.UUID  = 0;
        this.SkillID = 0;
    }
    Decode(buffer)
    {
        let record = fight_msg_pb.PreHurtRecord.decode(buffer);


        this.UUID    = record.UUID;
        this.SkillID = record.SkillID;
    }
    Flip()
    {
    }
    Debug()
    {
    }
}
RegisterRecord(RecordT.RecordT_PreHurt, new PreHurtRecord());

//战斗输入信息
export class FightInput
{
    constructor()
    {
        this.RandSeed    = 0;
        this.InputList   = new Map();
        this.BornBuffTab = new Map();
    }
    Encode()
    {
        let msg = new fight_msg_pb.FightInput();
        msg.RandSeed = this.RandSeed;
        for(let [frameid,framelist] of this.InputList)
        {
            let frame = new fight_msg_pb.FrameInputList();
            frame.Frame = frameid;
            for(let input of framelist)
            {
                let item = new fight_msg_pb.InputItem();
                item.Camp  = input.Camp;
                item.SkillID = input.SkillID;
                frame.FrameList.push(item);
            }
            msg.InputList.push(frame);
        }

        for ( let [objid, list] of this.BornBuffTab)
        {
            for(let buff of list)
            {
                let item = new fight_msg_pb.BornBuffItem();
                item.ObjID = objid;
                item.BuffID= buff.id;
                item.LstTm = buff.lsttm;
                for(let param of buff.params)
                    item.Params.push(param.toString());

                msg.BornBuffTab.push(item);
            }
        }

        return msg.toBuffer();
    }
}
