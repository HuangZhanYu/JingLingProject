/*
作者（Author）:    jason

描述（Describe）:战斗表现管理器，处理所有的战斗表现
    设计要点：
        1.对战斗所需的资源进行加载，模型，音效，预设等
        2.根据战斗逻辑管理器的指令，控制资源的行为，如模型攻击
        3.战斗镜头的切换和点击以及屏蔽也由战斗表现管理器负责
*/
/*jshint esversion: 6 */

import { FightInput, FightRecords, FrameRecords, BornRecord, DieRecord, MoveRecord, AttackRecord, StandRecord, AddHPRecord, AddBuffRecord, RmvBuffRecord, HudRecord, TeleportRecord, UseASkillRecord, RepelRecord, CrossFlyItemRecord, SprintRecord, PullRecord, PullFinishRecord, SprintFinishRecord, PreHurtRecord, RecordT, HurtRecord, FlyItemBornRecord } from "./FightRecord";
import FightTeam from "./FightTeam";
import BigNumber from "../../tool/BigNumber";
import FlyItemMgr from "./FlyItem/FlyItemMgr";
import { FightDef, SearchType, FightBuffType, FightCampType } from "./FightDef";
import FightBehaviorManager from "../fightBehavior/FightBehaviorManager";
import { SelectTarget } from "./FightRole";



export default class FightManager
{
    constructor()
    {
        this.FightID   = 0;     //战斗ID
        this.FightType = 0;
        this.TeamList  = [];    //战斗队伍列表
        this.StartTime = 0;     //战斗开始时间
        this.Timeline  = 0;     //战斗时间线
        this.Duration  = 0;     //战斗时长
        this.BornPosY  = [];    //Y出生点
        this.FlyMgr    = null;   //飞行道具管理器
        this.Height    = 0;     //战斗场景高
        this.Width     = 0;     //战斗场景宽
        this.UUIDNo    = 0;     //uuid序号
        this.Frame     = 0 ;    //当前帧数
        this.Record    = null;   //战斗过程记录
        this.Pause     = false; //暂停标记
        this.EndReason = 0;     //结束原因
        this.Input     = null;   //战斗输入记录
        this.RecordIndex = 0;
        this.BornBuffTab = [];
        this.CutLength   = 0;
    }
    //初始化函数 (战斗id,随机种子,战斗时长,战斗队伍列表)
    Init(id, seed, duration, teamlist)
    {
        this.FightID    = id;
        this.StartTime  = 10000000;
        this.Timeline   = this.StartTime;
        this.Duration   = duration;
        this.FlyMgr     = new FlyItemMgr(this);
        this.InitBornPosY();
    
        this.Input      = new FightInput();
        this.Input.RandSeed  = seed;
    
        this.Record     = new FightRecords();
        this.Record.Duration = duration;
        this.Record.RandSeed = seed;
        
        //Rand.Init(seed); //初始化随机种子
        //table.insert(this.Record.FrameList, FrameRecords:New())
    
        for(let i = 0;i < teamlist.length; ++i )
        {
            let teaminfo =  teamlist[i];
            let team = new FightTeam();
            team.Init(this, i, teaminfo);
            this.TeamList.push(team);
    
            let list = [];
            list.push(teaminfo.Boss.CardID);
            for(let card of teaminfo.CardList)
                list.push(card.CardID);
            this.Record.TeamList.push(list);
        }
    }
    
    SetFightType(typ)
    {
        this.FightType = typ;
        this.CutLength = FightBehaviorManager.GetCutCount();
    }
    
    //初始化Y轴出生位置
    InitBornPosY()
    {
        let conf  = LoadConfig.getConfigData(ConfigName.CanShu,35);
        this.Height = (conf.Param[1] - conf.Param[0]);
        this.Width  = FightDef.BaseSceneLength/2;
        let gap = Math.floor(this.Height / conf.Param[2]);
        for(let i = 0; i <  this.Height; i += gap)
        {
            this.BornPosY.push(conf.Param[0]+i);
        }
    }
    
    //生成uuid
    GenerateUUID()
    {
        this.UUIDNo = this.UUIDNo + 1;
        return this.UUIDNo;
    }
    
    //当前时间
    CurTm()
    {
        return this.Timeline;
    }
    
    //开始时间
    StartTime()
    {
        return this.StartTime;
    }
    
    //暂停
    Pause(pause)
    {
        this.Pause = pasue;
    }
    
    //获取飞行道具管理器
    GetFlyManager()
    {
        return this.FlyMgr;
    }
    
    //获取队伍
    GetTeam(camp)
    {
        for(let team of this.TeamList)
        {
            if (team.Camp == camp)
                return team;
        }
        return null;
    }
    //获取敌方队伍
    GetEnemyTeam(camp)
    {
        for(let team of this.TeamList)
        {
            if (team.Camp != camp)
                return team;
        }
           
        return null;
    }
    
    //获取队伍成员列表
    GetTeamList(camp)
    {
        let list = [];
        for(let team of this.TeamList)
        {
            if (team.Camp == camp)
            {
                 for(let role of team.RoleList)
                 {
                     if (role.IsVaild())
                        list.push(role);
                 }
                    
            }
        }
    
        return list;
    }
    
    GetTeamListAll(camp)
    {
        let list = [];
        for(let team of this.TeamList)
        {
            if (team.Camp == camp)
            {
                for(let role of team.RoleList)
                    list.push(role);
            }
                
        }
        return list;
    }
    //获取敌人成员列表
    GetEnemyList(camp)
    {
        let list = [];
        for(let team of this.TeamList)
        {
            if (team.Camp != camp)
            {
                for(let role of team.RoleList)
                {
                    if (role.IsVaild())
                        list.push(role);
                }
                    
            }
                
        } 
        return list;
    }
    
    GetEnemyListAll(camp)
    {
        let list = [];
        for(let team of this.TeamList)
        {
            if (team.Camp != camp)
            {
                for(let role of team.RoleList)
                    list.push(role);
            }
                
        }
    
        return list;
    }
    
    //随机函数(1-max)
    Rand(max)
    {
        return Math.floor(Math.random() * (max-1));
    }
        
    
    
    //获取出生点坐标的x值
    GetBornPosX(camp)
    {
        if (camp == FightCampType.My)
            return -this.Width + 10;
        else 
            return this.Width - 10;
    }
    
    //获取出生点坐标的y值
    GetBornPosY(index)
    {
        if (index < 0 || index > this.BornPosY.length)
            return 0;
        
        
        return this.BornPosY[index];
    }
        
    
    
    //获取城堡位置
    GetCastlePos(camp)
    {
        if (camp == FightCampType.My)
            return new cc.Vec2(-this.Width, 300);
        else 
            return new cc.Vec2(this.Width, 300);
    }
        
        
    
    
    IsOutofMap(pos)
    {
        return pos.x > this.Width || pos.x < -this.Width || pos.y > this.Height || pos.y < -this.Height;
    }
    
    //添加战斗记录
    AddRecord(type, cxt)
    {
        let len = this.Record.FrameList.length - 1;
        if (len < 0 || this.Record.FrameList[len].Frame != this.Frame)
        {
            let frame = new FrameRecords();
            frame.Frame = this.Frame;
            this.Record.FrameList.push(frame);
            len = len + 1;
        }
    
        let frame = this.Record.FrameList[len];
        frame.Records.push({RecordT : type, Content : cxt});
        //log("add record frame:"..this.Frame.." index:"..this.RecordIndex.." len1:"..len.." len2:"..#frame.Records)
    }
    
    //获取战斗记录
    GetFightInfo()
    {
        return this.Record;
    }
    
    //获取当前帧的战斗记录
    GetRecords()
    {
        //log("get record frame:"..this.Frame.." index:"..this.RecordIndex.." len1:"..#this.Record.FrameList)
        let frame = this.Record.FrameList[this.RecordIndex];
        if (frame == null)
            return [];
        
    
        //log("get record frame:"..this.Frame.." fra:"..frame.Frame.." len:"..#frame.Records)
        if (frame.Frame == this.Frame - 1)
        {
            this.RecordIndex = this.RecordIndex + 1;
            return frame.Records;
        }
    
        return [];
    }
        
    
    
    //获取输入
    GetInput()
    {
        return this.Input;
    }
    
    BuffNotDone()
    {
        let tab = [];
        let teamlist = this.GetTeamListAll(1);
        for(let role of teamlist)
        {
            let list = role.BuffNotDone();
            if (list.length > 0)
                tab[role.ObjID] = list;
        }
        return tab;
    }
       
    
    
    GetBornBuff(objid)
    {
        return this.BornBuffTab[objid];
    }
    
    SetBornBuffTab(tab)
    {
        this.BornBuffTab = tab;
    }
    
    //更新函数
    Update()
    {
        if(this.Pause)
            return false;
        
    
        //超时判断
        if (this.Timeline - this.StartTime >= this.Duration)
        {
            this.Frame = this.Frame + 1;
            this.Done(1);
            return true;
        }
    
        //战斗队伍update
        for(let team of this.TeamList)
            team.Update(FightDef.FREQUENCY);
        
    
        this.FlyMgr.Run(this.CurTm());  //飞行道具
        for(let team of this.TeamList)
        {
            if (team.IsDead())
            {
                this.Frame = this.Frame + 1;
                this.Done(2);
                return true;
            }
        }
        this.Timeline = this.Timeline + FightDef.FREQUENCY;
        this.Frame = this.Frame + 1;
    
        return false;
    }
    
    //使用主动技
    UseActiveSkill(id)
    {
        let team = this.GetTeam(1);
        if (!team.UseActiveSkill(id))
            return;
        
    
        let frameinput = this.Input.InputList[this.Frame];
        if (!frameinput)
            frameinput = [];
        
    
        let item = {Camp : 1, SkillID : id};
        frameinput.push(item);
        this.Input.InputList[this.Frame] = frameinput;
    }
        
    
    
    Run(){}
        // body
    
    
    //战斗结果
    Ret()
    { 
        if (this.EndReason == 1)
            return 2;
        else
        {
            for(let team of this.TeamList)
            {
                if (!team.IsDead())
                    return team.Camp;
            }
        }
    }
    //战斗结束
    Done(reason)
    {
        this.EndReason = reason;
        this.Record.RealTime = this.Timeline - this.StartTime;
        for (let i = 0; i < this.TeamList.length; ++i)
        {
            let team = this.TeamList[i];
            if (team.TotalBlood != BigNumber.zero)
                this.Record.TeamList[i].DropPerc = BigNumber.div(team.DropBlood, team.TotalBlood);
        }
            
    }
    
    //查找敌人
    Search(master, target, skillid)
    {
        let skill_conf = LoadConfig.getConfigData(ConfigName.JiNeng,skillid);
        if (null == skill_conf)
            return [];
        
    
        let list = [];
        if (skill_conf.SearchType == SearchType.Target)
            list.push(target);
        else if( skill_conf.SearchType == SearchType.Near)
            list = this.NearWithoutSearch(master, master, skill_conf, null);
        else if( skill_conf.SearchType == SearchType.Self)
            list.push(master);
        else if( skill_conf.SearchType == SearchType.Team)
            list = this.TeamSearch(master, skill_conf);
        else if( skill_conf.SearchType == SearchType.NearWithoutMe)
            list = this.NearWithoutSearch(master, master, skill_conf, master);
        else if( skill_conf.SearchType == SearchType.NearWithoutTarget)
            list = this.NearWithoutSearch(master, master, skill_conf, target);
        else if( skill_conf.SearchType == SearchType.NearTargetWithoutTarget)
            list = this.NearWithoutSearch(master, target, skill_conf, target);
        else if( skill_conf.SearchType == SearchType.NearSideBySide)
            list = this.NearSideBySide(master, skill_conf);
        
    
        list  = SelectTarget(list, skill_conf.Priority, skill_conf.HurtCount);
            
        return list;
    }
    
    GetSkillEnemyList(master, conf)
    {
        if (master.GetBuffCount(FightBuffType.Charm) > 0 || conf.Target == 1)
            return this.GetTeamList(master.Camp);
        
    
        return this.GetEnemyList(master.Camp);
    }
        
    
    
    //搜索技能的攻击对象 排除without
    NearWithoutSearch(master, dest, conf, without)
    {
        let list = this.GetSkillEnemyList(master, conf);
    
        let targets = [];
        let disarr  = [];
        for(let target of list)
        {
            if (target == without) //排除对象
                continue;

            if (!master.CheckIsTarget(target, conf)) //判断是否为攻击对象
                continue;
            
            let ok, dis = dest.CheckCanHit(target, conf);
            if (!ok)            //判断是否在射程范围内
                continue;
            

            //插入排序 按照距离远近进行排序
            targets.push(target);
            disarr.push( dis);
            for(let i = disarr.length; i >= 1; --i)
            {
                if (disarr[i] < disarr[i-1])
                {
                    let tmp   = disarr[i-1];
                    disarr[i-1] = disarr[i];
                    disarr[i]   = tmp;

                    tmp = targets[i-1];
                    targets[i-1] = targets[i];
                    targets[i]   = tmp;
                }
                else
                    break;
            }
        }
    
        return targets;
    }
        
    //队伍搜索
    TeamSearch(master, skill_conf)
    {
        return this.GetSkillEnemyList(master, skill_conf);
    }
    
    //搜索一条线的敌人
    NearSideBySide(master, skill_conf)
    {
        let list = this.GetSkillEnemyList(master, skill_conf);
    
        let width = master.GetHurtRange(skill_conf) / 2;
        let targets = [];
        for(let target of list)
        {
            if (master.CheckIsTarget(target, skill_conf))
            {
                if (target.Pos.x >= master.Pos.x - width && target.Pos.x <= master.Pos.x + width)
                    targets.push(target);
            }
        }
        return targets;
    }
        
    
    
    //搜索某个位置附近的可以被攻击的敌人
    SearchPos(pos, master, skill_conf)
    {
        let list = this.GetSkillEnemyList(master, skill_conf);
    
        let targets = [];
        let distance= master.GetHurtRange(skill_conf);
        for(let target of list)
        {
            if (master.CheckIsTarget(target, skill_conf))
            {
                let dis  = target.Pos;
                dis = dis.sub(pos);
                let disv = dis.magSqr();
                if (disv <= distance * distance) 
                    targets.push(target);
            }
        }
    
        targets  = SelectTarget(targets, skill_conf.Priority, skill_conf.WoundCount);
         
        return targets;
    }
    
    //寻找目标背后的人
    TargetBehind(master, target, skill_conf)
    {
        let list = this.GetTeamList(target.Camp);
        let team = this.GetTeam(target.Camp);
    
        let mindis = 1000000;
        let mintarget = null;
        for(let role of list)
        {
            if (role.UUID != target.UUID && master.CheckIsTarget(role, skill_conf))
            {
                let ok, dis = target.CheckCanHit(role, skill_conf);
                if (ok)
                {
                    if ((target.Camp == 2 && role.Pos.x > target.Pos.x) ||
                       (target.Camp == 1 && role.Pos.x < target.Pos.x))
                       {
                           if (dis < mindis)
                            {
                                mindis = dis;
                                mintarget = role;
                            }
                       }
                }
            }
        }
        return mintarget;
    }
       
    
    
    //寻找矩形内的人
    RectSearch(master, pos, w, h, skill_conf)
    {
        let list = this.GetSkillEnemyList(master, skill_conf);
        let targets = [];
        //log("rect w:"..w.." h:"..h.." camp:"..master.Camp.." target:"..skill_conf.Target.." c:"..#list)
        for(let target of list)
        {
            if (master.CheckIsTarget(target, skill_conf))
            {
                if (target.Pos.x >= pos.x - w && target.Pos.x <= pos.x + w && 
                   target.Pos.y >= pos.y - h && target.Pos.y <= pos.y + h)
                   {
                       targets.push(target);
                   }
            }
        }
        return targets;
    }
    
    //寻找对象，溅射使用，排除圆心对象
    SplashSearch(target, camp, distance)
    {
        let list = this.GetTeamList(camp);
    
        let targets = [];
        for(let role of list)
        {
            if (target.UUID != role.UUID)
            {
                let dis  = role.Pos;
                dis = dis.sub(target.Pos);
                let disv = dis.magSqr();
                if (disv <= distance * distance) 
                    targets.push(role);
            }
        }
        return targets;
    }
    
    //寻找闪现目标
    TeleportSearch(master, skill_conf)
    {
        let list = this.GetSkillEnemyList(master, skill_conf);
        let targets = [];
        let disarr  = [];
        for(let target of list)
        {
            if (!master.CheckIsTarget(target, skill_conf))
                continue;
            

            let [ok, dis] = master.CheckCanHitByRange(target, skill_conf.Range);
            if (!ok)
                continue;
            

            //插入排序 按照距离远近进行排序
            targets.push(target);
            disarr.push( dis);
            for(let i = disarr.length; i >= 1; --i)
            {
                if(disarr[i] < disarr[i-1])
                {
                    let tmp   = disarr[i-1];
                    disarr[i-1] = disarr[i];
                    disarr[i]   = tmp;

                    tmp = targets[i-1];
                    targets[i-1] = targets[i];
                    targets[i]   = tmp;
                }
                else
                    break;
            }
        }
        targets = SelectTarget(targets, skill_conf.Priority, 1);
        return targets[0];
    }
    
    RandomOneBuff(conf)
    {
        let list  = [];
        let total = 0;
        for(let i = 0;i < conf.BuffID.length; ++i)
        {
            total = total + conf.Prob[i];
        }
    
        let rand  = this.Rand(total);
        for(let i = 0;i < conf.BuffID.length; ++i)
        {
            let buffid = conf.BuffID[i];
            rand = rand - conf.Prob[i];
            if (rand <= 0)
            {
                let item  = {};
                item.buffid  = buffid;
                item.duration= conf.Duration[i]/10;
                item.params  = [conf.Params[(i-1)*4+1],conf.Params[(i-1)*4+2],conf.Params[(i-1)*4+3],conf.Params[(i-1)*4+4]];
                list.push(item);
                break;
            }
        }
        
        return list;
    }
        
    
    
    RandomAllBuff(conf)
    {
        let list = [];
        for(let i = 0;i < conf.BuffID.length; ++i)
        {
            let buffid = conf.BuffID[i];
            if (this.Rand(10000) <= conf.Prob[i])
            {
                let item  = {};
                item.buffid  = buffid;
                item.duration= conf.Duration[i]/10;
                item.params  = [conf.Params[(i-1)*4+1],conf.Params[(i-1)*4+2],conf.Params[(i-1)*4+3],conf.Params[(i-1)*4+4]];
                list.push(item);
            }
        }
        
        return list;
    }
    
    RandomBuffByPoolID(poolid)
    {
        let conf = LoadConfig.getConfigData(ConfigName.BuffPool,poolid);
        if (!conf)
            return [];
        
    
        if (conf.PoolT == 1)
            return this.RandomOneBuff(conf);
        else 
            return this.RandomAllBuff(conf);
    }
    
    //添加出生记录
    AddBornRecord(role, revive)
    {
        let record = new BornRecord();
        record.UUID  = role.UUID;
        record.ID    = role.ID;
        record.Camp  = role.Camp;
        record.Pos   = role.Pos;
        record.MaxHP = role.MaxHP;
        record.Type  = role.Type;
        record.HP    = role.HP;
    
        if (revive)
            this.AddRecord(RecordT.RecordT_Revive, record);
        else
            this.AddRecord(RecordT.RecordT_Born, record);
    }
    //添加死亡记录
    AddDieRecord(role)
    {
        let record = new DieRecord();
        record.UUID  = role.UUID;
    
        this.AddRecord(RecordT.RecordT_Die, record);
    }
    
    //添加移动记录
    AddMoveRecord(role, pos)
    {
        let record = new MoveRecord();
        record.UUID  = role.UUID;
        record.Speed = role.GetSpeed();
        if (pos != null)
            record.Pos = new cc.Vec2(pos.x, pos.y);
        else if( role.Target != null)
            record.TargetUUID = role.Target.UUID;
        
    
        record.CurPos  = new cc.Vec2(role.Pos.x, role.Pos.y);
        this.AddRecord(RecordT.RecordT_Move, record);
    }
    
    //添加攻击记录
    AddAttackRecord(role, skillid)
    {
        if (role.Target == null || !skillid)
            return ;
        
    
        let record = new AttackRecord();
        record.UUID  = role.UUID;
        record.TargetUUID = role.Target.UUID;
        record.SkillID = skillid;
        record.ASPD  = role.GetASPD();
    
        this.AddRecord(RecordT.RecordT_Attack, record);
    }
        
    //添加站立记录
    AddStandRecord(role)
    {
        let record = new StandRecord();
        record.UUID  = role.UUID;
    
        this.AddRecord(RecordT.RecordT_Stand, record);
    }
    
    //加血记录
    AddIncHPRecord(role, hp)
    {
        let record = new AddHPRecord();
        record.UUID  = role.UUID;
        record.HP    = hp;
    
        this.AddRecord(RecordT.RecordT_IncHP, record);
    }
    
    //受击记录
    AddHurtRecord(role, hurttype, hurtid, hurt, masterid)
    {
        let record = new HurtRecord();
        record.UUID    = role.UUID;
        record.Hurt    = hurt;
        record.HurtSrc = hurttype;
        record.HurtSrcID = hurtid;
        record.MasterUUID= masterid;
    
        this.AddRecord(RecordT.RecordT_Hurt, record);
    }
    //添加buff记录
    AddIncBuffRecord(role, buffid, lsttime, effectv)
    {
        let record = new AddBuffRecord();
        record.UUID  = role.UUID;
        record.BuffID= buffid;
        record.LstTime = lsttime;
        record.EffectV = effectv;
    
        this.AddRecord(RecordT.RecordT_AddBuff, record);
    }
    
    //删除buff记录
    AddRmvBuffRecord(role, buffid)
    {
        let record = new RmvBuffRecord();
        record.UUID  = role.UUID;
        record.BuffID= buffid;
    
        this.AddRecord(RecordT.RecordT_RmvBuff, record);
    }
    
    //飘字记录
    AddHudRecord(role, type)
    {
        let record = new HudRecord();
        record.UUID  = role.UUID;
        record.Type  = type;
    
        this.AddRecord(RecordT.RecordT_Hud, record);
    }
    
    //飞行道具记录
    AddFlyItemBornRecord(role, targetid, skillid, itemid, flytime)
    {
        let record = new FlyItemBornRecord();
        record.UUID  = role.UUID;
        record.TargetUUID = targetid;
        record.SkillID    = skillid;
        record.FlyItemID  = itemid;
        record.FlyTime    = flytime;
    
        this.AddRecord(RecordT.RecordT_FlyItem, record);
    }
    
    //闪现记录
    AddTeleportRecord(role, pos, time, skillid)
    {
        let record = new TeleportRecord();
        record.UUID  = role.UUID;
        record.Pos   = pos;
        record.Time  = time;
        record.SkillID = skillid;
    
        this.AddRecord(RecordT.RecordT_Teleport, record);
    }
    
    //闪现记录
    AddUseASkillRecord(skillid)
    {
        let record = new UseASkillRecord();
        record.SkillID = skillid;
    
        this.AddRecord(RecordT.RecordT_UseASkill, record);
    }
    
    //添加击退记录
    AddRepelRecord(role, buffid, lsttm, height, length)
    {
        let record = new RepelRecord();
        record.UUID  = role.UUID;
        record.BuffID = buffid;
        record.Height = height;
        record.Length = length;
        record.LstTime= lsttm;
    
        this.AddRecord(RecordT.RecordT_Repel, record);
    }
    
    //添加穿越飞行道具记录
    AddCrossFlyItemRecord(role, skillid, flyid, speed, dis)
    {
        let record = new CrossFlyItemRecord();
        record.UUID = role.UUID;
        record.SkillID = skillid;
        record.FlyItemID = flyid;
        record.Speed = speed;
        record.Distance = dis;
    
        this.AddRecord(RecordT.RecordT_CrossFly, record);
    }
    //添加冲锋记录
    AddAssaultRecord(role, target, skillid, aspd, speed, pos)
    {
        let record = new SprintRecord();
        record.UUID  = role.UUID;
        record.TargetUUID = target.UUID;
        record.SkillID = skillid;
        record.ASPD  = aspd;
        record.Speed = speed;
        if (pos)
        {
            record.Pos   = pos;
            record.UsePos= true;
        }
        
        this.AddRecord(RecordT.RecordT_Sprint, record);
    }
    
    //添加拉人记录
    AddPullRecord(role, target, speed, pos)
    {
        let record = new PullRecord();
        record.UUID  = role.UUID;
        record.TargetUUID = target.UUID;
        record.Speed = speed;
        record.Pos   = pos;
    
        this.AddRecord(RecordT.RecordT_Pull, record);
    }
    //拉人结束记录
    AddPullEndRecord(role, pos)
    {
        let record = new PullFinishRecord();
        record.UUID  = role.UUID;
        record.Pos   = pos;
    
        this.AddRecord(RecordT.RecordT_PullFinish, record);
    }
        
    
    
    //冲锋结束记录
    AddAssaultEndRecord(role, pos)
    {
        let record = new SprintFinishRecord();
        record.UUID  = role.UUID;
        record.Pos   = pos;
    
        this.AddRecord(RecordT.RecordT_SprintFinish, record);
    }
    //受击前置记录
    AddPreHurtRecord(role, skillid)
    {
        let record = new PreHurtRecord();
        record.UUID  = role.UUID;
        record.SkillID = skillid;
    
        this.AddRecord(RecordT.RecordT_PreHurt, record);
    }
    
}