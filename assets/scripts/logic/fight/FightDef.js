/*
作者（Author）:    jason

描述（Describe）: 定义战斗中使用的一些常量 
*/
/*jshint esversion: 6 */

export const FightDef = {
    BaseSearchTime      : 0.05, //最短搜索时间间隔
    BaseTimeScale       : 1,    //当前的时标
    BaseSceneLength     : 900,  //场景长度
    BaseEnterTimeGap    : 300,  //上阵间隔时间
    AITimeGap           : 0.1,  //AI思考时间间隔
    FREQUENCY           : 100,  //战斗间隔
    EndFightTime        : 1    //战斗结束时间
};

//特殊技能类型
export const SpecialSkillType = {
	TianFu                        : 1 , //1：天赋类型
	TianFuBeiBao                  : 2 , //2：天赋类型背包生效
	Hurt                          : 3 , //3：受击时触发
	Die                           : 4 , //4：死亡时触发
	InFight                       : 5 , //5：入战场时触发
	Respawn                       : 6 , //6：复活特殊能力
	GuildXunLianZhongXin          : 7 , //7：道馆训练中心生效
};

//攻击类型
export const FightHurtType = {
    Physical    : 1,//物理攻击
    Magical     : 2,//魔法攻击
};

//战斗属性枚举
export const FightAttr = {
    ATK                 : 0,    //攻击
    HP                  : 1,    //当前血量
    DEF                 : 2,    //物防
    MDEF                : 3,    //法防
    ASPD                : 4,    //攻速（60去除）攻速的加成
    Speed               : 5,    //移速（除以10）移速的加成
    Range               : 6,    //普攻技能射程
    CRI                 : 7,    //暴率（除以一万）
    CritDamage          : 8,    //暴伤（除以一万）
    HitRate             : 9,   //命中率
    DodgeRate           : 10,   //闪避率
    Respawn             : 11,   //复活时间
    Vision              : 12,   //视野范围
    Anger               : 13,   //怒气
    MaxHP               : 14,   //最大血量
	SkillRange          : 15,   //大招技能射程
	AttackRange         : 16,   //普攻攻击范围
	SkillAttackRange    : 17,   //大招攻击范围
    Max                 : 18,   //最大值
};

//战斗特殊属性枚举
export const FightSpecialAttr = {
    Ice_Hurt         : 1, //受到冰攻击伤害增加
	Dragon_Hurt      : 2, //受到龙攻击伤害增加
	Fire_Hurt        : 3, //受到火攻击伤害增加
	Grass_Hurt       : 4, //受到草攻击伤害增加
	Water_Hurt       : 5, //受到水攻击伤害增加
	Soil_Hurt        : 6, //受到地面攻击伤害增加
	Electricity_Hurt : 7, //受到电攻击伤害增加
	SuperPower_Hurt  : 8, //受到超能力攻击伤害增加
	Poison_Hurt      : 9, //受到毒攻击伤害增加
	Fairy_Hurt       : 10, //受到妖精攻击伤害增加
	Wrestle_Hurt     : 11, //受到格斗攻击伤害增加
	Steel_Hurt       : 12, //受到钢攻击伤害增加
	Rock_Hurt        : 13, //受到岩石攻击伤害增加
	Worm_Hurt        : 14, //受到虫攻击伤害增加
	Evil_Hurt        : 15, //受到恶攻击伤害增加
	Ghost_Hurt       : 16, //受到幽灵攻击伤害增加
	Ordinary_Hurt    : 17, //受到一般攻击伤害增加
	RestoreHP        : 18, //5秒恢复 【x】% 生命
	EasyHurt         : 19, //BOSS易伤 【x】 秒
	Max              : 20,
};

//战斗状态
export const FightRoleState = {
    Born    : 1,    //出生
    Stand   : 2,    //站立
    Move    : 3,    //移动
    Attack  : 4,    //攻击  
    Die     : 5,    //死亡
    Wound   : 6,    //受击
    Max     : 7,    //最大值
};

//攻击距离类型
export const FightDistanceType = {
    Melee   : 1,    //近战
    Remote  : 2,    //远程
};

export const FightTargetLimitType = {
//0，不限制。1，地面部队。2，地面远程部队。3，空中部队。4，地面近战。5，远程部队（地面和空中）
    NoLimit         : 0,    //0，不限制
    LandForce       : 1,    //1，地面部队
    LandRemoteForce : 2,    //2，地面远程部队
    AirForce        : 3,    //3，空中部队
    LandMeleeForce  : 4,    //4，地面近战
    RemoteForce     : 5,    //5，远程部队（地面和空中）
};

export const FightTroopType = {
    Air         : 1,    //空中兵
    Land        : 2,    //地面兵
    LandSuspend : 3,
};

//阵营
export const FightCampType = {
    My      : 0,    //我军
    Enemy   : 1,    //敌军
};

//技能伤害类型
export const FightSkillHurtType = {
    Single  : 1,    //单体
    Range   : 2,    //范围
    Splash  : 3,    //溅射
};

//战斗条件判定类型 与 或
export const FightConditionType = {
    And : 1,    //与，
    Or  : 2,    //或，
};

//技能表现类型
export const FightShowType = {
    NoAttack        : 0,    //无攻击表现
    Attack          : 1,    //近战
    Shot            : 2,    //飞行道具
    Summon          : 3,    //召唤
    AttackAndShot   : 4,    //近战+飞行道具
    Teleport        : 5,    //闪现
    ChainLightning  : 6,    //真闪电链
    Assault         : 7,    //冲锋 
    AssaultEx       : 8,    //贯穿 反复冲锋
    Pull            : 9,    //拉人
    Beam            : 10,   //光束
    Splash          : 11,   //溅射
};

//技能搜索类型
export const FightSearchType = {
    Single  : 1,    //单体
    Range   : 2,    //范围
    Crystal : 3,    //水晶
};

//飞行道具类型
export const FightFlyItemType = {
    Line        : 1,    //直线
    Parabola    : 2,    //抛物线
    Cross       : 3,    //穿透射击
    Chain       : 4,    //弹弹
    Trail       : 5,    //拖尾
};

//Buff类型
export const FightBuffType = {
    Coma        : 1,    //昏迷
    Summon      : 2,    //召唤   //溅射使用召唤完成
    PI          : 3,    //物理免疫
    MI          : 4,    //魔法免疫
    AddHP       : 5,    //单次回血
    SlowDown    : 6,    //减速
    Splash      : 7,    //溅射
    Repel       : 8,    //击退
    Off         : 9,    //击飞
    Frozen      : 10,   //冰冻
    Paralysis   : 11,   //麻痹
    Silence     : 12,   //沉默
    Curse       : 13,   //诅咒
    Invincible  : 14,   //无敌
    Dodge       : 15,   //闪避
    Hiding      : 16,   //隐身
    DropBlood   : 17,   //持续掉血
    Shield      : 18,   //护盾
    AddATK      : 19,   //增加物理攻击
    AddMATK     : 20,   //增加法术攻击
    AddASPD     : 21,   //增加攻击速度
    AddSpeed    : 22,   //增加移动速度
    ImmComa     : 23,   //免疫晕
    ImmFrozen   : 24,   //免疫冰冻
    ImmRepel    : 25,   //免疫击退
    ImmOff      : 26,   //免疫击飞
    Winding     : 27,   //缠绕
    Shock       : 28,   //震荡
    Interfere   : 29,   //干扰
    Poisoning   : 30,   //中毒
    Vampire     : 31,   //吸血
    Intimidate  : 32,   //恐吓
    GhostCurse  : 33,   //鬼咒
    Deter       : 34,   //威慑
    Rebound     : 35,   //反弹
    Charm       : 36,   //魅惑
    Petrifaction: 37,   //石化
    Ridicule    : 38,   //嘲讽
    IntoVoid    : 39,   //遁入虚空
    Purify      : 40,   //净化
    AddCrit     : 41,   //增加暴击率
    AddCritDmg  : 42,   //增加暴击伤害
    AddDEF      : 43,   //增加防御值
    AddMDEF     : 44,   //增加法术防御值
    ImmControl  : 45,   //免疫控制
    DecHit      : 46,   //减命中
    ImmSlow     : 47,   //免疫减速
    DecATK      : 48,   //减物攻
    DecMATK     : 49,   //减法攻
};

//战斗系统类型
export const FightSystemType = {
    GuanQia      : 1,    //关卡
    Arena        : 2,    //竞技场
    DiXiaCheng   : 3,    //地下城
    Demo         : 4,    //演示战斗
    FightTower   : 5,    //挑战塔
    FriendBattle : 6,    //好友切磋
    Plateau      : 7,    //精灵高原
    GuildBoss    : 8,    //公会boss
    PeakArena    : 9,    //巅峰竞技场
    Newbie       : 10,   //新手战斗
    GuildFight   : 11,   //道馆战
};

export const FightRaceType = {
	Ice          : 1 , //冰
	Dragon       : 2 , //龙
	Fire         : 3 , //火
	Grass        : 4 , //草
	Water        : 5 , //水
	Soil         : 6 , //地面
	Electricity  : 7 , //电
	SuperPower   : 8 , //超能力
	Poison       : 9 , //毒
	Fairy        : 10, //妖精
	Wrestle      : 11, //格斗
	Steel        : 12, //钢
	Rock         : 13, //岩石
	Worm         : 14, //虫
	Evil         : 15, //恶
	Ghost        : 16, //幽灵
	Ordinary     : 17, //一般

};

//Buff光效位置
export const FightEffectRootType = {
    Head           : 1,    //头顶
    Hit            : 2,    //受击点
    Root           : 3,    //0点
    HeadWorld      : 4,    //头顶，世界坐标
    HitWorld       : 5,    //受击点，世界坐标
    RootWorld      : 6,    //0点，世界坐标
    RootWorldYZero : 7,    //0点，世界坐标，Y轴为0
};

//
export const FightPathType = {
    Parent : 0,
    Role : 1,
    Hit : 2,
    Head : 3,
    UI : 4,
    Blood : 5,
    Hud : 6,
    Max : 7,
};
//主动技伤害类型
export const FightActiveSkillHurtType = {
    Floor       : 1, //1、按层数：伤害:1.05^(主线关卡最大记录层数-参数1)*参数2+参数3
    Level       : 2, //2、按等级：伤害:1.05^(角色等级*参数1)*参数2+参数3
    MaxAttack   : 3, //3、按最高攻击：伤害:最高攻击*参数1
};


//主动技伤害类型
export const FightActiveSkillTargetType = {
    My : 1,
    Enemy : 2,
    MyAndBoss : 3,
    EnemyAndBoss : 4,
};

// FightRole挂点枚举
export const FightRolePoint = {
    Head:-1,   //头顶
    Hit:-1,    //受击点
    Parent:-1, //父对象
};

//SpecialSkillType 特殊技能类型
export const FightSpecialSkillType = {
    TianFu : 1,
    TianFuBackpack : 2,
    OnHit : 3,
    OnDead : 4,
    OnBorn : 5,

};

//士兵类型
export const RoleT = {
	Castle	 : 1,
	Normal 	 : 2,
	Summoner : 3, 
};

//寻找敌人的几种方式
export const SearchType = {
	Target 		        	: 1,//目标
	Near    	        	: 2,//搜索自身附近的人
	Self 		         	: 3,//对自身起效果
	Team 		  	        : 4,//队伍
	NearWithoutMe 	        : 5,//搜索自身附近的人，排除自身
	NearWithoutTarget       : 6,//搜索自身附近的人，排除目标对象
	NearTargetWithoutTarget : 7,//搜索目标附近的人，排除目标对象
	NearSideBySide          : 8,//搜索自身附近的人，以z轴作为线，获取并排的目标
};

//搜索敌人游戏类型
export const PriorityT = {
    Melee    : 1,  //近战 
    Remote   : 2,  //远程
    Physical : 3,  //物理
    Magic    : 4,  //魔法
};

//净化类型
export const PurifyT = {
    BuffMainT   : 1,
    BuffSubT    : 2,
};

//buff大类
export const BuffMT = {
    Gain        : 1, //增益   
    Benefit     : 2, //减益
    Control     : 3, //控制
    Shield      : 4, //护盾
};

export const RoleRef = {
    NoAttack    : 1,
    NoMove      : 2,
    Hiding      : 3,
    LockTarget  : 4,
    LockState   : 5,
    Invincible  : 6,
    ImmControl  : 7,
    ImmSlow     : 8,
    Max         : 8,
};