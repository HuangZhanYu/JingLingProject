/*
作者（Author）:    jason
 
描述（Describe）: 溅射
*/
/*jshint esversion: 6 */
import Buff, { RegisterBuff } from "./Buff";
import { FightBuffType } from "../FightDef";


export default class SplashBuff extends Buff
{
    constructor(type, id, master, owner, duration, params)
    {
        super(type, id, master, owner, duration, params);
    }
    IsParamsVaild()
    {
        return this.Params.length >= 2;
    }
    Start(tm)
    {
        this.EndTime = tm + Math.floor(this.Params[0] / 10);
    }
    Done()
    {
        let skillid = this.Params[0];
        let skill_conf = LoadConfig.getConfigData(ConfigName.JiNeng,skillid);
        if (!skill_conf)
            return;

        //-log("splash range:"..this.Owner:GetRange(skill_conf.Range))
        let targets = this.Master.Manager.SplashSearch(this.Owner, this.Owner.Camp, this.Owner.GetAttackRange(skill_conf));
        targets  = SelectTarget(targets, skill_conf.Priority, skill_conf.WoundCount);
        for(let i = 0; i < targets.length; ++i)
        {
            let target = targets[i];
            target.Wound(this.Master, skill_conf, HurtT.Splash);
            if ( i >= skill_conf.WoundCount)
            {
                break;
            }
        }
    }
}
RegisterBuff(FightBuffType.Splash, new SplashBuff(0, 0, null, null, 0, []));
