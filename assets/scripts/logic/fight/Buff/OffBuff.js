/*
作者（Author）:    jason

描述（Describe）: 击飞
*/
/*jshint esversion: 6 */
import Buff, { RegisterBuff } from "./Buff";
import { FightBuffType, FightDef, FightCampType } from "../FightDef";
export default class OffBuff extends Buff
{
    constructor(type, id, master, owner, duration, params)
    {
        super(type, id, master, owner, duration, params);
    }
    IsParamsVaild()
    {
        return this.Params.length >= 2;
    }
    GetEffectParam()
    {
        return this.Length;
    }
    Start()
    {
        //计算抗性影响
        let height = this.Params[0];
        let length = this.Params[1];
        let resist = this.Owner.GetBuffResist(FightBuffType.Off);
        if (resist != 0)
        {
            let val = 1 - resist / 10000;
            this.EndTime = this.StartTime + (this.EndTime - this.StartTime) * val;
            length = length * val;
            height = height * val;
        }

        //计算击飞速度
        this.Owner.ChangeState(FightRoleState.Stand);
        this.Length = length;
        this.Speed  = length / (this.EndTime - this.StartTime) * 1000;
        if (this.Owner.Camp == FightCampType.My)
        {
            this.Speed = -this.Speed;
            length = -length;
        }

        let lsttm = this.EndTime + this.DoneDelay() - this.StartTime;
        this.Owner.Manager.AddRepelRecord(this.Owner, this.ID, lsttm, height, length);
    }
    Interval()
    {   
        return FightDef.FREQUENCY;
    }
    Do(times)
    {
        let x = times * this.Interval() * this.Speed / 1000;
        let pos = this.Owner.Pos;
        let manager = this.Owner.Manager;
        if ((pos.x >= manager.Width && x > 0) || (pos.x <= -manager.Width && x < 0))
            return;
        

        pos.x = pos.x + x;
        if (pos.x > manager.Width)
            pos.x = manager.Width;
        else if (pos.x < -manager.Width)
            pos.x = -manager.Width;
        
    }
    DoneDelay()
    {
        return 250;
    }
}
RegisterBuff(FightBuffType.Off, new OffBuff(0, 0, null, null, 0, []));
