/*
作者（Author）:    jason

描述（Describe）: 威慑
*/
/*jshint esversion: 6 */
import Buff, { RegisterBuff } from "./Buff";
import { FightBuffType, FightCampType } from "../FightDef";
export default class DeterBuff extends Buff
{
    constructor(type, id, master, owner, duration, params)
    {
        super(type, id, master, owner, duration, params);
    }
    IsParamsVaild()
    {
        return true;
    }
    GetEffectParam()
    {
        return this.Params[0];
    }
    Start()
    {
        let owner  = this.Owner;
        let direct = 1;
        if (owner.Camp == FightCampType.My)
            direct = -1;

        let speed = owner.Speed;
        this.Pos = owner.Pos;
        this.Pos = this.Pos.add(new cc.Vec2(direct, 0) * speed);

        owner.AddRef(RoleRef.NoAttack, 1);
        if (owner.CheckCanMove())
        {
            owner.AddRef(RoleRef.LockState, 1);
            owner.ChangeState(FightRoleState.Move, this.Pos);
        }
    }
    Do(times)
    {

    }
    Done()
    {
        this.Owner.DecRef(RoleRef.NoAttack, 1);
        this.Owner.DecRef(RoleRef.LockState, 1);
    }
}
RegisterBuff(FightBuffType.Deter, new DeterBuff(0, 0, null, null, 0, []));
