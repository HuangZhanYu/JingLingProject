/*
作者（Author）:    jason

描述（Describe）: 缠绕
*/
/*jshint esversion: 6 */
import Buff, { RegisterBuff } from "./Buff";
import { FightBuffType, RoleRef, FightRoleState } from "../FightDef";
export default class WindingBuff extends Buff
{
    constructor(type, id, master, owner, duration, params)
    {
        super(type, id, master, owner, duration, params);
    }
    IsParamsVaild()
    {
        return true;
    }
    GetEffectParam()
    {
        return 0;
    }
    Start()
    {
        this.Owner.AddRef(RoleRef.NoMove, 1);
        this.Owner.ChangeState(FightRoleState.Stand, null);
    }
    Done()
    {
        this.Owner.DecRef(RoleRef.NoMove, 1);
    }
}
RegisterBuff(FightBuffType.Winding, new WindingBuff(0, 0, null, null, 0, []));

