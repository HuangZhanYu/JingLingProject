/*
作者（Author）:    jason

描述（Describe）: 召唤
*/
/*jshint esversion: 6 */
import Buff, { RegisterBuff } from "./Buff";
import { FightBuffType } from "../FightDef";


import FightRole from "../FightRole";
import Card from "../../Card";
import GFightManager from "../GFightManager";
export default class SummonBuff extends Buff
{
    constructor(type, id, master, owner, duration, params)
    {
        super(type, id, master, owner, duration, params);
    }
    IsParamsVaild()
    {
        return this.Params.length >= 4;
    }
    Start(tm)
    {
        let pos = null;
        if (this.Params[0] == 1)
            pos = this.Master.Pos;
        else
            pos = this.Owner.Pos;

        let manager = this.Master.Manager;
        if (this.Params[2] == 1)
        {
            let v = manager.Rand(this.Params[1]*2);
            pos.x = pos.x + (v-this.Params[1])/10000;
            pos.y = pos.y + (v-this.Params[1])/10000;
            if (pos.y > manager.Height)
                pos.y = manager.Height;
            else if (pos.y < -manager.Height)
                pos.y = -manager.Height;
        }
        else
        {
            pos.x = pos.x + this.Params[1]/10000;
        }

        if (pos.x > manager.Width)
            pos.x = manager.Width;
        else if (pos.x < -manager.Width)
            pos.x = -manager.Width;

        let roleid = this.Params[0];
        let conf   = LoadConfig.getConfigData(ConfigName.ShiBing,roleid);
        if (!conf)
            return;

        let card = Card.NewByID(roleid, 1, 0);
        let fightcard = GFightManager.CreateFightCard(card);
        let team = manager.GetTeam(this.Master.Camp);
        let role = new FightRole(manager, RoleT.Summoner, this.Master.Camp, fightcard);
        role.MaxHP = this.Master.MaxHP;
        role.HP    = this.Master.MaxHP;
        role.ATK   = this.Master.ATK;
        role.DEF   = this.Master.DEF;
        role.MDEF  = this.Master.MDEF;
        role.FightLevel = this.Master.FightLevel;
        role.Born(team, pos, this.EndTime - this.StartTime);
        team.AddFightRole(role);
        this.EndTime = tm;
    }
}
RegisterBuff(FightBuffType.Summon, new SummonBuff(0, 0, null, null, 0, []));
