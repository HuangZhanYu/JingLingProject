/*
作者（Author）:    jason

描述（Describe）: 持续掉血
*/
/*jshint esversion: 6 */
import Buff, { RegisterBuff } from "./Buff";
import { FightBuffType } from "../FightDef";
import BigNumber from "../../../tool/BigNumber";
export default class DropBloodBuff extends Buff
{
    constructor(type, id, master, owner, duration, params)
    {
        super(type, id, master, owner, duration, params);
    }
    IsParamsVaild()
    {
        return this.Params.length >= 4;
    }
    GetEffectParam()
    {
        return this.Params[2]/10;
    }
    Start(tm)
    {
        this.Value = "0";
        if (this.Params[0] == 1)
        {   
            this.Value = BigNumber.mul(this.Master.ATK, this.Params[0]);
            this.Value = BigNumber.div(this.Value, BigNumber.tenThousand);
        }
        else if (this.Params[0] == 2)
        {
            this.Value = BigNumber.mul(this.Params[1], this.Params[0]);
            this.Value = BigNumber.div(this.Value, BigNumber.tenThousand);
        }
    }
    Do(times)
    {
        let value = this.Value;
        if(times > 1)
            value = BigNumber.mul(value, times.toString());
        
        this.Owner.SubHP(value, HurtT.Buff, this.ID, this.Master.UUID);
    }
}
RegisterBuff(FightBuffType.DropBlood, new DropBloodBuff(0, 0, null, null, 0, []));

