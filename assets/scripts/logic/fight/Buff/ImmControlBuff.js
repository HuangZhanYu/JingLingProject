/*
作者（Author）:    jason

描述（Describe）: 免疫控制
*/
/*jshint esversion: 6 */
import Buff, { RegisterBuff } from "./Buff";
import { FightBuffType, RoleRef } from "../FightDef";
export default class ImmControlBuff extends Buff
{
    constructor(type, id, master, owner, duration, params)
    {
        super(type, id, master, owner, duration, params);
    }
    IsParamsVaild()
    {
        return true;
    }
  
    Start(tm)
    {
        this.Owner.AddRef(RoleRef.ImmControl, 1);
    }
    Done()
    {
        this.Owner.DecRef(RoleRef.ImmControl, 1);
    }
}
RegisterBuff(FightBuffType.ImmControl, new ImmControlBuff(0, 0, null, null, 0, []));

