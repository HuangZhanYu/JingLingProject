/*
作者（Author）:    jason

描述（Describe）: 免疫减速
*/
/*jshint esversion: 6 */
import Buff, { RegisterBuff } from "./Buff";
import { FightBuffType, RoleRef } from "../FightDef";
export default class ImmSlowBuff extends Buff
{
    constructor(type, id, master, owner, duration, params)
    {
        super(type, id, master, owner, duration, params);
    }
    IsParamsVaild()
    {
        return true;
    }
    Start()
    {
        this.Owner.AddRef(RoleRef.ImmSlow, 1);
    }
    Done()
    {
        this.Owner.DecRef(RoleRef.ImmSlow, 1);
    }
}
RegisterBuff(FightBuffType.ImmSlow, new ImmSlowBuff(0, 0, null, null, 0, []));
