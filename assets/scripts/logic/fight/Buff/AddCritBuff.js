/*
作者（Author）:    jason

描述（Describe）:   增加暴击率
*/
/*jshint esversion: 6 */

import Buff, { RegisterBuff } from "./Buff";
import { FightBuffType } from "../FightDef";

export default class AddCritBuff extends Buff
{
    constructor(type, id, master, owner, duration, params)
    {
        super(type, id, master, owner, duration, params);
    }
    IsParamsVaild()
    {
        return this.Params.length >= 1;
    }
    GetEffectParam()
    {
        return this.Params[0];
    }
}
RegisterBuff(FightBuffType.AddCrit, new AddCritBuff(0, 0, null, null, 0, []));
