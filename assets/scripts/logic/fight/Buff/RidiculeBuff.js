/*
作者（Author）:    jason

描述（Describe）: 嘲讽
*/
/*jshint esversion: 6 */
import Buff, { RegisterBuff } from "./Buff";
import { FightBuffType } from "../FightDef";


export default class RidiculeBuff extends Buff
{
    constructor(type, id, master, owner, duration, params)
    {
        super(type, id, master, owner, duration, params);
    }
    IsParamsVaild()
    {
        return true;
    }
    Start()
    {
        let owner = this.Owner;
        let skillconf = LoadConfig.getConfigData(ConfigName.JiNeng,owner.GetCanUseSkillID());
        if (skillconf && owner.CheckIsTarget(this.Master, skillconf))
            owner.SetTarget(this.Master);
    }
}
RegisterBuff(FightBuffType.Ridicule, new RidiculeBuff(0, 0, null, null, 0, []));

