/*
作者（Author）:    jason

描述（Describe）:
*/
/*jshint esversion: 6 */

//buff工厂
let BuffFactory = {};
//注册buff
export function RegisterBuff(type, buff)
{
     if (BuffFactory[type])
     {
        return;
     }

    BuffFactory[type] = buff;
}


//创建buff
export function CreateBuff(type, id, master, owner, duration, params)
{
    let obj = BuffFactory[type];
    if (obj == null)
    {   
        return null;
    }

    let new_obj = new obj(type, id, master, owner, duration, params);
    return new_obj;
}

export default class Buff
{
    constructor(type, id, master, owner, duration, params)
    {
        this.ID = id;
        this.Type   = type;
        this.Master = master;
        this.Owner  = owner;
        if(owner != null)
        {
            this.StartTime = owner.Manager.CurTm();
        }
        else
        {
            this.StartTime = 0;
        }
        this.EndTime   = this.StartTime + duration;
        this.Params    = [];
        this.UpdateTime= 0;
        if (params)
        {
            for(let param of params)
            {
                table.insert(this.Params, param);
            }
        } 
    }
    Run(tm)
    {
        if (!this.IsParamsVaild())
            return true;
    

        if (this.UpdateTime == 0 && tm - this.StartTime >= this.StartDelay())
        {
            this.StartTime  = tm;
            this.Start(tm);
            this.Do(1);
            this.UpdateTime = tm;

            let lsttm = this.EndTime + this.DoneDelay() - this.StartTime;
            this.Owner.Manager.AddIncBuffRecord(this.Owner, this.ID, lsttm, this.GetEffectParam());
        }

        let interval = this.Interval();
        if (interval <= 0)
            return true;
        
        let pass = tm - this.UpdateTime;
        if (tm > this.EndTime)
            pass = this.EndTime - this.UpdateTime;
        
        let times = Math.floor(pass / interval);
        if (times > 0)
        {
            this.UpdateTime = tm;
            this.Do(times);
        }
            

        let done = tm >= this.EndTime + this.DoneDelay();
        if (done)
            this.Done();
        

        return done;
    }
    
    GetType()
    {
        return this.Type;
    }
    
    IsParamsVaild()
    {
        return false;
    }
    
    GetEffectParam()
    {   
        return 0;
    }

    StartDelay()
    {
        return 0;
    }

    Start(){}

    Interval()
    {
        return this.EndTime - this.StartTime;
    }

    Do(times){}

    DoneDelay()
    {
        return 0;
    }

    Done(){}

    SetFromASkill(b)
    {
        this.FromASkill = b;
    }

    IsFromASkill()
    {
        return this.FromASkill;
    }
}

