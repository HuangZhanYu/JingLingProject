/*
作者（Author）:    jason

描述（Describe）: 减速
*/
/*jshint esversion: 6 */
import Buff, { RegisterBuff } from "./Buff";
import { FightBuffType } from "../FightDef";
export default class SlowDownBuff extends Buff
{
    constructor(type, id, master, owner, duration, params)
    {
        super(type, id, master, owner, duration, params);
    }
    IsParamsVaild()
    {
        return this.Params.length >= 1;
    }
    GetEffectParam()
    {
        return this.Params[0];
    }
    Start()
    {
        let resist = this.Owner.GetBuffResist(FightBuffType.SlowDown);
        if (resist != 0)
            this.EndTime = this.StartTime + this.Interval() * (1 - resist / 10000);
    }
}
RegisterBuff(FightBuffType.SlowDown, new SlowDownBuff(0, 0, null, null, 0, []));