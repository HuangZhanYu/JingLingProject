/*
作者（Author）:    jason

描述（Describe）: 遁入虚空
*/
/*jshint esversion: 6 */

import Buff, { RegisterBuff } from "./Buff";
import { FightBuffType } from "../FightDef";
export default class IntoVoidBuff extends Buff
{
    constructor(type, id, master, owner, duration, params)
    {
        super(type, id, master, owner, duration, params);
    }
    IsParamsVaild()
    {
        return true;
    }
    Start()
    {
        let manager = this.Owner.Manager;
        let enemyteam = manager.GetEnemyTeam(this.Owner.Camp);
        let enemylist = manager.GetEnemyList(this.Owner.Camp);
        for(let role of enemylist)
        {
            if (role.Target && role.Target.UUID == this.Owner.mUUID)
                role.ChangeTarget(null);
        }

        this.Owner.ChangeTarget(enemyteam.GetCastle());
        this.Owner.AddRef(RoleRef.LockTarget, 1);
        this.Owner.AddRef(RoleRef.Hiding, 1);
    }
    Done()
    {
        this.Owner.DecRef(RoleRef.LockTarget, 1);
        this.Owner.DecRef(RoleRef.Hiding, 1);
    }
}
RegisterBuff(FightBuffType.IntoVoid, new IntoVoidBuff(0, 0, null, null, 0, []));

