/*
作者（Author）:    jason

描述（Describe）: 净化
*/
/*jshint esversion: 6 */
import Buff, { RegisterBuff } from "./Buff";
import { FightBuffType } from "../FightDef";
export default class PurifyBuff extends Buff
{
    constructor(type, id, master, owner, duration, params)
    {
        super(type, id, master, owner, duration, params);
    }
    IsParamsVaild()
    {
        return this.Params.length >= 2;
    }
    Start()
    {
        this.Owner.Add2PurifyList(this.Params[0], this.Params[0]);
    }
}
RegisterBuff(FightBuffType.Purify, new PurifyBuff(0, 0, null, null, 0, []));

