/*
作者（Author）:    jason

描述（Describe）: 魔法免疫
*/
/*jshint esversion: 6 */
import Buff, { RegisterBuff } from "./Buff";
import { FightBuffType } from "../FightDef";
export default class MIBuff extends Buff
{
    constructor(type, id, master, owner, duration, params)
    {
        super(type, id, master, owner, duration, params);
    }
    IsParamsVaild()
    {
        return true;
    }
   
}
RegisterBuff(FightBuffType.MI, new MIBuff(0, 0, null, null, 0, []));

