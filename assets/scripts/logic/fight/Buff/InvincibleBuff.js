/*
作者（Author）:    jason

描述（Describe）: 无敌
*/
/*jshint esversion: 6 */
import Buff, { RegisterBuff } from "./Buff";
import { FightBuffType, RoleRef } from "../FightDef";
export default class InvincibleBuff extends Buff
{
    constructor(type, id, master, owner, duration, params)
    {
        super(type, id, master, owner, duration, params);
    }
    IsParamsVaild()
    {
        return true;
    }
    Start()
    {
        this.Owner.AddRef(RoleRef.Invincible, 1);
    }
    Done()
    {
        this.Owner.DecRef(RoleRef.Invincible, 1);
    }
}
RegisterBuff(FightBuffType.Invincible, new InvincibleBuff(0, 0, null, null, 0, []));

