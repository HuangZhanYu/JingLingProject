/*
作者（Author）:    jason

描述（Describe）: 击退
*/
/*jshint esversion: 6 */
import Buff, { RegisterBuff } from "./Buff";
import { FightBuffType, FightRoleState, FightDef, FightCampType } from "../FightDef";
export default class RepelBuff extends Buff
{
    constructor(type, id, master, owner, duration, params)
    {
        super(type, id, master, owner, duration, params);
    }
    IsParamsVaild()
    {
        return this.Params.length >= 1;
    }
    GetEffectParam()
    {
        return this.Length;
    }
    Start()
    {
        //计算抗性影响
        let length = this.Params[0];
        let resist = this.Owner.GetBuffResist(FightBuffType.Repel);
        if (resist != 0)
        {
            let val = 1 - resist / 10000;
            this.EndTime = this.StartTime + (this.EndTime - this.StartTime) * val;
            length = length * val;
        }

        //log("repel id:"..this.Owner.UUID)
        //计算击飞速度
        this.Owner.ChangeState(FightRoleState.Stand);
        this.Length = length;
        this.Speed  = length / (this.EndTime - this.StartTime) * 1000;
        if (this.Owner.Camp == FightCampType.My)
        {
            this.Speed = -this.Speed;
            length = -length;
        }

        let lsttm = this.EndTime + this.DoneDelay() - this.StartTime;
        this.Owner.Manager.AddRepelRecord(this.Owner, this.ID, lsttm, 0, length);
    }
    Interval()
    {
        return FightDef.FREQUENCY;
    }
    Do(times)
    {
        let x = times * this.Interval() * this.Speed / 1000;
        //log("repel id:"..this.Owner.UUID.." x:"..x)
        let pos = this.Owner.Pos;
        let manager = this.Owner.Manager;
        if ((pos.x >= manager.Width && x > 0) || (pos.x <= -manager.Width && x < 0))
            return;
        
        pos.x = pos.x + x;
        if (pos.x > manager.Width)
            pos.x = manager.Width;
        else if (pos.x < -manager.Width)
            pos.x = -manager.Width;
    }
    DoneDelay()
    {
        return 250;
    }
}
RegisterBuff(FightBuffType.Repel, new RepelBuff(0, 0, null, null, 0, []));
