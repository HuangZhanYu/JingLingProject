/*
作者（Author）:    jason

描述（Describe）: 隐身
*/
/*jshint esversion: 6 */
import Buff, { RegisterBuff } from "./Buff";
import { FightBuffType, RoleRef } from "../FightDef";
export default class HidingBuff extends Buff
{
    constructor(type, id, master, owner, duration, params)
    {
        super(type, id, master, owner, duration, params);
    }
    IsParamsVaild()
    {
        return this.Params.length >= 1;
    }
    GetEffectParam()
    {
        return this.Params[0];
    }
    Start()
    {
        let enemylist = this.Owner.Manager.GetEnemyList(this.Owner.Camp);
        for(let role of enemylist)
        {
            if (role.Target && role.Target.UUID == this.Owner.mUUID)
                role.ChangeTarget(null);
        }
         
        this.Owner.AddRef(RoleRef.Hiding, 1);
    }
    Done()
    {
        this.Owner.DecRef(RoleRef.Hiding, 1);
    }
}
RegisterBuff(FightBuffType.Hiding, new HidingBuff(0, 0, null, null, 0, []));

