/*
作者（Author）:    jason

描述（Describe）: 鬼咒
*/
/*jshint esversion: 6 */
import Buff, { RegisterBuff } from "./Buff";
import { FightBuffType } from "../FightDef";
export default class GhostCurseBuff extends Buff
{
    constructor(type, id, master, owner, duration, params)
    {
        super(type, id, master, owner, duration, params);
    }
    IsParamsVaild()
    {
        return true;
    }
    GetEffectParam()
    {
        return 0;
    }
}
RegisterBuff(FightBuffType.GhostCurse, new GhostCurseBuff(0, 0, null, null, 0, []));

