/*
作者（Author）:    jason

描述（Describe）:   魅惑
*/
/*jshint esversion: 6 */
import Buff, { RegisterBuff } from "./Buff";
import { FightBuffType, FightRoleState } from "../FightDef";


export default class CharmBuff extends Buff
{
    constructor(type, id, master, owner, duration, params)
    {
        super(type, id, master, owner, duration, params);
    }
    IsParamsVaild()
    {
        return this.Params.length >= 1;
    }
    GetEffectParam()
    {
        return this.Params[0];
    }
    Start()
    {
        let owner = this.Owner;
        //owner:AddRef(RoleRef.LockTarget, 1)
        owner.ChangeState(FightRoleState.Stand);
        let team = owner.Manager.GetTeam(owner.Camp);
        let Conf_JiNeng = LoadConfig.getConfigData(ConfigName.JiNeng,owner.GetCanUseSkillID());
        owner.Target = owner.FindTarget(team, Conf_JiNeng);
    }
}
RegisterBuff(FightBuffType.Charm, new CharmBuff(0, 0, null, null, 0, []));
