/*
作者（Author）:    jason

描述（Describe）: 昏迷
*/
/*jshint esversion: 6 */
import Buff, { RegisterBuff } from "./Buff";
import { FightBuffType, FightRoleState } from "../FightDef";
export default class ComaBuff extends Buff
{
    constructor(type, id, master, owner, duration, params)
    {
        super(type, id, master, owner, duration, params);
    }
    IsParamsVaild()
    {
        return true;
    }
    Start()
    {
        let resist = this.Owner.GetBuffResist(FightBuffType.Coma);
        if (resist != 0)
            this.EndTime = this.StartTime + this.Interval() * (1 - resist / 10000);
    
        this.Owner.ChangeState(FightRoleState.Stand);
    }
}
RegisterBuff(FightBuffType.Coma, new ComaBuff(0, 0, null, null, 0, []));
