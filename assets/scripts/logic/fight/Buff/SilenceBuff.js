/*
作者（Author）:    jason

描述（Describe）: 沉默
*/
/*jshint esversion: 6 */
import Buff, { RegisterBuff } from "./Buff";
import { FightBuffType } from "../FightDef";
export default class SilenceBuff extends Buff
{
    constructor(type, id, master, owner, duration, params)
    {
        super(type, id, master, owner, duration, params);
    }
    IsParamsVaild()
    {
        return true;
    }
}
RegisterBuff(FightBuffType.Silence, new SilenceBuff(0, 0, null, null, 0, []));

