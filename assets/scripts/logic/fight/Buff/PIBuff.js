/*
作者（Author）:    jason

描述（Describe）:
*/
/*jshint esversion: 6 */
import Buff, { RegisterBuff } from "./Buff";
import { FightBuffType } from "../FightDef";
export default class PIBuff extends Buff
{
    constructor(type, id, master, owner, duration, params)
    {
        super(type, id, master, owner, duration, params);
    }
    IsParamsVaild()
    {
        return true;
    }

}
RegisterBuff(FightBuffType.PI, new PIBuff(0, 0, null, null, 0, []));

