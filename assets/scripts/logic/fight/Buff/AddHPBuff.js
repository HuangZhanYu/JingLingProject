/*
作者（Author）:    jason

描述（Describe）: 单次回血
*/
/*jshint esversion: 6 */
import Buff, { RegisterBuff } from "./Buff";
import { FightBuffType } from "../FightDef";
import BigNumber from "../../../tool/BigNumber";
export default class AddHPBuff extends Buff
{
    constructor(type, id, master, owner, duration, params)
    {
        super(type, id, master, owner, duration, params);
    }
    IsParamsVaild()
    {
        return this.Params.length >= 4;
    }
    Interval()
    {
        return this.Params[2]/10;
    }
    Start()
    {
        let value = '0';
        if (this.Params[0] == 1)
        {
            value = BigNumber.mul(this.Owner.ATK, this.Params[0]);
            value = BigNumber.div(value, BigNumber.tenThousand);
        }
        else if (this.Params[0] == 2)
        {
            value = BigNumber.mul(this.Params[1], this.Params[0]);
            value = BigNumber.div(value, BigNumber.tenThousand);
        }
        this.Value = value;
    }
    Do()
    {
        let value = this.Value;
        if (times > 1 )
            value = BigNumber.mul(value, tostring(times));
        
        this.Owner.AddHP(value);
    }
}
RegisterBuff(FightBuffType.AddHP, new AddHPBuff(0, 0, null, null, 0, []));
