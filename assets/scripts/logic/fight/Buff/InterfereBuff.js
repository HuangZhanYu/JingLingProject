/*
作者（Author）:    jason

描述（Describe）: 干扰
*/
/*jshint esversion: 6 */
import Buff, { RegisterBuff } from "./Buff";
import { FightBuffType } from "../FightDef";
export default class InterfereBuff extends Buff
{
    constructor(type, id, master, owner, duration, params)
    {
        super(type, id, master, owner, duration, params);
    }
    IsParamsVaild()
    {
        return this.Params.length >= 1;
    }
    GetEffectParam()
    {
        return this.Params[0];
    }
    Start()
    {
        this.Owner.Anger = this.Owner.Anger - this.GetEffectParam();
    }
}
RegisterBuff(FightBuffType.Interfere, new InterfereBuff(0, 0, null, null, 0, []));

