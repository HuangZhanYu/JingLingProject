/*
作者（Author）:    jason

描述（Describe）: 全局战斗管理器
*/
/*jshint esversion: 6 */

import PlayerBasePart from "../../part/PlayerBasePart";
import FightManager from "./FightManager";
import FightBehaviorManager from "../fightBehavior/FightBehaviorManager";
import Playback from "./Playback";
import CanShuTool from "../../tool/CanShuTool";
import { FightSystemType, FightDef } from "./FightDef";
import TimerManager from "../../tool/TimerManager";

const FightStateEnum = {
    Normal     : 1, //正常战斗状态
    ToBeQuiet  : 2, //进入静默状态
    Quieting   : 3, //静默行军中
    QuitQuiet  : 4, //正在退出静默行军
};

let GFightManager = (function(){

    let Handle    = null;
    let Callback  = null;
    let IsPause   = false;
    let TimeScale = 1;  //倍率
    let Behavior  = true;
    let State     = FightStateEnum.Normal;
    let FightType = 0;
    let StartTime = 0;
    return{
        Init()
        {
            Handle    = null;
            Callback  = null;
            IsPause   = false;
            TimeScale = 1;  //倍率
            Behavior  = true;
            State     = FightStateEnum.Normal;
            FightType = 0;
            StartTime = 0;
        },
        //设置表现标记
        SetBehavior(b)
        {
            if (!b ) 
                State = FightStateEnum.ToBeQuiet;
                //console.error("执行了状态赋值："..State);
            else if( !Behavior && b )
                State = FightStateEnum.QuitQuiet;
                //console.error("执行了状态赋值："..State);
            
            //console.error("赋值之前this.Behavior = :"..tostring(Behavior))
            //console.error("状态："..State);
            Behavior = b;
        },

        //获取战斗状态
        GetState()
        {
            return State;
        },

        //是否有重置提示
        IsNeedRevivePrompt()
        {
            let conf = CanShuTool.GetCanShuData(CanShuTool.CanShuEnum.RevivePromptParams);
            if (!conf )
                return [false, 0, 0];
            

            if( PlayerBasePart.GetIntValue(PlayerIntAttr.PlayerIntAttr_GuanQiaFailCount4Tips) >= conf.Param[0] )
                return [true, 1, conf.Param[0]];
            else if( TimePart.GetUnix() - StartTime >= conf.Param[1] )
                return [true, 2, conf.Param[1]/60];
            
            return [false, 0, 0];
        },

        //开始战斗 如果需要进行战斗验证 则fightid randseed必须从服务器获取 战斗完毕后将fightid 战斗输入回传给服务器进行验证
        Start(sence, fighttype, duration, fightid, randseed, myteam, enemyteam, callback)
        {
            //snapshotLuaMemory()
            let fight = new FightManager();
            fight.Init(fightid, randseed, duration, [myteam, enemyteam]);

            Handle    = fight;
            Callback  = callback;
            IsPause   = false;

            //开始加载资源
            let mylist = [];
            mylist.push(myteam.Boss.CardID);
            for(let card of myteam.CardList)
            {
                mylist.push(card.CardID);
            }
            
            let enemylist = [];
            enemylist.push(enemyteam.Boss.CardID);
            for(let card of enemyteam.CardList)
            {
                enemylist.push(card.CardID);
            }

            //主线战斗特殊处理
            if (fighttype == FightSystemType.GuanQia )
            {
                // 检查是否有宝箱显示
                //GuanQiaPart.ShowBaoXiang();
            }

            //开始加载资源
            if (State == FightStateEnum.Normal || State == FightStateEnum.ToBeQuiet || fighttype != FightSystemType.GuanQia || FightType != FightSystemType.GuanQia )
            {
                FightBehaviorManager.LoadFightResource(fighttype, sence, duration, mylist, enemylist, this.LoadResFinished.bind(this));
            }
            else 
            {
                this.LoadResFinished();
            }
            
            fight.SetFightType(fighttype);
            FightType = fighttype;
            StartTime = TimePart.GetUnix();
        },
            
        //战斗回放
        Playback(sence, fighttype, record, callback)
        {
            let fight = new Playback();
            fight.Init(record);

            Handle    = fight;
            Callback  = callback;
            IsPause   = false;
            FightType = fighttype;

            //开始加载资源
            FightBehaviorManager.LoadFightResource(fighttype, sence, record.Duration, record.TeamList[0].RoleList, record.TeamList[1].RoleList, LoadResFinished);
        },
            
        //战斗演示
        Demonstration(sence, duration, myteam, enemyteam, callback)
        {
            for(let cardinfo of enemyteam.CardList)
                cardinfo.IsDemo = true;
            

            this.Start(sence, FightSystemType.Demo, duration, "demo", TimePart.GetUnix(), myteam, enemyteam, callback)
        },
            
        //资源加载完毕
        LoadResFinished()
        {
            IsPause = false
            if (State == FightStateEnum.ToBeQuiet && !Behavior )
            {
                State = FightStateEnum.Quieting;
                //MainPanel.ShowQuietMarchPanel();//如果是从进入状态且这一次结束战斗，那么显示静默行军主界面
            }
                
            
            //极限情况：开启省电闯关时，在闯关进入下一关的黑屏瞬间，（还未进入省电模型出现界面）打开其他界面，出现一直停留在进入状态的情况
            if (State == FightStateEnum.Quieting && !Behavior )
            {
                //let state = MainPanel.GetQuietingPanelStateIsOk();
                if(!state )
                {
                    //MainPanel.ShowQuietMarchPanel();
                }
            }
                
                
            
            //如果是静默行军中，则关闭加载
            if (State == FightStateEnum.Quieting  )//&& LoadingPanel.IsShow()
            {
                //LoadingPanel.Close();
            }
                
            
            this.updateTimerKey = TimerManager.addAlwayTimeCallBack(this.GetFrequency(),this.Update.bind(this));
            //timerMgr.AddTimerEvent("Update", GetFrequency(), Update, {}, true);
        },
        //设置战斗的速率
        SetTimeScale(scale)
        {
            TimeScale = scale;
        },

        SetBornBuffTab(tab)
        {
            Handle.SetBornBuffTab(tab);
        },
        //获取没帧时长
        GetFrequency()
        {   
            return FightDef.FREQUENCY / TimeScale / 1000;
        },
        //暂停
        Pause()
        {
            IsPause = true;
            //timerMgr.RemoveTimerEvent('DoneTimer');
        },
        //继续
        Continue()
        {
            IsPause = false;
            //this.Update();
            this.updateTimerKey = TimerManager.addAlwayTimeCallBack(this.GetFrequency(),this.Update.bind(this));
        },
        //使用主动技
        UseActiveSkill(id)
        {
            Handle.UseActiveSkill(id);
        },
        //更新函数
        Update()
        {
            if (IsPause )
                return;
            

            //profiler.start("fightupdate.out")
            //let cur  = UnityEngine.Time.realtimeSinceStartup
            let done = Handle.Update();
            if (State == FightStateEnum.Normal || State == FightStateEnum.ToBeQuiet || FightType != FightSystemType.GuanQia )  //战斗表现
            {
                let recordlist = Handle.GetRecords();
                FightBehaviorManager.ReceiveRecordlist(recordlist);
            }
                
            
            
            //let cur2 = UnityEngine.Time.realtimeSinceStartup
            //console.error("logic update cost:"..(cur2 - cur))
            //调用表现层update
            //log("frame:"..Handle.Frame.." recordsc:"..#recordlist)
            
            //profiler.stop()
            //console.error("show update cost:"..(UnityEngine.Time.realtimeSinceStartup - cur2))

            if (done )
            {
                TimerManager.removeTimeCallBack(this.updateTimerKey);

                //Done(Handle.Ret())
                //timerMgr:AddTimerEvent("DoneTimer", FightDef.EndFightTime / FightBehaviorManager.GetSpeed(), DoneTimer, {}, true)
                TimerManager.addTimeCallBack(FightDef.EndFightTime / FightBehaviorManager.GetSpeed(),this.DoneTimer.bind(this));
            }
        },

        //1秒后结束战斗
        DoneTimer(params)
        {
            this.Done(Handle.Ret());
        },
        //战斗结束
        Done(ret)
        {
            if (Callback != null )
            {
                Callback(ret, Handle.GetInput(), Handle.BuffNotDone());
            }

            //FightBehaviorManager.EndFight(ret);
            if (State == FightStateEnum.QuitQuiet && Behavior )
            {
                State = FightStateEnum.Normal;
                //MainPanel.CloseQuietMarchPanel();//关闭静默行军
            }
        },
        //根据card创建fightcard
        CreateFightCard(card)
        {
            let fightcard = {};
            fightcard.ObjID  = card.ObjID;
            fightcard.CardID = card.CardID;
            fightcard.FightLevel = card.FightLevel;
            fightcard.FightAttr = [];
            for(let i = 0; i < card.FightAttr.length; ++i)
            {
                fightcard.FightAttr[i] = card.FightAttr[i];
            }
            
           fightcard.FightSpecAttr = card.GetFightSpecialAttr();
            return fightcard;
        },
        //创建战斗队伍 
        //参数： objid:(玩家id 或副本id)、boss:(Card)、cardlist:([]Card)、level(玩家等级)、guanqiatop(最高通过关卡)
        CreateFightTeam(objid, boss, cardlist, level, guanqiatop, param)
        {
            let team = {};
            team.ObjID = objid;
            team.Boss  = this.CreateFightCard(boss);
            team.Level = level;
            team.GuanqiaTop = guanqiatop;
            team.RespawnProb= param && param.RespawnProb || 0;
            team.CardList   = [];
            for (let index = 0; index < cardlist.length; index++) 
            {
                let fightCard = this.CreateFightCard(cardlist[index])
                team.CardList.push(fightCard);
                
            }

            return team;
        },

        //翻转战斗记录 用于战斗回放 玩家是防守方的情况
        FlipRecord(record)
        {
            for(let data of record.FrameList)
            {
                for(let item of data.Records)
                {
                    if (item.Content.Flip )
                        item.Content.Flip();
                }
            }
        }

    };
})();
export default GFightManager;

