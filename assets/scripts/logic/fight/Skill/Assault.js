/*
作者（Author）:    jason

描述（Describe）: 冲锋 属于技能前摇操作
*/
/*jshint esversion: 6 */
import { RoleRef } from "../FightDef";
export default class Assault
{
    constructor(master, conf)
    {
        this.Master = master;
        this.Target = master.Target;
        this.Conf   = conf;
        this.Speed  = conf.Param[0]/10000;
        this.Done   = false;
    }
    Init()
    {
        let master = this.Master;
        master.AddRef(RoleRef.Invincible, 1);
        master.Manager.AddAssaultRecord(master, this.Target, this.Conf.ID, master.GetASPD(), this.Speed, null);
        return true;
    }
    Run(tm, elaspe)
    {
        let heading = this.Target.Pos;
        heading.sub(this.Master.Pos);
        let dis = heading.magSqr();
        let v = this.Speed * elaspe / 1000;
        if (dis <= v*v)
        {
            this.Master.DecRef(RoleRef.Invincible, 1);
            this.Master.Manager.AddAssaultEndRecord(this.Master, this.Master.Pos);
            this.Done = true;
            return true;
        }

        heading = heading.normalize();
        heading.mul(v);

        let pos = this.Master.Pos;
        pos = pos.add(heading);
        return false;
    }
    Interrupt()
    {
        this.Master.DecRef(RoleRef.Invincible, 1);
    }
    IsDone()
    {
        return this.Done;
    }
}
