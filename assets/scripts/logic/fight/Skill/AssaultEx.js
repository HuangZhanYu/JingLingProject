/*
作者（Author）:    jason

描述（Describe）: 冲锋技能扩展
*/
/*jshint esversion: 6 */
import { SelectTarget } from "../FightRole";
import { RoleRef } from "../FightDef";

const AssaultDisT = {
    Variable    : 1,    //长度跟随目标远近变化
    Fixed       : 2,    //固定长度
};
export default class AssaultEx
{
    constructor(master, conf)
    {
        this.Master = master;
        this.Conf   = conf;
        this.Speed  = conf.Param[0]/10000;
        this.ExpectTimes = conf.Param[1];
        this.Times  = 0;
        this.TargetPos = master.Target.Pos;
        this.NextPos   = this.TargetPos;
        this.Distance  = 0;
        this.Height    = conf.Param[3]/10000/2;
        this.Heading   = null;
        this.CurrDis   = 0;
        this.FixedDis  = conf.Param[2]/10000;
        this.BeginPos  = master.Pos;
        this.AcosHeight = this.Height;
        this.DisType   = conf.Param[4];
    }
    Init()
    {
        this.Heading = this.TargetPos;
        this.Heading.sub(this.Master.Pos);

        let dis = this.Heading.mag();
        if (this.DisType == AssaultDisT.Fixed)
            this.Distance = this.FixedDis + this.Master.GetRangeAddition(this.Conf);
        else 
            this.Distance = dis + this.FixedDis;
        
        this.Heading = this.Heading.normalize();
        //log("assault dis:"..dis.." fixed:"..this.FixedDis)

        let cos = this.Heading.dot(new cc.Vec2(1, 0));
        if (cos != 0)
            this.AcosHeight = Math.abs(this.Height / cos);
        

        let master  = this.Master;
        let heading = this.Heading;
        this.NextPos.add(heading.mul(this.FixedDis));
        master.Manager.AddAssaultRecord(master, master.Target, this.Conf.ID, master.GetASPD(), this.Speed, this.NextPos);
        master.AddRef(RoleRef.Invincible, 1);
        return true;
    }
    Run(tm, elaspe)
    {
        let heading = this.Heading;
        heading.mul(this.Speed * elaspe / 1000);
        let pos = this.Master.Pos;
        pos = pos.add(heading);

        let done = false;
        if (this.Master.Manager.IsOutofMap(pos))
            done = true;
        else
        {
            this.Master.Pos.x = pos.x;
            this.Master.Pos.y = pos.y;
            let dis = heading.mag();
            this.CurrDis = this.CurrDis + dis;
            if (this.CurrDis >= this.Distance)
                done = true;
        }
        
        if (done )
        {
            //log("assault times:"..this.Times.." pos.x:"..this.Master.Pos.x.." y:"..this.Master.Pos.y)
            this.Times = this.Times + 1;
            this.Settle(pos);
            this.Master.Manager.AddAssaultEndRecord(this.Master, this.Master.Pos);
            if (this.Times >= this.ExpectTimes)
            {
                this.Master.DecRef(RoleRef.Invincible, 1);
                return true;
            }
            else 
                this.SwitchHeading();
        }

        return false;
    }
    SwitchHeading()
    {
        this.BeginPos.x = this.Master.Pos.x;
        this.BeginPos.y = this.Master.Pos.y;
        this.Heading.mul(-1);
        this.CurrDis = 0;
        this.Distance = this.FixedDis;

        let master  = this.Master;
        let pos = this.NextPos;
        if (this.Times%2 == 1)
            pos = this.TargetPos;
        
        master.Manager.AddAssaultRecord(master, master.Target, this.Conf.ID, master.GetASPD(), this.Speed, pos);
    }
    Settle(pos)
    {
        let manager = this.Master.Manager;
        let targets= [];
        if (pos.x == this.BeginPos.x)
        {
            let center = new cc.Vec2(this.BeginPos.x, this.BeginPos.y + this.Heading.y * this.Distance/2);
            targets = manager.RectSearch(this.Master, center, this.Height, this.Distance/2, this.Conf);
        }
        else if (pos.y == this.BeginPos.y)
        {
            let center = new cc.Vec2(this.BeginPos.x + this.Heading.x * this.Distance/2, this.BeginPos.y);
            targets = manager.RectSearch(this.Master, center, this.Distance/2, this.Height, this.Conf);
        }
        else
        {
            let disx = (pos.x - this.BeginPos.x)/2;
            let x = disx + this.BeginPos.x;
            let y = (pos.y - this.BeginPos.y)/2 + this.BeginPos.y;
            let list = manager.RectSearch(this.Master, new cc.Vec2(x, y), Math.abs(disx), manager.Height, this.Conf);
            for(let target of list)
            {
                y = this.BeginPos.y;
                if (pos.y < this.BeginPos.y)
                    y = y - (target.Pos.x - this.BeginPos.x)*Math.tan(Math.acos(this.Heading.x));
                else 
                    y = y + (target.Pos.x - this.BeginPos.x)*Math.tan(Math.acos(this.Heading.x));
                
                if (target.Pos.y >= y - this.AcosHeight-1 && target.Pos.y <= y + this.AcosHeight+1)
                    targets.push(target);
                
            }
                
        }
 
        //-log("assault hurt:"..#targets)
        targets = SelectTarget(targets, this.Conf.Priority, this.Conf.WoundCount);
        for(let i = 0; i < targets.length; ++i)
        {
            let target = targets[i];
            if(!target)
            {
                continue;
            }
            target.Wound(this.Master, this.Conf, HurtT.Attack);
            if (i >= this.Conf.WoundCount)
                break;
        }
    }
    Interrupt()
    {
        this.Master.DecRef(RoleRef.Invincible, 1);
    }
}

