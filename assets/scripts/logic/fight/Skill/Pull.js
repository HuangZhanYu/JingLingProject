/*
作者（Author）:    jason

描述（Describe）: 拉人 属于技能前摇操作
*/
/*jshint esversion: 6 */
import { FightRoleState, RoleRef } from "../FightDef";
export default class Pull
{
    constructor(master, conf)
    {
        this.Master = master;
        this.Conf   = conf;
        this.Speed  = conf.Param[0]/10000;
        this.Distance = 0;
        this.Heading = null;
        this.CurrDis = 0;
        this.Target  = master.Target;
        this.Done    = false;
    }
    Init()
    {
        let target = this.Target;
        if (target.IsRef(RoleRef.Invincible) || target.IsRef(RoleRef.ImmControl))
        {
            this.Done = true;
            return false;
        }

        if (target.PulledBy)
            target.PulledBy.ChangeState(FightRoleState.Stand);
        
        
        this.Heading = this.Master.Pos;
        this.Heading.sub(target.Pos);
        this.Distance = this.Heading.mag()-8;
        this.Heading = this.Heading.normalize();
        target.SetPulledBy(this.Master);
        target.ChangeState(FightRoleState.Stand);
        target.AddRef(RoleRef.NoMove, 1);
        target.AddRef(RoleRef.NoAttack, 1);

        let master = this.Master;
        master.Manager.AddPullRecord(target, master, this.Speed, target.Pos);
        return true;
    }
    Run(tm, elaspe)
    {
        let heading = this.Heading;
        heading.mul(this.Speed * elaspe / 1000);
        let pos = this.Target.Pos;
        pos = pos.add(heading);

        let dis = heading.mag();
        this.CurrDis = this.CurrDis + dis;
        if (this.CurrDis >= this.Distance)
        {
            this.Master.Manager.AddPullEndRecord(this.Target, pos);
            this.Target.DecRef(RoleRef.NoMove, 1);
            this.Target.DecRef(RoleRef.NoAttack, 1);
            this.Done = true;
            return true;
        }
        return false;
    }
    Interrupt()
    {
        this.Target.DecRef(RoleRef.NoMove, 1);
        this.Target.DecRef(RoleRef.NoAttack, 1);
    }
    IsDone()
    {
        return this.Done;
    }
}

