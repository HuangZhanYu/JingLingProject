/*
作者（Author）:    jason

描述（Describe）:
*/
/*jshint esversion: 6 */
import { FightRoleState, FightShowType, FightSkillHurtType } from "../FightDef";


import Assault from "./Assault";
import AssaultEx from "./AssaultEx";
import Pull from "./Pull";
import Teleport from "./Teleport";
import Beam from "./Beam";
import FightTool from "../../fightBehavior/FightTool";
import { HurtT } from "../FightRecord";
export default class BaseSkill
{
    constructor(id, master, target)
    {
        this.ID = id;               //技能ID
        this.Master    = master;   //当前施放者
        this.Target    = target;    //施法对象
        this.Targets   = [];        //攻击对象
        this.StartTime = 0;         //开始时间
        this.Conf      = null;       
        this.ShowConf  = null;
        this.HurtTime  = [];
        this.HurtIndex = 1;
        this.ShotTime  = [];
        this.ShotIndex = 1;
        this.ShotType  = 1;
        this.AniTime   = 1000 ;     //技能动画时长
        this.Stop      = false;     //是否结束
        this.PreOperation = null;    //技能前摇
        this.Ready     = false;
        this.RangeTargets = [];
        this.PreHurtTime  = [];
        this.PreHurtIndex = 1;
    }
    SetTargets(targets)
    {
        this.Targets = targets;
    }
    SetReady(ok)
    {
        this.Ready = ok;
    }
    Init(preop)
    {
        this.PreOperation = preop;
        
        this.Conf     = LoadConfig.getConfigData(ConfigName.JiNeng,this.ID);
        this.ShowConf = LoadConfig.getConfigData(ConfigName.JiNeng_BiaoXian,this.ID);
        if (this.Conf == null || this.ShowConf == null )
        {
             //console.error("技能ID错误>>>>"..this.ID)
             return;
        }

        this.StartTime= this.Master.Manager.CurTm();
        this.AniTime  = this.ShowConf.AniTime * 1000;
        this.ShotType = this.ShowConf.ShotType;
        let hurttime = FightTool.SplitTime(this.ShowConf.HurtTime);
        for(let tm of hurttime)
        {
            if (tm != 0)
                this.HurtTime.push(tm*1000);
        }

        let shottime = FightTool.SplitTime(this.ShowConf.FlyItemShotTime);
        for (let tm of shottime)
        {
            if (tm != 0)
                this.ShotTime.push(tm*1000);
        }
       
    
        let prehurttime = FightTool.SplitTime(this.ShowConf.PreHurtTime);
        for(let tm of prehurttime)
        {
            if (tm != 0)
                this.PreHurtTime.push(tm*1000);
        }

        //初始化技能前摇操作
        if (this.PreOperation != null)
            this.PreOperation.Init();
        
    }
    Run(tm, el)
    {
        if (this.Stop)
            return true;

        //判断是否有技能前摇
        if (this.PreOperation != null && !this.PreOperation.IsDone())
        {
            this.PreOperation.Run(tm, el);
            return false;
        }
        else if (!this.Ready)
        {
            //寻找技能攻击对象
            this.Targets = this.Master.Manager.Search(this.Master, this.Master.Target, this.ID);
            this.Ready = true;
        }

        let elaspe = tm - this.StartTime;
        this.RunPreAttack(elaspe);
        this.RunAttack(elaspe);  //攻击
        this.RunShot(elaspe);   //处理飞行道具
        this.CheckEnd(elaspe); //检测是否结束

        return this.Stop;
    }
    Interrupt()
    {
        if (this.PreOperation != null && !this.PreOperation.IsDone())
            this.PreOperation.Interrupt();
        
    }
    RunPreAttack(elaspe)
    {
        if (this.PreHurtIndex > this.PreHurtTime.length)
        {
            return;
        }
           

        if( elaspe < this.PreHurtTime[this.PreHurtIndex])
        {
            return;
        }
            
        

        this.PreHurtIndex = this.PreHurtIndex + 1;
        let manager = this.Master.Manager;
        for(let target of this.Targets)
        {
            if(!target)
            {
                continue;
            }
            manager.AddPreHurtRecord(target, this.ID);
        }
            
        
    }
    RunAttack(elaspe)
    {
        if (this.HurtIndex > this.HurtTime.length)
            return;
        

        if (elaspe < this.HurtTime[this.HurtIndex])
            return;
        

        this.HurtIndex = this.HurtIndex + 1;
        let manager = this.Master.Manager;
        for(let i = 0; i < this.Targets.length; ++i)
        {
            let target = this.Targets[i];
            if(!target)
                continue;
           
            if (this.Conf.SkillHurtType == FightSkillHurtType.Single) //单体攻击
                target.Wound(this.Master, this.Conf, HurtT.Attack);
            else if (this.Conf.SkillHurtType == FightSkillHurtType.Range) //单体范围攻击
            {
                if (this.RangeTargets.length <= i)
                {
                    let targets = manager.SearchPos(target.Pos, this.Master, this.Conf);
                    this.RangeTargets.push(targets);
                }
                for(let item of this.RangeTargets[i])
                {   
                    if(!item)
                    {
                        continue;
                    }
                    if (this.Conf.ShowType == FightShowType.Splash && item != target)
                        item.Wound(this.Master, this.Conf, HurtT.Splash);
                    else 
                        item.Wound(this.Master, this.Conf, HurtT.Attack);
                }
            }
        }
    }
    //创建飞行道具
    RunShot(elaspe)
    {
        if (this.ShotIndex > this.ShotTime.length)
            return;
        

        if (elaspe < this.ShotTime[this.ShotIndex])
            return;
        

        let manager  = this.Master.Manager;
        let flymanager = manager.GetFlyManager();
        let curtm      = manager.CurTm();
        if (this.ShotType == 2 && this.Targets.length > 0) //对多个目标 每个目标发射一次
        {
            let index = this.ShotIndex % this.Targets.length;
            if (index == 0)
                index = this.Targets.length;
            
            let target = this.Targets[index];
            flymanager.CreateFlyItem(this.Master, target, this.ID, curtm);
            if (this.ShotIndex >= this.Targets.length)
                this.ShotIndex = this.ShotTime.length;
        }
        else    //单次对多个目标发射
        {
            for(let target of this.Targets)
                flymanager.CreateFlyItem(this.Master, target, this.ID, curtm);
        }
    
        this.ShotIndex = this.ShotIndex + 1;
    }
    CheckEnd(elaspe)
    {
        if (elaspe < this.AniTime || this.HurtIndex <= this.HurtTime.length || this.ShotIndex <= this.ShotTime.length)
            return;
        

        this.Stop = true;
    }
}

//创建技能
export function CreateSkill(master, target, skillid)
{
    let skill_conf = LoadConfig.getConfigData(ConfigName.JiNeng,skillid);
    if (!skill_conf)
        return;
    

    let skill = null;
    let preop = null;
    let manager = master.Manager;
    if (skill_conf.ShowType == FightShowType.Assault)
    {
        skill = new BaseSkill(skillid, master, target);
        preop = new Assault(master, skill_conf);
    }
    else if (skill_conf.ShowType == FightShowType.AssaultEx)
    {   
        skill = new AssaultEx(master, skill_conf);
    }
    else if (skill_conf.ShowType == FightShowType.Pull && !target.IsCastle())
    {
        preop = new Pull(master, skill_conf);
        skill = new BaseSkill(skillid, master, target);
        manager.AddAttackRecord(master, skillid);
    }
    else if (skill_conf.ShowType == FightShowType.Teleport)
    {
        preop = new Teleport(master, skill_conf);
        skill = new BaseSkill(skillid, master, target);
        manager.AddAttackRecord(master, skillid);
    }
    else if (skill_conf.ShowType == FightShowType.Beam)
    {
        preop = new Beam(master, skill_conf);
        skill = new BaseSkill(skillid, master, target);
        preop.SetSkillMaster(skill);
        manager.AddAttackRecord(master, skillid);
    }
    else
    {
        skill = new BaseSkill(skillid, master, target);
        manager.AddAttackRecord(master, skillid);
    }

    skill.Init(preop);
    return skill;
}
    

