/*
作者（Author）:    jason

描述（Describe）: 光束攻击
*/
/*jshint esversion: 6 */
import { FightRoleState } from "../FightDef";
import { SelectTarget } from "../FightRole";
export default class Beam
{
    constructor(master, conf)
    {
        this.Master = master;
        this.Conf   = conf;
        this.Distance = conf.Param[0]/10000/2 + master.GetRangeAddition(conf);
        this.Height = conf.Param[1]/10000/2;
        this.Skill  = null;
        this.Done   = false;
        this.Delay  = 0;
        let showconf = LoadConfig.getConfigData(ConfigName.JiNeng_BiaoXian,conf.ID);
        if (showconf)
            this.Delay  = Math.floor(showconf.LockDelay * 1000);
    }
    SetSkillMaster(obj)
    {
        this.Skill = obj;
    }
    Init()
    {
        if (!this.Skill)
            return false;

        return true;
    }
    Run(tm, elaspe)
    {
        elaspe = tm - this.Skill.StartTime;
        if (elaspe < this.Delay)
            return false;

        let master = this.Master;
        let target = master.Target;
        let direct = 1;
        if (target.Pos.x < master.Pos.x)
            direct = -1;

        let pos = new cc.Vec2(master.Pos.x+direct*this.Distance, master.Pos.y);
        let targets = master.Manager.RectSearch(master, pos, this.Distance, this.Height, this.Conf);
        targets = SelectTarget(targets, this.Conf.Priority, this.Conf.WoundCount);
        // for(let i = targets.length; i > this.Conf.WoundCount+1; --i)
        // {
        //     table.remove(targets, i)
        // }
        this.Skill.SetTargets(targets);
        this.Skill.SetReady(true);
        this.Done = true;
        return true;
    }
    IsDone()
    {
        return this.Done;
    }
}
