/*
作者（Author）:    jason

描述（Describe）:
*/
/*jshint esversion: 6 */
import { RoleRef, FightCampType } from "../FightDef";
export default class Teleport
{
    constructor(master, conf)
    {
        this.TeleportPos = null;
        this.Done        = false;
        this.Master      = master;
        this.StartTime   = 0;
        this.Delay       = 100;
        this.Conf        = conf;
    }
    Init()
    {
        //找到闪现目标位置
        let master   = this.Master;
        let manager  = master.Manager;
        let teleport = manager.TeleportSearch(master, this.Conf);
        //log("teleport init :"..teleport.UUID)
        if (!teleport) //or master.Target == teleport
        {
            this.Done = true;
            return false;
        }
    
        if (teleport.Camp == FightCampType.Enemy)
            this.TeleportPos = new cc.Vec2(teleport.Pos.x-9, teleport.Pos.y);
        else 
            this.TeleportPos = new cc.Vec2(teleport.Pos.x+9, teleport.Pos.y);
        
    
        this.Delay = this.Conf.Param[0]/10;
        master.AddRef(RoleRef.Invincible, 1);
        master.Manager.AddTeleportRecord(master, this.TeleportPos, this.Delay, this.Conf.ID);
        master.SetTarget(teleport);
    
        this.StartTime = manager.CurTm();
        return true;
    }
    Run(tm, el)
    {
        if (tm - this.StartTime < this.Delay)
            return false;
        
        
        this.Done = true;
        let role = this.Master;
        role.Pos = this.TeleportPos;
        role.DecRef(RoleRef.Invincible, 1);
        //log("teleport pos.x:"..role.Pos.x.." y:"..role.Pos.y)
        return true;
    }
    Interrupt()
    {
        this.Master.DecRef(RoleRef.Invincible, 1);
    }
    IsDone()
    {
        return this.Done;
    }
}

