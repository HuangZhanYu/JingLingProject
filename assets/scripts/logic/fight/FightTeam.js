/*
作者（Author）:    jason

描述（Describe）: 战斗队伍
*/
/*jshint esversion: 6 */
import FightRole from "./FightRole";
import { RoleT, FightDef } from "./FightDef";
import BigNumber from "../../tool/BigNumber";


export default class FightTeam
{
    constructor()
    {
        this.ObjID      = '';    //玩家ID 或者副本ID
        this.Manager    = null;   //FightManager
        this.Camp       = 0;     //队伍阵营
        this.RoleList   = [];    //队伍成员
        this.BornList   = [];    //待出生成员
        this.CastleRole = null;   //boss
        this.StartDelay = 0;     //出生延迟
        this.HasBorn    = {};    //已经出生过的点
        this.Level      = 0;     //玩家等级
        this.GuanqiaTop = 0;     //最高通过关卡
        this.TriggerSkillList = []; //触发的主动技列表
        this.TotalBlood = '0';   //总血量
        this.DropBlood  = '0';   //总掉血量
        this.RespawnProb= 0;
        this.MaxAttack  = "0";
    }
    Init(manager, camp, teaminfo)
    {
        this.Manager = manager;
        this.Camp    = camp;
        this.ObjID   = teaminfo.ObjID;
        this.Level   = teaminfo.Level;
        this.GuanqiaTop = teaminfo.GuanqiaTop;
        this.RespawnProb= teaminfo.RespawnProb && teaminfo.RespawnProb || 0;

        this.CastleRole = new FightRole(manager, RoleT.Castle, camp, teaminfo.Boss);
        this.CastleRole.Born(this, manager.GetCastlePos(camp), 0);
        this.RoleList.push(this.CastleRole);
        for (let i in teaminfo.CardList)
        {
            let cardinfo = teaminfo.CardList[i];
            let role = null;
            if (!cardinfo.IsDemo)
                role = new FightRole(manager, RoleT.Normal, camp, cardinfo);
            
            this.RoleList.push(role);

            let bornitem = {};
            bornitem.role   = role;
            bornitem.hpparam= 0;
            bornitem.borntm = manager.CurTm() + this.StartDelay + i * FightDef.BaseEnterTimeGap;
            this.BornList.push(bornitem);

            if (BigNumber.greaterThan(role.ATK, this.MaxAttack))
                this.MaxAttack = role.ATK;
        }
    }
    //获取塔
    GetCastle()
    {   
        return this.CastleRole;
    }

    //获取阵营
    GetCamp()
    {
        return this.Camp;
    }

    //增加总血量
    AddBlood(val)
    {
        this.TotalBlood = BigNumber.add(this.TotalBlood, val);
    }

    //增加总掉血量
    AddDropBlood(val)
    {
        this.DropBlood = BigNumber.add(this.DropBlood, val);
    }

    //判断队伍是否死亡
    IsDead()
    {
        if (this.CastleRole)
            return this.CastleRole.IsDead();

        for(let role of this.RoleList)
        {
            if(!role.IsDead())
            {
                return false;
            }
        }
        return true;
    }

    //增加战斗角色
    AddFightRole(role)
    {
        this.RoleList.push(role);
    }

    //更新函数
    Update(elapse)
    {
        let tm = this.Manager.CurTm();
        this.BornUpdate(tm);        //出生update
        this.ActiveSkillUpdate(tm); //主动技update
        for(let role of this.RoleList)  //队伍成员update 
        {
            if (role.Vaild)
                role.Update(elapse);
        }
    }

//添加到复活列表
Add2ReviveList(role, tm, pos, hpparam)
{
    let item = {};
    item.role   = role;
    item.borntm = tm;
    item.pos    = pos;
    item.hpparam= hpparam;
    item.revive = 1;
    this.BornList.push(item);
}

//出生
BornUpdate(curtm)
{   
    if (this.BornList.length <= 0)
        return;

    let list = [];
    for(let item of this.BornList)
    {
        if (curtm >= item.borntm)
        {
            if (!item.revive)
                item.role.Born(this, this.GetBornPos(), 0);
            else 
                item.role.Revive(item.pos, item.hpparam);
        }
        else
            list.push(item);
    }
    this.BornList = list;
}

//获取出生位置
GetBornPos()
{
    let manager = this.Manager;
    let count = manager.BornPosY.length;
    let rand  = manager.Rand(count);
    //log("getpos random"..this.Manager.Frame)
    for(let i = 0; i < count; ++i)
    {
        if (this.HasBorn[rand] == null)
            break;
        //log("getpos random"..this.Manager.Frame)
        rand = manager.Rand(count);
    } 

    this.HasBorn[rand] = true;
    return new cc.Vec2(manager.GetBornPosX(this.Camp), manager.GetBornPosY(rand));
}

//主动技update
ActiveSkillUpdate(curtm)
{
    if (this.TriggerSkillList.length <= 0)
        return;

    let list = {};
    for(let item of this.TriggerSkillList)
    {
        if (curtm >= item.TriggerTm)
            this.TriggerActiveSkill(item.SkillID);
        else
            list.push(item);
    }
    this.TriggerSkillList = list;
}

//使用主动技
UseActiveSkill(skillid)
{
    let skill_conf = LoadConfig.getConfigData(ConfigName.JiNeng_ZhuDong_BiaoXian,skillid);
    if (!skill_conf)
        return false;

    let item = {};
    item.SkillID = skillid;
    item.TriggerTm = this.Manager.CurTm() + skill_conf.HurtTime * 1000;
    this.TriggerSkillList.push(item);
    this.Manager.AddUseASkillRecord(skillid);
    return true;
}

//使用主动技
TriggerActiveSkill(skillid)
{
    let skill_conf = LoadConfig.getConfigData(ConfigName.JiNeng_ZhuDong,skillid);
    if (!skill_conf)
        return false;

    //计算伤害
    let hurt = '0';
    if (skill_conf.IsHurt == 1)
    {
        if (skill_conf.Params[0] == FightActiveSkillHurtType.Floor)
        {
            let pownum = this.GuanqiaTop - skill_conf.Params[1];
            if (pownum <= 0)
                pownum = 1;
            
            hurt = BigNumber.mul(BigNumber.create(string.format("%e", Math.pow(1.05, pownum))), skill_conf.Params[2]);
            hurt = BigNumber.add(hurt, skill_conf.Params[3]);
        }
        else if(skill_conf.Params[0] == FightActiveSkillHurtType.Level)
        {
            let pownum = this.Level * skill_conf.Params[1];
            hurt = BigNumber.mul(BigNumber.create(string.format("%e", Math.pow(1.05, pownum))), skill_conf.Params[2]);
            hurt = BigNumber.add(hurt, skill_conf.Params[3]);
        }
            
        else if(skill_conf.Params[0] == FightActiveSkillHurtType.MaxAttack)
            hurt = BigNumber.cal([this.MaxAttack, BigNumber.create(skill_conf.Params[1]), "10000"], [BigNumberCalType.Mul, BigNumberCalType.Div]);
    }
        
   
    //受击
    let enemyteam = this.Manager.GetEnemyTeam(this.Camp);
    let enemylist = this.Manager.GetEnemyList(this.Camp);
    let length = hurt.length;
    if (length > this.Manager.CutLength)
        hurt = string.sub(hurt, 1, length-this.Manager.CutLength);
    for(let role of enemylist)
    {
        if (enemyteam.GetCastle() != role || skill_conf.IsHurtLevel == 1)
        {
            if (BigNumber.lessThanOrEqualTo(hurt, 0))
            {
                hurt = BigNumber.mul(role.HP, skill_conf.Params[1]);
                hurt = BigNumber.div(hurt, 10000);
                hurt = BigNumber.add(hurt, skill_conf.Params[2]);
                length = hurt.length;
                if (length > this.Manager.CutLength)
                    hurt = string.sub(hurt, 1, length-this.Manager.CutLength);
            }
        
            role.SubHP(hurt, HurtT.ASkill, skillid, 0);
        }
    }

    //buff
    let team = this.Manager.GetTeam(this.Camp);
    let teamlist = this.Manager.GetTeamListAll(this.Camp);
    for(let i = 0; i < skill_conf.EffectID.length; ++i)
    {
        let buffid = skill_conf.EffectID[i];
        let params = {};
        params.push(skill_conf.EffectParams[(i-1)*4+1]);
        params.push(skill_conf.EffectParams[(i-1)*4+2]);
        params.push(skill_conf.EffectParams[(i-1)*4+3]);
        params.push(skill_conf.EffectParams[(i-1)*4+4]);

        if (skill_conf.Target[i] == FightActiveSkillTargetType.My || skill_conf.Target[i] == FightActiveSkillTargetType.MyAndBoss)
        {
            for(let role of teamlist)
            {   
                if (role != team.GetCastle())
                    role.AddBuff(null, buffid, skill_conf.EffectDuration[i]*1000, params, true);
            }
        }
        else
        {
            for(let role of enemylist)
            {
                if (role != enemyteam.GetCastle())
                    role.AddBuff(null, buffid, skill_conf.EffectDuration[i]*1000, params, true);
            }
        }
    }
}
}

