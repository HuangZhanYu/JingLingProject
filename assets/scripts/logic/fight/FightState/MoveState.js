/*
作者（Author）:    jason

描述（Describe）: 移动状态
*/
/*jshint esversion: 6 */
import { FightRoleState } from "../FightDef";
export default class MoveState
{
    constructor(master, pos)
    {
        this.Master = master;
        this.Speed  = master.GetSpeed();
        this.Stop   = false;
        this.TargetPos = pos;
    }
    Type()
    {
        return FightRoleState.Move;
    }
    Run(elapse)
    {
        if (this.Stop)
            return;

        if (!this.Master.CheckCanMove())
        {
            this.Stop = true;
            return;
        }
        let pos = null;
        let target = this.Master.Target;
        if (this.TargetPos != null)
            pos = this.TargetPos;
        else if (target != null)
            pos = target.Pos;
        else
        {
            this.Stop = true;
            return;
        } 
            

        let speed = this.Master.GetSpeed();
        if (speed != this.Speed)
        {
            this.Speed = speed;
            this.Master.Manager.AddMoveRecord(this.Master, pos);
        }

        let heading = new cc.Vec2(pos.x, pos.y);
        heading = heading.sub(this.Master.Pos);
        let dis = heading.magSqr();
        let v = this.Speed * elapse / 1000;
        if (dis <= v*v)
        {
            this.Stop = true;
            this.Master.ChangeState(FightRoleState.Stand);
            return;
        }

        heading = heading.normalize();
        pos = this.Master.Pos;
        this.Master.Pos = pos.add(heading.mul(v));
    }
}

