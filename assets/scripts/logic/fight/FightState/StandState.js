/*
作者（Author）:    jason

描述（Describe）: 站立状态
*/
/*jshint esversion: 6 */
import { FightRoleState } from "../FightDef";
export default class StandState
{
    constructor(master)
    {
        this.Master = master;
    }
    Type()
    {
        return FightRoleState.Stand;
    }
    Run()
    {
        
    }
}
