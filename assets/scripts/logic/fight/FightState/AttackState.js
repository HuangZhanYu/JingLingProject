/*
作者（Author）:    jason

描述（Describe）: 攻击状态
*/
/*jshint esversion: 6 */

import { FightRoleState } from "../FightDef";

export default class AttackState
{
    constructor(master, skill)
    {
       this.Master = master;
       this.Skill  = skill;
    }
    Type()
    {
        return FightRoleState.Attack;
    }
    Run(elapse)
    {
        if (this.Skill.Run(this.Master.Manager.CurTm(), elapse))
        {
            this.Stop();
        }
        
    }
    Interrupt()
    {
        this.Skill.Interrupt();
    }
    Stop()
    {
        this.Master.ChangeState(FightRoleState.Stand);
    }
}

