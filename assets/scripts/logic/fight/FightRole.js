/*jshint esversion: 6 */


import { RoleRef, SpecialSkillType, FightAttr, FightBuffType, PurifyT, FightTargetLimitType, FightRoleState, FightHurtType, FightSystemType, FightDistanceType, PriorityT, FightTroopType } from "./FightDef";
import BigNumber, { BigNumberCalType } from "../../tool/BigNumber";
import MoveState from "./FightState/MoveState";
import StandState from "./FightState/StandState";
import AttackState from "./FightState/AttackState";
import { CreateSkill } from "./Skill/BaseSkill";
import { HudT } from "./FightRecord";
import { typeOf, jsType } from "../../common/PublicFunction";

/*
作者（Author）:    jason

描述（Describe）: 战斗角色基类
*/
//是否是优先目标
function IsPriorityTarget(target, priority)
{
    if (target.IsCastle())
    {
        return false;
    }

    return (PriorityT.Melee == priority && FightDistanceType.Melee == target.Conf.AttackType) ||
        (PriorityT.Remote == priority && FightDistanceType.Remote == target.Conf.AttackType) ||
        (PriorityT.Physical == priority && FightHurtType.Physical == target.Conf.AttackType) ||
        (PriorityT.Magic == priority && FightHurtType.Magical == target.Conf.AttackType);
}



//选取目标
export function SelectTarget(targets, priority, count)
{
    if (0 == priority)
    {
        return targets;
    }

    let finallist = [];
    let exclude = [];
    for (let i = 0; i < targets.length; ++i)
    {
        let target = targets[i];
        if (IsPriorityTarget(target, priority))
        {
            finallist.push(target);
            exclude.push(i);

            if(finallist.length >= count)
            {
                return finallist;
            }
        }
            
    }
        
        
    
    let targetc = finallist.length;
    let index = 0;
    let excludeindex = 0;
    for (let i = targetc; i < count; ++i )
    {
        if (exclude[excludeindex] == index)
            excludeindex = excludeindex + 1;
        else
        {
            finallist.push(targets[index]);
            if(finallist.length >= count)
            {
                return finallist;
            }
        }
        index = index + 1;
    }
    return finallist;
}
export default class FightRole
{
    constructor(manager, type, camp, cardinfo)
    {
        this.Manager    = manager;                  
        this.UUID       = manager.GenerateUUID();
        this.Type       = type;              //角色类型
        this.Camp       = camp;              //阵营
        this.ID         = cardinfo.CardID;   //卡牌id
        this.ObjID      = cardinfo.ObjID;    //对象id
        this.FightLevel = cardinfo.FightLevel;
        // if !this.FightLevel then
        //     this.FightLevel = 0
        // 

        //战斗属性
        this.ATK        = cardinfo.FightAttr[FightAttr.ATK];
        this.HP         = cardinfo.FightAttr[FightAttr.HP];
        this.DEF        = cardinfo.FightAttr[FightAttr.DEF];
        this.MDEF       = cardinfo.FightAttr[FightAttr.MDEF];
        this.ASPD       = parseFloat(cardinfo.FightAttr[FightAttr.ASPD]);
        this.Speed      = parseFloat(cardinfo.FightAttr[FightAttr.Speed]) / 10000;
        this.Range      = parseFloat(cardinfo.FightAttr[FightAttr.Range]) / 10000;
        this.HitRate    = parseFloat(cardinfo.FightAttr[FightAttr.HitRate]);
        this.DodgeRate  = parseFloat(cardinfo.FightAttr[FightAttr.DodgeRate]);
        this.CRI        = parseFloat(cardinfo.FightAttr[FightAttr.CRI]);
        this.CritDamage = cardinfo.FightAttr[FightAttr.CritDamage];
        this.Respawn    = parseFloat(cardinfo.FightAttr[FightAttr.Respawn]) / 10;
        this.SkillRange = parseFloat(cardinfo.FightAttr[FightAttr.SkillRange]) / 10000;
        this.AttackRange= parseFloat(cardinfo.FightAttr[FightAttr.AttackRange]) / 10000;
        this.SkillAttackRange = parseFloat(cardinfo.FightAttr[FightAttr.SkillAttackRange]) / 10000;
        this.MaxHP      = this.HP;

        this.Vaild      = false;   //是否有效
        this.Pos        = null;     //位置
        this.AttackTime = 0;       //攻击时间
        this.SpeAttackTime = 0;    //特殊技攻击时间
        this.BuffList   = [];      //buff列表
        this.Target     = null;     //攻击对象指针
        this.State      = null;     //当前状态
        this.ImmuneFlag = {};      //免疫标记
        this.BuffResist = {};      //buff抗性
        this.Invincible = false;
        this.Conf       = LoadConfig.getConfigData(ConfigName.ShiBing,this.ID);
        // if this.Conf == null then
        //     console.error("卡牌ID为空:"..this.ID)
        // 
        this.BornTime   = manager.CurTm();
        this.LifeTime   = 0;       //生命时长
        this.Anger      = this.Conf.Anger;
        this.Vision     = this.Conf.Vision/10000;
        this.TeleportTimes = 0;
        this.RespawnTimes  = 0;

        this.RefList    = {};
        // for i = 1, RoleRef.Max do
        //     table.insert(this.RefList, 0)
        // 
        this.PurifyList = {};
        this.FightSpecAttr = {};
        // for _, attr in ipairs(cardinfo.FightSpecAttr) do 
        //     table.insert(this.FightSpecAttr, attr)
        // 
        this.PulledBy   = null;

        //添加免疫
        this.ImmuneFlag[FightBuffType.Repel]       = this.Conf.ImmRepel; 
        this.ImmuneFlag[FightBuffType.Off]         = this.Conf.ImmOff;
        this.ImmuneFlag[FightBuffType.Coma]        = this.Conf.ImmComa;
        this.ImmuneFlag[FightBuffType.SlowDown]    = this.Conf.ImmSlow;
        this.ImmuneFlag[FightBuffType.Frozen]      = this.Conf.ImmFrozen;
        this.ImmuneFlag[FightBuffType.Paralysis]   = this.Conf.ImmParalysis;
        this.ImmuneFlag[FightBuffType.Deter]       = this.Conf.ImmOverawe;
        this.ImmuneFlag[FightBuffType.Winding]     = this.Conf.ImmTwine;
        this.ImmuneFlag[FightBuffType.Shock]       = this.Conf.ImmConcussion;
        this.ImmuneFlag[FightBuffType.Rebound]     = this.Conf.ImmBounce;
        this.ImmuneFlag[FightBuffType.Petrifaction]= this.Conf.ImmPetrifaction;
        this.ImmuneFlag[FightBuffType.Vampire]     = this.Conf.ImmVampire;
        this.ImmuneFlag[FightBuffType.Intimidate]  = this.Conf.ImmThreaten;
        this.ImmuneFlag[FightBuffType.GhostCurse]  = this.Conf.ImmGhostCurse;
        this.ImmuneFlag[FightBuffType.Interfere]   = this.Conf.ImmDisturb;
        this.ImmuneFlag[FightBuffType.Poisoning]   = this.Conf.ImmPoison;
        this.ImmuneFlag[FightBuffType.Charm]       = this.Conf.ImmCharm;
        this.ImmuneFlag[FightBuffType.SlowDown]    = this.Conf.ImmSlow;

        //抗性
        this.BuffResist[FightBuffType.Repel]       = this.Conf.ResistRepel;
        this.BuffResist[FightBuffType.Off]         = this.Conf.ResistOff;
        this.BuffResist[FightBuffType.Coma]        = this.Conf.ResistComa;
        this.BuffResist[FightBuffType.SlowDown]    = this.Conf.ResistFrozen;
        this.BuffResist[FightBuffType.Frozen]      = this.Conf.ResistSlow;
        this.BuffResist[FightBuffType.Paralysis]   = this.Conf.ResistParalysis;
    }
    //出生在战场上
    Born(team, pos, lifetime)
    {
        this.Pos   = pos;
        this.Vaild = true;
        //this.HP    = this.MaxHP
        this.LifeTime = lifetime;

        //log("born hp:"..this.HP.." time:"..this.LifeTime)
        this.Manager.AddBornRecord(this);

        //let team = this.Manager.GetTeam(this.Camp)
        team.AddBlood(this.HP);
        this.TriggerSpecialAttack(SpecialSkillType.InFight);

        let bufflist = this.Manager.GetBornBuff(this.ObjID);
        if (bufflist)
        {
            for(let buff of bufflist)
            {
                this.AddBuff(null, buff.id, buff.lsttm, buff.params, true);
            }
        }
    }
    //复活
    Revive(pos, hpparam)
    {
        this.Pos   = pos;
        this.State = null;
        this.Vaild = true;
        this.Anger = 0;
        this.RefList  = [];
        for (let i = 1; i < RoleRef.Max; ++i )
        {
            this.RefList.push(0);
        }
            
        let bufflist = [];
        for (let buff of this.BuffList )
        {
            if (buff.IsFromASkill())
            {
                bufflist.push(buff);
            }
        }
           
               
        this.BuffList = bufflist;

        let rate = 10000 - hpparam ;
        this.MaxHP = BigNumber.mul(this.MaxHP, rate);
        this.MaxHP = BigNumber.div(this.MaxHP, 10000);
        //-log("revive param:"..hpparam.." maxhp:"..this.MaxHP)
        this.HP = this.MaxHP;

        this.Manager.AddBornRecord(this, true);

        let team = this.Manager.GetTeam(this.Camp);
        team.AddBlood(this.HP);
        this.TriggerSpecialAttack(SpecialSkillType.InFight);
    }
    //增加血量
    AddHP(val)
    {
        let value = BigNumber.add(this.HP, val);
        if (BigNumber.greaterThan(value, this.MaxHP) )
        {
             val = BigNumber.sub(this.MaxHP, this.HP);
            value = this.MaxHP;
        }

        this.HP = value;
        this.Manager.AddIncHPRecord(this, val);

        let team = this.Manager.GetTeam(this.Camp);
        team.AddBlood(val);
    }
    //扣血
    SubHP(val, hurtt, hurtid, masterid)
    {
        let die = false;
        if (BigNumber.lessThanOrEqualTo(this.HP, val) )
        {
            val = this.HP;
            die = true;
        }   
    
        this.HP = BigNumber.sub(this.HP, val);
        this.Manager.AddHurtRecord(this, hurtt, hurtid, val, masterid);
    
        let team = this.Manager.GetTeam(this.Camp);
        team.AddDropBlood(val);
    
        if (die)
        {
            this.Die();
            return true;
        }
    
        return false;
    }
    //获取物防
    GetDEF()
    {
        let add = this.GetBuffEffectMax(FightBuffType.AddDEF);
        if (add == 0)
        {
            return this.DEF;
        }

        let val = BigNumber.cal([this.DEF, BigNumber.create(add), "10000"], [BigNumberCalType.Mul, BigNumberCalType.Div]);
        return BigNumber.add(this.DEF, val);
    }
    //获取法防
    GetMDEF()
    {
        let add = this.GetBuffEffectMax(FightBuffType.AddMDEF);
        if (add == 0 )
        {
            return this.MDEF;
        }
    
        let val = BigNumber.cal([this.MDEF, BigNumber.create(add), "10000"], [BigNumberCalType.Mul, BigNumberCalType.Div]);
        return BigNumber.add(this.MDEF, val);
    }
    //暴率
    GetCRI()
    {
        return this.CRI + this.GetBuffEffectMax(FightBuffType.AddCrit);
    }
    

    //暴伤
    GetCritDamage()
    {
        let add = this.GetBuffEffectMax(FightBuffType.AddCritDmg);
        if (add == 0)
        {
            return this.CritDamage;
        }

        let val = BigNumber.cal([this.CritDamage, BigNumber.create(add), "10000"], [BigNumberCalType.Mul, BigNumberCalType.Div]);
        return BigNumber.add(this.CritDamage, val);
    }
    


    //获取速度
    GetSpeed()
    {
        if ( this.GetBuffCount(FightBuffType.Winding) > 0 )
        {
            return 0;
        }

        var add = this.GetBuffEffect(FightBuffType.AddSpeed);
        var sub = this.GetBuffEffect(FightBuffType.SlowDown) + this.GetBuffEffectMax(FightBuffType.Shock);
        return this.Speed + (add - sub) / 10000 * this.Speed;
    }


    //获取射程范围
    GetRange(skillconf)
    {
        if (skillconf.ID == this.Conf.BigSkillID)
        {
            return this.SkillRange;
        }
        return this.Range; //* (1 + this.Range)
    }
    GetRangeAddition(skillconf)
    {
        if (skillconf.ID == this.Conf.BigSkillID)
        {
            return this.SkillRange - skillconf.Range/10000;
        }
        return this.Range - skillconf.Range/10000;
    }

    //获取攻击范围
    GetAttackRange(skillconf)
    {
        if (skillconf.ID == this.Conf.BigSkillID)
        {
            return this.SkillAttackRange;
        }
        return this.AttackRange;
    }

    //获取伤害范围
    GetHurtRange(skillconf)
    {
        return skillconf.HurtRange / 10000;
    }


    //获取攻速
    GetASPD()
    {
        return this.ASPD + (this.GetBuffEffect(FightBuffType.AddASPD) - this.GetBuffEffectMax(FightBuffType.Shock)) / 10000;
    }


    //获取攻击间隔时间
    GetAttackGap()
    {
        return Math.floor(60 / (this.GetASPD()/10000) * 1000);
    }
    //判断是否合法
    IsVaild()
    {
        return this.Vaild && BigNumber.greaterThan(this.HP, BigNumber.zero);
    }
    
    //判断目标是否合法
    IsTargetVaild()
    {
        return this.Target && this.Target.Vaild;
    }
    
    SetTarget(target)
    {
        this.Target = target;
    }
    
    //判断自身是否为boss
    IsCastle()
    {
        let team = this.Manager.GetTeam(this.Camp);
        //log("is castle cid:"..team.GetCastle().UUID.." selfid:"..this.UUID)
        return team.GetCastle() == this;
    }
   
    SetInvincible(b)
    {
        this.Invincible = b;
    }

    AddRef(reft, count)
    {
        this.RefList[reft] = this.RefList[reft] + count;
    }

    DecRef(reft, count)
    {
        this.RefList[reft] = this.RefList[reft] - count;
    }

    ClearRef(reft)
    {
        this.RefList[reft] = 0;
    }
    
    IsRef(reft)
    {
        return this.RefList[reft] > 0;
    }

    IsImmune(bufft)
    {
        return this.ImmuneFlag[bufft] == 1;
    }

    //获取buff生效参数
    GetBuffEffect(id)
    {
        let value = 0;
        for (let buff of this.BuffList )
        {
            if (buff.GetType() == id)
            {
                value = value + buff.GetEffectParam();
            }
        }
        return value;
    }
    //获取buff生效参数 最大值
    GetBuffEffectMax(id)
    {
        let value = 0;
        for (let buff of this.BuffList )
        {
            if (buff.GetType() == id)
            {
                let param = buff.GetEffectParam();
                if (param > value)
                {
                    value = param;
                }
            }
        }
        return value;
    }   

    //获取buff数量
    GetBuffCount(id)
    {
        let value = 0;
        // for (let buff of this.BuffList )
        // {
        //     if (buff.GetType() == id)
        //     {
        //         value = value + 1;
        //     }
        // }
        return value;
    }
    


    //获取buff抗性
    GetBuffResist(id)
    {
        return this.BuffResist[id];
    }
    


    //buff冲突关系判断
    CanAddBuff(bufftype)
    {
        if (bufftype == FightBuffType.Coma)
        {
            return this.GetBuffCount(FightBuffType.ImmComa) <= 0;
        }
        else if (bufftype == FightBuffType.Frozen)
        {
            return this.GetBuffCount(FightBuffType.ImmFrozen) <= 0;
        }
        else if (bufftype == FightBuffType.Repel)
        {
            return this.GetBuffCount(FightBuffType.ImmRepel) <= 0 && this.GetBuffCount(FightBuffType.Repel) <= 0 && 
            this.GetBuffCount(FightBuffType.Winding) <= 0;
        }
        else if (bufftype == FightBuffType.Off)
        {
            return this.GetBuffCount(FightBuffType.ImmOff) <= 0 && this.GetBuffCount(FightBuffType.Off) <= 0 && 
            this.GetBuffCount(FightBuffType.Winding) <= 0;
        }
        return true;
    }

    IsBuffImmune(buffconf)
    {
        let flag = this.ImmuneFlag[buffconf.Type];
        if (flag && flag == 1 ) //免疫判断
        {
            return true;
        }

        if (buffconf.MainType == BuffMT.Benefit)
            return this.IsRef(RoleRef.Invincible);
        else if (buffconf.MainType == BuffMT.Control)
            return this.IsRef(RoleRef.Invincible) || this.IsRef(RoleRef.ImmControl);
        else if (buffconf.Type == FightBuffType.SlowDown || buffconf.Type == FightBuffType.Shock)
            return this.IsRef(RoleRef.ImmSlow);

        return false;
    }

    //添加buff
    AddBuff(master, buffid, duration, params, askill)
    {
        let conf = LoadConfig.getConfigData(ConfigName.Buff,buffid);
        if (!conf)
        {
            return;
        }
    
        if (this.IsBuffImmune(conf))
        {
            return;
        }
        else if (!this.CanAddBuff(conf.Type)) //buff冲突判断
        {
            return;
        }

        //log("uuid:"..this.UUID.."add buff."..conf.Type.." pc:"..#params)
        let buff = CreateBuff(conf.Type, buffid, master, this, duration, params);
        if (!buff )
        {
            return;
        }
        //log("add uuid:"..this.UUID.."add buff."..conf.Type.." pc:"..#params)
        buff.SetFromASkill(askill);
        this.BuffList.push(buff);
        //this.Manager.AddIncBuffRecord(this, buffid)
    }

    //判断角色是否死亡
    IsDead()
    {
        return BigNumber.lessThanOrEqualTo(this.HP, "0");
    }

    //判断角色是否死透 没有复活的可能
    IsCompleteDead()
    {
        if (!BigNumber.lessThanOrEqualTo(this.HP, "0"))
        {
            return false;
        }
        if (this.Respawn > 0 ) //自动复活
            return false;
        else if (this.RespawnTimes > 0)
            return true;
        

        let team = this.Manager.GetTeam(this.Camp);
        return team.RespawnProb <= 0;
    }

    //检查是否可以活动
    CheckActive()
    {
        return this.GetBuffCount(FightBuffType.Coma) <= 0 && 
           this.GetBuffCount(FightBuffType.Repel) <= 0 && 
           this.GetBuffCount(FightBuffType.Silence) <= 0 &&
           this.GetBuffCount(FightBuffType.Frozen) <= 0;
    }

    //检查是否可以移动
    CheckCanMove()
    {
        return this.GetSpeed() > 0 && !this.IsRef(RoleRef.NoMove) && this.CheckActive();
    }

    //判断是否移动中
    IsMoving()
    {
        return this.State != null && this.State.Type() == FightRoleState.Move;
    }
    //检查是否可以攻击
    CheckCanAttack()
    {
        return (this.State == null || this.State.Type() != FightRoleState.Attack) && !this.IsRef(RoleRef.NoAttack) && this.CheckActive();
    }

    //检查是否在cd中
    CheckCD()
    {
        return this.Manager.CurTm() - this.AttackTime < this.GetAttackGap();
    }
    
    SetPulledBy(role)
    {
        this.PulledBy = role;
    }
    
    Add2PurifyList(maint1, subt1)
    {
        this.PurifyList.push({maint:maint1,subt:subt1});
    }
    
    //判断buff是否需要清除
    IsBuffNeedClear(buff)
    {
        let conf = LoadConfig.getConfigData(ConfigName.Buff,buff.ID);
        
        if (!conf)
        {
            return true;
        }

        for ( let item of this.PurifyList)
        {
             if (item.maint == PurifyT.BuffMainT)
             {
                 if (item.subt == conf.MainType)
                 {
                     return true;
                 }
                    
             }
            else if (conf.Type == item.subt)
            {
                return true;
            }
        }
        
        return false;
    }

    BuffNotDone()
    {
        let list  = [];
        let curtm = this.Manager.CurTm();
        for ( let buff of this.BuffList)
        {
            if (buff.IsFromASkill() && buff.EndTime > curtm)
            {
                list.push({id:buff.ID, lsttm:buff.EndTime-curtm, params:buff.Params});
            }
            
        }
        return list;
    }

    //运行所有buff
    RunBuff(tm)
    {
        //判断是否需要清除buff
        let list = [];
        if (this.PurifyList.length > 0)
        {
             for(let buff of this.BuffList)
             {
                if (this.IsBuffNeedClear(buff))
                {
                    buff.Done();
                    this.Manager.AddRmvBuffRecord(this, buff.ID);
                }
                else
                {
                    list.push(buff);
                }
             }
            this.BuffList = list;
        }
        this.PurifyList.length = 0;

        list.length = 0;
        for (let buff of this.BuffList)
        {
            if (!buff.Run(tm))
                list.push(buff);
            else
                this.Manager.AddRmvBuffRecord(this, buff.ID);
        }
        this.BuffList = list;
    }
    //寻找攻击目标
    FindTarget(enemyteam, skill_conf)
    {
        let targets = [];
        let disarr  = [];
        let list = this.Manager.GetTeamList(enemyteam.Camp);
        for(let enemy of list)
        {
            if (this == enemy)
            {
                continue;
            }
            if (!this.CheckIsTarget(enemy, skill_conf)) //判断是否是攻击目标
            {
                continue;
            }
            //判断是否在视野内
            let [vaild, dis] = this.CheckInVision(enemy);
            if (!vaild)
            {
                continue;
            }
            //按距离远近进行排序
            targets.push(enemy);
            disarr.push(dis);
            for(let i = targets.length-1; i >= 1; --i)
            {
                if (disarr[i] < disarr[i-1] )
                {
                    let tmp   = disarr[i-1];
                    disarr[i-1] = disarr[i];
                    disarr[i]   = tmp;

                    tmp = targets[i-1];
                    targets[i-1] = targets[i];
                    targets[i]   = tmp;
                }
                else
                    break;
            }
        }
        if (0 == targets.length)
        {
            return enemyteam.GetCastle();
        }
        
        targets = SelectTarget(targets, skill_conf.Priority, 1);
        return targets[0];
    }

    //获取当前可以使用的技能
    GetCanUseSkillID()
    {
        let skill_conf = LoadConfig.getConfigData(ConfigName.JiNeng,this.Conf.BigSkillID);
        if (!skill_conf || this.Anger < skill_conf.CostAnger)
        {
            return this.Conf.NormalSkillID;
        }
        return this.Conf.BigSkillID;
    }

    NotTarget()
    {
        return this.Conf.NotTarget == 1;
    }

    IsLand()
    {
        return this.Conf.Troop == FightTroopType.Land || this.Conf.Troop == FightTroopType.LandSuspend;
    }
    


    //判断是否为技能的攻击对象
    CheckIsTarget(target, skill_conf)
    {
        if (target.NotTarget())
        {
            return false;
        }
    
        //隐身判断
        if (target.IsRef(RoleRef.Hiding) && skill_conf.Limit[1] == 0)
        {
            return false;
        }

        let limit = typeOf(skill_conf.Limit) == jsType.array ? skill_conf.Limit[0]:skill_conf.Limit;
        if (limit == FightTargetLimitType.NoLimit)
            return true;
        else if (limit == FightTargetLimitType.LandForce)
            return target.IsLand();
        else if (limit == FightTargetLimitType.LandRemoteForce)
            return target.IsLand() && target.Conf.AttackType == FightDistanceType.Remote;
        else if (limit == FightTargetLimitType.AirForce)
            return target.Conf.Troop == FightTroopType.Air;
        else if (limit == FightTargetLimitType.LandMeleeForce)
            return target.IsLand() && target.Conf.AttackType == FightDistanceType.Melee;
        else if (limit == FightTargetLimitType.RemoteForce)
            return target.Conf.AttackType == FightDistanceType.Remote;

        return false;
    }
    


    //判断是否在攻击范围内
    CheckCanHit(target, skill_conf)
    {
        let range = this.GetAttackRange(skill_conf);
        let dis   = target.Pos;
        dis = dis.sub(this.Pos);
        let disv  = dis.magSqr();
        return [disv <= range * range, disv];
    }
   


    //检测视野
    CheckCanHitZ(target, skill_conf)
    {
        let range = this.GetAttackRange(skill_conf);
        let dis = Math.abs(this.Pos.x - target.Pos.x);
        return [dis <= range, dis * dis];
    }
    


    CheckCanHitByRange(target, range)
    {
        let dis   = target.Pos;
        dis = dis.sub(this.Pos);
        let disv  = dis.mag();
        return [disv <= range * range, disv];
    }
    


    CheckInVision(target)
    {
        let dis  = target.Pos;
        dis = dis.sub(this.Pos);
        let disv = dis.mag();
        return [disv <= this.Vision * this.Vision, disv];
    }
    


    //计算是否命中
    CheckIsHit(target)
    {
        let rate = this.HitRate - target.DodgeRate - this.GetBuffEffectMax(FightBuffType.DecHit);
        //log("check hit:"..this.HitRate.." dodge:"..target.DodgeRate)
        if (rate >= 10000)
            return true;
        

        if (rate < 5000)
            rate = 5000;
        

        //log("check hit random"..this.Manager.Frame)
        let rand = this.Manager.Rand(10000);
        return rand <= rate;
    }
    


    //跟换目标
    ChangeTarget(target)
    {
        this.Target = target;
    }
    


//切换状态
    ChangeState(state, param)
    {
        //技能打断
        if (this.State && this.State.Type() == FightRoleState.Attack)
            this.State.Interrupt();
        

        if (state == FightRoleState.Stand)
        {
            this.State = new StandState(this);
            this.Manager.AddStandRecord(this);
        }
        else if (state == FightRoleState.Move)
        {
            this.State = new MoveState(this, param);
            this.Manager.AddMoveRecord(this, param);
        }
        else if (state == FightRoleState.Attack)
        {
            this.State = new AttackState(this, param);
        }
    }

    Move()
    {
        this.ChangeState(FightRoleState.Move, null);
    }
    
    AddTeamBuff(poolid)
    {
        let poolconf = LoadConfig.getConfigData(ConfigName.BuffPool,poolid);
        if (!poolconf)
            return;     
        

        let teamlist = this.Manager.GetTeamList(this.Camp);
        if (poolconf.PoolT == 1)
        {
            for (let role of teamlist)
            {
                let bufflist = this.Manager.RandomOneBuff(poolconf);
                for( let buff of bufflist)
                {
                    role.AddBuff(this, buff.buffid, buff.duration, buff.params);
                }
            }
        }
        else
        {
            let bufflist = this.Manager.RandomAllBuff(poolconf);
            if (bufflist.length <= 0)
                return;
            
            for(let role of teamlist)
            {
                for(let buff of bufflist)
                {
                    role.AddBuff(this, buff.buffid, buff.duration, buff.params);
                }
            }
        }
           
    }

    //攻击
    Attack(skill_conf)
    {
        if (skill_conf.CostAnger > 0 && this.Anger < skill_conf.CostAnger)
        {
            return false;
        }   
        this.Anger = this.Anger - skill_conf.CostAnger + skill_conf.AddAnger;
        let skill = CreateSkill(this, this.Target, skill_conf.ID);
        this.ChangeState(FightRoleState.Attack, skill);

        //添加队伍buff
        for(let i = 0; i < skill_conf.TeamEffectID.length; ++i)
        {
            let id = skill_conf.TeamEffectID[i];
            let prob = skill_conf.TeamEffectProb[i];
            if (prob > 0 && this.Manager.Rand(10000) <= prob)
            {
                this.AddTeamBuff(id);
            }
        }

        //添加自身buff
        for(let i = 0; i < skill_conf.SelftEffectID.length; ++i)
        {
            let id = skill_conf.TeamEffectID[i];
        
            let prob = skill_conf.SelftEffectProb[i];
            if (prob > 0 && this.Manager.Rand(10000) <= prob)
            {
                let bufflist = this.Manager.RandomBuffByPoolID(id);
                for(let buff of bufflist)
                {
                    this.AddBuff(this, buff.buffid, buff.duration, buff.params);
                }
            }
        }

        return true;
    }
    
    //特殊攻击
    SpecialAttack(type, skillid)
    {
        if (type == SpecialSkillType.Respawn)
        {
            this.ReviveSkill(skillid);
            return;
        }
        

        let conf = LoadConfig.getConfigData(ConfigName.CanShu,66);
        if (!conf)
        {
            return;
        }
        let skill_conf = LoadConfig.getConfigData(ConfigName.JiNeng,skillid);
        if (!skill_conf)
            return;
        

        let curtm = this.Manager.CurTm();
        if (curtm - this.SpeAttackTime < conf.Param[0] * 1000)
            return;
        

        if (this.Attack(skill_conf))
            this.SpeAttackTime = curtm;
    }


    //触发特殊技
    TriggerSpecialAttack(type)
    {
        for(let i = 0; i < this.Conf.SpecialType.length; ++i)
        {
            let st = this.Conf.SpecialType[i];
            if (st == type)
            {
                let skillid = this.Conf.SpecialID[i];
                let skill_conf = LoadConfig.getConfigData(ConfigName.TeShuJiNeng,skillid);
                //if skill_conf != null then
                    //log("spec attack random"..this.Manager.Frame)
                //
                if (skill_conf && this.Manager.Rand(10000) <= skill_conf.Rate)
                {
                    if ((skill_conf.HurtTroop == 0 && skill_conf.HurtAttackType == 0) ||
                        this.Conf.Troop == skill_conf.HurtTroop || this.Conf.AttackType == skill_conf.HurtAttackType)
                    {
                        this.SpecialAttack(type, skill_conf.SkillID);
                    }
                }
            }
        }
    }

    //复活技能
    ReviveSkill(skillid)
    {
        let skill_conf = LoadConfig.getConfigData(ConfigName.TeShuJiNeng,skillid);
        if (!skill_conf)
            return;
        

        let tm = this.Manager.CurTm() + skill_conf.RespawnDelay / 10;
        let team = this.Manager.GetTeam(this.Camp);
        team.Add2ReviveList(this, tm, this.Pos, skill_conf.RespawnBlood);
    }
    


    //吸血
    Vampire(hurt, skill_conf)
    {
        if (skill_conf.AddEffectType != 1)
        {
            return;
        }   

        let add = BigNumber.cal([hurt, BigNumber.create(skill_conf.AddEffectPara), "10000"],[BigNumberCalType.Mul, BigNumberCalType.Div]);
        this.AddHP(add);
    }

    //反弹
    Rebound(hurt, master, skill_conf)
    {
        if (hurt == BigNumber.zero)
            return;
    

        let rate = this.GetBuffEffectMax(FightBuffType.Rebound);
        if (rate <= 0)
            return;
        

        if (!master.ImmuneFlag[FightBuffType.Rebound])
            return;
        

        let rebound_hurt = BigNumber.cal([hurt, BigNumber.create(rate), "10000"], [BigNumberCalType.Mul, BigNumberCalType.Div]);
        master.SubHP(rebound_hurt, HurtT.Attack, skill_conf.ID, this.UUID);
        master.TriggerSpecialAttack(SpecialSkillType.Hurt);
    }
   


    QuitHiding(master)
    {
        if (!this.IsRef(RoleRef.Hiding))
            return;
    

        this.ClearRef(RoleRef.Hiding);
        let skillconf = LoadConfig.getConfigData(ConfigName.JiNeng,this.GetCanUseSkillID());
        if (skillconf && this.CheckIsTarget(master, skillconf))
            this.ChangeTarget(master);
        else
            this.ChangeTarget(null);
    }

    //受击
    Wound(master, skill_conf, hurtt)
    {
        if (!this.Vaild)
            return;
        

        //无敌判断
        if (this.IsRef(RoleRef.Invincible))
            return;
        

        //免疫判断
        if (master.Conf.HurtType == FightHurtType.Physical && this.GetBuffCount(FightBuffType.PI) > 0)
        {
            this.Manager.AddHudRecord(this, HudT.PIMM);
            return;
        }
        else if (master.Conf.HurtType == FightHurtType.Magical && this.GetBuffCount(FightBuffType.MI) > 0)
        {
            this.Manager.AddHudRecord(this, HudT.MIMM);
            return;
        }

        //命中率判断
        if (!(master.CheckIsHit(this)))
        {
            this.Manager.AddHudRecord(this, HudT.MISS);
            return;
        }

        //计算伤害 扣血
        this.QuitHiding(master);
        let hurt = 0;
        let crit = false;
        if (skill_conf.IsHurt == 0)
        {
            const [hurt, crit] = master.CalcHurt(this, skill_conf);
            if (crit)
            {
                this.Manager.AddHudRecord(this, HudT.CRIT);
            } 

            if (this.SubHP(hurt, hurtt, skill_conf.ID, master.UUID))
            {
                this.TriggerSpecialAttack(SpecialSkillType.Die); //触发死亡特殊技能
                return;
            }

            this.TriggerSpecialAttack(SpecialSkillType.Hurt); //触发受击特殊技能
            this.Rebound(hurt, master, skill_conf);
            if (this.ImmuneFlag[FightBuffType.Vampire] != 1)
            {
                master.Vampire(hurt, skill_conf);
            }
        }
        //log("hurt:"..hurt.." master."..master.ID.." target."..this.ID)

        //给受击者添加buff
        for(let i = 0; i < skill_conf.HurtEffectID.lenght; ++i)
        {
            let id = skill_conf.HurtEffectID[i];
            let prob = skill_conf.HurtEffectProb[i];
            if (prob > 0 && this.Manager.Rand(10000) <= prob)
            {
                let bufflist = this.Manager.RandomBuffByPoolID(id);
                for(let buff of bufflist)
                {
                    let buffconf = LoadConfig.getConfigData(ConfigName.Buff,buff.buffid);
                    //LoadConfig.getConfigData(ConfigName.Buff,buff.buffid);
                    if (buffconf && buffconf.Type == FightBuffType.DropBlood)
                        buff.params[2] = hurt;
                    
                    this.AddBuff(master, buff.buffid, buff.duration, buff.params);
                }
            }
        }
            
    }

    //死亡
    Die()
    {
        if (this.State && this.State.Type() == FightRoleState.Attack)
        {
            this.State.Interrupt();
        }

        this.Vaild    = false;
        this.Target   = null;
        this.Manager.AddDieRecord(this);

        let team = this.Manager.GetTeam(this.Camp);
        if  (this.RespawnTimes <= 0 && !this.IsCastle() && this.Manager.Rand(10000) <= team.RespawnProb)
        {
            this.RespawnTimes = this.RespawnTimes + 1;
            team.Add2ReviveList(this, this.Manager.CurTm() + 1000, team.GetBornPos(), 0);
            return;
        }
            
          //重生
        if (this.Respawn == 0)
            return;

        team.Add2ReviveList(this, this.Manager.CurTm() + this.Respawn, team.GetBornPos(), 0);
    }
    IsNeedReFindTarget(skillconf)
    {
        if (this.IsRef(RoleRef.LockTarget))
            return false;


        return this.Target.IsCastle() || this.Target.Camp == this.Camp || !this.CheckIsTarget(this.Target, skillconf);
    }
    //ai
    AICheck()
    {
        if (this.IsRef(RoleRef.LockState))
            return;
    

        if (this.State != null && this.State.Type() == FightRoleState.Attack)
            return;
        

        let skillid = this.GetCanUseSkillID();
        let skill_conf = LoadConfig.getConfigData(ConfigName.JiNeng,skillid);
        if (!skill_conf)
        {
            return;
        }
        
        let switchs = false;
        //寻找目标
        if (!this.IsTargetVaild() || this.IsNeedReFindTarget(skill_conf))
        {
            let team = null;
            if (this.GetBuffCount(FightBuffType.Charm) > 0)
                team = this.Manager.GetTeam(this.Camp);
            else
                team = this.Manager.GetEnemyTeam(this.Camp);
            
            let target = this.FindTarget(team, skill_conf);
            if (target != this.Target)
            {
                this.Target = target;
                switchs = true;
            }
        }
            
            
            //log("this camp:"..this.Camp.." target camp:"..this.Target.Camp.." su:"..this.UUID.." tu:"..this.Target.UUID)
        
        if (!this.IsTargetVaild())
            return;
        

        //判断是否在攻击范围内
        let reach = this.GetRange(skill_conf);
        // let dis   = this.Pos;
        // dis.sub(this.Target.Pos);
        var dir = this.Target.Pos.sub(this.Pos);
        //log("1dunjia dis:"..dis:magSqr().." reach:"..reach*reach.." tpos.x:"..this.Target.Pos.x.." y:"..this.Target.Pos.y.." pos.x:"..this.Pos.x.." y:"..this.Pos.y)
        if (dir.mag() <= reach * reach)
        {
            //if skill_conf.ID == 10640102 then
                //log("dunjia dis:"..dis:magSqr().." reach:"..reach*reach)
            //
            if (this.CheckCanAttack())
            {
                if(!this.CheckCD())
                {
                    if(this.Attack(skill_conf))
                    {
                        this.AttackTime = this.Manager.CurTm();
                    }
                }
            } 
            // && !this.CheckCD() && this.Attack(skill_conf))
            //     this.AttackTime = this.Manager.CurTm();
                //log("id:"..this.ID.." attacktm:"..this.AttackTime.." targetid:"..this.Target.UUID)
        }
        else if (switchs || !this.IsMoving())
        {
            if (this.CheckCanMove())
                this.Move();
        }
            
    }

    Update(elapse)
    {
        if (this.LifeTime != 0 && this.Manager.CurTm() - this.BornTime >= this.LifeTime)
        {
            this.Die();
            return;
        }   
        //this.RunBuff(this.Manager.CurTm());
        this.AICheck();
        if (this.State)
            this.State.Run(elapse);
    }

    //计算技能伤害
    CalcHurt(target, skill_conf)
    {
        let atk = this.ATK;
        let def = '0';
        let val = 0;
        if (this.Conf.HurtType == FightHurtType.Physical)
        {
            def = target.GetDEF();
            val = this.GetBuffEffect(FightBuffType.AddATK)-this.GetBuffEffect(FightBuffType.DecATK);
        }
        else if (this.Conf.HurtType == FightHurtType.Magical)
        {
            def = target.GetMDEF();
            val = this.GetBuffEffect(FightBuffType.AddMATK)-this.GetBuffEffect(FightBuffType.DecMATK);
        }

        //主线关卡优化
        if (this.Manager.FightType == FightSystemType.GuanQia)
        {
            if (atk.length >= def.length + 2)
                return [target.HP, false];
            else if (atk.length <= def.length - 2)
                return [BigNumber.zero, false];
        }

        //log("attack:"..atk.." defense:"..def.." value:"..val)
        if (val > 0)
        {
            let add = BigNumber.mul(atk, val);
            add = BigNumber.div(add, 10000);
            atk = BigNumber.add(atk, add);
        }

        if (!target.IsImmune(FightBuffType.GhostCurse) && skill_conf.AddEffectType == 2)
        {
            def = BigNumber.cal([def, BigNumber.create(10000 - skill_conf.AddEffectPara), "10000"], [BigNumberCalType.Mul, BigNumberCalType.Div]);
        }

        //计算伤害
        let crit = false;
        let hurt = BigNumber.sub(atk, def);
        //log("calc hurt atk:"..atk.." def:"..def)
        if (BigNumber.lessThanOrEqualTo(hurt, BigNumber.zero))
            return [BigNumber.zero, false];
        

        //log("crit random:"..this.Manager.Frame)
        //log("hurt:"..hurt)
        if (this.Manager.Rand(10000) <= this.GetCRI()) //暴击
        {
            crit = true;
            hurt = BigNumber.cal([hurt, this.GetCritDamage(), "10000", hurt],[BigNumberCalType.Mul, BigNumberCalType.Div, BigNumberCalType.Add]);
        }
            
        

        //log("hurt:"..hurt)
        //技能加成伤害
        let addition = 0;
        if (this.Conf.Race <= target.FightSpecAttr.length && target.FightSpecAttr[this.Conf.Race] != 0)
            addition = target.FightSpecAttr[this.Conf.Race];
        
        if (skill_conf)
        {
            addition = addition + skill_conf.Addition;
            hurt = BigNumber.add(hurt, BigNumber.create(skill_conf.FixedDamage));
        }
            
        
        if (addition != 0)
        {
            hurt = BigNumber.cal([hurt, BigNumber.create(addition), "10000", hurt],[BigNumberCalType.Mul, BigNumberCalType.Div, BigNumberCalType.Add]);
        }

        //log("hurt:"..hurt)
        //护盾减伤
        let shield = target.GetBuffEffectMax(FightBuffType.Shield);
        if (shield > 0)
        {
            let sub = BigNumber.cal([hurt, BigNumber.create(shield), "10000"],[BigNumberCalType.Mul, BigNumberCalType.Div]);
            
            //log("shield:"..shield.." hurt:"..hurt.." dec:"..sub)
            hurt = BigNumber.sub(hurt, sub);
        }
        //log("hurt:"..hurt)
        //console.error("done")
        return [hurt, crit];
    }
}
