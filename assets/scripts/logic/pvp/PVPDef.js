/*
作者（Author）:    SkyHuang

描述（Describe）:   定义玩家对战模式使用的常量
*/

export const ROOMSTATE = {
    WAIT1       : 1,
    BANRACE1	: 2,
    BANRACE2 	: 3,
    WAIT2       : 4,
    BANCARDS1 	: 5,
    BANCARDS2 	: 6,
    CLOTHARRAY	: 7,
    WAIT3       : 8,
    FIGHT 		: 9,
};

export const OPERATORCODE = {
    BANRACE     : 1,
    BANCARD	    : 2,
    CLOTHARRAY 	: 3,
};

