/*
作者（Author）:    SkyHuang

描述（Describe）:  定义玩家对战模式 禁止比赛状态控制
*/

import {OPERATORCODE} from './PVPDef';

export default class PVPBanRaceState
{
    constructor(player)
    {
        this.mPlayer= player;
    
        //self.__index = self
    };
    
    operater(objid, code, param)
    {
        if(code != OPERATORCODE.BANRACE)
        {
            return
        }

        if(objid == this.mPlayer.ObjID)
        {
            return
        }
        //暂时还没有网络请求
        // local req = pvp_msg_pb.PVPPart_BanRaceRequest()
        // req.Races = param
        // SocketClient.callServerFun("PVPPart_BanRace", req)
    };

    operaterRet(objid, param)
    {
        for(let id of param)        
        {
            this.mPlayer.BanRaces.push(id);
        }
    }
}
