/*
作者（Author）:    SkyHuang

描述（Describe）:  定义玩家对战模式 禁止卡状态控制
*/

import {OPERATORCODE} from './PVPDef';

export default class PVPBanCardsState
{
    constructor(player)
    {
        this.mPlayer= player;
    
        //self.__index = self
    };
    
    operater(objid, code, param)
    {
        if(code != OPERATORCODE.BANCARD)
        {
            return
        }

        if(objid == this.mPlayer.ObjID)
        {
            return
        }
        //暂时还没有网络请求
        // let req = pvp_msg_pb.PVPPart_BanCardsRequest()
        // req.Cards = param
        // SocketClient.callServerFun("PVPPart_BanCards", req)
    };

    operaterRet(objid, param)
    {
        for(let id of param)        
        {
            this.mPlayer.BanCards.push(id);
        }
    }
}
