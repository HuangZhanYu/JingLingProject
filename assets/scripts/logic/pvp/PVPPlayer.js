/*
作者（Author）:    SkyHuang

描述（Describe）:  定义玩家对战模式 玩家数据基类
*/

export default class PvpPlayer
{
    constructor(param)
    {
        if(!param)
        {
            console.log('FightRole：玩家数据为空');
            return
        }
        this.mObjID       = param.ObjID;   
        this.mNick        = param.Nick;
        this.mIcon        = param.Icon;
        this.mLevel       = param.Level;
        this.mBossID      = param.BossID;
        this.mCards       = param.CardList;
        this.mServerName  = param.ServerName;
        this.mBanRaces    = [];
        this.mBanCards    = [];
        this.mFightCards  = [];
        this.mReady       = false;

        //self.__index = PVPPlayer;
    };

    //获取禁用卡牌列表
    getBanCards()
    {
        let list = [];
        for(let item of this.mCards)
        {
            let exist = false;
            let baseid = Math.floor(item.CardID/10)*10;
            for(let id of list)
            {
                if(id == baseid)
                {
                    exist = true;
                    break;
                }
            }
            if(!exist)
            {
                list.push(baseid);
            }
        }
        return list;
    };

    //获取可用于上阵的卡牌列表
    getUsableCards()
    {
        //需要读表，暂时这样 （会报错）
        return null;

        let list = [];
        for(let item of this.mCards)
        {
            //需要读表，暂时这样 （会报错）
            let conf = LoadConfig.getConfigData(ConfigName.ShiBing,item.CardID);
            if(conf)
            {
                let exist = false;
                for(let race of this.mBanCards)
                {
                    if(race == conf.Race)
                    {
                        exist = true;
                        break;
                    }
                }
                if(!exist)
                {
                    for(let id of this.mBanCards)
                    {
                        if(id == Math.floor(item.CardID/10)*10)
                        {
                            exist = true;
                            break;
                        }
                    }
                    if(!exist)
                    {
                        list.push(item);
                    }
                }
            }
        }
        return list;
    }
}
