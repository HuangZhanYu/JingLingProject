/*
作者（Author）:    SkyHuang

描述（Describe）:  定义玩家对战模式 布阵列状态控制
*/

import {OPERATORCODE} from './PVPDef';

export default class PvpClothArrayState
{
    constructor(players)
    {
        this.mPlayers= players;
    
        //self.__index = self
    };

    getPlayer(objid)
    {
        for(let player of this.mPlayers)        
        {
            if(player.ObjID == objid)
            {
                return player;
            }
        }
        return null;
    };
    
    operater(objid, code, param)
    {
        if(code != OPERATORCODE.CLOTHARRAY)
        {
            return;
        }

        let player = this.getPlayer(objid)
        if (player && player.Ready || !player)
        {
            return;
        }
        //暂时还没有网络请求
        // local req = pvp_msg_pb.PVPPart_ClothArrayRequest()
        // req.Cards = param
        // SocketClient.callServerFun("PVPPart_ClothArray", req)
    };

    operaterRet(objid, param)
    {
        let player = this.getPlayer(objid);
        player.FightCards = param;
        player.Ready = true;
    }
}
