/*
作者（Author）:    SkyHuang

描述（Describe）: 定义玩家对战模式 控制对战里面所有状态
*/

export default class PVPRoom
{
    constructor(players)
    {
        this.mPlayers = players;
        this.mStates  = 
        [
            new PVPWaitState(),
            new PVPBanRaceState(players[0]),
            new PVPBanRaceState(players[1]),
            new PVPWaitState(),
            new PVPBanCardsState(players[1]),
            new PVPBanCardsState(players[0]),
            new PVPClothArrayState(players),
            new PVPWaitState(),
            new PVPFightState(players),
        ];
        this.mStateIndex = 1;

        //self.__index = PVPRoom
    };
    
    //状态切换
    changeState(index)
    {
        this.mStateIndex = index;
    };

    //玩家操作
    operator(objid, code, param)
    {
        this.mStates[this.mStateIndex].operater(objid, code, param);
    };
    
    //服务器操作同步
    operatorRet(objid, param)
    {
        this.mStates[this.mStateIndex].operaterRet(objid, param);
    };

    //获取自己
    getMyself()
    {
        for(let player of this.mPlayers)        
        {
            if(player.ObjID == PlayerBasePart.ObjID)
            {
                return player;
            }
        }
        return null;
    };

    //获取对手
    getOpponent()
    {
        for(let player of this.mPlayers)        
        {
            if(player.ObjID != PlayerBasePart.ObjID)
            {
                return player;
            }
        }
        return null;
    }
}
