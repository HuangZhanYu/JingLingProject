/*
作者（Author）:    jason

描述（Describe）: 游戏内事件ID
*/

export const MessageID = {
    MsgID_LoadConfigProcess : "LoadConfigProcess",  //配置表加载进度
    MsgID_LoadConfigFinished : "LoadConfigFinished",
    MsgID_LoginSucceed:"LoginSucceed",              //登陆成功
    MsgID_LoginError:"LoginError",
    MsgID_CreatePlayerSucceed:"CreatePlayerSucceed",    //创建角色成功
    MsgID_CreatePlayerError:"CreatePlayerError",        //创建角色失败
    MsgID_CheckNameSuccess:"CheckNameSuccess",          //名字检查成功
    MsgID_AddGoldResponse:"AddGoldResponse",            //增加金币的返回
    MsgID_ShowSpeedUp:"ShowSpeedUp",                    //显示加速
};