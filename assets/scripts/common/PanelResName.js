/*
作者（Author）:    jason

描述（Describe）: 界面资源名字
*/

export const PanelResName = {
    loginPanel:"uiprefab/loginPanel",
    CreateRoolPanel:"uiprefab/CreateRolePanel",
    OneGeniusPanel:"uiprefab/OneGeniusPanel",
    AlertPanel:"uiprefab/AlertPanel",
    MainPanel:"uiprefab/MainPanel",
    BackPackPopPanel:"uiprefab/BackPackPopPanel",
    BottomFunctionPanel:"uiprefab/BottomFunctionPanel",
    MorePanel:"uiprefab/MorePanel",
    TaskPopPanel:"uiprefab/TaskPopPanel/TaskPopPanel",
    HuoDeWuPinTiShiPanel:"uiprefab/HuoDeWuPinTiShiPanel",
    ShengJiPanel:"uiprefab/ShengJiPanel",
    DungeonPopPanel:"uiprefab/DungeonPopPanel",
    DungeonDiffPopPanel:"uiprefab/DungeonDiffPopPanel",
    DungeonZhenRongPanel:"uiprefab/DungeonZhenRongPanel",
    EquipsPanel:"uiprefab/BagPrefab/EquipsPanel",
    HerosPanel:"uiprefab/BagPrefab/HerosPanel",
    ItemsPanel:"uiprefab/BagPrefab/ItemsPanel",
    ItemPrefab:"uiprefab/BagPrefab/ItemPrefab",
    MailPrefab:"uiprefab/MailPrefab/MailPanel",
    DungeonResultPopPanel:"uiprefab/TongGuanPrefab/DungeonResultPopPanel",
    OffLinePopPanel:"uiprefab/OffLinePopPanel",
    CommonFightPanel:"uiprefab/CommonFightPanel",
};
// 公共预设枚举
export const CommonPrefabNames = {
    Portrait: "uiprefab/CommonPrefab/Portrait",//-- 头像
    PlayerHeadPrefab:"uiprefab/CommonPrefab/Playerheadprefab",//-- 带VIP头像
    DaoJuPrefab:"uiprefab/CommonPrefab/DaoJuPrefab",//--新资源预设
    SkillPrefabs:"uiprefab/CommonPrefab/SkillPrefabs",//--新资源预设
    DeadTimePrefab:"uiprefab/CommonPrefab/DeadTimePrefab",//--新资源预设
};