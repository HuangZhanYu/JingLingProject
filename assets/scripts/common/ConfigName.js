/*
作者（Author）:    jason

描述（Describe）: 配置表的名字
*/
/*jshint esversion: 6 */
const ConfigName = {
    BaiWan_JinHuaYueShi:"BaiWan_JinHuaYueShi",
    BaiWan_MiaoShu:"BaiWan_MiaoShu",
    BaiWan_QiangHuaXiaoHao:"BaiWan_QiangHuaXiaoHao",
    BaiWan_ShengPinXiaoHao:"BaiWan_ShengPinXiaoHao",
    BangZhu:"BangZhu",
    BaoXiang_JiangChi:"BaoXiang_JiangChi",
    BaoXiang_JiangChi_GuDing:"BaoXiang_JiangChi_GuDing",
    Buff:"Buff",
    BuffPool:"BuffPool",
    Buff_ZhuoChong:"Buff_ZhuoChong",
    CanShu:"CanShu",
    ChangYong:"ChangYong",
    Chengjiu:"Chengjiu",
    ChongZhi:"ChongZhi",
    ChongZhi_BangZhu:"ChongZhi_BangZhu",
    ChuShengZiYuan:"ChuShengZiYuan",
    DanMu:"DanMu",
    DaoGuanZhan_DaoGanPaiMing:"DaoGuanZhan_DaoGanPaiMing",
    DaoGuanZhan_DaoJu:"DaoGuanZhan_DaoJu",
    DaoGuanZhan_DiTu:"DaoGuanZhan_DiTu",
    DaoGuanZhan_GeRenPaiMing:"DaoGuanZhan_GeRenPaiMing",
    DaoGuanZhan_JiangLi:"DaoGuanZhan_JiangLi",
    DaoGuanZhan_LingDi:"DaoGuanZhan_LingDi",
    DaoGuanZhan_ShiJian:"DaoGuanZhan_ShiJian",
    DaoGuanZhan_ZhenRong:"DaoGuanZhan_ZhenRong",
    DaoGuanZhan_ZhenRongFenPei:"DaoGuanZhan_ZhenRongFenPei",
    DaoGuan_BossDiQu:"DaoGuan_BossDiQu",
    DaoGuan_BossPaiMingJiangLi:"DaoGuan_BossPaiMingJiangLi",
    DaoGuan_BossPeiZhi:"DaoGuan_BossPeiZhi",
    DaoGuan_DaKaCiShu:"DaoGuan_DaKaCiShu",
    DaoGuan_JianZhu:"DaoGuan_JianZhu",
    DaoGuan_JianZhuShengJi:"DaoGuan_JianZhuShengJi",
    DaoGuan_JuanXian:"DaoGuan_JuanXian",
    DaoGuan_JuanXianXin:"DaoGuan_JuanXianXin",
    DaoGuan_QuanXian:"DaoGuan_QuanXian",
    DaoGuan_ShengJi:"DaoGuan_ShengJi",
    DaoGuan_ShiJian:"DaoGuan_ShiJian",
    DaoGuan_TuBiao:"DaoGuan_TuBiao",
    DaoGuan_ZengYiJingLing:"DaoGuan_ZengYiJingLing",
    DaoGuan_ZengYiShuXing:"DaoGuan_ZengYiShuXing",
    DaoGuan_ZhouPaiMingJiangLi:"DaoGuan_ZhouPaiMingJiangLi",
    DianFeng_DuanWei:"DianFeng_DuanWei",
    DianFeng_JiQiRen:"DianFeng_JiQiRen",
    DianFeng_PiPeiJiFeng:"DianFeng_PiPeiJiFeng",
    DianFeng_RiChang:"DianFeng_RiChang",
    DianFeng_SaiJiChengJiu:"DianFeng_SaiJiChengJiu",
    DiXiaCheng:"DiXiaCheng",
    DiXiaCheng_GuanKa:"DiXiaCheng_GuanKa",
    DuiWuShuXing:"DuiWuShuXing",
    DuiWuShuXing_Main:"DuiWuShuXing_Main",
    FanYi:"FanYi",
    FeiXingDaoJu:"FeiXingDaoJu",
    GengDuoXiangQing:"GengDuoXiangQing",
    GongNengCiShu:"GongNengCiShu",
    GongNengKaiFang:"GongNengKaiFang",
    GuanQia:"GuanQia",
    GuanQia_LiChengBei:"GuanQia_LiChengBei",
    GuanQia_PeiZhi:"GuanQia_PeiZhi",
    HeCheng_JingLing:"HeCheng_JingLing",
    HeCheng_ZhuangBei:"HeCheng_ZhuangBei",
    Huodong_ChongZhi:"Huodong_ChongZhi",
    Huodong_DengLuJiangLi:"Huodong_DengLuJiangLi",
    Huodong_JingLingQiYue:"Huodong_JingLingQiYue",
    HuoQuTuJin:"HuoQuTuJin",
    HuoQu_TuJingType:"HuoQu_TuJingType",
    HuoQu_XinHuoQuTuJing:"HuoQu_XinHuoQuTuJing",
    JiangChi:"JiangChi",
    JianSheBiaoXian:"JianSheBiaoXian",
    JinBiRenWu:"JinBiRenWu",
    JiNeng:"JiNeng",
    JiNeng_BiaoXian:"JiNeng_BiaoXian",
    JiNeng_ZhuDong:"JiNeng_ZhuDong",
    JiNeng_ZhuDong_BiaoXian:"JiNeng_ZhuDong_BiaoXian",
    JiNeng_ZhuDong_JiangChi:"JiNeng_ZhuDong_JiangChi",
    JiNeng_ZhuDong_KaCao:"JiNeng_ZhuDong_KaCao",
    JiNeng_ZhuoChong:"JiNeng_ZhuoChong",
    JingJiChang_DianFeng:"JingJiChang_DianFeng",
    JingJiChang_JieSuan:"JingJiChang_JieSuan",
    JingJiChang_PaiMing:"JingJiChang_PaiMing",
    JingLingGaoYuan:"JingLingGaoYuan",
    JingLingGaoYuan_ZhenRong:"JingLingGaoYuan_ZhenRong",
    JingLingJiBan:"JingLingJiBan",
    JingLingJiBan_JinDuJiangLi:"JingLingJiBan_JinDuJiangLi",
    JingLingSho1uJi:"JingLingSho1uJi",
    JingLingShouJi:"JingLingShouJi",
    JingLingShuXingZhanShi:"JingLingShuXingZhanShi",
    //JuJingType:"JuJingType",
    LiHe_JiangChi:"LiHe_JiangChi",
    LiHe_PeiZhi:"LiHe_PeiZhi",
    LiXianYuLan:"LiXianYuLan",
    MingZiKu:"MingZiKu",
    MoXing:"MoXing",
    RenWu_RiChang:"RenWu_RiChang",
    RenWu_RiChangCiShu:"RenWu_RiChangCiShu",
    RenWu_XinShou:"RenWu_XinShou",
    ShangDianLeiXing:"ShangDianLeiXing",
    ShangDianPeiZhi:"ShangDianPeiZhi",
    ShangDianShangPinPeiZhi:"ShangDianShangPinPeiZhi",
    ShanXianBiaoXian:"ShanXianBiaoXian",
    ShiBing:"ShiBing",
    ShiBingXingGe:"ShiBingXingGe",
    ShiBingXingGe_JiangChi:"ShiBingXingGe_JiangChi",
    ShiBing_JinBiLV:"ShiBing_JinBiLV",
    ShiBing_JiNengMiaoShu:"ShiBing_JiNengMiaoShu",
    ShiBing_JinHua:"ShiBing_JinHua",
    ShiBing_TianFuDengJi:"ShiBing_TianFuDengJi",
    ShiBing_TianFuKaiFangDengJi:"ShiBing_TianFuKaiFangDengJi",
    ShiBing_TianFuShuXing:"ShiBing_TianFuShuXing",
    ShiBing_XunLianDengJi:"ShiBing_XunLianDengJi",
    ShiBing_ZhuoChong:"ShiBing_ZhuoChong",
    ShouJiShuXing_HuiZong:"ShouJiShuXing_HuiZong",
    ShouJi_FenLei:"ShouJi_FenLei",
    ShuXingIcon:"ShuXingIcon",
    ShuXingKu:"ShuXingKu",
    ShuZhiShuXingDingYi:"ShuZhiShuXingDingYi",
    ShuZhiShuXingDingYi_HuiZong:"ShuZhiShuXingDingYi_HuiZong",
    SiWangBiaoXian:"SiWangBiaoXian",
    TeShuJiNeng:"TeShuJiNeng",
    TiShiXinXi:"TiShiXinXi",
    TuiJianZhenRong:"TuiJianZhenRong",
    TuiSong:"TuiSong",
    TuJian:"TuJian",
    TuJianJingLingShouJi:"TuJianJingLingShouJi",
    TuJianZhuangBeiShouJi:"TuJianZhuangBeiShouJi",
    VIP_GongNeng:"VIP_GongNeng",
    VIP_MiaoShu:"VIP_MiaoShu",
    //WanFaZhiNan:"WanFaZhiNan",
    WenJuan:"WenJuan",
    XiaoJiQiao:"XiaoJiQiao",
    XinHuoQuTuJing:"XinHuoQuTuJing",
    XinShouZhanDou:"XinShouZhanDou",
    XuanYao:"XuanYao",
    YanShiZhanDou:"YanShiZhanDou",
    YinDao:"YinDao",
    YinDao_BuZhou:"YinDao_BuZhou",
    YinXiao:"YinXiao",
    Yiwu:"Yiwu",
    YiWuTaoZhuang:"YiWuTaoZhuang",
    YiWu_JinHua:"YiWu_JinHua",
    YiWu_QiangHua:"YiWu_QiangHua",
    YouJian:"YouJian",
    ZhanDouPeiZhi:"ZhanDouPeiZhi",
    ZhanDouTa_JiangLi:"ZhanDouTa_JiangLi",
    ZhanDouTa_PeiZhi:"ZhanDouTa_PeiZhi",
    ZhanDouTa_ZhenRong:"ZhanDouTa_ZhenRong",
    ZheSuan_DaoGuan:"ZheSuan_DaoGuan",
    ZheSuan_JiBan:"ZheSuan_JiBan",
    ZheSuan_ZhuangBei:"ZheSuan_ZhuangBei",
    ZhiShuShuZhi:"ZhiShuShuZhi",
    ZhuJieMianShangDian:"ZhuJieMianShangDian",
    ZhuJue_Dengji:"ZhuJue_Dengji",
    ZhuJue_GengDuoXiangQing:"ZhuJue_GengDuoXiangQing",
    ZhuJue_TouXiang:"ZhuJue_TouXiang",
    ZhuoChongChangjingPeiZhi:"ZhuoChongChangjingPeiZhi",
    ZhuoChongDengJi:"ZhuoChongDengJi",
    ZhuoChongDuiHuan:"ZhuoChongDuiHuan",
    ZhuoChongPeiZhi:"ZhuoChongPeiZhi",
    ZhuoChong_ChangJingBaoDi:"ZhuoChong_ChangJingBaoDi",
    ZhuoChong_SouXunBaoDi:"ZhuoChong_SouXunBaoDi",
    ZiYuan_DaoGuan:"ZiYuan_DaoGuan",
    ZiYuan_DaoJu:"ZiYuan_DaoJu",
    ZiYuan_HuoBi:"ZiYuan_HuoBi",
    ZiYuan_JiaSu:"ZiYuan_JiaSu"
};
window.ConfigName = ConfigName;