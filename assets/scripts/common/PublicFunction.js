/*
作者（Author）:    jason

描述（Describe）: 游戏内通用函数处理脚本
*/
/*jshint esversion: 6 */
import BigNumber from "../tool/BigNumber";
import { PlayerStrAttr } from "../part/PlayerBasePart";

//安全检查javaScript基本类型和内置对象
//参数：0 表示检查的值
/*
    返回值：返回字符串 "undefined","number","boolean","string","function","regexp","array","date","error","object","null"
*/
export const jsType = {
    undefined:"undefined",
    number:"number",
    boolean:"boolean",
    string:"string",
    array:"array"
};
export function typeOf(o){
    //获取对象的toString()方法引用
    let _toString = Object.prototype.toString;
    //列举基本数据类型和内置对象类型
    let _type = {
        "undefined":"undefined",
        "number":"number",
        "boolean":"boolean",
        "string":"string",
        "[object Function]":"function",
        "[object RegExp]":"regexp",
        "[object Array]":"array",
        "[object Date]":"date",
        "[object Error]":"error"
    };
    return _type[typeof o] || _type[_toString.call(o)] || (o ? "object":"null");
}
//判断当前金币是否足够升级  返回BOOL和升级消耗
export function GetPlayerCanBuyBool(count,taskId,taskLv){
    let canOffer = true;
    let costString = "";
    let bigNumPlayer = BigNumber.create(PlayerBasePart.StrAttr[PlayerStrAttr.PlayerStrAttr_BigNumberGold]);

    if(taskLv == 0)
    {
        let bigNumCost = TaskPart.GetTaskInfo(taskId).OpenTaskCost;
        
        let tempBool = BigNumber.greaterThanOrEqualTo(bigNumPlayer,bigNumCost);
        costString =  BigNumber.getValue(bigNumCost);
        if(tempBool)
            canOffer = true;
        else
            canOffer = false;
    }
    else if(taskLv > 0)
    {
        //如果是1次直接用缓存
        if(count == 1)
        {
            let oneCost = TaskPart.GetTaskInfo(taskId).UpLevelCostReal;
            let tempBool = BigNumber.greaterThanOrEqualTo(bigNumPlayer,BigNumber.create(oneCost));
            if(tempBool)
                canOffer = true;
            else
                canOffer = false;
            
            costString = oneCost;
            return [canOffer,costString];
        }
        //如果是10次 直接用缓存
        if(count == 10)
        {
            let tenCost = TaskPart.GetTaskInfo(taskId).UpLevelTenCostReal;
            let tempBool = BigNumber.greaterThanOrEqualTo(bigNumPlayer,BigNumber.create(tenCost));
            if(tempBool)
                canOffer = true;
            else
                canOffer = false;
            
            costString = tenCost;
            return [canOffer,costString];
        }
            
        

        //如果是100 500 1a次 直接用缓存
        let hunCost = TaskPart.GetTaskInfo(taskId).UpLevelHunCostReal;
        let tempBool = BigNumber.greaterThanOrEqualTo(bigNumPlayer,BigNumber.create(hunCost));
        if(tempBool)
            canOffer = true;
        else
            canOffer = false;
        
        costString = hunCost;
        return [canOffer,costString];
    }
    
    return [canOffer,costString];
}
    