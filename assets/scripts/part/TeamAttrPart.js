let team_attr_msg_pb = require('team_attr_msg').msg;

let TeamAttrPart = { };

TeamAttrPart.Type = {
	None: 0,
	//空
	Count: 1,
	//上阵数量
	Quality: 2,
	//上阵数量，品质数量
	JinHua: 3,
	//上阵数量，品质数量，进化数量
	Max: 4,
	//最大值

};
TeamAttrPart.TeamAttr = { };
//当前的队伍属性
//结构明细
//.ID           --当前的ID，int
//.IsTrigger    --是否激活，bool
//获取方式
// for i = TeamAttrPart.Type.None + 1, TeamAttrPart.Type.Max - 1 do 
//     local typ = i  --i就是类型
//     local data = TeamAttrPart.TeamAttr[typ]
//     local id = data.ID                  --队伍属性表ID
//     local isTrigger = data.IsTrigger    --是否激活
// end
//信息刷新
TeamAttrPart.UpdateData = function(buffer) {
	var msg = team_attr_msg_pb.TeamAttrPart_TriggerResponse.decode(buffer);

	TeamAttrPart.TeamAttr = msg.Datas;
	//如果界面开启，刷新
}
TeamAttrPart.NewTriggerList = function(buffer) {
	var msg = team_attr_msg_pb.TeamAttrPart_NewTriggerResponse.decode(buffer);

	DuiWuZengYiPopPanel.ShowAchievementTips(msg.IDs);
}
window.TeamAttrPart = TeamAttrPart;