//region *.lua
//Date
//此文件由[BabeLua]插件自动生成
let playerbase_msg_pb = require('playerbase_msg').msg;

import BigNumber from '../tool/BigNumber';
import ViewManager, { PanelPos } from '../manager/ViewManager';
import { PanelResName } from '../common/PanelResName';
import PopupsController from '../view/PopupsController';

let PlayerBasePart = { };

PlayerBasePart.ObjID = '';
PlayerBasePart.IntAttr = { };
PlayerBasePart.StrAttr = { };
PlayerBasePart.TimeAttr = { };
PlayerBasePart.HeadIcons = { };
export const PlayerIntAttr = {
	PlayerIntAttr_DengJi: 0,
	//等级  骑士团等级
	PlayerIntAttr_JingYan: 1,
	//当前经验
	PlayerIntAttr_TiLi: 2,
	//体力
	PlayerIntAttr_TotalPassGuanQiaLevel: 3,
	//历史通关关卡次数
	PlayerIntAttr_RongYuBi: 4,
	//荣誉币
	PlayerIntAttr_TouXiangID: 5,
	//头像ID
	PlayerIntAttr_HuPoBi: 6,
	//琥珀币  地下城获取
	PlayerIntAttr_ZuanShi: 7,
	//钻石
	PlayerIntAttr_ZhanDouTaTiaoZhanCiShu: 8,
	//战斗塔挑战次数
	PlayerIntAttr_ZhanDouTaLiShiZuiGao: 9,
	//战斗塔历史最高层
	PlayerIntAttr_ZhanDouTaGouMaiCiShu: 10,
	//战斗塔今日购买的挑战次数
	PlayerIntAttr_ChangeNameCount: 11,
	//修改名称次数
	PlayerIntAttr_ZhanDouLi: 12,
	//战斗力
	PlayerIntAttr_Vip: 13,
	//vip等级
	PlayerIntAttr_CurrentVipExpPercent: 14,
	//vip当前经验
	PlayerIntAttr_HeighestRank: 15,
	//排名最高
	PlayerIntAttr_RevivalNumber: 16,
	//复活次数
	PlayerIntAttr_TaskGoldBuff: 17,
	//任务金币加成
	PlayerIntAttr_MedalBuff: 18,
	//勋章魔法
	PlayerIntAttr_CatchPetOrange: 19,
	//橙色抓宠大师   
	PlayerIntAttr_CatchPetPurple: 20,
	//紫色抓宠大师
	PlayerIntAttr_CatchPetBlue: 21,
	//蓝色抓宠大师 
	PlayerIntAttr_CatchPetGreen: 22,
	//绿色抓宠大师
	PlayerIntAttr_Sex: 23,
	//性别  1 男 2 女 
	PlayerIntAttr_ArenaFightCount: 24,
	//竞技场挑战次数
	PlayerIntAttr_EnhanceCount: 25,
	//训练卡牌
	PlayerIntAttr_X4SpeedCount: 26,
	//四倍加速计数
	PlayerIntAttr_PlateauTop: 27,
	//精灵高原最高关卡  精灵高原使用中
	PlayerIntAttr_PlateauBuyCount: 28,
	//精灵高原今日购买的挑战次数 使用中 
	PlayerIntAttr_PlateauChallengeCount: 29,
	//精灵高原今日挑战次数
	PlayerIntAttr_GuildCoin: 30,
	//公会币
	PlayerIntAttr_PeakArenaFightCount: 31,
	//巅峰竞技场挑战次数   张洋凡
	PlayerIntAttr_BattlePoint: 32,
	//竞技点
	PlayerIntAttr_PeakArenaBuyCount: 33,
	//巅峰的今日购买挑战次数
	PlayerIntAttr_JingTongShi: 34,
	//精通石
	PlayerIntAttr_ZhanDouTaZongTongGuan: 35,
	//战斗塔历史总通关层数
	PlayerIntAttr_TeamTrainingLevel: 36,
	//队伍训练等级
	PlayerIntAttr_GuanQiaFailCount: 37,
	//关卡失败次数
	PlayerIntAttr_GuanQiaFailCount4Tips: 38,
	//连续失败次数  大于等于5次，则提示光效
	PlayerIntAttr_TotalCostDiamond: 39,
	//玩家累计消耗钻石
	PlayerIntAttr_RechargePoints: 40,
	//充值积分
	PlayerIntAttr_LandgraveMedal: 41,
	//道馆战勋章
	PlayerIntAttr_PlateauStar: 42,
	//精通塔星星总数
	PlayerIntAttr_Max: 100
};
export const PlayerStrAttr = {
	PlayerStrAttr_MingZi: 0,
	//名字
	PlayerStrAttr_TouXiang: 1,
	//头像
	PlayerStrAttr_BigNumberGold: 2,
	//大数字金币
	PlayerStrAttr_BigNumberMedal: 3,
	//大数字勋章
	PlayerStrAttr_RelicMaterialA: 4,
	//遗物材料A  贝壳化石
	PlayerStrAttr_RelicMaterialB: 5,
	//遗物材料B
	PlayerStrAttr_RelicMaterialC: 6,
	//遗物材料C            
	PlayerStrAttr_RelicMaterialD: 7,
	//遗物材料D
	PlayerStrAttr_RelicMaterialE: 8,
	//遗物材料E
	PlayerStrAttr_RelicMaterialF: 9,
	//遗物材料F
	PlayerStrAttr_RelicMaterialG: 10,
	//遗物材料G
	PlayerStrAttr_WenZhang: 11,
	//累计纹章
	PlayerStrAttr_Max: 40
};
export const PlayerTimeAttr = {
	PlayerTimeAttr_LastTiLiHuiFuTime: 0,
	//体力恢复时间戳
	PlayerTimeAttr_CreateRoleTime: 1,
	//创建角色时间
	PlayerTimeAttr_ArenaFightTime: 2,
	//竞技场挑战时间戳
	PlayerTimeAttr_TowerFightTime: 3,
	//挑战塔挑战时间戳
	PlayerTimeAttr_ResetTime: 4,
	//上一次重置的时间  使用中 
	PlayerTimeAttr_PeakArenaFightTime: 5,
	//巅峰竞技场挑战时间戳
	PlayerTimeAttr_PeakArenaRewardTime: 6,
	//巅峰竞技场的增加竞技点时间戳
	PlayerStrAttr_Max: 20
};
PlayerBasePart.GetObjID = function() {
	return PlayerBasePart.ObjID;
}
PlayerBasePart.OnRecvBaseInfo = function(buffer) {
	let msg = playerbase_msg_pb.PlayerBase.decode(buffer);

	//logWarn('收到服务端下发的玩家基础信息'..tostring(msg.IntAttr))
	PlayerBasePart.ObjID = msg.ObjID;
	PlayerBasePart.StrAttr = msg.StrAttr;
	for (let i = 0; i != msg.IntAttr.length; ++i) {
		PlayerBasePart.IntAttr[i] = parseInt(msg.IntAttr[i]);
	}
	for (let i = 0; i != msg.TimeAttr.length; ++i) {
		PlayerBasePart.TimeAttr[i] = parseInt(msg.TimeAttr[i]);
	}
	//UI更新
	let MainPanel = ViewManager.getPanel(PanelResName.MainPanel);
	if(MainPanel)
	{
		MainPanel.UpdatePlayerInfo();
	}
}
PlayerBasePart.OnRecvChangeInfo = function(buffer) {
	if ((PlayerBasePart.IntAttr.length <= 0)) {
		return;
	}
	var msg = playerbase_msg_pb.PlayerBase_AttrChange.decode(buffer);

	//记录更新前数据
	var beforeDiamond = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_ZuanShi];
	var beforeHupobi = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_HuPoBi];
	var beforeGold = PlayerBasePart.StrAttr[PlayerStrAttr.PlayerStrAttr_BigNumberGold];
	var beforeMedal = PlayerBasePart.StrAttr[PlayerStrAttr.PlayerStrAttr_BigNumberMedal];
	var beforeVip = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_Vip];
	for (var i = 0; i != msg.IntAttr.IntAttrIndex.length; ++i) {
		PlayerBasePart.IntAttr[(msg.IntAttr.IntAttrIndex[i] + 1)] = parseInt(msg.IntAttr.IntAttr[i]);
	}
	for (var i = 0; i != msg.StrAttr.StrAttrIndex.length; ++i) {
		PlayerBasePart.StrAttr[(msg.StrAttr.StrAttrIndex[i] + 1)] = msg.StrAttr.StrAttr[i];
	}
	for (var i = 0; i != msg.TimeAttr.TimeAttrIndex.length; ++i) {
		PlayerBasePart.TimeAttr[(msg.TimeAttr.TimeAttrIndex[i] + 1)] = msg.TimeAttr.TimeAttr[i];
	}
	var afterGold = PlayerBasePart.StrAttr[PlayerStrAttr.PlayerStrAttr_BigNumberGold];
	var afterMedal = PlayerBasePart.StrAttr[PlayerStrAttr.PlayerStrAttr_BigNumberMedal];
	var afterVip = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_Vip];
	
	let MainPanel = ViewManager.getPanel(PanelResName.MainPanel);
	if(!MainPanel)
	{
		return;
	}
	MainPanel.UpdatePlayerInfo();
	var delta = (PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_ZuanShi] - beforeDiamond);
	if ((delta > 0)) {
		var str = ("+" + delta);
		MainPanel.CreatDiamondHudText(str);
	}
	var deltaHuPo = (PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_HuPoBi] - beforeHupobi);
	if ((deltaHuPo > 0)) {
		var str = ("+" + deltaHuPo);
		MainPanel.CreatHuPoHudText(str);
	}
	//计算插值 大于0 则创建飘字
	var delta = BigNumber.sub(BigNumber.create(afterGold), BigNumber.create(beforeGold));
	var compareResult = BigNumber.greaterThan(delta, BigNumber.create("0"));
	if (compareResult) {
		var str = ("+" + BigNumber.getValue(delta));
		MainPanel.CreatHudText(str);
		MainPanel.RefreshGoldText();
		//刷新金币
	}
	//计算插值 大于0 则创建纹章飘字
	var deltaMedal = BigNumber.sub(BigNumber.create(afterMedal), BigNumber.create(beforeMedal));
	var compareResultMedal = BigNumber.greaterThan(deltaMedal, BigNumber.create("0"));
	if (compareResultMedal) {
		var str = ("+" + BigNumber.getValue(deltaMedal));
		MainPanel.CreatMedalHudText(str);
		MainPanel.RefreshMedalText();
		//刷新文章
	}
	//vip升级弹窗
	var deltaVip = (afterVip - beforeVip);
	if ((deltaVip > 0)) {
		VipTQuanPopPanel.Show();
	}
}
PlayerBasePart.OnRecvChangeIntInfo = function(buffer) {
	var msg = playerbase_msg_pb.PlayerBase_IntChange.decode(buffer);

	var beforeDiamond = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_ZuanShi];
	var beforeHupobi = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_HuPoBi];
	var beforeVip = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_Vip];
	for (var i = 0; i != msg.IntAttrIndex.length; ++i) {
		PlayerBasePart.IntAttr[(msg.IntAttrIndex[i] + 1)] = parseInt(msg.IntAttr[i]);
	}
	var afterDiamond = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_ZuanShi];
	var afterHupobi = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_HuPoBi];
	var afterVip = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_Vip];
	//UI更新
	MainPanel.UpdatePlayerInfo();
	var delta = (afterDiamond - beforeDiamond);
	if ((delta > 0)) {
		var str = ("+" + delta);
		MainPanel.CreatDiamondHudText(str);
	}
	var deltaHuPo = (afterHupobi - beforeHupobi);
	if ((deltaHuPo > 0)) {
		var str = ("+" + deltaHuPo);
		MainPanel.CreatHuPoHudText(str);
	}
	var deltaVip = (afterVip - beforeVip);
	if ((deltaVip > 0)) {
		//vip升级
	}
}
PlayerBasePart.OnRecvChangeStrInfo = function(buffer) {
	var msg = playerbase_msg_pb.PlayerBase_StrChange.decode(buffer);

	//ogWarn('收到服务端下发的玩家基础信息'..tostring(msg.IntAttr))
	//存下之前的
	var beforeGold = PlayerBasePart.StrAttr[PlayerStrAttr.PlayerStrAttr_BigNumberGold];
	var beforeMedal = PlayerBasePart.StrAttr[PlayerStrAttr.PlayerStrAttr_BigNumberMedal];
	for (var i = 0; i != msg.StrAttrIndex.length; ++i) {
		PlayerBasePart.StrAttr[(msg.StrAttrIndex[i] + 1)] = msg.StrAttr[i];
	}
	//存下改变之后的值
	var afterGold = PlayerBasePart.StrAttr[PlayerStrAttr.PlayerStrAttr_BigNumberGold];
	var afterMedal = PlayerBasePart.StrAttr[PlayerStrAttr.PlayerStrAttr_BigNumberMedal];
	//UI更新
	MainPanel.UpdatePlayerInfo();
	//计算插值 大于0 则创建飘字
	var delta = BigNumber.sub(BigNumber.create(afterGold), BigNumber.create(beforeGold));
	var compareResult = BigNumber.greaterThan(delta, BigNumber.create("0"));
	if (compareResult) {
		var str = ("+" + BigNumber.getValue(delta));
		MainPanel.CreatHudText(str);
	}
	//计算插值 大于0 则创建纹章飘字
	var deltaMedal = BigNumber.sub(BigNumber.create(afterMedal), BigNumber.create(beforeMedal));
	var compareResultMedal = BigNumber.greaterThan(deltaMedal, BigNumber.create("0"));
	if (compareResultMedal) {
		var str = ("+" + BigNumber.getValue(deltaMedal));
		MainPanel.CreatMedalHudText(str);
	}
}
PlayerBasePart.OnRecvChangeTimeInfo = function(buffer) {
	var msg = playerbase_msg_pb.PlayerBase_TimeChange.decode(buffer);

	//ogWarn('收到服务端下发的玩家基础信息'..tostring(msg.IntAttr))
	for (var i = 0; i != msg.TimeAttrIndex.length; ++i) {
		PlayerBasePart.TimeAttr[(msg.TimeAttrIndex[i] + 1)] = msg.TimeAttr[i];
		//console.error(msg.TimeAttr[i])
	}
}
//升级时下发
PlayerBasePart.LevelUp = function(buffer) {
	var msg = playerbase_msg_pb.PlayerBase_LevelUp.decode(buffer);

	//升级回调
	//ShengJiPanel.Show(msg);
	PopupsController.getInstance().addShowPopup({urlOrPrefabOrNode : PanelResName.ShengJiPanel, panelPos : PanelPos.PP_Midle, param : msg});
	//修改等级
	PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_DengJi] = msg.NewLevel;
	//刷新界面，等级改变相关
	let MainPanel = ViewManager.getPanel(PanelResName.MainPanel);
	if(MainPanel)
	{
		MainPanel.OnPlayerLevelUp();
	}
	
	//升级传玩家信息到sdk
	//LuaSDKManager.SendUserInfo(UserInfoType.UserInfoType_UpLevel);
}
//更改头像请求
PlayerBasePart.ChangePortraitRequest = function(PortraitIndex) {
	var request = new  playerbase_msg_pb.PlayerBase_ChangePortraitRequest();
	request.PortraitIndex = PortraitIndex;
	SocketClient.callServerFun('PlayerBasePart_ChangePortrait', request);
	ViewManager.showLoading({});
}
//更改头像返回
PlayerBasePart.ChangePortraitResponse = function(buffer) {
	ViewManager.hideLoading();
	var msg = playerbase_msg_pb.PlayerBase_ChangePortraitResponse.decode(buffer);

	if ((msg.Flag != 0)) {
		console.error(("更改头像失败ID" + msg.Flag));
		return;
	}
	PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_TouXiangID] = msg.PortraitIndex;
	//刷新详情
	MainPanel.UpdateHead();
	ChangeHeadPopPanel.ChangeHeadSuccess();
}
//更改姓名请求
PlayerBasePart.ChangeNameRequest = function(name) {
	var request = new  playerbase_msg_pb.PlayerBase_ChangeNameRequest();
	request.Name = name;
	SocketClient.callServerFun('PlayerBasePart_ChangeName', request);
	ViewManager.showLoading({});
}
//更改姓名返回
PlayerBasePart.ChangeNameResponse = function(buffer) {
	ViewManager.hideLoading();
	var msg = playerbase_msg_pb.PlayerBase_ChangeNameResponse.decode(buffer);

	if ((msg.Flag != 0)) {
		if ((msg.Flag == 1)) {
			ToolTipPopPanel.Show(51);
		}
		if ((msg.Flag == 2)) {
			ToolTipPopPanel.Show(45);
		}
		return;
	}
	//刷新界面
	ChangeNamePopPanel.ChangeSuccess();
}
//更改猪脚请求
PlayerBasePart.ChangeMainCardRequest = function() {
	var request = new  playerbase_msg_pb.PlayerBase_ChangeMainCardRequest();
	SocketClient.callServerFun('PlayerBasePart_ChangeMainCard', request);
	ViewManager.showLoading({});
}
//更改猪脚返回
PlayerBasePart.ChangeMainCardResponse = function(buffer) {
	ViewManager.hideLoading();
	var msg = playerbase_msg_pb.PlayerBase_ChangeMainCardResponse.decode(buffer);

	JueSeXinXiPanel.SetRoleInfo();
	MainPanel.UpdataTuXiang();
	console.error(msg.Flag);
}
//接受头像信息
PlayerBasePart.HeadInfoResponse = function(buffer) {
	var msg = playerbase_msg_pb.PlayerBase_HeadIconResponse.decode(buffer);

	if ((msg.Flag != 0)) {
		console.error("错误，接受头像信息失败");
		return;
	}
	PlayerBasePart.HeadIcons = [];
	let Conf_ZhuJue_TouXiang = [];
	for (var i = 0; i != Conf_ZhuJue_TouXiang.length; ++i) {
		PlayerBasePart.HeadIcons[i] = { };
		PlayerBasePart.HeadIcons[i].IconID = LoadConfig.getConfigData(ConfigName.ZhuJue_TouXiang,i).TouXiangID;
		PlayerBasePart.HeadIcons[i].LockState = 0;
	}
	for (var i = 0; i < msg.HeadIcons.length; ++i) {
		for (let j = 0; j != PlayerBasePart.HeadIcons.length; ++j) {
			if ((PlayerBasePart.HeadIcons[j].IconID == msg.HeadIcons[i].IconID)) {
				PlayerBasePart.HeadIcons[j].LockState = msg.HeadIcons[i].LockState;
			}
		}
	}
	//排序
	//如果数量小于等于1  则不用排序
	if ((PlayerBasePart.HeadIcons.length > 1)) {
		PlayerBasePart.BackSort();
	}
	//console.error("头像数量"..PlayerBasePart.HeadIcons)
}
//阵容排序规则
PlayerBasePart.HeadCom = function(a, b) {
	var aHead = a;
	//CardPart.GetCard(a)
	var bHead = b;
	//CardPart.GetCard(b)
	//获取表格配置
	var aConf = LoadConfig.getConfigData(ConfigName.ZhuJue_TouXiang,aHead.IconID);
	var bConf = LoadConfig.getConfigData(ConfigName.ZhuJue_TouXiang,bHead.IconID);
	//根据是否解锁排序  解锁的在前
	if ((aHead.LockState != bHead.LockState)) {
		return (aHead.LockState > bHead.LockState);
	}
	//最后根据ID排序  ID 小的在前
	return (aHead.IconID < bHead.IconID);
}
//头像排序
PlayerBasePart.BackSort = function() {
	table.sort(PlayerBasePart.HeadIcons, PlayerBasePart.HeadCom);
}
//提示玩家的队伍训练等级
PlayerBasePart.UpTeamLevelRequest = function(count) {
	var request = new  playerbase_msg_pb.PlayerBase_TeamUpRequest();
	request.Count = count;
	SocketClient.callServerFun('PlayerBasePart_UpTeamLevel', request);
}
//提示玩家的队伍训练等级回应
PlayerBasePart.UpTeamLevelResponse = function(buffer) {
	var msg = playerbase_msg_pb.PlayerBase_TeamUpResponse.decode(buffer);

	if ((msg.Flag != 0)) {
		//console.error("升级训练等级失败，错误ID "..msg.Flag)
		return;
	}
	//小目标刷新
	XiaoMuBiaoPanel.SetInfo();
	//刷新队伍界面
	LineupPopPanel.RefreshDatas();
	LineupPopPanel.RefreshButton();
	LineupPopPanel.FreshAfterPopTrain();
}
//游戏进入后台信息
PlayerBasePart.OnAppPause = function() {
	var request = new  playerbase_msg_pb.PlayerBase_OnAppPauseRequest();
	SocketClient.callServerFun("PlayerBasePart_OnAppPauseRequest", request);
}
//游戏进入前台信息
PlayerBasePart.OnAppResume = function() {
	var request = new  playerbase_msg_pb.PlayerBase_OnAppResumeRequest();
	SocketClient.callServerFun("PlayerBasePart_OnAppPauseRequest", request);
}
//获取玩家字符串的值
PlayerBasePart.GetStringValue = function(strAttr) {
	var value = PlayerBasePart.StrAttr[strAttr];
	if (((value == "") || (value == null))) {
		console.error(("玩家字符串属性异常,枚举ID" + strAttr));
		return "数据异常";
	}
	return value;
}
//获取玩家整数数值
PlayerBasePart.GetIntValue = function(intAttr) {
	var value = PlayerBasePart.IntAttr[intAttr];
	if (((value == null) || (value < 0))) {
		console.error(("玩家整数属性异常,枚举ID" + intAttr));
		return -1;
	}
	return value;
}
//建议
PlayerBasePart.CommitAdvice = function(advice) {
	var req = new  msg_pb.CollectAdviceRequest();
	req.Advice = advice;
	SocketClient.callServerFun("OnCollectAdvice", req);
}
//endregion
window.PlayerBasePart = PlayerBasePart;