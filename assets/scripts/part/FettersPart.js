//文件定义：羁绊部件，管理羁绊相关的数据
//设计要点：
//负责人：张洋凡
let fetters_msg_pb = require('fetters_msg').msg;
import { PlayerIntAttr } from './PlayerBasePart';
import ViewManager from '../manager/ViewManager';


let FettersPart = { };

//羁绊品质枚举
FettersPart.QualityEnum = {
	Green: 1,
	Blue: 2,
	Purple: 3,
	Orange: 4,
	Red: 5
};
FettersPart.FettersQualityList = { };
//四个品质的羁绊数据
//接受服务器下发的地下城信息
FettersPart.RevcFettersData = function(buffer) {
	var msg = fetters_msg_pb.FettersPart_FettersData();

	FettersPart.FettersQualityList = { };
	FettersPart.FettersQualityList = msg.FettersQualityList;
	//FettersPart.FettersQualityList[0].FettersList  羁绊数组
	//FettersPart.FettersQualityList[0].FettersList[0].CardStateList   卡牌状态数组
	//FettersPart.FettersQualityList[0].FettersList[0].IsSet        该羁绊是否集齐
	//FettersPart.FettersQualityList[0].RewardList   奖励数组
	//FettersPart.FettersQualityList[0].Process      历史进度
}
//请求羁绊数据
FettersPart.FettersDataRequest = function() {
	var request = new  fetters_msg_pb.FettersPart_FettersDataRequest();
	ViewManager.showLoading({});
	SocketClient.callServerFun('FettersPart_RequestFettersData', request);
}
//请求回应
FettersPart.FettersDataResponse = function(buffer) {
	var msg = fetters_msg_pb.FettersPart_FettersDataResponse.decode(buffer);

	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		console.error(("错误ID" + msg.Flag));
		return;
	}
	//打开界面
	PokemonFettersPanel.OnRequestSuccess();
}
var requestIndex = 0;
var requestFettersID = 0;
var requestQuality = 0;
//请求镶嵌某张卡牌(参数1 索引 参数2 羁绊ID)
FettersPart.SetCardRequest = function(index, fettersID, quality) {
	var request = new  fetters_msg_pb.FettersPart_SetCardRequest();
	request.CardIndex = index;
	request.FettersID = fettersID;
	request.Quality = quality;
	requestIndex = index;
	requestFettersID = fettersID;
	requestQuality = quality;
	ViewManager.showLoading({});
	SocketClient.callServerFun('FettersPart_SetCardIntoFetters', request);
}
//镶嵌成功回应
FettersPart.SetCardResponse = function(buffer) {
	var msg = fetters_msg_pb.FettersPart_SetCardResponse.decode(buffer);

	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		console.error(("错误ID" + msg.Flag));
		return;
	}
	//刷新界面
	PokemonFettersPanel.OnSetSuccess(requestFettersID, requestQuality);
	PokemonFettersPanel.SetBottomText();
	PokemonFettersPanel.SetRightDetailInfo();
	//属性飘字
	PokemonFettersPanel.ShowHUDText(requestIndex, requestFettersID, requestQuality);
}
//请求卸下某张卡牌
FettersPart.UnloadCardRequest = function(index, fettersID, quality) {
	var request = new  fetters_msg_pb.FettersPart_UnloadCardRequest();
	request.CardIndex = index;
	request.FettersID = fettersID;
	request.Quality = quality;
	requestIndex = index;
	requestFettersID = fettersID;
	requestQuality = quality;
	ViewManager.showLoading({});
	SocketClient.callServerFun('FettersPart_RemoveCardFromFetters', request);
}
//卸下成功回应
FettersPart.UnloadCardResponse = function(buffer) {
	var msg = fetters_msg_pb.FettersPart_UnloadCardResponse.decode(buffer);

	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		console.error(("错误ID" + msg.Flag));
	}
	//刷新界面
	PokemonFettersPanel.OnSetSuccess(requestFettersID, requestQuality);
	PokemonFettersPanel.SetBottomText();
	PokemonFettersPanel.SetRightDetailInfo();
}
var boxRequestQuality = 0;
var boxRequestIndex = 0;
//请求领取宝箱
FettersPart.GetRewardRequest = function(quality, index) {
	var request = new  fetters_msg_pb.FettersPart_GetRewardRequest();
	request.Quality = quality;
	request.Index = index;
	boxRequestQuality = quality;
	boxRequestIndex = index;
	ViewManager.showLoading({});
	SocketClient.callServerFun('FettersPart_GetFettersReward', request);
}
//领取宝箱回应
FettersPart.GetRewardResponse = function(buffer) {
	var msg = fetters_msg_pb.FettersPart_GetRewardResponse.decode(buffer);

	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		console.error(("错误ID" + msg.Flag));
		return;
	}
	//修改宝箱状态
	FettersPart.FettersQualityList[boxRequestQuality].RewardList[boxRequestIndex] = 2;
	var rewardID = ((boxRequestQuality * 10000) + boxRequestIndex);
	var rewardInfo = LoadConfig.getConfigData(ConfigName.JingLingJiBan_JinDuJiangLi,rewardID);
	if ((rewardInfo == null)) {
		console.error(("进度奖励ID不存在" + rewardID));
		return;
	}
	var itemTypeArray = { };
	var itemIDArray = { };
	var itemCountArray = { };
	for (var i = 0; i != 4; ++i) {
		if ((rewardInfo.ResourceType[i] == 0)) {
			break;
		}
		itemTypeArray[i] = rewardInfo.ResourceType[i];
		itemIDArray[i] = rewardInfo.ResourceID[i];
		itemCountArray[i] = rewardInfo.ResourceCount[i];
	}
	//显示领取奖励
	HuoDeWuPinTiShiPanelData.Show(itemTypeArray, itemIDArray, itemCountArray);
	//刷新宝箱界面
	FettersRewardPopPanel.UpdateAfterGet();
}
//获取某个品质的开放羁绊总数
FettersPart.GetQualityFettersCount = function(quality) {
	var count = 0;
	count = FettersPart.FettersQualityList[quality].FettersList.length;
	return count;
}
//羁绊排序方法
FettersPart.SortFetters = function(a, b) {
	var aFetters = a;
	var bFetters = b;
	//可镶嵌的在前    已镶嵌的最后
	if ((aFetters.SortID != bFetters.SortID)) {
		return (aFetters.SortID < bFetters.SortID);
	}
	//ID小的在前
	return (aFetters.FettersID < bFetters.FettersID);
}
//奖励排序方法
FettersPart.SortRewards = function(a, b) {
	var aReward = a.BoxState;
	var bReward = b.BoxState;
	var aSortID = 0;
	if ((aReward == 0)) {
		aSortID = 2;
	} else if ((aReward == 1) ){
		aSortID = 1;
	} else if ((aReward == 2) ){
		aSortID = 3;
	}
	var bSortID = 0;
	if ((bReward == 0)) {
		bSortID = 2;
	} else if ((bReward == 1) ){
		bSortID = 1;
	} else if ((bReward == 2) ){
		bSortID = 3;
	}
	if ((aSortID != bSortID)) {
		return (aSortID < bSortID);
	}
	return (a.BoxIndex < b.BoxIndex);
}
//获取某个品质的羁绊列表(包括羁绊ID，羁绊集齐)
FettersPart.GetQualityFettersData = function(quality) {
	var count = FettersPart.GetQualityFettersCount(quality);
	var fettersList = { };
	if ((count <= 0)) {
		return fettersList;
	}
	for (var i = 0; i != count; ++i) {
		fettersList[i] = { };
		fettersList[i].FettersID = FettersPart.FettersQualityList[quality].FettersList[i].FettersID;
		fettersList[i].IsCollected = FettersPart.FettersQualityList[quality].FettersList[i].IsCollected;
		fettersList[i].SortID = 0;
		if (fettersList[i].IsCollected) {
			fettersList[i].SortID = 3;
		} else {
			var canSet = false;
			//拿到8个卡牌的镶嵌状态
			var fettersID = fettersList[i].FettersID;
			var cardSetStateList = FettersPart.SearchIsSet(quality, fettersID);
			var fettersData = LoadConfig.getConfigData(ConfigName.JingLingJiBan,fettersID);
			if ((fettersData == null)) {
				console.error(("羁绊ID不存在" + fettersID));
				return;
			}
			for (var i = 0; i != 8; ++i) {
				var cardID = fettersData.CardID[i];
				var isSet = cardSetStateList[i];
				if ((isSet != 1)) {
					if (((cardID != 0) && BackPackPart.IsCardExist(cardID))) {
						canSet = true;
						break;
					}
				}
			}
			//如果可镶嵌
			if (canSet) {
				fettersList[i].SortID = 1;
			} else {
				fettersList[i].SortID = 2;
			}
		}
	}
	//排序
	table.sort(fettersList, FettersPart.SortFetters);
	return fettersList;
}
//查询某个品质羁绊的羁绊卡牌镶嵌列表
FettersPart.SearchIsSet = function(quality, fettersID) {
	var tempList = { };
	//判断这个卡牌在羁绊中的索引
	var count = FettersPart.GetQualityFettersCount(quality);
	for (var i = 0; i != count; ++i) {
		if ((FettersPart.FettersQualityList[quality].FettersList[i].FettersID == fettersID)) {
			var cardStateList = FettersPart.FettersQualityList[quality].FettersList[i].CardStateList;
			for (var i = 0; i != cardStateList.length; ++i) {
				tempList[i] = cardStateList[i];
			}
		}
	}
	return tempList;
}
//获取某个品质羁绊的已镶嵌卡牌
FettersPart.GetHasSetCount = function(quality, fettersID) {
	var totalCount = 0;
	//判断这个卡牌在羁绊中的索引
	var count = FettersPart.GetQualityFettersCount(quality);
	for (var i = 0; i != count; ++i) {
		if ((FettersPart.FettersQualityList[quality].FettersList[i].FettersID == fettersID)) {
			var cardStateList = FettersPart.FettersQualityList[quality].FettersList[i].CardStateList;
			for (var i = 0; i != cardStateList.length; ++i) {
				if ((cardStateList[i] == 1)) {
					totalCount = (totalCount + 1);
				}
			}
			break;
		}
	}
	return totalCount;
}
//获取某个品质羁绊的集齐数量
FettersPart.GetHasCollectCount = function(quality) {
	var count = 0;
	for (var i = 0; i != FettersPart.FettersQualityList[quality].FettersList.length; ++i) {
		if (FettersPart.FettersQualityList[quality].FettersList[i].IsCollected) {
			count = (count + 1);
		}
	}
	return count;
}
//获取某个品质羁绊宝箱的状态列表
FettersPart.GetTreasureList = function(quality, tableCount) {
	var treasurelist = { };
	for (var i = 0; i != tableCount; ++i) {
		treasurelist[i] = { };
		treasurelist[i].BoxState = FettersPart.FettersQualityList[quality].RewardList[i];
		treasurelist[i].BoxIndex = i;
	}
	return treasurelist;
}
//属性排序规则
FettersPart.SortAttrs = function(a, b) {
	return (a.AttrID < b.AttrID);
}
//获取当前所有生效的羁绊属性ID
FettersPart.GetFettersAttrList = function() {
	var attrList = { };
	var attrBaseID = { };
	//遍历已开放的所有羁绊，如果有1张镶嵌的则添加进属性库ID列表,然后再将属性库里面的属性ID进行整合
	var index = 1;
	for (var i = 0; i != FettersPart.FettersQualityList.length; ++i) {
		for (let j = 0; j != FettersPart.FettersQualityList[i].FettersList.length; ++j) {
			var cardStateList = FettersPart.FettersQualityList[i].FettersList[j].CardStateList;
			for (var k = 1; k != cardStateList.length; ++k) {
				if ((cardStateList[k] == 1)) {
					//如果有一个激活，则记录这个羁绊ID
					//存下这个羁绊ID
					attrBaseID[index] = FettersPart.FettersQualityList[i].FettersList[j].FettersID;
					index = (index + 1);
					break;
				}
			}
		}
	}
	for (var i = 0; i != attrBaseID.length; ++i) {
		var conf_FettersData = LoadConfig.getConfigData(ConfigName.JingLingJiBan,attrBaseID[i]);
		if ((conf_FettersData == null)) {
			console.error(("羁绊ID不存在" + attrBaseID[i]));
			return attrList;
		}
		//去属性库找属性
		var shuxingkuID = conf_FettersData.AttrID;
		var conf_ShuXingKu = LoadConfig.getConfigData(ConfigName.ShuXingKu,shuxingkuID);
		if ((conf_ShuXingKu == null)) {
			console.error(("属性库ID不存在" + shuxingkuID));
			return attrList;
		}
		//判断当前的属性是否已经在属性列表
		for (let j = 0; j != conf_ShuXingKu.AttrID.length; ++j) {
			if ((conf_ShuXingKu.AttrID[j] == 0)) {
				break;
			}
			var isHave = false;
			for (var k = 1; k != attrList.length; ++k) {
				if ((attrList[k].AttrID == conf_ShuXingKu.AttrID[j])) {
					isHave = true;
					break;
				}
			}
			//不存在则加入
			if (!isHave) {
				var index = (attrList.length + 1);
				attrList[index] = { };
				attrList[index].AttrID = conf_ShuXingKu.AttrID[j];
				attrList[index].TargetID = conf_ShuXingKu.TargetID[j];
			}
		}
	}
	//排序属性，从小到大
	table.sort(attrList, FettersPart.SortAttrs);
	return attrList;
}
//获取羁绊折算的战斗等级
FettersPart.GetFightLevel = function() {
	var playerLevel = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_DengJi];
	var conf = LoadConfig.getConfigData(ConfigName.ZheSuan_JiBan,playerLevel);
	if ((conf == null)) {
		return 0;
	}
	return conf.Level;
}
window.FettersPart = FettersPart;