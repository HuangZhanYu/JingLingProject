
let backpack_msg_pb = require('backpack_msg').msg;
let card_msg_pb = require('card_msg').msg;
import CanShuTool from '../tool/CanShuTool';
import ViewManager from '../manager/ViewManager';

//region BackPackPart.lua
//Date
//此文件由[BabeLua]插件自动生成
let BackPackPart = { };

//使用效果类型
BackPackPart.UseEnum = {
	Resource: 1,
	//类型1：获得指定1种资源：参数1表示表ID，参数2表示资源ID，参数3表示数量。
	Resources: 2,
	//类型2：获得多种资源，参数1表示奖池ID
	PlateauCount: 3,
	//3 获得精通塔挑战次数
	FightTowerCount: 4,
	//4 获得战斗塔挑战次数
	DungeonCount: 7,
	//7，获得副本挑战次数。
	//8，获得全部道具奖励。
	RandomResources: 9,
	//9,随机获得多种资源

};
BackPackPart.RecycleType = {
	Decompos: 1,
	//分解
	Destroy: 2,
	//销毁
};
//玩家拥有的卡牌数组 每个对象是Card
BackPackPart.mCardList = [];
BackPackPart.mTreasureList = [];
BackPackPart.mItemList = [];
//时间商店数据(装备)
BackPackPart.mTimeShopEquipList = { };
//背包增加函数
BackPackPart.mBackPackAddFunc = { };
//背包减少函数
BackPackPart.mBackPackCutFunc = { };
//筛选后的装备列表
BackPackPart.mTreasureNotSame = { };
//登陆返回信息
BackPackPart.mIsLoginReturn = false;
//背包装备属性缓存库
BackPackPart.TreasureDataMap = { };
//使用道具前背包道具数量
BackPackPart.TotalItem = 0;
//进化前背包卡牌数量
BackPackPart.TotalCard = 0;
var evolutedID = -1;
//进化后的卡牌ID
// 注册背包增加卡牌 调用函数
// 参数1 string 函数名
// 参数2 函数     注册函数 func(objID) 背包的卡牌objid
BackPackPart.RegisterBackPackAddFunc = function(funcName, restoreFunc) {
	BackPackPart.mBackPackAddFunc[funcName] = restoreFunc;
}
// 注册背包减少卡牌 调用函数
// 参数1 string 函数名
// 参数2 函数     注册函数 func(objID) 背包的卡牌objid
BackPackPart.RegisterBackPackCutFunc = function(funcName, restoreFunc) {
	BackPackPart.mBackPackCutFunc[funcName] = restoreFunc;
}
var addKey = null;
//接收服务器所有卡牌信息
BackPackPart.RevcCardInfo = function(buffer) {
	var msg = backpack_msg_pb.BackPackPart_CardListResponse.decode(buffer);
	//如果组ID为0，则清空列表
	if ((msg.Group == 0)) {
		BackPackPart.mCardList = [];
	}
	var haveCard = false;
	//下发单张，需要判断是否存在
	if ((msg.CardList.length == 1)) {
		for (var i = 0; i != BackPackPart.mCardList.length; ++i) {
			if ((BackPackPart.mCardList[i].CardID == msg.CardList[0].CardID)) {
				BackPackPart.mCardList[i].Count = msg.CardList[0].Count;
				haveCard = true;
				break;
			}
		}
	}
	//没有这张卡则添加
	if (!haveCard) {
		var cardInfo = { };
		var cardCount = BackPackPart.mCardList.length;
		//添加卡牌信息到卡牌列表
		for (var i = 0; i != msg.CardList.length; ++i) {
			cardInfo = msg.CardList[i];
			var cardIndex = (i + cardCount);
			BackPackPart.mCardList[cardIndex] = msg.CardList[i];
		}
	}
	if ((msg.Group == 1)) {
		addKey = msg.CardList[0].CardID;
		BackPackPart.SendBackPackAdd();
	}
}
//用于回调战斗 告诉他哪张背包卡上阵了  也就是说背包减少一张  加入阵容
BackPackPart.SendBackPackAdd = function() {
	for (var key in BackPackPart.mBackPackAddFunc) {
		var value = BackPackPart.mBackPackAddFunc[key];
		{
			value(addKey);
		}
	}
}
//服务器更新卡牌数据
BackPackPart.OnServerUpdataCardInfo = function(buffer) {
	var msg = backpack_msg_pb.BackPackPart_UpdataCardInfo.decode(buffer);
	var haveCard = false;
	//查找客户端卡牌对象并且赋值
	for (var i = 0; i != BackPackPart.mCardList.length; ++i) {
		if ((BackPackPart.mCardList[i].CardID == msg.CardID)) {
			BackPackPart.mCardList[i] = msg;
			haveCard = true;
		}
	}
	//如果不存在卡牌则增加一个
	if ((haveCard == false)) {
		var cardIndex = (BackPackPart.mCardList.length + 1);
		BackPackPart.mCardList[cardIndex] = msg;
	}
	//BackPackPart.TransformToTimeShop()
}
//接受服务器遗物信息
BackPackPart.RevcTreasureInfo = function(buffer) {
	var msg = card_msg_pb.SendAllTreasureInfo.decode(buffer);

	//如果组ID为0，则清空列表
	if ((msg.Group == 0)) {
		BackPackPart.mTreasureList = [];
	}
	var cardCount = BackPackPart.mTreasureList.length;
	//添加卡牌信息到卡牌列表
	for (var i = 0; i != msg.TreasureInfoList.length; ++i) {
		var cardIndex = (i + cardCount);
		BackPackPart.mTreasureList[cardIndex] = msg.TreasureInfoList[i];
	}
	//如果是1 则是添加
	if ((msg.Group >= 1)) {
		for (var i = 0; i != msg.TreasureInfoList.length; ++i) {
			var temp = msg.TreasureInfoList[i];
			//更新属性库Map
			BackPackPart.TreasureDataMap[temp.ObjID] = { };
			BackPackPart.TreasureDataMap[temp.ObjID].AttrOneValue = TreasurePart.CalTreasureAttrValue(temp, 1);
			BackPackPart.TreasureDataMap[temp.ObjID].AttrTwoValue = TreasurePart.CalTreasureAttrValue(temp, 2);
		}
	}
	//未登录则设置为登录
	if (!BackPackPart.mIsLoginReturn) {
		BackPackPart.mIsLoginReturn = true;
		//初始化装备的属性
		BackPackPart.InitTreasureData();
	}
}
//初始化装备的属性加成
BackPackPart.InitTreasureData = function() {
	for (var i = 0; i != BackPackPart.mTreasureList.length; ++i) {
		var treasureInfo = BackPackPart.mTreasureList[i];
		BackPackPart.TreasureDataMap[treasureInfo.ObjID] = { };
		BackPackPart.TreasureDataMap[treasureInfo.ObjID].AttrOneValue = TreasurePart.CalTreasureAttrValue(treasureInfo, 1);
		BackPackPart.TreasureDataMap[treasureInfo.ObjID].AttrTwoValue = TreasurePart.CalTreasureAttrValue(treasureInfo, 2);
	}
}
//获取某个装备的属性库属性
BackPackPart.GetTreasureData = function(objID) {
	return BackPackPart.TreasureDataMap[objID];
}
var backpackKey = null;
//服务器发来删除卡牌
BackPackPart.OnServerDelCard = function(buffer) {
	var msg = backpack_msg_pb.BackPackPart_UpdataCardInfo.decode(buffer);
	backpackKey = msg.CardID;
	//查找客户端卡牌对象并且赋值
	for (var i = 0; i != BackPackPart.mCardList.length; ++i) {
		if ((BackPackPart.mCardList[i].CardID == msg.CardID)) {
			table.remove(BackPackPart.mCardList, i);
			break;
		}
	}
	BackPackPart.SendBackPackCut();
}
//用于回调战斗 告诉他哪张背包卡上阵了  也就是说背包减少一张  加入阵容
BackPackPart.SendBackPackCut = function() {
	for (var key in (BackPackPart.mBackPackCutFunc)) {
		var value = BackPackPart.mBackPackCutFunc[key];
		{
			value(backpackKey);
		}
	}
}
//服务器发来删除遗物
BackPackPart.OnServerDelTreasure = function(buffer) {
	var msg = card_msg_pb.UpdataTreasureInfo.decode(buffer);

	//查找客户端卡牌对象并且赋值
	for (var i = 0; i != BackPackPart.mTreasureList.length; ++i) {
		if ((BackPackPart.mTreasureList[i].ObjID == msg.Info.ObjID)) {
			table.remove(BackPackPart.mTreasureList, i);
			break;
		}
	}
}
//接受服务器道具信息
BackPackPart.RevcItemInfo = function(buffer) {
	var msg = card_msg_pb.SendAllItemInfo.decode(buffer);

	BackPackPart.mItemList = msg.ItemInfoList;
	//console.error("服务器下发道具数量"..msg.ItemInfoList)
	//console.error("msg.ItemInfoList[0].ResourceType"..msg.ItemInfoList[0].ResourceType)
	//console.error("msg.ItemInfoList[0].ResourceID"..msg.ItemInfoList[0].ResourceID)
	//console.error("msg.ItemInfoList[0].ResourceCount"..msg.ItemInfoList[0].ResourceCount)
}
//获取卡牌信息
BackPackPart.GetCard = function(index) {
	return BackPackPart.mCardList[index];
}
//获取卡牌信息
BackPackPart.GetCardByObjectId = function(cardObjectId) {
	for (var i = 0; i != BackPackPart.mCardList.length; ++i) {
		if ((BackPackPart.mCardList[i].ObjID == cardObjectId)) {
			return BackPackPart.mCardList[i];
		}
	}
	return null;
}
//获取当前总共闲置卡牌数量
BackPackPart.GetTotalCard = function() {
	var totalCount = 0;
	for (var i = 0; i != BackPackPart.mCardList.length; ++i) {
		totalCount = (totalCount + BackPackPart.mCardList[i].Count);
	}
	return totalCount;
}
//获取装备信息
BackPackPart.GetTreasureByObjectId = function(treasureObjectId) {
	for (var i = 0; i != BackPackPart.mTreasureList.length; ++i) {
		if ((BackPackPart.mTreasureList[i].ObjID == treasureObjectId)) {
			return BackPackPart.mTreasureList[i];
		}
	}
	return null;
}
//获取背包某张卡牌ID 对应的卡牌数量
BackPackPart.GetCardCount = function(cardID) {
	var count = 0;
	for (var i = 0; i != BackPackPart.mCardList.length; ++i) {
		if ((cardID == BackPackPart.mCardList[i].CardID)) {
			count = BackPackPart.mCardList[i].Count;
			break;
		}
	}
	return count;
}
//判断背包中是否存在某张卡牌
BackPackPart.IsCardExist = function(cardID) {
	for (var i = 0; i != BackPackPart.mCardList.length; ++i) {
		if ((cardID == BackPackPart.mCardList[i].CardID)) {
			return true;
		}
	}
	return false;
}
//判断背包中是否存在某张卡牌系列
BackPackPart.IsCardSeriesExist = function(cardID) {
	var origin = (Math.floor((cardID / 10)) * 10);
	//初始ID  个位为0
	for (var i = 0; i != BackPackPart.mCardList.length; ++i) {
		for (var j = 0; j != 7; ++j) {
			var id = (origin + j);
			if ((id == BackPackPart.mCardList[i].CardID)) {
				return true;
			}
		}
	}
	return false;
}
//将当前的mTreasureList转为时间商店的数据
BackPackPart.EquipTransformToTimeShop = function() {
	BackPackPart.mTimeShopEquipList = { };
	var index = 1;
	//主循环  CardList   副循环  遍历已有时间list  若有符合跳出
	for (var i = 0; i != BackPackPart.mTreasureList.length; ++i) {
		if ((i == 1)) {
			BackPackPart.mTimeShopEquipList[index] = { };
			BackPackPart.mTimeShopEquipList[index].Level = BackPackPart.mTreasureList[i].Level;
			BackPackPart.mTimeShopEquipList[index].BeyondLevel = BackPackPart.mTreasureList[i].BeyondLevel;
			BackPackPart.mTimeShopEquipList[index].ObjID = BackPackPart.mTreasureList[i].ObjID;
			BackPackPart.mTimeShopEquipList[index].TreasureID = BackPackPart.mTreasureList[i].TreasureID;
			BackPackPart.mTimeShopEquipList[index].Count = 1;
		} else if ((i > 1) ){
			var CheckIndex = 0;
			for (let j = 0; j != BackPackPart.mTimeShopEquipList.length; ++j) {
				if ((BackPackPart.mTreasureList[i].TreasureID == BackPackPart.mTimeShopEquipList[j].TreasureID)) {
					BackPackPart.mTimeShopEquipList[j].Count = (BackPackPart.mTimeShopEquipList[j].Count + 1);
					break;
				} else {
					CheckIndex = (CheckIndex + 1);
				}
			}
			//遍历若没有符合条件  则新增时间list项           
			if ((CheckIndex == BackPackPart.mTimeShopEquipList.length)) {
				index = (index + 1);
				BackPackPart.mTimeShopEquipList[index] = { };
				BackPackPart.mTimeShopEquipList[index].Level = BackPackPart.mTreasureList[i].Level;
				BackPackPart.mTimeShopEquipList[index].BeyondLevel = BackPackPart.mTreasureList[i].BeyondLevel;
				BackPackPart.mTimeShopEquipList[index].ObjID = BackPackPart.mTreasureList[i].ObjID;
				BackPackPart.mTimeShopEquipList[index].TreasureID = BackPackPart.mTreasureList[i].TreasureID;
				BackPackPart.mTimeShopEquipList[index].Count = 1;
			}
		}
	}
}
//获取背包某张遗物ID 对应的数量
BackPackPart.GetYiWuCount = function(treasureID) {
	var count = 0;
	for (var i = 0; i != BackPackPart.mTreasureList.length; ++i) {
		if ((((treasureID == BackPackPart.mTreasureList[i].TreasureID) && (BackPackPart.mTreasureList[i].Level == 0)) && (BackPackPart.mTreasureList[i].BeyondLevel == 0))) {
			count = (count + 1);
		}
	}
	return count;
}
//获取货币数据
BackPackPart.GetHuoBiCount = function(huoBiID) {
	if ((huoBiID == 1)) {
	}
}
//获取背包中某ID的道具数量
BackPackPart.GetItemCountByID = function(daojuID) {
	var count = 0;
	for (var i = 0; i != BackPackPart.mItemList.length; ++i) {
		if ((BackPackPart.mItemList[i].ResourceType == DaoJuTool.ResourceType.DaoJuBiao)) {
			if ((BackPackPart.mItemList[i].ResourceID == daojuID)) {
				count = BackPackPart.mItemList[i].ResourceCount;
				break;
			}
		}
	}
	return count;
}
//判断背包中某个资源是否足够,返回值是否足够，存在数量(type,id,count) (isenough,count)
BackPackPart.IsResourceEnough = function(resourceType, resourceID, resourceCount) {
	var count = 0;
	var isEnough = false;
	count = ResourceTool_GetResourceCount(resourceType, resourceID);
	if (count == (-1)) {
		console.error("物品数量异常");
		return isEnough;
	}
	var bigOne = BigNumber.create(count);
	var bigTwo = BigNumber.create(resourceCount);
	if (BigNumber.GreaterThanOrEqualTo(bigOne, bigTwo)) {
		isEnough = true;
	}
	return isEnough, count;
}
//卡牌排序规则
BackPackPart.CardBagCom = function(a, b) {
	var aCard = a;
	//CardPart.GetCard(a)
	var bCard = b;
	//CardPart.GetCard(b)
	//获取表格配置
	var aConf = LoadConfig.getConfigData(ConfigName.ShiBing,aCard.CardID);
	var bConf = LoadConfig.getConfigData(ConfigName.ShiBing,bCard.CardID);
	//根据星级
	if ((aConf.Star != bConf.Star)) {
		return (aConf.Star > bConf.Star);
	}
	//再更具品质排序
	if ((aConf.Quality != bConf.Quality)) {
		return (aConf.Quality > bConf.Quality);
	}
	//根据进化等级排序
	if ((aConf.EVO != bConf.EVO)) {
		return (aConf.EVO > bConf.EVO);
	}
	//最后根据ID排序
	return (aCard.CardID > bCard.CardID);
}
//时间商店 卡牌排序
BackPackPart.CardSort = function() {
	table.sort(BackPackPart.mCardList, BackPackPart.CardBagCom);
}
//通过索引拿到背包卡牌对象
BackPackPart.GetCardInfo = function(index) {
	var cardInfo = BackPackPart.mCardList[index];
	if ((cardInfo == null)) {
		return null;
	}
	return cardInfo;
}
//道具排序规则
BackPackPart.ItemBagCom = function(a, b) {
	var aItem = a;
	var bItem = b;
	//获取表格配置
	var aConf = LoadConfig.getConfigData(ConfigName.ZiYuan_DaoJu,aItem.ResourceID);
	if ((aConf == null)) {
		console.error(("道具ID不存在" + aItem.ResourceID));
		return false;
	}
	var bConf = LoadConfig.getConfigData(ConfigName.ZiYuan_DaoJu,bItem.ResourceID);
	if ((bConf == null)) {
		console.error(("道具ID不存在" + bItem.ResourceID));
		return false;
	}
	return (aConf.Sort < bConf.Sort);
}
//时间商店 道具排序
BackPackPart.ItemSort = function() {
	table.sort(BackPackPart.mItemList, BackPackPart.ItemBagCom);
}
//装备排序规则
BackPackPart.TreasureBagCom = function(a, b) {
	var aTreasure = a;
	var bTreasure = b;
	//获取表格配置
	var aConf = LoadConfig.getConfigData(ConfigName.Yiwu,aTreasure.TreasureID);
	var bConf = LoadConfig.getConfigData(ConfigName.Yiwu,bTreasure.TreasureID);
	//再更具品质排序
	if ((aConf.Quality != bConf.Quality)) {
		return (aConf.Quality > bConf.Quality);
	}
	//根据套装排序
	if ((aTreasure.TaoZhuangID != bTreasure.TaoZhuangID)) {
		return (aTreasure.TaoZhuangID > bTreasure.TaoZhuangID);
	}
	//根据进化等级排序
	if ((aTreasure.BeyondLevel != bTreasure.BeyondLevel)) {
		return (aTreasure.BeyondLevel > bTreasure.BeyondLevel);
	}
	//根据强化等级排序
	if ((aTreasure.Level != bTreasure.Level)) {
		return (aTreasure.Level > bTreasure.Level);
	}
	//最后根据ID排序
	return (aTreasure.TreasureID > bTreasure.TreasureID);
}
//装备分解排序规则
BackPackPart.RecycleCom = function(a, b) {
	// local aTreasure = a  --经过检查，这个函数只有一个地方调用过，所以在此做了更改
	// local bTreasure = b
	var aTreasure = a.Equip;
	var bTreasure = b.Equip;
	//获取表格配置
	var aConf = LoadConfig.getConfigData(ConfigName.Yiwu,aTreasure.TreasureID);
	var bConf = LoadConfig.getConfigData(ConfigName.Yiwu,bTreasure.TreasureID);
	//再更具品质排序
	if ((aConf.Quality != bConf.Quality)) {
		return (aConf.Quality < bConf.Quality);
	}
	//根据套装排序
	if ((aTreasure.TaoZhuangID != bTreasure.TaoZhuangID)) {
		return (aTreasure.TaoZhuangID > bTreasure.TaoZhuangID);
	}
	//根据进化等级排序
	if ((aTreasure.BeyondLevel != bTreasure.BeyondLevel)) {
		return (aTreasure.BeyondLevel < bTreasure.BeyondLevel);
	}
	//根据强化等级排序
	if ((aTreasure.Level != bTreasure.Level)) {
		return (aTreasure.Level < bTreasure.Level);
	}
	//最后根据ID排序
	return (aTreasure.TreasureID > bTreasure.TreasureID);
}
//时间商店 装备排序
BackPackPart.TreasureSort = function() {
	table.sort(BackPackPart.mTreasureList, BackPackPart.TreasureBagCom);
}
//获取背包中  装备里没有上过的 装备列表
BackPackPart.GetNotSameTreasure = function() {
	BackPackPart.mTreasureNotSame = { };
	var index = 1;
	for (var i = 0; i != BackPackPart.mTreasureList.length; ++i) {
		//遍历  如果阵容中有这个ID 则下一个
		var count = 0;
		for (let j = 0; j != TreasurePart.TreasureList.length; ++j) {
			if ((BackPackPart.mTreasureList[i].TreasureID != TreasurePart.TreasureList[j].TreasureID)) {
				count = (count + 1);
			}
		}
		if ((count == TreasurePart.TreasureList.length)) {
			BackPackPart.mTreasureNotSame[index] = BackPackPart.mTreasureList[i];
			index = (index + 1);
		}
	}
	//排序BackPackPart.mTreasureNotSame
	table.sort(BackPackPart.mTreasureNotSame, BackPackPart.TreasureBagCom);
}
//通过索引拿到背包装备对象
BackPackPart.GetEquipInfo = function(index) {
	var equipInfo = BackPackPart.mTreasureNotSame[index];
	if ((equipInfo == null)) {
		return null;
	}
	return equipInfo;
}
var requestID = null;
//进化背包中某张卡
BackPackPart.EvolutionCardRequest = function(cardID, evoType) {
	var request = new backpack_msg_pb.BackPackPart_EvolutionCardRequest();
	request.CardID = cardID;
	request.EvoType = evoType;
	requestID = cardID;
	BackPackPart.TotalCard = BackPackPart.mCardList.length;
	ViewManager.showLoading({});
	SocketClient.callServerFun("BackPackPart_EvolutionCard", request);
}
//进化卡牌回应
BackPackPart.EvolutionCardResponse = function(buffer) {
	var msg = backpack_msg_pb.BackPackPart_EvolutionCardResponse.decode(buffer);

	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		console.error(("进化失败,错误ID" + msg.Flag));
		return;
	}
	//设置进化后的卡牌ID
	BackPackPart.SetEvolutedCardID();
	//刷新界面  ObjID
	//DigimonDetailPopPanel.OnBagEvoSuccess(msg);
}
//设置进化后的卡牌ID
BackPackPart.SetEvolutedCardID = function() {
	var cardData = LoadConfig.getConfigData(ConfigName.ShiBing,requestID);
	if ((cardData != null)) {
		evolutedID = cardData.EvolutionID;
	}
}
//获取进化后的卡牌ID
BackPackPart.GetEvolutedCardID = function() {
	var result = evolutedID;
	//使用后重置 
	evolutedID = -1;
	return result;
}
var showdaojuID = 0;
var showdaojuCount = 0;
BackPackPart.ChangeTag = false;
//使用道具请求
//存下使用道具之前的背包数量
BackPackPart.UseItemRequest = function(daojuID, daoJuCount) {
	var request = new backpack_msg_pb.BackPackPart_UseItemRequest();
	request.DaoJuID = daojuID;
	request.Count = daoJuCount;
	showdaojuID = daojuID;
	showdaojuCount = daoJuCount;
	BackPackPart.TotalItem = BackPackPart.mItemList.length;
	if (((daojuID >= 530) && (daojuID <= 536))) {
		BackPackPart.ChangeTag = true;
	}
	ViewManager.showLoading({});
	SocketClient.callServerFun('BackPackPart_UseDaoJuFromBag', request);
}
var serverData = null;
BackPackPart.BeforeCatchAttr = null;
//使用回应
BackPackPart.UseItemResponse = function(buffer) {
	var msg = backpack_msg_pb.BackPackPart_UseItemResponse.decode(buffer);
	ViewManager.hideLoading();
	serverData = msg;
	if ((msg.Flag != 0)) {
		console.error(("使用道具错误，ID" + msg.Flag));
		return;
	}
	var daojuData = ResourceTool_GetResourceDetail(ResourceTypeEnum.ResType_DaoJu, showdaojuID);
	if ((daojuData.TableObj.EffectType == BackPackPart.UseEnum.Resource)) {
		HuoDeWuPinTiShiPanelData.Show([
				daojuData.TableObj.EffectParams[0]
			], [
				daojuData.TableObj.EffectParams[1]
			], [
				daojuData.TableObj.EffectParams[2]
			]);
		//刷新捉宠体力
		SearchPetPanel.UpdateEnergyText();
	} else if (((daojuData.TableObj.EffectType == BackPackPart.UseEnum.Resources) || (daojuData.TableObj.EffectType == BackPackPart.UseEnum.RandomResources)) ){
		var resourceType = { };
		var resourceID = { };
		var resourceCount = { };
		for (var i = 0; i != msg.RewardList.length; ++i) {
			resourceType[i] = msg.RewardList[i].RewardType;
			resourceID[i] = msg.RewardList[i].RewardID;
			resourceCount[i] = msg.RewardList[i].RewardCount;
		}
		if (((showdaojuID >= 530) && (showdaojuID <= 536))) {
			//等属性更新调用
			var cardID = msg.RewardList[0].RewardID;
			var currentPercent = PlayerAttrPart.GetCardCollectValue(cardID);
			var _;
			BackPackPart.BeforeCatchAttr = currentPercent;
			BackPackPart.OnPlayerAttrUpdate();
			//刷新背包道具
			BackPackPopPanel.UpdateCurPropInfo();
			return;
		}
		HuoDeWuPinTiShiPanelData.Show(resourceType, resourceID, resourceCount);
	} else if ((((daojuData.TableObj.EffectType == BackPackPart.UseEnum.DungeonCount) || (daojuData.TableObj.EffectType == BackPackPart.UseEnum.PlateauCount)) || (daojuData.TableObj.EffectType == BackPackPart.UseEnum.FightTowerCount)) ){
		var colorName = StringTool.AddTextColor(daojuData.Name, daojuData.Quality);
		ToolTipPopPanel.Show(317, [
				colorName,
				showdaojuCount
			]);
		//刷新精通塔界面的挑战次数和红点
		PlateauPanel.UpdateRedAndCount();
		//刷新战斗塔界面
		FightTowerPanel.RefreshAfeterUseCard();
		//刷新地下城次数界面
		DungeonDiffPopPanel.UpdataCiShuDXC();
	}
	//刷新背包道具
	BackPackPopPanel.UpdateCurPropInfo();
	//刷新弹窗道具
	BuyItemPopPanel.OnAfterLoad();
}
//参数传入2 为属性库调用
var serverTag = false;
BackPackPart.OnPlayerAttrUpdate = function(flag) {
	if ((flag == 2)) {
		serverTag = true;
	}
	//如果请求没返回，则return
	if ((serverData == null)) {
		return;
	}
	//如果玩家属性没更新，返回
	if (!serverTag) {
		return;
	}
	ToolTipPopPanel.ShowJingLingDan(serverData.RewardList[0].RewardID);
	serverTag = false;
	serverData = null;
}
var buyDaoJuID = 0;
var buyDaoJuCount = 0;
//购买道具请求
BackPackPart.BuyItemRequest = function(daojuID, count) {
	var request = new backpack_msg_pb.BackPackPart_BuyItemRequest();
	request.DaoJuID = daojuID;
	request.Count = count;
	buyDaoJuID = daojuID;
	buyDaoJuCount = count;
	ViewManager.showLoading({});
	SocketClient.callServerFun('BackPackPart_BuyDaoJu', request);
}
//购买道具回应
BackPackPart.BuyItemResponse = function(buffer) {
	var msg = backpack_msg_pb.BackPackPart_BuyItemResponse.decode(buffer);
	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		console.error(("购买道具错误，ID" + msg.Flag));
		return;
	}
	HuoDeWuPinTiShiPanelData.Show([
			ResourceTypeEnum.ResType_DaoJu
		], [
			buyDaoJuID
		], [
			buyDaoJuCount
		]);
	//刷新弹窗道具
	BuyItemPopPanel.OnAfterLoad();
}
var recycle = 0;
//分解装备请求
BackPackPart.DecomposEquipRequest = function(equipObjID, enumType) {
	var request = new backpack_msg_pb.BackPackPart_DecomposEquipRequest();
	request.ObjID = equipObjID;
	request.Type = enumType;
	recycle = enumType;
	ViewManager.showLoading({});
	SocketClient.callServerFun('BackPackPart_DecomposEquip', request);
}
//分解装备回应
BackPackPart.DecomposEquipResponse = function(buffer) {
	var msg = backpack_msg_pb.BackPackPart_DecomposEquipResponse.decod(buffer);

	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		console.error(("分解装备失败" + msg.Flag));
		return;
	}
	if ((recycle == BackPackPart.RecycleType.Decompos)) {
		//分解成功回调
		EquipRecyclePopPanel.OnDecomposSuccess();
	} else if ((recycle == BackPackPart.RecycleType.Destroy) ){
		//销毁成功回调
		EquipRecyclePopPanel.OnDestroySuccess();
	}
}
//重置装备请求
BackPackPart.ResetEquipRequest = function(equipObjID) {
	var request = new backpack_msg_pb.BackPackPart_DecomposEquipRequest();
	request.ObjID = equipObjID;
	request.Type = 1;
	//用同一个消息码，这个参数不用
	ViewManager.showLoading({});
	SocketClient.callServerFun('BackPackPart_ResetEquip', request);
};
//重置装备回应
BackPackPart.ResetEquipResponse = function(buffer) {
	var msg = backpack_msg_pb.BackPackPart_DecomposEquipResponse.decode(buffer);
	ViewManager.hideLoading();
	if (msg.Flag != 0) {
		console.error(("重置装备失败，ID" + msg.Flag));
		return;
	}
	//重置成功回调
	EquipRecyclePopPanel.OnResetSuccess();
};
//装备批量回收请求
BackPackPart.MultipleRecycleRequest = function(objArr) {
	var request = new backpack_msg_pb.BackPackPart_MultipleRecycleRequest();
	for (var i = 0; i != objArr.length; ++i) {
		request.ObjIDArr.push(objArr[i]);
	}
	SocketClient.callServerFun('BackPackPart_RecycleMultipleEquip', request);
};
//装备批量回收回应
BackPackPart.MultipleRecycleResponse = function(buffer) {
	var msg = backpack_msg_pb.BackPackPart_MultipleRecycleResponse.decode(buffer);
	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		console.error(("批量回收失败，错误ID" + msg.Flag));
		return;
	}
	var resourceType = { };
	var resourceID = { };
	var resourceCount = { };
	for (var i = 0; i != msg.RewardList.length; ++i) {
		resourceType[i] = msg.RewardList[i].RewardType;
		resourceID[i] = msg.RewardList[i].RewardID;
		resourceCount[i] = msg.RewardList[i].RewardCount;
	}
	//显示奖励
	HuoDeWuPinTiShiPanelData.Show(resourceType, resourceID, resourceCount);
	//刷新装备列表
	BackPackPopPanel.UpdateCurEquipInfo();
	RecycleEquipPopPanel.OnAfterLoad();
};
//装备批量销毁请求
BackPackPart.MultipleDestroyRequest = function(objArr) {
	var request = new backpack_msg_pb.BackPackPart_MultipleDestroyRequest();
	for (var i = 0; i != objArr.length; ++i) {
		request.ObjIDArr.push(objArr[i]);
	}
	SocketClient.callServerFun('BackPackPart_DestroyMultipleEquip', request);
};
//装备销毁回收回应
BackPackPart.MultipleDestroyResponse = function(buffer) {
	var msg = backpack_msg_pb.BackPackPart_MultipleDestroyResponse.decode(buffer);
	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		console.error(("批量回收失败，错误ID" + msg.Flag));
		return;
	}
	//提示信息 销毁成功
	//刷新
	BackPackPopPanel.UpdateCurEquipInfo();
	RecycleEquipPopPanel.OnAfterLoad();
};
//装备背包是否已满
BackPackPart.IsEquipBagFull = function() {
	var conf_Data = CanShuTool.GetCanShuData(CanShuTool.CanShuEnum.EquipBagMaxCount);
	var maxCount = 100;
	if ((conf_Data != null)) {
		maxCount = conf_Data.Param[0];
	}
	if ((BackPackPart.mTreasureList.length >= maxCount)) {
		return true;
	}
	return false;
};
BackPackPart.Compose = function(type, id) {
	var req = new backpack_msg_pb.BackPackPart_ComposeRequest();
	req.ItemType = type;
	req.ItemID = id;
	SocketClient.callServerFun("BackPackPart_Compose", req);
};
BackPackPart.ComposeResp = function(buffer) {
	var msg = backpack_msg_pb.BackPackPart_ComposeResponse.decode(buffer);

	if ((msg.Ret == 1)) {
		//无法合成
		return;
	} else if ((msg.Ret == 2) ){
		//材料不足
		return;
	} else if ((msg.Ret == 0) ){
		var resourceType = { };
		var resourceID = { };
		var resourceCount = { };
		resourceType[0] = msg.Rewards[0].RewardType;
		resourceID[0] = msg.Rewards[0].RewardID;
		resourceCount[0] = msg.Rewards[0].RewardCount;
		HuoDeWuPinTiShiPanelData.Show(resourceType, resourceID, resourceCount);
		//显示奖励
		BackPackPopPanel.UpdateCurPropInfo();
		//刷新背包道具
	}
};
BackPackPart.OptPackage = function(id) {
	var req = new backpack_msg_pb.BackPackPart_OptPackageRequest();
	req.ID = id;
	SocketClient.callServerFun("BackPackPart_OptPackage", req);
};
BackPackPart.OptPackageResp = function(buffer) {
	var msg = backpack_msg_pb.BackPackPart_OptPackageResponse.decode(buffer);

};
BackPackPart.OptPkgAward = function(id) {
	var req = new backpack_msg_pb.BackPackPart_OptPkgAwardRequest();
	req.ID = id;
	SocketClient.callServerFun("BackPackPart_OptPkgAward", req);
};
BackPackPart.OptPkgAwardResp = function(buffer) {
	var msg = backpack_msg_pb.BackPackPart_OptPkgAwardResponse.decode(buffer);
};

window.BackPackPart = BackPackPart;
