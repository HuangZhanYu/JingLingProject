//region *.lua
//Date
//此文件由[BabeLua]插件自动生成
//网页评论点赞活动part
let background_activity_msg_pb = require('background_activity_msg').msg;

let WebCommentActivityPart = { };

var WebCommentDataList = { };
//领奖结果
var GetWebCommentRewardResult = {
	GetWebCommentRewardResult_Success: 0,
	//成功
	GetWebCommentRewardResult_HadGet: 1,
	//已领取
	GetWebCommentRewardResult_NotExist: 2,
	//不存在

};
//收到服务器同步的活动信息
WebCommentActivityPart.OnRecvWebCommentData = function(buffer) {
	var msg = background_activity_msg_pb.SyncWebCommentDataList.decode(buffer);

	//网页点赞评论数据数组
	////网页评论点赞活动单个数据
	//message WebCommentData {
	//	required string ObjID        = 1;  //唯一ID
	//	required string ActivityName = 2;  //活动名称
	//	required string ActivityIcon = 3;  //活动图标
	//	required string Path         = 4;  //链接地址
	//	required string ActivityDescribe   = 5;  //描述
	//	repeated ActivityReward RewardList = 6; //奖励
	//}
	WebCommentDataList = msg.WebCommentDataList;
}
//是否为领取FB活动
WebCommentActivityPart.IsActivityNotGet = function(enumType) {
	if ((WebCommentDataList == null)) {
		return false;
	}
	if ((enumType == MainPanel.FBActType.Like)) {
		for (var i = 0; i != WebCommentDataList.length; ++i) {
			if ((WebCommentDataList[i].ActivityName == "FB点赞")) {
				return true;
			}
		}
	} else if ((enumType == MainPanel.FBActType.Comment) ){
		for (var i = 0; i != WebCommentDataList.length; ++i) {
			if ((WebCommentDataList[i].ActivityName == "FB评论")) {
				return true;
			}
		}
	}
	return false;
}
//拿到活动数据
WebCommentActivityPart.GetActData = function(enumType) {
	if ((WebCommentDataList == null)) {
		return null;
	}
	if ((enumType == MainPanel.FBActType.Like)) {
		for (var i = 0; i != WebCommentDataList.length; ++i) {
			if ((WebCommentDataList[i].ActivityName == "FB点赞")) {
				return WebCommentDataList[i];
			}
		}
	} else if ((enumType == MainPanel.FBActType.Comment) ){
		for (var i = 0; i != WebCommentDataList.length; ++i) {
			if ((WebCommentDataList[i].ActivityName == "FB评论")) {
				return WebCommentDataList[i];
			}
		}
	}
	return null;
}
//获取所有按钮
WebCommentActivityPart.GetAllActivityBtnInfo = function() {
	return WebCommentDataList;
}
//领取奖励
WebCommentActivityPart.OnGetRewardRequest = function(objId) {
	var request = new  background_activity_msg_pb.WebCommentRewardRequest();
	request.ObjID = objId;
	ViewManager.showLoading({});
	SocketClient.callServerFun("WebCommonActivity_GetRewardRequest", request);
}
//收到领取奖励回调
WebCommentActivityPart.OnGetRewardReponse = function(buffer) {
	var msg = background_activity_msg_pb.WebCommentRewardReponse.decode(buffer);

	ViewManager.hideLoading();
	UIManager.ClosePopPanel();
	if ((msg.MsgCode == GetWebCommentRewardResult.GetWebCommentRewardResult_Success)) {
		var resType = { };
		var resID = { };
		var resCount = { };
		for (var i = 0; i != msg.RewardList.length; ++i) {
			resType[(resType.length + 1)] = msg.RewardList[i].ResourceType;
			resID[(resID.length + 1)] = msg.RewardList[i].ResourceID;
			resCount[(resCount.length + 1)] = msg.RewardList[i].ResourceCount;
		}
		HuoDeWuPinTiShiPanelData.Show(resType, resID, resCount);
		//刷新按钮显隐
		MainPanel.SetFBButton();
	} else if ((msg.MsgCode == GetWebCommentRewardResult.GetWebCommentRewardResult_NotExist) ){
		//不存在
	} else if ((msg.MsgCode == GetWebCommentRewardResult.GetWebCommentRewardResult_HadGet) ){
		//已领取
	} else {
		console.error(("WebCommentActivityPart.OnGetRewardReponse不存在的消息码" + msg.MsgCode));
	}
}
//带SDK的需要显示分享的包名
var shareBundleID = [
	"com.mc.DigitalMon.bai"
];
//判断是否显示分享按钮
WebCommentActivityPart.IsShowShare = function() {
	var sdkType = LuaSDKManager.GetCurrentSdkType();
	var loginSDKData = LuaSDKManager.GetSDKResultData(sdkType, enSDKOperateType.enOperateType_AG_Login);
	if ((null != loginSDKData)) {
		var mPackageName = loginSDKData.packageName;
		for (var i = 0; i != shareBundleID.length; ++i) {
			if ((mPackageName == shareBundleID[i])) {
				return true;
			}
		}
	} else {
		return false;
	}
	return false;
}
//endregion
window.WebCommentActivityPart = WebCommentActivityPart;