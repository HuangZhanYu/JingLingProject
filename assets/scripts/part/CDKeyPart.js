//region *.lua
//Date
//此文件由[BabeLua]插件自动生成
let cdkey_msg_pb = require('cdkey_msg').msg;

let CDKeyPart = { };

CDKeyPart.Result = {
	Success: 0,
	//成功
	IDError: 1,
	//兑换码错误
	Already: 2,
	//已经被领取
	Limit: 3,
	//该类型兑换码领取超出数量上限
	TimeOver: 4,
	//时间过期

};
//使用兑换码
//skillID 技能ID
//index 卡槽位置
CDKeyPart.OnUse = function(cdKey) {
	var request = new  cdkey_msg_pb.CDKeyPart_UseCodeRequest();
	request.Code = cdKey;
	SocketClient.callServerFun('CDKeyPart_UseCode', request);
	ViewManager.showLoading({});
}
//使用兑换码返回
CDKeyPart.OnUseReturn = function(buffer) {
	ViewManager.hideLoading();
	var msg = cdkey_msg_pb.CDKeyPart_UseCodeResponse.decode(buffer);

	if ((msg.Flag == CDKeyPart.Result.IDError)) {
		ToolTipPopPanel.Show(142);
		return;
	} else if ((msg.Flag == CDKeyPart.Result.Already) ){
		ToolTipPopPanel.Show(143);
		return;
	} else if ((msg.Flag == CDKeyPart.Result.Limit) ){
		ToolTipPopPanel.Show(144);
		return;
	} else if ((msg.Flag == CDKeyPart.Result.TimeOver) ){
		ToolTipPopPanel.Show(145);
		return;
	}
	//message CDKeyPart_Reward{
	//	required int32 ResourceType = 1;
	//	required int32 ResourceID = 2;
	//	required string ResourceCount = 3;
	//}
	var rewardList = msg.RewardList;
	var resType = { };
	var resId = { };
	var resCount = { };
	for (var i = 0; i != rewardList.length; ++i) {
		resType[(resType.length + 1)] = rewardList[i].ResourceType;
		resId[(resId.length + 1)] = rewardList[i].ResourceID;
		resCount[(resCount.length + 1)] = rewardList[i].ResourceCount;
	}
	HuoDeWuPinTiShiPanelData.Show(resType, resId, resCount);
	//HuoDeJiangLiPanel.Show(resType,resId,null,resCount);
}
//endregion
window.CDKeyPart = CDKeyPart;