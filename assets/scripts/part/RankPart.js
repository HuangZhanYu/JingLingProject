//文件定义：排行榜部件
let rank_msg_pb = require('rank_msg').msg;
let RankPart = { };

//排行榜枚举定义
const enRankType = {
	PlayerRandType_WngZhang: 0,
	//总纹章
	PlayerRandType_QuanQia: 1,
	//关卡
	PlayerRandType_CatchPetOrange: 2,
	//橙色抓宠大师
	PlayerRandType_CatchPetPurple: 3,
	//紫色抓宠大师
	PlayerRandType_CatchPetBlue: 4,
	//蓝色抓宠大师
	PlayerRandType_CatchPetGreen: 5,
	//绿色抓宠大师
	PlayerRandType_Reset: 6,
	//重置次数
	PlayerRandType_FightTower: 7,
	//战斗塔
	PlayerRandType_BossDamage: 8,
	//boss伤害排行
	PlayerRandType_PersonalPoint: 9,
	//个人积分排行
	PlayerRandType_GuildPoint: 10,
	//公会积分排行
	PlayerRandType_PlateauStar: 11,
	//精通塔星星排行
	PlayerRandType_Zhanli: 12,
	//战力排行

};
//排行榜单个数据结构
/*
	RankPlayerData 
	{
		string Name 		--名字
		string Head 		--头像
		string RankValue	--具体排名数值
	}
*/
//请求排行榜数据
RankPart.GetRankData = function(rankType) {
	var request = new  rank_msg_pb.GetRankDataRequest();
	request.RankType = rankType;
	SocketClient.callServerFun("RankPart_GetRandDataRequest", request);
	ViewManager.showLoading({});
}
//接收服务器排行榜数据
RankPart.OnRecvRankData = function(buffer) {
	ViewManager.hideLoading();
	var msg = rank_msg_pb.RankData();

	//获取数据
	var rankType = msg.RankType;
	//当前类型
	var rankData = msg.Data;
	//当前单个数据
	var myRank = msg.MyRankIndex;
	//我的排行
	var myRankValue = msg.MyRankValue;
	//我的排名数值
	//如果是打开战斗塔排行榜就打开单独的那个排行界面
	if ((rankType == RankPanel.RankType.ZhanDouTai)) {
		CommonRankPopPanel.Show(rankData, rankType, myRank, myRankValue);
		return;
	}
	//刷新界面
	RankPanel.Show(rankData, rankType, myRank, myRankValue);
}
window.RankPart = RankPart;