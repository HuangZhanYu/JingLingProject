/*jshint esversion: 6 */
import { FightRaceType, FightAttr, FightSpecialAttr } from "../logic/fight/FightDef";
import BigNumber from "../tool/BigNumber";
let playerattr_msg_pb = require('playerattr_msg').msg;
//region PlayerAttrPart.lua
//Date 20170721
//增益信息部件，包括卡牌天赋，遗物 
let PlayerAttrPart = { };

//加成来源枚举
PlayerAttrPart.Source = {
	JingLing: 0,
	//精灵
	ZhuangBei: 1,
	//装备
	Backpack: 2,
	//背包
	Other: 3,
	//其他
	ChongWu: 4,
	//宠物
	Fetters: 5,
	//羁绊
	Guild: 6,
	//道馆
	GuildXunLianZhongXin: 7,
	//道馆训练中心
	TeamAttr: 8,
	//队伍属性
	MegaEvolution: 9,
	//百万进化
	Total: 10,
	//全部
	Max: 11,
	//最大

};
//-------------------------------------------------------------------
PlayerAttrPart.Addition_Master_Type = {
	Sneer: 0,
	//攻击自己
	BaseBattleTime: 1,
	//基础上阵时间
	BattleTimeReduce: 2,
	//上阵时间-
	TaskTimeReduce: 3,
	//任务时间-
	TaskGetAdd: 4,
	//任务金币+
	TaskOpenReduce: 5,
	//任务开启金币-
	TaskUpgrateReduce: 6,
	//任务升级金币-
	UpgrateCoseReduce: 7,
	//精灵升级金币-
	ReviveWenZhangAdd1: 8,
	//大师纹章+ 精灵进化和装备效果专用，上阵有效。对重置里程碑获得的大师纹章进行加成
	AmuletJinBiAdd: 9,
	//护符金币+
	DungeonRewardAdd1: 10,
	//副本化石+
	ReviveWenZhangAdd4: 11,
	//2011重置纹章+ 精灵进化追加的纹章加成属性绑定这个ID。所有精灵进化累加的重置纹章加成属性共用一个上限值
	ReviveWenZhangAdd5: 12,
	//2012重置纹章+ 精灵1收集技能
	ReviveWenZhangAdd6: 13,
	//2013重置纹章+ 精灵2收集技能
	ReviveWenZhangAdd7: 14,
	//2014重置纹章+ 精灵3收集技能
	ReviveWenZhangAdd8: 15,
	//2015重置纹章+ 精灵4收集技能
	ReviveWenZhangAdd9: 16,
	//2016重置纹章+ 预留
	ReviveWenZhangAdd10: 17,
	//2017重置纹章+ 预留
	DiaoLuoWenZhangAdd1: 18,
	//2020掉落纹章+ 羁绊专属
	DiaoLuoWenZhangAdd2: 19,
	//2021掉落纹章+ 装备专属
	DiaoLuoWenZhangAdd3: 20,
	//2022掉落纹章+ 精灵1收集技能
	DiaoLuoWenZhangAdd4: 21,
	//2023掉落纹章+ 精灵2收集技能
	DiaoLuoWenZhangAdd5: 22,
	//2024掉落纹章+ 精灵3收集技能
	DiaoLuoWenZhangAdd6: 23,
	//2025掉落纹章+ 精灵4收集技能
	DiaoLuoWenZhangAdd7: 24,
	//2026掉落纹章+ 预留
	DiaoLuoWenZhangAdd8: 25,
	//2027掉落纹章+ 预留
	LiHeWenZhangAdd1: 26,
	//2030礼盒纹章+ 公会技能
	LiHeWenZhangAdd2: 27,
	//2031礼盒纹章+ 精灵1收集技能
	LiHeWenZhangAdd3: 28,
	//2032礼盒纹章+ 精灵2收集技能
	LiHeWenZhangAdd4: 29,
	//2033礼盒纹章+ 精灵3收集技能
	LiHeWenZhangAdd5: 30,
	//2034礼盒纹章+ 精灵4收集技能
	LiHeWenZhangAdd6: 31,
	//2035礼盒纹章+ 预留
	LiHeWenZhangAdd7: 32,
	//2036礼盒纹章+ 预留
	DefaultSpeed: 33,
	//战斗热忱 队伍闯关基础速度+x
	SpeedAddition: 34,
	//冲锋号角 队伍闯关速度加成x%
	OffLineSpeedAdd1: 35,
	//偏执忠诚 训练师离线时，队伍闯关速度提升x%
	OffLineSpeedAdd2: 36,
	//忠诚温顺 训练师离线时，队伍闯关速度提升x%
	OffLineSpeedAdd3: 37,
	//耐心温和 训练师离线时，队伍闯关速度提升x%
	FreshSpeed: 38,
	//闪电速度 每10层有x%概率获得2分钟4倍游戏基础速度
	TeamSync: 39,
	//队伍同步 每10层有x%概率队伍基础属性提升50级，持续2分钟【背包效果减半】
	OffLineMaxLevel: 40,
	//噩梦掌控 训练师离线时，队伍闯关最高关卡上限+x%
	IQPassLevel: 41,
	//智商通关 通关时x%概率跳一关
	ReviveWenZhangAdd2: 42,
	//吃货天赋 精灵特殊收集技能。对重置里程碑获得的大师纹章进行加成
	ReviveWenZhangAdd3: 43,
	//生命吸取 精灵特殊收集技能。对重置里程碑获得的大师纹章进行加成
	LevelLimitAdd1: 44,
	//火焰光环 队伍等级上限+x【背包有效】
	LevelLimitAdd2: 45,
	//冰系光环 队伍等级上限+x【背包有效】
	LevelLimitAdd3: 46,
	//雷电光环 队伍等级上限+x【背包有效】
	BaseLevelAdd1: 47,
	//火焰掌控 队伍基础属性等级+x【背包有效】
	BaseLevelAdd2: 48,
	//水系掌控 队伍基础属性等级+x【背包有效】
	BaseLevelAdd3: 49,
	//雷电掌控 队伍基础属性等级+x【背包有效】
	ShareLife: 50,
	//生命分享 每10层有x%概率队伍基础属性提升100级，持续2分钟
	GreedDragon: 51,
	//龙之贪婪 已开启最高任务完成时有x%概率获得2~10倍金币
	BlingBling: 52,
	//金光闪闪 击败对手获得金币提升x%
	AutoBaoXiang: 53,
	//祈愿宝箱 关卡宝箱x%概率自动开启
	ReduceAbiltyCD: 54,
	//毁天灭地 主角技能冷却时间缩减
	DungeonRewardAdd2: 55,
	//矿石公主 副本矿石掉落数量提升x%【背包效果减半】
	DungeonTimeAdd: 56,
	//惊天伟力 副本挑战次数上限+x【背包有效】
	DungeonRateAdd: 57,
	//幸运之星 副本装备掉率提升x%
	ReviveLevel: 58,
	//时光倒流 队伍精灵阵亡时有x%概率立即复活，PVP时概率减半
	Guild1: 59,
	//公会预留1
	GoldenStreet: 60,
	//金光大道 每秒抛洒一次金币x(抛洒金币=当前每秒金币*x)，
	Guild2: 61,
	//公会预留3
	GuanQiaBaoXiang: 62,
	//2130关卡宝箱 每次重置，可自动开启x个关卡宝箱。【背包可生效】
	FlashSpeed: 63,
	//2131闪电速度 立即获得x分钟5倍速，每次重置后可再次获得。【背包效果减半】
	FantasticSpeed: 64,
	//梦幻速度 每10层有x%概率获得2分钟6倍游戏基础速度
	RagePassLevel: 65,
	//狂暴闯关 通关时x%概率跳二关
	TheCreator: 66,
	//创世之神 重置时，发动创世之力，直接激活额外x个【可叠加，背包可生效】里程碑奖励。
	LordShiva: 67,
	//毁灭之神 积蓄毁灭之力，在重置时释放，直接摧毁x个【可叠加，但是最多不可超过历史最高关卡，背包可生效】主线关卡。
	RespawnRate: 68,
	//队伍精灵阵亡时有x%概率立即复活，PVP时概率减半 
	MidasTouch: 69,
	//2132点石成金 副本材料数量+
	SeekingTreasures: 70,
	//2133探宝大师 副本材料数量+
	MiningExperts: 71,
	//2134挖矿专家 副本材料数量+
	Cheerleaders: 72,
	//2135啦啦啦队 队伍基础属性等级+
	TeamBond: 73,
	//2136队伍牵绊 队伍基础属性等级+
	UnityStrength: 74,
	//2137众志成城 队伍基础属性等级+
	MiniDiablo: 75,
	//2204小破坏神 重置后，可固定跳过一定的关卡
	RemoveExperts: 76,
	//2205拆迁专家 重置后，可固定跳过一定的关卡
	ElecTreat: 77,
	//2138电磁理疗 每次重置后，可立即获得x分钟3倍速。【背包效果减半】 
	QualityIntensify: 78,
	//2206素质强化 队伍金币强化等级上限+
	IntensifyWaveguide: 79,
	//2207强化波导 队伍金币强化等级上限+
	IronRampage: 80,
	//2208钢铁暴走 重置后，可百分比跳过一定的关卡
	WaterArea: 81,
	//2209水之流域 重置时，发动创世之力，直接激活额外x个【可叠加，背包可生效】里程碑奖励。
	BaseLevelAdd4: 82,
	//2210	基础属性等级	队伍属性等级+
	LevelLimitAdd4: 83,
	//2211	强化等级上限	队伍金币强化等级上限+
	ReviveWenZhangAdd11: 84,
	//2212	重置纹章加成	重置纹章加成
	DiaoLuoWenZhangAdd9: 85,
	//2213	掉落纹章加成	掉落纹章加成
	LiHeWenZhangAdd8: 86,
	//2214	礼盒纹章加成	礼盒纹章加成
	ChengFengPoLang: 87,
	//2215	乘风破浪	副本挑战次数上限+
	WanShiRuYi: 88,
	//2216	万事如意	提升金币等级上限+
	LevelLimitAdd5: 89,
	//2217	强化等级上限
	ReviveWenZhangAdd12: 90,
	//2218	重置纹章加成
	DiaoLuoWenZhangAdd10: 91,
	//2219	掉落纹章加成
	LiHeWenZhangAdd9: 92,
	//2220	礼盒纹章加成
	ChiHun: 93,
	//3000 炽魂等级
	ChiHun1: 94,
	//3001 炽魂等级
	ChiHun2: 95,
	//3002 炽魂等级
	ChiHun3: 96,
	//3003 炽魂等级
	ChiHun4: 97,
	//3004 炽魂等级
	ChiHun5: 98,
	//3005 炽魂等级
	ChiHun6: 99,
	//3006 炽魂等级
	ChiHun7: 100,
	//3007 炽魂等级
	ChiHun8: 101,
	//3008 炽魂等级
	ChiHun9: 102,
	//3009 炽魂等级
	ChiHun10: 103,
	//3010 炽魂等级
	ChiHun11: 104,
	//3011 炽魂等级
	ChiHun12: 105,
	//3012 炽魂等级
	LevelLimitAdd6: 106,
	//3200 强化等级上限
	LevelLimitAdd7: 107,
	//3201 强化等级上限
	LevelLimitAdd8: 108,
	//3202 强化等级上限
	LevelLimitAdd9: 109,
	//3203 强化等级上限
	LevelLimitAdd10: 110,
	//3204 强化等级上限
	LevelLimitAdd11: 111,
	//3205 强化等级上限
	LevelLimitAdd12: 112,
	//3206 强化等级上限
	LevelLimitAdd13: 113,
	//3207 强化等级上限
	LevelLimitAdd14: 114,
	//3208 强化等级上限
	LevelLimitAdd15: 115,
	//3209 强化等级上限
	LevelLimitAdd16: 116,
	//3210 强化等级上限
	LevelLimitAdd17: 117,
	//3211 强化等级上限
	ReviveSpeed: 118,
	//3300 重置闯关加速	每次重置后，可立即获得x分钟2倍速
	ReviveSpeed1: 119,
	//3301 重置闯关加速	每次重置后，可立即获得x分钟4倍速
	PassSpeed: 120,
	//3400 概率闯关加速	每10层有x%概率获得2分钟2倍速
	PassSpeed1: 121,
	//3401 概率闯关加速	每10层有x%概率获得2分钟3倍速
	PassSpeed2: 122,
	//3402 概率闯关加速	每10层有x%概率获得2分钟5倍速
	ReviveWenZhangAdd13: 123,
	//3500	重置纹章+
	ReviveWenZhangAdd14: 124,
	//3501	重置纹章+
	ReviveWenZhangAdd15: 125,
	//3502	重置纹章+
	ReviveWenZhangAdd16: 126,
	//3503	重置纹章+
	ReviveWenZhangAdd17: 127,
	//3504	重置纹章+
	ReviveWenZhangAdd18: 128,
	//3505	重置纹章+
	ReviveWenZhangAdd19: 129,
	//3506	重置纹章+
	ReviveWenZhangAdd20: 130,
	//3507	重置纹章+
	DiaoLuoWenZhangAdd11: 131,
	//3600 掉落纹章+
	DiaoLuoWenZhangAdd12: 132,
	//3601 掉落纹章+
	DiaoLuoWenZhangAdd13: 133,
	//3602 掉落纹章+
	DiaoLuoWenZhangAdd14: 134,
	//3603 掉落纹章+
	DiaoLuoWenZhangAdd15: 135,
	//3604 掉落纹章+
	DiaoLuoWenZhangAdd16: 136,
	//3605 掉落纹章+
	DiaoLuoWenZhangAdd17: 137,
	//3606 掉落纹章+
	DiaoLuoWenZhangAdd18: 138,
	//3607 掉落纹章+
	DiaoLuoWenZhangAdd19: 139,
	//3608 掉落纹章+
	DiaoLuoWenZhangAdd20: 140,
	//3609 掉落纹章+
	LiHeWenZhangAdd10: 141,
	//3700	礼盒纹章+
	LiHeWenZhangAdd11: 142,
	//3701	礼盒纹章+
	LiHeWenZhangAdd12: 143,
	//3702	礼盒纹章+
	LiHeWenZhangAdd13: 144,
	//3703	礼盒纹章+
	LiHeWenZhangAdd14: 145,
	//3704	礼盒纹章+
	LiHeWenZhangAdd15: 146,
	//3705	礼盒纹章+
	LiHeWenZhangAdd16: 147,
	//3706	礼盒纹章+
	LiHeWenZhangAdd17: 148,
	//3707	礼盒纹章+
	LiHeWenZhangAdd18: 149,
	//3708	礼盒纹章+
	LiHeWenZhangAdd19: 150,
	//3709	礼盒纹章+
	LiHeWenZhangAdd20: 151,
	//3710	礼盒纹章+
	DungeonFossilAdd: 152,
	//3800	副本化石+
	DungeonFossilAdd1: 153,
	//3801	副本化石+
	DungeonFossilAdd2: 154,
	//3802	副本化石+
	DungeonFossilAdd3: 155,
	//3803	副本化石+
	DungeonFossilAdd4: 156,
	//3804	副本化石+
	DungeonFossilAdd5: 157,
	//3805	副本化石+
	DungeonFossilAdd6: 158,
	//3806	副本化石+
	DungeonFossilAdd7: 159,
	//3807	副本化石+
	DungeonFossilAdd8: 160,
	//3808	副本化石+
	DungeonFossilAdd9: 161,
	//3809	副本化石+
	DungeonTimeAdd1: 162,
	//3900	副本挑战次数上限+
	DungeonTimeAdd2: 163,
	//3901	副本挑战次数上限+
	DungeonTimeAdd3: 164,
	//3902	副本挑战次数上限+
	OffLineSpeedAdd4: 165,
	//4000	离线闯关加速
	OffLineSpeedAdd5: 166,
	//4001	离线闯关加速
	OffLineMaxLevel1: 167,
	//4100	离线闯关上限	训练师离线时，队伍闯关最高关卡上限+x%
	OffLineMaxLevel2: 168,
	//4101	离线闯关上限	训练师离线时，队伍闯关最高关卡上限+x%
	OffLineMaxLevel3: 169,
	//4102	离线闯关上限	训练师离线时，队伍闯关最高关卡上限+x%
	OffLineMaxLevel4: 170,
	//4103	离线闯关上限	训练师离线时，队伍闯关最高关卡上限+x%
	ReviveLevel1: 171,
	//4200	重置跳关-百分比	重置后，从重置前层数n%处开始
	ReviveLevel2: 172,
	//4201	重置跳关-百分比	重置后，从重置前层数n%处开始
	ReviveLevel3: 173,
	//4202	重置跳关-百分比	重置后，从重置前层数n%处开始
	ReviveFixedLevel: 174,
	//4300	重置跳关-固定
	ReviveFixedLevel1: 175,
	//4301	重置跳关-固定
	ReviveFixedLevel2: 176,
	//4302	重置跳关-固定
	ReviveFixedLevel3: 177,
	//4303	重置跳关-固定
	ReviveFixedLevel4: 178,
	//4304	重置跳关-固定
	ReviveFixedLevel5: 179,
	//4305	重置跳关-固定
	ReviveFixedLevel6: 180,
	//4306	重置跳关-固定
	ReviveFixedLevel7: 181,
	//4307	重置跳关-固定
	ReviveFixedLevel8: 182,
	//4308	重置跳关-固定
	ReviveFixedLevel9: 183,
	//4309	重置跳关-固定
	ReviveFixedLevel10: 184,
	//4310	重置跳关-固定
	DungeonRateAdd1: 185,
	//4400	装备掉率	副本装备掉率提升x%
	DungeonRateAdd2: 186,
	//4401	装备掉率	副本装备掉率提升x%
	DungeonRateAdd3: 187,
	//4402	装备掉率	副本装备掉率提升x%
	DungeonRateAdd4: 188,
	//4403	装备掉率	副本装备掉率提升x%
	GuanQiaBaoXiang1: 189,
	//4500	关卡宝箱	每次重置，可自动开启x个关卡宝箱。
	GuanQiaBaoXiang2: 190,
	//4501	关卡宝箱	每次重置，可自动开启x个关卡宝箱。
	GuanQiaBaoXiang3: 191,
	//4502	关卡宝箱	每次重置，可自动开启x个关卡宝箱。
	ChiHunBUFF: 192,
	//3100	炽魂BUFF	每10层有x%概率队伍基础属性提升100级，持续2分钟
	ChiHunBUFF1: 193,
	//3101	炽魂BUFF	每10层有x%概率队伍基础属性提升150级，持续2分钟
	ChiHunBUFF2: 194,
	//3102	炽魂BUFF	每10层有x%概率队伍基础属性提升200级，持续2分钟
	ChiHunBUFF3: 195,
	//3103	炽魂BUFF	每10层有x%概率队伍基础属性提升250级，持续2分钟
	ChiHunBUFF4: 196,
	//3104	炽魂BUFF	每10层有x%概率队伍基础属性提升300级，持续2分钟
	ChiHunBUFF5: 197,
	//3105	炽魂BUFF	每10层有x%概率队伍基础属性提升350级，持续2分钟
	ChiHunBUFF6: 198,
	//3106	炽魂BUFF	每10层有x%概率队伍基础属性提升400级，持续2分钟
	ChiHunBUFF7: 199,
	//3107	炽魂BUFF	每10层有x%概率队伍基础属性提升450级，持续2分钟
	//新增
	ReviveWenZhangAdd21: 200,
	//3508	重置纹章+
	ReviveWenZhangAdd22: 201,
	//3509	重置纹章+
	ReviveWenZhangAdd23: 202,
	//3510	重置纹章+
	ReviveWenZhangAdd24: 203,
	//3511	重置纹章+
	ReviveWenZhangAdd25: 204,
	//3512	重置纹章+
	ReviveWenZhangAdd26: 205,
	//3513	重置纹章+
	ReviveWenZhangAdd27: 206,
	//3514	重置纹章+
	ReviveWenZhangAdd28: 207,
	//3515	重置纹章+
	ReviveWenZhangAdd29: 208,
	//3516	重置纹章+
	ReviveWenZhangAdd30: 209,
	//3517	重置纹章+
	ReviveWenZhangAdd31: 210,
	//3518	重置纹章+
	ReviveWenZhangAdd32: 211,
	//3519	重置纹章+
	ReviveWenZhangAdd33: 212,
	//3520	重置纹章+
	ReviveWenZhangAdd34: 213,
	//3521	重置纹章+
	ReviveWenZhangAdd35: 214,
	//3522	重置纹章+
	ReviveWenZhangAdd36: 215,
	//3523	重置纹章+
	ReviveWenZhangAdd37: 216,
	//3524	重置纹章+
	ReviveWenZhangAdd38: 217,
	//3525	重置纹章+
	ReviveWenZhangAdd39: 218,
	//3526	重置纹章+
	ReviveWenZhangAdd40: 219,
	//3527	重置纹章+
	ReviveWenZhangAdd41: 220,
	//3528	重置纹章+
	ReviveWenZhangAdd42: 221,
	//3529	重置纹章+
	ReviveWenZhangAdd43: 222,
	//3530	重置纹章+
	Max: 223,
	//最大

};
PlayerAttrPart.Addition_Summary_Type = {
	Sneer: 0,
	//攻击自己   **********暂时未接
	BaseBattleTime: 1,
	//基础上阵时间
	BattleTimeReduce: 2,
	//上阵时间-
	TaskTimeReduce: 3,
	//任务时间-
	TaskGetAdd: 4,
	//任务金币+
	TaskOpenReduce: 5,
	//任务开启金币-
	TaskUpgrateReduce: 6,
	//任务升级金币-
	UpgrateCoseReduce: 7,
	//精灵升级金币-
	ReviveWenZhangAdd: 8,
	//大师纹章+ 精灵进化和装备效果专用，上阵有效。对重置里程碑获得的大师纹章进行加成
	DungeonRewardAdd: 9,
	//副本化石+
	DefaultSpeed: 10,
	//队伍闯关基础速度+x **********暂时未接
	SpeedAddition: 11,
	//队伍闯关速度加成x% **********暂时未接
	OffLineSpeedAdd: 12,
	//训练师离线时，队伍闯关速度提升x%
	FreshSpeed: 13,
	//每10层有x%概率获得2分钟4倍游戏基础速度
	TeamSync: 14,
	//每10层有x%概率队伍基础属性提升50级，持续2分钟
	OffLineMaxLevel: 15,
	//训练师离线时，队伍闯关最高关卡上限+x%
	IQPassLevel: 16,
	//通关时x%概率跳一关
	LevelLimitAdd: 17,
	//队伍等级上限+x
	BaseLevelAdd: 18,
	//队伍基础属性等级+x
	ShareLife: 19,
	//每10层有x%概率队伍基础属性提升100级，持续2分钟
	GreedDragon: 20,
	//已开启最高任务完成时有x%概率获得2~10倍金币
	BlingBling: 21,
	//击败对手获得金币提升x%
	AutoBaoXiang: 22,
	//关卡宝箱x%概率自动开启 **********暂时未接
	ReduceAbiltyCD: 23,
	//主角技能冷却时间缩减
	DungeonTimeAdd: 24,
	//副本挑战次数上限+x
	DungeonRateAdd: 25,
	//副本装备掉率提升x%
	ReviveLevel: 26,
	//重置后，从重置前层数n%处开始
	RespawnRate: 27,
	//队伍精灵阵亡时有x%概率立即复活，PVP时概率减半  
	GoldenStreet: 28,
	//每秒抛洒一次金币x(抛洒金币=当前每秒金币*x)，	  **********暂时未接
	FantasticSpeed: 29,
	//每10层有x%概率获得2分钟6倍游戏基础速度
	RagePassLevel: 30,
	//通关时x%概率跳二关
	DiaoLuoWenZhang: 31,
	//掉落纹章+
	LiHeWenZhang: 32,
	//礼盒纹章+
	GuanQiaBaoXiang: 33,
	//关卡宝箱 每次重置，可自动开启x个关卡宝箱。【背包可生效】
	FlashSpeed: 34,
	//立即获得x分钟5倍速，每次重置后可再次获得。【背包效果减半】
	TheCreator: 35,
	//创世之神 重置时，发动创世之力，直接激活额外x个【可叠加，背包可生效】里程碑奖励。
	LordShiva: 36,
	//毁灭之神 积蓄毁灭之力，在重置时释放，直接摧毁x个【可叠加，但是最多不可超过历史最高关卡，背包可生效】主线关卡。
	DaoGuanFire: 37,
	//掠如火 队伍攻击 + 【x】% 在道馆中生效，其他系统中无效
	DaoGuanTree: 38,
	//徐如林 队伍生命 + 【x】% 在道馆中生效，其他系统中无效
	DaoGuanMount: 39,
	//不动如山 队伍防御 + 【x】% 在道馆中生效，其他系统中无效
	DaoGuanTiger: 40,
	//啸如虎 5秒恢复 【x】% 生命 在道馆中生效，其他系统中无效
	DaoGuanStab: 41,
	//断魂刺 BOSS易伤 【x】 秒 在道馆中生效，其他系统中无效
	ElecTreat: 42,
	//电磁理疗 每次重置后，可立即获得x分钟3倍速。【背包效果减半】
	AmuletJinBiAdd: 43,
	//护符金币增加
	ReviveSpeed: 44,
	//重置闯关加速	每次重置后，可立即获得x分钟2倍速
	ReviveSpeed1: 45,
	//重置闯关加速	每次重置后，可立即获得x分钟4倍速
	PassSpeed: 46,
	//概率闯关加速	每10层有x%概率获得2分钟2倍速
	PassSpeed1: 47,
	//概率闯关加速	每10层有x%概率获得2分钟3倍速
	PassSpeed2: 48,
	//概率闯关加速	每10层有x%概率获得2分钟5倍速
	ChiHunBUFF: 49,
	//炽魂BUFF	每10层有x%概率队伍基础属性提升100级，持续2分钟
	ChiHunBUFF1: 50,
	//炽魂BUFF	每10层有x%概率队伍基础属性提升150级，持续2分钟
	ChiHunBUFF2: 51,
	//炽魂BUFF	每10层有x%概率队伍基础属性提升200级，持续2分钟
	ChiHunBUFF3: 52,
	//炽魂BUFF	每10层有x%概率队伍基础属性提升250级，持续2分钟
	ChiHunBUFF4: 53,
	//炽魂BUFF	每10层有x%概率队伍基础属性提升300级，持续2分钟
	ChiHunBUFF5: 54,
	//炽魂BUFF	每10层有x%概率队伍基础属性提升350级，持续2分钟
	ChiHunBUFF6: 55,
	//炽魂BUFF	每10层有x%概率队伍基础属性提升400级，持续2分钟
	ChiHunBUFF7: 56,
	//炽魂BUFF	每10层有x%概率队伍基础属性提升450级，持续2分钟

};
//效果阵营类型
PlayerAttrPart.Addition_Kind = {
	Self: 0,
	//精灵自身
	Red: 1,
	//红 人类
	Green: 2,
	//绿 精灵
	Yellow: 3,
	//黄 不死
	Blue: 4,
	//蓝 怪兽
	Remote: 5,
	//远程
	Melee: 6,
	//近战
	MyTeam: 7,
	//友方
	EnemyTeam: 8,
	//敌方
	AirForce: 9,
	//空军
	LandForce: 10,
	//陆军-行走
	MyBoss: 11,
	//主线我方悬浮装置
	EnemyBoss: 12,
	//主线敌方悬浮装置
	DungeonNormal: 13,
	//副本小兵
	DungeonBoss: 14,
	//副本领主
	Master: 15,
	//主角
	Ice: 16,
	//冰
	Dragon: 17,
	//龙
	Fire: 18,
	//火
	Grass: 19,
	//草
	Water: 20,
	//水
	Soil: 21,
	//地面
	Electricity: 22,
	//电
	SuperPower: 23,
	//超能力
	Poison: 24,
	//毒
	Fairy: 25,
	//妖精
	Wrestle: 26,
	//格斗
	Steel: 27,
	//钢
	Rock: 28,
	//岩石
	Worm: 29,
	//虫
	Evil: 30,
	//恶
	Ghost: 31,
	//幽灵
	Ordinary: 32,
	//一般
	Float: 33,
	//陆军-悬浮
	GuildTeam: 34,
	//道馆我方队伍
	GuildBoss: 35,
	//道馆我方Boss
	GuildEnemyTeam: 36,
	//道馆敌方队伍
	GuildEnemyBoss: 37,
	//道馆敌方Boss
	Key: 38,
	//钥石
	Max: 39,
	//最大值

};
//--------------------------------战斗属性相关
PlayerAttrPart.Card_Fight_Type = {
	ATK: 1,
	//攻击固定值
	ATK_Add: 2,
	//攻击加成值
	ATK_Reduce: 3,
	//攻击减成值
	ATK_Coef: 4,
	//攻击系数
	ATK_Base: 5,
	//初始攻击加成
	HP: 6,
	//血量固定值
	HP_Add: 7,
	//血量加成值
	HP_Reduce: 8,
	//血量减成值
	HP_Coef: 9,
	//血量系数
	HP_Base: 10,
	//初始血量加成
	DEF: 11,
	//物防固定值
	DEF_Add: 12,
	//物防加成
	DEF_Reduce: 13,
	//物防减成
	DEF_Coef: 14,
	//物防系数
	DEF_Base: 15,
	//初始物防加成
	MDEF: 16,
	//法防固定值
	MDEF_Add: 17,
	//法防加成
	MDEF_Reduce: 18,
	//法防减成
	MDEF_Coef: 19,
	//法防系数
	MDEF_Base: 20,
	//初始法防加成
	ASPD: 21,
	//攻速固定值
	ASPD_Add: 22,
	//攻速加成
	ASPD_Reduce: 23,
	//攻速减成
	ASPD_Coef: 24,
	//攻速系数
	ASPD_Base: 25,
	//初始攻速加成
	Speed: 26,
	//移速固定值
	Speed_Add: 27,
	//移速加成
	Speed_Reduce: 28,
	//移速减成
	Speed_Coef: 29,
	//移速系数
	Speed_Base: 30,
	//初始移速加成
	Range: 31,
	//攻击范围固定值
	Range_Add: 32,
	//攻击范围加成
	Range_Reduce: 33,
	//攻击范围减成
	Range_Coef: 34,
	//攻击范围系数
	Range_Base: 35,
	//初始攻击范围加成
	CRI: 36,
	//暴率固定值
	CRI_Add: 37,
	//暴率加成
	CRI_Reduce: 38,
	//暴率减成
	CRI_Coef: 39,
	//暴率系数
	CRI_Base: 40,
	//初始暴率加成
	CritDamage: 41,
	//暴伤固定值
	CritDamage_Add: 42,
	//暴伤加成
	CritDamage_Reduce: 43,
	//暴伤减成
	CritDamage_Coef: 44,
	//暴伤系数
	CritDamage_Base: 45,
	//初始暴伤加成
	HitRate: 46,
	//命中率固定值
	HitRate_Add: 47,
	//命中率加成
	HitRate_Reduce: 48,
	//命中率减成
	HitRate_Coef: 49,
	//命中率系数
	HitRate_Base: 50,
	//初始命中率加成
	DodgeRate: 51,
	//闪避率固定值
	DodgeRate_Add: 52,
	//闪避率加成
	DodgeRate_Reduce: 53,
	//闪避率减成
	DodgeRate_Coef: 54,
	//闪避率系数
	DodgeRate_Base: 55,
	//初始闪避率加成
	Respawn: 56,
	//复活时间固定值
	Respawn_Add: 57,
	//复活时间加成
	Respawn_Reduce: 58,
	//复活时间减成
	Respawn_Coef: 59,
	//复活时间系数
	Respawn_Base: 60,
	//初始复活时间加成
	DoubleDef: 61,
	//双防固定值
	DoubleDef_Add: 62,
	//双防加成
	DoubleDef_Reduce: 63,
	//双防减成
	DoubleDef_Coef: 64,
	//双防系数
	DoubleDef_Base: 65,
	//初始双防加成
	ADH: 66,
	//攻防血固定值
	ADH_Add: 67,
	//攻防血加成
	ADH_Reduce: 68,
	//攻防血减成
	ADH_Coef: 69,
	//攻防血系数
	ADH_Base: 70,
	//初始攻防血加成
	//-------------------------------免疫，抗性
	ImmRepel: 71,
	//击退免疫
	ImmOff: 72,
	//击飞免疫
	ImmComa: 73,
	//昏迷免疫
	ImmFrozen: 74,
	//冰冻免疫
	ImmSlowDown: 75,
	//减速免疫
	ImmParalysis: 76,
	//麻痹免疫
	ImmOverawe: 77,
	//威慑免疫
	ImmTwine: 78,
	//缠绕免疫
	ImmConcussion: 79,
	//震荡免疫
	ImmBounce: 80,
	//反弹免疫
	ImmPetrifaction: 81,
	//石化免疫
	ImmVampire: 82,
	//吸血免疫
	ImmThreaten: 83,
	//恐吓免疫
	ImmGhostCurse: 84,
	//鬼咒免疫
	ImmDisturb: 85,
	//干扰免疫
	ImmPoison: 86,
	//中毒免疫
	ImmCharm: 87,
	//魅惑免疫
	ResisRepel: 88,
	//击退抗性
	ResisOff: 89,
	//击飞抗性
	ResisComa: 90,
	//昏迷抗性
	ResisFrozen: 91,
	//冰冻抗性
	ResisSlowDown: 92,
	//减速抗性
	ResisParalysis: 93,
	//麻痹抗性
	ResisOverawe: 94,
	//威慑抗性
	ResisTwine: 95,
	//缠绕抗性
	ResisConcussion: 96,
	//震荡抗性
	ResisBounce: 97,
	//反弹抗性
	ResisPetrifaction: 98,
	//石化抗性
	ResisVampire: 99,
	//吸血抗性
	ResisThreaten: 100,
	//恐吓抗性
	ResisGhostCurse: 101,
	//鬼咒抗性
	ResisDisturb: 102,
	//干扰抗性
	ResisPoison: 103,
	//中毒抗性
	ResisCharm: 104,
	//魅惑抗性
	//受属性攻击
	Ice_Add: 105,
	//受到冰属性精灵伤害增加
	Dragon_Add: 106,
	//受到龙属性精灵伤害增加
	Fire_Add: 107,
	//受到火属性精灵伤害增加
	Grass_Add: 108,
	//受到草属性精灵伤害增加
	Water_Add: 109,
	//受到水属性精灵伤害增加
	Soil_Add: 110,
	//受到地面属性精灵伤害增加
	Electricity_Add: 111,
	//受到电属性精灵伤害增加
	SuperPower_Add: 112,
	//受到超能力属性精灵伤害增加
	Poison_Add: 113,
	//受到毒属性精灵伤害增加
	Fairy_Add: 114,
	//受到妖精属性精灵伤害增加
	Wrestle_Add: 115,
	//受到格斗属性精灵伤害增加
	Steel_Add: 116,
	//受到钢属性精灵伤害增加
	Rock_Add: 117,
	//受到岩石属性精灵伤害增加
	Worm_Add: 118,
	//受到虫属性精灵伤害增加
	Evil_Add: 119,
	//受到恶属性精灵伤害增加
	Ghost_Add: 120,
	//受到幽灵属性精灵伤害增加
	Ordinary_Add: 121,
	//受到一般属性精灵伤害增加
	Ice_Reduce: 122,
	//受到冰属性精灵伤害减少
	Dragon_Reduce: 123,
	//受到龙属性精灵伤害减少
	Fire_Reduce: 124,
	//受到火属性精灵伤害减少
	Grass_Reduce: 125,
	//受到草属性精灵伤害减少
	Water_Reduce: 126,
	//受到水属性精灵伤害减少
	Soil_Reduce: 127,
	//受到地面属性精灵伤害减少
	Electricity_Reduce: 128,
	//受到电属性精灵伤害减少
	SuperPower_Reduce: 129,
	//受到超能力属性精灵伤害减少
	Poison_Reduce: 130,
	//受到毒属性精灵伤害减少
	Fairy_Reduce: 131,
	//受到妖精属性精灵伤害减少
	Wrestle_Reduce: 132,
	//受到格斗属性精灵伤害减少
	Steel_Reduce: 133,
	//受到钢属性精灵伤害减少
	Rock_Reduce: 134,
	//受到岩石属性精灵伤害减少
	Worm_Reduce: 135,
	//受到虫属性精灵伤害减少
	Evil_Reduce: 136,
	//受到恶属性精灵伤害减少
	Ghost_Reduce: 137,
	//受到幽灵属性精灵伤害减少
	Ordinary_Reduce: 138,
	//受到一般属性精灵伤害减少
	//特殊属性
	RestoreHP: 139,
	//5秒恢复 【x】% 生命
	EasyHurt: 140,
	//BOSS易伤 【x】 秒
	Max: 200
};
PlayerAttrPart.AddAttrType = {
	AddGuanQiaLevel: 0,
	// 增加关卡等级
	AddSpeed: 1,
	// 增加速度
	AddJingLingLevel: 2,
	// 增加精灵等级
	TaskAddition: 3,
	// 触发任务有x%概率获得2~50倍金币

};
PlayerAttrPart.PlayerAttrs = [];
PlayerAttrPart.StoneBaseAttrs = {};
//这里只记录钥石的精灵的属性，如果是阵营属性在PlayerAttrPart.PlayerAttrs里
//登陆下发
PlayerAttrPart.OnLogin = function(buffer) {
	let msg = playerattr_msg_pb.PlayerAttr_All.decode(buffer);

	for (let i = 0; i < msg.Updates.length; ++i) {
		
		let item = msg.Updates[i];
		
		let data = { };
		data.FightAttr = [];
		data.MasterAttr = [];
		for (let n = 1; n != PlayerAttrPart.Addition_Master_Type.Max; ++n) {
			data.MasterAttr.push("0");
		}
		for (let aitem of item.MasterAttr) {
			data.MasterAttr[aitem.Index] = BigNumber.create(aitem.Value);
		}
		for (let n = 0; n < item.FightAttr.length; ++n) {
			let attritem = item.FightAttr[n];
			
			let values = [];
			for (let m = 0; m != PlayerAttrPart.Card_Fight_Type.Max; ++m) {
				values.push("0");
			}
			for (let aitem of attritem.Values) {
				values[aitem.Index] = BigNumber.create(aitem.Value);
			}
			data.FightAttr[n] = {};
			data.FightAttr[n].Values = values;
			
		}
		PlayerAttrPart.PlayerAttrs[i] = data;
		
	}
	GuanQiaPart.SetPlayerAttrInitFlag(true);
	//重新计算离线最高关卡和离线行军速度
	GuanQiaPart.CalOffLineSpeed();
	GuanQiaPart.CalMaxOffLineLevel();
	GuanQiaPart.Init();
}

//更新钥石信息
PlayerAttrPart.UpdateStoneData = function(buffer) {
	let msg = playerattr_msg_pb.PlayerAttr_StoneUpdate.decode(buffer);

	for (let forinlet of msg.Datas) {
		
		let item = forinlet.item;
		{
			let attr = { };
			let values = [];
			for (let n = 0; n != PlayerAttrPart.Card_Fight_Type.Max; ++n) {
				values.push("0");
			}
			for (let forinlet of item.Attrs) {
				
				let sitem = forinlet.sitem;
				{
					values[sitem.Index] = BigNumber.create(sitem.Value);
				}
			}
			attr.Values = values;
			PlayerAttrPart.StoneBaseAttrs[item.StoneID] = attr;
		}
	}
}
PlayerAttrPart.OnLogout = function() {
	PlayerAttrPart.PlayerAttrs = { };
}
//更新下发
PlayerAttrPart.OnUpdate = function(buffer) {
	let msg = playerattr_msg_pb.PlayerAttr_Update.decode(buffer);

	let total = false;
	let isGuild = false;
	for (let i = 0; i < msg.Datas.length; ++i) {
		let item = msg.Datas[i];
		{
			let data = { };
			data.FightAttr = [];
			data.MasterAttr = [];
			for (let n = 0; n != PlayerAttrPart.Addition_Master_Type.Max; ++n) {
				data.MasterAttr.push("0");
			}
			for (let aitem of item.MasterAttr) {
				data.MasterAttr[aitem.Index] = BigNumber.create(aitem.Value);
			}
			for (let n = 0; n < item.FightAttr.length; ++n) {
				let attritem = item.FightAttr[n];
				{
					let values =[];
					for (let m = 0; m != PlayerAttrPart.Card_Fight_Type.Max; ++m) {
						values.push("0");
					}
					for (let aitem of attritem.Values) {
						values[aitem.Index] = BigNumber.create(aitem.Value);
					}
					data.FightAttr[n] = { };
					data.FightAttr[n].Values = values;
				}
			}
			let index = (msg.Indexs[i] - 1);
			if ((index == PlayerAttrPart.Source.Total)) {
				total = true;
			} else if ((index == PlayerAttrPart.Source.GuildXunLianZhongXin) ){
				isGuild = true;
			}
			PlayerAttrPart.PlayerAttrs[index] = data;
		}
	}
	//刷新阵容天赋属性
	LineupPart.UpdateCardPro();
	//重新计算离线最高关卡和离线行军速度
	GuanQiaPart.CalOffLineSpeed();
	GuanQiaPart.CalMaxOffLineLevel();
	//训练大师相关需要回调的
	if (LineupPart.ChangeTag) {
		if (total) {
			//LineupPart.HUDTextTrainMaster()  --新需求：训练大师获得成就不飘字了
			LineupPart.ChangeTag = false;
		}
	}
	//使用精灵蛋回调需要最新的属性
	if (BackPackPart.ChangeTag) {
		if (total) {
			BackPackPart.OnPlayerAttrUpdate(2);
			BackPackPart.ChangeTag = false;
		}
	}
	//一键捉宠
	if (CatchPetPart.ChangeTag) {
		if (total) {
			CatchPetPart.OneKeyShowResult();
			CatchPetPart.ChangeTag = false;
		}
	}
	//普通捉宠
	if (CatchPetPart.CatchResultTag) {
		if (total) {
			CatchPetPart.ShowResult();
			CatchPetPart.CatchResultTag = false;
		}
	}
	//刷新道馆训练中心属性
	if (isGuild) {
		//DaoGuanZengYiPopPanel.UpdataZengYiArr();
	}
}
//触发主角信息改变弹出提示
PlayerAttrPart.OnSendAddAttrResponse = function(buffer) {
	let msg = playerattr_msg_pb.PlayerAttr_SendAddAttrTips();

	let typ = msg.Type;
	let params = msg.Params;
	if ((typ == PlayerAttrPart.AddAttrType.AddGuanQiaLevel)) {
		let str = TranslateTool.GetText(200372);
		str = StringTool.ZhuanHuan(str, [
				params[0]
			]);
		CommonHUDPanel.Show(str);
	} else if ((typ == PlayerAttrPart.AddAttrType.AddSpeed) ){
		let str = TranslateTool.GetText(200373);
		str = StringTool.ZhuanHuan(str, [
				params[0],
				params[1]
			]);
		CommonHUDPanel.Show(str);
	} else if ((typ == PlayerAttrPart.AddAttrType.AddJingLingLevel) ){
		let str = TranslateTool.GetText(200374);
		str = StringTool.ZhuanHuan(str, [
				params[0],
				params[1]
			]);
		CommonHUDPanel.Show(str);
	} else if ((typ == PlayerAttrPart.AddAttrType.TaskAddition) ){
		let str = TranslateTool.GetText(200375);
		str = StringTool.ZhuanHuan(str, [
				params[0]
			]);
		CommonHUDPanel.Show(str);
	}
}
//获取精灵上阵和背包汇总信息
//参数1 source PlayerAttrPart.Source
//typ Addition_Summary_Type
PlayerAttrPart.GetMasterLineupAndBackpackAttr = function(typ) {
	let jingling = PlayerAttrPart.GetClassifiedSummaryAttr(PlayerAttrPart.Source.JingLing, typ);
	let backpack = PlayerAttrPart.GetClassifiedSummaryAttr(PlayerAttrPart.Source.Backpack, typ);
	return BigNumber.add(jingling, backpack);
}
//获取一个主角加成信息
//参数1 source PlayerAttrPart.Source
//参数2 typ PlayerAttrPart.Addition_Master_Type
PlayerAttrPart.GetMasterAttr = function(source, typ) {
	//Lua的下标是从1开始的，所以要+1
	let index = (typ + 1);
	let data = PlayerAttrPart.PlayerAttrs[source];
	if ((data == null)) {
		//console.error("PlayerAttrPart.GetMasterAttr, data == null,source then"..source.."|typ then"..typ)
		return "0";
	}
	if ((data.MasterAttr[index] == null)) {
		//console.error("PlayerAttrPart.GetMasterAttr, data.MasterAttr[typ] == null,source then"..source.."|typ then"..typ)
		return "0";
	}
	return data.MasterAttr[index];
}
//获取主角信息
//当前超过最大值时，返回最大值
//typ Addition_Summary_Type
PlayerAttrPart.GetSummaryAttr = function(typ) {
	let conf_HuiZong_Data = LoadConfig.getConfigData(ConfigName.ShuZhiShuXingDingYi_HuiZong,typ);
	let result = BigNumber.zero;
	if ((conf_HuiZong_Data == null)) {
		return result;
	}
	for (let i = 1; i != conf_HuiZong_Data.AttrID.length; ++i) {
		let effectID = conf_HuiZong_Data.AttrID[i];
		if (effectID <= 0) {
			break;
		}
		let enumType = PlayerAttrPart.ChangeEffectIDToMasterType(effectID);
		if ((enumType != PlayerAttrPart.Addition_Master_Type.Max)) {
			let value = PlayerAttrPart.GetSingleSummaryAttr(enumType);
			result = BigNumber.add(result, value);
		}
	}
	return result;
}
//获取一个属性的加成值，如果有最大上限，判断是否超过再返回
//typ Addition_Master_Type
PlayerAttrPart.GetSingleSummaryAttr = function(typ) {
	let value = PlayerAttrPart.GetMasterAttr(PlayerAttrPart.Source.Total, typ);
	let effectID = PlayerAttrPart.ChangeMasterType(typ);
	let conf_Data = LoadConfig.getConfigData(ConfigName.ShuZhiShuXingDingYi,effectID);
	if ((conf_Data == null)) {
		return value;
	}
	if ((conf_Data.Max == '')) {
		conf_Data.Max = '0';
	}
	let max = BigNumber.create(conf_Data.Max);
	if (BigNumber.lessThanOrEqualTo(max, BigNumber.zero)) {
		return value;
	}
	if (BigNumber.lessThanOrEqualTo(max, value)) {
		return max;
	}
	return value;
}
//获取一个属性的加成值，如果有最大上限，判断是否超过再返回(精灵收集专用)
//typ Addition_Master_Type
PlayerAttrPart.GetCollectSingleSummaryAttr = function(typ) {
	let value = PlayerAttrPart.GetMasterAttr(PlayerAttrPart.Source.JingLing, typ);
	let sec = PlayerAttrPart.GetMasterAttr(PlayerAttrPart.Source.Backpack, typ);
	value = BigNumber.add(value, sec);
	let effectID = PlayerAttrPart.ChangeMasterType(typ);
	let conf_Data = LoadConfig.getConfigData(ConfigName.ShuZhiShuXingDingYi,effectID);
	if ((conf_Data == null)) {
		return value;
	}
	if ((conf_Data.Max == '')) {
		conf_Data.Max = '0';
	}
	let max = BigNumber.create(conf_Data.Max);
	if (BigNumber.lessThanOrEqualTo(max, BigNumber.zero)) {
		return value;
	}
	if (BigNumber.lessThanOrEqualTo(max, value)) {
		return max;
	}
	return value;
}
//获取主角信息
//当前超过最大值时，返回最大值
//source PlayerAttrPart.Source
//typ Addition_Summary_Type
PlayerAttrPart.GetClassifiedSummaryAttr = function(source, typ) {
	let conf_HuiZong_Data = LoadConfig.getConfigData(ConfigName.ShuZhiShuXingDingYi_HuiZong,typ);
	let result = BigNumber.zero;
	if ((conf_HuiZong_Data == null)) {
		return result;
	}
	for (let i = 1; i != conf_HuiZong_Data.AttrID.length; ++i) {
		let effectID = conf_HuiZong_Data.AttrID[i];
		if ((effectID <= 0)) {
			break;
		}
		let enumType = PlayerAttrPart.ChangeEffectIDToMasterType(effectID);
		if ((enumType != PlayerAttrPart.Addition_Master_Type.Max)) {
			let value = PlayerAttrPart.GetMasterAttrInMax(source, enumType);
			result = BigNumber.add(result, value);
		}
	}
	return result;
}
//获取精灵收集的加成（特殊除掉2011，加上阵容和背包,读的是精灵收集的表）
PlayerAttrPart.GetCollectSummaryAttr = function(source, typ) {
	//单独获取重置纹章加成
	let conf_HuiZong_Data = LoadConfig.getConfigData(ConfigName.ShouJiShuXing_HuiZong,typ);
	let result = BigNumber.zero;
	if ((conf_HuiZong_Data == null)) {
		return result;
	}
	for (let i = 1; i != conf_HuiZong_Data.AttrID.length; ++i) {
		let effectID = conf_HuiZong_Data.AttrID[i];
		if ((effectID <= 0)) {
			break;
		}
		let enumType = PlayerAttrPart.ChangeEffectIDToMasterType(effectID);
		if ((enumType != PlayerAttrPart.Addition_Master_Type.Max)) {
			let value = PlayerAttrPart.GetMasterAttrInMax(source, enumType);
			result = BigNumber.add(result, value);
		}
	}
	return result;
}
//获取某一个途径的主角属性信息
PlayerAttrPart.GetMasterAttrInMax = function(source, typ) {
	let result = PlayerAttrPart.GetMasterAttr(source, typ);
	let id = PlayerAttrPart.ChangeMasterType(typ);
	let conf_Data = LoadConfig.getConfigData(ConfigName.ShuZhiShuXingDingYi,id);
	if ((conf_Data == null)) {
		return result;
	}
	if ((conf_Data.Max == '')) {
		conf_Data.Max = '0';
	}
	let max = BigNumber.create(conf_Data.Max);
	if (BigNumber.lessThanOrEqualTo(max, BigNumber.zero)) {
		return result;
	}
	if (BigNumber.greaterThan(result, conf_Data.Max)) {
		return conf_Data.Max;
	}
	return result;
}
//获取一个阵营的加成信息
//参数1 source PlayerAttrPart.Source
//参数2 kind PlayerAttrPart.Addition_Kind
PlayerAttrPart.GetOtherAttr = function(source, kind) {
	let data = PlayerAttrPart.PlayerAttrs[source];
	let index = (kind + 1);
	if ((data == null)) {
		console.error(((("PlayerAttrPart.GetOtherAttr, data == null,source then" + source) + "|kind then") + kind));
		return null;
	}
	return data.FightAttr[index];
}
//获取一个阵营的加成信息
//参数1 source PlayerAttrPart.Source
//参数2 kind PlayerAttrPart.Addition_Kind
//参数3 typ PlayerAttrPart.Card_Fight_Type
PlayerAttrPart.GetOtherFightAttr = function(source, kind, typ) {
	let data = PlayerAttrPart.PlayerAttrs[source];
	let index = (kind + 1);
	if ((data == null)) {
		console.error(((((("PlayerAttrPart.GetOtherAttr, data == null,source then" + source) + "|kind then") + kind) + "|typ then") + typ));
		return "0";
	}
	if ((data.FightAttr[index] == null)) {
		console.error(((((("PlayerAttrPart.GetOtherAttr, data.FightAttr[typ] == null,source then" + source) + "|index then") + index) + "|typ then") + typ));
		return "0";
	}
	return data.FightAttr[index].Values[typ];
}
//获取一个阵营的加成信息
//参数1 source PlayerAttrPart.Source
//参数2 kind PlayerAttrPart.Addition_Kind
//参数3 fightAttr FightAttr
PlayerAttrPart.GetOtherFightAttrWithTeam = function(source, kind, fightAttr) {
	let data = PlayerAttrPart.PlayerAttrs[source];
	let index = (kind + 1);
	if ((data == null)) {
		console.error((((("PlayerAttrPart.GetOtherAttr, data == null,source then" + source) + "|kind then") + kind) + "|typ then"));
		return "0", "0", "0";
	}
	if ((data.FightAttr[index] == null)) {
		console.error((((("PlayerAttrPart.GetOtherAttr, data.FightAttr[typ] == null,source then" + source) + "|kind then") + index) + "|typ then"));
		return "0", "0", "0";
	}
	const [add, addType, reduceType, coef]  = PlayerAttrPart.ChangeFightAttrToCard_Fight_Type(fightAttr);
	
	//加成
	let resultAdd = data.FightAttr[index].Values[addType];
	let resultReduce = data.FightAttr[index].Values[reduceType];
	let result = BigNumber.sub(resultAdd, resultReduce);
	//固定值
	let resultAddSingle = data.FightAttr[index].Values[add];
	//系数
	let resultCoef = data.FightAttr[index].Values[coef];
	//物防法防要加上双防御
	if (((fightAttr == FightAttr.DEF) || (fightAttr == FightAttr.MDEF))) {
		//加成
		let tempAdd = data.FightAttr[index].Values[PlayerAttrPart.Card_Fight_Type.DoubleDef_Add];
		let tempReduce = data.FightAttr[index].Values[PlayerAttrPart.Card_Fight_Type.DoubleDef_Reduce];
		let temp = BigNumber.sub(tempAdd, tempReduce);
		//固定值
		let tempAddSingle = data.FightAttr[index].Values[PlayerAttrPart.Card_Fight_Type.DoubleDef];
		//系数
		let tempCoef = data.FightAttr[index].Values[PlayerAttrPart.Card_Fight_Type.DoubleDef_Coef];
		result = BigNumber.add(result, temp);
		resultAddSingle = BigNumber.add(resultAddSingle, tempAddSingle);
		resultCoef = BigNumber.add(resultCoef, tempCoef);
	}
	//攻击物防法防血量要加上攻防血
	if (((((fightAttr == FightAttr.DEF) || (fightAttr == FightAttr.MDEF)) || (fightAttr == FightAttr.ATK)) || (fightAttr == FightAttr.HP))) {
		//加成
		let tempAdd = data.FightAttr[index].Values[PlayerAttrPart.Card_Fight_Type.ADH_Add];
		let tempReduce = data.FightAttr[index].Values[PlayerAttrPart.Card_Fight_Type.ADH_Reduce];
		let temp = BigNumber.sub(tempAdd, tempReduce);
		//固定值
		let tempAddSingle = data.FightAttr[index].Values[PlayerAttrPart.Card_Fight_Type.ADH];
		//系数
		let tempCoef = data.FightAttr[index].Values[PlayerAttrPart.Card_Fight_Type.ADH_Coef];
		result = BigNumber.add(result, temp);
		resultAddSingle = BigNumber.add(resultAddSingle, tempAddSingle);
		resultCoef = BigNumber.add(resultCoef, tempCoef);
	}
	//都要除以100
	result = BigNumber.div(result, BigNumber.create(100));
	resultAddSingle = BigNumber.div(resultAddSingle, BigNumber.create(100));
	resultCoef = BigNumber.div(resultCoef, BigNumber.create(100));
	return result, resultAddSingle, resultCoef;
}
//获取阵容精灵的复活文章加成
PlayerAttrPart.GetLineupWenZhangAdd = function() {
	let total = "0";
	for (let i = 1; i != LineupPart.CardList.length; ++i) {
		let card = LineupPart.CardList[i];
		let conf_ShiBing_Data = LoadConfig.getConfigData(ConfigName.ShiBing,card.CardID);
		if ((conf_ShiBing_Data != null)) {
			total = BigNumber.add(total, conf_ShiBing_Data.EvoReliveGoldAddition);
		}
	}
	return total;
}
// 检查参数是否为Long类型还是BigNumber类型
PlayerAttrPart.IsLongValue = function(effectID) {
	if (((((((effectID == 105) || (effectID == 106)) || (effectID == 107)) || (effectID == 108)) || (effectID == 2100)) || (effectID == 2010))) {
		return true;
	}
	return false;
}
// 获取某种卡牌的钥石基础属性加成， 有可能为空
PlayerAttrPart.GetStoneBaseAttr = function(cardID) {
	let stone = Math.floor((cardID / 10));
	return PlayerAttrPart.StoneBaseAttrs[stone];
}
//-------------------------------------属性定义表转换-----------------------
//获取某一个途径的加成信息
//参数为途径枚举，加成ID，作用对象ID
PlayerAttrPart.GetSingleSourceAttrByConfID = function(source, effectID, targetID) {
	//判断是主角属性，还是战斗属性
	let def_EffectID = null;
	if ((effectID >= 2000)) {
		def_EffectID = PlayerAttrPart.ChangeEffectIDToMasterType(effectID);
	} else {
		def_EffectID = PlayerAttrPart.ChangeEffectIDToCard_Fight_Type(effectID);
	}
	if ((def_EffectID == null)) {
		return BigNumber.zero;
	}
	let def_TargetID = PlayerAttrPart.ChangeEffectTarget(targetID);
	let result = null;
	//作用对象是主角
	if ((def_TargetID == PlayerAttrPart.Addition_Kind.Master)) {
		result = PlayerAttrPart.GetMasterAttr(source, def_EffectID);
		let conf_Data = LoadConfig.getConfigData(ConfigName.ShuZhiShuXingDingYi,effctID);
		if ((conf_Data == null)) {
			return result;
		}
		if ((conf_Data.Max == '')) {
			conf_Data.Max = '0';
		}
		let max = BigNumber.create(conf_Data.Max);
		if (BigNumber.lessThanOrEqualTo(max, BigNumber.zero)) {
			return result;
		}
		if (BigNumber.greaterThan(result, conf_Data.Max)) {
			return conf_Data.Max;
		}
		return result;
	} else {
		//作用对象是友方
		result = PlayerAttrPart.GetOtherFightAttr(source, def_TargetID, def_EffectID);
		return result;
	}
	return result;
}
//获取一个道馆队伍的增益
PlayerAttrPart.GetGuildFightAttr = function() {
	let data = PlayerAttrPart.PlayerAttrs[PlayerAttrPart.Source.GuildXunLianZhongXin];
	let ids = { };
	let additions = { };
	if ((data == null)) {
		return ids, additions;
	}
	let fightAttr = data.FightAttr[(PlayerAttrPart.Addition_Kind.GuildTeam + 1)];
	if ((fightAttr == null)) {
		return ids, additions;
	}
	for (let i = PlayerAttrPart.Card_Fight_Type.ATK; i != PlayerAttrPart.Card_Fight_Type.Max; ++i) {
		let rateBig = fightAttr.Values[i];
		let rate = BigNumber.create(rateBig);
		if (BigNumber.greaterThan(rate, BigNumber.zero)) {
			let effectID = PlayerAttrPart.ChangeCard_Fight_TypeToEffectID(i);
			table.insert(ids, effectID);
			table.insert(additions, rate);
		}
	}
	return ids, additions;
}
//--------------------------------------转换单位
//--------------------------------------初始化单位转换数组
let Addition_Master_TypeToEffectID = { };
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.Sneer] = 2000;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.BaseBattleTime] = 2001;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.BattleTimeReduce] = 2002;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.TaskTimeReduce] = 2003;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.TaskGetAdd] = 2004;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.TaskOpenReduce] = 2005;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.TaskUpgrateReduce] = 2006;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.UpgrateCoseReduce] = 2007;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd1] = 2008;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.AmuletJinBiAdd] = 2009;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.DungeonRewardAdd1] = 2010;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd4] = 2011;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd5] = 2012;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd6] = 2013;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd7] = 2014;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd8] = 2015;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd9] = 2016;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd10] = 2017;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.DiaoLuoWenZhangAdd1] = 2020;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.DiaoLuoWenZhangAdd2] = 2021;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.DiaoLuoWenZhangAdd3] = 2022;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.DiaoLuoWenZhangAdd4] = 2023;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.DiaoLuoWenZhangAdd5] = 2024;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.DiaoLuoWenZhangAdd6] = 2025;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.DiaoLuoWenZhangAdd7] = 2026;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.DiaoLuoWenZhangAdd8] = 2027;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.LiHeWenZhangAdd1] = 2030;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.LiHeWenZhangAdd2] = 2031;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.LiHeWenZhangAdd3] = 2032;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.LiHeWenZhangAdd4] = 2033;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.LiHeWenZhangAdd5] = 2034;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.LiHeWenZhangAdd6] = 2035;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.LiHeWenZhangAdd7] = 2036;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.DefaultSpeed] = 2100;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.SpeedAddition] = 2101;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.OffLineSpeedAdd1] = 2102;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.OffLineSpeedAdd2] = 2103;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.OffLineSpeedAdd3] = 2104;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.FreshSpeed] = 2105;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.TeamSync] = 2106;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.OffLineMaxLevel] = 2107;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.IQPassLevel] = 2108;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd2] = 2109;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd3] = 2110;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.LevelLimitAdd1] = 2111;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.LevelLimitAdd2] = 2112;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.LevelLimitAdd3] = 2113;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.BaseLevelAdd1] = 2114;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.BaseLevelAdd2] = 2115;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.BaseLevelAdd3] = 2116;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ShareLife] = 2117;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.GreedDragon] = 2118;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.BlingBling] = 2119;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.AutoBaoXiang] = 2120;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReduceAbiltyCD] = 2121;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.DungeonRewardAdd2] = 2122;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.DungeonTimeAdd] = 2123;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.DungeonRateAdd] = 2124;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveLevel] = 2125;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.RespawnRate] = 2126;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.Guild1] = 2127;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.GoldenStreet] = 2128;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.Guild2] = 2129;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.GuanQiaBaoXiang] = 2130;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.FlashSpeed] = 2131;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.MidasTouch] = 2132;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.SeekingTreasures] = 2133;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.MiningExperts] = 2134;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.Cheerleaders] = 2135;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.TeamBond] = 2136;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.UnityStrength] = 2137;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ElecTreat] = 2138;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.FantasticSpeed] = 2200;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.RagePassLevel] = 2201;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.TheCreator] = 2202;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.LordShiva] = 2203;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.MiniDiablo] = 2204;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.RemoveExperts] = 2205;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.QualityIntensify] = 2206;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.IntensifyWaveguide] = 2207;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.IronRampage] = 2208;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.WaterArea] = 2209;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.BaseLevelAdd4] = 2210;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.LevelLimitAdd4] = 2211;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd11] = 2212;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.DiaoLuoWenZhangAdd9] = 2213;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.LiHeWenZhangAdd8] = 2214;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ChengFengPoLang] = 2215;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.WanShiRuYi] = 2216;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.LevelLimitAdd5] = 2217;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd12] = 2218;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.DiaoLuoWenZhangAdd10] = 2219;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.LiHeWenZhangAdd9] = 2220;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ChiHun] = 3000;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ChiHun1] = 3001;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ChiHun2] = 3002;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ChiHun3] = 3003;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ChiHun4] = 3004;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ChiHun5] = 3005;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ChiHun6] = 3006;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ChiHun7] = 3007;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ChiHun8] = 3008;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ChiHun9] = 3009;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ChiHun10] = 3010;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ChiHun11] = 3011;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ChiHun12] = 3012;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.LevelLimitAdd6] = 3200;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.LevelLimitAdd7] = 3201;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.LevelLimitAdd8] = 3202;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.LevelLimitAdd9] = 3203;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.LevelLimitAdd10] = 3204;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.LevelLimitAdd11] = 3205;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.LevelLimitAdd12] = 3206;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.LevelLimitAdd13] = 3207;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.LevelLimitAdd14] = 3208;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.LevelLimitAdd15] = 3209;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.LevelLimitAdd16] = 3210;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.LevelLimitAdd17] = 3211;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveSpeed] = 3300;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveSpeed1] = 3301;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.PassSpeed] = 3400;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.PassSpeed1] = 3401;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.PassSpeed2] = 3402;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd13] = 3500;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd14] = 3501;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd15] = 3502;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd16] = 3503;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd17] = 3504;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd18] = 3505;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd19] = 3506;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd20] = 3507;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd21] = 3508;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd22] = 3509;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd23] = 3510;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd24] = 3511;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd25] = 3512;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd26] = 3513;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd27] = 3514;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd28] = 3515;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd29] = 3516;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd30] = 3517;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd31] = 3518;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd32] = 3519;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd33] = 3520;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd34] = 3521;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd35] = 3522;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd36] = 3523;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd37] = 3524;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd38] = 3525;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd39] = 3526;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd40] = 3527;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd41] = 3528;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd42] = 3529;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd43] = 3530;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.DiaoLuoWenZhangAdd11] = 3600;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.DiaoLuoWenZhangAdd12] = 3601;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.DiaoLuoWenZhangAdd13] = 3602;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.DiaoLuoWenZhangAdd14] = 3603;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.DiaoLuoWenZhangAdd15] = 3604;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.DiaoLuoWenZhangAdd16] = 3605;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.DiaoLuoWenZhangAdd17] = 3606;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.DiaoLuoWenZhangAdd18] = 3607;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.DiaoLuoWenZhangAdd19] = 3608;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.DiaoLuoWenZhangAdd20] = 3609;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.LiHeWenZhangAdd10] = 3700;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.LiHeWenZhangAdd11] = 3701;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.LiHeWenZhangAdd12] = 3702;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.LiHeWenZhangAdd13] = 3703;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.LiHeWenZhangAdd14] = 3704;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.LiHeWenZhangAdd15] = 3705;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.LiHeWenZhangAdd16] = 3706;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.LiHeWenZhangAdd17] = 3707;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.LiHeWenZhangAdd18] = 3708;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.LiHeWenZhangAdd19] = 3709;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.LiHeWenZhangAdd20] = 3710;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.DungeonFossilAdd] = 3800;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.DungeonFossilAdd1] = 3801;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.DungeonFossilAdd2] = 3802;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.DungeonFossilAdd3] = 3803;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.DungeonFossilAdd4] = 3804;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.DungeonFossilAdd5] = 3805;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.DungeonFossilAdd6] = 3806;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.DungeonFossilAdd7] = 3807;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.DungeonFossilAdd8] = 3808;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.DungeonFossilAdd9] = 3809;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.DungeonTimeAdd1] = 3900;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.DungeonTimeAdd2] = 3901;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.DungeonTimeAdd3] = 3902;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.OffLineSpeedAdd4] = 4000;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.OffLineSpeedAdd5] = 4001;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.OffLineMaxLevel1] = 4100;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.OffLineMaxLevel2] = 4101;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.OffLineMaxLevel3] = 4102;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.OffLineMaxLevel4] = 4103;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveLevel1] = 4200;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveLevel2] = 4201;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveLevel3] = 4202;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveFixedLevel] = 4300;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveFixedLevel1] = 4301;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveFixedLevel2] = 4302;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveFixedLevel3] = 4303;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveFixedLevel4] = 4304;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveFixedLevel5] = 4305;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveFixedLevel6] = 4306;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveFixedLevel7] = 4307;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveFixedLevel8] = 4308;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveFixedLevel9] = 4309;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ReviveFixedLevel10] = 4310;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.DungeonRateAdd1] = 4400;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.DungeonRateAdd2] = 4401;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.DungeonRateAdd3] = 4402;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.DungeonRateAdd4] = 4403;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.GuanQiaBaoXiang1] = 4500;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.GuanQiaBaoXiang2] = 4501;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.GuanQiaBaoXiang3] = 4502;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ChiHunBUFF] = 3100;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ChiHunBUFF1] = 3101;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ChiHunBUFF2] = 3102;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ChiHunBUFF3] = 3103;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ChiHunBUFF4] = 3104;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ChiHunBUFF5] = 3105;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ChiHunBUFF6] = 3106;
Addition_Master_TypeToEffectID[PlayerAttrPart.Addition_Master_Type.ChiHunBUFF7] = 3107;
let EffectIDToAddition_Master_Type = { };
EffectIDToAddition_Master_Type[2001] = PlayerAttrPart.Addition_Master_Type.BaseBattleTime;
EffectIDToAddition_Master_Type[2002] = PlayerAttrPart.Addition_Master_Type.BattleTimeReduce;
EffectIDToAddition_Master_Type[2003] = PlayerAttrPart.Addition_Master_Type.TaskTimeReduce;
EffectIDToAddition_Master_Type[2004] = PlayerAttrPart.Addition_Master_Type.TaskGetAdd;
EffectIDToAddition_Master_Type[2005] = PlayerAttrPart.Addition_Master_Type.TaskOpenReduce;
EffectIDToAddition_Master_Type[2006] = PlayerAttrPart.Addition_Master_Type.TaskUpgrateReduce;
EffectIDToAddition_Master_Type[2007] = PlayerAttrPart.Addition_Master_Type.UpgrateCoseReduce;
EffectIDToAddition_Master_Type[2008] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd1;
EffectIDToAddition_Master_Type[2009] = PlayerAttrPart.Addition_Master_Type.AmuletJinBiAdd;
EffectIDToAddition_Master_Type[2010] = PlayerAttrPart.Addition_Master_Type.DungeonRewardAdd1;
EffectIDToAddition_Master_Type[2011] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd4;
EffectIDToAddition_Master_Type[2012] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd5;
EffectIDToAddition_Master_Type[2013] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd6;
EffectIDToAddition_Master_Type[2014] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd7;
EffectIDToAddition_Master_Type[2015] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd8;
EffectIDToAddition_Master_Type[2016] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd9;
EffectIDToAddition_Master_Type[2017] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd10;
EffectIDToAddition_Master_Type[2020] = PlayerAttrPart.Addition_Master_Type.DiaoLuoWenZhangAdd1;
EffectIDToAddition_Master_Type[2021] = PlayerAttrPart.Addition_Master_Type.DiaoLuoWenZhangAdd2;
EffectIDToAddition_Master_Type[2022] = PlayerAttrPart.Addition_Master_Type.DiaoLuoWenZhangAdd3;
EffectIDToAddition_Master_Type[2023] = PlayerAttrPart.Addition_Master_Type.DiaoLuoWenZhangAdd4;
EffectIDToAddition_Master_Type[2024] = PlayerAttrPart.Addition_Master_Type.DiaoLuoWenZhangAdd5;
EffectIDToAddition_Master_Type[2025] = PlayerAttrPart.Addition_Master_Type.DiaoLuoWenZhangAdd6;
EffectIDToAddition_Master_Type[2026] = PlayerAttrPart.Addition_Master_Type.DiaoLuoWenZhangAdd7;
EffectIDToAddition_Master_Type[2027] = PlayerAttrPart.Addition_Master_Type.DiaoLuoWenZhangAdd8;
EffectIDToAddition_Master_Type[2030] = PlayerAttrPart.Addition_Master_Type.LiHeWenZhangAdd1;
EffectIDToAddition_Master_Type[2031] = PlayerAttrPart.Addition_Master_Type.LiHeWenZhangAdd2;
EffectIDToAddition_Master_Type[2032] = PlayerAttrPart.Addition_Master_Type.LiHeWenZhangAdd3;
EffectIDToAddition_Master_Type[2033] = PlayerAttrPart.Addition_Master_Type.LiHeWenZhangAdd4;
EffectIDToAddition_Master_Type[2034] = PlayerAttrPart.Addition_Master_Type.LiHeWenZhangAdd5;
EffectIDToAddition_Master_Type[2035] = PlayerAttrPart.Addition_Master_Type.LiHeWenZhangAdd6;
EffectIDToAddition_Master_Type[2036] = PlayerAttrPart.Addition_Master_Type.LiHeWenZhangAdd7;
EffectIDToAddition_Master_Type[2100] = PlayerAttrPart.Addition_Master_Type.DefaultSpeed;
EffectIDToAddition_Master_Type[2101] = PlayerAttrPart.Addition_Master_Type.SpeedAddition;
EffectIDToAddition_Master_Type[2102] = PlayerAttrPart.Addition_Master_Type.OffLineSpeedAdd1;
EffectIDToAddition_Master_Type[2103] = PlayerAttrPart.Addition_Master_Type.OffLineSpeedAdd2;
EffectIDToAddition_Master_Type[2104] = PlayerAttrPart.Addition_Master_Type.OffLineSpeedAdd3;
EffectIDToAddition_Master_Type[2105] = PlayerAttrPart.Addition_Master_Type.FreshSpeed;
EffectIDToAddition_Master_Type[2106] = PlayerAttrPart.Addition_Master_Type.TeamSync;
EffectIDToAddition_Master_Type[2107] = PlayerAttrPart.Addition_Master_Type.OffLineMaxLevel;
EffectIDToAddition_Master_Type[2108] = PlayerAttrPart.Addition_Master_Type.IQPassLevel;
EffectIDToAddition_Master_Type[2109] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd2;
EffectIDToAddition_Master_Type[2110] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd3;
EffectIDToAddition_Master_Type[2111] = PlayerAttrPart.Addition_Master_Type.LevelLimitAdd1;
EffectIDToAddition_Master_Type[2112] = PlayerAttrPart.Addition_Master_Type.LevelLimitAdd2;
EffectIDToAddition_Master_Type[2113] = PlayerAttrPart.Addition_Master_Type.LevelLimitAdd3;
EffectIDToAddition_Master_Type[2114] = PlayerAttrPart.Addition_Master_Type.BaseLevelAdd1;
EffectIDToAddition_Master_Type[2115] = PlayerAttrPart.Addition_Master_Type.BaseLevelAdd2;
EffectIDToAddition_Master_Type[2116] = PlayerAttrPart.Addition_Master_Type.BaseLevelAdd3;
EffectIDToAddition_Master_Type[2117] = PlayerAttrPart.Addition_Master_Type.ShareLife;
EffectIDToAddition_Master_Type[2118] = PlayerAttrPart.Addition_Master_Type.GreedDragon;
EffectIDToAddition_Master_Type[2119] = PlayerAttrPart.Addition_Master_Type.BlingBling;
EffectIDToAddition_Master_Type[2120] = PlayerAttrPart.Addition_Master_Type.AutoBaoXiang;
EffectIDToAddition_Master_Type[2121] = PlayerAttrPart.Addition_Master_Type.ReduceAbiltyCD;
EffectIDToAddition_Master_Type[2122] = PlayerAttrPart.Addition_Master_Type.DungeonRewardAdd2;
EffectIDToAddition_Master_Type[2123] = PlayerAttrPart.Addition_Master_Type.DungeonTimeAdd;
EffectIDToAddition_Master_Type[2124] = PlayerAttrPart.Addition_Master_Type.DungeonRateAdd;
EffectIDToAddition_Master_Type[2125] = PlayerAttrPart.Addition_Master_Type.ReviveLevel;
EffectIDToAddition_Master_Type[2126] = PlayerAttrPart.Addition_Master_Type.RespawnRate;
EffectIDToAddition_Master_Type[2127] = PlayerAttrPart.Addition_Master_Type.Guild1;
EffectIDToAddition_Master_Type[2128] = PlayerAttrPart.Addition_Master_Type.GoldenStreet;
EffectIDToAddition_Master_Type[2129] = PlayerAttrPart.Addition_Master_Type.Guild2;
EffectIDToAddition_Master_Type[2130] = PlayerAttrPart.Addition_Master_Type.GuanQiaBaoXiang;
EffectIDToAddition_Master_Type[2131] = PlayerAttrPart.Addition_Master_Type.FlashSpeed;
EffectIDToAddition_Master_Type[2132] = PlayerAttrPart.Addition_Master_Type.MidasTouch;
EffectIDToAddition_Master_Type[2133] = PlayerAttrPart.Addition_Master_Type.SeekingTreasures;
EffectIDToAddition_Master_Type[2134] = PlayerAttrPart.Addition_Master_Type.MiningExperts;
EffectIDToAddition_Master_Type[2135] = PlayerAttrPart.Addition_Master_Type.Cheerleaders;
EffectIDToAddition_Master_Type[2136] = PlayerAttrPart.Addition_Master_Type.TeamBond;
EffectIDToAddition_Master_Type[2137] = PlayerAttrPart.Addition_Master_Type.UnityStrength;
EffectIDToAddition_Master_Type[2138] = PlayerAttrPart.Addition_Master_Type.ElecTreat;
EffectIDToAddition_Master_Type[2200] = PlayerAttrPart.Addition_Master_Type.FantasticSpeed;
EffectIDToAddition_Master_Type[2201] = PlayerAttrPart.Addition_Master_Type.RagePassLevel;
EffectIDToAddition_Master_Type[2202] = PlayerAttrPart.Addition_Master_Type.TheCreator;
EffectIDToAddition_Master_Type[2203] = PlayerAttrPart.Addition_Master_Type.LordShiva;
EffectIDToAddition_Master_Type[2204] = PlayerAttrPart.Addition_Master_Type.MiniDiablo;
EffectIDToAddition_Master_Type[2205] = PlayerAttrPart.Addition_Master_Type.RemoveExperts;
EffectIDToAddition_Master_Type[2206] = PlayerAttrPart.Addition_Master_Type.QualityIntensify;
EffectIDToAddition_Master_Type[2207] = PlayerAttrPart.Addition_Master_Type.IntensifyWaveguide;
EffectIDToAddition_Master_Type[2208] = PlayerAttrPart.Addition_Master_Type.IronRampage;
EffectIDToAddition_Master_Type[2209] = PlayerAttrPart.Addition_Master_Type.WaterArea;
EffectIDToAddition_Master_Type[2210] = PlayerAttrPart.Addition_Master_Type.BaseLevelAdd4;
EffectIDToAddition_Master_Type[2211] = PlayerAttrPart.Addition_Master_Type.LevelLimitAdd4;
EffectIDToAddition_Master_Type[2212] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd11;
EffectIDToAddition_Master_Type[2213] = PlayerAttrPart.Addition_Master_Type.DiaoLuoWenZhangAdd9;
EffectIDToAddition_Master_Type[2214] = PlayerAttrPart.Addition_Master_Type.LiHeWenZhangAdd8;
EffectIDToAddition_Master_Type[2215] = PlayerAttrPart.Addition_Master_Type.ChengFengPoLang;
EffectIDToAddition_Master_Type[2216] = PlayerAttrPart.Addition_Master_Type.WanShiRuYi;
EffectIDToAddition_Master_Type[2217] = PlayerAttrPart.Addition_Master_Type.LevelLimitAdd5;
EffectIDToAddition_Master_Type[2218] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd12;
EffectIDToAddition_Master_Type[2219] = PlayerAttrPart.Addition_Master_Type.DiaoLuoWenZhangAdd10;
EffectIDToAddition_Master_Type[2220] = PlayerAttrPart.Addition_Master_Type.LiHeWenZhangAdd9;
EffectIDToAddition_Master_Type[3000] = PlayerAttrPart.Addition_Master_Type.ChiHun;
EffectIDToAddition_Master_Type[3001] = PlayerAttrPart.Addition_Master_Type.ChiHun1;
EffectIDToAddition_Master_Type[3002] = PlayerAttrPart.Addition_Master_Type.ChiHun2;
EffectIDToAddition_Master_Type[3003] = PlayerAttrPart.Addition_Master_Type.ChiHun3;
EffectIDToAddition_Master_Type[3004] = PlayerAttrPart.Addition_Master_Type.ChiHun4;
EffectIDToAddition_Master_Type[3005] = PlayerAttrPart.Addition_Master_Type.ChiHun5;
EffectIDToAddition_Master_Type[3006] = PlayerAttrPart.Addition_Master_Type.ChiHun6;
EffectIDToAddition_Master_Type[3007] = PlayerAttrPart.Addition_Master_Type.ChiHun7;
EffectIDToAddition_Master_Type[3008] = PlayerAttrPart.Addition_Master_Type.ChiHun8;
EffectIDToAddition_Master_Type[3009] = PlayerAttrPart.Addition_Master_Type.ChiHun9;
EffectIDToAddition_Master_Type[3010] = PlayerAttrPart.Addition_Master_Type.ChiHun10;
EffectIDToAddition_Master_Type[3011] = PlayerAttrPart.Addition_Master_Type.ChiHun11;
EffectIDToAddition_Master_Type[3012] = PlayerAttrPart.Addition_Master_Type.ChiHun12;
EffectIDToAddition_Master_Type[3200] = PlayerAttrPart.Addition_Master_Type.LevelLimitAdd6;
EffectIDToAddition_Master_Type[3201] = PlayerAttrPart.Addition_Master_Type.LevelLimitAdd7;
EffectIDToAddition_Master_Type[3202] = PlayerAttrPart.Addition_Master_Type.LevelLimitAdd8;
EffectIDToAddition_Master_Type[3203] = PlayerAttrPart.Addition_Master_Type.LevelLimitAdd9;
EffectIDToAddition_Master_Type[3204] = PlayerAttrPart.Addition_Master_Type.LevelLimitAdd10;
EffectIDToAddition_Master_Type[3205] = PlayerAttrPart.Addition_Master_Type.LevelLimitAdd11;
EffectIDToAddition_Master_Type[3206] = PlayerAttrPart.Addition_Master_Type.LevelLimitAdd12;
EffectIDToAddition_Master_Type[3207] = PlayerAttrPart.Addition_Master_Type.LevelLimitAdd13;
EffectIDToAddition_Master_Type[3208] = PlayerAttrPart.Addition_Master_Type.LevelLimitAdd14;
EffectIDToAddition_Master_Type[3209] = PlayerAttrPart.Addition_Master_Type.LevelLimitAdd15;
EffectIDToAddition_Master_Type[3210] = PlayerAttrPart.Addition_Master_Type.LevelLimitAdd16;
EffectIDToAddition_Master_Type[3211] = PlayerAttrPart.Addition_Master_Type.LevelLimitAdd17;
EffectIDToAddition_Master_Type[3300] = PlayerAttrPart.Addition_Master_Type.ReviveSpeed;
EffectIDToAddition_Master_Type[3301] = PlayerAttrPart.Addition_Master_Type.ReviveSpeed1;
EffectIDToAddition_Master_Type[3400] = PlayerAttrPart.Addition_Master_Type.PassSpeed;
EffectIDToAddition_Master_Type[3401] = PlayerAttrPart.Addition_Master_Type.PassSpeed1;
EffectIDToAddition_Master_Type[3402] = PlayerAttrPart.Addition_Master_Type.PassSpeed2;
EffectIDToAddition_Master_Type[3500] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd13;
EffectIDToAddition_Master_Type[3501] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd14;
EffectIDToAddition_Master_Type[3502] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd15;
EffectIDToAddition_Master_Type[3503] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd16;
EffectIDToAddition_Master_Type[3504] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd17;
EffectIDToAddition_Master_Type[3505] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd18;
EffectIDToAddition_Master_Type[3506] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd19;
EffectIDToAddition_Master_Type[3507] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd20;
EffectIDToAddition_Master_Type[3508] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd21;
EffectIDToAddition_Master_Type[3509] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd22;
EffectIDToAddition_Master_Type[3510] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd23;
EffectIDToAddition_Master_Type[3511] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd24;
EffectIDToAddition_Master_Type[3512] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd25;
EffectIDToAddition_Master_Type[3513] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd26;
EffectIDToAddition_Master_Type[3514] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd27;
EffectIDToAddition_Master_Type[3515] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd28;
EffectIDToAddition_Master_Type[3516] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd29;
EffectIDToAddition_Master_Type[3517] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd30;
EffectIDToAddition_Master_Type[3518] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd31;
EffectIDToAddition_Master_Type[3519] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd32;
EffectIDToAddition_Master_Type[3520] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd33;
EffectIDToAddition_Master_Type[3521] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd34;
EffectIDToAddition_Master_Type[3522] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd35;
EffectIDToAddition_Master_Type[3523] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd36;
EffectIDToAddition_Master_Type[3524] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd37;
EffectIDToAddition_Master_Type[3525] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd38;
EffectIDToAddition_Master_Type[3526] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd39;
EffectIDToAddition_Master_Type[3527] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd40;
EffectIDToAddition_Master_Type[3528] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd41;
EffectIDToAddition_Master_Type[3529] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd42;
EffectIDToAddition_Master_Type[3530] = PlayerAttrPart.Addition_Master_Type.ReviveWenZhangAdd43;
EffectIDToAddition_Master_Type[3600] = PlayerAttrPart.Addition_Master_Type.DiaoLuoWenZhangAdd11;
EffectIDToAddition_Master_Type[3601] = PlayerAttrPart.Addition_Master_Type.DiaoLuoWenZhangAdd12;
EffectIDToAddition_Master_Type[3602] = PlayerAttrPart.Addition_Master_Type.DiaoLuoWenZhangAdd13;
EffectIDToAddition_Master_Type[3603] = PlayerAttrPart.Addition_Master_Type.DiaoLuoWenZhangAdd14;
EffectIDToAddition_Master_Type[3604] = PlayerAttrPart.Addition_Master_Type.DiaoLuoWenZhangAdd15;
EffectIDToAddition_Master_Type[3605] = PlayerAttrPart.Addition_Master_Type.DiaoLuoWenZhangAdd16;
EffectIDToAddition_Master_Type[3606] = PlayerAttrPart.Addition_Master_Type.DiaoLuoWenZhangAdd17;
EffectIDToAddition_Master_Type[3607] = PlayerAttrPart.Addition_Master_Type.DiaoLuoWenZhangAdd18;
EffectIDToAddition_Master_Type[3608] = PlayerAttrPart.Addition_Master_Type.DiaoLuoWenZhangAdd19;
EffectIDToAddition_Master_Type[3609] = PlayerAttrPart.Addition_Master_Type.DiaoLuoWenZhangAdd20;
EffectIDToAddition_Master_Type[3700] = PlayerAttrPart.Addition_Master_Type.LiHeWenZhangAdd10;
EffectIDToAddition_Master_Type[3701] = PlayerAttrPart.Addition_Master_Type.LiHeWenZhangAdd11;
EffectIDToAddition_Master_Type[3702] = PlayerAttrPart.Addition_Master_Type.LiHeWenZhangAdd12;
EffectIDToAddition_Master_Type[3703] = PlayerAttrPart.Addition_Master_Type.LiHeWenZhangAdd13;
EffectIDToAddition_Master_Type[3704] = PlayerAttrPart.Addition_Master_Type.LiHeWenZhangAdd14;
EffectIDToAddition_Master_Type[3705] = PlayerAttrPart.Addition_Master_Type.LiHeWenZhangAdd15;
EffectIDToAddition_Master_Type[3706] = PlayerAttrPart.Addition_Master_Type.LiHeWenZhangAdd16;
EffectIDToAddition_Master_Type[3707] = PlayerAttrPart.Addition_Master_Type.LiHeWenZhangAdd17;
EffectIDToAddition_Master_Type[3708] = PlayerAttrPart.Addition_Master_Type.LiHeWenZhangAdd18;
EffectIDToAddition_Master_Type[3709] = PlayerAttrPart.Addition_Master_Type.LiHeWenZhangAdd19;
EffectIDToAddition_Master_Type[3710] = PlayerAttrPart.Addition_Master_Type.LiHeWenZhangAdd20;
EffectIDToAddition_Master_Type[3800] = PlayerAttrPart.Addition_Master_Type.DungeonFossilAdd;
EffectIDToAddition_Master_Type[3801] = PlayerAttrPart.Addition_Master_Type.DungeonFossilAdd1;
EffectIDToAddition_Master_Type[3802] = PlayerAttrPart.Addition_Master_Type.DungeonFossilAdd2;
EffectIDToAddition_Master_Type[3803] = PlayerAttrPart.Addition_Master_Type.DungeonFossilAdd3;
EffectIDToAddition_Master_Type[3804] = PlayerAttrPart.Addition_Master_Type.DungeonFossilAdd4;
EffectIDToAddition_Master_Type[3805] = PlayerAttrPart.Addition_Master_Type.DungeonFossilAdd5;
EffectIDToAddition_Master_Type[3806] = PlayerAttrPart.Addition_Master_Type.DungeonFossilAdd6;
EffectIDToAddition_Master_Type[3807] = PlayerAttrPart.Addition_Master_Type.DungeonFossilAdd7;
EffectIDToAddition_Master_Type[3808] = PlayerAttrPart.Addition_Master_Type.DungeonFossilAdd8;
EffectIDToAddition_Master_Type[3809] = PlayerAttrPart.Addition_Master_Type.DungeonFossilAdd9;
EffectIDToAddition_Master_Type[3900] = PlayerAttrPart.Addition_Master_Type.DungeonTimeAdd1;
EffectIDToAddition_Master_Type[3901] = PlayerAttrPart.Addition_Master_Type.DungeonTimeAdd2;
EffectIDToAddition_Master_Type[3902] = PlayerAttrPart.Addition_Master_Type.DungeonTimeAdd3;
EffectIDToAddition_Master_Type[4000] = PlayerAttrPart.Addition_Master_Type.OffLineSpeedAdd4;
EffectIDToAddition_Master_Type[4001] = PlayerAttrPart.Addition_Master_Type.OffLineSpeedAdd5;
EffectIDToAddition_Master_Type[4100] = PlayerAttrPart.Addition_Master_Type.OffLineMaxLevel1;
EffectIDToAddition_Master_Type[4101] = PlayerAttrPart.Addition_Master_Type.OffLineMaxLevel2;
EffectIDToAddition_Master_Type[4102] = PlayerAttrPart.Addition_Master_Type.OffLineMaxLevel3;
EffectIDToAddition_Master_Type[4103] = PlayerAttrPart.Addition_Master_Type.OffLineMaxLevel4;
EffectIDToAddition_Master_Type[4200] = PlayerAttrPart.Addition_Master_Type.ReviveLevel1;
EffectIDToAddition_Master_Type[4201] = PlayerAttrPart.Addition_Master_Type.ReviveLevel2;
EffectIDToAddition_Master_Type[4202] = PlayerAttrPart.Addition_Master_Type.ReviveLevel3;
EffectIDToAddition_Master_Type[4300] = PlayerAttrPart.Addition_Master_Type.ReviveFixedLevel;
EffectIDToAddition_Master_Type[4301] = PlayerAttrPart.Addition_Master_Type.ReviveFixedLevel1;
EffectIDToAddition_Master_Type[4302] = PlayerAttrPart.Addition_Master_Type.ReviveFixedLevel2;
EffectIDToAddition_Master_Type[4303] = PlayerAttrPart.Addition_Master_Type.ReviveFixedLevel3;
EffectIDToAddition_Master_Type[4304] = PlayerAttrPart.Addition_Master_Type.ReviveFixedLevel4;
EffectIDToAddition_Master_Type[4305] = PlayerAttrPart.Addition_Master_Type.ReviveFixedLevel5;
EffectIDToAddition_Master_Type[4306] = PlayerAttrPart.Addition_Master_Type.ReviveFixedLevel6;
EffectIDToAddition_Master_Type[4307] = PlayerAttrPart.Addition_Master_Type.ReviveFixedLevel7;
EffectIDToAddition_Master_Type[4308] = PlayerAttrPart.Addition_Master_Type.ReviveFixedLevel8;
EffectIDToAddition_Master_Type[4309] = PlayerAttrPart.Addition_Master_Type.ReviveFixedLevel9;
EffectIDToAddition_Master_Type[4310] = PlayerAttrPart.Addition_Master_Type.ReviveFixedLevel10;
EffectIDToAddition_Master_Type[4400] = PlayerAttrPart.Addition_Master_Type.DungeonRateAdd1;
EffectIDToAddition_Master_Type[4401] = PlayerAttrPart.Addition_Master_Type.DungeonRateAdd2;
EffectIDToAddition_Master_Type[4402] = PlayerAttrPart.Addition_Master_Type.DungeonRateAdd3;
EffectIDToAddition_Master_Type[4403] = PlayerAttrPart.Addition_Master_Type.DungeonRateAdd4;
EffectIDToAddition_Master_Type[4500] = PlayerAttrPart.Addition_Master_Type.GuanQiaBaoXiang1;
EffectIDToAddition_Master_Type[4501] = PlayerAttrPart.Addition_Master_Type.GuanQiaBaoXiang2;
EffectIDToAddition_Master_Type[4502] = PlayerAttrPart.Addition_Master_Type.GuanQiaBaoXiang3;
EffectIDToAddition_Master_Type[3100] = PlayerAttrPart.Addition_Master_Type.ChiHunBUFF;
EffectIDToAddition_Master_Type[3101] = PlayerAttrPart.Addition_Master_Type.ChiHunBUFF1;
EffectIDToAddition_Master_Type[3102] = PlayerAttrPart.Addition_Master_Type.ChiHunBUFF2;
EffectIDToAddition_Master_Type[3103] = PlayerAttrPart.Addition_Master_Type.ChiHunBUFF3;
EffectIDToAddition_Master_Type[3104] = PlayerAttrPart.Addition_Master_Type.ChiHunBUFF4;
EffectIDToAddition_Master_Type[3105] = PlayerAttrPart.Addition_Master_Type.ChiHunBUFF5;
EffectIDToAddition_Master_Type[3106] = PlayerAttrPart.Addition_Master_Type.ChiHunBUFF6;
EffectIDToAddition_Master_Type[3107] = PlayerAttrPart.Addition_Master_Type.ChiHunBUFF7;
let EffectIDToCard_Fight_Type = { };
EffectIDToCard_Fight_Type[1] = PlayerAttrPart.Card_Fight_Type.ATK;
EffectIDToCard_Fight_Type[2] = PlayerAttrPart.Card_Fight_Type.HP;
EffectIDToCard_Fight_Type[3] = PlayerAttrPart.Card_Fight_Type.DEF;
EffectIDToCard_Fight_Type[4] = PlayerAttrPart.Card_Fight_Type.MDEF;
EffectIDToCard_Fight_Type[5] = PlayerAttrPart.Card_Fight_Type.ASPD;
EffectIDToCard_Fight_Type[6] = PlayerAttrPart.Card_Fight_Type.Speed;
EffectIDToCard_Fight_Type[7] = PlayerAttrPart.Card_Fight_Type.Range;
EffectIDToCard_Fight_Type[8] = PlayerAttrPart.Card_Fight_Type.CRI;
EffectIDToCard_Fight_Type[9] = PlayerAttrPart.Card_Fight_Type.CritDamage;
EffectIDToCard_Fight_Type[10] = PlayerAttrPart.Card_Fight_Type.HitRate;
EffectIDToCard_Fight_Type[11] = PlayerAttrPart.Card_Fight_Type.DodgeRate;
EffectIDToCard_Fight_Type[12] = PlayerAttrPart.Card_Fight_Type.Respawn;
EffectIDToCard_Fight_Type[30] = PlayerAttrPart.Card_Fight_Type.DoubleDef;
EffectIDToCard_Fight_Type[40] = PlayerAttrPart.Card_Fight_Type.ADH;
EffectIDToCard_Fight_Type[51] = PlayerAttrPart.Card_Fight_Type.ATK_Base;
EffectIDToCard_Fight_Type[52] = PlayerAttrPart.Card_Fight_Type.HP_Base;
EffectIDToCard_Fight_Type[53] = PlayerAttrPart.Card_Fight_Type.DEF_Base;
EffectIDToCard_Fight_Type[54] = PlayerAttrPart.Card_Fight_Type.MDEF_Base;
EffectIDToCard_Fight_Type[55] = PlayerAttrPart.Card_Fight_Type.ASPD_Base;
EffectIDToCard_Fight_Type[56] = PlayerAttrPart.Card_Fight_Type.Speed_Base;
EffectIDToCard_Fight_Type[57] = PlayerAttrPart.Card_Fight_Type.Range_Base;
EffectIDToCard_Fight_Type[58] = PlayerAttrPart.Card_Fight_Type.CRI_Base;
EffectIDToCard_Fight_Type[59] = PlayerAttrPart.Card_Fight_Type.CritDamage_Base;
EffectIDToCard_Fight_Type[60] = PlayerAttrPart.Card_Fight_Type.HitRate_Base;
EffectIDToCard_Fight_Type[61] = PlayerAttrPart.Card_Fight_Type.DodgeRate_Base;
EffectIDToCard_Fight_Type[62] = PlayerAttrPart.Card_Fight_Type.Respawn_Base;
EffectIDToCard_Fight_Type[80] = PlayerAttrPart.Card_Fight_Type.DoubleDef_Base;
EffectIDToCard_Fight_Type[90] = PlayerAttrPart.Card_Fight_Type.ADH_Base;
EffectIDToCard_Fight_Type[101] = PlayerAttrPart.Card_Fight_Type.ATK_Add;
EffectIDToCard_Fight_Type[102] = PlayerAttrPart.Card_Fight_Type.HP_Add;
EffectIDToCard_Fight_Type[103] = PlayerAttrPart.Card_Fight_Type.DEF_Add;
EffectIDToCard_Fight_Type[104] = PlayerAttrPart.Card_Fight_Type.MDEF_Add;
EffectIDToCard_Fight_Type[105] = PlayerAttrPart.Card_Fight_Type.ASPD_Add;
EffectIDToCard_Fight_Type[106] = PlayerAttrPart.Card_Fight_Type.Speed_Add;
EffectIDToCard_Fight_Type[107] = PlayerAttrPart.Card_Fight_Type.Range_Add;
EffectIDToCard_Fight_Type[108] = PlayerAttrPart.Card_Fight_Type.CRI_Add;
EffectIDToCard_Fight_Type[109] = PlayerAttrPart.Card_Fight_Type.CritDamage_Add;
EffectIDToCard_Fight_Type[110] = PlayerAttrPart.Card_Fight_Type.HitRate_Add;
EffectIDToCard_Fight_Type[111] = PlayerAttrPart.Card_Fight_Type.DodgeRate_Add;
EffectIDToCard_Fight_Type[112] = PlayerAttrPart.Card_Fight_Type.Respawn_Add;
EffectIDToCard_Fight_Type[130] = PlayerAttrPart.Card_Fight_Type.DoubleDef_Add;
EffectIDToCard_Fight_Type[140] = PlayerAttrPart.Card_Fight_Type.ADH_Add;
EffectIDToCard_Fight_Type[151] = PlayerAttrPart.Card_Fight_Type.ATK_Reduce;
EffectIDToCard_Fight_Type[152] = PlayerAttrPart.Card_Fight_Type.HP_Reduce;
EffectIDToCard_Fight_Type[153] = PlayerAttrPart.Card_Fight_Type.DEF_Reduce;
EffectIDToCard_Fight_Type[154] = PlayerAttrPart.Card_Fight_Type.MDEF_Reduce;
EffectIDToCard_Fight_Type[155] = PlayerAttrPart.Card_Fight_Type.ASPD_Reduce;
EffectIDToCard_Fight_Type[156] = PlayerAttrPart.Card_Fight_Type.Speed_Reduce;
EffectIDToCard_Fight_Type[157] = PlayerAttrPart.Card_Fight_Type.Range_Reduce;
EffectIDToCard_Fight_Type[158] = PlayerAttrPart.Card_Fight_Type.CRI_Reduce;
EffectIDToCard_Fight_Type[159] = PlayerAttrPart.Card_Fight_Type.CritDamage_Reduce;
EffectIDToCard_Fight_Type[160] = PlayerAttrPart.Card_Fight_Type.HitRate_Reduce;
EffectIDToCard_Fight_Type[161] = PlayerAttrPart.Card_Fight_Type.DodgeRate_Reduce;
EffectIDToCard_Fight_Type[162] = PlayerAttrPart.Card_Fight_Type.Respawn_Reduce;
EffectIDToCard_Fight_Type[180] = PlayerAttrPart.Card_Fight_Type.DoubleDef_Reduce;
EffectIDToCard_Fight_Type[190] = PlayerAttrPart.Card_Fight_Type.ADH_Reduce;
EffectIDToCard_Fight_Type[201] = PlayerAttrPart.Card_Fight_Type.ATK_Coef;
EffectIDToCard_Fight_Type[202] = PlayerAttrPart.Card_Fight_Type.HP_Coef;
EffectIDToCard_Fight_Type[203] = PlayerAttrPart.Card_Fight_Type.DEF_Coef;
EffectIDToCard_Fight_Type[204] = PlayerAttrPart.Card_Fight_Type.MDEF_Coef;
EffectIDToCard_Fight_Type[205] = PlayerAttrPart.Card_Fight_Type.ASPD_Coef;
EffectIDToCard_Fight_Type[206] = PlayerAttrPart.Card_Fight_Type.Speed_Coef;
EffectIDToCard_Fight_Type[207] = PlayerAttrPart.Card_Fight_Type.Range_Coef;
EffectIDToCard_Fight_Type[208] = PlayerAttrPart.Card_Fight_Type.CRI_Coef;
EffectIDToCard_Fight_Type[209] = PlayerAttrPart.Card_Fight_Type.CritDamage_Coef;
EffectIDToCard_Fight_Type[210] = PlayerAttrPart.Card_Fight_Type.HitRate_Coef;
EffectIDToCard_Fight_Type[211] = PlayerAttrPart.Card_Fight_Type.DodgeRate_Coef;
EffectIDToCard_Fight_Type[212] = PlayerAttrPart.Card_Fight_Type.Respawn_Coef;
EffectIDToCard_Fight_Type[230] = PlayerAttrPart.Card_Fight_Type.DoubleDef_Coef;
EffectIDToCard_Fight_Type[240] = PlayerAttrPart.Card_Fight_Type.ADH_Coef;
EffectIDToCard_Fight_Type[301] = PlayerAttrPart.Card_Fight_Type.ImmRepel;
EffectIDToCard_Fight_Type[302] = PlayerAttrPart.Card_Fight_Type.ImmOff;
EffectIDToCard_Fight_Type[303] = PlayerAttrPart.Card_Fight_Type.ImmComa;
EffectIDToCard_Fight_Type[304] = PlayerAttrPart.Card_Fight_Type.ImmFrozen;
EffectIDToCard_Fight_Type[305] = PlayerAttrPart.Card_Fight_Type.ImmSlowDown;
EffectIDToCard_Fight_Type[306] = PlayerAttrPart.Card_Fight_Type.ImmParalysis;
EffectIDToCard_Fight_Type[307] = PlayerAttrPart.Card_Fight_Type.ImmOverawe;
EffectIDToCard_Fight_Type[306] = PlayerAttrPart.Card_Fight_Type.ImmTwine;
EffectIDToCard_Fight_Type[309] = PlayerAttrPart.Card_Fight_Type.ImmConcussion;
EffectIDToCard_Fight_Type[310] = PlayerAttrPart.Card_Fight_Type.ImmBounce;
EffectIDToCard_Fight_Type[311] = PlayerAttrPart.Card_Fight_Type.ImmPetrifaction;
EffectIDToCard_Fight_Type[312] = PlayerAttrPart.Card_Fight_Type.ImmVampire;
EffectIDToCard_Fight_Type[313] = PlayerAttrPart.Card_Fight_Type.ImmThreaten;
EffectIDToCard_Fight_Type[314] = PlayerAttrPart.Card_Fight_Type.ImmGhostCurse;
EffectIDToCard_Fight_Type[315] = PlayerAttrPart.Card_Fight_Type.ImmDisturb;
EffectIDToCard_Fight_Type[316] = PlayerAttrPart.Card_Fight_Type.ImmPoison;
EffectIDToCard_Fight_Type[317] = PlayerAttrPart.Card_Fight_Type.ImmCharm;
EffectIDToCard_Fight_Type[401] = PlayerAttrPart.Card_Fight_Type.ResisRepel;
EffectIDToCard_Fight_Type[402] = PlayerAttrPart.Card_Fight_Type.ResisOff;
EffectIDToCard_Fight_Type[403] = PlayerAttrPart.Card_Fight_Type.ResisComa;
EffectIDToCard_Fight_Type[404] = PlayerAttrPart.Card_Fight_Type.ResisFrozen;
EffectIDToCard_Fight_Type[405] = PlayerAttrPart.Card_Fight_Type.ResisSlowDown;
EffectIDToCard_Fight_Type[406] = PlayerAttrPart.Card_Fight_Type.ResisParalysis;
EffectIDToCard_Fight_Type[407] = PlayerAttrPart.Card_Fight_Type.ResisOverawe;
EffectIDToCard_Fight_Type[408] = PlayerAttrPart.Card_Fight_Type.ResisTwine;
EffectIDToCard_Fight_Type[409] = PlayerAttrPart.Card_Fight_Type.ResisConcussion;
EffectIDToCard_Fight_Type[410] = PlayerAttrPart.Card_Fight_Type.ResisBounce;
EffectIDToCard_Fight_Type[411] = PlayerAttrPart.Card_Fight_Type.ResisPetrifaction;
EffectIDToCard_Fight_Type[412] = PlayerAttrPart.Card_Fight_Type.ResisVampire;
EffectIDToCard_Fight_Type[413] = PlayerAttrPart.Card_Fight_Type.ResisThreaten;
EffectIDToCard_Fight_Type[414] = PlayerAttrPart.Card_Fight_Type.ResisGhostCurse;
EffectIDToCard_Fight_Type[415] = PlayerAttrPart.Card_Fight_Type.ResisDisturb;
EffectIDToCard_Fight_Type[416] = PlayerAttrPart.Card_Fight_Type.ResisPoison;
EffectIDToCard_Fight_Type[417] = PlayerAttrPart.Card_Fight_Type.ResisCharm;
EffectIDToCard_Fight_Type[600] = PlayerAttrPart.Card_Fight_Type.Ice_Add;
//受到冰属性精灵伤害增加
EffectIDToCard_Fight_Type[601] = PlayerAttrPart.Card_Fight_Type.Dragon_Add;
//受到龙属性精灵伤害增加
EffectIDToCard_Fight_Type[602] = PlayerAttrPart.Card_Fight_Type.Fire_Add;
//受到火属性精灵伤害增加
EffectIDToCard_Fight_Type[603] = PlayerAttrPart.Card_Fight_Type.Grass_Add;
//受到草属性精灵伤害增加
EffectIDToCard_Fight_Type[604] = PlayerAttrPart.Card_Fight_Type.Water_Add;
//受到水属性精灵伤害增加
EffectIDToCard_Fight_Type[605] = PlayerAttrPart.Card_Fight_Type.Soil_Add;
//受到地面属性精灵伤害增加
EffectIDToCard_Fight_Type[606] = PlayerAttrPart.Card_Fight_Type.Electricity_Add;
//受到电属性精灵伤害增加
EffectIDToCard_Fight_Type[607] = PlayerAttrPart.Card_Fight_Type.SuperPower_Add;
//受到超能力属性精灵伤害增加
EffectIDToCard_Fight_Type[608] = PlayerAttrPart.Card_Fight_Type.Poison_Add;
//受到毒属性精灵伤害增加
EffectIDToCard_Fight_Type[609] = PlayerAttrPart.Card_Fight_Type.Fairy_Add;
//受到妖精属性精灵伤害增加
EffectIDToCard_Fight_Type[610] = PlayerAttrPart.Card_Fight_Type.Wrestle_Add;
//受到格斗属性精灵伤害增加
EffectIDToCard_Fight_Type[611] = PlayerAttrPart.Card_Fight_Type.Steel_Add;
//受到钢属性精灵伤害增加
EffectIDToCard_Fight_Type[612] = PlayerAttrPart.Card_Fight_Type.Rock_Add;
//受到岩石属性精灵伤害增加
EffectIDToCard_Fight_Type[613] = PlayerAttrPart.Card_Fight_Type.Worm_Add;
//受到虫属性精灵伤害增加
EffectIDToCard_Fight_Type[614] = PlayerAttrPart.Card_Fight_Type.Evil_Add;
//受到恶属性精灵伤害增加
EffectIDToCard_Fight_Type[615] = PlayerAttrPart.Card_Fight_Type.Ghost_Add;
//受到幽灵属性精灵伤害增加
EffectIDToCard_Fight_Type[616] = PlayerAttrPart.Card_Fight_Type.Ordinary_Add;
//受到一般属性精灵伤害增加
EffectIDToCard_Fight_Type[617] = PlayerAttrPart.Card_Fight_Type.Ice_Reduce;
//受到冰属性精灵伤害减少
EffectIDToCard_Fight_Type[618] = PlayerAttrPart.Card_Fight_Type.Dragon_Reduce;
//受到龙属性精灵伤害减少
EffectIDToCard_Fight_Type[619] = PlayerAttrPart.Card_Fight_Type.Fire_Reduce;
//受到火属性精灵伤害减少
EffectIDToCard_Fight_Type[620] = PlayerAttrPart.Card_Fight_Type.Grass_Reduce;
//受到草属性精灵伤害减少
EffectIDToCard_Fight_Type[621] = PlayerAttrPart.Card_Fight_Type.Water_Reduce;
//受到水属性精灵伤害减少
EffectIDToCard_Fight_Type[622] = PlayerAttrPart.Card_Fight_Type.Soil_Reduce;
//受到地面属性精灵伤害减少
EffectIDToCard_Fight_Type[623] = PlayerAttrPart.Card_Fight_Type.Electricity_Reduce;
//受到电属性精灵伤害减少
EffectIDToCard_Fight_Type[624] = PlayerAttrPart.Card_Fight_Type.SuperPower_Reduce;
//受到超能力属性精灵伤害减少
EffectIDToCard_Fight_Type[625] = PlayerAttrPart.Card_Fight_Type.Poison_Reduce;
//受到毒属性精灵伤害减少
EffectIDToCard_Fight_Type[626] = PlayerAttrPart.Card_Fight_Type.Fairy_Reduce;
//受到妖精属性精灵伤害减少
EffectIDToCard_Fight_Type[627] = PlayerAttrPart.Card_Fight_Type.Wrestle_Reduce;
//受到格斗属性精灵伤害减少
EffectIDToCard_Fight_Type[628] = PlayerAttrPart.Card_Fight_Type.Steel_Reduce;
//受到钢属性精灵伤害减少
EffectIDToCard_Fight_Type[629] = PlayerAttrPart.Card_Fight_Type.Rock_Reduce;
//受到岩石属性精灵伤害减少
EffectIDToCard_Fight_Type[630] = PlayerAttrPart.Card_Fight_Type.Worm_Reduce;
//受到虫属性精灵伤害减少
EffectIDToCard_Fight_Type[631] = PlayerAttrPart.Card_Fight_Type.Evil_Reduce;
//受到恶属性精灵伤害减少
EffectIDToCard_Fight_Type[632] = PlayerAttrPart.Card_Fight_Type.Ghost_Reduce;
//受到幽灵属性精灵伤害减少
EffectIDToCard_Fight_Type[633] = PlayerAttrPart.Card_Fight_Type.Ordinary_Reduce;
//受到一般属性精灵伤害减少
EffectIDToCard_Fight_Type[700] = PlayerAttrPart.Card_Fight_Type.RestoreHP;
//5秒恢复 【x】% 生命
EffectIDToCard_Fight_Type[701] = PlayerAttrPart.Card_Fight_Type.EasyHurt;
//BOSS易伤 【x】 秒
let Card_Fight_TypeToEffectID = { };
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ATK] = 1;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.HP] = 2;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.DEF] = 3;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.MDEF] = 4;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ASPD] = 5;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Speed] = 6;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Range] = 7;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.CRI] = 8;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.CritDamage] = 9;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.HitRate] = 10;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.DodgeRate] = 11;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Respawn] = 12;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.DoubleDef] = 30;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ADH] = 40;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ATK_Base] = 51;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.HP_Base] = 52;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.DEF_Base] = 53;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.MDEF_Base] = 54;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ASPD_Base] = 55;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Speed_Base] = 56;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Range_Base] = 57;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.CRI_Base] = 58;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.CritDamage_Base] = 59;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.HitRate_Base] = 60;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.DodgeRate_Base] = 61;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Respawn_Base] = 62;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.DoubleDef_Base] = 80;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ADH_Base] = 90;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ATK_Add] = 101;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.HP_Add] = 102;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.DEF_Add] = 103;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.MDEF_Add] = 104;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ASPD_Add] = 105;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Speed_Add] = 106;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Range_Add] = 107;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.CRI_Add] = 108;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.CritDamage_Add] = 109;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.HitRate_Add] = 110;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.DodgeRate_Add] = 111;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Respawn_Add] = 112;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.DoubleDef_Add] = 130;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ADH_Add] = 140;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ATK_Reduce] = 151;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.HP_Reduce] = 152;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.DEF_Reduce] = 153;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.MDEF_Reduce] = 154;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ASPD_Reduce] = 155;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Speed_Reduce] = 156;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Range_Reduce] = 157;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.CRI_Reduce] = 158;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.CritDamage_Reduce] = 159;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.HitRate_Reduce] = 160;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.DodgeRate_Reduce] = 161;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Respawn_Reduce] = 162;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.DoubleDef_Reduce] = 180;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ADH_Reduce] = 190;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ATK_Coef] = 201;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.HP_Coef] = 202;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.DEF_Coef] = 203;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.MDEF_Coef] = 204;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ASPD_Coef] = 205;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Speed_Coef] = 206;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Range_Coef] = 207;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.CRI_Coef] = 208;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.CritDamage_Coef] = 209;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.HitRate_Coef] = 210;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.DodgeRate_Coef] = 211;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Respawn_Coef] = 212;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.DoubleDef_Coef] = 230;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ADH_Coef] = 240;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ImmRepel] = 301;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ImmOff] = 302;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ImmComa] = 303;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ImmFrozen] = 304;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ImmSlowDown] = 305;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ImmParalysis] = 306;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ImmOverawe] = 307;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ImmTwine] = 308;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ImmConcussion] = 309;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ImmBounce] = 310;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ImmPetrifaction] = 311;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ImmVampire] = 312;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ImmThreaten] = 313;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ImmGhostCurse] = 314;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ImmDisturb] = 315;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ImmPoison] = 316;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ImmCharm] = 317;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ResisRepel] = 401;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ResisOff] = 402;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ResisComa] = 403;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ResisFrozen] = 404;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ResisSlowDown] = 405;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ResisParalysis] = 406;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ResisOverawe] = 407;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ResisTwine] = 408;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ResisConcussion] = 409;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ResisBounce] = 410;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ResisPetrifaction] = 411;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ResisVampire] = 412;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ResisThreaten] = 413;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ResisGhostCurse] = 414;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ResisDisturb] = 415;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ResisPoison] = 416;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.ResisCharm] = 417;
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Ice_Add] = 600;
//受到冰属性精灵伤害增加
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Dragon_Add] = 601;
//受到龙属性精灵伤害增加
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Fire_Add] = 602;
//受到火属性精灵伤害增加
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Grass_Add] = 603;
//受到草属性精灵伤害增加
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Water_Add] = 604;
//受到水属性精灵伤害增加
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Soil_Add] = 605;
//受到地面属性精灵伤害增加
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Electricity_Add] = 606;
//受到电属性精灵伤害增加
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.SuperPower_Add] = 607;
//受到超能力属性精灵伤害增加
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Poison_Add] = 608;
//受到毒属性精灵伤害增加
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Fairy_Add] = 609;
//受到妖精属性精灵伤害增加
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Wrestle_Add] = 610;
//受到格斗属性精灵伤害增加
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Steel_Add] = 611;
//受到钢属性精灵伤害增加
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Rock_Add] = 612;
//受到岩石属性精灵伤害增加
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Worm_Add] = 613;
//受到虫属性精灵伤害增加
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Evil_Add] = 614;
//受到恶属性精灵伤害增加
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Ghost_Add] = 615;
//受到幽灵属性精灵伤害增加
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Ordinary_Add] = 616;
//受到一般属性精灵伤害增加
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Ice_Reduce] = 617;
//受到冰属性精灵伤害减少
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Dragon_Reduce] = 618;
//受到龙属性精灵伤害减少
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Fire_Reduce] = 619;
//受到火属性精灵伤害减少
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Grass_Reduce] = 620;
//受到草属性精灵伤害减少
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Water_Reduce] = 621;
//受到水属性精灵伤害减少
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Soil_Reduce] = 622;
//受到地面属性精灵伤害减少
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Electricity_Reduce] = 623;
//受到电属性精灵伤害减少
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.SuperPower_Reduce] = 624;
//受到超能力属性精灵伤害减少
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Poison_Reduce] = 625;
//受到毒属性精灵伤害减少
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Fairy_Reduce] = 626;
//受到妖精属性精灵伤害减少
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Wrestle_Reduce] = 627;
//受到格斗属性精灵伤害减少
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Steel_Reduce] = 628;
//受到钢属性精灵伤害减少
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Rock_Reduce] = 629;
//受到岩石属性精灵伤害减少
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Worm_Reduce] = 630;
//受到虫属性精灵伤害减少
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Evil_Reduce] = 631;
//受到恶属性精灵伤害减少
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Ghost_Reduce] = 632;
//受到幽灵属性精灵伤害减少
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.Ordinary_Reduce] = 633;
//受到一般属性精灵伤害减少
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.RestoreHP] = 700;
//5秒恢复 【x】% 生命
Card_Fight_TypeToEffectID[PlayerAttrPart.Card_Fight_Type.EasyHurt] = 701;
//BOSS易伤 【x】 秒
let EffectTargetToAddition_Kind = { };
EffectTargetToAddition_Kind[500] = PlayerAttrPart.Addition_Kind.Self;
EffectTargetToAddition_Kind[501] = PlayerAttrPart.Addition_Kind.Red;
EffectTargetToAddition_Kind[502] = PlayerAttrPart.Addition_Kind.Green;
EffectTargetToAddition_Kind[503] = PlayerAttrPart.Addition_Kind.Yellow;
EffectTargetToAddition_Kind[504] = PlayerAttrPart.Addition_Kind.Blue;
EffectTargetToAddition_Kind[505] = PlayerAttrPart.Addition_Kind.Remote;
EffectTargetToAddition_Kind[506] = PlayerAttrPart.Addition_Kind.Melee;
EffectTargetToAddition_Kind[507] = PlayerAttrPart.Addition_Kind.MyTeam;
EffectTargetToAddition_Kind[508] = PlayerAttrPart.Addition_Kind.EnemyTeam;
EffectTargetToAddition_Kind[509] = PlayerAttrPart.Addition_Kind.AirForce;
EffectTargetToAddition_Kind[510] = PlayerAttrPart.Addition_Kind.LandForce;
EffectTargetToAddition_Kind[511] = PlayerAttrPart.Addition_Kind.MyBoss;
EffectTargetToAddition_Kind[512] = PlayerAttrPart.Addition_Kind.EnemyBoss;
EffectTargetToAddition_Kind[513] = PlayerAttrPart.Addition_Kind.DungeonNormal;
EffectTargetToAddition_Kind[514] = PlayerAttrPart.Addition_Kind.DungeonBoss;
EffectTargetToAddition_Kind[515] = PlayerAttrPart.Addition_Kind.Self;
EffectTargetToAddition_Kind[516] = PlayerAttrPart.Addition_Kind.Master;
EffectTargetToAddition_Kind[517] = PlayerAttrPart.Addition_Kind.Ice;
EffectTargetToAddition_Kind[518] = PlayerAttrPart.Addition_Kind.Dragon;
EffectTargetToAddition_Kind[519] = PlayerAttrPart.Addition_Kind.Fire;
EffectTargetToAddition_Kind[520] = PlayerAttrPart.Addition_Kind.Grass;
EffectTargetToAddition_Kind[521] = PlayerAttrPart.Addition_Kind.Water;
EffectTargetToAddition_Kind[522] = PlayerAttrPart.Addition_Kind.Soil;
EffectTargetToAddition_Kind[523] = PlayerAttrPart.Addition_Kind.Electricity;
EffectTargetToAddition_Kind[524] = PlayerAttrPart.Addition_Kind.SuperPower;
EffectTargetToAddition_Kind[525] = PlayerAttrPart.Addition_Kind.Poison;
EffectTargetToAddition_Kind[526] = PlayerAttrPart.Addition_Kind.Fairy;
EffectTargetToAddition_Kind[527] = PlayerAttrPart.Addition_Kind.Wrestle;
EffectTargetToAddition_Kind[528] = PlayerAttrPart.Addition_Kind.Steel;
EffectTargetToAddition_Kind[529] = PlayerAttrPart.Addition_Kind.Rock;
EffectTargetToAddition_Kind[530] = PlayerAttrPart.Addition_Kind.Worm;
EffectTargetToAddition_Kind[531] = PlayerAttrPart.Addition_Kind.Evil;
EffectTargetToAddition_Kind[532] = PlayerAttrPart.Addition_Kind.Ghost;
EffectTargetToAddition_Kind[533] = PlayerAttrPart.Addition_Kind.Ordinary;
EffectTargetToAddition_Kind[534] = PlayerAttrPart.Addition_Kind.Float;
EffectTargetToAddition_Kind[535] = PlayerAttrPart.Addition_Kind.GuildTeam;
EffectTargetToAddition_Kind[536] = PlayerAttrPart.Addition_Kind.GuildBoss;
EffectTargetToAddition_Kind[537] = PlayerAttrPart.Addition_Kind.GuildEnemyTeam;
EffectTargetToAddition_Kind[538] = PlayerAttrPart.Addition_Kind.GuildEnemyBoss;
EffectTargetToAddition_Kind[540] = PlayerAttrPart.Addition_Kind.Key;
let FightRaceTypeToAddition_Kind = { };
FightRaceTypeToAddition_Kind[FightRaceType.Ice] = PlayerAttrPart.Addition_Kind.Ice;
FightRaceTypeToAddition_Kind[FightRaceType.Dragon] = PlayerAttrPart.Addition_Kind.Dragon;
FightRaceTypeToAddition_Kind[FightRaceType.Fire] = PlayerAttrPart.Addition_Kind.Fire;
FightRaceTypeToAddition_Kind[FightRaceType.Grass] = PlayerAttrPart.Addition_Kind.Grass;
FightRaceTypeToAddition_Kind[FightRaceType.Water] = PlayerAttrPart.Addition_Kind.Water;
FightRaceTypeToAddition_Kind[FightRaceType.Soil] = PlayerAttrPart.Addition_Kind.Soil;
FightRaceTypeToAddition_Kind[FightRaceType.Electricity] = PlayerAttrPart.Addition_Kind.Electricity;
FightRaceTypeToAddition_Kind[FightRaceType.SuperPower] = PlayerAttrPart.Addition_Kind.SuperPower;
FightRaceTypeToAddition_Kind[FightRaceType.Poison] = PlayerAttrPart.Addition_Kind.Poison;
FightRaceTypeToAddition_Kind[FightRaceType.Fairy] = PlayerAttrPart.Addition_Kind.Fairy;
FightRaceTypeToAddition_Kind[FightRaceType.Wrestle] = PlayerAttrPart.Addition_Kind.Wrestle;
FightRaceTypeToAddition_Kind[FightRaceType.Steel] = PlayerAttrPart.Addition_Kind.Steel;
FightRaceTypeToAddition_Kind[FightRaceType.Rock] = PlayerAttrPart.Addition_Kind.Rock;
FightRaceTypeToAddition_Kind[FightRaceType.Worm] = PlayerAttrPart.Addition_Kind.Worm;
FightRaceTypeToAddition_Kind[FightRaceType.Evil] = PlayerAttrPart.Addition_Kind.Evil;
FightRaceTypeToAddition_Kind[FightRaceType.Ghost] = PlayerAttrPart.Addition_Kind.Ghost;
FightRaceTypeToAddition_Kind[FightRaceType.Ordinary] = PlayerAttrPart.Addition_Kind.Ordinary;
//-----------------------------------------------------------初始化完毕
//工具函数转换FightSpecialAttr为Card_Fight_Type
PlayerAttrPart.ChangeFightSpecialAttrToCard_Fight_Type = function (fightSpecialAttr) {
	if ((fightSpecialAttr == FightSpecialAttr.Ice_Hurt)) {
		return [PlayerAttrPart.Card_Fight_Type.Ice_Add, PlayerAttrPart.Card_Fight_Type.Ice_Reduce];
		//受到冰攻击伤害增加
	} else if ((fightSpecialAttr == FightSpecialAttr.Dragon_Hurt) ){
		return [PlayerAttrPart.Card_Fight_Type.Dragon_Add, PlayerAttrPart.Card_Fight_Type.Dragon_Reduce];
		//受到龙攻击伤害增加
	} else if ((fightSpecialAttr == FightSpecialAttr.Fire_Hurt) ){
		return [PlayerAttrPart.Card_Fight_Type.Fire_Add, PlayerAttrPart.Card_Fight_Type.Fire_Reduce];
		//受到火攻击伤害增加
	} else if ((fightSpecialAttr == FightSpecialAttr.Grass_Hurt) ){
		return [PlayerAttrPart.Card_Fight_Type.Grass_Add, PlayerAttrPart.Card_Fight_Type.Grass_Reduce];
		//受到草攻击伤害增加
	} else if ((fightSpecialAttr == FightSpecialAttr.Water_Hurt) ){
		return [PlayerAttrPart.Card_Fight_Type.Water_Add, PlayerAttrPart.Card_Fight_Type.Water_Reduce];
		//受到水攻击伤害增加
	} else if ((fightSpecialAttr == FightSpecialAttr.Soil_Hurt) ){
		return [PlayerAttrPart.Card_Fight_Type.Soil_Add, PlayerAttrPart.Card_Fight_Type.Soil_Reduce];
		//受到地面攻击伤害增加
	} else if ((fightSpecialAttr == FightSpecialAttr.Electricity_Hurt) ){
		return [PlayerAttrPart.Card_Fight_Type.Electricity_Add, PlayerAttrPart.Card_Fight_Type.Electricity_Reduce];
		//受到电攻击伤害增加
	} else if ((fightSpecialAttr == FightSpecialAttr.SuperPower_Hurt) ){
		return [PlayerAttrPart.Card_Fight_Type.SuperPower_Add, PlayerAttrPart.Card_Fight_Type.SuperPower_Reduce];
		//受到超能力攻击伤害增加
	} else if ((fightSpecialAttr == FightSpecialAttr.Poison_Hurt) ){
		return [PlayerAttrPart.Card_Fight_Type.Poison_Add, PlayerAttrPart.Card_Fight_Type.Poison_Reduce];
		//受到毒攻击伤害增加
	} else if ((fightSpecialAttr == FightSpecialAttr.Fairy_Hurt) ){
		return [PlayerAttrPart.Card_Fight_Type.Fairy_Add, PlayerAttrPart.Card_Fight_Type.Fairy_Reduce];
		//受到妖精攻击伤害增加
	} else if ((fightSpecialAttr == FightSpecialAttr.Wrestle_Hurt) ){
		return [PlayerAttrPart.Card_Fight_Type.Wrestle_Add, PlayerAttrPart.Card_Fight_Type.Wrestle_Reduce];
		//受到格斗攻击伤害增加
	} else if ((fightSpecialAttr == FightSpecialAttr.Steel_Hurt) ){
		return [PlayerAttrPart.Card_Fight_Type.Steel_Add, PlayerAttrPart.Card_Fight_Type.Steel_Reduce];
		//受到钢攻击伤害增加
	} else if ((fightSpecialAttr == FightSpecialAttr.Rock_Hurt) ){
		return [PlayerAttrPart.Card_Fight_Type.Rock_Add, PlayerAttrPart.Card_Fight_Type.Rock_Reduce];
		//受到岩石攻击伤害增加
	} else if ((fightSpecialAttr == FightSpecialAttr.Worm_Hurt) ){
		return [PlayerAttrPart.Card_Fight_Type.Worm_Add, PlayerAttrPart.Card_Fight_Type.Worm_Reduce];
		//受到虫攻击伤害增加
	} else if ((fightSpecialAttr == FightSpecialAttr.Evil_Hurt) ){
		return [PlayerAttrPart.Card_Fight_Type.Evil_Add, PlayerAttrPart.Card_Fight_Type.Evil_Reduce];
		//受到恶攻击伤害增加
	} else if ((fightSpecialAttr == FightSpecialAttr.Ghost_Hurt) ){
		return [PlayerAttrPart.Card_Fight_Type.Ghost_Add, PlayerAttrPart.Card_Fight_Type.Ghost_Reduce];
		//受到幽灵攻击伤害增加
	} else if ((fightSpecialAttr == FightSpecialAttr.Ordinary_Hurt) ){
		return [PlayerAttrPart.Card_Fight_Type.Ordinary_Add, PlayerAttrPart.Card_Fight_Type.Ordinary_Reduce];
		//受到一般攻击伤害增加
	} else if ((fightSpecialAttr == FightSpecialAttr.EasyHurt) ){
		return [PlayerAttrPart.Card_Fight_Type.EasyHurt, PlayerAttrPart.Card_Fight_Type.EasyHurt];
	} else if ((fightSpecialAttr == FightSpecialAttr.RestoreHP) ){
		return [PlayerAttrPart.Card_Fight_Type.RestoreHP, PlayerAttrPart.Card_Fight_Type.RestoreHP];
	}
	return [PlayerAttrPart.Card_Fight_Type.Ice_Add, PlayerAttrPart.Card_Fight_Type.Ice_Reduce];
}
// 将FightAttr转换成Card_Fight_Type
PlayerAttrPart.ChangeFightAttrToCard_Fight_Type = function(fightAttr) {
	if ((fightAttr == FightAttr.ATK)) {
		return [PlayerAttrPart.Card_Fight_Type.ATK, PlayerAttrPart.Card_Fight_Type.ATK_Add, PlayerAttrPart.Card_Fight_Type.ATK_Reduce, PlayerAttrPart.Card_Fight_Type.ATK_Coef, PlayerAttrPart.Card_Fight_Type.ATK_Base];
	} else if ((fightAttr == FightAttr.HP) ){
		return [PlayerAttrPart.Card_Fight_Type.HP, PlayerAttrPart.Card_Fight_Type.HP_Add, PlayerAttrPart.Card_Fight_Type.HP_Reduce, PlayerAttrPart.Card_Fight_Type.HP_Coef, PlayerAttrPart.Card_Fight_Type.HP_Base];
	} else if ((fightAttr == FightAttr.DEF) ){
		return [PlayerAttrPart.Card_Fight_Type.DEF, PlayerAttrPart.Card_Fight_Type.DEF_Add, PlayerAttrPart.Card_Fight_Type.DEF_Reduce, PlayerAttrPart.Card_Fight_Type.DEF_Coef, PlayerAttrPart.Card_Fight_Type.DEF_Base];
	} else if ((fightAttr == FightAttr.MDEF) ){
		return [PlayerAttrPart.Card_Fight_Type.MDEF, PlayerAttrPart.Card_Fight_Type.MDEF_Add, PlayerAttrPart.Card_Fight_Type.MDEF_Reduce, PlayerAttrPart.Card_Fight_Type.MDEF_Coef, PlayerAttrPart.Card_Fight_Type.MDEF_Base];
	} else if ((fightAttr == FightAttr.ASPD) ){
		return [PlayerAttrPart.Card_Fight_Type.ASPD, PlayerAttrPart.Card_Fight_Type.ASPD_Add, PlayerAttrPart.Card_Fight_Type.ASPD_Reduce, PlayerAttrPart.Card_Fight_Type.ASPD_Coef, PlayerAttrPart.Card_Fight_Type.ASPD_Base];
	} else if ((fightAttr == FightAttr.Speed) ){
		return [PlayerAttrPart.Card_Fight_Type.Speed, PlayerAttrPart.Card_Fight_Type.Speed_Add, PlayerAttrPart.Card_Fight_Type.Speed_Reduce, PlayerAttrPart.Card_Fight_Type.Speed_Coef, PlayerAttrPart.Card_Fight_Type.Speed_Base];
	} else if ((fightAttr == FightAttr.Range) ){
		return [PlayerAttrPart.Card_Fight_Type.Range, PlayerAttrPart.Card_Fight_Type.Range_Add, PlayerAttrPart.Card_Fight_Type.Range_Reduce, PlayerAttrPart.Card_Fight_Type.Range_Coef, PlayerAttrPart.Card_Fight_Type.Range_Base];
	} else if ((fightAttr == FightAttr.CRI) ){
		return [PlayerAttrPart.Card_Fight_Type.CRI, PlayerAttrPart.Card_Fight_Type.CRI_Add, PlayerAttrPart.Card_Fight_Type.CRI_Reduce, PlayerAttrPart.Card_Fight_Type.CRI_Coef, PlayerAttrPart.Card_Fight_Type.CRI_Base];
	} else if ((fightAttr == FightAttr.CritDamage) ){
		return [PlayerAttrPart.Card_Fight_Type.CritDamage, PlayerAttrPart.Card_Fight_Type.CritDamage_Add, PlayerAttrPart.Card_Fight_Type.CritDamage_Reduce, PlayerAttrPart.Card_Fight_Type.CritDamage_Coef, PlayerAttrPart.Card_Fight_Type.CritDamage_Base];
	} else if ((fightAttr == FightAttr.HitRate) ){
		return [PlayerAttrPart.Card_Fight_Type.HitRate, PlayerAttrPart.Card_Fight_Type.HitRate_Add, PlayerAttrPart.Card_Fight_Type.HitRate_Reduce, PlayerAttrPart.Card_Fight_Type.HitRate_Coef, PlayerAttrPart.Card_Fight_Type.HitRate_Base];
	} else if ((fightAttr == FightAttr.DodgeRate) ){
		return [PlayerAttrPart.Card_Fight_Type.DodgeRate, PlayerAttrPart.Card_Fight_Type.DodgeRate_Add, PlayerAttrPart.Card_Fight_Type.DodgeRate_Reduce, PlayerAttrPart.Card_Fight_Type.DodgeRate_Coef, PlayerAttrPart.Card_Fight_Type.DodgeRate_Base];
	} else if ((fightAttr == FightAttr.Respawn) ){
		return [PlayerAttrPart.Card_Fight_Type.Respawn, PlayerAttrPart.Card_Fight_Type.Respawn_Add, PlayerAttrPart.Card_Fight_Type.Respawn_Reduce, PlayerAttrPart.Card_Fight_Type.Respawn_Coef, PlayerAttrPart.Card_Fight_Type.Respawn_Base];
	} else if ((fightAttr == FightAttr.SkillRange) ){
		return[PlayerAttrPart.Card_Fight_Type.Range, PlayerAttrPart.Card_Fight_Type.Range_Add, PlayerAttrPart.Card_Fight_Type.Range_Reduce, PlayerAttrPart.Card_Fight_Type.Range_Coef, PlayerAttrPart.Card_Fight_Type.Range_Base];
	} else if ((fightAttr == FightAttr.AttackRange) ){
		return [PlayerAttrPart.Card_Fight_Type.Range, PlayerAttrPart.Card_Fight_Type.Range_Add, PlayerAttrPart.Card_Fight_Type.Range_Reduce, PlayerAttrPart.Card_Fight_Type.Range_Coef, PlayerAttrPart.Card_Fight_Type.Range_Base];
	} else if ((fightAttr == FightAttr.SkillAttackRange) ){
		return [PlayerAttrPart.Card_Fight_Type.Range, PlayerAttrPart.Card_Fight_Type.Range_Add, PlayerAttrPart.Card_Fight_Type.Range_Reduce, PlayerAttrPart.Card_Fight_Type.Range_Coef, PlayerAttrPart.Card_Fight_Type.Range_Base];
	} else {
		return [PlayerAttrPart.Card_Fight_Type.ATK, PlayerAttrPart.Card_Fight_Type.ATK_Add, PlayerAttrPart.Card_Fight_Type.ATK_Reduce, PlayerAttrPart.Card_Fight_Type.ATK_Coef, PlayerAttrPart.Card_Fight_Type.ATK_Base];
	}
}
// 将Card_Fight_Type转换成FightAttr
PlayerAttrPart.ChangeCard_Fight_TypeToFightAttr = function(card_Fight_Type) {
	if (((((PlayerAttrPart.Card_Fight_Type.ATK == card_Fight_Type) || (PlayerAttrPart.Card_Fight_Type.ATK_Add == card_Fight_Type)) || (PlayerAttrPart.Card_Fight_Type.ATK_Reduce == card_Fight_Type)) || (PlayerAttrPart.Card_Fight_Type.ATK_Coef == card_Fight_Type))) {
		return FightAttr.ATK;
	} else if (((((PlayerAttrPart.Card_Fight_Type.HP == card_Fight_Type) || (PlayerAttrPart.Card_Fight_Type.HP_Add == card_Fight_Type)) || (PlayerAttrPart.Card_Fight_Type.HP_Reduce == card_Fight_Type)) || (PlayerAttrPart.Card_Fight_Type.HP_Coef == card_Fight_Type)) ){
		return FightAttr.HP;
	} else if (((((PlayerAttrPart.Card_Fight_Type.DEF == card_Fight_Type) || (PlayerAttrPart.Card_Fight_Type.DEF_Add == card_Fight_Type)) || (PlayerAttrPart.Card_Fight_Type.DEF_Reduce == card_Fight_Type)) || (PlayerAttrPart.Card_Fight_Type.DEF_Coef == card_Fight_Type)) ){
		return FightAttr.DEF;
	} else if (((((PlayerAttrPart.Card_Fight_Type.MDEF == card_Fight_Type) || (PlayerAttrPart.Card_Fight_Type.MDEF_Add == card_Fight_Type)) || (PlayerAttrPart.Card_Fight_Type.MDEF_Reduce == card_Fight_Type)) || (PlayerAttrPart.Card_Fight_Type.MDEF_Coef == card_Fight_Type)) ){
		return FightAttr.MDEF;
	} else if (((((PlayerAttrPart.Card_Fight_Type.ASPD == card_Fight_Type) || (PlayerAttrPart.Card_Fight_Type.ASPD_Add == card_Fight_Type)) || (PlayerAttrPart.Card_Fight_Type.ASPD_Reduce == card_Fight_Type)) || (PlayerAttrPart.Card_Fight_Type.ASPD_Coef == card_Fight_Type)) ){
		return FightAttr.ASPD;
	} else if (((((PlayerAttrPart.Card_Fight_Type.Speed == card_Fight_Type) || (PlayerAttrPart.Card_Fight_Type.Speed_Add == card_Fight_Type)) || (PlayerAttrPart.Card_Fight_Type.Speed_Reduce == card_Fight_Type)) || (PlayerAttrPart.Card_Fight_Type.Speed_Coef == card_Fight_Type)) ){
		return FightAttr.Speed;
	} else if (((((PlayerAttrPart.Card_Fight_Type.Range == card_Fight_Type) || (PlayerAttrPart.Card_Fight_Type.Range_Add == card_Fight_Type)) || (PlayerAttrPart.Card_Fight_Type.Range_Reduce == card_Fight_Type)) || (PlayerAttrPart.Card_Fight_Type.Range_Coef == card_Fight_Type)) ){
		return FightAttr.Range;
	} else if (((((PlayerAttrPart.Card_Fight_Type.CRI == card_Fight_Type) || (PlayerAttrPart.Card_Fight_Type.CRI_Add == card_Fight_Type)) || (PlayerAttrPart.Card_Fight_Type.CRI_Reduce == card_Fight_Type)) || (PlayerAttrPart.Card_Fight_Type.CRI_Coef == card_Fight_Type)) ){
		return FightAttr.CRI;
	} else if (((((PlayerAttrPart.Card_Fight_Type.CritDamage == card_Fight_Type) || (PlayerAttrPart.Card_Fight_Type.CritDamage_Add == card_Fight_Type)) || (PlayerAttrPart.Card_Fight_Type.CritDamage_Reduce == card_Fight_Type)) || (PlayerAttrPart.Card_Fight_Type.CritDamage_Coef == card_Fight_Type)) ){
		return FightAttr.CritDamage;
	} else if (((((PlayerAttrPart.Card_Fight_Type.HitRate == card_Fight_Type) || (PlayerAttrPart.Card_Fight_Type.HitRate_Add == card_Fight_Type)) || (PlayerAttrPart.Card_Fight_Type.HitRate_Reduce == card_Fight_Type)) || (PlayerAttrPart.Card_Fight_Type.HitRate_Coef == card_Fight_Type)) ){
		return FightAttr.HitRate;
	} else if (((((PlayerAttrPart.Card_Fight_Type.DodgeRate == card_Fight_Type) || (PlayerAttrPart.Card_Fight_Type.DodgeRate_Add == card_Fight_Type)) || (PlayerAttrPart.Card_Fight_Type.DodgeRate_Reduce == card_Fight_Type)) || (PlayerAttrPart.Card_Fight_Type.DodgeRate_Coef == card_Fight_Type)) ){
		return FightAttr.DodgeRate;
	} else if (((((PlayerAttrPart.Card_Fight_Type.Respawn == card_Fight_Type) || (PlayerAttrPart.Card_Fight_Type.Respawn_Add == card_Fight_Type)) || (PlayerAttrPart.Card_Fight_Type.Respawn_Reduce == card_Fight_Type)) || (PlayerAttrPart.Card_Fight_Type.Respawn_Coef == card_Fight_Type)) ){
		return FightAttr.Respawn;
	} else if (((((PlayerAttrPart.Card_Fight_Type.Range == card_Fight_Type) || (PlayerAttrPart.Card_Fight_Type.Range_Add == card_Fight_Type)) || (PlayerAttrPart.Card_Fight_Type.Range_Reduce == card_Fight_Type)) || (PlayerAttrPart.Card_Fight_Type.Range_Coef == card_Fight_Type)) ){
		return FightAttr.SkillRange;
	} else if (((((PlayerAttrPart.Card_Fight_Type.Range == card_Fight_Type) || (PlayerAttrPart.Card_Fight_Type.Range_Add == card_Fight_Type)) || (PlayerAttrPart.Card_Fight_Type.Range_Reduce == card_Fight_Type)) || (PlayerAttrPart.Card_Fight_Type.Range_Coef == card_Fight_Type)) ){
		return FightAttr.AttackRange;
	} else if (((((PlayerAttrPart.Card_Fight_Type.Range == card_Fight_Type) || (PlayerAttrPart.Card_Fight_Type.Range_Add == card_Fight_Type)) || (PlayerAttrPart.Card_Fight_Type.Range_Reduce == card_Fight_Type)) || (PlayerAttrPart.Card_Fight_Type.Range_Coef == card_Fight_Type)) ){
		return FightAttr.SkillAttackRange;
	} else {
		return FightAttr.ATK;
	}
}
//将Addition_Master_Type转换成EffectID
PlayerAttrPart.ChangeMasterType = function(typ) {
	let value = Addition_Master_TypeToEffectID[typ];
	if ((value == null)) {
		console.error(("PlayerAttrPart_ChangeMasterType错误：" + typ));
		return 0;
	}
	return value;
}
//根据效果ID获取主角属性枚举
PlayerAttrPart.ChangeEffectIDToMasterType = function(effectID) {
	let value = EffectIDToAddition_Master_Type[effectID];
	if ((value == null)) {
		console.error(("ChangeEffectIDToMasterType 转换错误 effectID:" + effectID));
		return PlayerAttrPart.Addition_Master_Type.Max;
	}
	return value;
}
//根据效果ID获取战斗属性枚举
PlayerAttrPart.ChangeEffectIDToCard_Fight_Type = function(effectID) {
	let value = EffectIDToCard_Fight_Type[effectID];
	if ((value == null)) {
		console.error(("ChangeEffectIDToCard_Fight_Type转换错误 effectID" + effectID));
		return PlayerAttrPart.Card_Fight_Type.ATK;
	}
	return value;
}
PlayerAttrPart.ChangeCard_Fight_TypeToEffectID = function(card_Fight_Type) {
	let value = Card_Fight_TypeToEffectID[card_Fight_Type];
	if ((value == null)) {
		console.error("ChangeCard_Fight_TypeToEffectID转换错误 card_Fight_Type", card_Fight_Type);
		return 1;
	}
	return value;
};
//转换effectTarget
PlayerAttrPart.ChangeEffectTarget = function(effectTarget) {
	let value = EffectTargetToAddition_Kind[effectTarget];
	if ((value == null)) {
		console.error("ChangeEffectTarget转换错误 effectTarget", effectTarget);
		return PlayerAttrPart.Addition_Kind.Max;
	}
	return value;
};
PlayerAttrPart.ChangeFightRaceTypeToAddition_Kind = function(typ) {
	let value = FightRaceTypeToAddition_Kind[typ];
	if ((value == null)) {
		console.error("ChangeFightRaceTypeToAddition_Kind转换错误 typ", typ);
		return PlayerAttrPart.Addition_Kind.Ice;
	}
	return value;
}
//算上钥石的加成
PlayerAttrPart.GetBaseCard = function(cardID) {
	let conf_Card_Data = LoadConfig.getConfigData(ConfigName.ShiBing,cardID);
	if ((conf_Card_Data == null)) {
		return null;
	}
	//怪物ID不加成 大于1600000
	if ((cardID > 1600000)) {
		return conf_Card_Data;
	}
	let new_Data = DeepCopy(conf_Card_Data);
	//获取钥石基础属性加成
	let addition = PlayerAttrPart.StoneBaseAttrs[Math.floor((cardID / 10))];
	//获取钥石的阵营加成
	let kind = PlayerAttrPart.ChangeFightRaceTypeToAddition_Kind(new_Data.Race);
	let raceAddition = PlayerAttrPart.GetOtherAttr(PlayerAttrPart.Source.MegaEvolution, kind);
	let totalAddition = new CardAddition();
	totalAddition.AddCardAddition(addition);
	totalAddition.AddCardAddition(raceAddition);
	for (let i = FightAttr.ATK; i != (FightAttr.Max - 1); ++i) {
		//这三类不用加成
		if ((((i != FightAttr.MaxHP) && (i != FightAttr.Vision)) && (i != FightAttr.Anger))) {
			const [add, _a, _b, _c, baseAdd] = PlayerAttrPart.ChangeFightAttrToCard_Fight_Type(i);
			
			let addBig = totalAddition.Get(add);
			let baseAddBig = totalAddition.Get(baseAdd);
			// 双防御
			if (((i == FightAttr.DEF) || (i == FightAttr.MDEF))) {
				let doubleDefAddBig = totalAddition.Get(PlayerAttrPart.Card_Fight_Type.DoubleDef);
				let doubleDefBaseAddBig = totalAddition.Get(PlayerAttrPart.Card_Fight_Type.DoubleDef_Base);
				addBig = BigNumber.add(addBig, doubleDefAddBig);
				baseAddBig = BigNumber.add(baseAddBig, doubleDefBaseAddBig);
			}
			//攻防血
			if (((((i == FightAttr.DEF) || (i == FightAttr.MDEF)) || (i == FightAttr.ATK)) || (i == FightAttr.HP))) {
				let adhAddBig = totalAddition.Get(PlayerAttrPart.Card_Fight_Type.ADH);
				let adhBaseAddBig = totalAddition.Get(PlayerAttrPart.Card_Fight_Type.ADH_Base);
				addBig = BigNumber.add(addBig, adhAddBig);
				baseAddBig = BigNumber.add(baseAddBig, adhBaseAddBig);
			}
			let value = PlayerAttrPart.GetAttrInShiBingConf(new_Data, i);
			if ((value != null)) {
				// 固定值
				value = BigNumber.add(value, addBig);
				//固定值加成
				let rate = BigNumber.add(baseAddBig, BigNumber.tenThousand);
				let result = BigNumber.mul(value, rate);
				value = BigNumber.div(result, BigNumber.tenThousand);
				PlayerAttrPart.SetAttrInShiBingConf(new_Data, i, value);
			}
		}
	}
	//返回结果
	return new_Data;
}
PlayerAttrPart.GetAttrInShiBingConf = function(data, fightAttr) {
	if ((fightAttr == FightAttr.ATK)) {
		return data.ATK;
	} else if ((fightAttr == FightAttr.HP) ){
		return data.HP;
	} else if ((fightAttr == FightAttr.DEF) ){
		return data.DEF;
	} else if ((fightAttr == FightAttr.MDEF) ){
		return data.MDEF;
	}
	return null;
}
PlayerAttrPart.SetAttrInShiBingConf = function(data, fightAttr, value) {
	if ((fightAttr == FightAttr.ATK)) {
		data.ATK = value;
	} else if ((fightAttr == FightAttr.HP) ){
		data.HP = value;
	} else if ((fightAttr == FightAttr.DEF) ){
		data.DEF = value;
	} else if ((fightAttr == FightAttr.MDEF) ){
		data.MDEF = value;
	}
}
PlayerAttrPart.MulRate = function(value, rate) {
	rate = BigNumber.add(rate, BigNumber.tenThousand);
	let result = BigNumber.mul(value, rate);
	return BigNumber.div(result, BigNumber.tenThousand);
}
//------------------------根据卡牌ID 获取卡牌当前的属性ID进度百分比-----------------------------
PlayerAttrPart.GetCardPropertyProcess = function(cardid) {
	let cardData = LoadConfig.getConfigData(ConfigName.ShiBing,cardid);
	if ((cardData == null)) {
		return "卡牌不存在";
	}
	//查找到卡牌的属性ID
	let mEnumID = 0;
	let Conf_ShuZhiShuXingDingYi = LoadConfig.getConfig(ConfigName.ShuZhiShuXingDingYi);
	for (let k in Conf_ShuZhiShuXingDingYi) {
		let v = Conf_ShuZhiShuXingDingYi[k];
		{
			if ((v.CardID == cardid)) {
				mEnumID = k;
				break;
			}
		}
	}
	if ((mEnumID == 0)) {
		return "error";
	}
	//计算当前收集进度 
	let conf_Data = LoadConfig.getConfigData(ConfigName.ShuZhiShuXingDingYi,mEnumID);
	if ((conf_Data != null)) {
		let transEnum = PlayerAttrPart.ChangeEffectIDToMasterType(mEnumID);
		let currentPercent = PlayerAttrPart.GetCollectSingleSummaryAttr(transEnum);
		let percent = ((parseInt(currentPercent) / parseInt(conf_Data.Max)) * 100);
		percent = Math.floor(percent);
		return (percent.toString() + "%");
	} else {
		return ("属性定义表为空" + mEnumID);
	}
}
//-----------------------根据卡牌ID 获取当前卡牌的收集属性值和上限--------------------------------------
PlayerAttrPart.GetCardCollectValue = function(cardid) {
	let cardData = LoadConfig.getConfigData(ConfigName.ShiBing,cardid);
	if ((cardData == null)) {
		console.error(("卡牌ID空" + cardid));
		return "0";
	}
	//查找到卡牌的属性ID
	let mEnumID = 0;
	let Conf_ShuZhiShuXingDingYi = LoadConfig.getConfig(ConfigName.ShuZhiShuXingDingYi);
	for (let k in Conf_ShuZhiShuXingDingYi) {
		let v = Conf_ShuZhiShuXingDingYi[k];
		{
			if ((v.CardID == cardid)) {
				mEnumID = k;
				break;
			}
		}
	}
	if ((mEnumID == 0)) {
		console.error(("属性定义表为空" + mEnumID));
		return "0";
	}
	//计算当前收集进度 
	let conf_Data = LoadConfig.getConfigData(ConfigName.ShuZhiShuXingDingYi,mEnumID);
	if ((conf_Data != null)) {
		let transEnum = PlayerAttrPart.ChangeEffectIDToMasterType(mEnumID);
		let currentPercent = PlayerAttrPart.GetCollectSingleSummaryAttr(transEnum);
		return currentPercent;
	} else {
		console.error(("属性定义表为空" + mEnumID));
		return "0";
	}
}
//endregion
window.PlayerAttrPart = PlayerAttrPart;