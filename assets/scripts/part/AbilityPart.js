import ViewManager from "../manager/ViewManager";

//文件定义：处理玩家的主动技能信息
//设计要点：
//1、技能卡槽的设置功能（添加，替换）
//2、技能卡槽的购买功能
//3、技能的使用功能
//4、技能的购买功能
//负责人：陈林辉
//评分：
//移交记录：

let ability_msg_pb = require('ability_msg').msg;
let AbilityPart = { };
//主动技部件消息码
AbilityPart.Result = {
	Success: 0,
	//成功
	NoExist: 1,
	//技能未获得
	CDing: 2,
	//技能CD中
	HasOpen: 3,
	//技能已开启
	NotOpen: 4,
	//技能未开启
	ZuanShiError: 5,
	//钻石不足
	Exist: 6,
	//技能已设置
	Error: 7,
	//技能不存在错误
	SkillFull: 8,
	//技能全部获取

};
AbilityPart.AllSkills = { };
// 当前已经获取的主动技
AbilityPart.SkillGrooves = { };
// 当前主动技卡槽的信息
AbilityPart.BuffCards = { };
// 主动技影响的卡牌ID
AbilityPart.CacheUseSkill = { };
// 没有在战斗的时候请求主动技返回会失效，这里存起来，在战斗的时候调用
// 消息码注释：
//  AbilityPart.SkillGrooves是AbilityPart_GrooveData数组
//  message AbilityPart_GrooveData{
//	required int32 Position 		= 1;//卡槽下标
//	required int32 SkillID			= 2;//技能ID       0就是加号
//	required int64 LastUsingTime	= 3;//上次使用时间
//	required bool  IsOpen 			= 4;//是否已购买
//}
//AbilityPart.AllSkills是技能ID数组
//登陆下发
AbilityPart.OnLoginRsponse = function(buffer) {
	var msg = ability_msg_pb.AbilityPart_UpdateDataResponse.decode(buffer);
	//所有已经获取的技能ID
	AbilityPart.AllSkills = { };
	for (var i = 0; i < msg.AllSkills.length; ++i) {
		var skillID = msg.AllSkills[i];
		AbilityPart.AllSkills[skillID] = true;
	}
	//主动技能卡槽信息
	AbilityPart.SkillGrooves = msg.SkillGrooves;
}
//请求设置主动技能卡槽（添加，替换）
AbilityPart.OnSetGrooveRequest = function(position, skillID) {
	ViewManager.showLoading({});
	var request = new ability_msg_pb.AbilityPart_SetGrooveRequest();
	request.Position = position;
	request.SkillID = skillID;
	SocketClient.callServerFun('AbilityPart_OnSetGrooveRequest', request);
}
//请求购买主动技能卡槽
AbilityPart.OnBuyGrooveRequest = function(position) {
	ViewManager.showLoading({});
	var request = new ability_msg_pb.AbilityPart_BuyGrooveRequest();
	request.Position = position;
	SocketClient.callServerFun('AbilityPart_OnBuyGrooveRequest', request);
}
//请求设置主动技能卡槽（添加，替换）返回
//请求购买主动技能卡槽返回
//技能卡槽等级开放返回
AbilityPart.OnGrooveUpdateResponse = function(buffer) {
	ViewManager.hideLoading();
	var msg = ability_msg_pb.AbilityPart_UpdateGrooveResponse.decode(buffer);
	var err = msg.Success;
	//错误，显示提示后返回
	if ((err != AbilityPart.Result.Success)) {
		AbilityPart.ShowErrorTips(err);
		return;
	}
	//刷新卡槽信息
	var data = msg.Data;
	AbilityPart.SkillGrooves[data.Position] = data;
	//刷新界面
	//SkillSetPopPanel.UpdataPanelInfo();
	//AbilityPrefabCreateor.UpdateAll();
	//判断是否是设置主动技
	if ((data.SkillID > 0)) {
		FightBehaviorManager.OnActiveSkillChange(data.SkillID);
	}
}
//请求使用主动技能
AbilityPart.OnUseSkillRequest = function(position) {
	ViewManager.showLoading({});
	var request = new ability_msg_pb.AbilityPart_UseSkillRequest();
	request.Position = position;
	SocketClient.callServerFun('AbilityPart_OnUseSkillRequest', request);
}
//请求使用主动技能返回
AbilityPart.OnUseSkillResponse = function(buffer) {
	ViewManager.hideLoading();
	var msg = ability_msg_pb.AbilityPart_UpdateGrooveResponse.decode(buffer);
	var err = msg.Success;
	//错误，显示提示后返回
	if ((err != AbilityPart.Result.Success)) {
		AbilityPart.ShowErrorTips(err);
		return;
	}
	//刷新卡槽信息
	var data = msg.Data;
	AbilityPart.SkillGrooves[data.Position] = data;
	//刷新界面
	SkillSetPopPanel.UpdataPanelInfo();
	AbilityPrefabCreateor.UpdateAll();
	if (FightBehaviorManager.IsEnd) {
		AbilityPart.CacheUseSkill[data.SkillID] = true;
	} else {
		//使用主动技，使用的时候Buff持续时间传入0
		GFightManager.UseActiveSkill(data.SkillID, 0);
	}
}
//开始战斗，检查是否有未使用的主动技
AbilityPart.CheckNotEffectAbility = function() {
	//timerMgr.RemoveTimerEvent("AbilityPart.CheckNotEffectAbilityTimer");
	//timerMgr.AddTimerEvent('AbilityPart.CheckNotEffectAbilityTimer', 0.1, AbilityPart.CheckNotEffectAbilityTimer, { }, false);
}
AbilityPart.CheckNotEffectAbilityTimer = function() {
	for (var skillID in (AbilityPart.CacheUseSkill)) {
		
		GFightManager.UseActiveSkill(skillID, 0);
	}
	AbilityPart.CacheUseSkill = { };
}
//请求购买技能
AbilityPart.OnBuySkillRequest = function() {
	ViewManager.showLoading({});
	var request = new ability_msg_pb.AbilityPart_BuySkillRequest();
	SocketClient.callServerFun('AbilityPart_OnBuySkillRequest', request);
}
//请求购买技能返回
AbilityPart.OnBuySkillResponse = function(buffer) {
	ViewManager.hideLoading();
	var msg = ability_msg_pb.AbilityPart_BuySkillResponse.decode(buffer);
	var err = msg.Success;
	//错误，显示提示后返回
	if ((err != AbilityPart.Result.Success)) {
		AbilityPart.ShowErrorTips(err);
		return;
	}
	var exist = msg.Exist;
	//是否已经存在
	var skillID = msg.SkillID;
	//获取的技能ID
	var resourceType = msg.ResourceType;
	//如果已经存在，转换成的资源
	var resourceID = msg.ResourceID;
	//如果已经存在，转换成的资源
	var resourceCount = msg.ResourceCount;
	//如果已经存在，转换成的资源
	if (!exist) {
		// 刷新技能列表
		AbilityPart.AllSkills[msg.SkillID] = true;
	}
	//显示抽取的界面
	//GetSkillPopPanel.Show(msg);
	SkillSetPopPanel.ChouQuJiNengEffect(msg);
}
//后端新增主动技能信息下发
AbilityPart.OnTriggerSkill = function(buffer) {
	var msg = ability_msg_pb.AbilityPart_UpdateTotalSkillIDResponse.decode(buffer);
	for (var i = 0; i != msg.AllSkills.length; ++i) {
		var addSkillID = msg.AllSkills[i];
		AbilityPart.AllSkills[addSkillID] = true;
	}
	//msg.AllSkills新增的主动信息ID数组，弹出主动技新增界面
	AbilityPart.YanChiShowSkillJiHuo(msg.AllSkills[0]);
}
//延迟2秒调用激活界面
AbilityPart.YanChiShowSkillJiHuo = function(id) {
	// timerMgr.RemoveTimerEvent("AbilityPart.YanChiSkill");
	// timerMgr.AddTimerEvent('AbilityPart.YanChiSkill', 2.0, AbilityPart.YanChiSkill, [
	// 		id
	// 	], false);
}
AbilityPart.YanChiSkill = function(param) {
	ActivatingSkillPopPanel.Show(param[0]);
}
//判断主动技是否存在
AbilityPart.ExistSkill = function(id) {
	return (AbilityPart.AllSkills[id] || false);
}
// 关卡战斗开启时，检测是否有持续的主动技Buff需要加载
AbilityPart.CheckBuff = function() {
	GFightManager.SetBornBuffTab(AbilityPart.BuffCards);
	AbilityPart.BuffCards = { };
}
//设置主动技影响的卡牌
AbilityPart.SetBuffCards = function(buffCards) {
	AbilityPart.BuffCards = buffCards;
}
// 获取一个技能卡槽的CD剩余时间，小于0说明没有在CD，
// Update中调用该函数显示
AbilityPart.GetCDTime = function(position) {
	var data = AbilityPart.SkillGrooves[position];
	if ((data == null)) {
		return 0;
	}
	var conf_Skill_Data = LoadConfig.getConfigData(ConfigName.JiNeng_ZhuDong,data.SkillID);
	if ((conf_Skill_Data == null)) {
		return 0;
	}
	//已经流逝的时间
	var elapsedTime = (TimePart.GetUnix() - parseInt(data.LastUsingTime));
	return (data.CDTime - elapsedTime);
}
//获取一个主动技能的Buff剩余时间
//如果小于等于0，说明没有持续的buff
AbilityPart.GetBuffTime = function(position) {
	var data = AbilityPart.SkillGrooves[position];
	if ((data == null)) {
		return 0;
	}
	var conf_Skill_Data = LoadConfig.getConfigData(ConfigName.JiNeng_ZhuDong,data.SkillID);
	if ((conf_Skill_Data == null)) {
		return 0;
	}
	//Buff持续时间
	var duration = conf_Skill_Data.Duration;
	//已经流逝的时间
	var elapsedTime = (TimePart.GetUnix() - parseInt(data.LastUsingTime));
	if ((elapsedTime > duration)) {
		return 0;
	}
	return (duration - elapsedTime);
}
//判断一个技能是否在使用中
//给界面显示使用
AbilityPart.IsUsing = function(position) {
	var data = AbilityPart.SkillGrooves[position];
	if ((data == null)) {
		return false;
	}
	//获取技能配置
	var conf_Skill_Data = LoadConfig.getConfigData(ConfigName.JiNeng_ZhuDong,data.SkillID);
	if ((conf_Skill_Data == null)) {
		return false;
	}
	//瞬发不需要显示使用中
	if ((conf_Skill_Data.IsInstant == 0)) {
		return false;
	}
	//获取技能表现配置
	var conf_Skill_BiaoXian_Data = LoadConfig.getConfigData(ConfigName.JiNeng_ZhuDong_BiaoXian,data.SkillID);
	if ((conf_Skill_BiaoXian_Data == null)) {
		return false;
	}
	//判断流逝的时间是否超过技能表现的时间
	return (((TimePart.GetUnix() - parseInt(data.LastUsingTime))) < conf_Skill_BiaoXian_Data.Duration);
}
//判断技能是否已设置
AbilityPart.HasSet = function(skillID) {
	//遍历技能卡槽信息判断技能ID是否相同
	for (var i = 0; i != AbilityPart.SkillGrooves.length; ++i) {
		var groove = AbilityPart.SkillGrooves[i];
		if ((groove.SkillID == skillID)) {
			return true;
		}
	}
	return false;
};
//判断是否已经获取所有技能
AbilityPart.IsGetTotal = function() {
	return (AbilityPart.AllSkills.length == Conf_JiNeng_ZhuDong.length);
};
//弹出错误提示
AbilityPart.ShowErrorTips = function(err) {
	if ((err == AbilityPart.Result.NoExist)) {
		ToolTipPopPanel.Show(219);
	} else if ((err == AbilityPart.Result.CDing) ){
		ToolTipPopPanel.Show(220);
	} else if ((err == AbilityPart.Result.HasOpen) ){
		ToolTipPopPanel.Show(222);
	} else if ((err == AbilityPart.Result.NotOpen) ){
		ToolTipPopPanel.Show(223);
	} else if ((err == AbilityPart.Result.ZuanShiError) ){
		//钻石不足
		ToolTipPopPanel.Show(211);
	} else if ((err == AbilityPart.Result.Exist) ){
		ToolTipPopPanel.Show(224);
	} else if ((err == AbilityPart.Result.Error) ){
		ToolTipPopPanel.Show(225);
	} else if ((err == AbilityPart.Result.SkillFull) ){
		//所有技能已抽完
		ToolTipPopPanel.Show(226);
	}
};
window.AbilityPart = AbilityPart;
//endregion
