let megaevo_msg_pb = require('megaevo_msg').msg;
let MegaEvoPart = { };

//状态枚举
MegaEvoPart.StateEnum = {
	HasCom: 1,
	//已合成
	CanCom: 2,
	//可合成
	NoneCom: 3,
	//不可合成

};
//钥石信息
MegaEvoPart.StoneList = { };
//前端刷新用的缓存假数据
MegaEvoPart.FakeDataList = { };
var ENHANCEMAXLEVEL = 50;
//接受钥石数据
MegaEvoPart.RecStoneData = function(buffer) {
	var msg = megaevo_msg_pb.MegaEvoPart_StoneDataResponse.decode(buffer);

	ViewManager.hideLoading();
	MegaEvoPart.StoneList = msg.StoneDatas;
}
//请求钥石数据
MegaEvoPart.StoneDataRequest = function() {
	var request = new  megaevo_msg_pb.MegaEvoPart_StoneDataRequest();
	ViewManager.showLoading({});
	SocketClient.callServerFun('MegaEvoPart_SendStoneDatas', request);
}
//数据返回
MegaEvoPart.StoneDataResponse = function(buffer) {
	var msg = megaevo_msg_pb.MegaEvoPart_StoneDataResponse.decode(buffer);

	ViewManager.hideLoading();
	MegaEvoPart.StoneList = msg.StoneDatas;
	MegaEvoPanel.ShowAfterRequest();
}
//请求合成钥石
MegaEvoPart.StoneSynthesizeRequest = function(stoneID) {
	var request = new  megaevo_msg_pb.MegaEvoPart_StoneSynthesizeRequest();
	request.StoneID = stoneID;
	ViewManager.showLoading({});
	SocketClient.callServerFun('MegaEvoPart_SynthesizeStone', request);
}
//请求合成返回
MegaEvoPart.StoneSynthesizeResponse = function(buffer) {
	var msg = megaevo_msg_pb.MegaEvoPart_StoneSynthesizeResponse.decode(buffer);

	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		console.error(("合成钥石失败,错误码" + msg.Flag));
		return;
	}
	//遍历有相等的则替换，没有则插入
	var isExist = false;
	for (var i = 0; i != MegaEvoPart.StoneList.length; ++i) {
		if ((MegaEvoPart.StoneList[i].StoneID == msg.StoneData.StoneID)) {
			MegaEvoPart.StoneList[i] = msg.StoneData;
			isExist = true;
		}
	}
	//没有则插入
	if (!isExist) {
		MegaEvoPanel.OnSynthesized();
		table.insert(MegaEvoPart.StoneList, msg.StoneData);
	} else {
		MegaDetailPopPanel.OnSynthesized();
	}
	//刷新当前界面
	MegaEvoPanel.UpdateCurrent();
	//刷新详情界面
	MegaDetailPopPanel.LoadAfter();
}
//请求强化钥石
MegaEvoPart.EnhanceRequest = function(stoneID, index, count) {
	var request = new  megaevo_msg_pb.MegaEvoPart_EnhanceRequest();
	request.StoneID = stoneID;
	request.Index = index;
	request.Count = count;
	ViewManager.showLoading({});
	SocketClient.callServerFun('MegaEvoPart_EnhanceStone', request);
}
//请求强化返回
MegaEvoPart.EnhanceResponse = function(buffer) {
	var msg = megaevo_msg_pb.MegaEvoPart_EnhanceResponse.decode(buffer);

	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		console.error(("强化钥石失败,错误码" + msg.Flag));
		return;
	}
	//遍历有相等的则替换，没有则插入
	var isExist = false;
	for (var i = 0; i != MegaEvoPart.StoneList.length; ++i) {
		if ((MegaEvoPart.StoneList[i].StoneID == msg.StoneData.StoneID)) {
			MegaEvoPart.StoneList[i] = msg.StoneData;
			isExist = true;
		}
	}
	if (!isExist) {
		console.error("强化的钥石不存在");
		return;
	}
	//刷新当前界面
	MegaEvoPanel.UpdateCurrent();
	//刷新单个品质界面
	MegaDetailPopPanel.LoadAfter();
}
//获取钥石列表
MegaEvoPart.GetMegaList = function() {
	var list = { };
	//先存入已经合成的
	var index = 1;
	let Conf_BaiWan_JinHuaYueShi = LoadConfig.getConfig(ConfigName.BaiWan_JinHuaYueShi);
	for (var k in Conf_BaiWan_JinHuaYueShi) {
		var v = Conf_BaiWan_JinHuaYueShi[k];
		{
			var isExist = MegaEvoPart.IsCombined(k);
			var stoneData;
			if (isExist) {
				list[index] = { };
				list[index].StoneID = k;
				list[index].State = MegaEvoPart.StateEnum.HasCom;
				list[index].JumpLevel = stoneData.JumpLevel;
				list[index].EnhanceLevels = { };
				list[index].EnhanceLevels = stoneData.EnhanceLevels;
				var costID = (stoneData.JumpLevel + 2);
				//计算消耗表ID
				//如果ID合法
				if ((costID <= 5)) {
					var costData = LoadConfig.getConfigData(ConfigName.BaiWan_ShengPinXiaoHao,costID);
					var cost = 0;
					if ((costData != null)) {
						cost = costData.StoneCost;
					}
					var conf_Data = LoadConfig.getConfigData(ConfigName.BaiWan_JinHuaYueShi,stoneData.StoneID);
					if ((conf_Data != null)) {
						list[index].CurCount = BackPackPart.GetItemCountByID(conf_Data.PieceID);
						list[index].CostCount = cost;
					}
				} else {
					var conf_Data = LoadConfig.getConfigData(ConfigName.BaiWan_JinHuaYueShi,stoneData.StoneID);
					if ((conf_Data != null)) {
						list[index].CurCount = BackPackPart.GetItemCountByID(conf_Data.PieceID);
						list[index].CostCount = 9999;
					}
				}
				index = (index + 1);
			} else {
				list[index] = { };
				var cost = LoadConfig.getConfigData(ConfigName.BaiWan_ShengPinXiaoHao,1).StoneCost;
				//拿到合成消耗
				var cur = BackPackPart.GetItemCountByID(v.PieceID);
				//根据合成消耗，判断状态
				if ((cur >= cost)) {
					list[index].State = MegaEvoPart.StateEnum.CanCom;
				} else {
					list[index].State = MegaEvoPart.StateEnum.NoneCom;
				}
				list[index].StoneID = k;
				list[index].JumpLevel = 0;
				list[index].EnhanceLevels = { };
				list[index].CurCount = cur;
				list[index].CostCount = cost;
				index = (index + 1);
			}
		}
	}
	table.sort(list, MegaEvoPart.StoneCom);
	return list;
}
//排序规则
MegaEvoPart.StoneCom = function(a, b) {
	//根据是否合成排序
	if ((a.State != b.State)) {
		return (a.State < b.State);
	}
	//根据品质排序
	if ((a.JumpLevel != b.JumpLevel)) {
		return (a.JumpLevel > b.JumpLevel);
	}
	//根据当前数量
	if ((a.CurCount != b.CurCount)) {
		return (a.CurCount > b.CurCount);
	}
	//最后根据ID排序
	return (a.StoneID > b.StoneID);
}
//是否已经有合成的钥石
MegaEvoPart.IsCombined = function(stoneID) {
	if ((MegaEvoPart.StoneList.length <= 0)) {
		return false, null;
	}
	for (var i = 0; i != MegaEvoPart.StoneList.length; ++i) {
		if ((MegaEvoPart.StoneList[i].StoneID == stoneID)) {
			return true, MegaEvoPart.StoneList[i];
		}
	}
	return false, null;
}
//是否达到百万进化条件
MegaEvoPart.CanMegaEvo = function(cardID) {
	var heroInfo = LoadConfig.getConfigData(ConfigName.ShiBing,cardID);
	if ((heroInfo == null)) {
		console.error(("士兵表空：ID " + cardID));
		return false;
	}
	if ((heroInfo.EVO != 6)) {
		return false;
	}
	var stoneID = Math.floor((cardID / 10));
	//判断是否有钥石  是否红色品质
	if (!MegaEvoPart.IsCombined(stoneID)) {
		return false;
	}
	//是否红色品质
	if ((MegaEvoPart.GetStoneDataByID(stoneID).JumpLevel < 4)) {
		return false;
	}
	return true;
}
//获取钥石对象
MegaEvoPart.GetStoneDataByID = function(stoneID) {
	for (var i = 0; i != MegaEvoPart.StoneList.length; ++i) {
		if ((MegaEvoPart.StoneList[i].StoneID == stoneID)) {
			return MegaEvoPart.StoneList[i];
		}
	}
	//到这里说明没有这个钥石，给默认数据
	var initData = { };
	initData.StoneID = stoneID;
	initData.JumpLevel = -1;
	initData.EnhanceLevels = [
		1,
		1,
		1,
		1,
		1,
		1,
		1,
		1,
		1,
		1
	];
	return initData;
}
//计算材料是否足够强化
MegaEvoPart.IsEnhanceEnough = function(stoneID, index) {
	//拿到钥石对象
	var stoneData = MegaEvoPart.GetStoneDataByID(stoneID);
	var level = stoneData.EnhanceLevels[index];
	//取钥石表格
	var costData = LoadConfig.getConfigData(ConfigName.BaiWan_QiangHuaXiaoHao,level);
	if ((costData == null)) {
		console.error(("Conf_BaiWan_QiangHuaXiaoHao表格不存在ID" + level));
		return false;
	}
	var resourceType = { };
	var resourceID = { };
	var resourceCount = { };
	var indexOne = ((((index - 1)) * 2) + 1);
	var indexTwo = ((((index - 1)) * 2) + 2);
	//判断是否有碎片消耗,如果有则判断碎片和第一个资源，如果没有则是两个
	if ((costData.PieceCost[index] > 0)) {
		table.insert(resourceType, 2);
		var conf_stone = LoadConfig.getConfigData(ConfigName.BaiWan_JinHuaYueShi,stoneID);
		if ((conf_stone == null)) {
			console.error(("Conf_BaiWan_JinHuaYueShi表格不存在ID" + stoneID));
			return false;
		}
		table.insert(resourceID, conf_stone.PieceID);
		table.insert(resourceCount, tostring(costData.PieceCost[index]));
		if ((costData.ResourceType[indexOne] != 0)) {
			table.insert(resourceType, costData.ResourceType[indexOne]);
			table.insert(resourceID, costData.ResourceID[indexOne]);
			table.insert(resourceCount, costData.ResourceCount[indexOne]);
		}
	} else {
		//判空
		if ((costData.ResourceType[indexOne] != 0)) {
			table.insert(resourceType, costData.ResourceType[indexOne]);
			table.insert(resourceID, costData.ResourceID[indexOne]);
			table.insert(resourceCount, costData.ResourceCount[indexOne]);
		}
		if ((costData.ResourceType[indexTwo] != 0)) {
			table.insert(resourceType, costData.ResourceType[indexTwo]);
			table.insert(resourceID, costData.ResourceID[indexTwo]);
			table.insert(resourceCount, costData.ResourceCount[indexTwo]);
		}
	}
	//开始和玩家资源对比
	for (var i = 0; i != resourceType.length; ++i) {
		var playerHave = ResourceTool_GetResourceCount(resourceType[i], resourceID[i]);
		if (BigNumber.lessThan(BigNumber.create(playerHave), BigNumber.create(resourceCount[i]))) {
			return false;
		}
	}
	return true;
}
//判断是否有可强化的品质
MegaEvoPart.IsEnough2LevelUp = function(stoneObj) {
	//如果不存在则是没合成
	if (!MegaEvoPart.IsCombined(stoneObj.StoneID)) {
		return false;
	}
	var currentIndex = stoneObj.JumpLevel;
	//只到橙色，排除红色
	for (var i = 0; i != currentIndex; ++i) {
		var level = stoneObj.EnhanceLevels[i];
		if ((level < ENHANCEMAXLEVEL)) {
			//小于50级才进来
			//取钥石表格
			var costData = LoadConfig.getConfigData(ConfigName.BaiWan_QiangHuaXiaoHao,level);
			if ((costData == null)) {
				console.error(("Conf_BaiWan_QiangHuaXiaoHao表格不存在ID" + level));
				return false;
			}
			var resourceType = { };
			var resourceID = { };
			var resourceCount = { };
			var indexOne = ((((i - 1)) * 2) + 1);
			var indexTwo = ((((i - 1)) * 2) + 2);
			//判断是否有碎片消耗,如果有则判断碎片和第一个资源，如果没有则是两个
			if ((costData.PieceCost[i] > 0)) {
				table.insert(resourceType, 2);
				var stoneID = stoneObj.StoneID;
				var conf_stone = LoadConfig.getConfigData(ConfigName.BaiWan_JinHuaYueShi,stoneID);
				if ((conf_stone == null)) {
					console.error(("Conf_BaiWan_JinHuaYueShi表格不存在ID" + stoneID));
					return false;
				}
				table.insert(resourceID, conf_stone.PieceID);
				table.insert(resourceCount, tostring(costData.PieceCost[i]));
				if ((costData.ResourceType[indexOne] != 0)) {
					table.insert(resourceType, costData.ResourceType[indexOne]);
					table.insert(resourceID, costData.ResourceID[indexOne]);
					table.insert(resourceCount, costData.ResourceCount[indexOne]);
				}
			} else {
				//判空
				if ((costData.ResourceType[indexOne] != 0)) {
					table.insert(resourceType, costData.ResourceType[indexOne]);
					table.insert(resourceID, costData.ResourceID[indexOne]);
					table.insert(resourceCount, costData.ResourceCount[indexOne]);
				}
				if ((costData.ResourceType[indexTwo] != 0)) {
					table.insert(resourceType, costData.ResourceType[indexTwo]);
					table.insert(resourceID, costData.ResourceID[indexTwo]);
					table.insert(resourceCount, costData.ResourceCount[indexTwo]);
				}
			}
			var biggerCount = 0;
			//开始和玩家资源对比
			for (let j = 0; j != resourceType.length; ++j) {
				var playerHave = ResourceTool_GetResourceCount(resourceType[j], resourceID[j]);
				if (!BigNumber.lessThan(BigNumber.create(playerHave), BigNumber.create(resourceCount[j]))) {
					biggerCount = (biggerCount + 1);
				}
			}
			if (biggerCount == resourceType.length) {
				return true;
			}
		}
	}
	return false;
}
//判断是否强化满级
MegaEvoPart.IsEnhanceMax = function(stoneID, index, maxLv) {
	var stoneObj = MegaEvoPart.GetStoneDataByID(stoneID);
	if ((stoneObj.EnhanceLevels[index] >= maxLv)) {
		return true;
	}
	return false;
}
//排序规则
MegaEvoPart.SortAttrs = function(a, b) {
	//先按照对象排序
	if ((a.TargetID != b.TargetID)) {
		return (a.TargetID > b.TargetID);
	}
	//再按照属性
	return (a.AttrID < b.AttrID);
}
//获取属性列表
MegaEvoPart.GetAttrList = function() {
	var attrList = { };
	//遍历已开放的所有钥石
	for (var i = 0; i != MegaEvoPart.StoneList.length; ++i) {
		var stoneID = MegaEvoPart.StoneList[i].StoneID;
		var jumpLv = (MegaEvoPart.StoneList[i].JumpLevel + 1);
		//跃迁等级+1是因为LUA数组从1开始
		if ((jumpLv > 4)) {
			jumpLv = 4;
		}
		var conf_Stone = LoadConfig.getConfigData(ConfigName.BaiWan_JinHuaYueShi,stoneID);
		if ((conf_Stone == null)) {
			console.error(("钥石ID不存在" + stoneID));
			return attrList;
		}
		//遍历已经激活的属性
		for (let j = 0; j != jumpLv; ++j) {
			var attrID = conf_Stone.ProOneID[j];
			var targetID = conf_Stone.TargetOne[j];
			var isHave = false;
			for (var k = 1; k != attrList.length; ++k) {
				if (((attrList[k].AttrID == attrID) && (attrList[k].TargetID == targetID))) {
					isHave = true;
					break;
				}
			}
			//不存在则加入
			if (!isHave) {
				var index = (attrList.length + 1);
				attrList[index] = { };
				attrList[index].AttrID = attrID;
				attrList[index].TargetID = targetID;
			}
			var attrID2 = conf_Stone.ProTwoID[j];
			if ((attrID2 != 0)) {
				var targetID2 = conf_Stone.TargetTwo[j];
				var isHave = false;
				for (var k = 1; k != attrList.length; ++k) {
					if (((attrList[k].AttrID == attrID2) && (attrList[k].TargetID == targetID))) {
						isHave = true;
						break;
					}
				}
				//不存在则加入
				if (!isHave) {
					var index = (attrList.length + 1);
					attrList[index] = { };
					attrList[index].AttrID = attrID2;
					attrList[index].TargetID = targetID2;
				}
			}
		}
	}
	//排序属性，从小到大
	table.sort(attrList, MegaEvoPart.SortAttrs);
	return attrList;
}
//------------------------------假数据相关操作-----------------------
//初始化假数据
MegaEvoPart.InitFakeDataList = function() {
	MegaEvoPart.FakeDataList = { };
}
//查询是否有假数据缓存,找到返回true
MegaEvoPart.FindFakeData = function(resourceType, resourceID) {
	for (var i = 0; i != MegaEvoPart.FakeDataList.length; ++i) {
		var listObj = MegaEvoPart.FakeDataList[i];
		if (((listObj.ResourceType == resourceType) && (listObj.ResourceID == resourceID))) {
			return true;
		}
	}
	return false;
}
//插入假数据用于计算
MegaEvoPart.InsertFakeData = function(resourceType, resourceID, resourceCount) {
	var obj = { };
	obj.ResourceType = resourceType;
	obj.ResourceID = resourceID;
	obj.ResourceCount = resourceCount;
	table.insert(MegaEvoPart.FakeDataList, obj);
}
//获得某个数据值
MegaEvoPart.GetFakeValue = function(resourceType, resourceID) {
	for (var i = 0; i != MegaEvoPart.FakeDataList.length; ++i) {
		var listObj = MegaEvoPart.FakeDataList[i];
		if (((listObj.ResourceType == resourceType) && (listObj.ResourceID == resourceID))) {
			return listObj.ResourceCount;
		}
	}
	return "0";
}
//扣除假数据的值
MegaEvoPart.SubFakeData = function(resourceType, resourceID, resourceCount) {
	for (var i = 0; i != MegaEvoPart.FakeDataList.length; ++i) {
		var listObj = MegaEvoPart.FakeDataList[i];
		if (((listObj.ResourceType == resourceType) && (listObj.ResourceID == resourceID))) {
			listObj.ResourceCount = BigNumber.sub(listObj.ResourceCount, resourceCount);
			break;
		}
	}
}
//假数据资源是否足够,足够直接扣除
MegaEvoPart.FakeIsEnhanceEnough = function(stoneID, index, level) {
	//取钥石表格
	var costData = LoadConfig.getConfigData(ConfigName.BaiWan_QiangHuaXiaoHao,level);
	if ((costData == null)) {
		console.error(("Conf_BaiWan_QiangHuaXiaoHao表格不存在ID" + level));
		return false, "0";
	}
	var resourceType = { };
	var resourceID = { };
	var resourceCount = { };
	var indexOne = ((((index - 1)) * 2) + 1);
	var indexTwo = ((((index - 1)) * 2) + 2);
	//判断是否有碎片消耗,如果有则判断碎片和第一个资源，如果没有则是两个
	if ((costData.PieceCost[index] > 0)) {
		table.insert(resourceType, 2);
		var conf_stone = LoadConfig.getConfigData(ConfigName.BaiWan_JinHuaYueShi,stoneID);
		if ((conf_stone == null)) {
			console.error(("Conf_BaiWan_JinHuaYueShi表格不存在ID" + stoneID));
			return false;
		}
		table.insert(resourceID, conf_stone.PieceID);
		table.insert(resourceCount, tostring(costData.PieceCost[index]));
		if ((costData.ResourceType[indexOne] != 0)) {
			table.insert(resourceType, costData.ResourceType[indexOne]);
			table.insert(resourceID, costData.ResourceID[indexOne]);
			table.insert(resourceCount, costData.ResourceCount[indexOne]);
		}
	} else {
		//判空
		if ((costData.ResourceType[indexOne] != 0)) {
			table.insert(resourceType, costData.ResourceType[indexOne]);
			table.insert(resourceID, costData.ResourceID[indexOne]);
			table.insert(resourceCount, costData.ResourceCount[indexOne]);
		}
		if ((costData.ResourceType[indexTwo] != 0)) {
			table.insert(resourceType, costData.ResourceType[indexTwo]);
			table.insert(resourceID, costData.ResourceID[indexTwo]);
			table.insert(resourceCount, costData.ResourceCount[indexTwo]);
		}
	}
	//开始和缓存假资源对比
	for (var i = 0; i != resourceType.length; ++i) {
		var isExist = MegaEvoPart.FindFakeData(resourceType[i], resourceID[i]);
		//不存在插入玩家数据
		if (!isExist) {
			var realData = ResourceTool_GetResourceCount(resourceType[i], resourceID[i]);
			MegaEvoPart.InsertFakeData(resourceType[i], resourceID[i], BigNumber.create(realData));
		}
		var playerHave = MegaEvoPart.GetFakeValue(resourceType[i], resourceID[i]);
		if (BigNumber.lessThan(BigNumber.create(playerHave), BigNumber.create(resourceCount[i]))) {
			return false;
		}
	}
	//扣除资源
	for (var i = 0; i != resourceType.length; ++i) {
		MegaEvoPart.SubFakeData(resourceType[i], resourceID[i], BigNumber.create(resourceCount[i]));
	}
	return true;
}
//获取跃升的总次数
MegaEvoPart.GetJumpCount = function() {
	var count = 0;
	for (var forinvar in ipairs(MegaEvoPart.StoneList)) {
		
		var stone = forinvar.stone;
		{
			count = (count + stone.JumpLevel);
		}
	}
	return count;
}
//获取升级总次数
MegaEvoPart.GetLevelUpCount = function() {
	var count = 0;
	for (var forinvar in ipairs(MegaEvoPart.StoneList)) {
		
		var stone = forinvar.stone;
		{
			for (var i = 0; i != stone.EnhanceLevels.length; ++i) {
				count = (count + stone.EnhanceLevels[i]);
				count = (count - 1);
			}
		}
	}
	return count;
}
window.MegaEvoPart = MegaEvoPart;