//region *.lua
//Date
//此文件由[BabeLua]插件自动生成
let recharge_pb = require('recharge').msg;

let RechargePart = { };

RechargePart.RechargeDataList = [];
//订单类型
const enOrderType = {
	enOrderType_Normal: 0,
	//普通订单
	enOrderType_MonCard: 1,
	//月卡
	enOrderType_SuperGift: 2,
	//超值礼包

};
//收到充值数据
RechargePart.OnRecvRechargeData = function(buffer) {
	var msg = recharge_pb.SyncRechargeData.decode(buffer);

	RechargePart.RechargeDataList = msg.RechargeDataList;
	//根据订单id排序
	var orderSortFunc = function(a, b) {
		return (a.OrderNo < b.OrderNo);
	};
	RechargePart.RechargeDataList.sort(orderSortFunc);
	//    //充值数据
	//    message RechargeData{
	//	    required int32 OrderNo            = 1; //订单编号
	//	    required int32 OrderState         = 2; //订单状态，如果是0表示没有充值过该类订单
	//    }
	//    //同步充值数据
	//    message SyncRechargeData {
	//	    repeated RechargeData RechargeDataList = 1; 
	//    }
	//刷新客户端
	//VipPopPanel.ShuaiXin();
}
//充值回调
RechargePart.RechargeReponse = function(buffer) {
	var msg = recharge_pb.RechargeReponse.decode(buffer);

	if ((msg.MsgCode == 0)) {
		//充值成功
		//message RechargeReward {
		//	required int32  ResourceType  = 1;  //资源类型
		//	required int32  ResourceID    = 2;  //资源ID
		//	required string ResourceCount = 3;  //资源数量
		//}     
		//奖励数组
		var rewardList = msg.RewardList;
		var resType = [];
		var resID = [];
		var resCount =[];
		for (var i = 0; i != rewardList.length; ++i) {
			var data = rewardList[i];
			if ((data.ResourceType <= 0)) {
				break;
			}
			resType[resType.length] = data.ResourceType;
			resID[resID.length] = data.ResourceID;
			resCount[resCount.length] = data.ResourceCount;
		}
		if ((resType.length <= 0)) {
			return;
		}
		HuoDeWuPinTiShiPanelData.Show(resType, resID, resCount);
	} else {
		//其他异常
	}
}
//endregion
window.RechargePart = RechargePart;