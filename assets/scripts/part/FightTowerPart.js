import GFightManager from "../logic/fight/GFightManager";
import SocketClient from "../network/SocketClient";
import CanShuTool from "../tool/CanShuTool";
import ViewManager from "../manager/ViewManager";

let fight_tower_msg_pb = require('fight_tower_msg').msg;

let FightTowerPart = { };
FightTowerPart.CardList = { };
FightTowerPart.OpenCount = 0;
FightTowerPart.StartTime = 0;
//战斗塔开始的时间  --可以攻打的时间 =  开始时间+ 68小时  剩下的8小时是休息时间
//收到服务端数据
FightTowerPart.RevcFightTowerInfo = function(buffer) {
	var msg = fight_tower_msg_pb.FightTowerPart_FightTowerData.decode(buffer);
	FightTowerPart.CardList = { };
	FightTowerPart.CardList = msg.LineUpCardList;
	FightTowerPart.OpenCount = msg.OpenCount;
	FightTowerPart.StartTime = msg.StartTime;
}
//获取当前的关卡阵容ID
FightTowerPart.GetMonsterLineUp = function(round) {
	//判断期数是否异常
	if ((FightTowerPart.OpenCount == 0)) {
		console.error("获取战斗塔怪物配置异常，还未开放战斗塔");
		return null;
	}
	//取参数 最大层数
	var conf_maxRound = CanShuTool.GetCanShuData(CanShuTool.CanShuEnum.FightTowerMaxRound);
	if ((conf_maxRound == null)) {
		return null;
	}
	//判断越界
	if ((round > conf_maxRound.Param[0])) {
		console.error(("获取阵容配置异常，层数越界" + round));
		return null;
	}
	//取参数 循环 根据关卡和开放次数，读表 (有可能出现当前开放次数大于配置表的最大ID，所以需要用当前开放次数对配置表最大数量取余)
	var conf_loop = CanShuTool.GetCanShuData(CanShuTool.CanShuEnum.FightTowerLoopCount);
	if ((conf_loop == null)) {
		return null;
	}
	//取余读表，若为0则为循环最大值
	//local peiZhiID = FightTowerPart.OpenCount % conf_loop.Param[0]
	//if peiZhiID == 0 then
	//peiZhiID = conf_loop.Param[0]
	//end
	//读战斗塔配置表，返回阵容配置
	var con_TowerInfo = LoadConfig.getConfigData(ConfigName.ZhanDouTa_PeiZhi,peiZhiID);
	if ((con_TowerInfo == null)) {
		console.error(("战斗塔配置表ID错误" + peiZhiID));
		return null;
	}
	//返回需要的配置信息
	return con_TowerInfo.ZhenRongID[round];
}
//判断当前是否在活动时间内
FightTowerPart.IsOpen = function() {
	var mStartTime = FightTowerPart.StartTime;
	//取参数表的挑战次数刷新时间间隔
	var canshuInfoTime = CanShuTool.GetCanShuData(CanShuTool.CanShuEnum.FightTowerOpenTime);
	if ((canshuInfoTime == null)) {
		return;
	}
	//开启持续时间 --参数为小时  需要乘以3600  转为秒
	var countDownTime = (canshuInfoTime.Param[2] * 3600);
	//倒计时时间   =  开始时间+ 持续时间 - 当前时间
	var countDownSec = ((mStartTime + countDownTime) - TimePart.GetUnix());
	if ((countDownSec < 0)) {
		return false;
	}
	return true;
}
//判断某张卡是否被禁
FightTowerPart.IsForbidden = function(zhenRongID, cardID) {
	var conf_ZhenRongInfo = LoadConfig.getConfigData(ConfigName.ZhanDouTa_ZhenRong,zhenRongID);
	if ((conf_ZhenRongInfo == null)) {
		console.error(("战斗塔阵容表空，ID" + zhenRongID));
		return true;
	}
	var conf_KaPaiInfo = LoadConfig.getConfigData(ConfigName.ShiBing,cardID);
	if ((conf_KaPaiInfo == null)) {
		console.error(("士兵表空，ID" + cardID));
		return true;
	}
	//先存下会被禁用的属性
	//属性///////////////////////
	var race = conf_KaPaiInfo.Race;
	//品质///////////////////////
	var quality = conf_KaPaiInfo.Quality;
	//品质 白1 绿2 蓝3 紫4 橙5 
	//是否对空
	var isFly = conf_KaPaiInfo.Troop;
	//性别//////////////////////
	var sex = (conf_KaPaiInfo.Category + 1);
	//LUA数组从1 开始  要加一
	//进化等级///////////////////
	var evo = (conf_KaPaiInfo.EVO + 1);
	//LUA数组从1 开始  要加一
	//兵种 空军 1   2和3是陆军
	var troop = conf_KaPaiInfo.Troop;
	//攻击类型 1近战 2远程(ID 从1开始 索引减一)   
	var attckType = conf_KaPaiInfo.AttackType;
	if (((((((race < 1) || (quality < 1)) || (sex < 1)) || (evo < 1)) || (troop < 1)) || (attckType < 1))) {
		console.error("数组索引小于1，判断禁用,战斗塔");
	}
	//先判断属性是否被禁 0不限制 1 限制
	if ((conf_ZhenRongInfo.LimitRace[race] == 1)) {
		return true;
	}
	//再判断品质是否被禁
	if ((conf_ZhenRongInfo.LimitQuality[quality] == 1)) {
		return true;
	}
	//再判断性别是否被禁
	if ((conf_ZhenRongInfo.LimitSex[sex] == 1)) {
		return true;
	}
	//判断进化等级是否被禁
	if ((conf_ZhenRongInfo.LimitEvo[evo] == 1)) {
		return true;
	}
	//判断军种是否被禁
	var troopIndex = troop;
	if ((troopIndex == 3)) {
		troopIndex = 2;
	}
	if ((conf_ZhenRongInfo.LimitTroop[troopIndex] == 1)) {
		return true;
	}
	//判断攻击类型
	if ((conf_ZhenRongInfo.LimitAttackType[attckType] == 1)) {
		return true;
	}
	return false;
}
//获取玩家拥有的所有卡牌列表，包含CardID,Count
FightTowerPart.GetPlayerCardList = function() {
	var cardList = { };
	var index = 1;
	//先算背包里的
	for (var i = 1; i != BackPackPart.mCardList.length; ++i) {
		cardList[index] = { };
		cardList[index].CardID = BackPackPart.mCardList[i].CardID;
		cardList[index].Count = BackPackPart.mCardList[i].Count;
		index = (index + 1);
	}
	//再算阵容中的
	for (var i = 1; i != LineupPart.CardList.length; ++i) {
		var isSave = false;
		var saveIndex = -1;
		for (var j = 1; j != cardList.length; ++j) {
			if ((cardList[j].CardID == LineupPart.CardList[i].CardID)) {
				isSave = true;
				saveIndex = j;
				break;
			}
		}
		//已经有则数量加
		if (isSave) {
			cardList[saveIndex].Count = (cardList[saveIndex].Count + 1);
		} else {
			//没有则新建
			cardList[index] = { };
			cardList[index].CardID = LineupPart.CardList[i].CardID;
			cardList[index].Count = 1;
			index = (index + 1);
		}
	}
	//排序
	//table.sort(cardList,FightTowerPart.CardBagCom)
	return cardList;
	//用的时候要用是否禁用筛选一次
}
//阵容排序规则   高品质在前，低品质在后,同品质时，高进化等级的在前,相同进化等级时，高训练等级的在前。同训练等级时，强化等级高的在前。强化等级相同时，按ID排序。
FightTowerPart.CardBagCom = function(a, b) {
	var aCard = a;
	var bCard = b;
	//获取表格配置
	var aConf = LoadConfig.getConfigData(ConfigName.ShiBing,aCard.CardID);
	var bConf = LoadConfig.getConfigData(ConfigName.ShiBing,bCard.CardID);
	//再更具品质排序
	if ((aConf.Quality != bConf.Quality)) {
		return (aConf.Quality > bConf.Quality);
	}
	//根据进化等级排序
	if ((aConf.EVO != bConf.EVO)) {
		return (aConf.EVO > bConf.EVO);
	}
	//最后根据ID排序
	return (aCard.CardID > bCard.CardID);
}
var mCardID = null;
//请求上阵卡牌
FightTowerPart.AddLineUpRequest = function(cardID, round) {
	//判断拥有这张卡的数量是否足够
	//先拿到拥有多少张,背包加上阵容
	var playerHave = BackPackPart.GetCardCount(cardID);
	for (var i = 1; i != LineupPart.CardList.length; ++i) {
		if ((LineupPart.CardList[i].CardID == cardID)) {
			playerHave = (playerHave + 1);
		}
	}
	//拿到已经上阵的数量
	var lineCount = 0;
	for (var i = 1; i != FightTowerPart.CardList.length; ++i) {
		if ((FightTowerPart.CardList[i] == cardID)) {
			lineCount = (lineCount + 1);
		}
	}
	//剩余的总量
	var leftCount = (playerHave - lineCount);
	//如果小于1则异常
	if ((leftCount < 1)) {
		return;
	}
	//获取当前关卡上阵配置ID
	var zhenRongID = FightTowerPart.GetMonsterLineUp(round);
	//获取阵容配置
	var conf_ZhenRongPeiZhi = LoadConfig.getConfigData(ConfigName.ZhanDouTa_ZhenRong,zhenRongID);
	if ((conf_ZhenRongPeiZhi == null)) {
		return;
	}
	//计算上阵索引
	var index = -1;
	for (var i = 1; i != FightTowerPart.CardList.length; ++i) {
		if (FightTowerPart.CardList[i] == 0) {
			index = i;
			break;
		}
	}
	//判断索引是否异常
	if ((index == (-1) || (index > conf_ZhenRongPeiZhi.LineCount))) {
		return;
	}
	//判断卡牌是否符合上阵要求,返回true则禁用，false则不禁用
	if (FightTowerPart.IsForbidden(zhenRongID, cardID)) {
		return;
	}
	//添加到阵容中
	FightTowerPart.CardList[index] = cardID;
	//刷新界面
	TowerChangePopPanel.UpdatePanelInfo();
	FightTowerPanel.UpdateLineInfo();
}
//请求上阵回应
FightTowerPart.AddLineUpResponse = function(buffer) {
	var msg = fight_tower_msg_pb.FightTowerPart_AddLineUpResponse.decode(buffer);
	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		console.error(("上阵失败，错误ID" + msg.Flag));
		return;
	}
	//成功则加入请求添加的卡
	for (var i = 1; i != FightTowerPart.CardList.length; ++i) {
		if ((FightTowerPart.CardList[i] == 0)) {
			FightTowerPart.CardList[i] = mCardID;
			break;
		}
	}
	//刷新界面
	TowerChangePopPanel.UpdatePanelInfo();
	FightTowerPanel.UpdateLineInfo();
}
var mIndex = null;
//请求下阵卡牌
FightTowerPart.SubLineDownRequest = function(index) {
	//判断索引越界
	if (((index < 1) || (index > 12))) {
		return;
	}
	//判断该索引是否存在卡牌
	var cardID = FightTowerPart.CardList[index];
	if ((cardID == 0)) {
		return;
	}
	//下阵卡牌
	FightTowerPart.CardList[index] = 0;
	//刷新界面
	TowerChangePopPanel.UpdatePanelInfo();
	FightTowerPanel.UpdateLineInfo();
}
//请求下阵卡牌回应
FightTowerPart.SubLineDownResponse = function(buffer) {
	var msg = fight_tower_msg_pb.FightTowerPart_SubLineDownResponse.decode(buffer);
	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		console.error(("下阵失败，错误ID" + msg.Flag));
		return;
	}
	//下阵成功则清空
	FightTowerPart.CardList[mIndex] = 0;
	//刷新界面
	TowerChangePopPanel.UpdatePanelInfo();
	FightTowerPanel.UpdateLineInfo();
}
//请求交换阵容上的卡牌
FightTowerPart.ChangeLineRequest = function(beforeIndex, afterIndex, round) {
	var id = FightTowerPart.CardList[beforeIndex];
	FightTowerPart.CardList[beforeIndex] = FightTowerPart.CardList[afterIndex];
	FightTowerPart.CardList[afterIndex] = id;
	TowerChangePopPanel.UpdatePanelInfo();
	FightTowerPanel.UpdateLineInfo();
}
//换阵回应
FightTowerPart.ChangeLineResponse = function(buffer) {
	var msg = fight_tower_msg_pb.FightTowerPart_ChangeLineResponse.decode(buffer);
	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		console.error(("换阵失败，错误ID" + msg.Flag));
		return;
	}
	TowerChangePopPanel.UpdatePanelInfo();
	FightTowerPanel.UpdateLineInfo();
}
var isFirstPass = false;
var Record = null;
var isWin = null;
//回应挑战
FightTowerPart.FightResponse = function(buffer) {
	var msg = fight_tower_msg_pb.FightTowerPart_FightResponse.decode(buffer);
	if ((msg.Flag != 0)) {
		console.error(("挑战失败，错误ID" + msg.Flag));
		FightBehaviorManager.SetFightingSystem(null);
		return;
	}
	isFirstPass = msg.IsFirtPass;
	isWin = msg.IsWin;
	Record = new FightRecords();
	Record.Decode(msg.Content);
	if (msg.Done) {
		FightTowerPart.PlayBack();
		ViewManager.hideLoading();
	}
}
FightTowerPart.FighResponseAppend = function(buffer) {
	var msg = fight_tower_msg_pb.FightTowerPart_FightAppendResponse.decode(buffer);
	Record.AppendFollow(msg.Content);
	if (msg.Done) {
		FightTowerPart.PlayBack();
		ViewManager.hideLoading();
	}
}
FightTowerPart.PlayBack = function() {
	//开启战斗
	var conf_ZhanDouPeiZhi_Data = LoadConfig.getConfigData(ConfigName.ZhanDouPeiZhi,10);
	if ((conf_ZhanDouPeiZhi_Data == null)) {
		return;
	}
	CommonFightPanel.Show(CommonFightPanel.FightType.FightTower, function() {
			GFightManager.Playback(conf_ZhanDouPeiZhi_Data.Scene, FightSystemType.FightTower, Record, FightTowerPart.OnFightFinished);
		});
}
FightTowerPart.OnFightFinished = function() {
	if (isWin) {
		DungeonResultPopData.ShowPanel(DungeonResultPopData.DungeonShowType.FightTower,true,{isFirstPass : isFirstPass});
		isFirstPass = false;
	} else {
		DungeonResultPopData.ShowPanel(DungeonResultPopData.DungeonShowType.FightTower,false,{isFirstPass : false});
	}
}
//请求验证阵容是否被禁,如果被禁，后端下发更新后的阵容
FightTowerPart.CheckLineRequest = function(round) {
	var request = new fight_tower_msg_pb.FightTowerPart_CheckLineRequest();
	request.Round = round;
	SocketClient.callServerFun('FightTowerPart_CheckLine', request);
}
//验证回调
FightTowerPart.CheckLineResponse = function(buffer) {
	var msg = new fight_tower_msg_pb.FightTowerPart_CheckLineResponse();
	msg.ParseFromString(buffer.ReadBuffer());
	if ((msg.Flag != 0)) {
		console.error(("验证失败,ID" + msg.Flag));
		return;
	}
}
//前端验证
FightTowerPart.CheckLineClient = function(round) {
	//获取当前关卡上阵配置ID
	var zhenRongID = FightTowerPart.GetMonsterLineUp(round);
	if ((zhenRongID == null)) {
		console.error(("前端验证，阵容ID空,层数" + round));
		return;
	}
	//获取阵容配置
	var conf_ZhenRongPeiZhi = LoadConfig.getConfigData(ConfigName.ZhanDouTa_ZhenRong,zhenRongID);
	if ((conf_ZhenRongPeiZhi == null)) {
		console.error(("战斗塔阵容表空，ID" + zhenRongID));
		return;
	}
	//判断当前阵容是否有当前关卡禁用的卡，有则剔除
	for (var i = 1; i != FightTowerPart.CardList.length; ++i) {
		var cardID = FightTowerPart.CardList[i];
		if ((cardID != 0)) {
			if (FightTowerPart.IsForbidden(zhenRongID, cardID)) {
				FightTowerPart.CardList[i] = 0;
			}
			//玩家是否有这张卡,验证进化后的卡遗留的问题
			var playerHave = BackPackPart.GetCardCount(cardID);
			var lineCount = 0;
			for (var i = 1; i != LineupPart.CardList.length; ++i) {
				if ((LineupPart.CardList[i].CardID == cardID)) {
					lineCount = (lineCount + 1);
				}
			}
			//玩家拥有的数量
			var totalCount = (playerHave + lineCount);
			if ((totalCount <= 0)) {
				FightTowerPart.CardList[i] = 0;
			}
		}
	}
	//判断当前阵容是否超过限制数量，将超过的归0
	var nowCount = 0;
	//创建可用的新卡牌ID数组
	var newCardList = { };
	var index = 1;
	for (var i = 1; i != FightTowerPart.CardList.length; ++i) {
		if ((FightTowerPart.CardList[i] != 0)) {
			nowCount = (nowCount + 1);
			//如果超过限制则跳出循环
			if ((nowCount > conf_ZhenRongPeiZhi.LineCount)) {
				break;
			}
			newCardList[index] = FightTowerPart.CardList[i];
			index = (index + 1);
		}
	}
	//重置
	for (var i = 1; i != FightTowerPart.CardList.length; ++i) {
		FightTowerPart.CardList[i] = 0;
	}
	//加入符合的卡牌
	if ((newCardList.length > 0)) {
		for (var i = 1; i != newCardList.length; ++i) {
			FightTowerPart.CardList[i] = newCardList[i];
		}
	}
	//检测结束
	FightTowerPanel.AfterCheck();
}
//请求购买次数
FightTowerPart.BuyChallengeCountRequest = function(count) {
	var request = new fight_tower_msg_pb.FightTowerPart_BuyChallengeCountRequest();
	request.Count = count;
	ViewManager.showLoading({});
	SocketClient.callServerFun('FightTowerPart_BuyChallengeCount', request);
}
//购买次数回应
FightTowerPart.BuyChallengeCountResponse = function(buffer) {
	var msg = new fight_tower_msg_pb.FightTowerPart_BuyChallengeCountResponse();
	msg.ParseFromString(buffer.ReadBuffer());
	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		console.error(("购买次数失败，错误ID" + msg.Flag));
		return;
	}
	//刷新挑战次数
	FightTowerPanel.UpdateChallengeCount();
	//刷新剩余时间
	FightTowerPanel.UpdateChallengeTime();
}
//根据关卡索引获取怪物阵容配置,返回阵容配置表ID
FightTowerPart.GetMonsterLineUp = function(level) {
	//判断期数是否异常
	if ((this.OpenCount == 0)) {
		console.error("获取战斗塔怪物配置异常，还未开放战斗塔");
		return 0;
	}
	//根据关卡和开放次数，读表 (有可能出现当前开放次数大于配置表的最大ID，所以需要用当前开放次数对配置表最大数量取余)
	var conf_MaxLoop = CanShuTool.GetCanShuData(CanShuTool.CanShuEnum.FightTowerLoopCount);
	if ((conf_MaxLoop == null)) {
		console.error("参数表88错误战斗塔期数循环参数");
		return 0;
	}
	//取余读表，若为0则为循环最大值
	//local peiZhiID = this.OpenCount % conf_MaxLoop.Param[0]
	//if peiZhiID == 0 then
	//	peiZhiID = conf_MaxLoop.Param[0]
	//end
	//读战斗塔配置表，返回阵容配置
	var con_TowerInfo = LoadConfig.getConfigData(ConfigName.ZhanDouTa_PeiZhi,peiZhiID);
	if ((con_TowerInfo == null)) {
		console.error(("战斗塔配置表ID错误" + peiZhiID));
		return 0;
	}
	//返回需要的配置信息
	return con_TowerInfo.ZhenRongID[level];
}
//----------------------------------战斗验证---------------------------------------------
var mFightID = null;
//开启战斗验证
FightTowerPart.OnBeginFightVerifyRequest = function(zhenRongID, roundID) {
	//设置战斗管理器标记
	FightBehaviorManager.SetFightingSystem(FightSystemType.FightTower);
	var request = new fight_tower_msg_pb.FightTowerPart_OnBeginFightVerifyRequest();
	request.ZhenRongID = zhenRongID;
	request.RoundID = roundID;
	for (var i = 1; i != FightTowerPart.CardList.length; ++i) {
		request.LineCardList.push(FightTowerPart.CardList[i]);
	}
	SocketClient.callServerFun('FightTowerPart_OnBeginFightVerifyRequest', request);
	ViewManager.showLoading({});
}

window.FightTowerPart = FightTowerPart;