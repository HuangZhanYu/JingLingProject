

let catchpet_msg_pb = require('catchpet_msg').msg;
//region *.lua
//Date
//此文件由[BabeLua]插件自动生成
let CatchPetPart = { };

CatchPetPart.CatchScenesList = null;
CatchPetPart.RefreshCount = null;
CatchPetPart.LastRecoveryTime = null;
CatchPetPart.HasUsedFreshCard = null;
CatchPetPart.EnemyPet = { };
CatchPetPart.CurePet = { };
CatchPetPart.MyTeam = { };
CatchPetPart.CatchRound = 0;
CatchPetPart.BeforeCatchAttr = null;
CatchPetPart.ChangeTag = false;
//一键捉宠标记
CatchPetPart.CatchResultTag = false;
//普通捉宠标记
CatchPetPart.EnemyEffect = { };
CatchPetPart.HasUnFinishedCatch = false;
//是否有未结束的捕捉
var isPassive = true;
//登录下发是否有未捕捉宠物
CatchPetPart.RecCatchTag = function(buffer) {
	var msg = catchpet_msg_pb.CatchPetPart_SendCatchTag.decode(buffer);
	CatchPetPart.HasUnFinishedCatch = msg.IsInCatch;
}
//请求所有数据
CatchPetPart.RequestAllData = function() {
	var request = new new catchpet_msg_pb.CatchPetPart_RequestAllData();
	isPassive = false;
	SocketClient.callServerFun('CatchPetPart_SendCatchPetInfo', request);
}
//接受所有数据
CatchPetPart.ResponseAllData = function(buffer) {
	var msg = catchpet_msg_pb.CatchPetPart_ResponseAllData.decode(buffer);
	if ((msg.Flag != 0)) {
		console.error(("错误ID" + msg.Flag));
	}
	CatchPetPart.LastRecoveryTime = msg.LastRecoveryTime;
	CatchPetPart.RefreshCount = msg.RefreshCount;
	CatchPetPart.CatchScenesList = msg.CatchScenesList;
	CatchPetPart.HasUsedFreshCard = msg.HasUsedFreshCard;
	//刷新界面
	if (!isPassive) {
		CatchPetPanel.ShowAfterRequest();
		isPassive = true;
	}
}
//刷新时间和次数
CatchPetPart.RecTimeAndCount = function(buffer) {
	var msg = catchpet_msg_pb.CatchPetPart_ResponseTimeData();

	CatchPetPart.LastRecoveryTime = msg.LastRecoveryTime;
	CatchPetPart.RefreshCount = msg.RefreshCount;
}
//请求刷新场景
CatchPetPart.RefreshRequest = function(refreshType) {
	var request = new catchpet_msg_pb.CatchPetPart_RefreshRequest();
	isPassive = false;
	request.RefreshType = refreshType;
	ViewManager.showLoading({});
	SocketClient.callServerFun('CatchPetPart_RefreshScene', request);
}
//请求刷新场景回应
CatchPetPart.RefreshResponse = function(buffer) {
	var msg = catchpet_msg_pb.CatchPetPart_RefreshResponse.decode(buffer);

	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		console.error(("错误ID" + msg.Flag));
	}
	//刷新界面
	CatchPetPanel.OnLoadAfter();
}
//请求搜寻小精灵   搜寻类型  普通  高级    搜寻位置编号 1 -- 6
CatchPetPart.SearchRequest = function(searchType, currentScenePos) {
	isPassive = true;
	var request = new catchpet_msg_pb.CatchPetPart_SearchRequest();
	request.SearchType = searchType;
	request.Position = currentScenePos;
	ViewManager.showLoading({});
	SocketClient.callServerFun('CatchPetPart_SearchPet', request);
}
var rewards = { };
var rewardIndex = 0;
//回应搜寻小精灵   
CatchPetPart.SearchResponse = function(buffer) {
	var msg = catchpet_msg_pb.CatchPetPart_SearchResponse.decode(buffer);

	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		console.error(("错误ID" + msg.Flag));
	}
	//如果有奖励则弹窗
	if ((msg.RewardList.length > 0)) {
		rewards = msg.RewardList;
		rewardIndex = 1;
		var strShow = TranslateTool.GetText(1030);
		var resourceData = ResourceTool_GetResourceDetail(rewards[0].RewardType, rewards[0].RewardID);
		strShow = StringTool.ZhuanHuan(strShow, [
				resourceData.Name,
				rewards[0].RewardCount
			]);
		CommonHUDPanel.Show(strShow, true);
		//如果奖励数量大于1，则设置定时器继续
		if ((rewards.length > 1)) {
			//timerMgr.AddTimerEvent('CatchPetPart.ShowMoreHud', 0.5, this.ShowMoreHud, { }, false);
		} else {
			HuoDeWuPinTiShiPanelData.Show([
					rewards[0].RewardType
				], [
					rewards[0].RewardID
				], [
					rewards[0].RewardCount
				]);
		}
	}
	//刷新搜寻界面
	SearchPetPanel.OnSearchSuccess();
	//刷新体力
	CatchPetPanel.UpdateEnergy();
}
//刷新战斗相关
CatchPetPart.RecFightInfo = function(buffer) {
	var msg = catchpet_msg_pb.CatchPetPart_EnterCatchFightResponse.decode(buffer);

	CatchPetPart.EnemyPet = msg.EnemyPet;
	CatchPetPart.CurePet = msg.CurePet;
	CatchPetPart.MyTeam = msg.MyTeam;
	CatchPetPart.CatchRound = msg.CatchRound;
	// CatchPetPart.EnemyPet.StateList[0].EffectID     = 1;//状态ID
	// CatchPetPart.EnemyPet.StateList[0].LeftRound    = 2;//状态回合剩余
}
//请求购买精灵球
CatchPetPart.BuyBallRequest = function(resourceID, count) {
	var request = new catchpet_msg_pb.CatchPetPart_BuyBallRequest();
	request.ResourceID = resourceID;
	request.Count = count;
	ViewManager.showLoading({});
	SocketClient.callServerFun('CatchPetPart_BuyItem', request);
}
//购买精灵球回应
CatchPetPart.BuyBallResponse = function(buffer) {
	var msg = catchpet_msg_pb.CatchPetPart_BuyBallResponse.decode(buffer);

	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		console.error(("购买失败" + msg.Flag));
		return;
	}
	//刷新界面
	CatchPetFightPanel.FreshBag();
	//刷新捕捉界面精灵球相关数据
	SearchPetPanel.RefreshBalls();
	//刷新快速复活
	GFFastRevivePopPanel.UpdateItems();
}
//进入捕捉 清除该捕捉宠物ID
//参数1 捕捉场景编号
//参数2 点击精灵编号  1 左  2 右
CatchPetPart.EnterCatchFightRequest = function(currentScenePos, heroPos) {
	var request = new catchpet_msg_pb.CatchPetPart_EnterCatchFightRequest();
	request.ScenePosition = currentScenePos;
	request.HeroPosition = heroPos;
	ViewManager.showLoading({});
	SocketClient.callServerFun('CatchPetPart_StartCatch', request);
}
//进入捕捉回应
CatchPetPart.EnterCatchFightResponse = function(buffer) {
	var msg = catchpet_msg_pb.CatchPetPart_EnterCatchFightResponse.decode(buffer);

	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		console.error(("错误ID" + msg.Flag));
		return;
	}
	CatchPetPart.EnemyPet = msg.EnemyPet;
	CatchPetPart.CurePet = msg.CurePet;
	CatchPetPart.MyTeam = msg.MyTeam;
	CatchPetPart.CatchRound = msg.CatchRound;
	//console.error("msg.MyTeam长度"..CatchPetPart.MyTeam)
	//开始抓宠战斗
	CatchPetFightPanel.StartFight();
}
var cardIDResult = 0;
var costBall = 0;
//一键捉宠请求
CatchPetPart.OneKeyBuyPetRequest = function(currentScenePos, heroPos) {
	var request = new catchpet_msg_pb.CatchPetPart_OneKeyBuyPetRequest();
	request.ScenePosition = currentScenePos;
	request.HeroPosition = heroPos;
	cardIDResult = 0;
	costBall = 0;
	//存下敌人的当前属性
	var cardID = CatchPetFightPanel.GetCurEnemyID();
	CatchPetPart.BeforeCatchAttr = PlayerAttrPart.GetCardCollectValue(cardID);
	CatchPetPart.ChangeTag = true;
	ViewManager.showLoading({});
	SocketClient.callServerFun('CatchPetPart_OneKeyBuyPet', request);
}
var oneKeyReward = null;
//一键捉宠回应
CatchPetPart.OneKeyBuyPetResponse = function(buffer) {
	var msg = catchpet_msg_pb.CatchPetPart_OneKeyBuyPetResponse.decode(buffer);

	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		console.error(("错误ID" + msg.Flag));
		return;
	}
	//刷新界面
	SearchPetPanel.OnAfterLoad();
	//等待属性触发
	cardIDResult = msg.CardID;
	costBall = msg.RealCostBall;
	oneKeyReward = msg.RewardList;
	//如果成功并且满了，这直接弹出
	if ((PlayerAttrPart.GetCardPropertyProcess(CatchPetFightPanel.GetCurEnemyID()) == "100%")) {
		CatchPetPart.OneKeyShowResult();
		return;
	}
	if (!CatchPetPart.ChangeTag) {
		CatchPetPart.OneKeyShowResult();
	}
}
//显示一键捕捉结果
CatchPetPart.OneKeyShowResult = function() {
	if ((cardIDResult == 0)) {
		return;
	}
	var resourceType = { };
	var resourceID = { };
	var resourceCount = { };
	for (var i = 0; i != oneKeyReward.length; ++i) {
		resourceType[i] = oneKeyReward[i].RewardType;
		resourceID[i] = oneKeyReward[i].RewardID;
		resourceCount[i] = oneKeyReward[i].RewardCount;
	}
	if ((resourceType.length > 0)) {
		HuoDeWuPinTiShiPanelData.ChangeDelayTime(0.5);
		HuoDeWuPinTiShiPanelData.Show(resourceType, resourceID, resourceCount);
		//0.5s后调用捕捉成功
		//timerMgr.AddTimerEvent('CatchPetPart.OpenCatched', 0.5, this.OpenCatched, { }, false);
	} else {
		CatchPetPart.OpenCatched();
	}
}
//弹出成功
CatchPetPart.OpenCatched = function() {
	CatchPetResultPopPanel.OneKeyShow(cardIDResult, costBall);
}
//请求使用精灵球
CatchPetPart.UseBallRequest = function(BallId) {
	var request = new catchpet_msg_pb.CatchPetPart_UseBallRequest();
	request.BallID = BallId;
	//存下敌人的当前属性
	var cardID = CatchPetFightPanel.GetCurEnemyID();
	CatchPetPart.BeforeCatchAttr = PlayerAttrPart.GetCardCollectValue(cardID);
	CatchPetPart.CatchResultTag = true;
	ViewManager.showLoading({});
	SocketClient.callServerFun('CatchPetPart_UseBall', request);
}
var ballCatchResult = 0;
var ballRunResult = null;
var ballRewardList = { };
//使用精灵球回应
CatchPetPart.UseBallResponse = function(buffer) {
	var msg = catchpet_msg_pb.CatchPetPart_UseBallResponse.decode(buffer);

	ViewManager.hideLoading();
	var catchResult = 0;
	if ((msg.Flag == 0)) {
		catchResult = 1;
		//捕捉成功
	}
	if ((msg.Flag != 0)) {
		if ((msg.Flag == 7)) {
			catchResult = 2;
			//捕捉失败
			//逃跑 还是攻击
			if (msg.IsRun) {
				console.error("逃跑");
				//逃跑是没回合结束 都有概率
			} else {
				//console.error("攻击")
			}
		} else {
			console.error(("错误ID" + msg.Flag));
			return;
		}
	}
	//刷新球
	CatchPetFightPanel.UpdateBallCount();
	ballCatchResult = catchResult;
	ballRunResult = msg.IsRun;
	ballRewardList = msg.RewardList;
	//如果成功并且满了，这直接弹出
	if (((PlayerAttrPart.GetCardPropertyProcess(CatchPetFightPanel.GetCurEnemyID()) == "100%") && (catchResult == 1))) {
		CatchPetPart.ShowResult();
		return;
	}
	//如果是成功则等待服务器更新
	if ((!CatchPetPart.CatchResultTag && (catchResult == 1))) {
		CatchPetPart.ShowResult();
		return;
	}
	//使用成功  关闭精灵球界面  根据服务器下发的成功或者失败  判断显示哪个界面
	if ((catchResult != 1)) {
		CatchPetPart.UseBallAfterServer(catchResult, msg.IsRun, msg.RewardList);
		//额外下发一个奖励
	}
}
//刷新
CatchPetPart.ShowResult = function() {
	CatchPetPart.UseBallAfterServer(ballCatchResult, ballRunResult, ballRewardList);
}
var changeObjID = null;
//请求更换宠物
CatchPetPart.ChangePetRequest = function(objID) {
	var request = new catchpet_msg_pb.CatchPetPart_ChangePetRequest();
	request.ObjID = objID;
	changeObjID = objID;
	//ViewManager.showLoading({})
	SocketClient.callServerFun('CatchPetPart_ChangeCurePet', request);
}
//更换宠物回调
CatchPetPart.ChangePetResponse = function(buffer) {
	var msg = catchpet_msg_pb.CatchPetPart_ChangePetResponse.decode(buffer);

	//ViewManager.hideLoading()
	if ((msg.Flag != 0)) {
		console.error(("换宠失败，错误ID" + msg.Flag));
		return;
	}
	//换宠成功   删除旧模型  加载新模型  然后播放敌人攻击  更新我方UI
	CatchPetSkillManger.OnChangePet(changeObjID);
}
//请求继续战斗
CatchPetPart.ContinueFightRequest = function() {
	var request = new catchpet_msg_pb.CatchPetPart_EnterCatchFightRequest();
	request.ScenePosition = 1;
	request.HeroPosition = 1;
	ViewManager.showLoading({});
	SocketClient.callServerFun('CatchPetPart_ContinueCatch', request);
}
//继续战斗回应
CatchPetPart.ContinueFightResponse = function(buffer) {
	var msg = catchpet_msg_pb.CatchPetPart_EnterCatchFightResponse.decode(buffer);

	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		console.error(("错误ID" + msg.Flag));
		return;
	}
	CatchPetPart.EnemyPet = msg.EnemyPet;
	CatchPetPart.CurePet = msg.CurePet;
	CatchPetPart.MyTeam = msg.MyTeam;
	CatchPetPart.CatchRound = msg.CatchRound;
	//开始抓宠战斗
	CatchPetFightPanel.StartFight();
}
//逃跑发送标记
CatchPetPart.RunRequest = function() {
	var request = new catchpet_msg_pb.CatchPetPart_RunRequest();
	SocketClient.callServerFun('CatchPetPart_Escp', request);
}
CatchPetPart.RunResponse = function(buffer) {
	var msg = catchpet_msg_pb.CatchPetPart_RunResponse.decode(buffer);

	if ((msg.Flag != 0)) {
		return;
	}
}
//------------分割线--------------------------
//----以下内容为战斗部分-----------------------
//加载完毕正式开始战斗
var mCurSkillID = null;
//使用技能
CatchPetPart.UseSkill = function(skillID) {
	CatchPetPart.UseCatchSkillRequest(skillID);
}
CatchPetPart.UseCatchSkillRequest = function(skillID) {
	var request = new catchpet_msg_pb.CatchPetPart_UseCatchSkillRequest();
	request.SkillID = skillID;
	mCurSkillID = skillID;
	//ViewManager.showLoading({})
	SocketClient.callServerFun('CatchPetPart_UseSkill', request);
}
CatchPetPart.UseCatchSkillResponse = function(buffer) {
	var msg = catchpet_msg_pb.CatchPetPart_UseCatchSkillResponse.decode(buffer);

	//ViewManager.hideLoading()
	if ((msg.Flag != 0)) {
		if ((msg.Flag == 5)) {
			//console.error("敌人死亡战斗结束")
			//调用普通攻击命令    前端表现  表现完毕战斗结束
			CatchPetSkillManger.CommondNormalAttack(mCurSkillID);
			return;
		}
		if ((msg.Flag == 6)) {
			//console.error("我方死亡 调用换阵")
			//console.error("调用前端换阵表现")
			CatchPetSkillManger.CommondNormalAttack(mCurSkillID);
		}
		if (((msg.Flag != 5) && (msg.Flag != 6))) {
			console.error(("使用技能攻击失败,错误" + msg.Flag));
		}
		return;
	}
	//前端开始表现
	//调用普通攻击命令    前端表现
	CatchPetSkillManger.CommondNormalAttack(mCurSkillID);
}
//切换宠物
CatchPetPart.ChangePokemon = function() {
	CatchPetSkillManger.CommondAfterChangeAttack();
}
//使用精灵球   后端下发成功还是失败
CatchPetPart.UseBallAfterServer = function(catchResult, isRun, rewardList) {
	if ((catchResult == 1)) {
		//成功 动画不变   弹出成功窗口
		//console.error("成功，弹出捕捉成功   点击确定 退出捉宠")
		//摇一摇  然后弹出成功弹窗
		CatchPetSkillManger.OnCatchSuccess();
	} else if ((catchResult == 2) ){
		//失败 播放宠物跳出动画  显示模型  模型开始新回合 攻击还是逃跑根据后端来决定
		//console.error("失败 播放宠物跳出动画  显示模型  模型开始新回合 攻击还是逃跑根据后端来决定")
		//摇一摇 然后 播放光效 弹出怪物  继续下一回合
		rewards = rewardList;
		rewardIndex = 1;
		//失败，调用飘字
		var strShow = TranslateTool.GetText(1028);
		var resourceData = ResourceTool_GetResourceDetail(rewardList[0].RewardType, rewardList[0].RewardID);
		strShow = StringTool.ZhuanHuan(strShow, [
				resourceData.Name,
				rewardList[0].RewardCount
			]);
		CommonHUDPanel.Show(strShow, true);
		//如果奖励数量大于1，则设置定时器继续
		if ((rewardList.length > 1)) {
			//timerMgr.AddTimerEvent('CatchPetPart.ShowMoreHud', 0.5, this.ShowMoreHud, { }, false);
		} else {
			//HuoDeWuPinTiShiPanelData.Show({rewardList[0].RewardType},{rewardList[0].RewardID},{rewardList[0].RewardCount});
			CatchPetFightPanel.PlayItemsAnimation([
					rewardList[0].RewardType
				], [
					rewardList[0].RewardID
				], [
					rewardList[0].RewardCount
				]);
		}
		if (isRun) {
			CatchPetSkillManger.OnCatchFailRun();
		} else {
			CatchPetSkillManger.OnCatchFail();
		}
	} else {
		console.error("数据错误");
		return;
	}
}
CatchPetPart.ShowMoreHud = function() {
	rewardIndex = (rewardIndex + 1);
	var strShow = TranslateTool.GetText(1028);
	var resourceData = ResourceTool_GetResourceDetail(rewards[rewardIndex].RewardType, rewards[rewardIndex].RewardID);
	strShow = StringTool.ZhuanHuan(strShow, [
			resourceData.Name,
			rewards[rewardIndex].RewardCount
		]);
	CommonHUDPanel.Show(strShow, true);
	if ((rewardIndex >= rewards.length)) {
		var resType = { };
		var resId = { };
		var resCount = { };
		for (var i = 0; i != rewards.length; ++i) {
			var data = rewards[i];
			resType[i] = data.RewardType;
			resId[i] = data.RewardID;
			resCount[i] = data.RewardCount;
		}
		//显示得奖UI
		//HuoDeWuPinTiShiPanelData.Show(resType,resId,resCount);
		CatchPetFightPanel.PlayItemsAnimation(resType, resId, resCount);
		return;
	}
	//timerMgr.AddTimerEvent('CatchPetPart.ShowMoreHud', 0.5, this.ShowMoreHud, { }, false);
}
//--------------结束战斗部分代码----------------
var sceneIDs = { };
//升级提示解锁光效
CatchPetPart.OnPlayerLevelUp = function() {
	var level = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_DengJi];
	var conf_Level = LoadConfig.getConfigData(ConfigName.ZhuJue_Dengji,level);
	if ((conf_Level == null)) {
		console.error(("等级异常" + level));
		return;
	}
	if ((conf_Level.SceneID[0] != 0)) {
		sceneIDs = { };
		for (var i = 0; i != conf_Level.SceneID.length; ++i) {
			if ((conf_Level.SceneID[i] == 0)) {
				break;
			}
			sceneIDs[i] = conf_Level.SceneID[i];
		}
		var effects = [
			"Ui_JieSuoBuZhuoChangJing"
		];
		resMgr.LoadEffect(effects, this.LoadEffectOver);
	} else {
		//显示升级后的新手引导
		MainPanel.ShowLevelGuide();
	}
}
var effectObj = null;
//加载光效完毕
CatchPetPart.LoadEffectOver = function(obj) {
	effectObj = obj[0];
	//UIDepthTool_SetEffectDepth(effectObj)
	effectObj.transform.SetParent(UIManager.mPopPanelRoot);
	effectObj.transform.localPosition = Vector3.zero;
	effectObj.transform.localScale = Vector3.one;
	effectObj.active = true;
	var luabehavior = effectObj.getComponent("LuaBehaviour");
	//设置图片
	var prefab = PanleFindChild(effectObj, "guadian/prefab");
	var parent = PanleFindChild(effectObj, "guadian/Horizontal");
	var secondText = PanleFindChild(effectObj, "guadian/Image/Text_1");
	for (var i = 0; i != sceneIDs.length; ++i) {
		var newScene = newObject(prefab);
		newScene.active = true;
		// GOTool.SetParent(newScene, parent);
		// GOTool.SetVector(newScene, TransformOperateType.LocalPosition, {
		// 		x: 0,
		// 		y: 0,
		// 		z: 0
		// 	});
		// GOTool.SetVector(newScene, TransformOperateType.LocalScale, {
		// 		x: 1,
		// 		y: 1,
		// 		z: 1
		// 	});
		var sceneImage = PanleFindChild(newScene, "Image/Image");
		var frame = PanleFindChild(newScene, "Image/Image/DaoJuPrefab/Frame");
		var headicon = PanleFindChild(newScene, "Image/Image/DaoJuPrefab/Frame/icon");
		var sceneName = PanleFindChild(newScene, "Image/Image/Text");
		var cardName = PanleFindChild(newScene, "Image/Image/DaoJuPrefab/TianFuLv");
		//设置图片
		var scenceID = sceneIDs[i];
		var conf_Scene = LoadConfig.getConfigData(ConfigName.ZhuoChongChangjingPeiZhi,scenceID);
		if ((conf_Scene == null)) {
			console.error(("错误场景ID " + scenceID));
			break;
		}
		GOTool.SetImage(sceneImage, "catchpeticon", conf_Scene.Icon);
		sceneName.getComponent("Text").text = conf_Scene.Name;
		var shibingInfo = LoadConfig.getConfigData(ConfigName.ShiBing,conf_Scene.SpecialID);
		if ((shibingInfo == null)) {
			console.error(("士兵表ID为空" + sceneInfo.SpecialID));
			return;
		}
		CatchPetPart.SetFrameImage(frame, shibingInfo.Quality);
		cardName.getComponent("Text").text = shibingInfo.Name;
		var shibingIcon = LoadConfig.getConfigData(ConfigName.MoXing,shibingInfo.ModelID);
		if ((shibingIcon == null)) {
			console.error(("士兵模型表ID错误" + shibingInfo.ModelID));
			return;
		}
		GOTool.SetImage(headicon, "headicon", shibingIcon.Icon);
		if ((i == 1)) {
			var shuxingData = LoadConfig.getConfigData(ConfigName.JingLingShuXingZhanShi,conf_Scene.SpecialID);
			secondText.getComponent("Text").text = shuxingData.Show[0];
		}
	}
	//1.5秒后销毁
	//timerMgr.AddTimerEvent('CatchPetPart.RemoveEffect', 2, this.RemoveEffect, { }, false);
}
//销毁光效
CatchPetPart.RemoveEffect = function() {
	GameObject.Destroy(effectObj);
	//场景解锁结束，显示引导
	MainPanel.ShowLevelGuide();
}
//设置角表图标
CatchPetPart.SetFrameImage = function(go, quality) {
	if ((quality == 1)) {
		GOTool.SetImage(go, "common", "GY_0069_baipinzhi");
	} else if ((quality == 2) ){
		GOTool.SetImage(go, "common", "GY_0066_lvpinzhi");
	} else if ((quality == 3) ){
		GOTool.SetImage(go, "common", "GY_0068_lanpinzhi");
	} else if ((quality == 4) ){
		GOTool.SetImage(go, "common", "GY_0067_zipinzhi");
	} else if ((quality == 5) ){
		GOTool.SetImage(go, "common", "GY_0065_chengpinzhi");
	} else if ((quality == 6) ){
		GOTool.SetImage(go, "common", "GY_0064_hongpinzhi");
	} else {
		console.error(("设置边框错误，品质" + quality));
	}
}
//endregion
window.CatchPetPart = CatchPetPart;