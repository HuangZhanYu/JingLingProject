let shop_msg_pb = require('shop_msg').msg;
let ShopPart = { };

ShopPart.ShopEnum = {
	MainShop: 1,
	//主界面商店
	ArenaShop: 2,
	//荣誉币商店
	GuildShop: 3,
	//公会商店
	TalismanShop: 4,
	//护符商店
	PeakShop: 5,
	//巅峰商店

};
//主界面商品
ShopPart.GoodsList = { };
ShopPart.FreeRefreshCount = 0;
ShopPart.LastRecoverTime = 0;
ShopPart.HasUsedFreshCard = 0;
ShopPart.DailyFreshCount = { };
ShopPart.MainBuyCount = { };
//接受服务器下发信息
ShopPart.RevcShopInfo = function(buffer) {
	var msg = shop_msg_pb.ShopPart_SendShopInfo.decode(buffer);

	ShopPart.MainBuyCount = { };
	ShopPart.MainBuyCount = msg.BuyInfoList;
}
//接受服务器下发信息(公用商店)
ShopPart.RevcCommonShopInfo = function(buffer) {
	var msg = shop_msg_pb.ShopPart_SendCommonShopInfo.decode(buffer);

	ShopPart.GoodsList[msg.ShopEnum] = { };
	ShopPart.GoodsList[msg.ShopEnum] = msg.ShopList;
	ShopPart.FreeRefreshCount = msg.FreeRefreshCount;
	ShopPart.LastRecoverTime = msg.LastRecoverTime;
	ShopPart.HasUsedFreshCard = msg.HasUsedFreshCard;
	ShopPart.DailyFreshCount[msg.ShopEnum] = msg.DailyFreshCount;
}
//获取购买次数
ShopPart.GetMainBuyCount = function(id) {
	return ShopPart.MainBuyCount[(id + 1)];
}
var flagIndex = 0;
//购买商品请求
ShopPart.BuyItemRequest = function(buyIndex, enumType) {
	var request = new  shop_msg_pb.ShopPart_BuyItemRequest();
	request.Index = buyIndex;
	request.ShopEnum = enumType;
	flagIndex = buyIndex;
	flagEnum = enumType;
	ViewManager.showLoading({});
	SocketClient.callServerFun('ShopPart_BuyItem', request);
}
//购买商品回应
ShopPart.BuyItemResponse = function(buffer) {
	var msg = shop_msg_pb.ShopPart_BuyItemResponse.decode(buffer);

	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		console.error(("错误ID" + msg.Flag));
		return;
	}
	var flagEnum = msg.ShopEnum;
	if ((flagEnum == ShopPart.ShopEnum.MainShop)) {
		//刷新商店 商品信息
		ShopPopPanel.AfterBuySuccess();
		FakeShopPopPanel.AfterBuySuccess();
	} else if ((flagEnum == ShopPart.ShopEnum.GuildShop) ){
		//刷新公会战界面和工会界面
		GuildFightPanel.UpdatePoints();
		//刷新商店 商品信息
		ArenaShopPopPanel.OnAfterLoad();
	} else {
		//刷新商店 商品信息
		ArenaShopPopPanel.OnAfterLoad();
	}
	//弹出获得物品
	var goodInfo = ShopPart.GoodsList[flagEnum][flagIndex];
	HuoDeWuPinTiShiPanelData.Show([
			goodInfo.GoodsResType
		], [
			goodInfo.GoodsID
		], [
			goodInfo.GoodsCount
		]);
}
//刷新商品请求
ShopPart.RefreshItemRequest = function(enumType) {
	var request = new  shop_msg_pb.ShopPart_RefreshItemRequest();
	request.ShopEnum = enumType;
	ViewManager.showLoading({});
	SocketClient.callServerFun('ShopPart_RefreshShopInfo', request);
}
//刷新商品回应
ShopPart.RefreshItemResponse = function(buffer) {
	var msg = shop_msg_pb.ShopPart_RefreshItemResponse.decode(buffer);

	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		console.error(("错误ID" + msg.Flag));
	}
	if ((msg.ShopEnum == ShopPart.ShopEnum.MainShop)) {
		//刷新商店 商品信息
		ShopPopPanel.UpdateAllInfo();
		FakeShopPopPanel.UpdateAllInfo();
	} else {
		//刷新商店 商品信息
		ArenaShopPopPanel.OnAfterLoad();
	}
}
//获取今日剩余扫货卡次数
ShopPart.GetTodayFreshCardLeft = function() {
	//读表拿到上限
	var maxCountInfo = LoadConfig.getConfigData(ConfigName.CanShu,41);
	if ((maxCountInfo == null)) {
		console.error("参数表41，每日扫货卡上限参数错误");
		return 0;
	}
	var count = (maxCountInfo.Param[0] - ShopPart.HasUsedFreshCard);
	return count;
}
//获取今日剩余使用荣誉币刷新的次数
ShopPart.GetTodayFreshCountLeft = function(shopTypeID) {
	//读表拿到上限
	//local maxCountInfo = LoadConfig.getConfigData(ConfigName.CanShu,111) 
	//if maxCountInfo == null then
	//    console.error("参数表111，每日荣誉币刷新上限参数错误")
	//    return 0
	//end
	//local count = maxCountInfo.Param[0] + VIPTool_GetFuncCount(VIPFunc.VIPFunc_HonorShopFreshCount) - ShopPart.HasFreshHonorCount
	return ShopPart.DailyFreshCount[shopTypeID];
}
//获取今日公会刷新次数
ShopPart.GetTodayGuildLeft = function() {
	//读表拿到上限
	var maxCountInfo = LoadConfig.getConfigData(ConfigName.CanShu,111);
	if ((maxCountInfo == null)) {
		console.error("参数表111，每日荣誉币刷新上限参数错误");
		return 0;
	}
	var count = (maxCountInfo.Param[0] - ShopPart.HasFreshContriCount);
	return count;
}
//通过枚举请求商店
ShopPart.SingleShopDataRequest = function(shopTypeID) {
	var request = new  shop_msg_pb.ShopPart_SingleShopInfoRequest();
	request.ShopEnum = shopTypeID;
	ViewManager.showLoading({});
	SocketClient.callServerFun('ShopPart_SendShopDataByEnum', request);
}
//请求商店信息后返回
ShopPart.SingleShopDataResponse = function(buffer) {
	var msg = shop_msg_pb.ShopPart_SingleShopInfoResponse.decode(buffer);

	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		console.error(("错误ID" + msg.Flag));
		return;
	}
	ShopPart.GoodsList[msg.ShopEnum] = { };
	ShopPart.GoodsList[msg.ShopEnum] = msg.ShopList;
	ShopPart.DailyFreshCount[msg.ShopEnum] = msg.DailyFreshCount;
	if ((msg.ShopEnum != ShopPart.ShopEnum.MainShop)) {
		ArenaShopPopPanel.AfterRequest();
	}
}
var mainID = 0;
var mCount = 0;
//购买主界面的商品请求
ShopPart.BuyMainItemRequest = function(index, count) {
	var request = new  shop_msg_pb.ShopPart_BuyMainRequest();
	request.ItemID = index;
	request.Count = count;
	mainID = index;
	mCount = count;
	ViewManager.showLoading({});
	SocketClient.callServerFun('ShopPart_BuyMainItem', request);
}
ShopPart.BuyMainItemResponse = function(buffer) {
	var msg = shop_msg_pb.ShopPart_BuyMainResponse.decode(buffer);

	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		console.error(("错误ID" + msg.Flag));
		return;
	}
	var conf_Data = LoadConfig.getConfigData(ConfigName.ZhuJieMianShangDian,mainID);
	if ((conf_Data != null)) {
		var count = (parseInt(conf_Data.ResourceCount) * mCount);
		HuoDeWuPinTiShiPanelData.Show([
				conf_Data.ResourceType
			], [
				conf_Data.ResourceID
			], [
				count
			]);
	}
	//刷新界面
	//NewShopPopPanel.RefreshBuyInfo();
	//DigimonDetailPopPanel.OnBuySuccess();
	//CatchPetPanel.UpdateRefreshCard();
}
window.ShopPart = ShopPart;