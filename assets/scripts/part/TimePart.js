//region *.lua
//Date
//此文件由[BabeLua]插件自动生成
let time_msg_pb = require('time_msg').msg;
let TimePart = { };

TimePart.ServerTime = 0;
//同步时的服务器时间
TimePart.SynchTime = 0;
//同步时的手机时间
//服务器更新时间
TimePart.UpdateTime = function(buffer) {
	var msg = time_msg_pb.ServerTimeMessage.decode(buffer);

	TimePart.ServerTime = parseInt(msg.TimeNow);
	TimePart.SynchTime = (new Date()).getTime()/1000;
}
//获取当前的时间戳
TimePart.GetUnix = function() {
	return parseInt(((TimePart.ServerTime + (new Date()).getTime()/1000) - TimePart.SynchTime));
}
//endregion
window.TimePart = TimePart;