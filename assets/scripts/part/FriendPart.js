//region *.lua
//Date
//此文件由[BabeLua]插件自动生成
let card_msg_pb = require('card_msg').msg;
import CanShuTool from '../tool/CanShuTool';
import ViewManager from '../manager/ViewManager';

let FriendPart = { };

FriendPart.Result = {
	Success: 0,
	//成功
	IDError: 1,
	//ID错误
	NameError: 2,
	//名称错误
	HadRequestError: 3,
	//已经申请过
	MyOverLimit: 4,
	//我超过好友上限数量
	TheyOverLimit: 5,
	//对方超出好友上限数量
	BeyondWords: 6,
	//发言过长
	SpeakTooQuick: 7,
	//发言太快了
	IsMy: 8,
	//不能是自己
	PlayerNotExist: 9,
	//玩家不存在
	DataWrong: 10,
	//数据错误
	NotOpenFriend: 11,
	//未开放好友功能
	NotOnLine: 12,
	//玩家不在线
	NotOnLineOrNotExist: 13,
	//玩家不在线或者不存在
	YouAreBlack: 14,
	//你已经被拉黑了
	HasBlack: 15,
	//已经拉黑过了
	HasFriend: 16,
	//已经是好友了
	NotInBlack: 17,
	//该玩家不再拉黑列表
	NotHaveThisFriend: 18,
	//没有这个好友
	NotInRequest: 19,
	//该玩家不在申请列表
	RequestListFull: 20,
	//对方申请列表已满
	BlackListFull: 21,
	//我的黑名单已满
	HasBeenDelete: 22,
	//对方已经不是你的好友

};
FriendPart.TotalFriends = { };
//所有好友是每次请求后再获取
FriendPart.RequestFriends = { };
//好友申请是每次请求后再获取
FriendPart.BlackList = { };
//随好友请求下发
FriendPart.ChatMessage = { };
//存放所有好友的聊天消息
FriendPart.NewMessageRedPoint = { };
//存放所有好友的消息红点  通过好友名称，获取红点状态
//进入好友页面请求
FriendPart.OnShowRequest = function() {
	var request = friend_pb.FriendShowRequest();
	SocketClient.callServerFun("FriendPart_ShowRequest", request);
	ViewManager.showLoading({});
}
//进入好友页面回应  返回好友
FriendPart.OnShowReturn = function(buffer) {
	ViewManager.hideLoading();
	var msg = friend_pb.FriendShowResponse.decode(buffer);

	if ((msg.Flag != 0)) {
		console.error("接受好友消息错误，未开放好友系统");
		return;
	}
	//required	string	PlayerObjectID	= 1;//玩家唯一ID
	//required	string	PlayerName      = 2;//玩家名字
	//required	int32	PlayerLevel     = 3;//玩家等级
	//required	string	GuildName 	    = 4;//公会名称
	//required	int64	LeaveTime       = 5;//离开时间，0说明在线
	FriendPopPanel.Show();
}
//黑名单返回
FriendPart.OnBlackListReturn = function(buffer) {
	ViewManager.hideLoading();
	var msg = friend_pb.FriendBlackListResponse.decode(buffer);

	//所有好友
	FriendPart.BlackList = msg.BlackList;
}
//更新好友
FriendPart.OnUpdateTotalReturn = function(buffer) {
	var msg = friend_pb.FriendUpdateTotalResponse.decode(buffer);

	//所有好友
	FriendPart.TotalFriends = msg.FriendDatas;
}
//更新申请列表
FriendPart.OnGetNewFriendReturn = function(buffer) {
	var msg = friend_pb.FriendNewResponse.decode(buffer);

	FriendPart.RequestFriends = msg.RequestDatas;
}
//请求添加好友
//参数1，对方ObjID
FriendPart.OnAddFriendByID = function(objID) {
	var request = friend_pb.FriendAddByIDRequest();
	request.PlayerObjectID = objID;
	SocketClient.callServerFun("FriendPart_OnAddFriendByIDRequest", request);
	ViewManager.showLoading({});
}
//请求添加好友
//参数1，对方名字
FriendPart.OnAddFriendByName = function(name) {
	if (((name == null) || (name == ''))) {
		ToolTipPopPanel.Show(38);
		return;
	}
	for (var i = 0; i != FriendPart.TotalFriends.length; ++i) {
		var friendData = FriendPart.TotalFriends[i];
		if ((friendData.Name == name)) {
			ToolTipPopPanel.Show(325);
			return;
		}
	}
	var request = friend_pb.FriendAddByNameRequest();
	request.PlayerName = name;
	SocketClient.callServerFun("FriendPart_OnAddFriendByNameRequest", request);
	ViewManager.showLoading({});
}
//请求添加好友返回
FriendPart.OnAddFriendByNameReturn = function(buffer) {
	ViewManager.hideLoading();
	var msg = friend_pb.FriendAddByNameResponse.decode(buffer);

	var success = msg.Flag;
	//返回添加成功
	if ((success == FriendPart.Result.Success)) {
		//添加 成功提示
		ToolTipPopPanel.Show(232);
	} else {
		//添加不成功提示
		FriendPart.ShowErrorTips(success);
	}
}
//请求添加好友返回
FriendPart.OnAddFriendByIDReturn = function(buffer) {
	ViewManager.hideLoading();
	var msg = friend_pb.FriendAddByIDResponse.decode(buffer);

	var success = msg.Flag;
	//返回添加成功
	if ((success != 0)) {
		//console.error("添加失败，错误ID"..success);
		FriendPart.ShowErrorTips(success);
		return;
	}
	ToolTipPopPanel.Show(232);
}
//请求删除好友
FriendPart.OnDelete = function(objID) {
	var request = friend_pb.FriendDeleteRequest();
	request.PlayerObjectID = objID;
	SocketClient.callServerFun("FriendPart_OnDeleteRequest", request);
	ViewManager.showLoading({});
}
//请求删除好友返回
FriendPart.OnDeleteReturn = function(buffer) {
	ViewManager.hideLoading();
	var msg = friend_pb.FriendDeleteResponse.decode(buffer);

	var success = msg.Flag;
	//返回删除成功
	if ((success == FriendPart.Result.Success)) {
		//更新界面
		FriendPopPanel.UpdataInfoCountFriend();
		if ((msg.Name != PlayerBasePart.StrAttr[PlayerStrAttr.PlayerStrAttr_MingZi])) {
			return;
		}
		//弹出删除成功提示
		ToolTipPopPanel.Show(251);
	} else {
		if ((msg.Name != PlayerBasePart.StrAttr[PlayerStrAttr.PlayerStrAttr_MingZi])) {
			return;
		}
		//弹出删除失败提示
		FriendPart.ShowErrorTips(success);
	}
}
//请求拉黑好友
FriendPart.OnBlack = function(objID) {
	var request = friend_pb.FriendBlackRequest();
	request.PlayerObjectID = objID;
	SocketClient.callServerFun("FriendPart_OnBlackRequest", request);
	ViewManager.showLoading({});
}
//请求拉黑好友返回
FriendPart.OnBlackReturn = function(buffer) {
	ViewManager.hideLoading();
	var msg = friend_pb.FriendBlackResponse.decode(buffer);

	var success = msg.Flag;
	//返回拉黑成功
	if ((success == FriendPart.Result.Success)) {
		//弹出拉黑成功提示
		ToolTipPopPanel.Show(254);
		//更新界面
		//FriendsPanel.UpdateTotal()
		//FriendPopPanel.UpdataInfoFriend()
		FriendPopPanel.UpdataInfoCountFriend();
	} else {
		//弹出失败提示
		FriendPart.ShowErrorTips(success);
	}
}
//请求解除拉黑好友
FriendPart.OnWhite = function(objID) {
	var request = friend_pb.FriendWhiteRequest();
	request.PlayerObjectID = objID;
	SocketClient.callServerFun("FriendPart_OnWhiteRequest", request);
	ViewManager.showLoading({});
}
//请求解除拉黑好友返回
FriendPart.OnWhiteReturn = function(buffer) {
	ViewManager.hideLoading();
	var msg = friend_pb.FriendWhiteResponse.decode(buffer);

	var success = msg.Flag;
	//返回解除拉黑成功
	if ((success == FriendPart.Result.Success)) {
		//弹出提示
		ToolTipPopPanel.Show(252);
		//更新界面
		//FriendsPanel.UpdateTotal()
		//FriendPopPanel.UpdataInfoFriend()
		FriendPopPanel.UpdataInfoCountFriend();
	} else {
		//弹出提示
		FriendPart.ShowErrorTips(success);
	}
}
//接受好友请求
//参数1，好友ObjID
//参数2，操作数，0取消，1通过
FriendPart.OnAccept = function(objID, operate) {
	var request = friend_pb.FriendAcceptRequest();
	request.PlayerObjectID = objID;
	request.Operate = operate;
	ViewManager.showLoading({});
	SocketClient.callServerFun("FriendPart_OnAcceptRequest", request);
}
//接受好友回应
FriendPart.OnAcceptReturn = function(buffer) {
	ViewManager.hideLoading();
	var msg = friend_pb.FriendAcceptResponse.decode(buffer);

	var success = msg.Flag;
	//返回接受好友成功
	if ((success != 0)) {
		//console.error("接受失败，错误ID"..success);
		FriendPart.ShowErrorTips(success);
		return;
	}
	FriendPopPanel.UpdataInfoCountFriend();
	//更新主界面红点
	FriendPart.SetFriendRedPoint();
}
var fightPlayerObjID = null;
//请求切磋
FriendPart.OnFightRequest = function(objID) {
	var request = friend_pb.FriendQieChuoRequest();
	request.PlayerObjectID = objID;
	fightPlayerObjID = objID;
	SocketClient.callServerFun("FriendPart_OnFightRequest", request);
	ViewManager.showLoading({});
}
//切磋返回战斗指令
FriendPart.OnFightResponse = function(buffer) {
	ViewManager.hideLoading();
	var msg = card_msg_pb.SendLineupAllCardInfo();

	var success = msg.Group;
	if ((success != FriendPart.Result.Success)) {
		if ((success == FriendPart.Result.NotOnLine)) {
			ToolTipPopPanel.Show(255);
		} else {
			console.error(("切磋失败，错误ID" + success));
		}
		return;
	}
	//敌人的阵容
	FriendPart.StartFight(msg.CardInfoList);
}
FriendPart.StartFight = function(lineList) {
	CommonFightPanel.Show(CommonFightPanel.FightType.FriendBattle);
	var demeFightID = 1;
	//获取我军boss --取战斗配置表  
	var conf_ZhanDouPeiZhi_Data = LoadConfig.getConfigData(ConfigName.ZhanDouPeiZhi,13);
	if ((conf_ZhanDouPeiZhi_Data == null)) {
		return;
	}
	//创建我军战斗信息  如果有限制 要过滤
	var myCards = { };
	for (var i = 0; i != LineupPart.CardList.length; ++i) {
		var cardData = LineupPart.CardList[i];
		var card = cardData.CloneWenZhangCard();
		card.SetNotRespawn();
		table.insert(myCards, card);
	}
	var myBossCard = Card.NewByID(FightTool.GetMyBossID(), 1, 0);
	var respawn = PlayerAttrPart.GetSummaryAttr(PlayerAttrPart.Addition_Summary_Type.RespawnRate);
	var par = { };
	par.RespawnProb = parseInt(respawn);
	if ((conf_ZhanDouPeiZhi_Data.IsPVP == 1)) {
		par.RespawnProb = (par.RespawnProb / 2);
	}
	var myTeam = GFightManager.CreateFightTeam(tostring(demeFightID), myBossCard, myCards, 1, 1, par);
	//创建敌军战斗信息
	var enemyCards = { };
	for (var i = 0; i != lineList.length; ++i) {
		var cardMsg = lineList[i];
		var cardData = new Card(cardMsg);
		var card = cardData.CloneWenZhangCard();
		card.SetNotRespawn();
		table.insert(enemyCards, card);
	}
	//获取敌军boss
	var enemyBossCard = Card.NewByID(conf_ZhanDouPeiZhi_Data.EnemyBossID, 1, 0);
	var par = { };
	par.RespawnProb = 0;
	var enemyTeam = GFightManager.CreateFightTeam(tostring(demeFightID), enemyBossCard, enemyCards, 1, 1, par);
	//获取场景
	var scene = conf_ZhanDouPeiZhi_Data.Scene;
	//获取战斗时长
	var fightTime = conf_ZhanDouPeiZhi_Data.FightTime;
	GFightManager.Start(scene, FightSystemType.FriendBattle, (fightTime * 1000), 1, TimePart.GetUnix(), myTeam, enemyTeam, FriendPart.OnFightEnd);
}
//结束好友切磋
FriendPart.OnFightEnd = function(isWin, input) {
	if ((isWin == 1)) {
		DungeonResultPopData.ShowPanel(DungeonResultPopData.DungeonShowType.AfterFriend,true);
	} else {
		DungeonResultPopData.ShowPanel(DungeonResultPopData.DungeonShowType.AfterFriend,false);
	}
}
//跳过好友切磋
FriendPart.JumpRequest = function() {
	var request = friend_pb.FriendQieChuoRequest();
	request.PlayerObjectID = fightPlayerObjID;
	SocketClient.callServerFun("FriendPart_OnJumpRequest", request);
	ViewManager.showLoading({});
}
//跳过返回
FriendPart.JumpResponse = function(buffer) {
	ViewManager.hideLoading();
	var msg = friend_pb.JumpFriendFightResponse.decode(buffer);

	if (msg.IsWin) {
		DungeonResultPopData.ShowPanel(DungeonResultPopData.DungeonShowType.AfterFriend,true);
	} else {
		DungeonResultPopData.ShowPanel(DungeonResultPopData.DungeonShowType.AfterFriend,false);
	}
}
//获取好友信息
FriendPart.GetFriendDataBtID = function(objID) {
	for (var i = 0; i != this.TotalFriends.length; ++i) {
		var friendData = this.TotalFriends[i];
		if ((friendData.PlayerObjectID == objID)) {
			return friendData;
		}
	}
	return null;
}
//发送聊天信息  
//参数1 玩家唯一ID  
//参数2 发送内容
FriendPart.SpeakRequest = function(objID, content) {
	var request = friend_pb.FriendPart_SpeakRequest();
	request.PlayerObjectID = objID;
	//超过长度需要截取  60代表30个汉字
	content = StringTool.GetMaxLenString(content, 60);
	request.Content = content;
	ViewManager.showLoading({});
	SocketClient.callServerFun("FriendPart_SpeakToFriend", request);
}
FriendPart.SpeakResponse = function(buffer) {
	ViewManager.hideLoading();
	var msg = friend_pb.FriendPart_SpeakResponse.decode(buffer);

	if ((msg.Flag != 0)) {
		FriendPart.ShowErrorTips(msg.Flag);
		return;
	}
	FriendPopPanel.ClearText();
}
//接受好友聊天消息
FriendPart.AddMsg = function(buffer) {
	var msg = friend_pb.FriendPart_ChatMessage();

	//required 	string 				Name 		  			= 1; 			//发送者名字
	//required 	string				Content  				= 2; 			//内容
	//required 	int32				Level		  			= 3; 			//发送者等级
	//required 	int32 				HeadIconID				= 4;			//头像ID
	//required	string			    ReceiverName			= 5;			//接受者名字
	friendName = "";
	//如果发送者是自己，则接受者就是好友
	if ((msg.Name == PlayerBasePart.StrAttr[PlayerStrAttr.PlayerStrAttr_MingZi])) {
		friendName = msg.ReceiverName;
	} else {
		friendName = msg.Name;
	}
	//聊天消息key是好友的名字,value就是消息结构体
	if ((FriendPart.ChatMessage[friendName] == null)) {
		FriendPart.ChatMessage[friendName] = { };
		FriendPart.ChatMessage[friendName][0] = msg;
	} else {
		var index = (FriendPart.ChatMessage[friendName].length + 1);
		FriendPart.ChatMessage[friendName][index] = msg;
		//如果聊天消息大于30条 ，则只保留最新的30条
		var maxChatCount = CanShuTool.GetCanShuData(CanShuTool.CanShuEnum.MaxFriendChat);
		if ((maxChatCount == null)) {
			return;
		}
		if ((FriendPart.ChatMessage[friendName].length > maxChatCount.Param[0])) {
			var nowCount = FriendPart.ChatMessage[friendName].length;
			var startIndex = ((nowCount - maxChatCount.Param[0]) + 1);
			var tempData = FriendPart.ChatMessage[friendName];
			FriendPart.ChatMessage[friendName] = { };
			var index = 1;
			for (var i = startIndex; i != nowCount; ++i) {
				FriendPart.ChatMessage[friendName][index] = tempData[i];
				index = (index + 1);
			}
		}
	}
	//可以通过好友的名字，拿到所有聊天的消息
	//设置消息红点
	if ((FriendPart.NewMessageRedPoint[friendName] == null)) {
		FriendPart.NewMessageRedPoint[friendName] = { };
		FriendPart.NewMessageRedPoint[friendName] = true;
	} else {
		FriendPart.NewMessageRedPoint[friendName] = true;
	}
	//设置主界面红点和好友红点,消息红点
	RedPointPart.SetEnumActive(RedPointPart.Enum.Friend_NewMessage, true, RedPointPart.Enum.Main_Friend, RedPointPart.Enum.Main_OffenUse);
	//刷新一次列表的红点（前端自己判断是否在好友列表界面）
	FriendPopPanel.ChatInfo();
	FriendPopPanel.UpdataInfoFriend();
}
FriendPart.ShowErrorTips = function(errorCode) {
	if ((errorCode == FriendPart.Result.IDError)) {
		ToolTipPopPanel.Show(239);
	} else if ((errorCode == FriendPart.Result.NameError) ){
		ToolTipPopPanel.Show(240);
	} else if ((errorCode == FriendPart.Result.HadRequestError) ){
		ToolTipPopPanel.Show(230);
	} else if ((errorCode == FriendPart.Result.MyOverLimit) ){
		ToolTipPopPanel.Show(228);
	} else if ((errorCode == FriendPart.Result.TheyOverLimit) ){
		ToolTipPopPanel.Show(229);
	} else if ((errorCode == FriendPart.Result.BeyondWords) ){
		ToolTipPopPanel.Show(241);
	} else if ((errorCode == FriendPart.Result.SpeakTooQuick) ){
		ToolTipPopPanel.Show(242);
	} else if ((errorCode == FriendPart.Result.IsMy) ){
		ToolTipPopPanel.Show(234);
	} else if ((errorCode == FriendPart.Result.PlayerNotExist) ){
		ToolTipPopPanel.Show(230);
	} else if ((errorCode == FriendPart.Result.DataWrong) ){
		ToolTipPopPanel.Show(243);
	} else if ((errorCode == FriendPart.Result.NotOpenFriend) ){
		ToolTipPopPanel.Show(244);
	} else if ((errorCode == FriendPart.Result.NotOnLine) ){
		ToolTipPopPanel.Show(231);
	} else if ((errorCode == FriendPart.Result.NotOnLineOrNotExist) ){
		ToolTipPopPanel.Show(231);
	} else if ((errorCode == FriendPart.Result.YouAreBlack) ){
		ToolTipPopPanel.Show(235);
	} else if ((errorCode == FriendPart.Result.HasBlack) ){
		ToolTipPopPanel.Show(245);
	} else if ((errorCode == FriendPart.Result.HasFriend) ){
		ToolTipPopPanel.Show(246);
	} else if ((errorCode == FriendPart.Result.NotInBlack) ){
		ToolTipPopPanel.Show(247);
	} else if ((errorCode == FriendPart.Result.NotHaveThisFriend) ){
		ToolTipPopPanel.Show(248);
	} else if ((errorCode == FriendPart.Result.NotInRequest) ){
		ToolTipPopPanel.Show(249);
	} else if ((errorCode == FriendPart.Result.RequestListFull) ){
		ToolTipPopPanel.Show(230);
	} else if ((errorCode == FriendPart.Result.BlackListFull) ){
		ToolTipPopPanel.Show(250);
	} else if ((errorCode == FriendPart.Result.HasBeenDelete) ){
		ToolTipPopPanel.Show(257);
	}
}
FriendPart.SortFriendList = function() {
}
//阅读好友消息调用
FriendPart.SetFriendRedPoint = function() {
	var friendTopRed = false;
	for (var isNew in FriendPart.NewMessageRedPoint) {
		if (isNew) {
			friendTopRed = true;
		}
	}
	RedPointPart.SetEnumActive(RedPointPart.Enum.Friend_NewMessage, friendTopRed, RedPointPart.Enum.Main_Friend, RedPointPart.Enum.Main_OffenUse);
	//判断是否需要关闭主界面的红点(没有新消息，并且同时没有新申请，则设置红点关闭)
	if (!friendTopRed) {
		if (!RedPointPart.GetActive(RedPointPart.Enum.Friend_Request)) {
			RedPointPart.SetEnumActive(RedPointPart.Enum.Main_Friend, false, RedPointPart.Enum.Main_OffenUse);
		}
	}
}
//endregion
window.FriendPart = FriendPart;