//region *.lua
//Date
//此文件由[BabeLua]插件自动生成
let card_msg_pb = require('card_msg').msg;
let treasure_msg_pb = require('treasure_msg').msg;
import CanShuTool from '../tool/CanShuTool';
import ViewManager from '../manager/ViewManager';



let TreasurePart = { };

TreasurePart.TreasureList = [];
TreasurePart.TreasureCapacity = 0;
TreasurePart.TreasureSuitList = { };
//是否今日不显示弹窗
TreasurePart.IsNotShowToday = false;
//登陆返回信息
TreasurePart.mIsLoginReturn = false;
//装备属性缓存库
TreasurePart.TreasureDataMap = { };
//接受服务器下发数据
TreasurePart.RevcTreasureInfo = function(buffer) {
	var msg = card_msg_pb.SendTreasureAllInfo.decode(buffer);

	if ((msg.Group == 0)) {
		TreasurePart.TreasureList.length = 0;
	}
	var treasureInfo = { };
	var treasureCount = TreasurePart.TreasureList.length;
	//添加卡牌信息到卡牌列表
	for (var i = 0; i != msg.TreasureInfoList.length; ++i) {
		treasureInfo = msg.TreasureInfoList[i];
		var treasureIndex = (i + treasureCount);
		TreasurePart.TreasureList[treasureIndex] = treasureInfo;
		//console.error('下发一张卡牌'..treasureInfo.TreasureID)
	}
	TreasurePart.TreasureCapacity = msg.CurrentCapacity;
	//如果是1 则是添加
	if ((msg.Group == 1)) {
		var temp = msg.TreasureInfoList[0];
		//更新属性库Map
		TreasurePart.TreasureDataMap[temp.ObjID] = { };
		TreasurePart.TreasureDataMap[temp.ObjID].AttrOneValue = TreasurePart.CalTreasureAttrValue(temp, 1);
		TreasurePart.TreasureDataMap[temp.ObjID].AttrTwoValue = TreasurePart.CalTreasureAttrValue(temp, 2);
	}
	//未登录则设置为登录
	if (!TreasurePart.mIsLoginReturn) {
		TreasurePart.mIsLoginReturn = true;
		//初始化装备的属性
		TreasurePart.InitTreasureData();
	}
}
//服务器更新数据
TreasurePart.OnServerUpdataTreasureInfo = function(buffer) {
	var msg = card_msg_pb.UpdataTreasureInfo.decode(buffer);

	var haveCard = false;
	//查找客户端卡牌对象并且赋值
	for (var i = 0; i != TreasurePart.TreasureList.length; ++i) {
		if ((TreasurePart.TreasureList[i].ObjID == msg.Info.ObjID)) {
			TreasurePart.TreasureList[i] = { };
			TreasurePart.TreasureList[i] = msg.Info;
			haveCard = true;
		}
	}
	//如果不存在卡牌则增加一个
	if ((haveCard == false)) {
		var cardIndex = (TreasurePart.TreasureList.length + 1);
		TreasurePart.TreasureList[cardIndex] = { };
		TreasurePart.TreasureList[cardIndex] = msg.Info;
	}
	//更新属性库Map
	TreasurePart.TreasureDataMap[msg.Info.ObjID] = { };
	TreasurePart.TreasureDataMap[msg.Info.ObjID].AttrOneValue = TreasurePart.CalTreasureAttrValue(msg.Info, 1);
	TreasurePart.TreasureDataMap[msg.Info.ObjID].AttrTwoValue = TreasurePart.CalTreasureAttrValue(msg.Info, 2);
	//如果数量小于等于1  则不用排序
	if ((TreasurePart.TreasureList.length > 1)) {
		//LineupPart.BackSort()
	}
}
//服务器发来删除遗物
TreasurePart.OnServerDelTreasure = function(buffer) {
	var msg = card_msg_pb.UpdataTreasureInfo.decode(buffer);

	//查找客户端卡牌对象并且赋值
	for (var i = 0; i != TreasurePart.TreasureList.length; ++i) {
		if ((TreasurePart.TreasureList[i].ObjID == msg.Info.ObjID)) {
			table.remove(TreasurePart.TreasureList, i);
			break;
		}
	}
}
//初始化装备的属性加成
TreasurePart.InitTreasureData = function() {
	for (var i = 0; i != TreasurePart.TreasureList.length; ++i) {
		var treasureInfo = TreasurePart.TreasureList[i];
		TreasurePart.TreasureDataMap[treasureInfo.ObjID] = { };
		TreasurePart.TreasureDataMap[treasureInfo.ObjID].AttrOneValue = TreasurePart.CalTreasureAttrValue(treasureInfo, 1);
		TreasurePart.TreasureDataMap[treasureInfo.ObjID].AttrTwoValue = TreasurePart.CalTreasureAttrValue(treasureInfo, 2);
	}
}
//获取某个装备的属性库属性
TreasurePart.GetTreasureData = function(objID) {
	return TreasurePart.TreasureDataMap[objID];
}
//传入一个装备对象,返回某个加成值
TreasurePart.CalTreasureAttrValue = function(treasure, pos) {
	var treasureInfo = LoadConfig.getConfigData(ConfigName.Yiwu,treasure.TreasureID);
	if ((treasureInfo == null)) {
		console.error(("取表数据为空,ID" + treasure.TreasureID));
		return;
	}
	var baseValue = treasureInfo.EffectValue[pos];
	var upValue = treasureInfo.EffectValueUp[pos];
	var exponent = treasureInfo.MulParams[pos];
	//指数系数
	var bigNumBase = BigNumber.create(baseValue);
	var bigNumUpValue = BigNumber.create(upValue);
	var result = BigNumber.add(bigNumBase, BigNumber.mul(bigNumUpValue, treasure.Level));
	//计算完等级的，然后计算进化等级的
	if ((treasure.BeyondLevel > 0)) {
		var jinhuaID = ((treasure.TreasureID * 100) + treasure.BeyondLevel);
		var conf_JinHua = LoadConfig.getConfigData(ConfigName.YiWu_JinHua,jinhuaID);
		if ((conf_JinHua == null)) {
			console.error(("遗物进化表为空,ID" + jinhuaID));
			return;
		}
		//进化后公式 = 基础 * (10000 + 进化倍数)
		var rate = BigNumber.create((conf_JinHua.BeiShu[pos] + 10000));
		result = BigNumber.mul(result, rate);
		result = BigNumber.div(result, "10000");
	}
	//增加一个系数
	var expBig = BigNumber.create(Math.floor((Math.pow(((exponent / 10000)), treasure.Level) * 10000)));
	result = BigNumber.mul(result, expBig);
	result = BigNumber.div(result, "10000");
	//转化成显示数值
	result = BigNumber.div(result, BigNumber.create("100"));
	return BigNumber.getValue(result);
}
//传入一个装备对象,返回某个加成值(进化前)
TreasurePart.CalTreasureAttrValueBeforeEvo = function(treasure, pos) {
	var treasureInfo = LoadConfig.getConfigData(ConfigName.Yiwu,treasure.TreasureID);
	if ((treasureInfo == null)) {
		console.error(("取表数据为空,ID" + treasure.TreasureID));
		return;
	}
	var baseValue = treasureInfo.EffectValue[pos];
	var upValue = treasureInfo.EffectValueUp[pos];
	var exponent = treasureInfo.MulParams[pos];
	//指数系数
	var bigNumBase = BigNumber.create(baseValue);
	var bigNumUpValue = BigNumber.create(upValue);
	var result = BigNumber.add(bigNumBase, BigNumber.mul(bigNumUpValue, treasure.Level));
	//计算完等级的，然后计算进化等级的
	if ((treasure.BeyondLevel > 0)) {
		var jinhuaID = (((treasure.TreasureID * 100) + treasure.BeyondLevel) - 1);
		var conf_JinHua = LoadConfig.getConfigData(ConfigName.YiWu_JinHua,jinhuaID);
		if ((conf_JinHua == null)) {
			console.error(("遗物进化表为空,ID" + jinhuaID));
			return;
		}
		//进化后公式 = 基础 * (10000 + 进化倍数)
		var rate = BigNumber.create((conf_JinHua.BeiShu[pos] + 10000));
		result = BigNumber.mul(result, rate);
		result = BigNumber.div(result, "10000");
	}
	//增加一个系数
	var expBig = BigNumber.create(Math.floor((Math.pow(((exponent / 10000)), treasure.Level) * 10000)));
	result = BigNumber.mul(result, expBig);
	result = BigNumber.div(result, "10000");
	//转化成显示数值
	result = BigNumber.div(result, BigNumber.create("100"));
	return BigNumber.getValue(result);
}
//传入一个装备对象,返回某个加成值(进化后)
TreasurePart.CalTreasureAttrValueAfterEvo = function(treasure, pos) {
	var treasureInfo = LoadConfig.getConfigData(ConfigName.Yiwu,treasure.TreasureID);
	if ((treasureInfo == null)) {
		console.error(("取表数据为空,ID" + treasure.TreasureID));
		return;
	}
	var baseValue = treasureInfo.EffectValue[pos];
	var upValue = treasureInfo.EffectValueUp[pos];
	var exponent = treasureInfo.MulParams[pos];
	//指数系数
	var bigNumBase = BigNumber.create(baseValue);
	var bigNumUpValue = BigNumber.create(upValue);
	var result = BigNumber.add(bigNumBase, BigNumber.mul(bigNumUpValue, treasure.Level));
	//计算完等级的，然后计算进化等级的
	if ((treasure.BeyondLevel >= 0)) {
		var jinhuaID = (((treasure.TreasureID * 100) + treasure.BeyondLevel) + 1);
		var conf_JinHua = LoadConfig.getConfigData(ConfigName.YiWu_JinHua,jinhuaID);
		if ((conf_JinHua == null)) {
			console.error(("遗物进化表为空,ID" + jinhuaID));
			return;
		}
		//进化后公式 = 基础 * (10000 + 进化倍数)
		var rate = BigNumber.create((conf_JinHua.BeiShu[pos] + 10000));
		result = BigNumber.mul(result, rate);
		result = BigNumber.div(result, "10000");
	}
	//增加一个系数
	var expBig = BigNumber.create(Math.floor((Math.pow(((exponent / 10000)), treasure.Level) * 10000)));
	result = BigNumber.mul(result, expBig);
	result = BigNumber.div(result, "10000");
	//转化成显示数值
	result = BigNumber.div(result, BigNumber.create("100"));
	return BigNumber.getValue(result);
}
//前端假计算函数
TreasurePart.FakeLevelUp = function(confInfo, pos, level, evoLevel, id) {
	var baseValue = confInfo.EffectValue[pos];
	var upValue = confInfo.EffectValueUp[pos];
	var exponent = confInfo.MulParams[pos];
	//指数系数
	var bigNumBase = BigNumber.create(baseValue);
	var bigNumUpValue = BigNumber.create(upValue);
	var result = BigNumber.add(bigNumBase, BigNumber.mul(bigNumUpValue, level));
	//计算完等级的，然后计算进化等级的
	if ((evoLevel > 0)) {
		var jinhuaID = ((id * 100) + evoLevel);
		var conf_JinHua = LoadConfig.getConfigData(ConfigName.YiWu_JinHua,jinhuaID);
		if ((conf_JinHua == null)) {
			console.error(("遗物进化表为空,ID" + jinhuaID));
			return;
		}
		//进化后公式 = 基础 * (10000 + 进化倍数)
		var rate = BigNumber.create((conf_JinHua.BeiShu[pos] + 10000));
		result = BigNumber.mul(result, rate);
		result = BigNumber.div(result, "10000");
	}
	//增加一个系数
	var expBig = BigNumber.create(Math.floor((Math.pow(((exponent / 10000)), level) * 10000)));
	result = BigNumber.mul(result, expBig);
	result = BigNumber.div(result, "10000");
	result = BigNumber.div(result, BigNumber.create("100"));
	return BigNumber.getValue(result);
}
//排序规则
//阵容排序规则
TreasurePart.TreasureBagCom = function(a, b) {
	var aTreasure = a;
	var bTreasure = b;
	//获取表格配置
	var aConf = LoadConfig.getConfigData(ConfigName.Yiwu,aTreasure.TreasureID);
	var bConf = LoadConfig.getConfigData(ConfigName.Yiwu,bTreasure.TreasureID);
	//再更具品质排序
	if ((aConf.Quality != bConf.Quality)) {
		return (aConf.Quality > bConf.Quality);
	}
	//根据套装排序
	if ((aTreasure.TaoZhuangID != bTreasure.TaoZhuangID)) {
		return (aTreasure.TaoZhuangID > bTreasure.TaoZhuangID);
	}
	//根据进化等级排序
	if ((aTreasure.BeyondLevel != bTreasure.BeyondLevel)) {
		return (aTreasure.BeyondLevel > bTreasure.BeyondLevel);
	}
	//根据强化等级排序
	if ((aTreasure.Level != bTreasure.Level)) {
		return (aTreasure.Level > bTreasure.Level);
	}
	//最后根据ID排序
	return (aTreasure.TreasureID > bTreasure.TreasureID);
}
//装备排序
TreasurePart.TreasureSort = function() {
	//table.sort(TreasurePart.TreasureList, TreasurePart.TreasureBagCom);
}
//请求增加遗物槽
TreasurePart.AddTreasurePositionRequest = function() {
	var request = new  treasure_msg_pb.TreasurePart_AddNewPositionRequest();
	ViewManager.showLoading({});
	SocketClient.callServerFun('TreasurePart_AddNewPosition', request);
}
//回应增加遗物槽
TreasurePart.AddTreasurePositionResponse = function(buffer) {
	var msg = treasure_msg_pb.TreasurePart_AddNewPositionResponse.decode(buffer);

	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		console.error(("开启失败,错误ID" + msg.Flag));
		return;
	}
	TreasurePart.TreasureCapacity = msg.Capacity;
	//刷新界面
	MainPanel.UpdatePlayerInfo();
	TreasurePopPanel.OnRestore();
}
//上阵宝物请求    --传入的是背包里装备的KEY   添加需要删除背包的  加入宝物阵容
var addEquipID = null;
TreasurePart.AddTreasureRequest = function(objId) {
	var request = new  treasure_msg_pb.TreasurePart_AddTreasureRequest();
	request.Key = objId;
	addEquipID = BackPackPart.GetTreasureByObjectId(objId).TreasureID;
	ViewManager.showLoading({});
	SocketClient.callServerFun('TreasurePart_AddTreasureInLine', request);
}
//上阵宝物回应 
TreasurePart.AddTreasureResponse = function(buffer) {
	var msg = treasure_msg_pb.TreasurePart_AddTreasureResponse.decode(buffer);

	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		console.error(("上阵装备失败,错误ID" + msg.Flag));
		return;
	}
	//上阵成功关闭弹窗
	UIManager.ClosePopPanel();
	//显示套装激活
	TreasurePopPanel.ShowPopSuit(addEquipID);
}
var changeEquipID = null;
//换阵宝物请求   第一个是当前装备里的Key  第二个是背包里的
TreasurePart.ChangeTreasureRequest = function(equipKey, bagKey) {
	var request = new  treasure_msg_pb.TreasurePart_ChangeTreasureRequest();
	request.EquipKey = equipKey;
	request.BagKey = bagKey;
	changeEquipID = BackPackPart.GetTreasureByObjectId(bagKey).TreasureID;
	ViewManager.showLoading({});
	SocketClient.callServerFun('TreasurePart_ChangeTreasure', request);
}
//换阵宝物回应   
TreasurePart.ChangeTreasureResponse = function(buffer) {
	var msg = treasure_msg_pb.TreasurePart_ChangeTreasureResponse.decode(buffer);

	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		console.error(("换阵装备失败,错误ID" + msg.Flag));
		return;
	}
	//换阵成功关闭弹窗
	UIManager.ClosePopPanel();
	UIManager.ClosePopPanel();
	//显示套装激活
	TreasurePopPanel.ShowPopSuit(changeEquipID);
	ToolTipPopPanel.Show(72);
}
//提升遗物等级请求
TreasurePart.UpTreasureLevelRequest = function(objId, count) {
	var request = new  treasure_msg_pb.TreasurePart_UpTreasureLevelRequest();
	request.ObjID = objId;
	request.Count = count;
	ViewManager.showLoading({});
	SocketClient.callServerFun('TreasurePart_UpTreasureLevel', request);
}
//提升装备等级回应
TreasurePart.UpTreasureLevelResponse = function(buffer) {
	var msg = treasure_msg_pb.TreasurePart_UpTreasureLevelResponse.decode(buffer);

	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		//console.error("升级装备失败,错误ID"..msg.Flag)
		if ((msg.Flag == 4)) {
			ToolTipPopPanel.Show(15);
		}
		return;
	}
	//刷新装备界面
	TreasurePopPanel.LevelUpSuccess();
}
//提升装备进化等级请求
TreasurePart.UpTreasureEvoLevelRequest = function(objId) {
	var request = new  treasure_msg_pb.TreasurePart_UpTreasureEvoLevelRequest();
	request.ObjID = objId;
	ViewManager.showLoading({});
	SocketClient.callServerFun('TreasurePart_UpTreasureEvoLevel', request);
}
//提升装备进化等级回应
TreasurePart.UpTreasureEvoLevelResponse = function(buffer) {
	var msg = treasure_msg_pb.TreasurePart_UpTreasureEvoLevelResponse.decode(buffer);

	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		console.error(("进化装备失败,错误ID" + msg.Flag));
		return;
	}
	//刷新界面
	EquipInfoPanel.OnEvoSuccess();
}
//更改弹窗状态请求
TreasurePart.ChangePopStateRequest = function(isShow) {
	var request = new  treasure_msg_pb.TreasurePart_ChangePopStateRequest();
	request.IsNotShowToday = isShow;
	SocketClient.callServerFun('TreasurePart_ChangePopState', request);
}
//更改弹窗状态回应
TreasurePart.ChangePopStateResponse = function(buffer) {
	var msg = treasure_msg_pb.TreasurePart_ChangePopStateResponse.decode(buffer);

	if ((msg.Flag != 0)) {
		console.error(("更改状态失败,错误ID" + msg.Flag));
		return;
	}
	TreasurePart.IsNotShowToday = msg.IsNotShowToday;
}
//判断是否拥有该装备,并返回装备对象
TreasurePart.IsEquipped = function(treasureID) {
	var isHave = false;
	var equipObj = null;
	for (var i = 0; i != TreasurePart.TreasureList.length; ++i) {
		if ((TreasurePart.TreasureList[i].TreasureID == treasureID)) {
			isHave = true;
			equipObj = TreasurePart.TreasureList[i];
			break;
		}
	}
	return isHave, equipObj;
}
//通过唯一ID 获取装备对象
TreasurePart.GetTreasureByObjID = function(treasureKey) {
	for (var i = 0; i != TreasurePart.TreasureList.length; ++i) {
		if ((TreasurePart.TreasureList[i].ObjID == treasureKey)) {
			return TreasurePart.TreasureList[i];
		}
	}
}
//传入一个套装id  判断该装备的开启了几件套 
//参数1 套装ID
//返回值  已开启套装的数组  如果长度为0则没有开启套装  数组内容为已开启的套装编号 比如 {2}  {2，4}  {2，4，6}  {3}  这种
TreasurePart.GetSuitInfo = function(suitID) {
	var conf_YiWuTaoZhuang = LoadConfig.getConfigData(ConfigName.YiWuTaoZhuang,suitID);
	if ((conf_YiWuTaoZhuang == null)) {
		console.error(("套装ID为空" + suitID));
		return;
	}
	//存下所有套装装备的进化等级
	var evoLevelArr = { };
	//玩家拥有的套装装备数量
	var playerHaveNum = 0;
	for (var i = 0; i != conf_YiWuTaoZhuang.ZhuangBeiID.length; ++i) {
		if ((conf_YiWuTaoZhuang.ZhuangBeiID[i] != 0)) {
			//如果有 则+1
			var isHave = this.IsEquipped(conf_YiWuTaoZhuang.ZhuangBeiID[i]);
			var equipObj;
			if (isHave) {
				playerHaveNum = (playerHaveNum + 1);
				evoLevelArr[playerHaveNum] = equipObj.BeyondLevel;
			}
		}
	}
	//玩家已经开启的套装组
	var playerOpen = { };
	var index = 1;
	//拿到玩家拥有的套装数量 就可以判断开启了几个套装属性
	for (var i = 0; i != conf_YiWuTaoZhuang.NeedNum.length; ++i) {
		if ((conf_YiWuTaoZhuang.NeedNum[i] != 0)) {
			//如果玩家拥有数量大于等于这个数字 则已开启套装
			if ((playerHaveNum >= conf_YiWuTaoZhuang.NeedNum[i])) {
				playerOpen[index] = conf_YiWuTaoZhuang.NeedNum[i];
				index = (index + 1);
			}
		}
	}
	//冒泡排序 evoLevelArr 从大到小
	if ((evoLevelArr.length > 0)) {
		for (var i = 0; i != evoLevelArr.length; ++i) {
			for (var j = i; j != evoLevelArr.length; ++j) {
				if ((evoLevelArr[i] < evoLevelArr[j])) {
					var temp = evoLevelArr[i];
					evoLevelArr[i] = evoLevelArr[j];
					evoLevelArr[j] = temp;
				}
			}
		}
	}
	//玩家已经开启的套装对应的套装星级 {2，4，6}   {1,0,0}
	var openSuitLevelArr = { };
	//从已经拥有的套装中，查找套装等级
	if ((playerOpen.length > 0)) {
		for (var i = 0; i != playerOpen.length; ++i) {
			openSuitLevelArr[i] = this.GetSuitEffectLevel(playerOpen[i], evoLevelArr);
		}
	}
	return playerOpen, openSuitLevelArr;
}
//根据传入的套装数量，返回排序从大到小的前X中的最小等级
TreasurePart.GetSuitEffectLevel = function(effectCount, evoLevelArr) {
	//从排序好的等级中取出effectCount个等级,然后返回其中最小的那个
	var suitLevel = evoLevelArr[0];
	for (var i = 0; i != effectCount; ++i) {
		if ((suitLevel > evoLevelArr[i])) {
			suitLevel = evoLevelArr[i];
		}
	}
	//返回effectCount件套的星级
	return suitLevel;
}
//根据传入的唯一ID，判断强化一次的材料是否足够
TreasurePart.IsEnhanceEnough = function(objid) {
	var treasure = TreasurePart.GetTreasureByObjID(objid);
	if ((treasure == null)) {
		return false;
	}
	var conf_Treaure = LoadConfig.getConfigData(ConfigName.Yiwu,treasure.TreasureID);
	if ((conf_Treaure == null)) {
		return false;
	}
	var level = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_DengJi];
	var playerData = LoadConfig.getConfigData(ConfigName.ZhuJue_Dengji,level);
	if ((playerData == null)) {
		return false;
	}
	//判断是否主角等级限制
	if ((treasure.Level >= playerData.YiLevelMax)) {
		return false;
	}
	var playerhave = ResourceTool_GetResourceCount(conf_Treaure.ResourceType[0], conf_Treaure.ResourceID[0]);
	var cost = TreasurePart.GetMaterialCost(treasure);
	if ((cost == "error")) {
		return false;
	}
	if (BigNumber.lessThan(playerhave, cost)) {
		return false;
	}
	if ((conf_Treaure.ResourceID[1] != 0)) {
		playerhave = ResourceTool_GetResourceCount(conf_Treaure.ResourceType[1], conf_Treaure.ResourceID[1]);
		if (BigNumber.lessThan(playerhave, cost)) {
			return false;
		}
	}
	return true;
}
//根据传入的唯一ID，判断超越一次的材料是否足够
TreasurePart.IsBeyondEnough = function(objid) {
	var treasure = TreasurePart.GetTreasureByObjID(objid);
	if ((treasure == null)) {
		return false;
	}
	var maxEvo = CanShuTool.GetCanShuData(CanShuTool.CanShuEnum.EquipMaxEvoLevel);
	if ((maxEvo == null)) {
		return false;
	}
	if ((treasure.BeyondLevel >= maxEvo.Param[0])) {
		return false;
	}
	var jinhuaID = ((treasure.TreasureID * 100) + treasure.BeyondLevel);
	var conf_JinHua = LoadConfig.getConfigData(ConfigName.YiWu_JinHua,jinhuaID);
	if ((conf_JinHua == null)) {
		return false;
	}
	//判断主角等级是否达到
	if ((PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_DengJi] < conf_JinHua.PlayerLevel)) {
		return false;
	}
	//判断货币是否足够，判断装备是否足够
	var resTypeArr = { };
	var resIDArr = { };
	var resCountArr = { };
	for (var i = 0; i != conf_JinHua.ResourceType.length; ++i) {
		if ((conf_JinHua.ResourceType[i] == 0)) {
			break;
		}
		resTypeArr[i] = conf_JinHua.ResourceType[i];
		resIDArr[i] = conf_JinHua.ResourceID[i];
		resCountArr[i] = conf_JinHua.ResourceCount[i];
	}
	//装备如果不足，弹窗
	for (var i = 0; i != resTypeArr.length; ++i) {
		var isEnough = BackPackPart.IsResourceEnough(resTypeArr[i], resIDArr[i], resCountArr[i]);
		var hasCount;
		if (!isEnough) {
			return false;
		}
	}
	return true;
}
//传入装备的ID
TreasurePart.IsHaveHigherQuality = function(treasureID) {
	var treasureData = LoadConfig.getConfigData(ConfigName.Yiwu,treasureID);
	if ((treasureData == null)) {
		return false;
	}
	for (var i = 0; i != BackPackPart.mTreasureList.length; ++i) {
		var tempid = BackPackPart.mTreasureList[i].TreasureID;
		var tempData = LoadConfig.getConfigData(ConfigName.Yiwu,tempid);
		if ((tempData == null)) {
			return false;
		}
		var ishave = TreasurePart.IsEquipped(tempid);
		
		//前提是不同ID,且未装备
		if ((tempid != treasureID) && (!ishave)) {
			//如果有比他品质高的
			if ((tempData.Quality > treasureData.Quality)) {
				return true;
			}
		}
	}
	return false;
}
//计算强化消耗
TreasurePart.GetMaterialCost = function(treasure) {
	var conf_Treaure = LoadConfig.getConfigData(ConfigName.Yiwu,treasure.TreasureID);
	if ((conf_Treaure == null)) {
		return "error";
	}
	var level = (treasure.Level + 1);
	if ((level > Conf_YiWu_QiangHua.length)) {
		return "error";
	}
	var conf_Enhance = LoadConfig.getConfigData(ConfigName.YiWu_QiangHua,level);
	if ((conf_Enhance == null)) {
		return "error";
	}
	var index = conf_Treaure.Quality;
	if ((index > conf_Enhance.QualityCost.length)) {
		return "error";
	}
	return BigNumber.create(conf_Enhance.QualityCost[index]);
}
//前端假计算消耗
TreasurePart.FakeGetMaterialCost = function(confInfo, curlevel) {
	var level = (curlevel + 1);
	if ((level > Conf_YiWu_QiangHua.length)) {
		return "error";
	}
	var conf_Enhance = LoadConfig.getConfigData(ConfigName.YiWu_QiangHua,level);
	if ((conf_Enhance == null)) {
		return "error";
	}
	var index = confInfo.Quality;
	if ((index > conf_Enhance.QualityCost.length)) {
		return "error";
	}
	return BigNumber.create(conf_Enhance.QualityCost[index]);
}
//前端假计算是否材料足够
TreasurePart.FakeIsEnhanceEnough = function(confInfo, curlevel, mat1, mat2) {
	var cost = TreasurePart.FakeGetMaterialCost(confInfo, curlevel);
	if ((cost == "error")) {
		return false, "0";
	}
	if (BigNumber.lessThan(mat1, cost)) {
		return false, "0";
	}
	if ((confInfo.ResourceID[1] != 0)) {
		if (BigNumber.lessThan(mat2, cost)) {
			return false, "0";
		}
	}
	return true, cost;
}
//拿到当前装备消耗的两个材料
TreasurePart.GetCurrentMat = function(id) {
	var mat1 = "0";
	var mat2 = "0";
	var conf_Treaure = LoadConfig.getConfigData(ConfigName.Yiwu,id);
	if ((conf_Treaure == null)) {
		return mat1, mat2;
	}
	mat1 = ResourceTool_GetResourceCount(conf_Treaure.ResourceType[0], conf_Treaure.ResourceID[0]);
	if ((conf_Treaure.ResourceID[1] != 0)) {
		mat2 = ResourceTool_GetResourceCount(conf_Treaure.ResourceType[1], conf_Treaure.ResourceID[1]);
	}
	return mat1, mat2;
}
//获取装备最大的品质
TreasurePart.GetMaxQuality = function() {
	var maxQuality = 0;
	for (var i = 0; i != TreasurePart.TreasureList.length; ++i) {
		var tre = TreasurePart.TreasureList[i];
		var id = tre.TreasureID;
		var conf = LoadConfig.getConfigData(ConfigName.Yiwu,id);
		if (((conf != null) && (conf.Quality > maxQuality))) {
			maxQuality = conf.Quality;
		}
	}
	return maxQuality;
}
//获取装备品质折算的战斗等级
TreasurePart.GetFightLevel = function() {
	var quality = TreasurePart.GetMaxQuality();
	var conf = LoadConfig.getConfigData(ConfigName.ZheSuan_ZhuangBei,quality);
	if ((conf != null)) {
		return conf.Level;
	}
	return 0;
}
//获取当前最大的装备强化等级
TreasurePart.GetMaxEnhanceLv = function() {
	var level = 0;
	for (var equipObj of TreasurePart.TreasureList) {
		if ((equipObj.Level > level)) {
			level = equipObj.Level;
		}
	}
	return level;
}
//获取当前最大的装备进化等级
TreasurePart.GetMaxEvoLv = function() {
	var level = 0;
	for (var equipObj of TreasurePart.TreasureList) {
		if ((equipObj.BeyondLevel > level)) {
			level = equipObj.BeyondLevel;
		}
		
	}
	return level;
}
window.TreasurePart = TreasurePart;