let notice_msg_pb = require('notice_msg').msg;
let NoticePart = { };

NoticePart.NormalQueue = null;
//正常消息队列
NoticePart.InsertQueue = null;
//插入消息队列
/*
//消息
message Notice {
	required bool IsRoll = 1;		//是否重复滚动，不是重复滚动
	required string Content = 2;	//通知内容
	optional int64 StartTime = 3;	//开始滚动时间
	optional int64 EndTime = 4;		//结束滚动时间
	optional bool Insert = 5;		//是否插入
}

//消息
message NoticeResponse {
	repeated Notice Datas = 1;	//所有数据
}*/
NoticePart.Init = function() {
	if ((NoticePart.NormalQueue == null)) {
		NoticePart.NormalQueue = new MCQueue();
	}
	if ((NoticePart.InsertQueue == null)) {
		NoticePart.InsertQueue = new MCQueue();
	}
}
//收到通知消息
NoticePart.OnNoticeResponse = function(buffer) {
	var msg = notice_msg_pb.NoticeResponse.decode(buffer);

	NoticePart.Init();
	var datas = msg.Datas;
	if (((datas != null) && (datas.length > 0))) {
		for (var i = 0; i != datas.length; ++i) {
			var data = datas[i];
			if (data.Insert) {
				NoticePart.InsertQueue.Push(data);
			} else {
				NoticePart.NormalQueue.Push(data);
			}
		}
	} else {
		return;
	}
	//显示
	NoticePart.ShowNextNotice();
}
//收到清空消息
NoticePart.OnNoticeClearResponse = function(buffer) {
	var msg = notice_msg_pb.NoticeResponse.decode(buffer);

	NoticePart.NormalQueue = new MCQueue();
	NoticePart.InsertQueue = new MCQueue();
}
var index = 1;
//显示下一个公告
NoticePart.ShowNextNotice = function() {

	//优先判断插入队列中的公告
	var notice = null;
	if ((NoticePart.InsertQueue.Count() > 0)) {
		notice = NoticePart.InsertQueue.Back();
	} else {
		return;
	}
	//判断时间是否已经超过
	if (((TimePart.GetUnix() > parseInt(notice.StartTime)) && (TimePart.GetUnix() < parseInt(notice.EndTime)))) {
		//通过弹幕播放公告
		var msg = { };
		msg.Content = notice.Content[index];
		if (((msg.Content == "") || (msg.Content == null))) {
			msg.Content = notice.Content[0];
			index = 1;
		}
		index = (index + 1);
		if ((index == 4)) {
			index = 1;
		}
		msg.Name = "";
		msg.ChatType = ChatPart.ChatType.ChatPartype_Total;
		//显示弹幕
		if ((ChatPopPanel_IsOpenDanMu && GOTool.IsDestroy(DanMuPopPanel.GetObj()))) {
			DanMuPopPanel.ShowSystemGG(msg);
		} else if (ChatPopPanel_IsOpenDanMu && (!GOTool.IsDestroy(DanMuPopPanel.GetObj())) ){
			DanMuPopPanel.AddMsg(msg);
		}
		//没有过期且滚动则定时显示下一条
		//AddTimerEvent('NoticePart.ShowNextNotice', 15, this.ShowNextNotice, { }, false);
		return;
	}
	//过期
	NoticePart.InsertQueue.Pop();
}
window.NoticePart = NoticePart;