let achievement_msg_pb = require('achievement_msg').msg;
let AchievementPart = { };
AchievementPart.Progress = { };
//成就等级数组
const AchievementType = {
	AchievementType_TopGuanQia: 1,
	//历史最高关卡
	AchievementType_SumGuanQia: 2,
	//累计通关关卡
	AchievementType_EnhanceCount: 3,
	//训练卡牌
	AchievementType_Dungeon1: 4,
	//副本1通关多少层
	AchievementType_Dungeon2: 5,
	//副本2通关多少层
	AchievementType_Dungeon3: 6,
	//副本3通关多少层
	AchievementType_Dungeon4: 7,
	//副本4通关多少层
	AchievementType_Dungeon5: 8,
	//副本5通关多少层
	AchievementType_Dungeon6: 9,
	//副本6通关多少层
	AchievementType_Dungeon7: 10,
	//副本7通关多少层
	AchievementType_FightTower: 11,
	//战斗塔通关多少层
	AchievementType_ResetNum: 12,
	//重置次数
	AchievementType_CatchPetOrange: 13,
	//橙色抓宠大师
	AchievementType_CatchPetPurple: 14,
	//紫色抓宠大师
	AchievementType_CatchPetBlue: 15,
	//蓝色抓宠大师
	AchievementType_CatchPetGreen: 16,
	//绿色抓宠大师
	AchievementType_TotalMedals: 17,
	//累计获得文章
	AchievementType_TopRank: 18,
	//最高排名
	AchievementType_GBossFtTimes: 19,
	//道馆boss累计挑战次数
	AchievementType_EpStrengthenLev: 20,
	//装备强化等级
	AchievementType_EpBeyondLev: 21,
	//装备超越等级
	AchievementType_EvStrengthenLev: 22,
	//精灵强化等级
	AchievementType_EvBeyondLev: 23,
	//精灵超越等级
	AchievementType_MStoneCount: 24,
	//合成月石数量
	AchievementType_UpMStoneLev: 25,
	//升级月石等级
	AchievementType_JumpMStoneLev: 26,
	//月石跃升
	AchievementType_MegaElvesCount: 27,
	//百万进化精灵数量
	AchievementType_LandgraveRob: 28,
	//道馆战掠夺
	AchievementType_LandgraveSeize: 29,
	//道馆战占领
	AchievementType_PlateauPassed: 30,
	//精通塔通关第多少章

};
//接受服务器成就信息
AchievementPart.OnAchievementData = function(buffer) {
	var msg = achievement_msg_pb.AchievementData.decode(buffer);
	AchievementPart.Progress = [];
	AchievementPart.Progress = msg.Progress;
	AchievementPart.Progress.splice(0,1);
	//table.remove(AchievementPart.Progress, 1);
	//MainPanel.IsPanDuanChengJiu();
}
//领取成就
AchievementPart.GetAchievementReward = function(achievementType) {
	//发送消息给服务器
	var msg = new achievement_msg_pb.AchievementGetRequest();
	msg.Type = achievementType;
	SocketClient.callServerFun("AchievementPart_GetRequest", msg);
}
//收到服务器领取成就成功的消息
AchievementPart.GetAchievementResponse = function(buffer) {
	var msg = achievement_msg_pb.AchievementGetResponse.decode(buffer);
	//是否成功
	var susseed = msg.Susseed;
	var typ = msg.Type;
	var progress = msg.Progress;
	if (!susseed) {
		return;
	}
	var id = ((typ * 1000) + progress);
	var data = LoadConfig.getConfigData(ConfigName.Chengjiu,id);
	if ((data == null)) {
		console.error(("Conf_Chengjiu表数据为空，此ID为：" + id));
		return;
	}
	var resType = { };
	var resID = { };
	var resCount = { };
	for (var i = 0; i != data.ResourceType.length; ++i) {
		if ((data.ResourceType[i] <= 0)) {
			break;
		}
		resType[(resType.length + 1)] = data.ResourceType[i];
		resID[(resID.length + 1)] = data.ResourceID[i];
		resCount[(resCount.length + 1)] = data.ResourceCount[i];
	}
	AchievementPart.Progress[typ] = progress;
	//HuoDeWuPinTiShiPanelData.Show(resType, resID, resCount);
	//刷新界面
	//MainPanel.IsPanDuanChengJiu();
	//ChengJiuPopPanel.UpdateInfo();
}
//根据任务类型获取进度
AchievementPart.GetMaxTargetNum = function(taskType) {
	if ((taskType == AchievementType.AchievementType_TopGuanQia)) {
		//历史最高关卡
		return BigNumber.create(GuanQiaPart.HistoryTop);
	} else if ((taskType == AchievementType.AchievementType_SumGuanQia) ){
		//累计通关关卡
		return BigNumber.create(PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_TotalPassGuanQiaLevel]);
	} else if ((taskType == AchievementType.AchievementType_EnhanceCount) ){
		//训练卡牌
		return BigNumber.create(PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_EnhanceCount]);
	} else if ((taskType == AchievementType.AchievementType_Dungeon1) ){
		//副本1通关多少层
		return BigNumber.create(DungeonPart.GetTopRound(1));
	} else if ((taskType == AchievementType.AchievementType_Dungeon2) ){
		//副本2通关多少层
		return BigNumber.create(DungeonPart.GetTopRound(2));
	} else if ((taskType == AchievementType.AchievementType_Dungeon3) ){
		//副本3通关多少层
		return BigNumber.create(DungeonPart.GetTopRound(3));
	} else if ((taskType == AchievementType.AchievementType_Dungeon4) ){
		//副本4通关多少层
		return BigNumber.create(DungeonPart.GetTopRound(4));
	} else if ((taskType == AchievementType.AchievementType_Dungeon5) ){
		//副本5通关多少层
		return BigNumber.create(DungeonPart.GetTopRound(5));
	} else if ((taskType == AchievementType.AchievementType_Dungeon6) ){
		//副本6通关多少层
		return BigNumber.create(DungeonPart.GetTopRound(6));
	} else if ((taskType == AchievementType.AchievementType_Dungeon7) ){
		//副本7通关多少层
		return BigNumber.create(DungeonPart.GetTopRound(7));
	} else if ((taskType == AchievementType.AchievementType_FightTower) ){
		//战斗塔通关多少层
		return BigNumber.create(PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_ZhanDouTaLiShiZuiGao]);
	} else if ((taskType == AchievementType.AchievementType_ResetNum) ){
		//重置次数
		return BigNumber.create(PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_RevivalNumber]);
	} else if ((taskType == AchievementType.AchievementType_CatchPetOrange) ){
		//橙色抓宠大师
		return BigNumber.create(PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_CatchPetOrange]);
	} else if ((taskType == AchievementType.AchievementType_CatchPetPurple) ){
		//紫色抓宠大师
		return BigNumber.create(PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_CatchPetPurple]);
	} else if ((taskType == AchievementType.AchievementType_CatchPetBlue) ){
		//蓝色抓宠大师
		return BigNumber.create(PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_CatchPetBlue]);
	} else if ((taskType == AchievementType.AchievementType_CatchPetGreen) ){
		//绿色抓宠大师
		return BigNumber.create(PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_CatchPetGreen]);
	} else if ((taskType == AchievementType.AchievementType_TotalMedals) ){
		//累计获得文章
		return BigNumber.create(PlayerBasePart.StrAttr[PlayerStrAttr.PlayerStrAttr_WenZhang]);
	} else if ((taskType == AchievementType.AchievementType_TopRank) ){
		return BigNumber.create(PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_HeighestRank]);
	} else if ((taskType == AchievementType.AchievementType_GBossFtTimes) ){
		return BigNumber.create(GuildPart.TotalFightTimes);
	} else if ((taskType == AchievementType.AchievementType_LandgraveRob) ){
		return BigNumber.create(LandgravePart.TotalRobTimes);
	} else if ((taskType == AchievementType.AchievementType_LandgraveSeize) ){
		return BigNumber.create(LandgravePart.MaxSeizeNum);
	} else if ((taskType == AchievementType.AchievementType_EpStrengthenLev) ){
		//装备强化等级
		return BigNumber.create(TreasurePart.GetMaxEnhanceLv());
	} else if ((taskType == AchievementType.AchievementType_EpBeyondLev) ){
		//装备超越等级
		return BigNumber.create(TreasurePart.GetMaxEvoLv());
	} else if ((taskType == AchievementType.AchievementType_EvStrengthenLev) ){
		//精灵强化等级
		return BigNumber.create(LineupPart.GetCardMaxGoldLevel());
	} else if ((taskType == AchievementType.AchievementType_EvBeyondLev) ){
		//精灵超越等级
		return BigNumber.create(LineupPart.GetMaxEvoLv());
	} else if ((taskType == AchievementType.AchievementType_MStoneCount) ){
		//合成月石数量
		return BigNumber.create(MegaEvoPart.StoneList.length);
	} else if ((taskType == AchievementType.AchievementType_UpMStoneLev) ){
		//升级月石等级
		return BigNumber.create(MegaEvoPart.GetLevelUpCount());
	} else if ((taskType == AchievementType.AchievementType_JumpMStoneLev) ){
		//月石跃升
		return BigNumber.create(MegaEvoPart.GetJumpCount());
	} else if ((taskType == AchievementType.AchievementType_MegaElvesCount) ){
		//百万进化精灵数量
		return BigNumber.create(LineupPart.GetMegaCardCount());
	} else if ((taskType == AchievementType.AchievementType_LandgraveRob) ){
		//道馆战掠夺
		return BigNumber.create(LandgravePart.TotalRobTimes);
	} else if ((taskType == AchievementType.AchievementType_LandgraveSeize) ){
		//道馆战占领
		return BigNumber.create(LandgravePart.MaxSeizeNum);
	} else if ((taskType == AchievementType.AchievementType_PlateauPassed) ){
		//精通塔通关第多少章
		return BigNumber.create(PlateauPart.GetPassedChapter());
	}
	return 0;
}
//判断是否有成就红点
AchievementPart.IsChengJiuHD = function() {
	if (!FunctionOpenTool.IsFunctionOpen(FunctionOpenTool.FuntionEnum.ChengJiu)) {
		return false;
	}
	var isChengJiuHD = false;
	for (var i = 0; i != AchievementPart.Progress.length; ++i) {
		var id = (((i * 1000) + AchievementPart.Progress[i]) + 1);
		var data = LoadConfig.getConfigData(ConfigName.Chengjiu,id);
		if ((data != null)) {
			//最大任务限制
			var maxNum = BigNumber.create(data.TargetNum);
			//当前进度
			var curJinDu = AchievementPart.GetMaxTargetNum(data.TaskType);
			if ((data.TaskType == AchievementType.AchievementType_TopRank)) {
				if ((curJinDu != "0") && BigNumber.GreaterThanOrEqualTo(maxNum, curJinDu)) {
					return true;
				}
			} else if (BigNumber.GreaterThanOrEqualTo(curJinDu, maxNum) ){
				return true;
			}
		}
	}
	return isChengJiuHD;
}

window.AchievementPart = AchievementPart;