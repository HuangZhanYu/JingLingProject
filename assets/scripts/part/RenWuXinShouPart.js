//新手任务部件
//功能描述：引导新手玩家的早期任务目标
let xinshou_renwu_pb = require('xinshou_renwu').msg;

const RenWuXinShouType = {
	XinShouRenWuType_LvupGoldTask: 1,
	//升级金币任务
	XinShouRenWuType_LvupCard: 2,
	//升级卡牌
	XinShouRenWuType_ResetNum: 3,
	//重置次数
	XinShouRenWuType_TrainingCard: 4,
	//训练卡牌
	XinShouRenWuType_TopGuanQia: 5,
	//历史最高关卡
	XinShouRenWuType_TuJianJiHuo: 6,
	//图鉴激活
	XinShouRenWuType_EvoCardCount: 7,
	//拥有进化+X的精灵X个  目标数量=品质*100000+进化等级*10000+达成数量

};
const ResetTaskState = {
	NotFinish: 0,
	//未完成
	CanGet: 1,
	//可领取
	Get: 2,
	//已领取

};
let RenWuXinShouPart = { };

RenWuXinShouPart.CureTaskID = 0;
//当前任务ID
RenWuXinShouPart.ResetTaskID = 0;
//当前重置任务的ID
RenWuXinShouPart.ResetTargetNum = 0;
//当前重置任务的进度
RenWuXinShouPart.ResetTaskState = { };
//当前重置任务的状态
//接收服务任务信息
RenWuXinShouPart.OnRecvTaskInfo = function(buffer) {
	var msg = xinshou_renwu_pb.XinShouRenWuInfo.decode(buffer);

	RenWuXinShouPart.CureTaskID = msg.CureRenWuID;
}
//获取当前新手任务ID
RenWuXinShouPart.GetCureTaskID = function() {
	//判断当前任务ID
	if ((RenWuXinShouPart.CureTaskID == 0)) {
		return 0;
	}
	//获取任务配置
	var renwu_conf = LoadConfig.getConfigData(ConfigName.RenWu_XinShou,RenWuXinShouPart.CureTaskID);
	if ((renwu_conf == null)) {
		return 0;
	}
	return RenWuXinShouPart.CureTaskID;
}
//根据任务类型获取进度
RenWuXinShouPart.GetMaxTargetNum = function(taskType) {
	if ((taskType == RenWuXinShouType.XinShouRenWuType_LvupGoldTask)) {
		return TaskPart.GetTaskMaxLevel();
	} else if ((taskType == RenWuXinShouType.XinShouRenWuType_LvupCard) ){
		return LineupPart.GetCardMaxGoldLevel();
	} else if ((taskType == RenWuXinShouType.XinShouRenWuType_ResetNum) ){
		return PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_RevivalNumber];
	} else if ((taskType == RenWuXinShouType.XinShouRenWuType_TrainingCard) ){
		return LineupPart.GetCardMaxMedalLevel();
	} else if ((taskType == RenWuXinShouType.XinShouRenWuType_TopGuanQia) ){
		return GuanQiaPart.HistoryTop;
	} else if ((taskType == RenWuXinShouType.XinShouRenWuType_TuJianJiHuo) ){
		return TuJianPart.TotalCardIDs.length;
	} else if ((taskType == RenWuXinShouType.XinShouRenWuType_EvoCardCount) ){
		var renwu_conf = LoadConfig.getConfigData(ConfigName.RenWu_XinShou,RenWuXinShouPart.CureTaskID);
		if ((renwu_conf == null)) {
			return 0;
		}
		var quality = Math.floor((renwu_conf.TargetNum / 100000));
		var evo = Math.floor((((renwu_conf.TargetNum - (quality * 100000))) / 10000));
		return RenWuXinShouPart.GetEvoCardNum(quality, evo);
	}
	return 0;
}
//特殊获取 获取精灵进化数量
RenWuXinShouPart.GetEvoCardNum = function(quality, evo) {
	var result = 0;
	//遍历阵容中的卡牌，是否有进化等级>=evo 并且  品质==quality的
	for (var i = 0; i != LineupPart.CardList.length; ++i) {
		var conf_shibing = LoadConfig.getConfigData(ConfigName.ShiBing,LineupPart.CardList[i]).CardID;
		if ((conf_shibing != null)) {
			//符合条件+1
			if (((conf_shibing.Quality == quality) && (conf_shibing.EVO >= evo))) {
				result = (result + 1);
			}
		}
	}
	//遍历背包中的
	for (var i = 0; i != BackPackPart.mCardList.length; ++i) {
		var conf_shibing = LoadConfig.getConfigData(ConfigName.ShiBing,BackPackPart.mCardList[i]).CardID;
		if ((conf_shibing != null)) {
			if (((conf_shibing.Quality == quality) && (conf_shibing.EVO >= evo))) {
				result = (result + BackPackPart.mCardList[i].Count);
			}
		}
	}
	return result;
}
//获取当前任务进度
RenWuXinShouPart.GetCureTargetNum = function() {
	//判断当前任务ID
	if ((RenWuXinShouPart.CureTaskID == 0)) {
		return 0;
	}
	//获取任务配置
	var renwu_conf = LoadConfig.getConfigData(ConfigName.RenWu_XinShou,RenWuXinShouPart.CureTaskID);
	if ((renwu_conf == null)) {
		return 0;
	}
	//获取任务当前进度
	var cureTargetNum = RenWuXinShouPart.GetMaxTargetNum(renwu_conf.TaskType);
	return cureTargetNum;
}
//领取任务奖励
RenWuXinShouPart.GetTaskReward = function() {
	//判断任务id
	if ((RenWuXinShouPart.CureTaskID == 0)) {
		return;
	}
	//获取任务配置
	var renwu_conf = LoadConfig.getConfigData(ConfigName.RenWu_XinShou,RenWuXinShouPart.CureTaskID);
	if ((renwu_conf == null)) {
		return;
	}
	//获取任务当前进度
	var cureTargetNum = RenWuXinShouPart.GetMaxTargetNum(renwu_conf.TaskType);
	var targetNum = renwu_conf.TargetNum;
	if ((renwu_conf.TaskType == RenWuXinShouType.XinShouRenWuType_EvoCardCount)) {
		targetNum = RenWuXinShouPart.GetCount(renwu_conf.TargetNum);
	}
	//判断进度
	if ((cureTargetNum < targetNum)) {
		ToolTipPopPanel.Show(200);
		return;
	}
	//发送消息给服务器
	var request = new xinshou_renwu_pb.XinShouRenWuRequest();
	SocketClient.callServerFun("XinShouRenWuPart_GetRewardRequest", request);
}
//计算进化数量
RenWuXinShouPart.GetCount = function(count) {
	var quality = Math.floor((count / 100000));
	var evo = Math.floor((((count - (quality * 100000))) / 10000));
	var needCount = ((count - (quality * 100000)) - (evo * 10000));
	return needCount;
}
//服务器返回领奖结果
RenWuXinShouPart.OnRecvGetReward = function(buffer) {
	//解析消息
	var msg = xinshou_renwu_pb.XinShouRenWuResponse.decode(buffer);

	//显示奖励UI
	RenWuXinShouPart.CureTaskID = msg.CureRenWuID;
	//显示奖励界面和刷新UI
	XiaoMuBiaoPanel.Show();
	var data = LoadConfig.getConfigData(ConfigName.RenWu_XinShou,(RenWuXinShouPart.CureTaskID - 1));
	if ((data == null)) {
		console.error(("Conf_RenWu_XinShou数据为空，此ID：" + ((RenWuXinShouPart.CureTaskID - 1))));
		return;
	}
	var resType = { };
	var resID = { };
	var resCount = { };
	for (var i = 0; i != data.ResourceType.length; ++i) {
		if ((data.ResourceType[i] <= 0)) {
			break;
		}
		resType[(resType.length + 1)] = data.ResourceType[i];
		resID[(resID.length + 1)] = data.ResourceID[i];
		resCount[(resCount.length + 1)] = data.ResourceCount[i];
	}
	HuoDeWuPinTiShiPanelData.Show(resType, resID, resCount);
}
//接受服务器重置任务信息
RenWuXinShouPart.OnRecvRestTaskInfo = function(buffer) {
	var msg = xinshou_renwu_pb.ResetRenWuInfo.decode(buffer);

	RenWuXinShouPart.ResetTaskID = msg.CureRenWuID;
	RenWuXinShouPart.ResetTargetNum = msg.TargetNum;
	RenWuXinShouPart.ResetTaskState = msg.RenWuState;
}
//领取重置任务
RenWuXinShouPart.GetResetTaskReward = function(taskID) {
	//获取任务配置
	var renwu_conf = LoadConfig.getConfigData(ConfigName.Huodong_ChongZhi,taskID);
	if ((renwu_conf == null)) {
		return;
	}
	if ((RenWuXinShouPart.ResetTaskState[taskID] != ResetTaskState.CanGet)) {
		return;
	}
	//发送消息给服务器
	var request = new xinshou_renwu_pb.ResetRenWuRequest();
	request.RenWuID = taskID;
	SocketClient.callServerFun("XinShouRenWuPart_GetRewardResetRequest", request);
}
//收到服务器领取重置任务成功的消息
RenWuXinShouPart.GetResetTaskResponse = function(buffer) {
	var msg = xinshou_renwu_pb.ResetRenWuResponse.decode(buffer);

	var taskID = msg.CureRenWuID;
	//刷新界面
	ChongZhiDaRenPanel.UpdateInfo();
	//检查重置达人红点
	MainPanel.IsDaRenHongDian();
	var data = LoadConfig.getConfigData(ConfigName.Huodong_ChongZhi,taskID);
	if ((data == null)) {
		console.error(("Conf_Huodong_ChongZhi表数据为空，此ID为：" + taskID));
		return;
	}
	var resTyp = { };
	var resID = { };
	var resCount = { };
	for (var i = 0; i != data.ResourceType.length; ++i) {
		if ((data.ResourceType[i] <= 0)) {
			break;
		}
		resTyp[(resTyp.length + 1)] = data.ResourceType[i];
		resID[(resID.length + 1)] = data.ResourceID[i];
		resCount[(resCount.length + 1)] = data.ResourceCount[i];
	}
	HuoDeWuPinTiShiPanelData.Show(resTyp, resID, resCount);
}
//获取是否有重置任务可领取
RenWuXinShouPart.GetResetTaskCanGet = function() {
	var canGet = false;
	for (var i = 0; i != RenWuXinShouPart.ResetTaskState.length; ++i) {
		if ((RenWuXinShouPart.ResetTaskState[i] == ResetTaskState.CanGet)) {
			CanGet = true;
			return CanGet;
		}
	}
	return canGet;
}
window.RenWuXinShouPart = RenWuXinShouPart;