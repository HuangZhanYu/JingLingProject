let landgrave_msg_pb = require('landgrave_msg').msg;

let LandgravePart = { };
LandgravePart.Map = { };

LandgravePart.Records = { };
LandgravePart.FightCallback = null;
LandgravePart.MyPoint = 0;
//我的积分
LandgravePart.GuildPoint = 0;
//公会积分
LandgravePart.GridsInfo = { };
LandgravePart.UnlockLands = { };
LandgravePart.TotalRow = 0;
//总行数
LandgravePart.TotalColumn = 0;
//总列数
LandgravePart.ReviveInfo = { };
//复活队列
LandgravePart.OccupyGrid = { };
//占领的格子
LandgravePart.CardList = [
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0
];
//当前挑战的卡牌数组
LandgravePart.TotalRobTimes = 0;
LandgravePart.MaxSeizeNum = 0;
LandgravePart.GiveUpEndTime = 0;
LandgravePart.IsOpen = false;
//道馆战当前是否开启中
LandgravePart.IsBoxShow = false;
//可获取是否显示
LandgravePart.EndTime = 0;
LandgravePart.LstRobTimes = 0;
//剩余掠夺次数
var mResultPanelType = null;
//战斗结束显示的类型
var isOpen = false;
//获取当前可以上阵的精灵
LandgravePart.AlternateCardList = function() {
	var tab = { };
	for (var forinvar in ipairs(BackPackPart.mCardList)) {
		
		var card = forinvar.card;
		{
			if ((tab[card.CardID] == null)) {
				tab[card.CardID] = card.Count;
			} else {
				tab[card.CardID] = (tab[card.CardID] + card.Count);
			}
		}
	}
	for (var forinvar in ipairs(LineupPart.CardList)) {
		
		var card = forinvar.card;
		{
			if ((tab[card.CardID] == null)) {
				tab[card.CardID] = 1;
			} else {
				tab[card.CardID] = (tab[card.CardID] + 1);
			}
		}
	}
	for (var forinvar in ipairs(LandgravePart.ReviveInfo)) {
		
		var item = forinvar.item;
		{
			for (var forinvar in ipairs(item.Lineup)) {
				
				var id = forinvar.id;
				{
					if ((tab[id] != null)) {
						tab[id] = (tab[id] - 1);
					}
				}
			}
		}
	}
	for (var forinvar in ipairs(LandgravePart.OccupyGrid)) {
		
		var item = forinvar.item;
		{
			for (var forinvar in ipairs(item.Lineup)) {
				
				var id = forinvar.id;
				{
					if ((tab[id] != null)) {
						tab[id] = (tab[id] - 1);
					}
				}
			}
		}
	}
	var cardList = { };
	var index = 1;
	for (var cardid in tab) {
		var count = tab[cardid];
		{
			if ((count > 0)) {
				cardList[index] = { };
				cardList[index].CardID = cardid;
				cardList[index].Count = count;
				index = (index + 1);
			}
		}
	}
	return cardList;
}
//登录同步
LandgravePart.LoginSync = function(buffer) {
	var msg = landgrave_msg_pb.LandgravePart_Sync.decode(buffer);

	LandgravePart.TotalRobTimes = msg.TotalRobTimes;
	LandgravePart.MaxSeizeNum = msg.MaxSeizeNum;
	LandgravePart.UnlockLands = msg.UnlockLands;
}
//获取地图信息
LandgravePart.GetInfo = function() {
	if ((GuildPart.GuildID == "")) {
		return;
	}
	var req = new  landgrave_msg_pb.LandgravePart_InfoRequest();
	req.Appoint = false;
	isOpen = true;
	SocketClient.callServerFun("LandgravePart_GetInfo", req);
	ViewManager.showLoading({});
}
//获取地图信息回调
LandgravePart.InfoResp = function(buffer) {
	ViewManager.hideLoading();
	var ans = landgrave_msg_pb.LandgravePart_InfoResponse.decode(buffer);

	LandgravePart.MyPoint = ans.Point;
	LandgravePart.GuildPoint = ans.GuildPoint;
	LandgravePart.GridsInfo = { };
	LandgravePart.GridsInfo = ans.CatGrids;
	//地图信息(数组)
	LandgravePart.TotalRow = ans.Height;
	LandgravePart.TotalColumn = ans.Weight;
	LandgravePart.ReviveInfo = { };
	//复活队列
	LandgravePart.ReviveInfo = ans.Lineup;
	LandgravePart.OccupyGrid = { };
	//占领的格子
	LandgravePart.OccupyGrid = ans.SeizeGrids;
	LandgravePart.GiveUpEndTime = ans.GvCDEndTime;
	//放弃的结束时间
	GuildPart.GuildLevel = ans.GuildLevel;
	LandgravePart.IsBoxShow = ans.HasOutput;
	LandgravePart.EndTime = ans.EndTime;
	//下发结束时间
	LandgravePart.LstRobTimes = ans.LstRobTimes;
	if (isOpen) {
		GuildFightPanel.ShowAfterRequest();
		isOpen = false;
		return;
	}
	GuildFightPanel.RefreshAfterRequest();
}
//通过位置获取地图信息
LandgravePart.GetInfoByPos = function(pos) {
	if ((GuildPart.GuildID == "")) {
		return;
	}
	var req = new  landgrave_msg_pb.LandgravePart_InfoRequest();
	req.Appoint = true;
	req.Pos.X = pos.X;
	req.Pos.Y = pos.Y;
	SocketClient.callServerFun("LandgravePart_GetInfo", req);
}
//查看格子信息
LandgravePart.CatGrid = function(x, y) {
	if ((GuildPart.GuildID == "")) {
		return;
	}
	LandgravePart.CardList = [
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0,
		0
	];
	//重置阵容数据
	var req = new  landgrave_msg_pb.LandgravePart_CatGridRequest();
	req.Pos.X = x;
	req.Pos.Y = y;
	SocketClient.callServerFun("LandgravePart_CatGrid", req);
	ViewManager.showLoading({});
}
//查看格子信息回调
LandgravePart.CatGridResp = function(buffer) {
	ViewManager.hideLoading();
	var ans = landgrave_msg_pb.LandgravePart_CatGridResponse.decode(buffer);

	//打开格子
	GuildFightPanel.CheckGridSuccess(ans);
}
var enumData = null;
//获取数据(不包括格子数据)
LandgravePart.GetInfoNoGrids = function(requestEnum) {
	if ((GuildPart.GuildID == "")) {
		return;
	}
	enumData = requestEnum;
	//存下哪个界面请求
	var req = new  landgrave_msg_pb.LandgravePart_InfoNoGridsRequest();
	SocketClient.callServerFun("LandgravePart_InfoNoGrids", req);
	ViewManager.showLoading({});
}
//获取数据回调
LandgravePart.InfoNoGridsResp = function(buffer) {
	ViewManager.hideLoading();
	var ans = landgrave_msg_pb.LandgravePart_InfoNoGridsResponse.decode(buffer);

	LandgravePart.MyPoint = ans.Point;
	LandgravePart.GuildPoint = ans.GuildPoint;
	LandgravePart.OccupyGrid = { };
	//占领的格子
	LandgravePart.OccupyGrid = ans.SeizeGrids;
	LandgravePart.ReviveInfo = { };
	//复活队列
	LandgravePart.ReviveInfo = ans.Lineup;
	GuildPart.GuildLevel = ans.GuildLevel;
	LandgravePart.IsBoxShow = ans.HasOutput;
	LandgravePart.EndTime = ans.EndTime;
	LandgravePart.LstRobTimes = ans.LstRobTimes;
	if ((enumData == PopPanelEnum.GFMyTeamPop)) {
		//打开我的队伍界面
		GFMyTeamPopPanel.Show();
	} else if ((enumData == PopPanelEnum.GFFastRevivePop) ){
		//打开快速复活界面
		GFFastRevivePopPanel.ShowAfterRequest();
	}
}
//判断某个格子是否有资格占领或抢夺()
LandgravePart.AbleSO = function(x, y) {
	var req = new  landgrave_msg_pb.LandgravePart_AbleSORequest();
	req.Pos.X = x;
	req.Pos.Y = y;
	SocketClient.callServerFun("LandgravePart_AbleSO", req);
}
LandgravePart.AbleSOResp = function(buffer) {
	var ans = landgrave_msg_pb.LandgravePart_AbleSOResponse.decode(buffer);

}
//占领格子
LandgravePart.Seize = function(x, y, lineup) {
	var req = new  landgrave_msg_pb.LandgravePart_SeizeRequest();
	req.Pos.X = x;
	req.Pos.Y = y;
	for (var i = 0; i != lineup.length; ++i) {
		req.Lineup.push(lineup[i]);
	}
	mResultPanelType = DungeonResultPopData.EnterType.GuildFightFirst;
	//通知主界面保存位置
	GuildFightPanel.SaveContainerPos();
	SocketClient.callServerFun("LandgravePart_Seize", req);
	LandgravePart.FightCallback = LandgravePart.OnSeizeFightDone;
	ViewManager.showLoading({});
}
var fightReward = { };
//占领格子回调
LandgravePart.SeizeResp = function(buffer) {
	ViewManager.hideLoading();
	var ans = landgrave_msg_pb.LandgravePart_SeizeResponse.decode(buffer);

	if ((ans.Ret != 0)) {
		ToolTipPopPanel.Show(ans.Ret);
		return;
	}
	//存下奖励，等战斗结束显示
	fightReward = { };
	fightReward = ans.Awards;
}
//抢夺资源
LandgravePart.Rob = function(x, y, lineup) {
	var req = new  landgrave_msg_pb.LandgravePart_RobRequest();
	req.Pos.X = x;
	req.Pos.Y = y;
	for (var i = 0; i != lineup.length; ++i) {
		req.Lineup.push(lineup[i]);
	}
	mResultPanelType = DungeonResultPopData.EnterType.GuildFightTake;
	//通知主界面保存位置
	GuildFightPanel.SaveContainerPos();
	SocketClient.callServerFun("LandgravePart_Rob", req);
	LandgravePart.FightCallback = LandgravePart.OnSeizeFightDone;
	ViewManager.showLoading({});
}
//抢夺资源回调
LandgravePart.RobResp = function(buffer) {
	ViewManager.hideLoading();
	var ans = landgrave_msg_pb.LandgravePart_RobResponse.decode(buffer);

	if ((ans.Ret != 0)) {
		ToolTipPopPanel.Show(ans.Ret);
	} else {
		LandgravePart.LstRobTimes = ans.LstRobTimes;
	}
	//存下奖励，等战斗结束显示
	fightReward = { };
	fightReward = ans.Awards;
}
//放弃领地
LandgravePart.Giveup = function(x, y) {
	var req = new  landgrave_msg_pb.LandgravePart_GiveupRequest();
	req.Pos.X = x;
	req.Pos.Y = y;
	SocketClient.callServerFun("LandgravePart_Giveup", req);
	GuildFightPanel.SaveContainerPos();
	ViewManager.showLoading({});
}
//放弃领地回调
LandgravePart.GiveupResp = function(buffer) {
	ViewManager.hideLoading();
	var ans = landgrave_msg_pb.LandgravePart_GiveupResponse.decode(buffer);

	if ((ans.Ret != 0)) {
		ToolTipPopPanel.Show(ans.Ret);
		return;
	}
	LandgravePart.GiveUpEndTime = ans.GvCDEndTime;
	//放弃的结束时间
	//给该格子数据重新赋值
	var gridData = ans.Grid;
	for (var forinvar in ipairs(LandgravePart.GridsInfo)) {
		var k = forinvar.k;
		var v = forinvar.v;
		{
			if (((v.Pos.X == gridData.Pos.X) && (v.Pos.Y == gridData.Pos.Y))) {
				LandgravePart.GridsInfo[k] = { };
				LandgravePart.GridsInfo[k] = gridData;
				break;
			}
		}
	}
	//从占领的领地中移除
	for (var forinvar in ipairs(LandgravePart.OccupyGrid)) {
		var k = forinvar.k;
		var v = forinvar.v;
		{
			if (((v.Pos.X == gridData.Pos.X) && (v.Pos.Y == gridData.Pos.Y))) {
				table.remove(LandgravePart.OccupyGrid, k);
				break;
			}
		}
	}
	//弹出获得奖励界面
	if ((ans.Awards.length > 0)) {
		HuoDeWuPinTiShiPanelData.ShowResources(ans.Awards);
	}
	//回调界面，关闭弹窗，刷新主界面
	GuildMyAreaPopPanel.GiveUpSuccess();
	//刷新当前的主界面
	GuildFightPanel.UpdatePoints();
	GuildFightPanel.UpdateSingleGridByXY(gridData, gridData.Pos.X, gridData.Pos.Y);
}
//解锁领地
LandgravePart.Unlock = function(landid) {
	var req = new  landgrave_msg_pb.LandgravePart_UnlockRequest();
	req.LandID = landid;
	SocketClient.callServerFun("LandgravePart_Unlock", req);
	ViewManager.showLoading({});
}
//解锁领地回调
LandgravePart.UnlockResp = function(buffer) {
	ViewManager.hideLoading();
	var ans = landgrave_msg_pb.LandgravePart_UnlockResponse.decode(buffer);

	if ((ans.Ret != 0)) {
		ToolTipPopPanel.Show(ans.Ret);
		return;
	}
	LandgravePart.UnlockLands = { };
	LandgravePart.UnlockLands = ans.UnlockLands;
	//刷新界面
	GuildFightPanel.UpdatePoints();
	GFMyTerritoryPopPanel.UpdatePanel();
}
//领取领地奖励
LandgravePart.GetAward = function(x, y) {
	var req = new  landgrave_msg_pb.LandgravePart_GetAwardRequest();
	req.Pos.X = x;
	req.Pos.Y = y;
	SocketClient.callServerFun("LandgravePart_GetAward", req);
	ViewManager.showLoading({});
}
//领取奖励回调
LandgravePart.GetAwardResp = function(buffer) {
	ViewManager.hideLoading();
	var ans = landgrave_msg_pb.LandgravePart_GetAwardResponse.decode(buffer);

	if ((ans.Ret != 0)) {
		ToolTipPopPanel.Show(ans.Ret);
		return;
	}
	LandgravePart.MyPoint = ans.Point;
	LandgravePart.GuildPoint = ans.GuildPoint;
	LandgravePart.IsBoxShow = ans.HasOutput;
	//显示奖励
	//弹出获得奖励界面
	if ((ans.Awards.length > 0)) {
		HuoDeWuPinTiShiPanelData.ShowResources(ans.Awards);
	}
	//关闭气泡
	GuildFightPanel.ClosePaoPao();
	//可获取
	GuildFightPanel.UpdateBox();
}
//获取个人积分榜
LandgravePart.GetPointRank = function() {
	var req = new  landgrave_msg_pb.LandgravePart_PointRankRequest();
	SocketClient.callServerFun("LandgravePart_GetPointRank", req);
	ViewManager.showLoading({});
}
//个人积分榜回调
LandgravePart.PointRankResp = function(buffer) {
	ViewManager.hideLoading();
	var ans = landgrave_msg_pb.LandgravePart_PointRankResponse.decode(buffer);

	var myrank = 0;
	var myDamage = 0;
	for (var forinvar in ipairs(ans.RankList)) {
		var i = forinvar.i;
		var item = forinvar.item;
		{
			if ((item.ObjID == PlayerBasePart.ObjID)) {
				myrank = i;
				break;
			}
		}
	}
	DaoGuanBossRankPopPanel.CommonShow(DaoGuanBossRankPopPanel.EnterType.PersonPoint, ans.RankList, myrank);
}
//获取公会积分榜
LandgravePart.GetGuildPointRank = function() {
	var req = new  landgrave_msg_pb.LandgravePart_GPointRankRequest();
	SocketClient.callServerFun("LandgravePart_GetGuildPointRank", req);
	ViewManager.showLoading({});
}
//公会积分榜回调
LandgravePart.GuildPointRankResp = function(buffer) {
	ViewManager.hideLoading();
	var ans = landgrave_msg_pb.LandgravePart_GPointRankResponse.decode(buffer);

	var myrank = 0;
	var myDamage = 0;
	for (var forinvar in ipairs(ans.RankList)) {
		var i = forinvar.i;
		var item = forinvar.item;
		{
			if ((item.GuildID == GuildPart.GuildID)) {
				myrank = i;
				break;
			}
		}
	}
	DaoGuanBossRankPopPanel.CommonShow(DaoGuanBossRankPopPanel.EnterType.GuildPoint, ans.RankList, myrank);
}
var beforeReviveCount = 0;
//复活前队列长度
//快速复活请求
LandgravePart.Revive = function(index, itemid, itemnum) {
	var req = new  landgrave_msg_pb.LandgravePart_ReviveRequest();
	req.Index = (index - 1);
	req.ItemID = itemid;
	req.ItemNum = itemnum;
	beforeReviveCount = LandgravePart.ReviveInfo.length;
	SocketClient.callServerFun("LandgravePart_Revive", req);
	ViewManager.showLoading({});
}
//复活回调
LandgravePart.ReviveResp = function(buffer) {
	ViewManager.hideLoading();
	var ans = landgrave_msg_pb.LandgravePart_ReviveResponse.decode(buffer);

	if ((ans.Ret != 0)) {
		ToolTipPopPanel.Show(ans.Ret);
		return;
	}
	LandgravePart.ReviveInfo = { };
	//复活队列
	LandgravePart.ReviveInfo = ans.Lineup;
	var isChange = (beforeReviveCount != LandgravePart.ReviveInfo.length);
	//刷新界面
	GFFastRevivePopPanel.UseSuccess(isChange);
}
//获取战斗记录  true就是获取胜利的，false获取失败的
LandgravePart.GetFightRecords = function(win) {
	var req = new  landgrave_msg_pb.LandgravePart_RecordsRequest();
	req.Win = win;
	SocketClient.callServerFun("LandgravePart_FightRecords", req);
	ViewManager.showLoading({});
}
//获取战斗记录的回调
LandgravePart.FightRecordsResp = function(buffer) {
	ViewManager.hideLoading();
	var ans = landgrave_msg_pb.LandgravePart_RecordsResponse.decode(buffer);

	//打开界面
	GFRecordPopPanel.ShowAfterRequest(ans.Records);
}
//战斗回放
LandgravePart.ReviewBattle = function(battleid) {
	var req = new  landgrave_msg_pb.LandgravePart_ReviewFightRequest();
	req.BattleID = battleid;
	fightReward = { };
	mResultPanelType = DungeonResultPopData.EnterType.GuildFightPlayBack;
	GuildFightPanel.SaveContainerPos();
	SocketClient.callServerFun("LandgravePart_ReviewBattle", req);
}
//获取奖励格子
LandgravePart.AwardGrid = function() {
	var req = new  landgrave_msg_pb.LandgravePart_AwardGridRequest();
	SocketClient.callServerFun("LandgravePart_AwardGrid", req);
	ViewManager.showLoading({});
}
LandgravePart.AwardGridResp = function(buffer) {
	ViewManager.hideLoading();
	var ans = landgrave_msg_pb.LandgravePart_AwardGridResponse.decode(buffer);

	if ((ans.Ret != 0)) {
		return;
	}
	//拿到有奖励的坐标后跳转
	GuildFightPanel.LocatePosition(ans.Pos.X, ans.Pos.Y);
}
var reqPanel = null;
//获取状态
LandgravePart.Stat = function(requestPanel) {
	reqPanel = requestPanel;
	var req = new  landgrave_msg_pb.LandgravePart_StatRequest();
	SocketClient.callServerFun("LandgravePart_Stat", req);
	ViewManager.showLoading({});
};
LandgravePart.StatResp = function(buffer) {
	ViewManager.hideLoading();
	var ans = landgrave_msg_pb.LandgravePart_StatResponse.decode(buffer);

	LandgravePart.IsOpen = (ans.Status == 1);
	if ((reqPanel == PanelEnum.DaoGuanMain)) {
		GuildFightPanel.Show();
	} else if ((reqPanel == PanelEnum.MainBtn) ){
		//打开冒险
		MainBtnPanel.ShowAfterRequest();
	} else if (reqPanel == (-1) ){
		GuildFightPanel.Show();
		//前往打开的
	}
};
LandgravePart.ErrorCode = function(buffer) {
	ViewManager.hideLoading();
	var ans = landgrave_msg_pb.LandgravePart_ErrCodeSync.decode(buffer);

	ToolTipPopPanel.Show(ans.Ret);
};
//-----------------------------战斗回放数据相关----------------------------
//战斗相关
LandgravePart.FightRecord = function(buffer) {
	var msg = landgrave_msg_pb.LandgravePart_FightRecord.decode(buffer);

	LandgravePart.Records = new FightRecords();
	LandgravePart.Records.Decode(msg.Content);
	if (msg.Done) {
		LandgravePart.Playback();
	}
};
LandgravePart.FightRecordFollow = function(buffer) {
	var msg = landgrave_msg_pb.LandgravePart_FightRecord.decode(buffer);

	LandgravePart.Records.AppendFollow(msg.Content);
	if (msg.Done) {
		LandgravePart.Playback();
	}
};
//占领战斗回放
LandgravePart.Playback = function() {
	//开启战斗
	var conf_ZhanDouPeiZhi_Data = LoadConfig.getConfigData(ConfigName.ZhanDouPeiZhi,17);
	if ((conf_ZhanDouPeiZhi_Data == null)) {
		return;
	}
	CommonFightPanel.Show(CommonFightPanel.FightType.GuildFight, function() {
			GFightManager.Playback(conf_ZhanDouPeiZhi_Data.Scene, FightSystemType.GuildFight, LandgravePart.Records, LandgravePart.OnSeizeFightDone);
		});
};
//占领战斗结束回调
LandgravePart.OnSeizeFightDone = function(ret) {
	//根据战斗类型，显示结果
	if ((ret == 2)) {
		DungeonResultPopData.ShowPanel(DungeonResultPopData.DungeonShowType.Common,false,{
			enterType : mResultPanelType,
			rewards : fightReward
		});
	} else {
		DungeonResultPopData.ShowPanel(DungeonResultPopData.DungeonShowType.Common,true,{
			enterType : mResultPanelType,
			rewards : fightReward
		});
	}
};
//-----------------------------前端用的相关函数-------------------------
//通过行列获取格子的数据
LandgravePart.GetGridInfo = function(row, column) {
	var curX = (column - 2);
	var curY = (LandgravePart.TotalRow - ((row - 1)));
	for (var forinvar in ipairs(LandgravePart.GridsInfo)) {
		var k = forinvar.k;
		var v = forinvar.v;
		{
			if (((v.Pos.X == curX) && (v.Pos.Y == curY))) {
				return v;
			}
		}
	}
	return null;
};
//通过XY获取格子的数据
LandgravePart.GetGridInfoByXY = function(x, y) {
	for (var forinvar in ipairs(LandgravePart.GridsInfo)) {
		var k = forinvar.k;
		var v = forinvar.v;
		{
			if (((v.Pos.X == x) && (v.Pos.Y == y))) {
				return v;
			}
		}
	}
	return null;
};
//通过XY 拿到所在的行列
LandgravePart.GetRowAndColumn = function(x, y) {
	var row = 0;
	var column = 0;
	column = (x + 2);
	row = ((LandgravePart.TotalRow - y) + 1);
	return row, column;
};
//判断某张卡是否被禁
LandgravePart.IsForbidden = function(zhenRongID, cardID) {
	var conf_ZhenRongInfo = LoadConfig.getConfigData(ConfigName.DaoGuanZhan_ZhenRong,zhenRongID);
	if ((conf_ZhenRongInfo == null)) {
		console.error(("道馆战阵容表空，ID" + zhenRongID));
		return true;
	}
	var conf_KaPaiInfo = LoadConfig.getConfigData(ConfigName.ShiBing,cardID);
	if ((conf_KaPaiInfo == null)) {
		console.error(("士兵表空，ID" + cardID));
		return true;
	}
	//先存下会被禁用的属性
	//属性///////////////////////
	var race = conf_KaPaiInfo.Race;
	//品质///////////////////////
	var quality = conf_KaPaiInfo.Quality;
	//品质 白1 绿2 蓝3 紫4 橙5 
	//是否对空
	var isFly = conf_KaPaiInfo.Troop;
	//性别//////////////////////
	var sex = (conf_KaPaiInfo.Category + 1);
	//LUA数组从1 开始  要加一
	//进化等级///////////////////
	var evo = (conf_KaPaiInfo.EVO + 1);
	//LUA数组从1 开始  要加一
	//兵种 空军 1   2和3是陆军
	var troop = conf_KaPaiInfo.Troop;
	//攻击类型 1近战 2远程(ID 从1开始 索引减一)   
	var attckType = conf_KaPaiInfo.AttackType;
	if (((((((race < 1) || (quality < 1)) || (sex < 1)) || (evo < 1)) || (troop < 1)) || (attckType < 1))) {
		console.error("数组索引小于1，判断禁用,战斗塔");
	}
	//先判断属性是否被禁 0不限制 1 限制
	if ((conf_ZhenRongInfo.LimitRace[race] == 1)) {
		return true;
	}
	//再判断品质是否被禁
	if ((conf_ZhenRongInfo.LimitQuality[quality] == 1)) {
		return true;
	}
	//再判断性别是否被禁
	if ((conf_ZhenRongInfo.LimitSex[sex] == 1)) {
		return true;
	}
	//判断进化等级是否被禁
	if ((conf_ZhenRongInfo.LimitEvo[evo] == 1)) {
		return true;
	}
	//判断军种是否被禁
	var troopIndex = troop;
	if ((troopIndex == 3)) {
		troopIndex = 2;
	}
	if ((conf_ZhenRongInfo.LimitTroop[troopIndex] == 1)) {
		return true;
	}
	//判断攻击类型
	if ((conf_ZhenRongInfo.LimitAttackType[attckType] == 1)) {
		return true;
	}
	return false;
};
//上阵
LandgravePart.AddLineUpRequest = function(cardID, zhenRongID) {
	//获取阵容配置
	var conf_ZhenRongPeiZhi = LoadConfig.getConfigData(ConfigName.DaoGuanZhan_ZhenRong,zhenRongID);
	if ((conf_ZhenRongPeiZhi == null)) {
		console.error(("道馆战阵容表空，ID" + zhenRongID));
		return;
	}
	//计算上阵索引
	var index = -1;
	for (var i = 0; i != LandgravePart.CardList.length; ++i) {
		if ((LandgravePart.CardList[i] == 0)) {
			index = i;
			break;
		}
	}
	//判断索引是否异常
	if ((index == (-1) || (index > conf_ZhenRongPeiZhi.LineCount))) {
		return;
	}
	//添加到阵容中
	LandgravePart.CardList[index] = cardID;
	//刷新界面
	TowerChangePopPanel.UpdatePanelInfo();
	GuildAreaPopPanel.InitMyTeam();
};
//下阵请求
LandgravePart.SubLineDownRequest = function(index) {
	//判断索引越界
	if (((index < 1) || (index > 12))) {
		return;
	}
	//判断该索引是否存在卡牌
	var cardID = LandgravePart.CardList[index];
	if ((cardID == 0)) {
		return;
	}
	//下阵卡牌
	LandgravePart.CardList[index] = 0;
	//刷新界面
	TowerChangePopPanel.UpdatePanelInfo();
	GuildAreaPopPanel.InitMyTeam();
};
//通过表格获取公会战积分排行奖励
LandgravePart.GetGuildReward = function(rank) {
	var rewardType = { };
	var rewardID = { };
	var rewardCount = { };
	for (var forinvar of Conf_DaoGuanZhan_DaoGanPaiMing) {
		var i = forinvar.i;
		var item = forinvar.item;
		{
			if (((rank <= item.RankEnd) && (rank >= item.RankBegin))) {
				var jiangchiID = item.PoolID;
				var tableData = LoadConfig.getConfigData(ConfigName.JiangChi,jiangchiID);
				if ((tableData == null)) {
					console.error(("奖池表ID空" + jiangchiID));
					return rewardType, rewardID, rewardCount;
				}
				for (var i = 0; i != tableData.ResourceTypes.length; ++i) {
					var mType = tableData.ResourceTypes[i];
					if (((i > 3) || (mType == 0))) {
						break;
					}
					rewardType[i] = mType;
					rewardID[i] = tableData.ResourceIDs[i];
					rewardCount[i] = tableData.MinCounts[i];
				}
				return [rewardType, rewardID, rewardCount];
			}
		}
	}
	return [rewardType, rewardID, rewardCount];
}
//通过表格获取个人积分排行奖励
LandgravePart.GetPersonReward = function(rank) {
	var rewardType = { };
	var rewardID = { };
	var rewardCount = { };
	for (var forinvar in ipairs(Conf_DaoGuanZhan_GeRenPaiMing)) {
		var i = forinvar.i;
		var item = forinvar.item;
		{
			if (((rank <= item.RankEnd) && (rank >= item.RankBegin))) {
				var jiangchiID = item.PoolID;
				var tableData = LoadConfig.getConfigData(ConfigName.JiangChi,jiangchiID);
				if ((tableData == null)) {
					console.error(("奖池表ID空" + jiangchiID));
					return rewardType, rewardID, rewardCount;
				}
				for (var i = 0; i != tableData.ResourceTypes.length; ++i) {
					var mType = tableData.ResourceTypes[i];
					if (((i > 3) || (mType == 0))) {
						break;
					}
					rewardType[i] = mType;
					rewardID[i] = tableData.ResourceIDs[i];
					rewardCount[i] = tableData.MinCounts[i];
				}
				return rewardType, rewardID, rewardCount;
			}
		}
	}
	return rewardType, rewardID, rewardCount;
}
//获取已经占领的个数
LandgravePart.GetOwnLandCount = function(level) {
	var count = 0;
	for (var i = 0; i != LandgravePart.OccupyGrid.length; ++i) {
		count = (count + 1);
	}
	return count;
}
//获取可占领某个品质领地数
LandgravePart.GetOccLandCount = function(level) {
	var count = 0;
	for (var forinvar in ipairs(Conf_DaoGuanZhan_LingDi)) {
		var id = forinvar.id;
		var conf = forinvar.conf;
		{
			if (((conf.ConditionT == 1) && (GuildPart.GuildLevel >= conf.Param))) {
				count = (count + 1);
			} else {
				for (var forinvar in ipairs(LandgravePart.UnlockLands)) {
					
					var landid = forinvar.landid;
					{
						if ((id == landid)) {
							count = (count + 1);
						}
					}
				}
			}
		}
	}
	return count;
}
//获取当前可以占领多少领地
LandgravePart.GetCanOwnLandCount = function() {
	var count = 0;
	for (var forinvar in ipairs(Conf_DaoGuanZhan_LingDi)) {
		var id = forinvar.id;
		var conf = forinvar.conf;
		{
			if (((conf.ConditionT == 1) && (GuildPart.GuildLevel >= conf.Param))) {
				count = (count + 1);
			} else {
				for (var forinvar in ipairs(LandgravePart.UnlockLands)) {
					
					var landid = forinvar.landid;
					{
						if ((id == landid)) {
							count = (count + 1);
						}
					}
				}
			}
		}
	}
	return count;
}
//获取总共可占领几个该品质的，包括未解锁
LandgravePart.GetTotalQualityCount = function(level) {
	var count = 0;
	for (var forinvar in ipairs(Conf_DaoGuanZhan_LingDi)) {
		var id = forinvar.id;
		var conf = forinvar.conf;
		{
			count = (count + 1);
		}
	}
	return count;
}
//是否有可以购买的领地
LandgravePart.CanBuyMore = function() {
	for (var forinvar in ipairs(Conf_DaoGuanZhan_LingDi)) {
		var id = forinvar.id;
		var data = forinvar.data;
		{
			var isUnlocked = LandgravePart.IsLandUnlocked(id);
			if (!isUnlocked) {
				if ((data.ConditionT == 1)) {
					if ((GuildPart.GuildLevel < data.Param)) {
						return true;
					}
				} else {
					return true;
				}
			}
		}
	}
	return false;
}
//判断某个id是否已经在已解锁领地中
LandgravePart.IsLandUnlocked = function(id) {
	for (var forinvar in ipairs(LandgravePart.UnlockLands)) {
		
		var landid = forinvar.landid;
		{
			if ((id == landid)) {
				return true;
			}
		}
	}
	return false;
}
//获取一个当前品质可购买的ID
LandgravePart.GetCanBuyID = function(level) {
	for (var forinvar in ipairs(Conf_DaoGuanZhan_LingDi)) {
		var id = forinvar.id;
		var conf = forinvar.conf;
		{
			//品质对应，判断解锁条件是否达到
			if ((conf.Level == level)) {
				//条件未达到，可以直接请求购买
				if (((conf.ConditionT == 1) && (GuildPart.GuildLevel < conf.Param))) {
					if (!LandgravePart.IsLandUnlocked(id)) {
						return conf;
					}
				}
				//如果未解锁的条件的话，只需要判断是否在已解锁
				if ((conf.ConditionT == 2)) {
					if (!LandgravePart.IsLandUnlocked(id)) {
						return conf;
					}
				}
			}
		}
	}
	return null;
}
//判断复活所需材料是否足够
LandgravePart.IsMatEnough = function(id) {
	var count = ResourceTool_GetResourceCount(DaoJuTool.ResourceType.DaoJuBiao, id);
	return (count > 0);
}
//请求交换卡牌
LandgravePart.ChangeLineRequest = function(beforeIndex, afterIndex) {
	var id = LandgravePart.CardList[beforeIndex];
	LandgravePart.CardList[beforeIndex] = LandgravePart.CardList[afterIndex];
	LandgravePart.CardList[afterIndex] = id;
	TowerChangePopPanel.UpdatePanelInfo();
	GuildAreaPopPanel.InitMyTeam();
}
window.LandgravePart = LandgravePart;