//region *.lua
//Date
//此文件由[BabeLua]插件自动生成
import BigNumber from "../tool/BigNumber";
import FightBehaviorManager from "../logic/fightBehavior/FightBehaviorManager";
import { FightSystemType } from "../logic/fight/FightDef";
import ViewManager, { PanelPos } from "../manager/ViewManager";
import SocketClient from "../network/SocketClient";
import Card from "../logic/Card";
import GFightManager from "../logic/fight/GFightManager";
import FightTool from "../logic/fightBehavior/FightTool";
import { PlayerIntAttr } from "./PlayerBasePart";
import { PanelResName } from "../common/PanelResName";
import CommonFightPanel, { FightType } from "../view/uipanel/CommonFightPanel"
let dungeon_msg_pb = require('dungeon_msg').msg;
let DungeonPart = { };

DungeonPart.DungeonList = { };
DungeonPart.ChallengeCount = 0;
DungeonPart.LastRecoverTime = 0;
DungeonPart.WinRewardList = { };
//接受服务器下发的地下城信息
DungeonPart.RevcDungeonInfo = function(buffer) {
	var msg = dungeon_msg_pb.DungeonPart_SendDungeonInfo.decode(buffer);
	DungeonPart.DungeonList = { };
	DungeonPart.DungeonList = msg.DungeonList;
	DungeonPart.ChallengeCount = msg.ChallengCount;
	DungeonPart.LastRecoverTime = msg.LastRecoverTime;
}
//通过地下城ID  拿到当前地下城最高关卡
DungeonPart.GetTopRound = function(dungeonID) {
	var round = -1;
	for (var i = 0; i != DungeonPart.DungeonList.length; ++i) {
		if ((dungeonID == DungeonPart.DungeonList[i].DungeonID)) {
			round = DungeonPart.DungeonList[i].DungeonTopRound;
		}
	}
	if (round > (-1)) {
		return round;
	} else {
		return round;
	}
}
//根据地下城id获取最大层数
DungeonPart.GetCurDungeonMaxRound = function(dungeonID) {
	if ((LoadConfig.getConfigData(ConfigName.DiXiaCheng_GuanKa,1001) == null)) {
		console.error("关卡表为空");
		return 0;
	}
	var compareNumOne = (dungeonID * 1000);
	var compareNumTwo = (((dungeonID + 1)) * 1000);
	var maxCount = 0;
	let Conf_DiXiaCheng_GuanKa = LoadConfig.getConfig(ConfigName.DiXiaCheng_GuanKa);
	//遍历关卡表 
	for (var key in Conf_DiXiaCheng_GuanKa) {
		var value = Conf_DiXiaCheng_GuanKa[key];
		{
			let id = parseInt(key);
			if (((id > compareNumOne) && (id < compareNumTwo))) {
				maxCount = (maxCount + 1);
			}
		}
	}
	return maxCount;
}
DungeonPart.FightResponse = function(buffer) {
	var msg = dungeon_msg_pb.DungeonPart_FightResponse.decode(buffer);
	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		console.error(("错误ID" + msg.Flag));
	}
	//拿到奖励
	DungeonPart.WinRewardList = msg.RewardList;
}
//-------------战斗相关---------------------------------------------
var mDungeonID = 0;
var mRoundID = 0;
var mFightID = null;
//开启战斗验证
DungeonPart.OnBeginFightVerifyRequest = function(dungeonID, roundID) {
	//设置标记
	FightBehaviorManager.SetFightingSystem(FightSystemType.DiXiaCheng);
	var request = new  dungeon_msg_pb.DungeonPart_OnBeginFightVerifyRequest();
	request.DungeonID = dungeonID;
	request.RoundID = roundID;
	mDungeonID = dungeonID;
	mRoundID = roundID;
	SocketClient.callServerFun('DungeonPart_OnBeginFightVerifyRequest', request);
	ViewManager.showLoading({});
}
//开启战斗验证返回
DungeonPart.OnBeginFightVerifyReturn = function(buffer) {
	var msg = dungeon_msg_pb.DungeonPart_BeginFightVerifyResponse.decode(buffer);
	ViewManager.hideLoading();
	var fightID = msg.FightID;
	var randSeed = msg.RandSeed;
	mFightID = msg.FightID;
	DungeonPart.WinRewardList = { };
	//拿到奖励
	DungeonPart.WinRewardList = msg.RewardList;
	//console.error("地下城收到收到战斗ID:"..fightID)
	DungeonPart.StartDungeonFight(fightID, randSeed);
}
//存下提前下发的奖励
//判断是否有奖励
DungeonPart.IsTriggerPassReward = function() {
	if ((DungeonPart.WinRewardList.length > 0)) {
		return true;
	}
	return false;
}
DungeonPart.GetPassReward = function() {
	var countReward = DungeonPart.WinRewardList.length;
	var resType = { };
	var resID = { };
	var resCount = { };
	for (var i = 0; i != countReward; ++i) {
		resType[i] = DungeonPart.WinRewardList[i].RewardType;
		resID[i] = DungeonPart.WinRewardList[i].RewardID;
		resCount[i] = DungeonPart.WinRewardList[i].RewardCount;
	}
	return resType, resID, resCount;
}
//开启地下城战斗
DungeonPart.StartDungeonFight = function(fightID, randSeed) {
	var conf_ID = ((mDungeonID * 1000) + mRoundID);
	var conf_GuanKa = LoadConfig.getConfigData(ConfigName.DiXiaCheng_GuanKa,conf_ID);
	if ((conf_GuanKa == null)) {
		console.error(("地下城关卡ID错误:" + conf_ID));
		FightBehaviorManager.SetFightingSystem(null);
		return;
	}
	var conf_Dungeon = LoadConfig.getConfigData(ConfigName.DiXiaCheng,mDungeonID);
	if ((conf_Dungeon == null)) {
		console.error(("地下城ID错误:" + mDungeonID));
		FightBehaviorManager.SetFightingSystem(null);
		return;
	}
	//获取我军boss --取战斗配置表   --地下城的ID 对应战斗配置   地下城ID如果是1   则对应战斗配置3
	var conf_ZhanDouPeiZhi_Data = LoadConfig.getConfigData(ConfigName.ZhanDouPeiZhi,(mDungeonID + 2));
	//创建我军战斗信息  如果有限制 要过滤
	var myCards = [];
	for (var i = 0; i != LineupPart.CardList.length; ++i) {
		var cardData = LineupPart.CardList[i];
		//判断阵容中的这张卡 是否被限制进入
		var conf_shibing = LoadConfig.getConfigData(ConfigName.ShiBing,cardData.CardID);
		if ((conf_shibing == null)) {
			console.error(("地下城我方士兵ID错误" + cardData.CardID));
		}
		var race = conf_shibing.Race;
		//种族(1红2绿3黄4蓝)
		//判断该种族是否禁用
		var isSoliderLimit = DungeonPart.GetForbidden(race, conf_Dungeon);
		//若没有限制 则加入
		if (!isSoliderLimit) {
			//参数1，卡牌
			var card = cardData.CloneWenZhangCard();
			myCards.push(card)
		}
	}
	var myBossCard = Card.NewByID(FightTool.GetMyBossID(), 1, conf_GuanKa.MyBossXunZhangLevel);
	var playerLevel = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_DengJi];
	var topGuanQia = GuanQiaPart.HistoryTop;
	var respawn = PlayerAttrPart.GetSummaryAttr(PlayerAttrPart.Addition_Summary_Type.RespawnRate);
	var par = { };
	par.RespawnProb = parseInt(respawn);
	if ((conf_ZhanDouPeiZhi_Data.IsPVP == 1)) {
		par.RespawnProb = (par.RespawnProb / 2);
	}
	let tempConf_Id = conf_ID+"";//conf_ID.toString();
	var myTeam = GFightManager.CreateFightTeam(tempConf_Id, myBossCard, myCards, playerLevel, topGuanQia, respawn);
	//创建敌军战斗信息
	var enemyCards = [];
	for (var i = 0; i != conf_GuanKa.GuaiWuID.length; ++i) {
		var cardData = conf_GuanKa.GuaiWuID[i];
		if ((cardData > 0)) {
			var card = Card.NewByID(cardData, conf_GuanKa.WenZhang, conf_GuanKa.XunZhangLevel, PlayerAttrPart.Addition_Kind.DungeonNormal);
			card.SetNotRespawn();
			enemyCards.push(card)
		}
	}
	//获取敌军boss
	var enemyBossCard = Card.NewByID(conf_GuanKa.BossID, null, conf_GuanKa.BossXunZhangLevel, PlayerAttrPart.Addition_Kind.DungeonBoss);
	var par = { };
	par.RespawnProb = 0;
	var enemyTeam = GFightManager.CreateFightTeam(tempConf_Id, enemyBossCard, enemyCards, 1, 1, par);
	//获取场景
	var scene = conf_GuanKa.Sence;
	//获取战斗时长
	var fightTime = conf_ZhanDouPeiZhi_Data.FightTime;
	//显示地下城界面
	// CommonFightPanel.Show(CommonFightPanel.FightType.Dungeon, function() {
	// 		GFightManager.Start(scene, FightSystemType.DiXiaCheng, (fightTime * 1000), fightID, randSeed, myTeam, enemyTeam, DungeonPart.OnFightFinished);
	// 	});
	let fightType = FightType.Dungeon;
	ViewManager.addPanel(PanelResName.CommonFightPanel,(panel)=>
	{
		GFightManager.Start(scene, FightSystemType.DiXiaCheng, (fightTime * 1000), fightID, randSeed, myTeam, enemyTeam, DungeonPart.OnFightFinished);
		DungeonPart.onFightCallBack();
	},PanelPos.PP_Midle,{fightType});
}
DungeonPart.onFightCallBack = function()
{
	let zhenRongPanel = ViewManager.getPanel(PanelResName.DungeonZhenRongPanel);
	zhenRongPanel.hiddenPanel();

	let dungeonDiffPopPanel = ViewManager.getPanel(PanelResName.DungeonDiffPopPanel);
	dungeonDiffPopPanel.hiddenPanel();

	let dungeonPopPanel = ViewManager.getPanel(PanelResName.DungeonPopPanel);
	dungeonPopPanel.hiddenPanel();
}
//战斗结束回调
DungeonPart.OnFightFinished = function(isWin, input) {
	if ((isWin != 1)) {
		DungeonResultPopData.ShowPanel(DungeonResultPopData.DungeonShowType.Normal,false);
		return;
	}
	DungeonPart.OnStartFightVerifyRequest(mFightID, isWin, input);
}
var misWin = 0;
DungeonPart.OnStartFightVerifyRequest = function(fightID, isWin, input) {
	var request = new  dungeon_msg_pb.DungeonPart_StartFightVerifyRequest();
	request.FightID = fightID;
	request.Input = input.Encode();
	request.FightResult = isWin;
	misWin = isWin;
	SocketClient.callServerFun('DungeonPart_OnStartFightVerifyRequest', request);
	ViewManager.showLoading({});
}
//战斗验证返回
DungeonPart.OnStartFightVerifyReturn = function(buffer) {
	var msg = dungeon_msg_pb.DungeonPart_StartFightVerifyResponse.decode(buffer);

	ViewManager.hideLoading();
	//弹出战斗胜利 如果到这里说明验证成功了
	if (msg.IsWin) {
		DungeonResultPopData.ShowPanel(DungeonResultPopData.DungeonShowType.Normal,true);
	} else {
		DungeonResultPopData.ShowPanel(DungeonResultPopData.DungeonShowType.Normal,false);
	}
}
//地下城战斗结束回调
DungeonPart.OnDungeonFightFinished = function(isWin, fightTime, remainBlood) {
	//如果是前五个 直接使用
	if ((mDungeon <= 5)) {
		if (isWin) {
			//发送排名交换请求  拿到胜利后的奖励 弹出胜利界面
			DungeonPart.FightRequest(mDungeon, mRound);
			//DungeonResultPopPanel.Show(DungeonResultPopPanel.ShowType.Win)
		} else {
			DungeonResultPopData.ShowPanel(DungeonResultPopData.DungeonShowType.Normal,false);
		}
		return;
	}
	//第六个 判断时间
	if ((mDungeon == 6)) {
		if (((fightTime > 180) || isWin)) {
			DungeonPart.FightRequest(mDungeon, mRound);
		} else {
			DungeonResultPopData.ShowPanel(DungeonResultPopData.DungeonShowType.Normal,false);
		}
	}
	//第七个 判断血量
	if ((mDungeon == 7)) {
		if (isWin) {
			DungeonPart.FightRequest(mDungeon, mRound);
		} else {
			if ((remainBlood > 0.3)) {
				DungeonResultPopData.ShowPanel(DungeonResultPopData.DungeonShowType.Normal,false);
			} else {
				DungeonPart.FightRequest(mDungeon, mRound);
			}
		}
	}
}
//判断是否被禁
DungeonPart.GetForbidden = function(race, tableInfo) {
	//0 不限制 1 限制   种族(1红2绿3黄4蓝)
	var isForbidden = false;
	if ((tableInfo.Islimit[race] == 1)) {
		isForbidden = true;
		return isForbidden;
	}
	return isForbidden;
}
//获取地下城材料加成
DungeonPart.GetStoneBonus = function() {
	//装备加成
	var equip = PlayerAttrPart.GetSummaryAttr(PlayerAttrPart.Addition_Summary_Type.DungeonRewardAdd);
	//VIP加成
	var vip = 1*100;//(VIPTool_GetFuncCount(VIPFunc.VIPFunc_FuBenCaiLiaoChanChu) * 100); //TODO 因为现在vip 还没有数据，所以只能先屏蔽掉
	//合并
	var result = BigNumber.add(equip, BigNumber.create(vip));
	return result;
}
//请求扫荡
DungeonPart.SwapRequest = function(dungeonID, round) {
	var request = new  dungeon_msg_pb.DungeonPart_SwapRequest();
	request.DungeonID = dungeonID;
	request.Round = round;
	SocketClient.callServerFun('DungeonPart_SwapDungeonRound', request);
}
//扫荡成功
DungeonPart.SwapResponse = function(buffer) {
	var msg = dungeon_msg_pb.DungeonPart_SwapResponse.decode(buffer);

	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		console.error(("错误ID" + msg.Flag));
		return;
	}
	//显示奖励
	var resourceType = { };
	var resourceID = { };
	var resourceCount = { };
	for (var i = 0; i != msg.RewardList.length; ++i) {
		resourceType[i] = msg.RewardList[i].RewardType;
		resourceID[i] = msg.RewardList[i].RewardID;
		resourceCount[i] = msg.RewardList[i].RewardCount;
	}
	//显示奖励
	HuoDeWuPinTiShiPanelData.Show(resourceType, resourceID, resourceCount);
	//刷新界面
	DungeonDiffPopPanel.UpdataCiShuDXC();
	//刷新侧边次数
	DungeonPopPanel.SetTopInfo();
}
//请求跳过战斗
DungeonPart.JumpRequest = function(dungeonID, round) {
	var request = new  dungeon_msg_pb.DungeonPart_JumpRequest();
	request.DungeonID = dungeonID;
	request.Round = round;
	ViewManager.showLoading({});
	SocketClient.callServerFun('DungeonPart_JumpRound', request);
}
//回应跳过战斗
DungeonPart.JumpResponse = function(buffer) {
	ViewManager.hideLoading();
}
//endregion
window.DungeonPart = DungeonPart;