let sdk_msg_pb = require('sdk_msg').msg;
//SDKPart
let SDKPart = { };

//SDK登录回调
const enLoginRepose = {
	enLoginRepose_Success : 0,
	//表示成功
	enLoginRepose_OtherException: -1,
	//其他异常
	enLoginRepose_AccountError: 81,
	//帐号ID错误
	enLoginRepose_GameIDError: 82,
	//游戏ID错误
	enLoginRepose_GameIDUnFound:83,
	//游戏ID未找到
	enLoginRepose_NotLogin: 84,
	//您还未登录,请先登录再试
	enLoginRepose_LoginRepeat: 85,
	//您的账号在其他地方登陆,请重新登陆后再试

};
//SDK登录,不走服务器验证的不用调用这个方法
SDKPart.Login = function(loginSDKData) {
	var request = new  sdk_msg_pb.SDKLoginRequest();
	var sdkType = LuaSDKManager.GetCurrentSdkType();
	if ((((sdkType == enSDKType.enSDKType_YiJie) || (sdkType == enSDKType.enSDKType_KuaiJie)) || (sdkType == enSDKType.enSDKType_HuLuXia))) {
		request.Gameid = loginSDKData.gameid;
		request.Appkey = loginSDKData.gamekey;
	} else {
		var sdkData = LuaSDKManager.GetSDKConstData();
		if ((null == sdkData)) {
			console.error("SDK Login 登陆参数异常");
			return;
		}
		var stSdkType = tostring(LuaFramework.SDKManager.GetSDKType());
		if ((stSdkType == "enSDKType_MoyaMycard")) {
			sdkType = enSDKType.enSDKType_MoyaMycard;
		}
		request.Gameid = sdkData.gameid;
		request.Appkey = sdkData.gamekey;
	}
	request.SdkType = sdkType;
	request.IsIphone = Game.IsIphone();
	request.AccountID = loginSDKData.accountid;
	request.Sessionid = loginSDKData.sessionid;
	//调用服务器SDK登录
	ViewManager.showLoading({});
	SocketClient.callServerFun("SDK_Login", request);
}
//登录回调
SDKPart.LoginReponse = function(buff) {
	var reponse = sdk_msg_pb.SDKLoginResponse.decode(buffer);

	ViewManager.hideLoading();
	//判断是否登录成功
	if ((reponse.MsgCode == enLoginRepose.enLoginRepose_Success)) {
		//sdk登录成功
		LoginPart.GameLogin();
	} else if ((reponse.MsgCode == enLoginRepose.enLoginRepose_AccountError) ){
		//帐号ID错误
		ToolTipPopPanel.Show(11001);
	} else if ((reponse.MsgCode == enLoginRepose.enLoginRepose_GameIDError) ){
		//游戏ID错误
		ToolTipPopPanel.Show(11002);
	} else if ((reponse.MsgCode == enLoginRepose.enLoginRepose_GameIDUnFound) ){
		//游戏ID未找到
		ToolTipPopPanel.Show(11003);
	} else if ((reponse.MsgCode == enLoginRepose.enLoginRepose_NotLogin) ){
		//您还未登录,请先登录再试
		ToolTipPopPanel.Show(11004);
	} else if ((reponse.MsgCode == enLoginRepose.enLoginRepose_LoginRepeat) ){
		//您的账号在其他地方登陆,请重新登陆后再试
		ToolTipPopPanel.Show(11005);
	} else {
		//其他异常
		ToolTipPopPanel.Show(11006);
	}
}
window.SDKPart = SDKPart;