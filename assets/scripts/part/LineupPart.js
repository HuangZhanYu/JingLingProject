let lineup_msg_pb = require('lineup_msg').msg;
let  card_msg_pb = require('card_msg').msg;

import CanShuTool from '../tool/CanShuTool';
import Card from '../logic/Card';
import { FightHurtType } from '../logic/fight/FightDef';
import SocketClient from '../network/SocketClient';
import ViewManager from '../manager/ViewManager';
import { PanelResName } from '../common/PanelResName';

//region *.lua
//Date
//此文件由[BabeLua]插件自动生成
let LineupPart = { };

LineupPart.CardList = [];
//卡牌列表
LineupPart.CardCapacity = 0;
//可上阵数量
LineupPart.ZhanliBefore = "0";
//战力
LineupPart.IsNotShowToday = false;
//是否今日不显示弹窗
LineupPart.mSoliderLevelUpFunc = { };
//士兵等级提升回调的函数
LineupPart.mSoliderEnhanceLevelUpFunc = { };
//士兵强化等级提升回调的函数
LineupPart.mLineUpAddFunc = new Map();
//阵容增加卡牌后 回调函数
LineupPart.mLineUpCutFunc = { };
//阵容减少卡牌后 回调函数
LineupPart.BounsList = [];
//阵容额外加成列表
LineupPart.ChangeTag = false;
//属性变化标记
LineupPart.MaxEvoLevel = 7;
//最大进化等级
// 注册士兵升级提升调用函数
// 参数1 string 函数名
// 参数2 函数       注册函数 func(objID)
LineupPart.RegisterSoldierLevelUpFunc = function(funcName, restoreFunc) {
	LineupPart.mSoliderLevelUpFunc[funcName] = restoreFunc;
}
// 注册士兵强化升级提升调用函数
// 参数1 string 函数名
// 参数2 函数     注册函数 func(objID)
LineupPart.RegisterSoldierEnhanceLevelUpFunc = function(funcName, restoreFunc) {
	LineupPart.mSoliderEnhanceLevelUpFunc[funcName] = restoreFunc;
}
// 注册阵容增加卡牌 调用函数
// 参数1 string 函数名
// 参数2 函数     注册函数 func(objID) 背包的卡牌objid
LineupPart.RegisterLineupAddFunc = function(funcName, restoreFunc) {
	LineupPart.mLineUpAddFunc.set(funcName,restoreFunc);
}
// 注册阵容减少卡牌 调用函数
// 参数1 string 函数名
// 参数2 函数     注册函数 func(objID) 背包的卡牌objid
LineupPart.RegisterLineupCutFunc = function(funcName, restoreFunc) {
	LineupPart.mLineUpCutFunc[funcName] = restoreFunc;
}
//获取战力
LineupPart.GetZhanli = function() {
	var conf = CanShuTool.GetCanShuData(CanShuTool.CanShuEnum.ZhanliParam);
	var zhanli = "0";
	for (var card of LineupPart.CardList) {
		zhanli = BigNumber.add(zhanli, card.GetZhanli(conf.Param));
		
	}
	if (!this.ZhanliBefore) {
		this.ZhanliBefore = zhanli;
	}
	return zhanli;
}
//战力更新
LineupPart.ZhanliUp = function(buffer) {
	var msg = lineup_msg_pb.LineupPart_ZhanliUp.decode(buffer);

	var zhanli = msg.Zhanli;
	/*if this.ZhanliBefore then
        console.error("zhanli update:"..zhanli.." before:"..this.ZhanliBefore)
	end*/	//
	let MainPanel = ViewManager.getPanel(PanelResName.MainPanel);
	if(MainPanel)
	{
		MainPanel.UpdateTeamStrength(zhanli);
	}
	//log("zhanliup:"..zhanli.." before:"..this.ZhanliBefore)
	this.ZhanliBefore = zhanli;
}
var lineupaddKey = null;
//接受服务器下发数据
LineupPart.RevcCardInfo = function(buffer) {
	var msg = card_msg_pb.SendLineupAllCardInfo.decode(buffer);

	if ((msg.Group == 0)) {
		LineupPart.CardList.length = 0;
	}
	//console.error('卡牌数量'..LineupPart.CardList.."服务器数量"..msg.CardInfoList)
	var cardInfo = { };
	var cardCount = LineupPart.CardList.length;
	//添加卡牌信息到卡牌列表
	for (var i = 0; i != msg.CardInfoList.length; ++i) {
		cardInfo = msg.CardInfoList[i];
		var cardIndex = (i + cardCount);
		LineupPart.CardList[cardIndex] = new Card(cardInfo);
	}
	LineupPart.CardCapacity = msg.CurrentCapacity;
	if ((msg.Group == 1)) {
		lineupaddKey = msg.CardInfoList[0].ObjID;
		LineupPart.SendLineupAdd();
	}
	//刷新界面
	//DigimonDetailPopPanel.RefreshTopInfo();
	//DigimonDetailPopPanel.RefreshTalentPanel();
	//DigimonDetailPopPanel.FreshAfterPop();
	//刷新士兵界面
	//LineupPopPanel.SoldierLevelUpResponse.decode(buffer);
	//AuditLin1stPopPanel.SoldierLevelUpResponse.decode(buffer);
	if ((msg.Group == 0)) {
		//如果是复活，或者登陆，可能要打开
		GuanQiaPart.SetLineupInitFlag(true);
		//普通闯关
		GuanQiaPart.Init();
	}
	//刷新红点
	//MainPanel.SetLineUpRedPoint();
}
//接收服务器下发的额外加成数据
LineupPart.RecvBounsList = function(buffer) {
	var msg = lineup_msg_pb.LineupPart_BounsInfo.decode(buffer);

	LineupPart.BounsList.length = 0;
	for (var i = 0; i != msg.Levels.length; ++i) {
		LineupPart.BounsList[i] = { };
		LineupPart.BounsList[i].Level = msg.Levels[i];
		LineupPart.BounsList[i].TriggerTime = msg.TriggerTime[i];
	}
}
//获取等级加成
LineupPart.GetBounsLevel = function() {
	if ((LineupPart.BounsList.length == 0)) {
		return 0;
	}
	var level = 0;
	for (var i = 0; i != LineupPart.BounsList.length; ++i) {
		level = (level + LineupPart.BounsList[i].Level);
	}
	return level;
}
LineupPart.SendLineupAdd = function() {
	for (var [key,value ] of LineupPart.mLineUpAddFunc) {
		
		value(lineupaddKey);
	}
}
//更改弹窗状态请求
LineupPart.ChangePopStateRequest = function(isShow) {
	var request = new  lineup_msg_pb.LineupPart_ChangePopStateRequest();
	request.IsNotShowToday = isShow;
	SocketClient.callServerFun('Lineup_ChangePopState', request);
}
//更改弹窗状态回应
LineupPart.ChangePopStateResponse = function(buffer) {
	var msg = lineup_msg_pb.LineupPart_ChangePopStateResponse.decode(buffer);

	if ((msg.Flag != 0)) {
		console.error(("更改状态失败,错误ID" + msg.Flag));
		return;
	}
	LineupPart.IsNotShowToday = msg.IsNotShowToday;
}
//服务器更新卡牌数据
LineupPart.OnServerUpdataCardInfo = function(buffer) {
	var msg = card_msg_pb.UpdataCardInfo.decode(buffer);

	var haveCard = false;
	var needUpdateCard = null;
	//查找客户端卡牌对象并且赋值
	for (var i = 0; i != LineupPart.CardList.length; ++i) {
		if ((LineupPart.CardList[i].ObjID == msg.Info.ObjID)) {
			LineupPart.CardList[i].SetCardInfo(msg.Info);
			needUpdateCard = LineupPart.CardList[i];
			haveCard = true;
		}
	}
	//如果不存在卡牌则增加一个
	if ((haveCard == false)) {
		var cardIndex = (LineupPart.CardList.length + 1);
		LineupPart.CardList[cardIndex] = new Card(msg.Info);
		needUpdateCard = LineupPart.CardList[cardIndex];
	}
	//如果数量小于等于1  则不用排序
	if ((LineupPart.CardList.length > 1)) {
		//LineupPart.BackSort()
	}
	//刷新界面
	//DigimonDetailPopPanel.RefreshTopInfo();
	//DigimonDetailPopPanel.RefreshTalentPanel();
	//DigimonDetailPopPanel.FreshAfterPop();
	//刷新士兵界面
	LineupPopPanel.SoldierLevelUpResponse();
	AuditLin1stPopPanel.SoldierLevelUpResponse();
}
//获取卡牌信息
LineupPart.GetCardByObjectId = function(cardObjectId) {
	for (var i = 0; i != LineupPart.CardList.length; ++i) {
		if ((LineupPart.CardList[i].ObjID == cardObjectId)) {
			return LineupPart.CardList[i];
		}
	}
	return null;
}
//请求升级阵容中的某个卡牌
//参数1  唯一ID
//参数2  升级次数
var normalObj = null;
LineupPart.LevelUpCardRequest = function(objId, count) {
	var request = new  lineup_msg_pb.LineupPart_LevelUpCardRequest();
	normalObj = objId;
	request.ObjId = objId;
	request.Count = count;
	SocketClient.callServerFun('Lineup_LevelUpCard', request);
}
//升级回应
LineupPart.LevelUpCardResponse = function(buffer) {
	var msg = lineup_msg_pb.LineupPart_LevelUpCardResponse.decode(buffer);

	if ((msg.Flag != 0)) {
		return;
	}
	//LineupPopPanel.DetailEnabled = false;
	//设置不可点击详情
	//timerMgr.RemoveTimerEvent("LineupPopPanel.DelayDetailClick");
	//timerMgr.AddTimerEvent("LineupPopPanel.DelayDetailClick", LineupPopPanel.DetailDelayTime, LineupPopPanel.DelayDetailClick, null, false);
	//大数字属性更新
	//LineupPart.SendFightLv();
	//主界面红点刷新
	//MainPanel.SetLineUpRedPoint();
	//小目标刷新
	//XiaoMuBiaoPanel.SetInfo();
	//刷新引导
	//LineupPopPanel.ShowGuide();
}
//用于回调战斗 告诉他等级提升到了多少
LineupPart.SendFightLv = function() {
	var level = LineupPart.GetCardByObjectId(normalObj).Level;
	for (var key in LineupPart.mSoliderLevelUpFunc) {
		var value = LineupPart.mSoliderLevelUpFunc[key];
		{
			value(normalObj);
		}
	}
}
//请求升级某个卡牌的强化等级
//参数1 卡牌唯一ID
//参数2 强化次数
var enhanceObj = null;
LineupPart.EnhanceLevelUpRequest = function(objId, count) {
	var request = new  lineup_msg_pb.LineupPart_EnhanceLevelUpRequest();
	enhanceObj = objId;
	request.ObjId = objId;
	request.Count = count;
	SocketClient.callServerFun('Lineup_EnhanceLevelUp', request);
}
//强化等级升级回应
LineupPart.EnhanceLevelUpResponse = function(buffer) {
	var msg = lineup_msg_pb.LineupPart_EnhanceLevelUpResponse.decode(buffer);

	if ((msg.Flag != 0)) {
		//console.error("强化失败，错误ID "..msg.Flag)
		return;
	}
	//小目标刷新
	XiaoMuBiaoPanel.SetInfo();
	//刷新
	//DigimonDetailPopPanel.ShowGuide();
	//DigimonDetailPopPanel.RefreshTopInfo();
	//DigimonDetailPopPanel.RefreshDetailPanel();
}
//用于回调战斗 告诉他强化等级提升到了多少
LineupPart.SendFightEnhanceLv = function() {
	var enhancelevel = LineupPart.GetCardByObjectId(enhanceObj).EvolutionLevel;
	for (var key in LineupPart.mSoliderEnhanceLevelUpFunc) {
		var value = LineupPart.mSoliderEnhanceLevelUpFunc[key];
		{
			value(enhanceObj);
		}
	}
}
//阵容排序规则   高品质在前，低品质在后,同品质时，高进化等级的在前,相同进化等级时，高训练等级的在前。同训练等级时，强化等级高的在前。强化等级相同时，按ID排序。
LineupPart.CardBagCom = function(a, b) {
	var aCard = a;
	var bCard = b;
	//获取表格配置
	var aConf = LoadConfig.getConfigData(ConfigName.ShiBing,aCard.CardID);
	var bConf = LoadConfig.getConfigData(ConfigName.ShiBing,bCard.CardID);
	//再更具品质排序
	if ((aConf.Quality != bConf.Quality)) {
		return (aConf.Quality > bConf.Quality);
	}
	//根据强化等级排序
	if ((aCard.EvolutionLevel != bCard.EvolutionLevel)) {
		return (aCard.EvolutionLevel > bCard.EvolutionLevel);
	}
	//根据进化等级排序
	if ((aConf.EVO != bConf.EVO)) {
		return (aConf.EVO > bConf.EVO);
	}
	//根据金币等级排序
	if ((aCard.Level != bCard.Level)) {
		return (aCard.Level > bCard.Level);
	}
	//最后根据ID排序
	return (aCard.CardID > bCard.CardID);
}
//卡牌阵容界面排序
LineupPart.BackSort = function() {
	LineupPart.CardList.sort(LineupPart.CardBagCom)
}
//阵位增加请求
LineupPart.AddLineupPositionRequest = function() {
	var request = new  lineup_msg_pb.LineupPart_AddNewPositionRequest();
	ViewManager.showLoading({});
	SocketClient.callServerFun('Lineup_AddNewPosition', request);
}
LineupPart.AddLineupPositionResponse = function(buffer) {
	var msg = lineup_msg_pb.LineupPart_AddNewPositionResponse.decode(buffer);

	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		console.error(("开启失败,错误ID" + msg.Flag));
		return;
	}
	LineupPart.CardCapacity = msg.Capacity;
	//刷新界面

	MainPanel.UpdatePlayerInfo();
	LineupPopPanel.OnRestore();
}
var changeLineID = null;
var changeBagID = null;
var changeValueBefore = { };
var changeValueAfter = { };
var changeDelta = { };
var changeEnum = { };
var cardIDs = { };
//添加背包卡牌到阵容 请求
LineupPart.AddLineupCardRequest = function(cardId, key) {
	var request = new  lineup_msg_pb.LineupPart_AddCardRequest();
	request.CardID = cardId;
	//存下训练大师相关
	LineupPart.BeforeChange();
	SocketClient.callServerFun('Lineup_AddCardToLineUp', request);
	ViewManager.showLoading({});
}
//添加背包卡牌到阵容 回应
LineupPart.AddLineupCardResponse = function(buffer) {
	var msg = lineup_msg_pb.LineupPart_AddCardResponse.decode(buffer);

	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		console.error(("开启失败,错误ID" + msg.Flag));
		return;
	}
	//上阵成功回调
	AddDigimonPopPanel.OnAddSuccess();
	MainPanel.UpdateModels();
	//刷新静默行军模型，函数里面已经处理了没有打开静默行军的情况
}
//更换阵容中的某一个卡牌请求
//参数1 阵容卡牌
//参数2 背包卡牌
LineupPart.ChangeCardRequest = function(lineupKey, backpackKey) {
	var request = new  lineup_msg_pb.LineupPart_ChangeCardRequest();
	request.LineUpCardKey = lineupKey;
	request.BackPackCardID = backpackKey;
	//存下训练大师相关
	LineupPart.BeforeChange();
	SocketClient.callServerFun("Lineup_ChangeLineUpCard", request);
	ViewManager.showLoading({});
}
//更换阵容中的某一个卡牌回应
LineupPart.ChangeCardResponse = function(buffer) {
	var msg = lineup_msg_pb.LineupPart_ChangeCardResponse.decode(buffer);

	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		console.error(("开启失败,错误ID" + msg.Flag));
		
		return;
	}
	//换阵成功回调
	AddDigimonPopPanel.OnChangeSuccess();
	MainPanel.UpdateModels();
	//刷新静默行军模型，函数里面已经处理了没有打开静默行军的情况
}
//延迟换阵
LineupPart.DelayChangeSucess = function() {
	//console.error("换阵后ID"..changeLineID.."属性ID"..LoadConfig.getConfigData(ConfigName.JingLingShuXingZhanShi,changeLineID).Enum.."属性值"..PlayerAttrPart.GetSummaryAttr(LoadConfig.getConfigData(ConfigName.JingLingShuXingZhanShi,changeLineID).Enum))
	//console.error("换阵后ID"..changeBagID.."属性ID"..LoadConfig.getConfigData(ConfigName.JingLingShuXingZhanShi,changeBagID).Enum.."属性值"..PlayerAttrPart.GetSummaryAttr(LoadConfig.getConfigData(ConfigName.JingLingShuXingZhanShi,changeBagID).Enum))
	var lineEnum = LoadConfig.getConfigData(ConfigName.JingLingShuXingZhanShi,changeLineID).Enum;
	var bagEnum = null;
	if ((changeBagID != null)) {
		bagEnum = LoadConfig.getConfigData(ConfigName.JingLingShuXingZhanShi,changeBagID).Enum;
	}
	changeValueAfter = { };
	changeValueAfter[0] = PlayerAttrPart.GetSummaryAttr(lineEnum);
	if ((bagEnum != null)) {
		changeValueAfter[1] = PlayerAttrPart.GetSummaryAttr(bagEnum);
	}
	changeEnum = { };
	changeDelta = { };
	cardIDs = { };
	if ((changeValueAfter[0] == null)) {
		changeValueAfter[0] = 0;
	}
	if ((changeValueBefore[0] == null)) {
		changeValueBefore[0] = 0;
	}
	if (((lineEnum == bagEnum) || (bagEnum == null))) {
		changeEnum[0] = lineEnum;
		changeDelta[0] = (parseInt(changeValueAfter[0]) - parseInt(changeValueBefore[0]));
		cardIDs[0] = changeLineID;
	} else {
		changeEnum[0] = lineEnum;
		changeEnum[1] = bagEnum;
		changeDelta[0] = (parseInt(changeValueAfter[0]) - parseInt(changeValueBefore[0]));
		changeDelta[1] = (parseInt(changeValueAfter[1]) - parseInt(changeValueBefore[1]));
		cardIDs[0] = changeLineID;
		cardIDs[1] = changeBagID;
	}
	//阵容换阵成功
	LineupPopPanel.ChangeSuccess(changeEnum, changeDelta, cardIDs);
}
var lineupCutkey = null;
//删除卡牌
LineupPart.OnServerDelCard = function(buffer) {
	var msg = card_msg_pb.UpdataCardInfo.decode(buffer);

	//查找客户端卡牌对象并且赋值
	for (var i = 0; i != LineupPart.CardList.length; ++i) {
		if ((LineupPart.CardList[i].ObjID == msg.Info.ObjID)) 
		{
			LineupPart.CardList.slice(i,1);
			break;
		}
	}
	lineupCutkey = msg.Info.ObjID;
	LineupPart.SendLineupCut();
	LineupPart.SendLineupAdd();
}
//发送消息
LineupPart.SendLineupCut = function() {
	for (var key in LineupPart.mLineUpCutFunc) {
		var value = fLineupPart.mLineUpCutFunc[key];
		{
			value(lineupCutkey);
		}
	}
}
//进化卡牌  要进化的唯一ID  进化方式1进化石  2 材料
LineupPart.EvolutionCardRequest = function(objID, evoType) {
	var request = new  lineup_msg_pb.LineupPart_EvolutionCardRequest();
	request.ObjId = objID;
	request.EvoType = evoType;
	//存下训练大师相关
	LineupPart.BeforeChange();
	SocketClient.callServerFun("Lineup_EvolutionCard", request);
	//ViewManager.showLoading({});
}
//进化卡牌回应
LineupPart.EvolutionCardResponse = function(buffer) {
	var msg = lineup_msg_pb.LineupPart_EvolutionCardResponse.decode(buffer);

	//ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		console.error(("进化失败,错误ID" + msg.Flag));
		return;
	}
	//刷新界面  ObjID
	//DigimonDetailPopPanel.OnEvoSuccess(msg);
	LineupPart.BackSort();
	//LineupPopPanel.SoldierLevelUpResponse.decode(buffer);
	//刷新界面
	//MainPanel.UpdateModels();
	//刷新静默行军模型，函数里面已经处理了没有打开静默行军的情况
}
//遗物升级后 刷新卡牌属性
LineupPart.UpdateCardPro = function() {

	//刷新界面
	//LineupPopPanel.SoldierLevelUpResponse();
}
//获取最高卡牌等级
//返回等级
LineupPart.GetCardMaxGoldLevel = function() {
	var maxGoldLevel = 0;
	for (var i = 0; i != LineupPart.CardList.length; ++i) {
		if ((LineupPart.CardList[i].Level > maxGoldLevel)) {
			maxGoldLevel = LineupPart.CardList[i].Level;
		}
	}
	return maxGoldLevel;
}
//获取当前最大的卡牌进化等级(进化等级)
LineupPart.GetMaxEvoLv = function() {
	var level = 0;
	for (var forinvar of LineupPart.CardList) {
		
		var card = forinvar.card;
		{
			var conf_Card = LoadConfig.getConfigData(ConfigName.ShiBing,card.CardID);
			//不存在直接con
			if ((conf_Card != null)) {
				if ((conf_Card.EVO > level)) {
					level = conf_Card.EVO;
				}
			}
		}
	}
	return level;
}
//获取最高勋章等级
//返回等级
LineupPart.GetCardMaxMedalLevel = function() {
	var maxMedalLevel = 0;
	for (var i = 0; i != LineupPart.CardList.length; ++i) {
		if ((LineupPart.CardList[i].EvolutionLevel > maxMedalLevel)) {
			maxMedalLevel = LineupPart.CardList[i].EvolutionLevel;
		}
	}
	return maxMedalLevel;
}
//激活天赋回调
LineupPart.OnTianFuAdd = function(buffer) {
	var msg = lineup_msg_pb.LineupPart_OnTianFuOpenResponse.decode(buffer);

	var cardID = msg.CardID;
	var name = "";
	var conf_ShiBing_Data = LoadConfig.getConfigData(ConfigName.ShiBing,cardID);
	if ((conf_ShiBing_Data != null)) {
		name = conf_ShiBing_Data.Name;
	}
	var conf_ShiBing_TianFuShuXing_Data = LoadConfig.getConfigData(ConfigName.ShiBing_TianFuShuXing,cardID);
	if ((conf_ShiBing_TianFuShuXing_Data != null)) {
		for (var i = 0; i != msg.Indexs.length; ++i) {
			var index = (msg.Indexs[i] + 1);
			var value = BigNumber.create(msg.Values[i]);
			value = BigNumber.div(value, BigNumber.create(100));
			var des = conf_ShiBing_TianFuShuXing_Data.Describe[index];
			var showStr = TranslateTool.GetText(200376);
			showStr = StringTool.ZhuanHuan(showStr, [
					name,
					des,
					BigNumber.getValue(value)
				]);
			CommonHUDPanel.Show(showStr);
		}
	}
}
//获取升级消耗
//参数，card 卡牌对象
//参数，curLevel 当前等级
//参数，upLevel 需要升级的等级，只能传 1,10,100,500,1000，如果超过自身的最大等级，返回已经计算过的结果
//返回 升级消耗的金币
//返回 是否Max
//返回实际升级的数量
LineupPart.GetLevelUpCost = function(card, upLevel) {
	var maxLevel = card.MaxLevel;
	var curLevel = card.Level;
	var index = 0;
	if ((upLevel == 1)) {
		index = 1;
	} else if ((upLevel == 10) ){
		index = 2;
	} else if ((upLevel == 100) ){
		index = 3;
	} else if ((upLevel == 500) ){
		index = 4;
	} else if ((upLevel == 1000) ){
		index = 5;
	}
	if ((index == 0)) {
		console.error(("LineupPart.GetLevelUpCost的upLevel错误:" + upLevel));
		return "0", false, 0;
	}
	//如果没有超过界限，返回读表的值
	if ((maxLevel >= (curLevel + upLevel))) {
		var conf_ShiBing_TianFuDengJi_Data = LoadConfig.getConfigData(ConfigName.ShiBing_TianFuDengJi,curLevel);
		if ((conf_ShiBing_TianFuDengJi_Data == null)) {
			return "0", false, 0;
		}
		var result = (conf_ShiBing_TianFuDengJi_Data.Cost[index] || "0");
		result = BigNumber.create(result);
		result = LineupPart.GetRealUpCost(result);
		return result, false, upLevel;
	}
	//如果超过最大值，计算后返回
	var cost = "0";
	var tempLevel = curLevel;
	var realUpLevel = (maxLevel - curLevel);
	while((realUpLevel > 0)) {
		var canUpdate = 0;
		var tempIndex = 0;
		if ((1000 <= realUpLevel)) {
			canUpdate = 1000;
			tempIndex = 5;
		} else if ((500 <= realUpLevel) ){
			canUpdate = 500;
			tempIndex = 4;
		} else if ((100 <= realUpLevel) ){
			canUpdate = 100;
			tempIndex = 3;
		} else if ((10 <= realUpLevel) ){
			canUpdate = 10;
			tempIndex = 2;
		} else if ((1 <= realUpLevel) ){
			canUpdate = 1;
			tempIndex = 1;
		}
		var conf_ShiBing_TianFuDengJi_Data = LoadConfig.getConfigData(ConfigName.ShiBing_TianFuDengJi,tempLevel);
		if ((conf_ShiBing_TianFuDengJi_Data == null)) {
			return "0", false, 0;
		}
		var result = BigNumber.create(conf_ShiBing_TianFuDengJi_Data.Cost[tempIndex]);
		cost = BigNumber.add(cost, result);
		realUpLevel = (realUpLevel - canUpdate);
		tempLevel = (tempLevel + canUpdate);
	}
	return LineupPart.GetRealUpCost(cost), true, (maxLevel - curLevel);
}
//计算削减后的升级消耗
LineupPart.GetRealUpCost = function(baseCost) {
	var realCost = "0";
	var buff = PlayerAttrPart.GetSummaryAttr(PlayerAttrPart.Addition_Summary_Type.UpgrateCoseReduce);
	//表中的所有  50000
	var totalBuff = BigNumber.add(BigNumber.create(buff), BigNumber.create("10000"));
	realCost = BigNumber.mul(baseCost, "10000");
	realCost = BigNumber.div(realCost, totalBuff);
	if (BigNumber.lessThan(realCost, BigNumber.create("1"))) {
		realCost = "0";
	}
	return realCost;
}
//判断金币是否足够  并且返回显示文字
LineupPart.GetCostTextAndBool = function() {
	var level = PlayerBasePart.GetIntValue(PlayerIntAttr.PlayerIntAttr_TeamTrainingLevel);
	//注意勋章强化等级初始是0
	var costNow = this.GetCurCost(level, 1)[0];
	//比较大小
	var gold = PlayerBasePart.StrAttr[PlayerStrAttr.PlayerStrAttr_BigNumberMedal];
	var bool = BigNumber.GreaterThanOrEqualTo(gold, costNow);
	return costNow, bool;
}
//获取当前等级 + 10  +N 的消耗
//参数1  当前等级
//参数2  需要升级的次数
LineupPart.GetCurCost = function(curLevel, count) {
	//如果没有超过界限，返回读表的值
	var conf_Cure = LoadConfig.getConfigData(ConfigName.ShiBing_XunLianDengJi,(curLevel + 1));
	if ((conf_Cure == null)) {
		return "1zz";
	}
	var result = { };
	for (var i = 0; i != conf_Cure.Cost.length; ++i) {
		result[i] = BigNumber.create(conf_Cure.Cost[i]);
	}
	return result;
}
//计算通关评估
LineupPart.CalPlayerFightPower = function() {
	//LineupPart.BackSort()  --排序
	var fightPower = 0;
	//计算每个卡牌的战力
	for (var i = 0; i != LineupPart.CardList.length; ++i) {
		var cardID = LineupPart.CardList[i].CardID;
		fightPower = (fightPower + LineupPart.CardList[i].EvolutionLevel);
	}
	return fightPower;
};
//  log2 4 = 2   输入 4,2
LineupPart.LogN = function(x, n) {
	var y = 0;
	if (((x == 0) || (n == 0))) {
		console.error('warning:log of zero!');
		y = 0;
	} else if ((n == 1) ){
		console.error('error:n==1');
		y = 0;
	} else {
		if (((x < 0) || (n < 0))) {
			console.error('warning:the result will be imaginary!');
			y = 0;
			return y;
		}
		if ((n == 10)) {
			y = Math.log10(x);
		} else {
			y = (Math.log(x) / Math.log(n));
		}
	}
	return y;
};
//----------------------------训练大师飘字相关--------------------------------------
var soulvalue = null;
//炽魂属性
var upvalue = null;
//强化属性
var revivevalue = null;
//重置属性
var dropvalue = null;
//掉落属性
var boxvalue = null;
//礼盒属性
var hudList = [];
//飘字列表
var hudIndex = 1;
//飘字索引
var hudCount = 1;
//飘字总数
//存下训练大师相关属性，用于飘字
LineupPart.BeforeChange = function() {
	hudList = [];
	soulvalue = PlayerAttrPart.GetSummaryAttr(PlayerAttrPart.Addition_Summary_Type.BaseLevelAdd);
	upvalue = PlayerAttrPart.GetSummaryAttr(PlayerAttrPart.Addition_Summary_Type.LevelLimitAdd);
	revivevalue = PlayerAttrPart.GetSummaryAttr(PlayerAttrPart.Addition_Summary_Type.ReviveWenZhangAdd);
	dropvalue = PlayerAttrPart.GetSummaryAttr(PlayerAttrPart.Addition_Summary_Type.DiaoLuoWenZhang);
	boxvalue = PlayerAttrPart.GetSummaryAttr(PlayerAttrPart.Addition_Summary_Type.LiHeWenZhang);
	LineupPart.ChangeTag = true;
}
//显示飘字
LineupPart.HUDTextTrainMaster = function() {
	var chihunText = LoadConfig.getConfigData(ConfigName.ShuZhiShuXingDingYi,2210);
	var qianghuaText = LoadConfig.getConfigData(ConfigName.ShuZhiShuXingDingYi,2211);
	var chongzhiText = LoadConfig.getConfigData(ConfigName.ShuZhiShuXingDingYi,2212);
	var diaoluoText = LoadConfig.getConfigData(ConfigName.ShuZhiShuXingDingYi,2213);
	var liheText = LoadConfig.getConfigData(ConfigName.ShuZhiShuXingDingYi,2214);
	if ((((((chihunText == null) || (qianghuaText == null)) || (chongzhiText == null)) || (diaoluoText == null)) || (liheText == null))) {
		return;
	}
	var detlaSoul = BigNumber.sub(PlayerAttrPart.GetSummaryAttr(PlayerAttrPart.Addition_Summary_Type.BaseLevelAdd), soulvalue);
	var detlaUp = BigNumber.sub(PlayerAttrPart.GetSummaryAttr(PlayerAttrPart.Addition_Summary_Type.LevelLimitAdd), upvalue);
	var detlaRevive = BigNumber.sub(PlayerAttrPart.GetSummaryAttr(PlayerAttrPart.Addition_Summary_Type.ReviveWenZhangAdd), revivevalue);
	var detlaDrop = BigNumber.sub(PlayerAttrPart.GetSummaryAttr(PlayerAttrPart.Addition_Summary_Type.DiaoLuoWenZhang), dropvalue);
	var detlaBox = BigNumber.sub(PlayerAttrPart.GetSummaryAttr(PlayerAttrPart.Addition_Summary_Type.LiHeWenZhang), boxvalue);
	var deltaList = [
		detlaSoul,
		detlaUp,
		detlaRevive,
		detlaDrop,
		detlaBox
	];
	var conf_Data = [
		chihunText,
		qianghuaText,
		chongzhiText,
		diaoluoText,
		liheText
	];
	//循环加入飘字
	for (var i = 0; i != 5; ++i) {
		LineupPart.AddOneHud(deltaList[i], conf_Data[i]);
	}
	hudCount = hudList.length;
	//根据飘字长度飘字
	if ((hudCount <= 0)) {
		return;
	}
	hudIndex = 1;
	LineupPart.ShowAttrHud();
}
//连续飘字
LineupPart.ShowAttrHud = function() {
	CommonHUDPanel.Show(hudList[hudIndex], true);
	if ((hudIndex < hudCount)) {
		//开启定时器 0.5秒后开启
		hudIndex = (hudIndex + 1);
		//timerMgr.AddTimerEvent('LineupPart.ShowAttrHud', 0.3, this.ShowAttrHud, { }, false);
	}
}
//添加一条飘字
LineupPart.AddOneHud = function(delta, confPro) {
	if (BigNumber.notEqualTo(delta, BigNumber.zero)) {
		var symbom = "";
		if (BigNumber.greaterThan(delta, BigNumber.zero)) {
			symbom = "+";
		}
		if ((confPro.Type == "万分率")) {
			delta = (BigNumber.div(delta, "100") + "%");
		}
		//改变颜色
		var colorText = (symbom + delta);
		if ((symbom == "")) {
			colorText = StringTool.AddTextColor(colorText, 6);
		} else {
			colorText = StringTool.AddTextColor(colorText, 2);
		}
		var showText = (confPro.Name + colorText);
		hudList.push(showText);
	}
}
//获取金币强化折算等级
LineupPart.GetFightLevel = function() {
	var maxPhyLevel = 0;
	var maxMagLevel = 0;
	for (var i = 0; i != LineupPart.CardList.length; ++i) {
		var card = LineupPart.CardList[i];
		var conf = LoadConfig.getConfigData(ConfigName.ShiBing,card.CardID);
		if ((((conf != null) && (conf.HurtType == FightHurtType.Physical)) && (card.Level > maxPhyLevel))) {
			maxPhyLevel = card.Level;
		}
		if ((((conf != null) && (conf.HurtType == FightHurtType.Magical)) && (card.Level > maxMagLevel))) {
			maxMagLevel = card.Level;
		}
	}
	return Math.floor((((maxPhyLevel + maxMagLevel)) / 2));
}
//获取当前百万进化的精灵数量
LineupPart.GetMegaCardCount = function() {
	var count = 0;
	for (var card of LineupPart.CardList){
		var conf = LoadConfig.getConfigData(ConfigName.ShiBing,card.CardID);
		//不存在直接con
		if (conf != null) {
			if ((conf.EVO == 7)) {
				count = (count + 1);
			}
		}
		
	}
	for (var card of BackPackPart.mCardList) {
		var conf = LoadConfig.getConfigData(ConfigName.ShiBing,card.CardID);
		//不存在直接con
		if (conf != null) {
			if ((conf.EVO == 7)) {
				count = (count + card.Count);
			}
		}
		
	}
	return count;
}
//获取当前强化等级
LineupPart.GetCurrentEnhanceLevel = function() {
	var level = 0;
	for (var card of LineupPart.CardList) {
		
		if (card.EvolutionLevel > 0) {
			level = card.EvolutionLevel;
			break;
		}
	
	}
	return level;
}
//获取某ID的卡牌数量
LineupPart.GetCardCountByID = function(cardid) {
	var count = 0;
	for (var card of LineupPart.CardList) {
		
		if (card.CardID == cardid) {
			count = (count + 1);
		}
		
	}
	return count;
}
//获取某ID的卡牌是否存在
LineupPart.IsExistByID = function(cardid) {
	for (var card of LineupPart.CardList) {
		
		if ((card.CardID == cardid)) {
			return true;
		}
		
	}
	return false;
}
//获取某ID的系列卡牌是否存在
LineupPart.IsSeriesExistByID = function(cardid) {
	var origin = (Math.floor((cardid / 10)) * 10);
	//初始ID  个位为0
	for (var card of LineupPart.CardList) {
		
		for (var j = 0; j != 7; ++j) {
			var id = (origin + j);
			if ((card.CardID == id)) {
				return true;
			}
		}
		
	}
	return false;
}
//判断某张卡是否足够材料进化
LineupPart.IsCardCouldEvo = function(cardid) {
	var cardinfo = LoadConfig.getConfigData(ConfigName.ShiBing,cardid);
	if ((cardinfo == null)) {
		return false;
	}
	//如果当前已经是最大进化等级
	if ((cardinfo.EVO >= LineupPart.MaxEvoLevel)) {
		return false;
	}
	//如果没有达到进化等级也要false
	var functionID = (117 + cardinfo.EVO);
	//根据这张卡当前的进化等级，判断进化到下一级是否开放  进化+1的功能开放ID为117
	if (!FunctionOpenTool.IsFunctionOpen(functionID)) {
		return false;
	}
	var honor = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_HuPoBi];
	if ((honor < cardinfo.CostRongYuBiNum)) {
		return false;
	}
	//判断进化的卡牌是否拥有
	for (var i = 0; i != cardinfo.EvoResType.length; ++i) {
		if ((cardinfo.EvoResType[i] == 0)) {
			break;
		}
		var count = ResourceTool_GetCurrentCount(cardinfo.EvoResType[i], cardinfo.EvoResID[i]);
		var bigCount = BigNumber.create(count);
		var bigCost = BigNumber.create(cardinfo.EvoResCount[i]);
		if (BigNumber.lessThan(bigCount, bigCost)) {
			return false;
		}
	}
	return true;
}
//判断某张卡是否拦截并提示(针对进化+0)
LineupPart.IsInterceptCard = function(cardid) {
	var cardinfo = LoadConfig.getConfigData(ConfigName.ShiBing,cardid);
	if ((cardinfo == null)) {
		return true;
	}
	//去进化按钮和页签只拦截第一次进化  也就是进化+0
	if ((cardinfo.EVO > 0)) {
		return false;
	}
	var functionID = (117 + cardinfo.EVO);
	//根据这张卡当前的进化等级，判断进化到下一级是否开放  进化+1的功能开放ID为117
	//如果开放了，不拦截
	if (FunctionOpenTool.ShowTips(functionID)) {
		return false;
	} else {
		return true;
	}
}
//判断某张卡是否达到主角进化等级要求，返回false则不能进化
LineupPart.IsCardGetToEvolutionLv = function(cardid) {
	var cardinfo = LoadConfig.getConfigData(ConfigName.ShiBing,cardid);
	if ((cardinfo == null)) {
		return false;
	}
	var functionID = (117 + cardinfo.EVO);
	//根据这张卡当前的进化等级，判断进化到下一级是否开放  进化+1的功能开放ID为117
	//如果没开放，返回false
	if (!FunctionOpenTool.ShowTips(functionID)) {
		return false;
	}
	return true;
}
//endregion
window.LineupPart = LineupPart;