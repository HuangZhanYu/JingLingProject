//region *.lua
//Date
//此文件由[BabeLua]插件自动生成
let msg_pb = require('msg').msg;
import MessageCenter from "../tool/MessageCenter";
import { MessageID } from '../common/MessageID';
import SocketClient from '../network/SocketClient';
import ViewManager from '../manager/ViewManager';
import { PanelResName } from '../common/PanelResName';
import LocalStorage from '../tool/LocalStorage';

let LoginPart = { };

var mServerID = 0;
//服务器ID
//苹果服kdwj.smbbgo.com:8094
//苹果3服kdwj.smbbgo.com:8095
//海外 kdwjbj.smbbgo.com:8090
//海外安卓 kdwjbj.smbbgo.com:8092
//酷派应用宝 112.74.31.59:8090
//内部测试服 120.24.169.210:8090
//国内安卓Demo包 120.24.169.210:8092
//内网 192.168.1.250:8090
//备案服 112.74.198.164:8097   
//小Y测试服120.24.169.210:8093
var mRootURL = "http://112.74.31.59:8093";
//根地址
var mURL = (mRootURL + "/act/ReturnList?userName=");
//ServerList请求地址
var mVersionURL = (mRootURL + "/act/Version?");
//Version请求地址
var mNoticeURL = "https://www.baidu.com/";
var mCenterURL = "http://112.74.31.59:8055";
var mAccount = '';
//用户名
var mPassword = '';
//密码
var mDeviceId = "fddfs";
//设备唯一标识
var mTotalServers = { };
//所有服务器
var mCurServerInfos = { };
//曾经的所有服务器账号
var mNotice = null;
//公告内容
var mVersion = '';
//版本号
var mIsAudit = null;
//审核标记
var mCreating = false;
var mWebSocial = { };
var OffLineType = {
	Duplicate: 0,
	//被其他人登录
	Force: 1,
	//被T下线
	TryAgain: 2,
	//

};
export const LoginError = {
	Duplicate: 0,
	//重复登陆
	Password: 1,
	//密码错误
	TryAgain :2,
};
export const CreateRoleResult = {
	Success: 0,
	//创建成功
	Already: 1,
	//已经有角色名
	Occupy: 2,
	//名字被占用

};
//服务器信息
LoginPart.PlayerInfo = {
	Name: "",
	//用户名
	Level: 0,
	//等级
	Icon: "",
	//头像图标
	ServerID: 0,
	//服务器ID

};
//这句是重定义元表的索引，就是说有了这句，这个才是一个类。
//LoginPart.PlayerInfo.__index = LoginPart.PlayerInfo;
//构造体，构造体的名字是随便起的，习惯性改为New()
// new LoginPart.PlayerInfo = function(name, level, icon, id) {
// 	var self = { };
// 	//初始化self，如果没有这句，那么类所建立的对象改变，其他对象都会改变
// 	setmetatable(self, LoginPart.PlayerInfo);
// 	//将self的元表设定为Class
// 	self.Name = name;
// 	self.Level = level;
// 	self.Icon = icon;
// 	self.ServerID = id;
// 	return self;
// 	//返回自身
// }
//服务器信息
LoginPart.ServerData = {
	ID: 0,
	//服务器ID
	Name: "",
	//服务器名称
	IP: "",
	//服务器地址
	Port: 0,
	//服务器端口
	State: 0,
	//服务器状态
	StateTips: "",
	//服务器状态文本
	Number: 0,
	//服务器序号
	Audit: 0,
	//是否是审核标记
	PushTag: 0,
	//推送分组

};
//这句是重定义元表的索引，就是说有了这句，这个才是一个类。
// LoginPart.ServerData.__index = LoginPart.ServerData;
// //构造体，构造体的名字是随便起的，习惯性改为New()
// new LoginPart.ServerData = function(id, name, ip, port, state, number, stateTips, audit, pushTag) {
// 	var self = { };
// 	//初始化self，如果没有这句，那么类所建立的对象改变，其他对象都会改变
// 	setmetatable(self, LoginPart.ServerData);
// 	//将self的元表设定为Class
// 	self.ID = id;
// 	self.Name = name;
// 	self.IP = ip;
// 	self.Port = port;
// 	self.State = state;
// 	self.StateTips = stateTips;
// 	self.Number = number;
// 	self.Audit = audit;
// 	self.PushTag = pushTag;
// 	return self;
// 	//返回自身
// }
//通过ID获取当前的服务器信息
//参数1，服务器ID
//参数2，返回服务器信息 类型Game.ServerDate
LoginPart.GetServerDate = function(id) {
	for (var i = 0; i != mTotalServers.length; ++i) {
		if ((mTotalServers[i].ID == id)) {
			return mTotalServers[i];
		}
	}
}
//获取服务器列表
//返回值，Game.ServerDate数组，第一个是最新的服务器
LoginPart.GetServerList = function() {
	return mTotalServers;
}
//获取已经存在角色的服务器列表
//返回值,通过遍历curServerInfos数组，获得existServerInfo
//通过existServerInfo可以点出，existServerInfo.PlayerInfo是玩家信息
//existServerInfo.ServerData，是服务器信息
LoginPart.GetExistServerInfos = function() {
	return mCurServerInfos;
}
//获取官网社交信息
LoginPart.GetWebSocialInfo = function() {
	return mWebSocial;
}
LoginPart.GetCenterURL = function() {
	return mCenterURL;
}
// 点击登录按钮
LoginPart.Login = function(acct, psd, serverID) {
	// 获取服务器信息
	// var serverData = LoginPart.GetServerDate(serverID);
	// if ((serverData == null)) {
	// 	return;
	// }
	// //判断版本号，如果服务器版本号为空，不进行验证，方便开发使用
	// if (!LoginPart.CheckVersion()) {
	// 	return;
	// }
	//ViewManager.showLoading({});
	mAccount = acct;
	mPassword = psd;
	//mServerID = serverID;
	// 链接服务器
	//AppConst.SocketPort = serverData.Port;
	//AppConst.SocketAddress = serverData.IP;
	//networkMgr.SendConnect();
	SocketClient.connectServer();
};
//获取服务器id
LoginPart.GetServerID = function() {
	return mServerID;
}
//获取服务器名称
LoginPart.GetServerName = function() {
	var serverData = LoginPart.GetServerDate(mServerID);
	if ((serverData == null)) {
		return '';
	}
	return serverData.Name;
}
//获取服务器推送标记
LoginPart.GetPushTag = function() {
	var serverData = LoginPart.GetServerDate(mServerID);
	if ((serverData == null)) {
		return '';
	}
	return (serverData.PushTag || "");
}
//判断是否审核用
LoginPart.GetAuditFlag = function() {
	//包区分的审核
	if ((mIsAudit == null)) {
		var auditFlag = LocalStorage.getString("Audit");
		if (!auditFlag) {
			mIsAudit = true;
		} else {
			mIsAudit = false;
		}
	}
	return mIsAudit;
	//服务器区分的审核
	//local serverData = LoginPart.GetServerDate(mServerID)
	//if serverData == null then
	//    return false
	//end
	//return serverData.Audit == 1
}
//点击登录按钮，好付,魔方没有服务器登录验证
LoginPart.LoginAfterLink = function() {
	if (Game.IsSDK()) {
		var sdkType = LuaSDKManager.GetCurrentSdkType();
		var loginSDKData = LuaSDKManager.GetSDKResultData(sdkType, enSDKOperateType.enOperateType_AG_Login);
		if ((null != loginSDKData)) {
			mAccount = loginSDKData.accountid;
			var sdkData = LuaSDKManager.GetSDKConstData();
			if ((null == sdkData)) {
				return;
			}
			mPassword = sdkData.gamekey;
			mDeviceId = loginSDKData.deviceId;
			mPackageName = loginSDKData.packageName;
			//好付,魔方没有服务器登录验证
			if (((sdkType == enSDKType.enSDKType_HaoFu) || (sdkType == enSDKType.enSDKType_MoreFun))) {
				LoginPart.GameLogin();
			} else {
				SDKPart.Login(loginSDKData);
			}
		} else {
			console.error('LoginPart.LoginAfterLink,获取sdk登录数据失败');
		}
	} else {
		mPackageName = "com.mc.DigitalMon.bai";
		mDeviceId = Util.GetWinMacAddress();
		LoginPart.GameLogin();
	}
}
//游戏登录（走我们游戏的登录）
LoginPart.GameLogin = function() {
	// 构建消息
	let request = new msg_pb.LoginRequest();
	request.Account   = mAccount;
	request.PassWord  = mPassword;
	request.ServerId  = "1009";
	request.DeviceId  = "aaaaa";
	request.ChannelId = 100;
	request.PackageName = "com.kd";
	request.Reconnect = 0;
	//let te = PB.LoginRequest.decode(request.toArrayBuffer());
	SocketClient.callServerFun("OnLogin",request);
}
//重连
LoginPart.ReConnect = function() {
	// 构建消息
	var request = new  msg_pb.LoginRequest();
	request.Account = mAccount;
	request.PassWord = mPassword;
	request.ServerId = tostring(mServerID);
	request.DeviceId = mDeviceId;
	request.ChannelId = LuaSDKManager.GetChannelId();
	request.PackageName = mPackageName;
	ViewManager.showLoading({});
	//调用服务器登录
	SocketClient.callServerFun("OnReConnectRequest", request);
}
// 登录返回
LoginPart.OnLoginReturn = function(buffer) {
	var msg = msg_pb.LoginResponse.decode(buffer);

	//ViewManager.hideLoading();
	// 判断是否登录成功
	if ((msg.Susseed != 1)) {
		return;
	}
	//UIManager.CloseLoginUI();
	// 登录成功
	//LoadingPanel.Show();
	//LoadingPanel.SetReconnectTimer(20, Network.Logout);
	// UIManager.LoadFirstUI(LoginPart.GoToGame, {
	// 		NewPlayer: msg.NewPlayer
	// 	});
	// // 上传推送ID
	// var request = new  msg_pb.SendPushUIDRequest();
	// var pid = JPush.JPushBinding.GetRegistrationId();
	// request.PushUID = pid;
	// SocketClient.callServerFun("OnPushUIDSend", request);
	// 设置推送Tag为serverid
	MessageCenter.sendMessage(MessageID.MsgID_LoginSucceed,msg.NewPlayer);
}
LoginPart.GoToGame = function(params) {
	//设置本次登陆日期
	Game.SetLoginTime();
	//登陆返回原本要调用LoadingPanel.Close()
	//但是由于有时候会穿帮，因此不调用
	//战斗加载完毕会调用
	//新手引导错误会关闭
	var newPlayer = params.NewPlayer;
	if ((newPlayer == 1)) {
		//NewPlayer值为1说明需要创建角色
		//开启新手引导
		NewbiePanel.Show(15);
		Network.SetMissHeartTimes(0);
	} else {
		//NewPlayer值为其他则直接进入游戏
		MainPanel.Show();
		//登录传玩家信息到sdk(新号则在创建成功后调用)
		LuaSDKManager.SendUserInfo(UserInfoType.UserInfoType_Login);
	}
}
//发送请求判断名称是否被使用  被使用则不进入选择宠物界面
LoginPart.CheckNameRequest = function(name) {
	var request = new  msg_pb.CheckPlayerNameRequest();
	request.Name = name;
	//ViewManager.showLoading({});
	SocketClient.callServerFun("OnNewPlayerNameCheckRequest", request);
}
//验证名称返回
LoginPart.CheckNameResponse = function(buffer) {
	var msg = msg_pb.CheckPlayerNameResponse.decode(buffer);

	//ViewManager.hideLoading();
	//判断是否创建成功
	if ((msg.Susseed != CreateRoleResult.Success)) {
		//LoginPart.CreateRoleError(msg.Susseed);
		MessageCenter.sendMessage(MessageID.MsgID_CreatePlayerError,msg.Susseed);
		return;
	}
	//MessageCenter.sendMessage(MessageID.MsgID_CreatePlayerSucceed);
	//CreateRolePanel.CheckNameSuccess();
	MessageCenter.sendMessage(MessageID.MsgID_CheckNameSuccess);
	
};
//请求创建角色
//name 名字
//typ 男1女2
//pet 宠物参数 --旧参数是1，2，3，新参数是0
LoginPart.CreatePlayer = function(name, typ, pet) {
	if (mCreating) {
		return;
	}
	var request = new  msg_pb.CreatePlayerRequest();
	request.Name = name;
	request.PlayerType = typ;
	request.BornPet = (pet || 0);
	request.DeviceId = mDeviceId;
	request.ChannelId = 100;
	//ViewManager.showLoading({});
	SocketClient.callServerFun("OnNewPlayerRequest", request);
	GuanQiaPart.OnRevive();
	PlayerAttrPart.OnLogout();
	mCreating = true;
}
LoginPart.OnCreatePlayerReturn = function(buffer) {
	var msg = msg_pb.CreatePlayerResponse.decode(buffer);

	mCreating = false;
	//ViewManager.hideLoading();
	//判断是否创建成功
	if ((msg.Susseed != CreateRoleResult.Success)) {
		//LoginPart.CreateRoleError(msg.Susseed);
		return;
	}
	//创建角色传玩家信息到sdk
	//LuaSDKManager.SendUserInfo(UserInfoType.UserInfoType_Login);
	//LuaSDKManager.SendUserInfo(UserInfoType.UserInfoType_CreateRole);
	//CreateRolePanel.OnCreateReturn();
	MessageCenter.sendMessage(MessageID.MsgID_CreatePlayerSucceed);
}
LoginPart.RequestServer = function(acct, psd) {
	// 通过Web获取服务器信息
	//html5 103.20.249.168
	//外网  120.24.58.229
	//白服 120.25.100.174  SLG服务器
	//外网小精灵120.25.100.174:8090
	//玩家服112.74.198.164:8090
	//内部测试112.74.198.164:8092
	//ViewManager.showLoading({});
	var url = (((((mURL + acct) + "&userPassword=") + psd) + "&time=") + os.time());
	LuaFramework.WebTool.Instance().Request(url, "LoginPart", "OnServerRespone");
}

//获取服务器的Web请求回调
LoginPart.OnServerRespone = function(param) {
	//通过某种格式获取服务器列表
	var t = json.decode(param);
	if ((t == null)) {
		return;
	}
	//服务器信息
	for (var i = 0; i != t.Server.length; ++i) {
		var s = t.Server[i];
		var serverData = new LoginPart.ServerData(s.id, s.title, s.ip, s.port, s.state, 0, s.statetips, s.audit, s.pushtag);
		mTotalServers[i] = serverData;
	}
	//角色服务器信息
	if (((t.Player != null) && (typeof(t.Player) != 'userdata'))) {
		for (var i = 0; i != t.Player.length; ++i) {
			var p = t.Player[i];
			var s = p.Server;
			var serverData = new LoginPart.ServerData(s.id, s.title, s.ip, s, s.state, 0, s.statetips, s.audit, s.pushtag);
			var playerInfo = new LoginPart.PlayerInfo(p.Name, p.Level, null, p.ServerId);
			var existServerInfo = { };
			existServerInfo.ServerData = serverData;
			existServerInfo.PlayerInfo = playerInfo;
			mCurServerInfos[i] = existServerInfo;
		}
	}
	//公告信息
	if ((t.Notice != null)) {
		mNotice = t.Notice.Content;
	}
	//版本信息
	if ((t.Version != null)) {
		mVersion = t.Version;
	}
	mWebSocial = t.WebSocial;
	//登录调用
	LoginPanel.OnServerInfoReturn();
	ViewManager.hideLoading();
}
LoginPart.CreateRoleError = function(errID) {
	if ((errID == CreateRoleResult.Already)) {
		ToolTipPopPanel.Show(45);
	} else if ((errID == CreateRoleResult.Occupy) ){
		ToolTipPopPanel.Show(45);
	}
}
//收到踢下线通知
LoginPart.OffLineResponse = function(buffer) {
	var msg = msg_pb.KickMessage();

	var typ = msg.Type;
	//根据不同类型弹出不同提示
	if ((typ == OffLineType.Duplicate)) {
		//重复登陆
		Network.ShowReloginTips(25);
	} else if ((typ == OffLineType.Force) ){
		//被踢下线
		Network.ShowReloginTips(26);
	}
	Network.CloseCheckOff();
	Network.CloseConnect();
}
//关闭游戏
LoginPart.CloseGame = function() {
	LuaSDKManager.ExitGame();
	UnityEngine.Application.Quit();
}
//获取角色账号
LoginPart.HuoQuRoleAcct = function() {
	return mAccount;
}
//重新登录游戏
LoginPart.ReLogin = function() {
	Game.LogOut();
}
//登陆错误返回
LoginPart.OnLoginError = function(buffer) {
	var msg = msg_pb.LoginErrorResponse.decode(buffer);

	//ViewManager.hideLoading();
	// if ((msg.Susseed == LoginError.Password)) {
	// 	ToolTipPopPanel.Show(27);
	// } else if ((msg.Susseed == LoginError.Duplicate) ){
	// 	ToolTipPopPanel.Show(24);
	// } else if ((msg.Susseed == LoginError.TryAgain) ){
	// 	ToolTipPopPanel.Show(11024);
	// }
	MessageCenter.sendMessage(MessageID.MsgID_LoginError,msg.Susseed);
	Network.SetNoReconnect();
	Network.CloseConnect();
	//不在主界面的话回到主界面
	// var curPanelData = UIManager.GetCurPanelData();
	// if (((curPanelData != null) && (((curPanelData.Type == PanelType.Panel) && (curPanelData.Enum == PanelEnum.Login))))) {
	// 	return;
	// }
	// Game.LogOut();
};
//日更五分钟后调用重登
LoginPart.OnDailyUpdateReLoginResponse = function(buffer) {
	var msg = msg_pb.ReLoginOnDailyUpdateResponse.decode(buffer);

	LoginPanel.OnDailyUpdateRelogin();
}
LoginPart.RequestServerOnDailyUpdateReLogin = function(acct, psd) {
	// 通过Web获取服务器信息
	//html5 103.20.249.168
	//外网  120.24.58.229
	//白服 120.25.100.174  SLG服务器
	//外网小精灵120.25.100.174:8090
	//玩家服112.74.198.164:8090
	//内部测试112.74.198.164:8092
	ViewManager.showLoading({});
	var url = (((((mURL + acct) + "&userPassword=") + psd) + "&time=") + os.time());
	LuaFramework.WebTool.Instance().Request(url, "LoginPart", "OnDailyUpdateReLoginServerRespone");
}
//获取服务器的Web请求回调
LoginPart.OnDailyUpdateReLoginServerRespone = function(param) {
	//通过某种格式获取服务器列表
	var t = json.decode(param);
	if ((t == null)) {
		return;
	}
	//服务器信息
	for (var i = 0; i != t.Server.length; ++i) {
		var s = t.Server[i];
		var serverData = new LoginPart.ServerData(s.id, s.title, s.ip, s.port, s.state, 0, s.statetips, s.audit, s.pushtag);
		mTotalServers[i] = serverData;
	}
	//玩家服务器信息
	if (((t.Player != null) && (typeof(t.Player) != 'userdata'))) {
		for (var i = 0; i != t.Player.length; ++i) {
			var p = t.Player[i];
			var s = p.Server;
			var serverData = new LoginPart.ServerData(s.id, s.title, s.ip, s, s.state, 0, s.statetips, s.audit, s.pushtag);
			var playerInfo = new LoginPart.PlayerInfo(p.Name, p.Level, null, p.ServerId);
			var existServerInfo = { };
			existServerInfo.ServerData = serverData;
			existServerInfo.PlayerInfo = playerInfo;
			mCurServerInfos[i] = existServerInfo;
		}
	}
	//公告信息
	if ((t.Notice != null)) {
		mNotice = t.Notice.Content;
	}
	//版本信息
	if ((t.Version != null)) {
		mVersion = t.Version;
	}
	//登录调用
	LoginPanel.OnDailyUpdateReLoginServerInfoReturn();
	ViewManager.hideLoading();
}
LoginPart.GetNoticeURL = function() {
	return mNoticeURL;
}
//获取更新公告
LoginPart.GetNotice = function() {
	return LoginPart.ChangeName(mNotice);
}
//获取版本号
LoginPart.GetVersion = function() {
	return mVersion;
}
//苹果根据包名替换公告
LoginPart.ChangeName = function(notice) {
	var loginSDKData = LuaSDKManager.GetSDKResultData(LuaSDKManager.GetCurrentSdkType(), enSDKOperateType.enOperateType_AG_Login);
	if ((null == loginSDKData)) {
		return notice;
	}
	if ((loginSDKData.packageName == 'com.koudaiwujin5')) {
		notice = StringTool.Replace(notice, "究极宠物", "进化吧宝贝");
	} else if ((loginSDKData.packageName == 'com.koudaiwujin4') ){
		notice = StringTool.Replace(notice, "究极宠物", "究极宠物");
	}
	return notice;
}
//-----------------------------------------------------------版本号请求----------------------------
LoginPart.CheckVersionOnResume = function() {
	var url = ((mVersionURL + "time=") + os.time());
	LuaFramework.WebTool.Instance().Request(url, "LoginPart", "OnVersionResponse");
}
LoginPart.OnVersionResponse = function(param) {
	//通过某种格式获取服务器列表
	var t = json.decode(param);
	if ((t == null)) {
		return;
	}
	//版本信息
	if ((t.Version != null)) {
		mVersion = t.Version;
	}
	//判断版本号，如果服务器版本号为空，不进行验证，方便开发使用
	LoginPart.CheckVersion();
}
//判断版本号，如果服务器版本号为空，不进行验证，方便开发使用
LoginPart.CheckVersion = function() {
	if ((2 == 2)) {
		return true;
	}
	if (((StringTool.trim(AppConst.ResVersion) != mVersion) && (mVersion != ''))) {
		TiShiXinXiPanel.Show(11009, null, GOTool.Quit, GOTool.Quit);
		return false;
	}
	return true;
}
//endregion
window.LoginPart = LoginPart;