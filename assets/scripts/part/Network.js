/*
作者（Author）:    jason

描述（Describe）: 
*/
/*jshint esversion: 6 */
import TimerManager from "../tool/TimerManager";
import SocketClient from "../network/SocketClient";

let protomsg = require("msg").msg;
let timeKey = 0;
class Network
{
    //与服务器链接成功
    static OnConnect()
    {
        //Network.heartBeat();
        LoginPart.GameLogin();
        timeKey = TimerManager.addAlwayTimeCallBack(3,Network.heartBeat);

    }
    static SetNoReconnect()
    {

    }
    static CloseConnect()
    {
        TimerManager.removeTimeCallBack(timeKey);
    }
    static heartBeat()
    {
        let request = new protomsg.HeartPkgRequest();
        request.Connect = true;

        SocketClient.callServerFun("OnHeartPkgRequest", request);
    }
    //心跳包回调
    static RecvHeart(buff)
    {
        let rsp = protomsg.HeartPkgResponse.decode(buff);
        console.log(`RecvHeart Connect = ${rsp.Connect}`);
    }
}
window.Network = Network;