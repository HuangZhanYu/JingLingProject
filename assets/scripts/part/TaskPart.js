//region *.lua
//Date
//此文件由[BabeLua]插件自动生成
import BigNumber from '../tool/BigNumber';
import { Tool_CDTime_IsLerpTimerExist, Tool_CDTime_StartLerpTimer, Tool_CDTime_RemoveLerpTimer } from '../tool/Tool_CDTime';
import ViewManager from '../manager/ViewManager';
import { PanelResName } from '../common/PanelResName';
import MessageCenter from '../tool/MessageCenter';
import { MessageID } from '../common/MessageID';
import TimerManager from '../tool/TimerManager';
import SocketClient from '../network/SocketClient';


let task_msg_pb = require('task_msg').msg;

let TaskPart = { };

TaskPart.DailyTaskList = [];
TaskPart.CurrentAutoId = 0;
TaskPart.TaskInfo = [];
//后端下发的剩余任务时间
TaskPart.TaskLessTime = [];
//购买可以获得的金币
TaskPart.BuyGold = "0";

let SaveLessTaskTimerkey = -1;

//登陆返回信息
TaskPart.mIsLoginReturn = false;
const TaskUseType = {
	TaskPart_Success: 0,
	TaskPart_DataWrong: 1,
	TaskPart_GetLimit: 2,
	TaskPart_NotEnough: 3,
	TaskPart_NotEnoughGold: 4,
	TaskPart_NotEnoughDiamond: 5,
	TaskPart_PoinsLimit: 6,
	TaskPart_NotOpenTask: 7,
	//未开启该任务
};
//接受服务器下发数据
TaskPart.OnRecvTaskInfo = function(buffer) {
	let msg = task_msg_pb.TaskPart_TaskList.decode(buffer);

	TaskPart.DailyTaskList = msg.DailyTaskList;
	TaskPart.CurrentAutoId = msg.CurrentAutoId;
	TaskPart.mIsLoginReturn = true;
	//登录有数据调用开始自动
	TaskPart.AutoOnLogin();
}
//接受服务器下发的剩余时间
TaskPart.OnRecvTaskLessTime = function(buffer) {
	let msg = task_msg_pb.TaskPart_TaskLessTime.decode(buffer);

	TaskPart.TaskLessTime = msg.LessTimeList;
}
//接受服务器下发的购买金币
TaskPart.OnRecvBuyGold = function(buffer) {
	let msg = task_msg_pb.TaskPart_TaskCanBuyGold.decode(buffer);

	TaskPart.BuyGold = msg.Gold;
}
//请求购买金币
TaskPart.BuyGoldRequest = function() {
	let request = new  task_msg_pb.TaskPart_BuyGoldRequest();
	ViewManager.showLoading({});
	SocketClient.callServerFun('TaskPart_BuyGold', request);
}
//请求购买金币回应
TaskPart.BuyGoldResponse = function(buffer) {
	let msg = task_msg_pb.TaskPart_BuyGoldResponse.decode(buffer);

	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		return;
	}
	HuoDeWuPinTiShiPanelData.Show([
			ResourceTypeEnum.ResType_HuoBi
		], [
			HuoBiTypeEnum.HuoBiType_JinBi
		], [
			BigNumber.getValue(TaskPart.BuyGold)
		]);
}
//通过等级返回key
TaskPart.LevelToKey = function() {
	let level = 10000;
	let Conf_RiChangBaoXiang = LoadConfig.getConfig(ConfigName.RiChangBaoXiang);
	for (let k in Conf_RiChangBaoXiang) {
		if ((PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_DengJi] < k)) {
			if ((k < level)) {
				level = k;
			}
		}
		
	}
	return level;
}
//请求增加金币
TaskPart.AddGoldRequest = function(count, id) {
	//如果当前任务>1  < max - 5 不请求
	let maxTaskID = -1;
	for (let i = TaskPart.DailyTaskList.length -1; i >= 0; --i) {
		if (TaskPart.DailyTaskList[i].TaskLevel >= 1){
			maxTaskID = i;
			break;
		}
	}
	if (((id > 1) && (id <= ((maxTaskID - 3))))) {
		return;
	}
	let request = new  task_msg_pb.TaskPart_AddGoldRequest();
	request.TaskId = id;
	request.Count = count;
	SocketClient.callServerFun('TaskPart_AddGold', request);
}
//增加金币回应
TaskPart.AddGoldResponse = function(buffer) {
	let msg = task_msg_pb.TaskPart_AddGoldResponse.decode(buffer);

	if ((msg.Flag != 0)) {
		//console.error("增加失败")
		return;
	}


	MessageCenter.sendMessage(MessageID.MsgID_AddGoldResponse);
	// //检测红点
	// MainPanel.SetTaskRedPoint();
	// //刷新界面
	// TaskPopPanel.LevelUpResponse();
	// //刷新卡牌界面
	// LineupPopPanel.OnRestore();
}
//请求提升任务等级
TaskPart.UpgradeTaskLvRequest = function(count, id) {
	let request = new  task_msg_pb.TaskPart_UpgradeTaskLvRequest();
	request.TaskId = id;
	request.Count = count;
	SocketClient.callServerFun('TaskPart_UpgradeTaskLv', request);
}
//请求提升任务等级回应
TaskPart.UpgradeTaskLvResponse = function(buffer) {
	let msg = task_msg_pb.TaskPart_UpgradeTaskLvResponse.decode(buffer);

	if ((msg.Flag != 0)) {
		//console.error("任务升级失败"..msg.Flag)
		return;
	}
	//校验
	for (let i = 0; i != TaskPart.DailyTaskList.length; ++i) {
		if ((TaskPart.DailyTaskList[i].TaskId == msg.TaskId)) {
			TaskPart.DailyTaskList[i] = msg.TaskData;
		}
	}

	let TaskPopPanel = ViewManager.getPanel(PanelResName.TaskPopPanel);
	if(TaskPopPanel)
	{
		TaskPopPanel.LevelUpResponse();
	}
	// XiaoMuBiaoPanel.SetInfo();
}
//请求开启自动任务
TaskPart.AutoTaskRequest = function(goldType) {
	let request = new  task_msg_pb.TaskPart_AutoTaskRequest();
	request.Type = goldType;
	ViewManager.showLoading({});
	SocketClient.callServerFun('TaskPart_OpenAutoTask', request);
}
//开启自动回应
TaskPart.AutoTaskResponse = function(buffer) {
	let msg = task_msg_pb.TaskPart_AutoTaskResponse.decode(buffer);

	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		//console.error("增加失败"..msg.Flag)
		TaskPart.ShowErrorTips(msg.Flag);
		return;
	}
	//成功更改当前ID
	TaskPart.CurrentAutoId = msg.CurrentAutoID;
	//刷新界面
	let TaskPopPanel = ViewManager.getPanel(PanelResName.TaskPopPanel);
	if(TaskPopPanel)
	{
		TaskPopPanel.OnAutoClearTaskSuccess();
	}
}
//通过ID获取任务数据
TaskPart.GetTaskInfoByID = function(taskId) {
	if ((TaskPart.DailyTaskList.length == 0)) {
		console.error("没有任务数据");
		return null;
	}
	for (let i = 0; i != TaskPart.DailyTaskList.length; ++i) {
		if ((taskId == TaskPart.DailyTaskList[i].TaskId)) {
			return TaskPart.DailyTaskList[i];
		}
	}
}
//错误提示
TaskPart.ShowErrorTips = function(errorFlag) {
	if ((errorFlag == TaskUseType.TaskPart_NotOpenTask)) {
		ToolTipPopPanel.Show(12);
	}
}
//根据索引获取当前任务对象
TaskPart.GetTaskInfo = function(taskID) {
	return TaskPart.DailyTaskList[taskID];
}
//获取某任务加成前的收益
TaskPart.GetTableReward = function(taskInfo, lvl) {
	if ((lvl < 1)) {
		lvl = 1;
	}
	let levelOneGet = BigNumber.create(taskInfo.InitReward);
	//1级时 获得的  400
	let uplevelGet = BigNumber.create(taskInfo.LevelAddGold);
	//升一级 增加的 200
	let level = BigNumber.create((lvl - 1));
	let finalGet = BigNumber.add(levelOneGet, BigNumber.mul(uplevelGet, level));
	//最终获得= 初始+ （等级-1） * 每级增加获得
	return finalGet;
}
//获取某任务的加成收益  
TaskPart.GetRealReward = function(baseGold) {
	let buff = PlayerAttrPart.GetSummaryAttr(PlayerAttrPart.Addition_Summary_Type.TaskGetAdd);
	buff = BigNumber.add(buff, tostring((PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_TaskGoldBuff] * 100)));
	let base = baseGold;
	let real = BigNumber.mul(base, buff);
	real = BigNumber.div(real, BigNumber.create("10000"));
	return real;
}
//获取某任务的真实开启金币
TaskPart.GetRealOpenGold = function(baseGold) {
	let realGold = "0";
	let buff = PlayerAttrPart.GetSummaryAttr(PlayerAttrPart.Addition_Summary_Type.TaskOpenReduce);
	//表中的所有  50000
	let totalBuff = BigNumber.add(buff, BigNumber.create("10000"));
	realGold = BigNumber.mul(BigNumber.create(baseGold), "10000");
	realGold = BigNumber.div(realGold, totalBuff);
	if (BigNumber.lessThan(realGold, BigNumber.create("1"))) {
		realGold = "0";
	}
	return realGold;
}
//获取某任务加成前的升级消耗
TaskPart.GetTableUpCost = function(taskInfo, lvl) {
	if ((lvl < 1)) {
		return "0";
	}
	let uplevelCost = BigNumber.create(taskInfo.UpCount);
	//升一级消耗 5  任务升级消耗金币=任务等级*5
	let level = BigNumber.create(lvl);
	let finalCost = BigNumber.mul(uplevelCost, level);
	//任务升级消耗金币=任务等级*5
	return finalCost;
}
//获取某任务削减后的升级消耗
TaskPart.GetRealUpCost = function(baseCost) {
	let realCost = "0";
	let buff = PlayerAttrPart.GetSummaryAttr(PlayerAttrPart.Addition_Summary_Type.TaskUpgrateReduce);
	//表中的所有  50000
	let totalBuff = BigNumber.add(buff, BigNumber.create("10000"));
	realCost = BigNumber.mul(BigNumber.create(baseCost), "10000");
	realCost = BigNumber.div(realCost, totalBuff);
	if (BigNumber.lessThan(realCost, BigNumber.create("1"))) {
		realCost = "0";
	}
	return realCost;
}
//获取某任务实际 升级增加的收益
TaskPart.UpRealAddGold = function(baseAdd) {
	let buff = PlayerAttrPart.GetSummaryAttr(PlayerAttrPart.Addition_Summary_Type.TaskGetAdd);
	let base = BigNumber.create(baseAdd);
	let real = BigNumber.mul(base, buff);
	real = BigNumber.div(real, BigNumber.create("10000"));
	return real;
}
//计时器  登录游戏后开启  一直在后台运行  用于增加自动任务给的金币
TaskPart.AutoOnLogin = function() {
	if ((TaskPart.DailyTaskList.length <= 0)) {
		return;
	}
	let Conf_JinBiRenWu = LoadConfig.getConfig(ConfigName.JinBiRenWu);
	//初始化任务的基本信息
	let i = 0;
	for(let key in Conf_JinBiRenWu)
	{
		//存下服务端数据
		let taskInfo = TaskPart.GetTaskInfo(i);
		TaskPart.TaskInfo[i] = { };
		TaskPart.TaskInfo[i].IsCountDown = false;
		TaskPart.TaskInfo[i].LerpFuncName = ("TaskPart.TimeCountDown" + i);
		TaskPart.TaskInfo[i].CurrentLerpTime = 0;
		//如果上次任务进行了一半，则继续进行,否则从头进行
		if (((TaskPart.TaskLessTime[i] != null) && (TaskPart.TaskLessTime[i] > 0))) {
			TaskPart.TaskInfo[i].IsCountDown = true;
			if (!Tool_CDTime_IsLerpTimerExist(TaskPart.TaskInfo[i].LerpFuncName)) {
				Tool_CDTime_StartLerpTimer(this.TestLerpFresh.bind(this), TaskPart.TaskInfo[i].LerpFuncName, TaskPart.TaskLessTime[i], this.EndCDtime.bind(this));
			}
		} else {
			if ((i <= TaskPart.CurrentAutoId)) {
				TaskPart.TaskInfo[i].IsCountDown = true;
				let realTime = taskInfo.BaseTime;
				if (!Tool_CDTime_IsLerpTimerExist(TaskPart.TaskInfo[i].LerpFuncName)) {
					Tool_CDTime_StartLerpTimer(this.TestLerpFresh.bind(this), TaskPart.TaskInfo[i].LerpFuncName, realTime, this.EndCDtime.bind(this));
				}
			}
		}
		++i;
	}
	if(SaveLessTaskTimerkey >= 0)
	{
		TimerManager.removeTimeCallBack(SaveLessTaskTimerkey);
	}
	//timerMgr.RemoveTimerEvent('TaskPart.SaveLessTaskTime');
	//开始缓存任务剩余时间
	//timerMgr.AddTimerEvent('TaskPart.SaveLessTaskTime', 5, TaskPart.SaveLessTaskTime, { }, false);
	SaveLessTaskTimerkey = TimerManager.addTimeCallBack(5,TaskPart.SaveLessTaskTime.bind(this));
}
//开始自动任务
TaskPart.TestLerpFresh = function(seconds, refreshFuncName) {
	let taskPopPanel = ViewManager.getPanel(PanelResName.TaskPopPanel);
	if(!taskPopPanel)
	{
		return;
	}
	for (let i = 0; i != TaskPart.DailyTaskList.length; ++i) {
		if ((refreshFuncName == TaskPart.TaskInfo[i].LerpFuncName)) {
			//这里回调界面的某个任务
			TaskPart.TaskInfo[i].CurrentLerpTime = seconds;
			taskPopPanel.UpdateTimeInfo(seconds, i);
			//AuditTas1stPopPanel.UpdateTimeInfo(seconds, i);
		}
	}
}
//结束
TaskPart.EndCDtime = function(refreshFuncName) {
	let taskPopPanel = ViewManager.getPanel(PanelResName.TaskPopPanel);
	
	for (let i = 0; i != TaskPart.DailyTaskList.length; ++i) {
		if(i >= TaskPart.TaskInfo.length){
			break;
		}
		if ((refreshFuncName == TaskPart.TaskInfo[i].LerpFuncName)) {
			if(taskPopPanel)
			{
				taskPopPanel.UpdateTimeInfoEnd(i);
			}
			
			TaskPart.TaskInfo[i].IsCountDown = false;
			let taskInfo = TaskPart.GetTaskInfo(i);
			//金币增加  判断当前任务等级是否大于等于1
			if ((taskInfo.TaskLevel >= 1)) {
				TaskPart.AddGoldRequest(1, taskInfo.TaskId);
			} else {
				//console.error("重生后增加失败,任务等级:"..taskInfo.TaskLevel.."任务ID"..taskInfo.TaskId)
			}
			Tool_CDTime_RemoveLerpTimer(refreshFuncName);
			//如果开启了自动 则继续
			if ((TaskPart.CurrentAutoId >= taskInfo.TaskId)) {
				TaskPart.TaskInfo[i].IsCountDown = true;
				//计算真是倒计时时间  取缩短加成
				let realTime = taskInfo.BaseTime;
				Tool_CDTime_StartLerpTimer(this.TestLerpFresh.bind(this), TaskPart.TaskInfo[i].LerpFuncName, realTime, this.EndCDtime.bind(this));
				
			}
		}
	}
}
//任务时间缩短了，重置当前剩余时间
TaskPart.ResetTaskTime = function(buffer) {
	let msg = task_msg_pb.TaskPart_CurrentRealTime.decode(buffer);

	//移除所有当前的定时，根据当前剩余时间 / 下发的真实时间 得到一个比例 乘以现在的真实时间
	if ((TaskPart.TaskInfo.length == 0)) {
		return;
	}
	for (let i = 0; i != Conf_JinBiRenWu.length; ++i) {
		Tool_CDTime_RemoveLerpTimer(TaskPart.TaskInfo[i].LerpFuncName);
	}
	let percentList = { };
	for (let i = 0; i != Conf_JinBiRenWu.length; ++i) {
		percentList[i] = (TaskPart.TaskInfo[i].CurrentLerpTime / msg.TimeList[i]);
		let taskInfo = TaskPart.GetTaskInfo(i);
		//如果是自动任务，或者还没倒计时结束，则继续倒计时
		if (((i <= TaskPart.CurrentAutoId) || (TaskPart.TaskInfo[i].CurrentLerpTime > 0))) {
			TaskPart.TaskInfo[i].IsCountDown = true;
			let realTime = (taskInfo.BaseTime * percentList[i]);
			Tool_CDTime_StartLerpTimer(this.TestLerpFresh.bind(this), TaskPart.TaskInfo[i].LerpFuncName, realTime, this.EndCDtime.bind(this));
		}
	}
}
//定时存下剩余任务时间
TaskPart.SaveLessTaskTime = function() {
	let request = new task_msg_pb.TaskPart_TaskLessTime();
	let timeList = [];
	for (let i = 0; i != TaskPart.TaskInfo.length; ++i) {
		timeList[i] = TaskPart.TaskInfo[i].CurrentLerpTime;
		request.LessTimeList.push(parseInt(timeList[i]));
	}
	SocketClient.callServerFun('TaskPart_SaveLessTime', request);
	//timerMgr.AddTimerEvent('TaskPart.SaveLessTaskTime', 5, TaskPart.SaveLessTaskTime, { }, false);
	SaveLessTaskTimerkey = TimerManager.addTimeCallBack(5,TaskPart.SaveLessTaskTime.bind(this));
}
//重生成功后 重置时间数据
TaskPart.ResetTimeInfo = function() {
	//移除所有定时器
	for (let i = 0; i != Conf_JinBiRenWu.length; ++i) {
		Tool_CDTime_RemoveLerpTimer(TaskPart.TaskInfo[i].LerpFuncName);
	}
	//重新赋值
	TaskPart.AutoOnLogin();
}
//移除所有定时器
TaskPart.RemoveAllTimer = function() {
	if ((TaskPart.TaskInfo.length == 0)) {
		return;
	}
	//移除所有定时器
	for (let i = 0; i != TaskPart.TaskInfo.length; ++i) {
		Tool_CDTime_RemoveLerpTimer(TaskPart.TaskInfo[i].LerpFuncName);
		TaskPart.TaskInfo[i].IsCountDown = false;
		TaskPart.TaskInfo[i].CurrentLerpTime = 0;
	}
	let TaskPopPanel = ViewManager.getPanel(PanelResName.TaskPopPanel);
	if(TaskPopPanel)
	{
		TaskPopPanel.OnAutoClearTaskSuccess();
	}
}
//获取某任务的真实倒计时时间
TaskPart.GetRealCountDownTime = function(baseTime) {
	let realTime = 0;
	//真实时间 = 基础时间 / (1 + 减少百分比)
	let buff = PlayerAttrPart.GetSummaryAttr(PlayerAttrPart.Addition_Summary_Type.TaskTimeReduce);
	//通过辉哥给的接口  拿到百分比 真实string
	//local buff = "0"--"21500"   --215%       
	let totalBuff = (parseInt(buff) / 10000);
	//所有加成
	realTime = (baseTime / ((1 + totalBuff)));
	//取整   95.4  取  95
	realTime = Math.floor(realTime);
	if ((realTime < 1)) {
		realTime = 1;
	}
	return realTime;
}
//获取当前最大任务等级
//返回等级
TaskPart.GetTaskMaxLevel = function() {
	//当前最大等级
	let maxLevel = 0;
	//拿到最大
	for (let i = 0; i != TaskPart.DailyTaskList.length; ++i) {
		if ((TaskPart.DailyTaskList[i].TaskLevel == 0)) {
			break;
		}
		if ((TaskPart.DailyTaskList[i].TaskLevel > maxLevel)) {
			maxLevel = TaskPart.DailyTaskList[i].TaskLevel;
		}
	}
	return maxLevel;
}
//计算每秒金币
TaskPart.GetPerSecGold = function() {
	let gold = "0";
	//没有自动任务的，任务停止时，就减掉    计算所有当前正在进行的
	//计算已经开启的每个任务的每秒金币，加到一起
	let rewardList = [];
	let timeList = [];
	let index = 0;
	for (let i = 0; i != TaskPart.TaskInfo.length; ++i) {
		if (TaskPart.TaskInfo[i].IsCountDown) {
			let taskInfo = TaskPart.GetTaskInfo(i);
			rewardList[index] = BigNumber.create(taskInfo.OnceRewardReal);
			timeList[index] = taskInfo.BaseTime;
			index = (index + 1);
		}
	}
	
	gold = BigNumber.GetPerSecGold(rewardList, timeList);
	return gold;
}
window.TaskPart = TaskPart;