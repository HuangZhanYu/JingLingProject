let msg_pb = require('msg').msg;
let ViewOtherPlayerPart = { };

//查看别的玩家信息
ViewOtherPlayerPart.ViewOtherPlayer = function(objID) {
	var msg = new  msg_pb.ViewOtherPlayerRequest();
	msg.ObjectID = objID;
	SocketClient.callServerFun("OnViewOtherPlayerRequest", msg);
}
//查看别的玩家信息的返回
ViewOtherPlayerPart.OnViewOtherPlayerResponse = function(buffer) {
	var msg = msg_pb.ViewOtherPlayerResponse.decode(buffer);

	//显示玩家信息
	RoleInfoPopPanel.ShowAfter(msg);
}
//通过玩家账号
ViewOtherPlayerPart.ViewOtherPlayerByAccount = function(account) {
	var msg = new  msg_pb.ViewOtherPlayerByAccountRequest();
	msg.Account = account;
	SocketClient.callServerFun("OnViewOtherPlayerByAccountRequest", msg);
}
//通过玩家账号查看别的玩家信息的返回
ViewOtherPlayerPart.ViewOtherPlayerByAccountResponse = function(buffer) {
	var msg = msg_pb.ViewOtherPlayerResponse.decode(buffer);

	//显示玩家信息
	RoleInfoPopPanel.ShowAfter(msg);
}
window.ViewOtherPlayerPart = ViewOtherPlayerPart;