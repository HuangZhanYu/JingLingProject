let wizardcoll_msg_pb = require('wizardcoll_msg').msg;
let WizardCollPart = { };

WizardCollPart.GotFlag = { };
WizardCollPart.RewardMessage = { };
var mStack = null;
//判断是否领取
WizardCollPart.IsGot = function(id, subid, index) {
	for (var forinvar in ipairs(WizardCollPart.GotFlag)) {
		
		var item = forinvar.item;
		{
			if ((((item.ID == id) && (item.SubID == subid)) && (index <= item.Stats.length))) {
				return item.Stats[index];
			}
		}
	}
	return false;
}
WizardCollPart.Get = function(id, subid, index) {
	for (var forinvar in ipairs(WizardCollPart.GotFlag)) {
		
		var item = forinvar.item;
		{
			if ((((item.ID == id) && (item.SubID == subid)) && (index <= item.Stats.length))) {
				item.Stats[index] = true;
				return;
			}
		}
	}
}
//获取奖励领取信息
WizardCollPart.GetInfo = function() {
	var req = new  wizardcoll_msg_pb.WizardCollPart_InfoRequest();
	SocketClient.callServerFun("WizardCollPart_GetInfo", req);
	ViewManager.showLoading({});
}
//获取信息回调
WizardCollPart.InfoResp = function(buffer) {
	ViewManager.hideLoading();
	var ans = wizardcoll_msg_pb.WizardCollPart_InfoResponse.decode(buffer);
	WizardCollPart.GotFlag = ans.List;
	CollecetPanel.ShowAfterRequest();
}
//领取奖励
WizardCollPart.GetAward = function(id, subid, index) {
	var req = new  wizardcoll_msg_pb.WizardCollPart_AwardRequest();
	req.ID = id;
	req.SubID = subid;
	req.Index = index;
	SocketClient.callServerFun("WizardCollPart_GetAward", req);
	ViewManager.showLoading({});
}
//领取奖励回调
WizardCollPart.GetAwardResp = function(buffer) {
	ViewManager.hideLoading();
	var ans = wizardcoll_msg_pb.WizardCollPart_AwardResponse.decode(buffer);

	if ((ans.Ret == 1)) {
		//已领取
		return;
	} else if ((ans.Ret == 2) ){
		//条件不满足
		return;
	} else if ((ans.Ret != 0) ){
		//表格错误
		return;
	}
	if ((ans.Awards.length > 0)) {
		HuoDeWuPinTiShiPanelData.ShowResources(ans.Awards);
	}
	WizardCollPart.Get(ans.ID, ans.SubID, ans.Index);
	CollecetPanel.RefreshAfterGet();
}
//有新的奖励通知
WizardCollPart.NewAward = function(buffer) {
	var msg = wizardcoll_msg_pb.WizardCollPart_NewAward();

	// console.error("奖励通知"..msg.ID)
	// console.error("奖励通知"..msg.Index)
	if ((mStack == null)) {
		mStack = new MCStack();
	}
	mStack.Push(msg);
	//主界面不存在不弹
	if ((MainPanel.GetObj() == null)) {
		return;
	}
	var lastData = UIManager.GetCurPanelData();
	//签到存在不弹
	if (((lastData.Type == PanelType.PopPanel) && (lastData.Enum == PopPanelEnum.OffLinePop))) {
		return;
	}
	//签到存在不弹
	if (((lastData.Type == PanelType.PopPanel) && (lastData.Enum == PopPanelEnum.DengLuHuoDong))) {
		return;
	}
	//奖励
	if (ReachCollectPopPanel.IsShow()) {
		return;
	}
	var message = mStack.Pop();
	ReachCollectPopPanel.Show(message.ID, message.Index);
}
//继续弹窗
WizardCollPart.CheckOtherReward = function() {
	if ((mStack.Count() <= 0)) {
		return;
	}
	var message = mStack.Pop();
	ReachCollectPopPanel.Show(message.ID, message.Index);
}
window.WizardCollPart = WizardCollPart;