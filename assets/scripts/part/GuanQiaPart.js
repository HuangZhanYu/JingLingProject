/*jshint esversion: 6 */
let guanqia_msg_pb = require('guanqia_msg').msg;

import SocketClient from '../network/SocketClient';
import CanShuTool from '../tool/CanShuTool';
import ViewManager from '../manager/ViewManager';
import { PanelResName } from '../common/PanelResName';


import FightTool from '../logic/fightBehavior/FightTool';
import GFightManager from '../logic/fight/GFightManager';
import { PlayerIntAttr } from './PlayerBasePart';
import Card from '../logic/Card';
import SceneManager from '../manager/SceneManager';
import { FightSystemType, FightCampType } from '../logic/fight/FightDef';
import FightBehaviorManager from '../logic/fightBehavior/FightBehaviorManager';
import BigNumber from '../tool/BigNumber';
import StringTool from '../tool/StringTool';
//主线关卡逻辑
let GuanQiaPart = {};

GuanQiaPart.Def = {
    CircleCoefficient : 1,      //基础属性条件系数
    MyBossCircleXunZhangLevel : 2,    //我军Boss勋章强化等级
    EnemyCircleJinBiLevel : 3,       //敌军金币强化等级
    CircleStartGuanQiaID : 4,   //重复关卡起始ID
    CircleEndGuanQiaID : 5,     //重复关卡结束ID
    MinGuanQiaID : 6,           //转生需要最低挑战关卡数
    ChuanYue    :7,             //追加金币的穿越按钮解锁等级
    DailyMaxBaoXiang :8,        //每天最大获得宝箱数
    GapBaoXiangOdds : 9,        //间隔多少分钟没获得宝箱概率增加
    GetBaoXiangOdds : 10,       //获得宝箱概率增加的万分比
};
//错误
GuanQiaPart.Result = {
    Success : 0,          //成功
    BaoXiangNotExist : 1, //宝箱不存在
};

GuanQiaPart.HistoryTop = 0 ;     //历史最高通关
GuanQiaPart.CurHistoryTop = 0;  //本次复活最高通关

let mCurFloor     = 1;     //当前挑战的关卡id
let mFightID      = "";    //战斗ID
let mRandSeed     = 1;     //随机种子

let mCurBaoXiangID = 0;    //当前的宝箱ID
let mBaoXiangData = {};    //当前拥有宝箱的关卡
let mPassLevelData = {};   //当前会跳关的关卡信息
let mIsReseting = false;   //是否灵魂休息中

let mIsTriggerTimeReward      = false;  //是否触发时间奖励
let mShowTimeRewardIndex      = 0 ;     //显示时间奖励的怪物下标
let mShowTimeRewardCounting   = 0;   //当前已经击杀的数量
let mTimeRewardTypes          = null;        //时间奖励
let mTimeRewardIDs            = null;
let mTimeRewardCounts         = null;

let mIsTriggerPassReward = false;  //是否触发通关奖励
let mShowPassRewardIndex = 0;      //显示通关奖励的怪物下标
let mShowPassRewardCounting = 0;   //当前已经击杀的数量
let mPassRewardTypes = null;        //通关奖励
let mPassRewardIDs = null;
let mPassRewardCounts = null;

let mCurLeveLGuaiWuCount = 0;      //当前关卡怪物数量

let mIsMainInit = false ;          //主界面是否已加载
let mIsLineupInit = false;         //阵容是否初始化
let mIsPlayerAttrInit = false;     //属性库是否初始化
let mIsGuanQiaInit = false;        //关卡部件是否下发信息
let mIsInitFinish = false;        //关卡部件是否下发信息

let mAutoOpenCount = 0;            //自动开启宝箱次数

let mIsStop       = false;           //关卡战斗是否已停止
let mLstIsWin     = null;           //上一场战斗的输赢
let mLstInput     = null;           //上一场战斗的输入
let mLstFloor     = 0;             //上一场战斗的层数
let mChoosePop    = false;        //是否弹出了战斗失败提示

let MAX_RANDOM_BAOXIANG_COUNT = 50;    //每次随机的最大宝箱数量


let mMaxOffLineLevel = 0; //最大离线关卡
let mOffLineSpeed = 0; //离线速度
//////////////////////////////新手任务特殊
let mGuideFightIsWin = 0;		//新手战斗结果
let mGuideFightInput = null;	//新手战斗结果
let mGuideFightOld = false;	//是否是新手战斗
let OfflineReward  = false;
let mBoxAward       = false;

//重置关卡数据
GuanQiaPart.Release = function()
{
	GuanQiaPart.HistoryTop = 0;
    GuanQiaPart.CurHistoryTop = 0;
    mCurFloor = 1; // 当前挑战的关卡id
    mCurBaoXiangID = 0;
    mBaoXiangData = { };
};

GuanQiaPart.Init = function()
{
	if(!mIsMainInit)
        return;
    
    if(!mIsLineupInit)
        return;
    
    if(!mIsPlayerAttrInit)
        return;
    
    if(mIsInitFinish)
        return mIsInitFinish;
    
    mIsInitFinish = true;
	if(mIsStop && LoadingPanel.IsShow())
	{
		SceneManager.ShowScene('CaoCong', function(){
			LoadingPanel.Close();
		});
        return;
	}
        
    
    if(!mIsReseting)
        GuanQiaPart.Start(mCurFloor, mFightID, mRandSeed);
	else
	{
		SceneManager.ShowScene('CaoCong', function(){
			if(LoadingPanel.IsShow())
			{
				FightBehaviorManager.mIsInitGuanQia = false;
				LoadingPanel.Close();
			}
		});
        MainPanel.UpdateCurrentRound(mCurFloor);
	}
};

//重生 登陆 下发
//1，当前关卡
//2，最高关卡
//3，当前最高关卡
GuanQiaPart.OnReviveResponse = function(buffer)
{
	let msg = guanqia_msg_pb.GuanQiaPart_LoginResponse.decode(buffer);

    GuanQiaPart.HistoryTop = msg.HistoryTopLevel; //最高关卡
    GuanQiaPart.CurHistoryTop = msg.CurTopLevel;   //当前最高关卡

    mCurFloor = msg.CurLevel;    //当前关卡
    mFightID    = msg.FightID;       //战斗ID
    mRandSeed    = msg.RandSeed;      //随机种子
    mIsReseting = msg.IsReseting;   //是否灵魂休息中
    mAutoOpenCount = msg.AutoOpenCount;  //自动开启宝箱次数

    mIsGuanQiaInit = true;
    mIsStop = false;
    GuanQiaPart.Init();

    //设置离线奖励标记，用于请求
    OffLinePopPanelData.SetTag(msg.OfflineReward);
};

//重生 登陆 下发
//1，当前关卡
//2，最高关卡
//3，当前最高关卡
GuanQiaPart.OnDataUpdate = function(buffer)
{
	let msg = guanqia_msg_pb.GuanQiaPart_LoginResponse.decode(buffer)

    GuanQiaPart.HistoryTop = msg.HistoryTopLevel; //最高关卡
    GuanQiaPart.CurHistoryTop = msg.CurTopLevel;   //当前最高关卡

    mCurFloor = msg.CurLevel;    //当前关卡
    mFightID    = msg.FightID;       //战斗ID
    mRandSeed    = msg.RandSeed;      //随机种子
    mIsReseting = msg.IsReseting;
    mAutoOpenCount = msg.AutoOpenCount;
}


GuanQiaPart.OnBaoXiangInfoResponse = function(buffer)
{
	let msg = guanqia_msg_pb.GuanQiaPart_BaoXiangInfoResponse.decode(buffer);
    
    mBaoXiangData = {};
	for(let i = 0; i < msg.HasBaoXiangLevel.length; ++i)
	{
		let level = msg.HasBaoXiangLevel[i];
        mBaoXiangData[level] = true;
	}
    mBoxAward = msg.BoxAward;
}
    


//还原关卡战斗
GuanQiaPart.Resume = function()
{
	if(ResetPart.IsReseting)
        return;
    
    if(mIsNeedVerifyMsg)
        return;
    
    //引导暂停时不重启
    if(mIsStop)
        return;
    
    if(mChoosePop)
        return;
    
    GuanQiaPart.Start(mCurFloor, mFightID, mRandSeed)
}
    



//开始某一个关卡，用来恢复关卡战斗
//参数1，guanQiaID 需要开始的关卡ID
GuanQiaPart.Start = function(guanQiaID,fightID, randSeed)
{
	if(mIsStop)
		return;

	let MainPanel = ViewManager.getPanel(PanelResName.MainPanel);
	if(!MainPanel)
	{
		console.error(`GuanQiaPart.Start getPanel(PanelResName.MainPanel) = null`);
		return;
	}
	//刷新主界面关卡ID
	MainPanel.UpdateCurrentRound(guanQiaID);
	//刷新成就
	MainPanel.IsPanDuanChengJiu();
	let [conf_GuanQia_Data, enemyCircleJinBiLevel, myBossCircleJinBiLevel] = GuanQiaPart.GetGuanQiaData(guanQiaID);

	if(conf_GuanQia_Data == null)
	{
		console.error("战斗关卡获取失败:"+guanQiaID);
		return;
	}

	mCurBaoXiangID = conf_GuanQia_Data.BossReward;
	//1是关卡战斗配置，等定下来后，写成枚举
	let conf_ZhanDouPeiZhi_Data = LoadConfig.getConfigData(ConfigName.ZhanDouPeiZhi,1);

	let teamFightLevel = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_TeamTrainingLevel];
	let zhuangBeiFightLevel = TreasurePart.GetFightLevel();
	let jiBanFightLevel = FettersPart.GetFightLevel();
	let guildFightLevel = GuildPart.GetFightLevel();
	let goldFightLevel = LineupPart.GetFightLevel();

	let bonusLevel =  LineupPart.GetBounsLevel();
	let buffLevel = PlayerAttrPart.GetSummaryAttr(PlayerAttrPart.Addition_Summary_Type.BaseLevelAdd);
	let chiHunFightLevel = bonusLevel + buffLevel;

	let myFightLevel = teamFightLevel + chiHunFightLevel + goldFightLevel + zhuangBeiFightLevel + jiBanFightLevel + guildFightLevel;

	let myCards = [];
	//我方士兵
	for(let card of LineupPart.CardList)
	{
		let copyCard = card.CloneCard();
		copyCard.FightLevel = myFightLevel;
		myCards.push(copyCard);
	}
		

	//我军Boss的金币等级//添加循环提升的金币等级
	let myBossShiBingJinBiLevel  =  conf_GuanQia_Data.MyBossXunZhangLevel + myBossCircleJinBiLevel;
	//我方Boss
	let myBossCard = Card.NewByID(FightTool.GetMyBossID(), myBossShiBingJinBiLevel, 0, PlayerAttrPart.Addition_Kind.MyBoss);
	myBossCard.FightLevel = myFightLevel;
	let playerLevel = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_DengJi];
	let topGuanQia = GuanQiaPart.HistoryTop;

	let respawn = PlayerAttrPart.GetSummaryAttr(PlayerAttrPart.Addition_Summary_Type.RespawnRate);
	let par = {};
	par.RespawnProb = parseInt(respawn);
	if(conf_ZhanDouPeiZhi_Data.IsPVP == 1);
		par.RespawnProb = par.RespawnProb / 2;

	//敌军的士兵金币等级//添加循环提升的金币等级
	let enemyShiBingJinBiLevel = conf_GuanQia_Data.XunZhangLevel + enemyCircleJinBiLevel;
	//当前关卡怪物数量
	mCurLeveLGuaiWuCount = 0;
	//获取敌军数据;
	let enemyCards = [];
	for (let i = 0; i < conf_GuanQia_Data.GuaiWuID.length; ++i)
	{
		let guaiWuID = conf_GuanQia_Data.GuaiWuID[i];
		if(guaiWuID > 0)
		{
			// 可能不会配满12个人
			let card = Card.NewByID(guaiWuID, enemyShiBingJinBiLevel, 0, PlayerAttrPart.Addition_Kind.EnemyTeam);
			card.FightLevel = enemyShiBingJinBiLevel;
			card.SetNotRespawn();
			enemyCards.push(card);
			mCurLeveLGuaiWuCount = mCurLeveLGuaiWuCount + 1;
		}
	}

	//获取敌军boss
	//敌军boss的金币等级//添加循环提升的金币等级
	let enemyBossShiBingJinBiLevel = conf_GuanQia_Data.BossXunZhangLevel + enemyCircleJinBiLevel ;
	let enemyBossCard = Card.NewByID(conf_GuanQia_Data.BossID, enemyBossShiBingJinBiLevel, 0, PlayerAttrPart.Addition_Kind.EnemyBoss);
	enemyBossCard.FightLevel = enemyShiBingJinBiLevel;
	let par2 = {};
	par2.RespawnProb = 0;

	//对所有卡牌属性进行约分并返回新的卡
	[myCards, myBossCard, enemyCards, enemyBossCard] = FightTool.ReduceFightAttr(myCards, myBossCard, enemyCards, enemyBossCard);
	let myTeam = GFightManager.CreateFightTeam(guanQiaID.toString(), myBossCard, myCards, playerLevel, topGuanQia, par2);
	let enemyTeam = GFightManager.CreateFightTeam(guanQiaID.toString(), enemyBossCard, enemyCards, 1, 1, par2);
	//获取场景，随机场景
	let scene = SceneManager.getRandomScene();
	fightID = fightID || guanQiaID.toString();
	if(randSeed == null || randSeed == 0)
		randSeed = TimePart.GetUnix();

	GFightManager.Start(scene, FightSystemType.GuanQia, 300*1000, fightID, randSeed, myTeam, enemyTeam, GuanQiaPart.OnFightFinished);
};
    


//战斗结束回调
GuanQiaPart.OnFightFinished = function(isWin, input, buffCards)
{
	mLstIsWin = isWin;
    mLstInput = input;
    mLstFloor = mCurFloor;
    AbilityPart.SetBuffCards(buffCards);
    //输了要打开主界面光效
    //MainPanel.UpdateResetRedPoint(!(isWin == 1))
    //上传战斗数据
    GuanQiaPart.SendFightResult();
	if(isWin == FightCampType.My)
	{
		//获取下一场跳关
		let passLevel = mPassLevelData[mCurFloor];
		passLevel = passLevel || 0;
		if(passLevel > 0)
		{
			//跳关提示
			//let str = TranslateTool.GetText(200372);
			//str = StringTool.ZhuanHuan(str,{passLevel});
			//CommonHUDPanel.Show(str);
		}
			

		mCurFloor = mCurFloor + passLevel + 1;
		//设置历史最高关卡;
		if(GuanQiaPart.HistoryTop < mCurFloor - 1)
			GuanQiaPart.HistoryTop = mCurFloor - 1;

		//设置当前最高关卡
		if(GuanQiaPart.CurHistoryTop < mCurFloor - 1)
			GuanQiaPart.CurHistoryTop = mCurFloor - 1;

		//特殊引导停止战斗判断
		// if(GuideManager.SpecialStopGuanQiaByLevel(GuanQiaPart.HistoryTop))
		// {
		// 	GuanQiaPart.Pause();
		// 	return;
		// }
	}
    
    //直接开启下一场战斗
    GuanQiaPart.Start(mCurFloor);
};

GuanQiaPart.GoRevive = function()
{
	mChoosePop = false;
    RevivePopPanel.Show();
};

GuanQiaPart.GoContinue = function()
{
	mChoosePop = false;
    GuanQiaPart.SendFightResult();
};

GuanQiaPart.SendFightResult = function()
{
	let request = new guanqia_msg_pb.GuanQiaPart_FinishFightRequest();
    request.FrameCount = FightBehaviorManager.mFrameCount;
    request.GuanQiaID = mLstFloor;
    request.FightResult = mLstIsWin;
    mLstInput.BornBuffTab = AbilityPart.BuffCards;
    request.Input = mLstInput.Encode();
    request.FightID = mFightID;
    SocketClient.callServerFun('GuanQiaPart_OnFinishFight',request);
}

GuanQiaPart.SendGuideFightResult = function()
{
	let request = new guanqia_msg_pb.GuanQiaPart_FinishFightRequest();
    request.GuanQiaID = mCurFloor;
    request.FightResult = mGuideFightIsWin;
    request.Input = mGuideFightInput.Encode();
    request.FightID = mFightID;
    SocketClient.callServerFun('GuanQiaPart_OnFinishFight',request);
};

//开启战斗验证
GuanQiaPart.OnFightStart = function(buffer)
{
	let msg = guanqia_msg_pb.GuanQiaPart_FinishFightResponse.decode(buffer);
    mCurFloor   = msg.CurLevel;      //当前关卡
    mFightID    = msg.FightID;       //战斗ID
    mRandSeed   = msg.RandSeed;      //随机种子
    mBoxAward   = msg.BoxAward;

    //还原弹窗标记
    mChoosePop = false;
	if(FightBehaviorManager.IsInOtherFightSystem())
	{	
		return;
	}
};

//获取当前关卡
GuanQiaPart.GetCurFloor = function()
{
	return mCurFloor;
};

//获取当前已通关的关卡
GuanQiaPart.GetPassFloor = function()
{
	return mCurFloor - 1;
};

//领取宝箱
GuanQiaPart.OnGetBaoXiang = function()
{
	let request = new guanqia_msg_pb.GuanQiaPart_GetBaoXiangRequest();
    request.Level = mCurFloor;
    SocketClient.callServerFun('GuanQiaPart_OnGetBaoXiang',request);
};

//领取宝箱返回
GuanQiaPart.OnGetBaoXiangReturn = function(buffer)
{
	let msg = guanqia_msg_pb.GuanQiaPart_GetBaoXiangResponse.decode(buffer);
	if(msg.Success != GuanQiaPart.Result.Success)
	{
		//宝箱领取不成功
        return;
	}
    
    mBoxAward = msg.BoxAward;
	for(let i = 0; i < msg.Level.length; ++i)
	{
		let level = msg.Level[i];
        mBaoXiangData[level] = null;
	} 
    
    MainPanel.SetRewardEffect(msg);
}
    


//自动领取宝箱
GuanQiaPart.OnAutoGetBaoXiang = function()
{
	//本关没有宝箱
    if(!mBoxAward)
        return;
    

    //自动次数
    let autoCount = GuanQiaPart.GetAutoBaoXiangCount();

    //自动次数已用完
    if(mAutoOpenCount >= autoCount)
        return;
    

    let request = new guanqia_msg_pb.GuanQiaPart_GetBaoXiangRequest();
    request.Level = mCurFloor;
    SocketClient.callServerFun('GuanQiaPart_OnAutoGetBaoXiang',request);
}
    


//
GuanQiaPart.OnAutoGetBaoXiangReturn = function(buffer)
{
	let msg = guanqia_msg_pb.GuanQiaPart_AutoOpenCountResponse.decode(buffer);

    mAutoOpenCount = msg.AutoOpenCount;
}	
    


//显示宝箱
GuanQiaPart.ShowBaoXiang = function()
{
	//判断宝箱是否功能开放
    if(!FunctionOpenTool.IsFunctionOpen(FunctionOpenTool.FuntionEnum.MainBoxButton))
        return
    

	if(GuanQiaPart.IsHaveBaoXiang(mCurFloor))
	{	
		MainPanel.SetBaoXiangActive(true)
        GuanQiaPart.OnAutoGetBaoXiang()
	}
	else
	{
		MainPanel.SetBaoXiangActive(false)
	}
}

//判断某一个关卡是否有宝箱
GuanQiaPart.IsHaveBaoXiang = function(floor)
{
	return mBoxAward;
}

// 关闭宝箱
GuanQiaPart.CloseBaoXiang = function()
{
	MainPanel.CloseEffect();
    MainPanel.SetBaoXiangActive(false);
}

//获取当前可以离线挂机到的最高关卡
GuanQiaPart.GetOffLineTopRound = function()
{
	let round = GuanQiaPart.HistoryTop;
    let rate = 0.9;
    let conf_Canshu = CanShuTool.GetCanShuData(CanShuTool.CanShuEnum.OffLineTopRound);
    if(conf_Canshu == null)
        console.error("参数表14为空");
    else
        rate = conf_Canshu.Param[0] / 10000;
    
    round = Math.floor(round * rate);

    return round;
};

//历史最高关卡改变
GuanQiaPart.OnHistoryTopLevelChange = function(buffer)
{
	let msg = guanqia_msg_pb.GuanQiaPart_HistoryTopLevelChangeResponse.decode(buffer);
    GuanQiaPart.HistoryTop = msg.Level;
	//刷新主界面最高关卡
	let MainPanel = ViewManager.getPanel(PanelResName.MainPanel);
	if(!MainPanel)
	{
		console.error(`GuanQiaPart.Start getPanel(PanelResName.MainPanel) = null`);
		return;
	}
    MainPanel.GuanKaPartUpateHistoryTop();
    //刷新小目标
    //XiaoMuBiaoPanel.SetInfo();

    let exp = msg.Exp;
	if(exp > 0)
	{
		let str = tostring(exp);
        MainPanel.CreateExpHudText(GuanQiaPart.HistoryTop,str);
	}
        
}
//本次最高关卡改变
GuanQiaPart.OnCurTopLevelChange = function(buffer)
{
	let msg = guanqia_msg_pb.GuanQiaPart_CurTopLevelChangeResponse.decode(buffer);
    GuanQiaPart.CurHistoryTop = msg.CurTopLevel;
};
    


//获取当前可以重置的里程碑ID 
GuanQiaPart.GetCurrentMilepostID = function()
{
	let curLiChengBeiID = 0;
	for(let v of Conf_GuanQia_LiChengBei)
	{
		if(v.FloorDown >= GuanQiaPart.CurHistoryTop && v.FloorUp <= GuanQiaPart.CurHistoryTop)
		{
			curLiChengBeiID = v.ID;
			break;
		}
	}
    
	if(curLiChengBeiID == 0)
	{
		console.error("里程碑ID异常，：ID 0，当前关卡", GuanQiaPart.CurHistoryTop);
		return 0;
	}

	//里程碑额外奖励，最高不会超出表
	let additionBig = PlayerAttrPart.GetSummaryAttr(PlayerAttrPart.Addition_Summary_Type.TheCreator);
	let addition = parseInt(additionBig);
	curLiChengBeiID = curLiChengBeiID + addition;
	if(curLiChengBeiID > Conf_GuanQia_LiChengBei.length)
		curLiChengBeiID = Conf_GuanQia_LiChengBei.length;
    
	return curLiChengBeiID;
};

//获取历史最高的重置里程碑ID 
GuanQiaPart.GetHistroyTopMilepostID = function()
{
	let curLiChengBeiID = 0
	for(let v of Conf_GuanQia_LiChengBei)
	{
		if(v.FloorDown >= GuanQiaPart.HistoryTop && v.FloorUp <= GuanQiaPart.HistoryTop)
		{
			curLiChengBeiID = v.ID;
			break;
		}
	}
		
        
    
	if(curLiChengBeiID == 0)
	{
		console.error("里程碑ID异常，：ID 0，当前关卡", GuanQiaPart.HistoryTop);
		return 0;
	}

	//里程碑额外奖励，最高不会超出表
	let additionBig = PlayerAttrPart.GetSummaryAttr(PlayerAttrPart.Addition_Summary_Type.TheCreator);
	let addition = parseInt(additionBig);
	curLiChengBeiID = curLiChengBeiID + addition;
	if(curLiChengBeiID > Conf_GuanQia_LiChengBei.length)
		curLiChengBeiID = Conf_GuanQia_LiChengBei.length;
    
	return curLiChengBeiID;
};
    


////////////////////////////////////////时间奖励 通关奖励 离线奖励

//时间奖励触发
GuanQiaPart.OnTimeRewardResponse = function(buffer)
{	
	let msg = guanqia_msg_pb.GuanQiaPart_TriggerResponse.decode(buffer);

    mIsTriggerTimeReward = true;

    mTimeRewardTypes = msg.Types;
    mTimeRewardIDs = msg.IDs;
    mTimeRewardCounts = msg.Counts;

    mShowTimeRewardCounting = 0;
    mShowTimeRewardIndex = Math.random()*(mCurLeveLGuaiWuCount - 1) + 1;
};
    


//判断当前是否有时间奖励
GuanQiaPart.IsTriggerTimeReward = function()
{
	return mIsTriggerTimeReward;
};

//获取当前的时间奖励
GuanQiaPart.GetTimeReward = function()
{
	return [mTimeRewardTypes, mTimeRewardIDs , mTimeRewardCounts];
};

//判断当前怪物时候要显示时间奖励表现
//可能要返回奖励的类型，ID
GuanQiaPart.CheckShowTimeReward = function()
{
	if(!mIsTriggerTimeReward)
        return false;
    

    mShowTimeRewardCounting = mShowTimeRewardCounting + 1;
    if(mShowTimeRewardCounting == mShowTimeRewardIndex)
        return true;
    
    return false;
};

GuanQiaPart.FinishTimeReward = function()
{
	mShowTimeRewardCounting = 0;
    mIsTriggerTimeReward = false;
};

//通关奖励触发
GuanQiaPart.OnPassRewardResponse = function(buffer)
{
	let msg = guanqia_msg_pb.GuanQiaPart_TriggerResponse.decode(buffer);

    mIsTriggerPassReward = true;

    mPassRewardTypes =  msg.Types;
    mPassRewardIDs = msg.IDs;
    mPassRewardCounts = msg.Counts;

    mShowPassRewardCounting = 0;
    mShowPassRewardIndex = Math.random()*(mCurLeveLGuaiWuCount - 1) + 1;

	for(let i = 0; i < mPassRewardTypes.length; ++i)
	{
		let typ = mPassRewardTypes[i];
        let id = mPassRewardIDs[i];
        let count = mPassRewardCounts[i];

		if(typ == 1 && id == 6)
		{
			//let str = TranslateTool.GetText(310);
            let str = StringTool.Replace("经验+【x】",'【x】',BigNumber.getValue(count));
            //CommonHUDPanel.Show(str);
            break;
		}
	}
};
    
//判断当前是否有通关奖励
GuanQiaPart.IsTriggerPassReward = function()
{	
	return mIsTriggerPassReward;
};
    


//获取当前的通关奖励
GuanQiaPart.GetPassReward = function()
{
	return [mPassRewardTypes, mPassRewardIDs , mPassRewardCounts];
};
    


//判断当前怪物时候要显示通关奖励表现
//可能要返回奖励的类型，ID
GuanQiaPart.CheckShowPassReward = function()
{
	if(!mIsTriggerPassReward)
        return false;
    

    mShowPassRewardCounting = mShowPassRewardCounting + 1;
  
    if(mShowPassRewardCounting == mShowPassRewardIndex)
        return true;
    

    return false;
};

GuanQiaPart.FinishPassReward = function()
{
	mShowPassRewardCounting = 0;
    mIsTriggerPassReward = false;
};
    
GuanQiaPart.GetOfflineInfo = function()
{
	let req = new guanqia_msg_pb.GuanQiaPart_OfflineRequest();
	SocketClient.callServerFun("GuanQiaPart_GetOfflineInfo", req);
	
};

//离线奖励返回
GuanQiaPart.OnOffLineReward = function(buffer)
{
	let msg = guanqia_msg_pb.GuanQiaPart_OffLineResponse.decode(buffer);
    //显示界面
    OffLinePopPanelData.SetData(msg);
};

//领取离线奖励
GuanQiaPart.GetOfflineAwards = function()
{
	let req = new guanqia_msg_pb.GuanQiaPart_OfflineAwardRequest();
    SocketClient.callServerFun("GuanQiaPart_GetOfflineAward", req);
};

//领取离线奖励回调
GuanQiaPart.GetOffLineRewardResp = function(buffer)
{
	let msg = guanqia_msg_pb.GuanQiaPart_OfflineAwardResponse.decode(buffer);

    HuoDeWuPinTiShiPanelData.Show(msg.Types, msg.IDs, msg.Counts);
};

//请求离线预览信息
GuanQiaPart.OnGetOffLineYuLanData = function()
{
	let request = new guanqia_msg_pb.GuanQiaPart_OffLineYuLanRequest();
    SocketClient.callServerFun('GuanQiaPart_OnOffLineYuLanRequest',request);
};

//离线预览信息返回
GuanQiaPart.OnGetOffLineYuLanDataResponse = function(buffer)
{
	let msg = guanqia_msg_pb.GuanQiaPart_OffLineYuLanResponse.decode(buffer);
    let maxOffLineLevel = msg.MaxOffLineLevel;
    let speed = msg.Speed;

    OffLineYuLanPopPanel.OnDataResponse(maxOffLineLevel, speed);
};
//获取离线最高关卡
GuanQiaPart.GetMaxOffLineLevel = function()
{
	return mMaxOffLineLevel;
};

//计算离线最高关卡
GuanQiaPart.CalMaxOffLineLevel = function()
{
	//获取离线关卡到最高关卡百分比位置的配置
    let conf_CanShu_Data = CanShuTool.GetCanShuData(CanShuTool.CanShuEnum.OffLineTopRound);
    let rateStr = PlayerAttrPart.GetSummaryAttr(PlayerAttrPart.Addition_Summary_Type.OffLineMaxLevel) ;
    let rate = BigNumber.getIntValue(rateStr);
    let calCount = GuanQiaPart.HistoryTop * conf_CanShu_Data.Param[0] * (10000 + rate) / 10000 / 10000;

    mMaxOffLineLevel = Math.floor(calCount);
};

//获取离线行军速度，单位是秒/关
GuanQiaPart.GetOffLineSpeed = function()
{	
	return mOffLineSpeed;
};

//计算离线行军速度，单位是秒/关
GuanQiaPart.CalOffLineSpeed = function()
{
	let conf_CanShu_Data = CanShuTool.GetCanShuData(CanShuTool.CanShuEnum.OffLineRoundPerMinute);

	let minPerLevel = conf_CanShu_Data.Param[0];
    let secPerLevel = 60 / minPerLevel;
    //加成离线闯关速度
	let rateBig = PlayerAttrPart.GetSummaryAttr(PlayerAttrPart.Addition_Summary_Type.OffLineSpeedAdd);
    let rate = BigNumber.getIntValue(rateBig);
    secPerLevel = secPerLevel * 10000 / (10000 + rate);

    // //加成跳关，测试用，不给玩家看到
    // let addOneRateBig = PlayerAttrPart.GetSummaryAttr(PlayerAttrPart.Addition_Summary_Type.IQPassLevel)
    // let addOneRate = BigNumber.getIntValue(addOneRateBig)
    // let addTwoRateBig = PlayerAttrPart.GetSummaryAttr(PlayerAttrPart.Addition_Summary_Type.RagePassLevel)
    // let addTwoRate = BigNumber.getIntValue(addTwoRateBig)
    // let addition = 1 + (addTwoRate / 10000 * 2 + (1 - addTwoRate / 10000) * addOneRate / 10000 * 1)
    // secPerLevel = secPerLevel / addition

    //转换成 关/分钟
    let levelPerMin = 60 / secPerLevel;

    //保留一位小数
    mOffLineSpeed = levelPerMin.toFixed(1);
};
////////////////////////////////////////暂停主线
//暂停主线战斗
GuanQiaPart.Pause = function()
{
	mIsStop = true;
};

GuanQiaPart.CheckPauseTimer = function()
{
	if(!mIsStop)
        return;
    
    //开启下一次检查
    //timerMgr:AddTimerEvent('GuanQiaPart.CheckPauseTimer', 0.5, GuanQiaPart.CheckPauseTimer, {}, false)
    if(GFightManager.IsPause)
        return;
    
    GFightManager.Pause();
};
    
//开启主线
GuanQiaPart.Continue = function()
{
	mIsStop = false;
    // if(FightBehaviorManager.mIsFighting)
    //     GFightManager.Continue()
    // else
    //    GuanQiaPart.Start(mCurFloor, mFightID, mRandSeed)
    // 
    GuanQiaPart.Start(mCurFloor, mFightID, mRandSeed);
};

////////////////////////////////////////登陆相关
GuanQiaPart.SetMainInitFlag = function(flag)
{
	mIsMainInit = flag;
};

GuanQiaPart.SetLineupInitFlag = function(flag)
{
	mIsLineupInit = flag;
};

GuanQiaPart.SetPlayerAttrInitFlag = function(flag)
{
	mIsPlayerAttrInit = flag;
};


GuanQiaPart.OnRevive = function()
{
	 //mIsLineupInit = false;         //阵容是否初始化
	 mIsGuanQiaInit = false;        //关卡是否初始化
	 //mIsPlayerAttrInit = false;    //玩家属性是否下发
	 mIsInitFinish = false;
};

GuanQiaPart.OnLogout = function()
{
	mIsMainInit = false;           //主界面是否已加载
    mIsLineupInit = false;         //阵容是否初始化
    mIsPlayerAttrInit = false ;    //玩家属性是否下发
    mIsGuanQiaInit = false;        //关卡是否初始化
    mIsInitFinish = false;
};

//获取某一关level重置后的关卡数
// level 当前的关卡
GuanQiaPart.GetReviveLevel = function(level)
{
	//重置关卡百分比
    let revivePercentStr = PlayerAttrPart.GetSummaryAttr(PlayerAttrPart.Addition_Summary_Type.ReviveLevel);
    let revivePer = BigNumber.getIntValue(revivePercentStr);
    let newLevel = level * revivePer / 10000;

    //添加2203 毁灭之神 积蓄毁灭之力，在重置时释放，直接摧毁x个【可叠加，但是最多不可超过历史最高关卡，背包可生效】主线关卡。
    let addLevelBig = PlayerAttrPart.GetSummaryAttr(PlayerAttrPart.Addition_Summary_Type.LordShiva);
    let addLevel = BigNumber.getIntValue(addLevelBig);

    newLevel = newLevel + addLevel;
    let maxLevel = GuanQiaPart.CurHistoryTop * 8000 / 10000;

    if(newLevel > maxLevel)
        newLevel = maxLevel;
    

    //只保留整数部分
    return Math.floor(newLevel);
};

//获取在当前关卡的重置到达的关卡
GuanQiaPart.GetCurReviveLevel = function()
{
	return GuanQiaPart.GetReviveLevel(GuanQiaPart.CurHistoryTop);
};

//获取重置后自动开启的宝箱数量
GuanQiaPart.GetAutoBaoXiangCount = function()
{
	let autoCountBig = PlayerAttrPart.GetSummaryAttr(PlayerAttrPart.Addition_Summary_Type.GuanQiaBaoXiang) ;
    let autoCount = BigNumber.getIntValue(autoCountBig);
    return autoCount;
};

//获取跳关下标
GuanQiaPart.OnPassLevelDataResponse = function(buffer)
{
	let msg = guanqia_msg_pb.GuanQiaPart_PassLevelResponse.decode(buffer);

    mPassLevelData = {};

	for(let i = 0; i < msg.Level.length; ++i)
	{
		let level = msg.Level[i];
        let pass  = msg.Pass[i];
        mPassLevelData[level] = pass;
	}
};
//获取关卡是否停止
GuanQiaPart.IsFightStop = function()
{
	return mIsStop;
};

//获取关卡信息
//返回1，关卡数据
//返回2，当前关卡的我方boss加成值
//返回3，当前关卡的敌方boss和士兵加成值
GuanQiaPart.GetGuanQiaData = function(guanQiaID)
{
	let conf_GuanQia_Data = LoadConfig.getConfigData(ConfigName.GuanQia,guanQiaID);
    let enemyCircleJinBiLevel = 0;
    let myBossCircleJinBiLevel = 0;

	if(conf_GuanQia_Data == null )
	{
		//如果关卡数据为空，寻找循环
		let conf_Def_Data1 = LoadConfig.getConfigData(ConfigName.GuanQia_PeiZhi,GuanQiaPart.Def.CircleEndGuanQiaID);
        if(conf_Def_Data1 == null)
            return;
        
		let endQuanQiaID = parseInt(conf_Def_Data1.Value);
		let conf_Def_Data2 = LoadConfig.getConfigData(ConfigName.GuanQia_PeiZhi,GuanQiaPart.Def.CircleStartGuanQiaID);
        if(conf_Def_Data2 == null)
            return;
        
        let startQuanQiaID = parseInt(conf_Def_Data2.Value);

        let gap = endQuanQiaID - startQuanQiaID + 1;
        let dis = guanQiaID - startQuanQiaID;

        //获取当前循环倍数
        let mul = Math.floor(dis / gap);

        let max = 500;
        let counting = 0;
        //获取需要循环的ID;
        let realGuanQia = guanQiaID - gap;
		
		conf_GuanQia_Data = LoadConfig.getConfigData(ConfigName.GuanQia,realGuanQia);
		while(conf_GuanQia_Data == null)
		{
			realGuanQia = realGuanQia - gap;
            conf_GuanQia_Data = LoadConfig.getConfigData(ConfigName.GuanQia,realGuanQia);

            counting = counting + 1;
			if(counting > max)
			{
				console.error("循环计算错误guanQiaID:"+guanQiaID+"|realGuanQia:"+realGuanQia);
                return;
			}
		}

        let conf_Def_Data3 = LoadConfig.getConfigData(ConfigName.GuanQia_PeiZhi,GuanQiaPart.Def.MyBossCircleXunZhangLevel);
        let conf_Def_Data4 = LoadConfig.getConfigData(ConfigName.GuanQia_PeiZhi,GuanQiaPart.Def.EnemyCircleJinBiLevel);

		if(conf_Def_Data3 == null || conf_Def_Data4 == null)
		{
			console.error("数据缺失>>>"+guanQiaID);
            return;
		}

        myBossCircleJinBiLevel = mul * conf_Def_Data3.Value;
        enemyCircleJinBiLevel = mul * conf_Def_Data4.Value;
	}	

    return [conf_GuanQia_Data, myBossCircleJinBiLevel, enemyCircleJinBiLevel];
};

window.GuanQiaPart = GuanQiaPart;