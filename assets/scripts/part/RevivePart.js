//重生部件
let revive_msg_pb = require('revive_msg').msg;
import CanShuTool from '../tool/CanShuTool';
import ViewManager from '../manager/ViewManager';

let RevivePart = { };

RevivePart.FirstPassReward = { };
//首通奖励状态
RevivePart.PassReward = { };
//关卡奖励状态
RevivePart.AutoReviveFlag = false;
RevivePart.Message = {
	Success: 0,
	//成功
	NotEnoughGold: 1,
	//金币不足
	DataWrong: 2,
	//数据错误
	NotEnoughMedal: 3,
	//勋章不足
	NotEnoughStone: 4,
	//进化石不足
	NotEnoughRongYuBi: 5,
	//荣誉币不足
	NotEnoughCard: 6,
	//进化卡牌不足
	MedalMax: 7,
	//达到当前主角等级勋章等级最大
	NotEnoughDiamond: 8,
	//钻石不足
	FastReviveCostError: 9,
	//快速重置消耗品不足
	MaxTime: 10,
	//超过最大次数

};
//获取重置信息
RevivePart.GetInfo = function() {
	var request = new  revive_msg_pb.RevivePart_InfoRequest();
	SocketClient.callServerFun('RevivePart_GetInfo', request);
}
//是否拥有一键领取的资格
RevivePart.IsOwnOnekeyAccess = function() {
	return FunctionOpenTool.IsFunctionOpen(FunctionOpenTool.FuntionEnum.ReviveOneKeyGuanQia);
}
//获取离线奖励开启重置次数
RevivePart.GetOfflineAwardsNeedResetNum = function() {
	return 2;
}
RevivePart.GetReviveMedal = function(sid) {
	var typeList = { };
	var id = { };
	var count = { };
	var conf = LoadConfig.getConfigData(ConfigName.GuanQia_LiChengBei,sid);
	if ((null == conf)) {
		return typeList, id, count;
	}
	for (var i = 0; i != conf.ResourceType.length; ++i) {
		if (((conf.ResourceType[i] == ResourceTypeEnum.ResType_HuoBi) && (conf.ResourceID[i] == HuoBiTypeEnum.HuoBiType_WenZhang))) {
			table.insert(typeList, conf.ResourceType[i]);
			table.insert(id, conf.ResourceID[i]);
			table.insert(count, conf.ResouceCount[i]);
		}
	}
	return [typeList, id, count];
}
RevivePart.GetReviveReward = function(sid) {
	var typeList = { };
	var id = { };
	var count = { };
	for (var i = 0; i != sid; ++i) {
		var conf = LoadConfig.getConfigData(ConfigName.GuanQia_LiChengBei,i);
		if ((null != conf) && (!this.PassReward[i])) {
			if (!this.FirstPassReward[i]) {
				if ((conf.FirstRewardType != 0) && (conf.FirstRewardID != 0)) {
					table.insert(typeList, conf.FirstRewardType);
					table.insert(id, conf.FirstRewardID);
					table.insert(count, conf.FirstRewardCount);
				}
			}
			for (let n = 0; n != conf.RewardType.length; ++n) {
				if ((conf.RewardType[n] != 0) && (conf.RewardID[n] != 0)) {
					var exist = false;
					for (let m = 0; m != typeList.length; ++m) {
						if (((conf.RewardType[n] == typeList[m]) && (conf.RewardID[n] == id[m]))) {
							exist = true;
							count[m] = BigNumber.add(count[m], conf.RewardCount[n]);
							break;
						}
					}
					if (!exist) {
						table.insert(typeList, conf.RewardType[n]);
						table.insert(id, conf.RewardID[n]);
						table.insert(count, conf.RewardCount[n]);
					}
				}
			}
		}
	}
	return typeList, id, count;
}
//复活请求
RevivePart.ReviveRequest = function(revivetype) {
	var request = new  revive_msg_pb.RevivePart_ReviveRequest();
	request.ReviveType = revivetype;
	ViewManager.showLoading({});
	GuanQiaPart.OnRevive();
	SocketClient.callServerFun('RevivePart_Revive', request);
}
//复活回应
RevivePart.ReviveResponse = function(buffer) {
	var msg = revive_msg_pb.RevivePart_ReviveResponse.decode(buffer);

	ViewManager.hideLoading();
	if ((msg.Flag != RevivePart.Message.Success)) {
		RevivePart.ShowErrorTips(msg.Flag);
		return;
	}
	//刷新界面
	//重置任务计时器
	if ((CanShuTool.GetCanShuData(CanShuTool.CanShuEnum.ReviveParams).Param[0] == 0)) {
		TaskPart.RemoveAllTimer();
	}
	//重置界面刷新
	RevivePopPanel.OnThroughSuccess();
	//小目标刷新
	XiaoMuBiaoPanel.SetInfo();
	//更新主界面相关
	MainPanel.AfterReset();
	//弹出获得奖励提示框
	if ((msg.Types.length > 0)) {
		HuoDeWuPinTiShiPanelData.Show(msg.Types, msg.IDs, msg.Counts);
	}
	//新手结束，传入统计时间
	if ((PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_RevivalNumber] == 1)) {
		LuaSDKManager.CountEvent({
				countEventType: EventType.NewbieFinish,
				value: ""
			});
	}
}
RevivePart.OnUpdateFirstFlagResponse = function(buffer) {
	var msg = revive_msg_pb.RevivePart_LoginResponse.decode(buffer);

	RevivePart.AutoReviveFlag = msg.AutoReviveFlag;
	RevivePart.FirstPassReward = { };
	RevivePart.PassReward = { };
	for (var i = 0; i != msg.IDs.length; ++i) {
		var id = msg.IDs[i];
		RevivePart.FirstPassReward[id] = true;
	}
	for (var id of msg.GotIDs) {
		RevivePart.PassReward[id] = true;
	}
	//如果重置界面存在，刷新一次
}
//是否领取过首次奖励
RevivePart.IsGet = function(id) {
	return RevivePart.FirstPassReward[id];
}
//是否获取过普通奖励
RevivePart.IsGetGeneral = function(id) {
	return RevivePart.PassReward[id];
}
//自动复活请求
RevivePart.AutoReviveRequest = function(flag) {
	var request = new  revive_msg_pb.RevivePart_AutoReviveRequest();
	request.Flag = flag;
	ViewManager.showLoading({});
	SocketClient.callServerFun('RevivePart_OnAutoReviveRequest', request);
}
//自动复活回应
RevivePart.AutoReviveResponse = function(buffer) {
	ViewManager.hideLoading();
	var msg = revive_msg_pb.RevivePart_AutoReviveResponse.decode(buffer);

	RevivePart.AutoReviveFlag = msg.Flag;
}
//快速重置功能请求
RevivePart.OnFastReviveRequest = function(typeList) {
	var request = new  revive_msg_pb.RevivePart_FastRequest();
	request.ReviveType = typeList;
	ViewManager.showLoading({});
	SocketClient.callServerFun('RevivePart_OnFastReviveRequest', request);
}
//快速重置返回
RevivePart.OnFastReviveResponse = function(buffer) {
	ViewManager.hideLoading();
	var msg = revive_msg_pb.RevivePart_FastResponse.decode(buffer);

	if ((msg.Success != RevivePart.Message.Success)) {
		return;
	}
	//弹出奖励窗口
	var types = msg.Types;
	var ids = msg.IDs;
	var counts = msg.Counts;
	if ((types.length > 0)) {
		HuoDeWuPinTiShiPanelData.Show(types, ids, counts);
	}
	//刷新界面
	FastResetPopPanel.OnReviveSuccess();
}
//请求领取某个里程碑奖励 id = 0表示一键领取
RevivePart.GetAward = function(id) {
	var request = new  revive_msg_pb.RevivePart_GetAwardRequest();
	request.ID = id;
	ViewManager.showLoading({});
	SocketClient.callServerFun('RevivePart_GetAward', request);
}
//
RevivePart.GetAwardResp = function(buffer) {
	ViewManager.hideLoading();
	var msg = revive_msg_pb.RevivePart_GetAwardResponse.decode(buffer);

	if ((msg.Ret == 1)) {
		// 没有奖励可领
		ToolTipPopPanel.Show(11033);
		// 提示没有东西可领
		return;
	} else if ((msg.Ret == 2) ){
		//不能一键领取
		return;
	}
	HuoDeWuPinTiShiPanelData.Show(msg.Types, msg.IDs, msg.Counts);
	RevivePopPanel.UpdateCur();
}
RevivePart.ShowErrorTips = function(errorCode) {
	if ((errorCode == RevivePart.Message.MaxTime)) {
		ToolTipPopPanel.Show(11025);
	}
}
window.RevivePart = RevivePart;