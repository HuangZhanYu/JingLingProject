//文件定义：处理训练竞技场逻辑
//设计要点：
//1、请求竞技场信息
//2、请求竞技场对战记录
//3、请求竞技场对手信息
//4、请求竞技场战斗
//5、竞技场战斗结果返回
//6、请求购买竞技场挑战次数
//负责人：陈林辉
//评分：
//移交记录：
let single_arena_msg_pb = require('single_arena_msg').msg;
import CanShuTool from '../tool/CanShuTool';
import ViewManager from '../manager/ViewManager';


let SingleArenaPart  = { };

//消息码
SingleArenaPart.Result = {
	Success: 0,
	//成功
	RivalChange: 1,
	//对手改变
	FightTimeError: 2,
	//挑战次数不足
	RivalError: 3,
	//对手错误
	CostError: 4,
	//购买金额不足
	FightTimeFullMaxError: 5,
	//挑战次数已满，不可购买
	BuyCountError: 6,
	//购买数量错误

};
SingleArenaPart.mPosition = 0;
//我的排名
SingleArenaPart.mNextFreshTime = 0;
//挑战次数的下次刷新时间
SingleArenaPart.mRivals = { };
//竞技场对手
SingleArenaPart.mPosChanges = { };
//排名改变记录
SingleArenaPart.mDefends = { };
//对战记录
SingleArenaPart.mRank = { };
//竞技场排名榜数据
SingleArenaPart.mFightResult = null;
//战斗结果
SingleArenaPart.mFightRecord = null;
//战斗记录
SingleArenaPart.mCurRewards = null;
//当前可以领取的奖励列表
SingleArenaPart.mNextRewards = null;
//下次领取的奖励列表
SingleArenaPart.State = 0;
//判断排名是否三千名内外的依据，0三千外1三千内
SingleArenaPart.NextRewardRank = 0;
//获得奖励下个阶段奖励所需的排名
SingleArenaPart.AwardSameDay = false;
//领取竞技场排名奖励时是否是当天
SingleArenaPart.RankRewards = null;
//领取奖励后下次要领取的奖励
SingleArenaPart.RankYesterday = 0;
//上次结算的排名
SingleArenaPart.TopPlayers = null;
//点赞排行榜玩家信息列表
//请求竞技场主界面信息
SingleArenaPart.OnArenaMainDataRequest = function() {
	var request = new  single_arena_msg_pb.SingleArenaPart_MainDataRequest();
	SocketClient.callServerFun('SingleArenaPart_OnMainDataRequest', request);
	ViewManager.showLoading({});
}
//竞技场主界面信息返回
SingleArenaPart.OnArenaMainDataResponse = function(buffer) {
	ViewManager.hideLoading();
	var msg = single_arena_msg_pb.SingleArenaPart_MainDataResponse.decode(buffer);

	SingleArenaPart.mPosition = msg.Position;
	//我的排名
	SingleArenaPart.mNextFreshTime = parseInt(msg.NextTime);
	//下次刷新时间
	SingleArenaPart.mRivals = msg.Rivals;
	//竞技场对手数组
	SingleArenaPart.mCurRewards = msg.Rewards;
	//当前可以领取的奖励列表
	SingleArenaPart.mNextRewards = msg.NextRewards;
	//下次领取的奖励列表
	SingleArenaPart.State = msg.Stat;
	//判断排名是否三千名内外的依据，0三千外1三千内
	SingleArenaPart.NextRewardRank = msg.NextRewardRank;
	//获得奖励下个阶段奖励所需的排名
	SingleArenaPart.AwardSameDay = msg.AwardSameDay;
	//领取竞技场排名奖励时是否是当天
	SingleArenaPart.RankRewards = msg.RankRewards;
	//领取奖励后下次要领取的奖励
	SingleArenaPart.RankYesterday = msg.RewardRank;
	//上次结算的排名
	SingleArenaPart.TopPlayers = msg.TopPlayers;
	////竞技场对手信息 SingleArenaPart.mRivals数组的内容
	//message SingleArenaPart_PlayerArenaData{
	//	required string Name 		= 1;	//名称
	//	required string ObjID 		= 2;	//ObjID
	//	required int32 	Level 		= 3;	//等级
	//	required int32 	IconID 		= 4;	//头像ID
	//	required string MaxWenZhang = 5;	//最高纹章
	//	required int32 	Position 	= 6;	//竞技场名次
	//	required int32  XunLianLevel= 7;	//训练等级
	//}
	//打开竞技场主界面
	TrainArenaPanel.Show();
}
//刷新竞技场对手信息，以防有所改动
SingleArenaPart.OnUpdateInfoRequest = function() {
	var request = new  single_arena_msg_pb.SingleArenaPart_MainDataRequest();
	SocketClient.callServerFun('SingleArenaPart_OnPositionInfoUpdateRequest', request);
	ViewManager.showLoading({});
}
//刷新竞技场对手
SingleArenaPart.OnFreshRequest = function() {
	var request = new  single_arena_msg_pb.SingleArenaPart_MainDataRequest();
	SocketClient.callServerFun('SingleArenaPart_OnFreshRequest', request);
	ViewManager.showLoading({});
}
//竞技场主界面信息更新
SingleArenaPart.OnArenaMainDataUpdate = function(buffer) {
	ViewManager.hideLoading();
	var msg = single_arena_msg_pb.SingleArenaPart_MainDataResponse.decode(buffer);

	SingleArenaPart.mPosition = msg.Position;
	//我的排名
	SingleArenaPart.mNextFreshTime = parseInt(msg.NextTime);
	//下次刷新时间
	SingleArenaPart.mRivals = msg.Rivals;
	//竞技场对手
	SingleArenaPart.mCurRewards = msg.Rewards;
	//当前可以领取的奖励列表
	SingleArenaPart.mNextRewards = msg.NextRewards;
	//下次领取的奖励列表
	SingleArenaPart.State = msg.Stat;
	//判断排名是否三千名内外的依据，0三千外1三千内
	SingleArenaPart.NextRewardRank = msg.NextRewardRank;
	//获得奖励下个阶段奖励所需的排名
	SingleArenaPart.AwardSameDay = msg.AwardSameDay;
	//领取竞技场排名奖励时是否是当天
	SingleArenaPart.RankRewards = msg.RankRewards;
	//领取奖励后下次要领取的奖励
	SingleArenaPart.RankYesterday = msg.RewardRank;
	//刷新竞技场主界面
	TrainArenaPanel.UpdataArena();
}
//请求竞技场对战记录信息
SingleArenaPart.OnRecordRequest = function() {
	var request = new  single_arena_msg_pb.SingleArenaPart_RecordRequest();
	SocketClient.callServerFun('SingleArenaPart_RecordRequest', request);
	ViewManager.showLoading({});
}
//竞技场对战记录信息返回
SingleArenaPart.OnRecordResponse = function(buffer) {
	ViewManager.hideLoading();
	var msg = single_arena_msg_pb.SingleArenaPart_RecordResponse.decode(buffer);

	SingleArenaPart.mPosChanges = msg.PosChanges;
	//竞技场排名变化记录数组
	SingleArenaPart.mDefends = msg.Defends;
	//竞技场防守战报记录
	//    SingleArenaPart.mPosChanges数组的内容
	//    //竞技场排名变化记录
	//message SingleArenaPart_PosChangeRecord{
	//	required bool 	Initiative 		= 1; 	//主动挑战还是被动挑战
	//	required bool 	IsWin 			= 2;	//输赢
	//	required int32 	PositionChange 	= 3;	//正上升，负下降
	//	required string	RivalObjID  	= 4;  	//对手唯一ID
	//	required string	RivalName      	= 5;	//对手名称
	//	required int32 	RivalLevel      = 6;  	//对手等级
	//	required int32	RivalIconID   	= 7;   	//对手头像ID
	//	required int64 	RecordTime      = 8; 	//记录时间
	//	required int32 	FightRecordID 	= 9; 	//对战信息ID
	//}
	//SingleArenaPart.mDefends数组的内容
	////竞技场防守战报记录
	//message SingleArenaPart_DefendRecord{
	//	required bool 	Initiative 		= 1; 	//主动挑战还是被动挑战
	//	required bool 	IsWin 			= 2;	//输赢
	//	required string	RivalObjID  	= 3;  	//对手唯一ID
	//	required string	RivalName      	= 4;	//对手名称
	//	required int32 	RivalLevel      = 5;  	//对手等级
	//	required int32	RivalIconID   	= 6;   	//对手头像ID
	//	required int64 	RecordTime      = 7; 	//记录时间
	//	required int32 	FightRecordID 	= 8; 	//对战信息ID
	//}
	//打开对战记录界面
	PKRecordPopPanel.Show(PKRecordPopPanel.EnterEnum.SingleArena);
}
//请求竞技场对手信息
SingleArenaPart.OnGetRivalRequest = function(position, objID) {
	var request = new  single_arena_msg_pb.SingleArenaPart_GetRivalRequest();
	request.Position = position;
	request.ObjID = objID;
	SocketClient.callServerFun('SingleArenaPart_OnGetRivalRequest', request);
	ViewManager.showLoading({});
}
//竞技场对手信息返回
SingleArenaPart.OnGetRivalResponse = function(buffer) {
	ViewManager.hideLoading();
	var msg = single_arena_msg_pb.SingleArenaPart_GetRivalResponse.decode(buffer);

	var err = msg.Success;
	//错误，显示提示后返回
	if ((err != SingleArenaPart.Result.Success)) {
		SingleArenaPart.ShowErrorTips(err);
		return;
	}
	var rival = msg.Rival;
	//该玩家的信息，和主界面返回的一样
	var cardIDs = msg.Cards;
	//卡牌ID数组
	var xunLianLevels = msg.QuangHuaLevel;
	//训练等级数组
	var positon = rival.Position;
	//当前点击的排名
	PKConformPopPanel.Show(rival, cardIDs, xunLianLevels, positon);
}
//请求训练竞技场排名榜
SingleArenaPart.OnPosRandRequest = function() {
	var request = new  single_arena_msg_pb.SingleArenaPart_PosRankRequest();
	SocketClient.callServerFun('SingleArenaPart_OnPosRandRequest', request);
	ViewManager.showLoading({});
}
//训练竞技场排名榜返回
SingleArenaPart.OnPosRandResponse = function(buffer) {
	ViewManager.hideLoading();
	var msg = single_arena_msg_pb.SingleArenaPart_PosRankReSponse();

	SingleArenaPart.mPosition = msg.Position;
	SingleArenaPart.mRank = msg.Datas;
	//该玩家的信息，和主界面返回的一样
	//显示排行榜
	RankPanel.ShowJiangLi();
}
//请求挑战竞技场
SingleArenaPart.OnFightRequest = function(position, objID) {
	//设置战斗标记
	FightBehaviorManager.SetFightingSystem(FightSystemType.Arena);
	ViewManager.showLoading({});
	var request = new  single_arena_msg_pb.SingleArenaPart_FightRequest();
	request.Position = position;
	request.ObjID = objID;
	SocketClient.callServerFun('SingleArenaPart_OnFightRequest', request);
}
//竞技场战斗结算信息以及战斗记录
SingleArenaPart.OnFightResultResponse = function(buffer) {
	var msg = single_arena_msg_pb.SingleArenaPart_FightResultResponse.decode(buffer);

	var err = msg.Success;
	//错误，显示提示后返回
	if ((err != SingleArenaPart.Result.Success)) {
		ViewManager.hideLoading();
		SingleArenaPart.ShowErrorTips(err);
		FightBehaviorManager.SetFightingSystem(null);
		console.error(("竞技场挑战错误:" + tostring(err)));
		return;
	}
	//战斗结果和战斗记录
	SingleArenaPart.mFightResult = msg;
	SingleArenaPart.mFightRecord = new FightRecords();
	SingleArenaPart.mFightRecord.Decode(msg.FightRecord);
	//如果消息是完整的
	if (msg.Done) {
		ViewManager.hideLoading();
		//开启战斗
		var conf_ZhanDouPeiZhi_Data = LoadConfig.getConfigData(ConfigName.ZhanDouPeiZhi,2);
		if ((conf_ZhanDouPeiZhi_Data == null)) {
			return;
		}
		CommonFightPanel.Show(CommonFightPanel.FightType.Arena, function() {
				//开启战斗
				GFightManager.Playback(conf_ZhanDouPeiZhi_Data.Scene, FightSystemType.Arena, SingleArenaPart.mFightRecord, function() {
						TrainArenaResultPopPanel.Show(SingleArenaPart.mFightResult, TrainArenaResultPopPanel.EnterEnum.TrainArena);
					});
			});
	}
}
//战斗记录的剩余部分
SingleArenaPart.OnFightResultFollowResponse = function(buffer) {
	var msg = single_arena_msg_pb.SingleArenaPart_FightResultFollowResponse.decode(buffer);

	SingleArenaPart.mFightRecord.AppendFollow(msg.FightRecord);
	//如果消息是完整的
	if (msg.Done) {
		ViewManager.hideLoading();
		//开启战斗
		var conf_ZhanDouPeiZhi_Data = LoadConfig.getConfigData(ConfigName.ZhanDouPeiZhi,2);
		if ((conf_ZhanDouPeiZhi_Data == null)) {
			return;
		}
		//开启战斗
		GFightManager.Playback(conf_ZhanDouPeiZhi_Data.Scene, FightSystemType.Arena, SingleArenaPart.mFightRecord, function() {
				TrainArenaResultPopPanel.Show(SingleArenaPart.mFightResult, TrainArenaResultPopPanel.EnterEnum.TrainArena);
			});
		CommonFightPanel.Show(CommonFightPanel.FightType.Arena);
	}
}
//竞技场挑战次数购买请求
SingleArenaPart.OnBuyFightTimeRequest = function() {
	var request = new  single_arena_msg_pb.SingleArenaPart_BuyFightTimeRequest();
	SocketClient.callServerFun('SingleArenaPart_OnBuyFightTimeRequest', request);
	ViewManager.showLoading({});
}
//竞技场挑战次数购买返回
SingleArenaPart.OnBuyFightTimeResponse = function(buffer) {
	ViewManager.hideLoading();
	var msg = single_arena_msg_pb.SingleArenaPart_BuyFightTimeResponse.decode(buffer);

	var err = msg.Success;
	//错误，显示提示后返回
	if ((err != SingleArenaPart.Result.Success)) {
		SingleArenaPart.ShowErrorTips(err);
		return;
	}
	//刷新界面
	//TrainArenaPanel.Show();
	TrainArenaPanel.SetJiangLiDaoJu();
}
//获取某名次上的玩家信息
SingleArenaPart.GetRivalData = function(position) {
	return SingleArenaPart.mRivals[position];
}
//根据排名返回排名奖池ID
SingleArenaPart.GetPaiMingJiangChiID = function(pos) {
	var jiangChiID = null;
	for (var i = 0; i != Conf_JingJiChang_PaiMing.length; ++i) {
		var data = LoadConfig.getConfigData(ConfigName.JingJiChang_PaiMing,i);
		if ((data == null)) {
			console.error(("竞技场排名奖励数据为空，id:" + i));
			return;
		}
		if (((pos >= data.Min) && (pos <= data.Max))) {
			return data.JiangChiID;
		}
	}
	return;
}
//获取竞技场奖励发放时间
SingleArenaPart.GetSendRewardTime = function() {
	var conf_CanShu_Data = CanShuTool.GetCanShuData(CanShuTool.CanShuEnum.ArenaRewardTime);
	var hour = conf_CanShu_Data.Param[0];
	var minute = conf_CanShu_Data.Param[1];
	if ((minute >= 10)) {
		return ((hour + ":") + minute);
	} else {
		return ((hour + ":0") + minute);
	}
}
//弹出错误提示
SingleArenaPart.ShowErrorTips = function(err) {
	if ((err == SingleArenaPart.Result.RivalChange)) {
		ToolTipPopPanel.Show(206);
	} else if ((err == SingleArenaPart.Result.FightTimeError) ){
		ToolTipPopPanel.Show(209);
	} else if ((err == SingleArenaPart.Result.RivalError) ){
		ToolTipPopPanel.Show(210);
	} else if ((err == SingleArenaPart.Result.CostError) ){
		ToolTipPopPanel.Show(211);
	} else if ((err == SingleArenaPart.Result.FightTimeFullMaxError) ){
		ToolTipPopPanel.Show(212);
	} else if ((err == SingleArenaPart.Result.BuyCountError) ){
		ToolTipPopPanel.Show(213);
	}
}
//战斗回放
SingleArenaPart.OnFightRecordRequest = function(recordID) {
	ViewManager.showLoading({});
	var request = new  single_arena_msg_pb.SingleArenaPart_FightRecordRequest();
	request.RecordID = recordID;
	SocketClient.callServerFun('SingleArenaPart_OnFightRecordRequest', request);
}
//战斗回放返回
SingleArenaPart.OnFightRecordResponse = function(buffer) {
	var msg = single_arena_msg_pb.SingleArenaPart_FightRecordResponse.decode(buffer);

	var err = msg.Success;
	//错误，显示提示后返回
	if ((err != SingleArenaPart.Result.Success)) {
		ViewManager.hideLoading();
		SingleArenaPart.ShowErrorTips(err);
		return;
	}
	SingleArenaPart.mFightRecord = new FightRecords();
	SingleArenaPart.mFightRecord.Decode(msg.FightRecord);
	if (msg.Done) {
		ViewManager.hideLoading();
		//开启战斗
		var conf_ZhanDouPeiZhi_Data = LoadConfig.getConfigData(ConfigName.ZhanDouPeiZhi,2);
		if ((conf_ZhanDouPeiZhi_Data == null)) {
			return;
		}
		CommonFightPanel.Show(CommonFightPanel.FightType.Arena, function() {
				//开启战斗
				GFightManager.Playback(conf_ZhanDouPeiZhi_Data.Scene, FightSystemType.Arena, SingleArenaPart.mFightRecord, function() {
						//恢复主线战队
						GuanQiaPart.Resume();
						UIManager.Close();
					});
			});
	}
}
//战斗记录的剩余部分
SingleArenaPart.OnFightRecordFollowResponse = function(buffer) {
	ViewManager.hideLoading();
	var msg = single_arena_msg_pb.SingleArenaPart_FightRecordFollowResponse.decode(buffer);

	SingleArenaPart.mFightRecord.AppendFollow(msg.FightRecord);
	//如果消息是完整的
	if (msg.Done) {
		ViewManager.hideLoading();
		//开启战斗
		var conf_ZhanDouPeiZhi_Data = LoadConfig.getConfigData(ConfigName.ZhanDouPeiZhi,2);
		if ((conf_ZhanDouPeiZhi_Data == null)) {
			return;
		}
		//开启战斗
		GFightManager.Playback(conf_ZhanDouPeiZhi_Data.Scene, FightSystemType.Arena, SingleArenaPart.mFightRecord, function() {
				//恢复主线战队
				GuanQiaPart.Resume();
				UIManager.Close();
			});
		CommonFightPanel.Show(CommonFightPanel.FightType.Arena);
	}
}
//请求领取训练场奖励
SingleArenaPart.GetReward = function() {
	var request = new  single_arena_msg_pb.SingleArenaPart_RewardRequest();
	SocketClient.callServerFun('SingleArenaPart_GetReward', request);
	ViewManager.showLoading({});
}
//领取训练场奖励返回
SingleArenaPart.GetRewardResp = function(buffer) {
	ViewManager.hideLoading();
	var msg = single_arena_msg_pb.SingleArenaPart_RewardResponse.decode(buffer);

	if ((msg.Ret != 0)) {
		return;
	}
	var resType = { };
	var resID = { };
	var resCount = { };
	for (var forinvar in ipairs(msg.Rewards)) {
		
		var award = forinvar.award;
		{
			table.insert(resType, award.Type);
			table.insert(resID, award.ID);
			table.insert(resCount, award.Count);
		}
	}
	this.mCurRewards = { };
	HuoDeWuPinTiShiPanelData.Show(resType, resID, resCount);
	//刷新竞技场的奖励盒子按钮显示
	TrainArenaPanel.SetRewardState();
	AwardPreviewPopPanel.Show();
	//刷新显示
}
SingleArenaPart.LikeSomeOne = function(objid) {
	var request = new  single_arena_msg_pb.SingleArenaPart_LikeRequest();
	request.ObjID = objid;
	SocketClient.callServerFun('SingleArenaPart_LikesSomeOne', request);
	ViewManager.showLoading({});
}
SingleArenaPart.LikesSomeOneResp = function(buffer) {
	ViewManager.hideLoading();
	var msg = single_arena_msg_pb.SingleArenaPart_LikeResponse.decode(buffer);

	if ((msg.Ret != 0)) {
		return;
	}
	//刷新点赞数据并飘字
	AwardPreviewPopPanel.UpdateLikeInfo(msg.LikesNum);
}
window.SingleArenaPart = SingleArenaPart;