//文件定义：玩家加速信息部件，存储玩家的加速信息
//设计要点：
//1，登陆时，或者加速信息变动时下发所有加速信息
//2，当前显示的加速信息是所有加速信息中加速倍数最大的 
//3, 游戏进入后台需要调用停止加速倒计时的请求
//4，游戏进入前台需要调用重新计算加速倒计时的请求
//负责人：陈林辉
//评分：
//
//移交记录：
let speedup_msg_pb = require('speedup_msg').msg;
import CanShuTool from '../tool/CanShuTool';
import BigNumber from '../tool/BigNumber';
import GFightManager from '../logic/fight/GFightManager';
import FightBehaviorManager from '../logic/fightBehavior/FightBehaviorManager';
import MessageCenter from '../tool/MessageCenter';
import { MessageID } from '../common/MessageID';

let SpeedUpPart = { };

//加速倍数类型枚举
SpeedUpPart.SpeedUpType = {
	SpeedUpType_1: 1,
	//1倍数
	SpeedUpType_2: 2,
	//2倍数
	SpeedUpType_3: 3,
	//3倍数
	SpeedUpType_4: 4,
	//4倍数
	SpeedUpType_5: 5,
	//5倍数
	SpeedUpType_6: 6,
	//6倍数

};
//加速信息实际加速值
SpeedUpPart.SpeedUpTimeScale = [
	1,
	//1倍数
	2,
	//2倍数
	2.5,
	//3倍数
	3,
	//4倍数
	3.5,
	//5倍数
	4,
	//6倍数

];
SpeedUpPart.Result = {
	Success: 0,
	//成功
	Error: 1,
	//数据不存在
	CostError: 2,
	//钻石不足

};
//所有加速信息
SpeedUpPart.TotalData = { };
//当前正在使用的加速信息，如果为空，说明没有加速
SpeedUpPart.CurData = null;
//当前是否开关速度
SpeedUpPart.OpenFlag = true;
//上次是否开关速度
SpeedUpPart.LstOpenFlag = false;
SpeedUpPart.ControlFlag = false;
//加速部件初始化
//注册游戏进入前台后台的回调
SpeedUpPart.Init = function() {
	//注册游戏进入后台回调
	Game.RegisterPauseFunc("SpeedUpPart.OnAppPause", SpeedUpPart.OnAppPause);
	//注册游戏进入前台回调
	Game.RegisterResumeFunc("SpeedUpPart.OnAppResume", SpeedUpPart.OnAppResume);
}
//游戏进入后台
//调用加速信息停止倒计时请求
SpeedUpPart.OnAppPause = function() {
	var request = new  speedup_msg_pb.SpeedUpPart_PauseRequest();
	SocketClient.callServerFun('SpeedUpPart_OnPauseRequest', request);
}
// 游戏进入前台
//调用加速信息重新设置加速开始时间请求
SpeedUpPart.OnAppResume = function() {
	var request = new  speedup_msg_pb.SpeedUpPart_ResumeRequest();
	SocketClient.callServerFun('SpeedUpPart_OnResumeRequest', request);
}
//购买加速请求
SpeedUpPart.OnBuyRequest = function() {
	var request = new  speedup_msg_pb.SpeedUpPart_BuyRequest();
	SocketClient.callServerFun('SpeedUpPart_OnBuyRequest', request);
}
//购买加速返回
SpeedUpPart.OnBuyResponse = function(buffer) {
	var msg = speedup_msg_pb.SpeedUpPart_BuyResponse.decode(buffer);

	if ((msg.Success != SpeedUpPart.Result.Success)) {
		SpeedUpPart.ShowErrorTips(msg.Success);
	}
	ToolTipPopPanel.Show(269);
}
//定时更新信息
SpeedUpPart.OnTimerUpdate = function(buffer) {
	var msg = speedup_msg_pb.SpeedUpPart_SpeedUpDataUpdate.decode(buffer);

	SpeedUpPart.TotalData = { };
	//重置当前加速信息
	SpeedUpPart.CurData = null;
	var max = SpeedUpPart.SpeedUpType.SpeedUpType_1;
	//遍历所有新下发的加速信息
	for (var i = 0; i != msg.Datas.length; ++i) {
		var data = msg.Datas[i];
		//将加速信息添加到SpeedUpPart.TotalData中
		SpeedUpPart.TotalData[data.Speed] = data;
		if ((data.Speed > max)) {
			max = data.Speed;
		}
	}
	//获取当前的信息
	SpeedUpPart.CurData = SpeedUpPart.TotalData[max];
}
//更新信息
SpeedUpPart.OnDataUpdate = function(buffer) {
	var msg = speedup_msg_pb.SpeedUpPart_SpeedUpDataUpdate.decode(buffer);

	//重置所有加速信息
	SpeedUpPart.TotalData = { };
	//重置当前加速信息
	SpeedUpPart.CurData = null;
	var max = SpeedUpPart.SpeedUpType.SpeedUpType_1;
	//遍历所有新下发的加速信息
	for (var i = 0; i != msg.Datas.length; ++i) {
		var data = msg.Datas[i];
		//将加速信息添加到SpeedUpPart.TotalData中
		SpeedUpPart.TotalData[data.Speed] = data;
		if ((data.Speed > max)) {
			max = data.Speed;
		}
	}
	//获取当前的信息
	SpeedUpPart.CurData = SpeedUpPart.TotalData[max];
	//立刻设置加速
	if ((max == SpeedUpPart.SpeedUpType.SpeedUpType_1)) {
		//加速信息倍速为1，不加速
		SpeedUpPart.Update(false);
	} else {
		//其他倍数，加速
		SpeedUpPart.Update(true);
	}

	MessageCenter.sendMessage(MessageID.MsgID_ShowSpeedUp);
	//修改加速界面
	// MainPanel.ShowSpeedUp();
	// FakeMainPanel.ShowSpeedUp();
	// CommonFightPanel.ShowSpeedUp();
}
SpeedUpPart.Update = function(flag) {
	SpeedUpPart.OpenFlag = flag;
	//默认一倍数
	var curSpeed = SpeedUpPart.SpeedUpTimeScale[SpeedUpPart.SpeedUpType.SpeedUpType_1];
	if ((SpeedUpPart.OpenFlag && (SpeedUpPart.CurData != null))) {
		//加速开，并且有当前的加速信息，设置速度
		curSpeed = SpeedUpPart.SpeedUpTimeScale[SpeedUpPart.CurData.Speed];
	}
	//设置加成，由于每一级加速其实只加速0.5，所以加成值也要乘0.5
	var addition = PlayerAttrPart.GetSummaryAttr(PlayerAttrPart.Addition_Summary_Type.SpeedAddition);
	//最大不超过2
	var addSpeed = (((BigNumber.getIntValue(addition) / 10000)) * 0.5);
	if ((addSpeed > 2)) {
		addSpeed = 2;
	}
	curSpeed = (curSpeed + addSpeed);
	FightBehaviorManager.SetSpeed(curSpeed);
	GFightManager.SetTimeScale(curSpeed);
}
//开启加速
//如果当前没有加速信息或者加速信息已关闭，设置为一倍数
//参数1，flag 开关加速
SpeedUpPart.SetSpeedUpFlag = function(flag) {
	//开启的时候会重新下发加速信息，所以等待SpeedUpPart.OnDataUpdate调用就可以了，不需要手动更新
	if (!flag) {
		//更新加速信息
		SpeedUpPart.Update(flag);
	}
	//停止加速时间，或者开启加速
	SpeedUpPart.OnControlRequest(flag);
}
//请求开启或者关闭加速
SpeedUpPart.OnControlRequest = function(stop) {
	var request = new  speedup_msg_pb.SpeedUpPart_ControlTimeRequest();
	request.Control = stop;
	SocketClient.callServerFun('SpeedUpPart_OnControlTime', request);
}
//获取当前的倍数信息，返回nil，说明当前没有倍数信息
//返回值，可以点出：.StartTime 开始时间，.Duration 持续时间， .Speed 倍数
SpeedUpPart.GetCurSpeedUpData = function() {
	return SpeedUpPart.CurData;
}
//获取其他倍数的信息，返回nil，说明当前没有该倍数信息
//参数1，倍数枚举 SpeedUpPart.SpeedUpType
//返回值，可以点出：.StartTime 开始时间，.Duration 持续时间， .Speed 倍数
SpeedUpPart.GetSpeedUpData = function(typ) {
	return SpeedUpPart.TotalData[typ];
}
//弹出错误提示
SpeedUpPart.ShowErrorTips = function(err) {
	if ((err == SpeedUpPart.Result.Error)) {
		ToolTipPopPanel.Show(270);
	} else if ((err == SpeedUpPart.Result.CostError) ){
		ToolTipPopPanel.Show(271);
	}
}
//某类战斗关闭加速
SpeedUpPart.StopOnFighting = function() {
	if (SpeedUpPart.ControlFlag) {
		return;
	}
	SpeedUpPart.LstOpenFlag = SpeedUpPart.OpenFlag;
	SpeedUpPart.SetSpeedUpFlag(false);
	SpeedUpPart.ControlFlag = true;
}
//某类战斗还原加速
SpeedUpPart.StartAfterFighting = function() {
	if (!SpeedUpPart.ControlFlag) {
		return;
	}
	SpeedUpPart.SetSpeedUpFlag(SpeedUpPart.LstOpenFlag);
	SpeedUpPart.ControlFlag = false;
}
//获取重置后获得的加速信息，按最大的那个来算
//返回值，倍速，时间（秒）
SpeedUpPart.GetReviveSpeed = function() {
	//5倍数时间
	var fiveSecondStr = PlayerAttrPart.GetSummaryAttr(PlayerAttrPart.Addition_Summary_Type.FlashSpeed);
	var fiveSecond = BigNumber.getIntValue(fiveSecondStr);
	if ((fiveSecond > 0)) {
		return 5, fiveSecond;
	}
	//3倍数时间
	var threeSecondStr = PlayerAttrPart.GetSummaryAttr(PlayerAttrPart.Addition_Summary_Type.ElecTreat);
	var threeSecond = BigNumber.getIntValue(threeSecondStr);
	if ((threeSecond > 0)) {
		return 3, threeSecond;
	}
	//默认加速
	var conf_CanShu_Data = CanShuTool.GetCanShuData(CanShuTool.CanShuEnum.ReviveSpeedUp);
	if ((conf_CanShu_Data == null)) {
		return 1, 0;
	}
	return conf_CanShu_Data.Param[0], conf_CanShu_Data.Param[1];
}
//endregion
window.SpeedUpPart = SpeedUpPart;