//文件定义：处理玩家的捉宠提示数据
//设计要点：
//负责人：张洋凡
//评分：
//移交记录：
let CatchTipToolPart = { };

CatchTipToolPart.CurrentEvo = 1;
//当前提醒的进化等级
CatchTipToolPart.CurrentCardID = 0;
// 当前提醒的卡牌ID
CatchTipToolPart.CurrentCostCards = { };
//当前会提醒的卡牌数组
//设置当前的进化等级
CatchTipToolPart.SetCurrentEvo = function(evoLevel) {
	CatchTipToolPart.CurrentEvo = evoLevel;
}
//设置当前的卡牌
CatchTipToolPart.SetCurrentCard = function(cardID) {
	CatchTipToolPart.CurrentCardID = cardID;
}
//保存当前要提醒的所有卡牌
CatchTipToolPart.SetCurrentCostList = function(costList) {
	CatchTipToolPart.CurrentCostCards = { };
	CatchTipToolPart.CurrentCostCards = costList;
}
window.CatchTipToolPart = CatchTipToolPart;