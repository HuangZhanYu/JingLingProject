//文件定义：精灵高原相关数据部件
//负责人：张洋凡
let plateau_msg_pb = require('plateau_msg').msg;
let PlateauPart = { };

PlateauPart.RoundDataList = { };
//当前章节的数据
PlateauPart.UnPassHpLeft = { };
//未通过的关卡剩余血量
PlateauPart.UnPassID = 0;
//未通过的关卡ID
PlateauPart.CardList = { };
//当前阵容
PlateauPart.DeadCardList = { };
//今日死亡的卡牌map
PlateauPart.PassChapterTag = false;
//通章节标记
PlateauPart.WaysDataList = { };
//获取途径数据
var requestChapter = 0;
//请求章节
//请求章节数据
PlateauPart.ChapterDataRequest = function(chapter) {
	var request = new  plateau_msg_pb.PlateauPart_ChapterDataRequest();
	request.ChapterIndex = chapter;
	requestChapter = chapter;
	ViewManager.showLoading({});
	SocketClient.callServerFun('PlateauPart_SendPlateauData', request);
}
//请求数据回应
PlateauPart.ChapterDataResponse = function(buffer) {
	var msg = plateau_msg_pb.PlateauPart_ChapterDataResponse.decode(buffer);

	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		console.error(("下发数据失败，错误ID" + msg.Flag));
		return;
	}
	PlateauPart.RoundDataList = { };
	PlateauPart.RoundDataList = msg.ChapterData;
	//PlateauPart.RoundDataList.RoundList[i].Stars   .IsTodayPass
	//如果是-1 则为打开界面请求
	if (requestChapter == (-1)) {
		PlateauPanel.ShowRequestSuccess();
		return;
	}
	//刷新界面
	PlateauPanel.OnChangeRequestSuccess();
}
//达到下一章单独下发
PlateauPart.OnPassChapter = function(buffer) {
	var msg = plateau_msg_pb.PlateauPart_ChapterDataResponse.decode(buffer);

	if ((msg.Flag != 0)) {
		console.error(("下发数据失败，错误ID" + msg.Flag));
		return;
	}
	PlateauPart.RoundDataList = { };
	PlateauPart.RoundDataList = msg.ChapterData;
	PlateauPart.PassChapterTag = true;
	//更新章节
	PlateauPanel.OnAfterLoad();
}
//章节通关更新数据
PlateauPart.UpdateChapterData = function(buffer) {
	var msg = plateau_msg_pb.PlateauPart_ChapterDataResponse.decode(buffer);

	PlateauPart.RoundDataList = { };
	PlateauPart.RoundDataList = msg.ChapterData;
	//刷新界面
	PlateauPanel.OnOpenNewChapter();
}
//更新关卡数据
PlateauPart.UpdateRoundData = function(buffer) {
	var msg = plateau_msg_pb.PlateauPart_SendRoundData();

	PlateauPart.RoundDataList.RoundList[msg.Round].Stars = msg.Stars;
	PlateauPart.RoundDataList.RoundList[msg.Round].IsTodayPass = msg.IsTodayPass;
}
//接收血量数据
PlateauPart.RecHpDatas = function(buffer) {
	var msg = plateau_msg_pb.PlateauPart_SendUnPassData();

	//更新血量数据
	PlateauPart.UnPassHpLeft = { };
	PlateauPart.UnPassHpLeft = msg.HpList;
	PlateauPart.UnPassID = msg.PlateauID;
}
//获取当前章节的X关数据
PlateauPart.GetRoundData = function(round) {
	var roundData = PlateauPart.RoundDataList.RoundList[round];
	if ((roundData == null)) {
		console.error(("关卡数据异常" + round));
		return null;
	}
	return roundData;
}
//接收当前阵容数据
PlateauPart.RecvLineData = function(buffer) {
	var msg = plateau_msg_pb.PlateauPart_SendLineData();

	PlateauPart.CardList = { };
	PlateauPart.CardList = msg.CardList;
}
//接收今日阵亡卡牌数据
PlateauPart.RecDeadCardData = function(buffer) {
	var msg = plateau_msg_pb.PlateauPart_SendDeadCardData();

	PlateauPart.DeadCardList = { };
	for (var i = 0; i != msg.DeadCardList.length; ++i) {
		PlateauPart.DeadCardList[msg.DeadCardList[i].CardID] = msg.DeadCardList[i].Count;
	}
}
//判断某张卡是否被禁
PlateauPart.IsForbidden = function(zhenRongID, cardID) {
	var conf_ZhenRongInfo = LoadConfig.getConfigData(ConfigName.JingLingGaoYuan_ZhenRong,zhenRongID);
	if ((conf_ZhenRongInfo == null)) {
		console.error(("精灵高原阵容表空，ID" + zhenRongID));
		return true;
	}
	var conf_KaPaiInfo = LoadConfig.getConfigData(ConfigName.ShiBing,cardID);
	if ((conf_KaPaiInfo == null)) {
		console.error(("士兵表空，ID" + cardID));
		return true;
	}
	//先存下会被禁用的属性
	//属性///////////////////////
	var race = conf_KaPaiInfo.Race;
	//品质///////////////////////
	var quality = conf_KaPaiInfo.Quality;
	//品质 白1 绿2 蓝3 紫4 橙5 
	//是否对空
	var isFly = conf_KaPaiInfo.Troop;
	//性别//////////////////////
	var sex = (conf_KaPaiInfo.Category + 1);
	//LUA数组从1 开始  要加一
	//进化等级///////////////////
	var evo = (conf_KaPaiInfo.EVO + 1);
	//LUA数组从1 开始  要加一
	//兵种 空军 1   2和3是陆军
	var troop = conf_KaPaiInfo.Troop;
	//攻击类型 1近战 2远程(ID 从1开始 索引减一)   
	var attckType = conf_KaPaiInfo.AttackType;
	if (((((((race < 1) || (quality < 1)) || (sex < 1)) || (evo < 1)) || (troop < 1)) || (attckType < 1))) {
		console.error("数组索引小于1，判断禁用,战斗塔");
	}
	//先判断属性是否被禁 0不限制 1 限制
	if ((conf_ZhenRongInfo.LimitRace[race] == 1)) {
		return true;
	}
	//再判断品质是否被禁
	if ((conf_ZhenRongInfo.LimitQuality[quality] == 1)) {
		return true;
	}
	//再判断性别是否被禁
	if ((conf_ZhenRongInfo.LimitSex[sex] == 1)) {
		return true;
	}
	//判断进化等级是否被禁
	if ((conf_ZhenRongInfo.LimitEvo[evo] == 1)) {
		return true;
	}
	//判断军种是否被禁
	var troopIndex = troop;
	if ((troopIndex == 3)) {
		troopIndex = 2;
	}
	if ((conf_ZhenRongInfo.LimitTroop[troopIndex] == 1)) {
		return true;
	}
	//判断攻击类型
	if ((conf_ZhenRongInfo.LimitAttackType[attckType] == 1)) {
		return true;
	}
	return false;
}
//获取玩家拥有的所有卡牌列表，包含CardID,Count
PlateauPart.GetPlayerCardList = function() {
	var cardList = { };
	var index = 1;
	//先算背包里的
	for (var i = 0; i != BackPackPart.mCardList.length; ++i) {
		cardList[index] = { };
		cardList[index].CardID = BackPackPart.mCardList[i].CardID;
		cardList[index].Count = BackPackPart.mCardList[i].Count;
		index = (index + 1);
	}
	//再算阵容中的
	for (var i = 0; i != LineupPart.CardList.length; ++i) {
		var isSave = false;
		var saveIndex = -1;
		for (let j = 0; j != cardList.length; ++j) {
			if ((cardList[j].CardID == LineupPart.CardList[i].CardID)) {
				isSave = true;
				saveIndex = j;
				break;
			}
		}
		//已经有则数量加
		if (isSave) {
			cardList[saveIndex].Count = (cardList[saveIndex].Count + 1);
		} else {
			//没有则新建
			cardList[index] = { };
			cardList[index].CardID = LineupPart.CardList[i].CardID;
			cardList[index].Count = 1;
			index = (index + 1);
		}
	}
	//减去今日已经阵亡的卡牌
	for (var i = 0; i != cardList.length; ++i) {
		var count = PlateauPart.DeadCardList[cardList[i].CardID];
		if ((count != null)) {
			cardList[i].Count = (cardList[i].Count - count);
		}
	}
	return cardList;
	//用的时候要用是否禁用筛选一次
}
//上阵请求
PlateauPart.AddLineUpRequest = function(cardID, gaoYuanID) {
	mCardID = cardID;
	//判断拥有这张卡的数量是否足够
	//先拿到拥有多少张
	var playerHave = BackPackPart.GetCardCount(cardID);
	for (var i = 0; i != LineupPart.CardList.length; ++i) {
		if ((LineupPart.CardList[i].CardID == cardID)) {
			playerHave = (playerHave + 1);
		}
	}
	//拿到已经上阵的数量
	var lineCount = 0;
	for (var i = 0; i != PlateauPart.CardList.length; ++i) {
		if ((PlateauPart.CardList[i] == cardID)) {
			lineCount = (lineCount + 1);
		}
	}
	//剩余的总量
	var leftCount = (playerHave - lineCount);
	//再减去今日死亡的卡牌
	if ((PlateauPart.DeadCardList[cardID] != null)) {
		leftCount = (leftCount - PlateauPart.DeadCardList[cardID]);
	}
	//如果小于1则异常
	if ((leftCount < 1)) {
		return;
	}
	//获取当前关卡上阵配置ID
	var conf_GaoYuan = LoadConfig.getConfigData(ConfigName.JingLingGaoYuan,gaoYuanID);
	if ((conf_GaoYuan == null)) {
		console.error(("精灵高原表空，ID" + gaoYuanID));
		return;
	}
	var zhenRongID = conf_GaoYuan.LineID;
	//获取阵容配置
	var conf_ZhenRongPeiZhi = LoadConfig.getConfigData(ConfigName.JingLingGaoYuan_ZhenRong,zhenRongID);
	if ((conf_ZhenRongPeiZhi == null)) {
		console.error(("精灵高原阵容表空，ID" + zhenRongID));
		return;
	}
	//计算上阵索引
	var index = -1;
	for (var i = 0; i != PlateauPart.CardList.length; ++i) {
		if ((PlateauPart.CardList[i] == 0)) {
			index = i;
			break;
		}
	}
	//判断索引是否异常
	if ((index == (-1) || (index > conf_ZhenRongPeiZhi.LineCount))) {
		return;
	}
	//判断卡牌是否符合上阵要求,返回true则禁用，false则不禁用
	if (PlateauPart.IsForbidden(zhenRongID, cardID)) {
		console.error("卡牌禁用");
		return;
	}
	//添加到阵容中
	PlateauPart.CardList[index] = cardID;
	//刷新界面
	TowerChangePopPanel.UpdatePanelInfo();
	PlateauRoundPopPanel.UpdateLine();
}
//下阵请求
PlateauPart.SubLineDownRequest = function(index) {
	//判断索引越界
	if (((index < 1) || (index > 12))) {
		return;
	}
	//判断该索引是否存在卡牌
	var cardID = PlateauPart.CardList[index];
	if ((cardID == 0)) {
		return;
	}
	//下阵卡牌
	PlateauPart.CardList[index] = 0;
	//刷新界面
	TowerChangePopPanel.UpdatePanelInfo();
	PlateauRoundPopPanel.UpdateLine();
}
//请求交换卡牌
PlateauPart.ChangeLineRequest = function(beforeIndex, afterIndex) {
	var id = PlateauPart.CardList[beforeIndex];
	PlateauPart.CardList[beforeIndex] = PlateauPart.CardList[afterIndex];
	PlateauPart.CardList[afterIndex] = id;
	TowerChangePopPanel.UpdatePanelInfo();
	PlateauRoundPopPanel.UpdateLine();
}
//请求验证阵容是否被禁,如果被禁，后端下发更新后的阵容
PlateauPart.CheckLineRequest = function(plateauID) {
	var request = new  plateau_msg_pb.PlateauPart_CheckLineRequest();
	request.PlateauID = plateauID;
	ViewManager.showLoading({});
	SocketClient.callServerFun('PlateauPart_CheckLine', request);
}
//验证回调
PlateauPart.CheckLineResponse = function(buffer) {
	var msg = plateau_msg_pb.PlateauPart_CheckLineResponse.decode(buffer);

	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		console.error(("验证失败,ID" + msg.Flag));
		return;
	}
	PlateauPanel.CheckSuccess();
}
//请求购买次数
PlateauPart.BuyChallengeCountRequest = function(count) {
	var request = new  plateau_msg_pb.PlateauPart_BuyChallengeCountRequest();
	request.Count = count;
	ViewManager.showLoading({});
	SocketClient.callServerFun('PlateauPart_BuyChallengeCount', request);
}
//购买次数回应
PlateauPart.BuyChallengeCountResponse = function(buffer) {
	var msg = plateau_msg_pb.PlateauPart_BuyChallengeCountResponse.decode(buffer);

	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		console.error(("购买次数失败，错误ID" + msg.Flag));
		return;
	}
	//刷新挑战次数和红点
	PlateauPanel.UpdateRedAndCount();
}
//计算某关的BOSS剩余血量百分比
PlateauPart.GetHpLeftPercent = function() {
	var conf_GaoYuan = LoadConfig.getConfigData(ConfigName.JingLingGaoYuan,PlateauPart.UnPassID);
	if ((conf_GaoYuan == null)) {
		console.error(("未通关ID错误" + PlateauPart.UnPassID));
		return 1;
	}
	var zhenRongID = conf_GaoYuan.LineID;
	var conf_ZhenRong = LoadConfig.getConfigData(ConfigName.JingLingGaoYuan_ZhenRong,zhenRongID);
	if ((conf_ZhenRong == null)) {
		console.error(("未通关阵容ID错误" + zhenRongID));
		return 1;
	}
	//加总拿到当前血量
	var currentHp = "0";
	for (var i = 0; i != PlateauPart.UnPassHpLeft.length; ++i) {
		currentHp = BigNumber.add(currentHp, PlateauPart.UnPassHpLeft[i]);
	}
	var totalHp = "0";
	//读表计算表格总血量
	for (var i = 0; i != conf_ZhenRong.MonsterID.length; ++i) {
		if ((conf_ZhenRong.MonsterID[i] == 0)) {
			break;
		}
		var card = Card.NewByID(conf_ZhenRong.MonsterID[i], 1, 0, null);
		var cardHp = card.FightAttr[FightAttr.HP];
		totalHp = BigNumber.add(totalHp, cardHp);
	}
	var bossID = LoadConfig.getConfigData(ConfigName.ZhanDouPeiZhi,14).EnemyBossID;
	var bosscard = Card.NewByID(bossID, 1, 0, null);
	var bosscardHp = bosscard.FightAttr[FightAttr.HP];
	totalHp = BigNumber.add(totalHp, bosscardHp);
	//console.error("currentHp"..currentHp.."totalHp"..totalHp)
	//放大精度的计算
	var result = BigNumber.floatDivision(currentHp, totalHp);
	//保留三位小数
	var fmt = (('%.' + 3) + 'f');
	var nRet = parseInt(string.format(fmt, result));
	return nRet;
}
var swapPlateauID = 0;
//请求扫荡关卡
PlateauPart.SwapRoundRequest = function(chapter, round) {
	var request = new  plateau_msg_pb.PlateauPart_SwapRequest();
	request.ChapterID = chapter;
	request.RoundID = round;
	swapPlateauID = ((((chapter - 1)) * 10) + round);
	ViewManager.showLoading({});
	SocketClient.callServerFun('PlateauPart_SwapFight', request);
}
PlateauPart.SwapRoundResponse = function(buffer) {
	var msg = plateau_msg_pb.PlateauPart_SwapResponse.decode(buffer);

	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		console.error(("扫荡失败，错误ID" + msg.Flag));
		return;
	}
	var conf_GaoYuan = LoadConfig.getConfigData(ConfigName.JingLingGaoYuan,swapPlateauID);
	if ((conf_GaoYuan == null)) {
		console.error(("高原ID异常" + swapPlateauID));
		return;
	}
	var conf_JiangLi = LoadConfig.getConfigData(ConfigName.JiangChi,conf_GaoYuan.RewardID);
	if ((conf_JiangLi == null)) {
		console.error(("奖池异常" + conf_GaoYuan.RewardID));
		return;
	}
	var resourceType = { };
	var resourceID = { };
	var resoureCount = { };
	for (var i = 0; i != 6; ++i) {
		if ((conf_JiangLi.ResourceTypes[i] == 0)) {
			break;
		}
		resourceType[i] = conf_JiangLi.ResourceTypes[i];
		resourceID[i] = conf_JiangLi.ResourceIDs[i];
		resoureCount[i] = conf_JiangLi.MinCounts[i];
	}
	//扫荡结束 弹窗获得奖励  关闭弹窗
	HuoDeWuPinTiShiPanelData.Show(resourceType, resourceID, resoureCount);
	PlateauRoundPopPanel.OnBtnCloseClick();
	//刷新获取途径界面的次数
	HowToGetPopPanel.AfterSwap();
}
//请求获取途径的相关数据
PlateauPart.GetWaysDataRequest = function(tujingID) {
	var request = new  plateau_msg_pb.PlateauPart_GetWaysDataRequest();
	request.ConfID = tujingID;
	ViewManager.showLoading({});
	SocketClient.callServerFun('PlateauPart_GetWaysData', request);
}
//获取途径数据返回
PlateauPart.GetWaysDataResponse = function(buffer) {
	var msg = plateau_msg_pb.PlateauPart_GetWaysDataResponse.decode(buffer);

	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		console.error(("PlateauPart.GetWaysDataResponse失败，错误码" + msg.Flag));
		return;
	}
	PlateauPart.WaysDataList = { };
	for (var i = 0; i != msg.DataList.length; ++i) {
		table.insert(PlateauPart.WaysDataList, msg.DataList[i]);
	}
	for (var i = 0; i != PlateauPart.WaysDataList.length; ++i) {
		var data = PlateauPart.WaysDataList[i];
	}
	//拿到数据打开界面
	HowToGetPopPanel.ShowAfterRequest();
}
//获取某个索引的获取途径数据
PlateauPart.GetWayDataByIndex = function(index) {
	var data = PlateauPart.WaysDataList[index];
	if ((data == null)) {
		console.error(("PlateauPart.GetWayDataByIndex索引数据为空" + index));
		return null;
	}
	return data;
}
//----------------------------------战斗验证---------------------------------------------
var plateauID = 0;
PlateauPart.OnBeginFightVerifyRequest = function(chapterID, roundID) {
	//设置战斗管理器标记
	FightBehaviorManager.SetFightingSystem(FightSystemType.Plateau);
	var request = new  plateau_msg_pb.PlateauPart_OnBeginFightVerifyRequest();
	request.ChapterID = chapterID;
	request.RoundID = roundID;
	for (var i = 0; i != PlateauPart.CardList.length; ++i) {
		request.LineCardList.push(PlateauPart.CardList[i]);
	}
	plateauID = ((((chapterID - 1)) * 10) + roundID);
	ViewManager.showLoading({});
	SocketClient.callServerFun('PlateauPart_OnBeginFightVerifyRequest', request);
}
var Record = null;
var isWin = null;
var finalStar = 0;
//回应挑战(后端下发奖励)
PlateauPart.FightResponse = function(buffer) {
	var msg = plateau_msg_pb.PlateauPart_FightResponse.decode(buffer);

	if ((msg.Flag != 0)) {
		console.error(("挑战失败，错误ID" + msg.Flag));
		FightBehaviorManager.SetFightingSystem(null);
		return;
	}
	isWin = msg.IsWin;
	finalStar = msg.Stars;
	Record = new FightRecords();
	Record.Decode(msg.Content);
	if (msg.Done) {
		PlateauPart.PlayBack();
		ViewManager.hideLoading();
	}
}
PlateauPart.FighResponseAppend = function(buffer) {
	var msg = plateau_msg_pb.PlateauPart_FightAppendResponse.decode(buffer);

	Record.AppendFollow(msg.Content);
	if (msg.Done) {
		PlateauPart.PlayBack();
		ViewManager.hideLoading();
	}
}
//回放
PlateauPart.PlayBack = function() {
	//开启战斗
	var conf_ZhanDouPeiZhi_Data = LoadConfig.getConfigData(ConfigName.ZhanDouPeiZhi,14);
	if ((conf_ZhanDouPeiZhi_Data == null)) {
		return;
	}
	CommonFightPanel.Show(CommonFightPanel.FightType.Plateau, function() {
			GFightManager.Playback(conf_ZhanDouPeiZhi_Data.Scene, FightSystemType.Plateau, Record, PlateauPart.OnFightFinished);
		});
}
PlateauPart.OnFightFinished = function() {
	if (isWin) {
		DungeonResultPopData.ShowPanel(DungeonResultPopData.DungeonShowType.Plateau,true,{
			confid : plateauID,
			star : finalStar
		});
	} else {
		DungeonResultPopData.ShowPanel(DungeonResultPopData.DungeonShowType.Plateau,false,{
			confid : plateauID,
			star : 0
		});
	}
}
//功能相关
//获取当前通关到第几章
PlateauPart.GetPassedChapter = function() {
	var topround = PlayerBasePart.IntAttr[PlayerIntAttr.PlayerIntAttr_PlateauTop];
	if ((topround == 0)) {
		return 0;
	}
	var conf_plateau = LoadConfig.getConfigData(ConfigName.JingLingGaoYuan,topround);
	if ((conf_plateau == null)) {
		console.error(("PlateauPart.GetPassedChapter精灵高原表异常topround:" + topround));
		return 0;
	}
	return conf_plateau.Chapter;
}
window.PlateauPart = PlateauPart;