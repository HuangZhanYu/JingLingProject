let guild_msg_pb = require('guild_msg').msg;
let guildboss_msg_pb = require('guildboss_msg').msg;
import CanShuTool from '../tool/CanShuTool';
import ViewManager from '../manager/ViewManager';

const GuildErrorNo = {
	HasApply: 10001,
	//已经申请过此公会
	ApplyFull: 10002,
	//公会申请列表已满
	JoinLimit: 10003,
	//不满足加入条件
	MemberFull: 10004,
	//公会成员已满	
	NotInApply: 10005,
	//不在申请列表	
	NoAccess: 10006,
	//没有权限
	NeedCheck: 10007,
	//需要管理员审核
	ManNoQuit: 10008,
	//会长不能退出公会
	NotIn: 10009,
	//此玩家不在公会
	ViceCountLimit: 10010,
	//副会长数量达到上限
	EliteCountLimit: 10011,
	//精英数量达到上限
	TimeLimit: 10012,
	//退出公会时间限制
	NameUsed: 10013,
	//公会名称已经被使用
	NameLength: 10014,
	//公会名长度不对
	InGuild: 10015,
	//玩家已有公会
	SelfApplyFull: 10016,
	//自己的申请数达到上限
	NoticeLength: 10017,
	//公告长度错误
	HasSign: 10018,
	//今日已经签过到
	Conf: 10019,
	//配置错误
	StatueHasActive: 10020,
	//建筑已经激活了
	StatueNotActive: 10021,
	//建筑没有激活
	StateMaxLevel: 10022,
	//建筑满级了，
	CoinNotEnough: 10023,
	//公会币不足
	WizardNotEnough: 10024,
	//精灵不足
	ContNotEnough: 10025,
	//玩家贡献不足
	BossIsOpen: 10026,
	//boss已经开启
	BossNotOpen: 10027,
	//boss没有开启
	BossHasDie: 10028,
	//boss已经死亡
	BossInFighting: 10029,
	//boss正在战斗中
	LineupInvaild: 10030,
	//阵容不合法
	OwnCard: 10031,
	//已拥有训练卡
	NotOwnCard: 10032,
	//没有训练卡
	HasGetAward: 10033,
	//已经领取过训练卡奖励
	NoMoreFightTimes: 10035,
	//挑战次数不足
	GuanQiaLimit: 10036,
	//最高通关关卡限制
	GuildNotExist: 10037,
	//公会不存在
	BossCountLimit: 10038,
	//开启boss数量限制
	SameAreaDiffLimit: 10039,
	//同地区难度boss开启限制

};
const GuildAccess = {
	SendCard: 1,
	//赠送卡
	Quit: 2,
	//退出公会
	AppElite: 3,
	//任命精英
	AppVice: 4,
	//任命副会长
	AppMan: 5,
	//转让馆长
	Kick: 6,
	//踢人
	OpenBoss: 7,
	//开启boss
	FightBoss: 8,
	//挑战boss
	DealApply: 9,
	//处理入会申请
	SetCheck: 10,
	//设置审核
	Impeach: 11,
	//弹劾会长
	SetNotice: 12,
	//设置公告
	UpBuilding: 13,
	//升级建筑
	Donate: 14,
	//捐献
	SetIcon: 15,
	//设置图标
	Sign: 16,
	//签到

};
const GuildP = {
	Chairman: 1,
	//会长
	Viceman: 2,
	//副会长
	Elite: 3,
	//精英
	Ordinary: 4,
	//普通成员
	New: 5,
	//新成员

};
const GuildBuild = {
	Hall: 1,
	//市政大厅
	Logistics: 2,
	//后勤中心
	Recruits: 3,
	//新兵营
	Training: 4,
	//训练中心
	FightPt: 5,
	//战斗台
	Shop: 6,
	//商店
	Statue1: 7,
	//雕像1
	Statue2: 8,
	//雕像2
	Statue3: 9,
	//雕像3

};
let GuildPart = { };

GuildPart.GuildID = "";
GuildPart.GuildLevel = 1;
GuildPart.Position = GuildP.New;
GuildPart.GainElves = [];
//{1,2,3,4,5} 光环队
GuildPart.Lineup = [];
//{{1,2},{3,4},{5,6}} 先锋队 支援队......
GuildPart.ErrorTip = { };
GuildPart.TeamIndex = 1;
GuildPart.InFight = false;
GuildPart.RecordList = { };
GuildPart.Records = null;
GuildPart.DoneList = { };
GuildPart.FightDone = false;
GuildPart.FightAns = { };
GuildPart.RecordIndex = 0;
GuildPart.DoubleCards = { };
GuildPart.BossAlive = false;
GuildPart.SignT = 0;
GuildPart.TotalFightTimes = 0;
GuildPart.SignTimes = 0;
//公会累计签到次数
GuildPart.GetRecords = [];
//已经领取的次数记录
GuildPart.mBuildID = null;
//雕像ID
GuildPart.StatueData = null;
//雕像数据
GuildPart.mBossArea = 0;
//boss区域
GuildPart.mBossDiff = 0;
//boss难易
GuildPart.mBossIndex = 0;
//bossid
GuildPart.mShangDiff = 0;
//伤害排名难度
GuildPart.mSgin = 0;
//签到难度
GuildPart.RewardList = { };
//签到奖励
GuildPart.LevelAtZero = 1;
//零点等级 默认1 
GuildPart.BossInfoType = 1;
//boss界面类型(1，boss信息界面，2.boss挑战界面)
GuildPart.Init = function() {
	//初始化错误提示map
	this.ErrorTip[GuildErrorNo.HasApply] = 1;
	this.ErrorTip[GuildErrorNo.ApplyFull] = 1;
	this.ErrorTip[GuildErrorNo.JoinLimit] = 1;
	this.ErrorTip[GuildErrorNo.MemberFull] = 1;
	this.ErrorTip[GuildErrorNo.NotInApply] = 1;
	this.ErrorTip[GuildErrorNo.NoAccess] = 1;
	this.ErrorTip[GuildErrorNo.NeedCheck] = 1;
	this.ErrorTip[GuildErrorNo.ManNoQuit] = 1;
	this.ErrorTip[GuildErrorNo.NotIn] = 1;
	this.ErrorTip[GuildErrorNo.ViceCountLimit] = 1;
	this.ErrorTip[GuildErrorNo.EliteCountLimit] = 1;
	this.ErrorTip[GuildErrorNo.TimeLimit] = 1;
	this.ErrorTip[GuildErrorNo.NameUsed] = 1;
	this.ErrorTip[GuildErrorNo.NameLength] = 1;
	this.ErrorTip[GuildErrorNo.InGuild] = 1;
	this.ErrorTip[GuildErrorNo.SelfApplyFull] = 1;
	this.ErrorTip[GuildErrorNo.NoticeLength] = 1;
	this.ErrorTip[GuildErrorNo.HasSign] = 1;
	this.ErrorTip[GuildErrorNo.Conf] = 1;
	this.ErrorTip[GuildErrorNo.StatueHasActive] = 1;
	this.ErrorTip[GuildErrorNo.StatueNotActive] = 1;
	this.ErrorTip[GuildErrorNo.StateMaxLevel] = 1;
	this.ErrorTip[GuildErrorNo.CoinNotEnough] = 1;
	this.ErrorTip[GuildErrorNo.WizardNotEnough] = 1;
	this.ErrorTip[GuildErrorNo.ContNotEnough] = 1;
	this.ErrorTip[GuildErrorNo.BossIsOpen] = 1;
	this.ErrorTip[GuildErrorNo.BossNotOpen] = 1;
	this.ErrorTip[GuildErrorNo.BossHasDie] = 1;
	this.ErrorTip[GuildErrorNo.BossInFighting] = 1;
	this.ErrorTip[GuildErrorNo.LineupInvaild] = 1;
	this.ErrorTip[GuildErrorNo.OwnCard] = 1;
	this.ErrorTip[GuildErrorNo.NotOwnCard] = 1;
	this.ErrorTip[GuildErrorNo.HasGetAward] = 1;
	this.ErrorTip[GuildErrorNo.NoMoreFightTimes] = 1;
	this.ErrorTip[GuildErrorNo.GuanQiaLimit] = 1;
}
GuildPart.Init();
GuildPart.OnErrorNo = function(ret) {
	//	local id = this.ErrorTip[ret]
	//    console.error("ret:"..ret);
	//	if not id then
	//		return
	//	end
	ToolTipPopPanel.Show(ret);
}
//是否拥有权限
GuildPart.OwnAccess = function(access, position) {
	var conf = LoadConfig.getConfigData(ConfigName.DaoGuan_QuanXian,position);
	if (!conf) {
		return false;
	}
	return (conf.Permissions[access] == 1);
}
//获取建筑开启等级
GuildPart.GetBuildOpenLevel = function(buildid) {
	var conf = LoadConfig.getConfigData(ConfigName.DaoGuan_JianZhu,buildid);
	if (!conf) {
		return 10000;
	}
	return conf.Open;
}
//某个建筑是否开启
GuildPart.IsBuildOpen = function(buildid, level) {
	return (level >= this.GetBuildOpenLevel(buildid));
}
//获取可以上阵的精灵或增益精灵 {id=count,id=count}
GuildPart.AlternateCardList = function() {
	var tab = { };
	for (var card of BackPackPart.mCardList) {
	
		if ((tab[card.CardID] == null)) {
			tab[card.CardID] = card.Count;
		} else {
			tab[card.CardID] = (tab[card.CardID] + card.Count);
		}
	}
	for (var card of LineupPart.CardList) {
		if ((tab[card.CardID] == null)) {
			tab[card.CardID] = 1;
		} else {
			tab[card.CardID] = (tab[card.CardID] + 1);
		}
	}
	for (var cardid of this.GainElves) {
		if ((tab[cardid] != null)) {
			tab[cardid] = (tab[cardid] - 1);
		}
		
	}
	for (var cloth of this.Lineup) {
		
		for (var cardid of cloth) {
			
			if ((tab[cardid] != null)) {
				tab[cardid] = (tab[cardid] - 1);
			}
		
		}
		
	}
	return tab;
}
//获取可上阵的增益精灵
GuildPart.AlternateEvles = function() {
	var newtab = { };
	var tab = this.AlternateCardList();
	for (var id in tab) {
		var count = tab[id];
		
		if (LoadConfig.getConfigData(ConfigName.DaoGuan_ZengYiJingLing,id)) {
			newtab[id] = count;
		}
	}
	return newtab;
}
//获取可上阵的增益精灵,和所有可上阵
GuildPart.GetCanUsePokemen = function() {
	var newtab = { };
	var tab = this.AlternateCardList();
	for (var forinvar in tab) {
		var id = forinvar.id;
		var count = forinvar.count;
		{
			if (LoadConfig.getConfigData(ConfigName.DaoGuan_ZengYiJingLing,id)) {
				newtab[id] = count;
			}
		}
	}
	return [newtab, tab];
}
//获取全部精灵。包括上阵的 {id=count,id=count}
GuildPart.AlternateALLCardList = function() {
	var tab = { };
	for (var card of BackPackPart.mCardList) {
		if ((tab[card.CardID] == null)) {
			tab[card.CardID] = card.Count;
		} else {
			tab[card.CardID] = (tab[card.CardID] + card.Count);
		}
		
	}
	for (var card of LineupPart.CardList) {
		if ((tab[card.CardID] == null)) {
			tab[card.CardID] = 1;
		} else {
			tab[card.CardID] = (tab[card.CardID] + 1);
		}
	
	}
	return tab;
}
//判断签到次数奖励是否领取
GuildPart.IsTimesAwardGet = function(times) {
	for (var num of this.GetRecords) {
		
		if ((num == times)) {
			return true;
		}
	}
	return false;
}
GuildPart.LoginSync = function(buffer) {
	var msg = guild_msg_pb.GuildPart_Sync.decode(buffer);

	this.GuildID = msg.GuildID;
	this.DoubleCards = msg.DoubleCards;
	this.BossAlive = msg.BossAlive;
	this.GuildLevel = msg.GuildLevel;
	this.TotalFightTimes = msg.TotalFightTimes;
	//MainPanel.RefreshBossButton();
}
//获取公会列表
GuildPart.GetGuildList = function() {
	var req = new  guild_msg_pb.GuildPart_GuildListRequest();
	SocketClient.callServerFun("GuildPart_GetGuildList", req);
}
//获取公会列表的回调
GuildPart.GuildListResp = function(buffer) {
	var ans = guild_msg_pb.GuildPart_GuildListResponse.decode(buffer);

	var guildList = ans.GuildList;
	//道馆链表界面
	DaoGuanLieBiaoPopPanel.Show(guildList);
}
//搜索公会
GuildPart.Search = function(name) {
	var req = new  guild_msg_pb.GuildPart_SearchRequest();
	req.Name = name;
	SocketClient.callServerFun("GuildPart_SearchRequest", req);
}
//搜索公会的回调
GuildPart.SearchResp = function(buffer) {
	var ans = guild_msg_pb.GuildPart_SearchResponse.decode(buffer);

	if ((ans.Ret != 0)) {
		this.OnErrorNo(ans.Ret);
		return;
	}
	//道馆链表界面
	DaoGuanLieBiaoPopPanel.Show(ans.GuildList);
}
//获取公会信息
GuildPart.GetGuildInfo = function(typ) {
	if ((this.GuildID == "")) {
		return;
	}
	var req = new  guild_msg_pb.GuildPart_GuildInfoRequest();
	GuildPart.BossInfoType = typ;
	SocketClient.callServerFun("GuildPart_GetGuildInfo", req);
}
//获取公会信息会滴
GuildPart.GetInfoResp = function(buffer) {
	var ans = guild_msg_pb.GuildPart_GuildInfoResponse.decode(buffer);

	this.SignT = ans.SignT;
	this.GainElves = ans.GainElves;
	this.GuildLevel = ans.Level;
	for (let i = 0; i < ans.ClothArray.length; ++i) {
		var cloth = ans.ClothArray[i];
		
		var tab = [];
		for (var id of cloth.CardList) {
			tab.push(id);
		}
		this.Lineup[i] = tab;
		
	}
	if ((GuildPart.BossInfoType == 1)) {
		//道馆主界面
		DaoGuanMainPanel.Show(ans);
	} else {
		var bossData = ans.OpenedBoss[0];
		if ((bossData != null)) {
			DaoGuanMainBossPopPanel.Show(ans, bossData);
		}
	}
}
//获取公会成员
GuildPart.GetMembers = function() {
	if ((this.GuildID == "")) {
		return;
	}
	var req = new  guild_msg_pb.GuildPart_MembersRequest();
	SocketClient.callServerFun("GuildPart_GetMembers", req);
}
//成员排序函数
function GuildMembersComp(m1, m2) {
	var w1 = (((((10 - m1.Position)) * 10000000) + (m1.Level * 100000)) + m1.TopGuanQia);
	var w2 = (((((10 - m2.Position)) * 10000000) + (m2.Level * 100000)) + m2.TopGuanQia);
	if (m1.Online) {
		w1 = (w1 + 100000000);
	}
	if (m2.Online) {
		w2 = (w2 + 100000000);
	}
	if ((w1 == w2)) {
		if ((m1.Contribution == m2.Contribution)) {
			return (m1.ObjID > m2.ObjID);
		} else {
			return (m1.Contribution > m2.Contribution);
		}
	}
	return (w1 > w2);
}
//获取公会成员回调
GuildPart.MembersResp = function(buffer) {
	var ans = guild_msg_pb.GuildPart_MembersResponse.decode(buffer);

	table.sort(ans.Members, GuildMembersComp);
	//行政大厅
	DaoGuanXZPopPanel.Show(ans);
}
//获取申请列表
GuildPart.GetApplyList = function() {
	if ((this.GuildID == "")) {
		return;
	}
	var req = new  guild_msg_pb.GuildPart_AppliesRequest();
	SocketClient.callServerFun("GuildPart_GetApplyList", req);
}
//申请列表回调
GuildPart.AppiesResp = function(buffer) {
	var ans = guild_msg_pb.GuildPart_AppliesResponse.decode(buffer);

	//设置申请链表信息
	DaoGuanXZPopPanel.SetDaoGuanShenQingInfo(ans);
}
//获取日志列表
GuildPart.GetLogList = function() {
	if ((this.GuildID == "")) {
		return;
	}
	var req = new  guild_msg_pb.GuildPart_LogsRequest();
	SocketClient.callServerFun("GuildPart_GetLogList", req);
}
//获取日志回调
GuildPart.LogsResp = function(buffer) {
	var ans = guild_msg_pb.GuildPart_LogsResponse.decode(buffer);

	//设置日志信息
	DaoGuanXZPopPanel.GongHuiNoteInfo(ans);
}
//创建公会
GuildPart.CreateGuild = function(name, notice, icon) {
	if ((this.GuildID != "")) {
		return;
	}
	if ((BannedWords[name] != null)) {
		//敏感字
		return;
	}
	var req = new  guild_msg_pb.GuildPart_CreateRequest();
	req.Name = name;
	req.Notice = notice;
	req.Icon = icon;
	SocketClient.callServerFun("GuildPart_CreateGuild", req);
}
//创建公会回调
GuildPart.CreateResp = function(buffer) {
	var ans = guild_msg_pb.GuildPart_CreateResponse.decode(buffer);

	if ((ans.Ret != 0)) {
		//this.ErrorTip(ans.Ret)
		GuildPart.OnErrorNo(ans.Ret);
		return;
	}
	this.GuildID = ans.GuildID;
	this.GuildLevel = 1;
	//道馆主界面信息
	GuildPart.GetGuildInfo(1);
}
//申请加入
GuildPart.Apply = function(guildid) {
	var req = new  guild_msg_pb.GuildPart_ApplyRequest();
	req.GuildID = guildid;
	SocketClient.callServerFun("GuildPart_Apply", req);
}
//申请加入回调
GuildPart.ApplyResp = function(buffer) {
	var ans = guild_msg_pb.GuildPart_ApplyResponse.decode(buffer);

	if ((ans.Ret != 0)) {
		GuildPart.OnErrorNo(ans.Ret);
		return;
	}
	this.GuildID = ans.MyGuildID;
	if ((ans.MyGuildID != '')) {
		GuildPart.GetGuildInfo(1);
	} else {
		GuildPart.GetGuildList();
	}
}
//取消申请
GuildPart.CancelApply = function(guildid) {
	if ((this.GuildID != "")) {
		return;
	}
	var req = new  guild_msg_pb.GuildPart_CancelApplyRequest();
	req.GuildID = guildid;
	SocketClient.callServerFun("GuildPart_CancelApply", req);
}
//取消申请回调
GuildPart.CancelApplyResp = function(buffer) {
	var ans = guild_msg_pb.GuildPart_CancelApplyResponse.decode(buffer);

	if ((ans.Ret != 0)) {
		GuildPart.OnErrorNo(ans.Ret);
		return;
	}
	GuildPart.GetGuildList();
}
//同意加入
GuildPart.Agree = function(objid) {
	var req = new  guild_msg_pb.GuildPart_AgreeRequest();
	req.ObjID = objid;
	SocketClient.callServerFun("GuildPart_Agree", req);
}
//同意加入回调
GuildPart.AgreeResp = function(buffer) {
	var ans = guild_msg_pb.GuildPart_AgreeResponse.decode(buffer);

	if ((ans.Ret != 0)) {
		GuildPart.OnErrorNo(ans.Ret);
		return;
	}
	//刷新界面
	GuildPart.GetApplyList();
}
//拒绝加入
GuildPart.Refuse = function(objid) {
	var req = new  guild_msg_pb.GuildPart_RefuseRequest();
	req.ObjID = objid;
	SocketClient.callServerFun("GuildPart_Refuse", req);
}
//拒绝加入回调
GuildPart.RefuseResp = function(buffer) {
	var ans = guild_msg_pb.GuildPart_RefuseResponse.decode(buffer);

	if ((ans.Ret != 0)) {
		GuildPart.OnErrorNo(ans.Ret);
		return;
	}
}
//退出
GuildPart.Quit = function() {
	var req = new  guild_msg_pb.GuildPart_QuitRequest();
	SocketClient.callServerFun("GuildPart_Quit", req);
}
//退出回调
GuildPart.QuitResp = function(buffer) {
	var ans = guild_msg_pb.GuildPart_QuitResponse.decode(buffer);

	if ((ans.Ret != 0)) {
		GuildPart.OnErrorNo(ans.Ret);
		return;
	}
	this.GuildID = "";
	//退出道馆事件
	DaoGuanMainPanel.QuitDaoGuan();
};
//踢出
GuildPart.Kick = function(objid) {
	var req = new  guild_msg_pb.GuildPart_KickRequest();
	req.ObjID = objid;
	SocketClient.callServerFun("GuildPart_Kick", req);
};
//踢出回调
GuildPart.KickResp = function(buffer) {
	var ans = guild_msg_pb.GuildPart_KickResponse.decode(buffer);

	if ((ans.Ret != 0)) {
		GuildPart.OnErrorNo(ans.Ret);
		return;
	}
	GuildPart.GetMembers();
};
//任命
GuildPart.Appoint = function(objid, position) {
	var req = new  guild_msg_pb.GuildPart_AppointRequest();
	req.ObjID = objid;
	req.Position = position;
	SocketClient.callServerFun("GuildPart_Appoint", req);
};
//任命回调
GuildPart.AppointResp = function(buffer) {
	var ans = guild_msg_pb.GuildPart_AppointResponse.decode(buffer);

	if ((ans.Ret != 0)) {
		GuildPart.OnErrorNo(ans.Ret);
		return;
	}
	GuildPart.GetMembers();
};
//弹劾
GuildPart.Impeach = function() {
	var req = new  guild_msg_pb.GuildPart_ImpeachRequest();
	SocketClient.callServerFun("GuildPart_Impeach", req);
};
//弹劾回调
GuildPart.ImpeachResp = function(buffer) {
	var ans = guild_msg_pb.GuildPart_ImpeachResponse.decode(buffer);

	if ((ans.Ret != 0)) {
		GuildPart.OnErrorNo(ans.Ret);
		return;
	}
	GuildPart.GetMembers();
};
//设置审核
GuildPart.SetCheck = function(check, limit) {
	var req = new  guild_msg_pb.GuildPart_SetCheckRequest();
	req.Open = check;
	req.LimitV = limit;
	SocketClient.callServerFun("GuildPart_SetCheck", req);
};
//设置审核回调
GuildPart.SetCheckResp = function(buffer) {
	var ans = guild_msg_pb.GuildPart_SetCheckResponse.decode(buffer);

	if ((ans.Ret != 0)) {
		GuildPart.OnErrorNo(ans.Ret);
		return;
	}
	//刷新
	GuildPart.GetApplyList();
};
//设置图标
GuildPart.SetIcon = function(icon) {
	var req = new  guild_msg_pb.GuildPart_SetIconRequest();
	req.Icon = icon;
	SocketClient.callServerFun("GuildPart_SetIcon", req);
};
//设置图标回调
GuildPart.SetIconResp = function(buffer) {
	var ans = guild_msg_pb.GuildPart_SetIconResponse.decode(buffer);

	if ((ans.Ret != 0)) {
		GuildPart.OnErrorNo(ans.Ret);
		return;
	}
	//刷新主界面图标
	DaoGuanMainPanel.SetDaoGuanIcon(ans);
}
//设置公告
GuildPart.SetNotice = function(notice) {
	var req = new  guild_msg_pb.GuildPart_SetNoticeRequest();
	req.Notice = notice;
	SocketClient.callServerFun("GuildPart_SetNotice", req);
}
//设置公告回调
GuildPart.SetNoticeResp = function(buffer) {
	var ans = guild_msg_pb.GuildPart_SetNoticeResponse.decode(buffer);

	if ((ans.Ret != 0)) {
		GuildPart.OnErrorNo(ans.Ret);
		return;
	}
	DaoGuanMainPanel.SetGongGaoInfo(ans);
}
//清空申请
GuildPart.ClearApply = function() {
	var req = new  guildboss_msg_pb.GuildPart_ClearApplyRequest();
	SocketClient.callServerFun("GuildPart_ClearApply", req);
	ViewManager.showLoading({});
}
//清空申请回调
GuildPart.ClearApplyResp = function(buffer) {
	ViewManager.hideLoading();
	var ans = guildboss_msg_pb.GuildPart_ClearApplyResponse.decode(buffer);

	if ((ans.Ret != 0)) {
		GuildPart.OnErrorNo(ans.Ret);
		return;
	}
	//刷新界面
	GuildPart.GetApplyList();
}
//获取雕像信息
GuildPart.GetBuildingInfo = function(buildid) {
	var req = new  guild_msg_pb.GuildPart_BuildingInfoRequest();
	req.BuildID = buildid;
	GuildPart.mBuildID = buildid;
	SocketClient.callServerFun("GuildPart_BuildingInfo", req);
	ViewManager.showLoading({});
}
//获取雕像信息回调
GuildPart.BuildingInfoResp = function(buffer) {
	ViewManager.hideLoading();
	var ans = guild_msg_pb.GuildPart_BuildingInfoResponse.decode(buffer);

	GuildPart.StatueData = ans;
	//获取雕像信息
	DaoGuanRoleGHPopPanel.Show(ans, GuildPart.mBuildID);
}
//获取已经捐献的次数
GuildPart.GetDonateCount = function(typ, id) {
	if ((typ == 1)) {
		return GuildPart.StatueData.GuildDonateTimes;
	}
	if ((typ == 2)) {
		for (var i = 0; i != GuildPart.StatueData.Records.length; ++i) {
			var data = GuildPart.StatueData.Records[i];
			if ((data.ID == id)) {
				return data.Times;
			}
		}
		return 0;
	}
	return 0;
}
//捐献
GuildPart.Donate = function(taskid, buildid) {
	var req = new  guild_msg_pb.GuildPart_DonateRequest();
	req.BuildID = buildid;
	req.DonateT = taskid;
	SocketClient.callServerFun("GuildPart_Donate", req);
}
//捐献回调
GuildPart.DonateResp = function(buffer) {
	var ans = guild_msg_pb.GuildPart_DonateResponse.decode(buffer);

	if ((ans.Ret != 0)) {
		GuildPart.OnErrorNo(ans.Ret);
		return;
	}
	// required    int32   Ret         = 1;
	// optional    bool    Active      = 2;
	// optional    int32   ActiveExp   = 3;
	// optional    int32   Level       = 4;
	// optional    int32   DonateT     = 5;
	//经验变更，雕像等级变更
	GuildPart.StatueData.ActiveExp = ans.ActiveExp;
	GuildPart.StatueData.Level = ans.Level;
	var data = LoadConfig.getConfigData(ConfigName.DaoGuan_JuanXianXin,ans.DonateT);
	if ((data == null)) {
		return;
	}
	if ((data.DonateT == 1)) {
		GuildPart.StatueData.GuildDonateTimes = ans.GuildDonateTimes;
	} else if ((data.DonateT == 2) ){
		for (var v of ans.Records) {
			//如果存在则修改 不存在则插入
			var isExist = false;
			var index = -1;
			for (var i = 0; i != GuildPart.StatueData.Records.length; ++i) {
				if ((GuildPart.StatueData.Records[i].ID == v.ID)) {
					isExist = true;
					index = i;
					break;
				}
			}
			if (!isExist) {
				GuildPart.StatueData.Records.push(v);
			} else {
				GuildPart.StatueData.Records[index].Times = v.Times;
			}
			
		}
	}
	//显示物品获得界面
	var resJiangChiType = [
		DaoJuTool.ResourceType.Guild,
		data.RewardType
	];
	var resJiangChiId = [
		22,
		data.RewardID
	];
	var resJiangChiCount = [
		data.GetExp,
		data.RewardCount
	];
	HuoDeWuPinTiShiPanelData.Show(resJiangChiType, resJiangChiId, resJiangChiCount);
	//刷新捐献界面数据
	GuildDonatePopPanel.OnDonateSuccess();
}
//升级建筑
GuildPart.UpBuilding = function(buildid) {
	var req = new  guild_msg_pb.GuildPart_UpBuildingRequest();
	req.BuildID = buildid;
	SocketClient.callServerFun("GuildPart_UpBuilding", req);
}
//升级建筑回调
GuildPart.UpBuildingResp = function(buffer) {
	var ans = guild_msg_pb.GuildPart_UpBuildingResponse.decode(buffer);

	if ((ans.Ret != 0)) {
		GuildPart.OnErrorNo(ans.Ret);
		return;
	}
	//ToolTipPopPanel.Show(302);
	DaoGuanRoleGHPopPanel.DiaoXiangLevelEffect();
	//需要调用刷新界面
	GuildPart.GetBuildingInfo(GuildPart.mBuildID);
}
//升级自身建筑
GuildPart.UpSelfBuilding = function(buildid) {
	var req = new  guild_msg_pb.GuildPart_UpSelfBuildRequest();
	req.BuildID = buildid;
	SocketClient.callServerFun("GuildPart_UpSelfBuilding", req);
	ViewManager.showLoading({});
}
//升级自身建筑回调
GuildPart.UpSelfBuildingResp = function(buffer) {
	ViewManager.hideLoading();
	var ans = guild_msg_pb.GuildPart_UpSelfBuildResponse.decode(buffer);

	if ((ans.Ret != 0)) {
		GuildPart.OnErrorNo(ans.Ret);
		return;
	}
	//ToolTipPopPanel.Show(303);
	//DaoGuanRoleGHPopPanel.ZengYiLevelEffect();
	GuildPart.GetBuildingInfo(GuildPart.mBuildID);
	DaoGuanZYSJPopPanel.Show(ans, GuildPart.mBuildID);
}
//获取打卡信息
GuildPart.SignInfo = function() {
	var req = new  guildboss_msg_pb.GuildPart_SignInfoRequest();
	SocketClient.callServerFun("GuildPart_SignInfo", req);
	ViewManager.showLoading({});
}
//打卡信息回调
GuildPart.SignInfoResp = function(buffer) {
	ViewManager.hideLoading();
	var ans = guildboss_msg_pb.GuildPart_SignInfoResponse.decode(buffer);

	this.SignTimes = ans.SignTimes;
	this.LevelAtZero = ans.LevelAtZero;
	this.GetRecords.length = 0;
	for (var times of ans.GetRecords) {
		this.GetRecords.push(times);
	}
	this.RewardList = { };
	this.RewardList = ans.RewardList;
	//打开道馆打卡界面
	DaoGuanDaKaPopPanel.Show();
}
//通过签到数量获取签到奖励
GuildPart.GetSignRewardByIndex = function(signTimes) {
	var rewardData = { };
	if ((GuildPart.RewardList == null)) {
		return rewardData;
	}
	for (var k in GuildPart.RewardList) {
		var v = GuildPart.RewardList[k];
		
		if ((v.Times == signTimes)) {
			rewardData = v.Awards;
			return rewardData;
		}
		
	}
	return rewardData;
}
//签到
GuildPart.Sign = function(signt) {
	var req = new  guildboss_msg_pb.GuildPart_SignRequest();
	req.SignT = signt;
	GuildPart.mSgin = signt;
	SocketClient.callServerFun("GuildPart_Sign", req);
	ViewManager.showLoading({});
}
//签到回调
GuildPart.SignResp = function(buffer) {
	ViewManager.hideLoading();
	var ans = guildboss_msg_pb.GuildPart_SignResponse.decode(buffer);

	if ((ans.Ret != 0)) {
		GuildPart.OnErrorNo(ans.Ret);
		return;
	}
	this.SignT = ans.SignT;
	this.SignTimes = ans.SignTimes;
	var conf_DaoGuan = LoadConfig.getConfigData(ConfigName.DaoGuan_ShengJi,ans.GuildLevel);
	if ((conf_DaoGuan == null)) {
		console.error(("Conf_DaoGuan_ShengJi数据为空，id:" + ans.GuildLevel));
		return;
	}
	var puTongType = conf_DaoGuan.ResourceType;
	var gaoJiType = conf_DaoGuan.ZResourceType;
	var haohuaType = conf_DaoGuan.HHesourceType;
	var length = null;
	var resType = { };
	var resID = { };
	var resCount = { };
	if ((GuildPart.mSgin == 0)) {
		for (var i = 0; i != puTongType.length; ++i) {
			if ((puTongType[i] <= 0)) {
				break;
			}
			resType[(resType.length + 1)] = conf_DaoGuan.ResourceType[i];
			resID[(resID.length + 1)] = conf_DaoGuan.ResourceID[i];
			resCount[(resCount.length + 1)] = conf_DaoGuan.ResourceCount[i];
		}
	} else if ((GuildPart.mSgin == 1) ){
		for (var i = 0; i != gaoJiType.length; ++i) {
			if ((gaoJiType[i] <= 0)) {
				break;
			}
			resType[(resType.length + 1)] = conf_DaoGuan.ZResourceType[i];
			resID[(resID.length + 1)] = conf_DaoGuan.ZResourceID[i];
			resCount[(resCount.length + 1)] = conf_DaoGuan.ZResourceCount[i];
		}
	} else {
		for (var i = 0; i != haohuaType.length; ++i) {
			if ((haohuaType[i] <= 0)) {
				break;
			}
			resType[(resType.length + 1)] = conf_DaoGuan.HHesourceType[i];
			resID[(resID.length + 1)] = conf_DaoGuan.HHesourceID[i];
			resCount[(resCount.length + 1)] = conf_DaoGuan.HHesourceCount[i];
		}
	}
	//刷新主界面货币
	//GuildPart.GetGuildInfo();
	//获得货币
	HuoDeWuPinTiShiPanelData.Show(resType, resID, resCount);
	//刷新打卡界面
	DaoGuanDaKaPopPanel.SignSuccess();
	DaoGuanDaKaPopPanel.UpdateRewards(this.SignTimes);
}
//获取打卡次数奖励
GuildPart.SignTimesAward = function(times) {
	var req = new  guildboss_msg_pb.GuildPart_SignTimesAwardRequest();
	req.Times = times;
	SocketClient.callServerFun("GuildPart_SignTimesAward", req);
	ViewManager.showLoading({});
}
//获取打卡次数奖励回调
GuildPart.SignTimesAwardResp = function(buffer) {
	ViewManager.hideLoading();
	var ans = guildboss_msg_pb.GuildPart_SignTimesAwardResponse.decode(buffer);

	if ((ans.Ret == 1)) {
		return;
	} else if ((ans.Ret == 2) ){
		return;
	}
	this.SignTimes = ans.SignTimes;
	this.GetRecords.length = 0;
	for (var times of ans.GetRecords) {
		this.GetRecords.push(times);
	}
	var resType = [];
	var resID = [];
	var resCount = [];
	for (var award of ans.Awards) {
		resType.push(award.ItemType);
		resID.push(award.ItemID);
		resCount.push(award.ItemNum);
	}
	HuoDeWuPinTiShiPanelData.Show(resType, resID, resCount);
	//只刷新打卡奖励模块
	DaoGuanDaKaPopPanel.UpdateRewards(this.SignTimes);
}
//获取训练卡信息
GuildPart.CardInfo = function() {
	if ((this.GuildID == "")) {
		return;
	}
	var req = new  guildboss_msg_pb.GuildPart_CardInfoRequest();
	SocketClient.callServerFun("GuildPart_CardInfo", req);
	ViewManager.showLoading({});
}
//训练卡信息回调
GuildPart.CardInfoResp = function(buffer) {
	ViewManager.hideLoading();
	var ans = guildboss_msg_pb.GuildPart_CardInfoResponse.decode(buffer);

	BootCampPopPanel.OnBCRequestSuccess(ans);
}
//获取可赠送的成员列表
GuildPart.CaSMembers = function() {
	if ((this.GuildID == "")) {
		return;
	}
	var req = new  guildboss_msg_pb.GuildPart_CaSMembersRequest();
	SocketClient.callServerFun("GuildPart_CaSMembers", req);
}
//可赠送成员列表回调
GuildPart.CaSMembersResp = function(buffer) {
	var ans = guildboss_msg_pb.GuildPart_CaSMembersResponse.decode(buffer);

	table.sort(ans.Members, GuildMembersComp);
	console.error("返回可赠送成员列表");
	BootCampListPopPanel.OnBCListRequestSuccess(ans);
}
//赠送训练卡
GuildPart.SendCard = function(objid) {
	var req = new  guildboss_msg_pb.GuildPart_SendCardRequest();
	req.ObjID = objid;
	SocketClient.callServerFun("GuildPart_SendCard", req);
}
//赠送训练卡回调
GuildPart.SendCardResp = function(buffer) {
	var ans = guildboss_msg_pb.GuildPart_SendCardResponse.decode(buffer);

	console.error(("收到返回赠送训练卡回调信息" + ans.ObjID));
	//自己开通
	if (((ans.ObjID == PlayerBasePart.ObjID) && BootCampPopPanel.IsShow())) {
		BootCampPopPanel.OnSuccessXunlianka();
	}
}
//赠送成功回调
GuildPart.SendCardRet = function(buffer) {
	var ans = guildboss_msg_pb.GuildPart_SendCardRet();

}
//领取训练卡奖励
GuildPart.GetCardAwards = function() {
	var req = new  guildboss_msg_pb.GuildPart_CardAwardsRequest();
	SocketClient.callServerFun("GuildPart_GetCardAwards", req);
}
//领取训练卡奖励回调
GuildPart.GetCardAwardsResp = function(buffer) {
	var ans = guildboss_msg_pb.GuildPart_CardAwardsResponse.decode(buffer);

	console.error("返回领取训练卡奖励");
	//获取奖励
	BootCampPopPanel.OntakeJiangLi(ans);
}
//boss信息
GuildPart.BossInfo = function() {
	var req = new  guildboss_msg_pb.GuildPart_BossInfoRequest();
	SocketClient.callServerFun("GuildPart_BossInfo", req);
}
//boss信息回调
GuildPart.BossInfoResp = function(buffer) {
	var ans = guildboss_msg_pb.GuildPart_BossInfoResponse.decode(buffer);

	DaoGuanXunLPopPanel.Show(ans);
}
//开启boss
GuildPart.OpenBoss = function(area, diff, bossid) {
	var req = new  guildboss_msg_pb.GuildPart_OpenBossRequest();
	req.Area = area;
	req.Diff = diff;
	req.BossID = bossid;
	SocketClient.callServerFun("GuildPart_OpenBoss", req);
}
//开启boss回调
GuildPart.OpenBossResp = function(buffer) {
	var ans = guildboss_msg_pb.GuildPart_OpenBossResponse.decode(buffer);

	if ((ans.Ret != 0)) {
		GuildPart.OnErrorNo(ans.Ret);
		return;
	}
	//刷新boss界面
	GuildPart.BossInfo();
}
//挑战boss
GuildPart.FightBoss = function(area, diff, bossid) {
	var req = new  guildboss_msg_pb.GuildPart_FightBossRequest();
	req.Area = area;
	req.Diff = diff;
	req.BossID = bossid;
	GuildPart.mBossArea = area;
	GuildPart.mBossDiff = diff;
	GuildPart.mBossIndex = bossid;
	SocketClient.callServerFun("GuildPart_FightBoss", req);
	ViewManager.showLoading({});
	this.RecordList = { };
	this.Records = null;
	this.DoneList = { };
	this.TeamIndex = 1;
	this.RecordIndex = 0;
	this.InFight = false;
	this.FightDone = false;
}
//挑战boss回调
GuildPart.FightBossResp = function(buffer) {
	ViewManager.hideLoading();
	var ans = guildboss_msg_pb.GuildPart_FightBossResponse.decode(buffer);

	if ((ans.Ret != 0)) {
		GuildPart.OnErrorNo(ans.Ret);
		return;
	}
	GuildPart.FightAns = ans;
	this.TotalFightTimes = ans.TotalFightTimes;
}
//挑战boss战斗过程下发
GuildPart.FightBossRecord = function(buffer) {
	var msg = guildboss_msg_pb.GuildPart_FightBossRecord();

	this.RecordIndex = (this.RecordIndex + 1);
	this.Records = new FightRecords();
	this.Records.Decode(msg.Content);
	if (msg.Done) {
		table.insert(this.DoneList, this.RecordIndex);
		this.Playback();
	}
}
GuildPart.FightBossRecordFollow = function(buffer) {
	var msg = guildboss_msg_pb.GuildPart_FightBossRecord();

	this.Records.AppendFollow(msg.Content);
	//log("push count:"..this.Records.FrameList.." recordindex:"..this.RecordIndex)
	if (msg.Done) {
		table.insert(this.DoneList, this.RecordIndex);
		this.Playback();
	}
}
GuildPart.Playback = function() {
	if (!this.InFight) {
		var zdData = LoadConfig.getConfigData(ConfigName.ZhanDouPeiZhi,15);
		this.InFight = true;
		//boss挑战界面
		DaoGuanFightPanel.Show(GuildPart.mBossArea, GuildPart.mBossDiff, GuildPart.mBossIndex, function() {
				GFightManager.Playback(zdData.Scene, FightSystemType.GuildBoss, this.Records, GuildPart.OnFightEnd);
			});
	}
}
//挑战boss战斗回调  回调结算界面
GuildPart.OnFightEnd = function(ret) {
	this.TeamIndex = (this.TeamIndex + 1);
	this.InFight = false;
	if (this.FightDone) {
		//战斗结束 
		//弹战队结束界面
		DaoGuanJieSuanPopPanel.Show(GuildPart.mBossArea, GuildPart.mBossDiff, GuildPart.mBossIndex);
		return;
	}
	/*if this.TeamIndex > this.DoneList then
		return
	end*/	//
	DaoGuanFightPanel.UpdataHouQingInfo();
	var timer = 3;
	GuildPart.OnYanShiFight(timer);
}
//延迟三秒调用支援队
GuildPart.OnYanShiFight = function(time) {
	DaoGuanFightPanel.CountDownTime();
	//timerMgr.RemoveTimerEvent('GuildPart.CWrefreshFight');
	//timerMgr.AddTimerEvent('GuildPart.CWrefreshFight', time, GuildPart.CWrefreshFight, { }, false);
}
GuildPart.CWrefreshFight = function() {
	this.GetFightRecords(this.TeamIndex);
	//local zdData = LoadConfig.getConfigData(ConfigName.ZhanDouPeiZhi,15);
	//GFightManager.Playback(zdData.Scene, FightSystemType.GuildBoss, this.RecordList[this.TeamIndex], GuildPart.OnFightEnd);
}
GuildPart.GetFightRecords = function(index) {
	var req = new  guildboss_msg_pb.GuildPart_FightRecordsRequest();
	req.TeamIndex = index;
	console.error(("get records:" + index));
	SocketClient.callServerFun("GuildPart_FightBossRecord", req);
}
GuildPart.FightBossRecordDone = function(buffer) {
	var ans = guildboss_msg_pb.GuildPart_FightRecordsResponse.decode(buffer);

	this.FightDone = true;
	//DaoGuanJieSuanPopPanel.Show(GuildPart.mBossArea,GuildPart.mBossDiff,GuildPart.mBossIndex);
}
//伤害排行
GuildPart.DamageRank = function(area, diff, bossid) {
	var req = new  guildboss_msg_pb.GuildPart_DamageRankRequest();
	req.Area = area;
	req.Diff = diff;
	req.BossID = bossid;
	GuildPart.mShangDiff = diff;
	SocketClient.callServerFun("GuildPart_DamageRank", req);
}
function DamageComp(m1, m2) {
	if (BigNumber.equalTo(m1.Damage, m2.Damage)) {
		return (m1.ObjID > m2.ObjID);
	}
	return BigNumber.greaterThan(m1.Damage, m2.Damage);
}
//伤害排行回调
GuildPart.DamageRankResp = function(buffer) {
	var ans = guildboss_msg_pb.GuildPart_DamageRankResponse.decode(buffer);

	if (!ans.DamageList) {
		DaoGuanSHPHPopPanel.Show({ }, 0, GuildPart.mShangDiff);
		return;
	}
	ans.DamageList.sort(DamageComp);
	for (var i = 0; i < ans.DamageList.length; ++i) {
		var item = ans.DamageList[i];
		{
			if ((item.ObjID == PlayerBasePart.ObjID)) {
				myrank = i;
				break;
			}
		}
	}
	DaoGuanSHPHPopPanel.Show(ans.DamageList, myrank, GuildPart.mShangDiff);
}
//获取布阵的精灵
//获取布阵信息
GuildPart.ClothInfo = function() {
	var req = new  guildboss_msg_pb.GuildPart_ClothInfoRequest();
	SocketClient.callServerFun("GuildPart_ClothInfo", req);
}
//布阵信息回调
GuildPart.ClothInfoResp = function(buffer) {
	var ans = guildboss_msg_pb.GuildPart_ClothInfoResponse.decode(buffer);

	LogisticsPopPanel.OnLogicticsRequestSuccess(ans);
}
//布阵
GuildPart.ClothArray = function(index, wizardlist) {
	var req = new  guildboss_msg_pb.GuildPart_ClothArrayRequest();
	req.Index = index;
	for (var cardid of wizardlist) {
		req.WizardList.push(cardid);
	}
	SocketClient.callServerFun("GuildPart_ClothArray", req);
}
//布阵回调
GuildPart.ClothArrayResp = function(buffer) {
	var ans = guildboss_msg_pb.GuildPart_ClothArrayResponse.decode(buffer);

	if ((ans.Ret != 0)) {
		GuildPart.OnErrorNo(ans.Ret);
		return;
	}
	this.Lineup[ans.Index] = ans.WizardList;
	DaoGuanMainPanel.UpdataHouQingInfo();
	//刷新阵容红点
	DaoGuanXunLPopPanel.UpdateRedPoint();
	DaoGuanMainBossPopPanel.UpdateRedPoint();
}
//增益精灵上阵
GuildPart.ClothElves = function(cardlist) {
	var req = new  guildboss_msg_pb.GuildPart_ClothElvesRequest();
	for (var cardid of cardlist) {
		
		req.ElvesList.push(cardid);
	}
	SocketClient.callServerFun("GuildPart_ClothElves", req);
	ViewManager.showLoading({});
}
//增益精灵上阵回调
GuildPart.ClothElvesResp = function(buffer) {
	ViewManager.hideLoading();
	var ans = guildboss_msg_pb.GuildPart_ClothElvesResponse.decode(buffer);

	if ((ans.Ret != 0)) {
		GuildPart.OnErrorNo(ans.Ret);
		return;
	}
	this.GainElves = ans.ElvesList;
	//卸下时刷新增益界面
	DaoGuanZengYiPopPanel.UpdataZengYiJingLing();
	//刷新训练中心增益信息
	DaoGuanXunLPopPanel.UpdataZengYiJingLing();
	//更新道馆主界面挑战boss增益精灵
	DaoGuanMainBossPopPanel.UpdataZengYiJing();
}
//获取当天boss伤害总量排行
GuildPart.TodayDamageRank = function() {
	var req = new  guildboss_msg_pb.GuildPart_TodayDamageRankRequest();
	SocketClient.callServerFun("GuildPart_TodayDamageRank", req);
}
GuildPart.TodayDamageRankResp = function(buffer) {
	var ans = guildboss_msg_pb.GuildPart_TodayDamageRankResponse.decode(buffer);

	var myrank = 0;
	var myDamage = 0;
	ans.DamageList.sort(DamageComp);
	for (var i = 0; i < ans.DamageList.length; ++i) {
		var item = ans.DamageList[i];
		
		if ((item.ObjID == PlayerBasePart.ObjID)) {
			myrank = i;
			myDamage = item.Damage;
			break;
		}
		
	}
	DaoGuanBossRankPopPanel.Show(ans.DamageList, myrank, myDamage);
}
//获取公会击杀boss排行
GuildPart.KilledRank = function() {
	var req = new  guildboss_msg_pb.GuildPart_KilledRankRequest();
	SocketClient.callServerFun("GuildPart_KilledRank", req);
	ViewManager.showLoading({});
}
GuildPart.KilledRankResp = function(buffer) {
	ViewManager.hideLoading();
	var ans = guildboss_msg_pb.GuildPart_KilledRankResponse.decode(buffer);

	//击杀排行榜
	RankPanel.ShowDaoGuan(ans);
}
//获取捐献特需卡
GuildPart.GetDoubleCards = function() {
	var req = new  guildboss_msg_pb.GuildPart_DoubleCardsRequest();
	SocketClient.callServerFun("GuildPart_DoubleCards", req);
}
GuildPart.DoubleCardsResp = function(buffer) {
	var ans = guildboss_msg_pb.GuildPart_DoubleCardsResponse.decode(buffer);

	this.DoubleCards = ans.DoubleCards;
}
//获取捐献日志
GuildPart.DonateLog = function(buildid) {
	var req = new  guildboss_msg_pb.GuildPart_DonateLogRequest();
	req.BuildID = buildid;
	SocketClient.callServerFun("GuildPart_DonateLog", req);
	ViewManager.showLoading({});
}
GuildPart.DonateLogResp = function(buffer) {
	ViewManager.hideLoading();
	var ans = guildboss_msg_pb.GuildPart_DonateLogResponse.decode(buffer);

	//捐献日志界面
	DaoGuanJXNotePopPanel.Show(ans);
}
//周伤害排名规则
GuildPart.WeekDamageComp = function(m1, m2) {
	if (BigNumber.equalTo(m1.Damage, m2.Damage)) {
		return (m1.Level > m2.Level);
	}
	return BigNumber.greaterThan(m1.Damage, m2.Damage);
}
//获取周伤害排名
GuildPart.WeekDamageRank = function() {
	var req = new  guildboss_msg_pb.GuildPart_WeekDamageRankRequest();
	SocketClient.callServerFun("GuildPart_WeekDamageRank", req);
}
GuildPart.WeekDamageRankResp = function(buffer) {
	var ans = guildboss_msg_pb.GuildPart_WeekDamageRankResponse.decode(buffer);

	var myrank = 0;
	var myDamage = 0;
	var myReward = { };
	for (var i = 0; i < ans.RankList.length; ++i) {
		var item = ans.RankList[i];
		
		if ((item.Nick == PlayerBasePart.StrAttr[PlayerStrAttr.PlayerStrAttr_MingZi])) {
			myrank = i;
			myDamage = item.Damage;
			myReward = item.Awards;
			break;
		}
		
	}
	DaoGuanBossRankPopPanel.Show(ans.RankList, myrank, myDamage, myReward);
}
//转换为工具识别的数组
GuildPart.ChangeGuildAward = function(awardsArr) {
	var resTypeArr = { };
	var resIDArr = { };
	var resCountArr = { };
	if (((awardsArr == null) || (awardsArr.length == 0))) {
		return resTypeArr, resIDArr, resCountArr;
	}
	for (var i = 0; i != awardsArr.length; ++i) {
		resTypeArr[i] = awardsArr[i].ItemType;
		resIDArr[i] = awardsArr[i].ItemID;
		resCountArr[i] = awardsArr[i].ItemNum;
	}
	return resTypeArr, resIDArr, resCountArr;
}
//道馆界面
GuildPart.DaoGuanShow = function() {
	if ((GuildPart.GuildID == '')) {
		//道馆链表
		GuildPart.GetGuildList();
	} else {
		//道馆主界面
		GuildPart.GetGuildInfo(1);
	}
}
//获取道馆等级折算的战斗等级
GuildPart.GetFightLevel = function() {
	var level = GuildPart.GuildLevel;
	var conf = LoadConfig.getConfigData(ConfigName.ZheSuan_DaoGuan,level);
	if ((conf == null)) {
		return 0;
	}
	return conf.Level;
}
//获取出站红点
GuildPart.GetRedState = function() {
	//如果有一个没有上满，并且有可上的，就显示红点GuildPart.AlternateEvles()
	var [canUp,all] = GuildPart.GetCanUsePokemen();
	var count = 0;
	var countAll = 0;
	for (var key in canUp) {
		
		var v = canUp[key];
		{
			count = (count + v);
		}
	}
	for (var key in all) {
		
		var v = all[key];
		{
			countAll = (countAll + v);
		}
	}
	//光环队
	if (((GuildPart.GainElves.length < 5) && (count > 0))) {
		return true;
	}
	var canShuData = CanShuTool.GetCanShuData(CanShuTool.CanShuEnum.DaoGuanZhiYuanDui);
	var openLv = canShuData.Param;
	//5个队伍
	for (var i = 0; i != GuildPart.Lineup.length; ++i) {
		var line = GuildPart.Lineup[i];
		//并且玩家最高通关数大于队伍开启条件
		var topRound = 0;
		if ((i > 1)) {
			topRound = openLv[(i - 1)];
		}
		if ((((line.length < 12) && (countAll > 0)) && (GuanQiaPart.HistoryTop >= topRound))) {
			return true;
		}
	}
	return false;
}
window.GuildPart = GuildPart;