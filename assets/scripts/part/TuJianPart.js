//region *.lua
//Date
//此文件由[BabeLua]插件自动生成
let tujian_msg_pb = require('tujian_msg').msg;
let TuJianPart = { };

TuJianPart.TotalCardIDs = { };
TuJianPart.TotalEquipIDs = { };
TuJianPart.CardRewardState = { };
TuJianPart.EquipRewardState = { };
//登录下发已收集卡牌
TuJianPart.RecvLoginData = function(buffer) {
	var msg = tujian_msg_pb.TuJianPart_LoginDatas.decode(buffer);

	TuJianPart.TotalCardIDs = { };
	TuJianPart.TotalEquipIDs = { };
	TuJianPart.TotalCardIDs = msg.TotalCardIDs;
	TuJianPart.TotalEquipIDs = msg.TotalEquipIDs;
}
//图鉴数据请求
TuJianPart.AllDataRequest = function() {
	var request = new  tujian_msg_pb.TuJianPart_TuJianDataRequest();
	SocketClient.callServerFun('TuJianPart_RequestTuJianData', request);
	ViewManager.showLoading({});
}
//图鉴数据回应
TuJianPart.AllDataResponse = function(buffer) {
	ViewManager.hideLoading();
	var msg = tujian_msg_pb.TuJianPart_TuJianDataResponse.decode(buffer);

	TuJianPart.TotalCardIDs = { };
	TuJianPart.TotalEquipIDs = { };
	TuJianPart.TotalCardIDs = msg.TotalCardIDs;
	TuJianPart.TotalEquipIDs = msg.TotalEquipIDs;
	TuJianPart.CardRewardState = { };
	TuJianPart.EquipRewardState = { };
	//初始化卡牌奖励
	var index = 1;
	let Conf_TuJianJingLingShouJi = LoadConfig.getConfig(ConfigName.TuJianJingLingShouJi);
	for (var id in Conf_TuJianJingLingShouJi) {
		
		TuJianPart.CardRewardState[index] = { };
		TuJianPart.CardRewardState[index].ConfID = id;
		//将服务器数据赋值给前端
		var isGet = false;
		for (var i = 0; i != msg.CardRewardGetList.length; ++i) {
			if ((msg.CardRewardGetList[i] == id)) {
				isGet = true;
				TuJianPart.CardRewardState[index].State = 2;
				TuJianPart.CardRewardState[index].SortID = 2;
				break;
			}
		}
		if (!isGet) {
			if ((TuJianPart.GetCollectedCardCount() >= id)) {
				TuJianPart.CardRewardState[index].State = 1;
				TuJianPart.CardRewardState[index].SortID = 0;
			} else {
				TuJianPart.CardRewardState[index].State = 0;
				TuJianPart.CardRewardState[index].SortID = 1;
			}
		}
		index = (index + 1);
		
	}
	//初始化装备奖励
	var index = 1;
	let Conf_TuJianZhuangBeiShouJi = LoadConfig.getConfig(ConfigName.TuJianZhuangBeiShouJi);
	for (var id in Conf_TuJianZhuangBeiShouJi) {
		
		TuJianPart.EquipRewardState[index] = { };
		TuJianPart.EquipRewardState[index].ConfID = id;
		//将服务器数据赋值给前端
		var isGet = false;
		for (var i = 0; i != msg.EquipRewardGetList.length; ++i) {
			if ((msg.EquipRewardGetList[i] == id)) {
				isGet = true;
				TuJianPart.EquipRewardState[index].State = 2;
				TuJianPart.EquipRewardState[index].SortID = 2;
				break;
			}
		}
		if (!isGet) {
			if ((TuJianPart.GetCollectedEquipCount() >= id)) {
				TuJianPart.EquipRewardState[index].State = 1;
				TuJianPart.EquipRewardState[index].SortID = 0;
			} else {
				TuJianPart.EquipRewardState[index].State = 0;
				TuJianPart.EquipRewardState[index].SortID = 1;
			}
		}
		index = (index + 1);
	}
	
	//刷新界面
	PokedexPanel.ShowAfterRequest();
}
//奖励排序
TuJianPart.SortReward = function(a, b) {
	var aReward = a;
	var bReward = b;
	if ((aReward.SortID != bReward.SortID)) {
		return (aReward.SortID < bReward.SortID);
	}
	if ((aReward.ConfID != bReward.ConfID)) {
		return (aReward.ConfID < bReward.ConfID);
	}
}
var getType = 0;
var getID = 0;
//领取奖励请求
TuJianPart.GetRewardRequest = function(rewardType, rewardID) {
	var request = new  tujian_msg_pb.TuJianPart_GetRewardRequest();
	request.RewardType = rewardType;
	request.RewardID = rewardID;
	getType = rewardType;
	getID = rewardID;
	SocketClient.callServerFun('TuJianPart_GetReward', request);
	ViewManager.showLoading({});
}
//领取奖励回应
TuJianPart.GetRewardResponse = function(buffer) {
	ViewManager.hideLoading();
	var msg = tujian_msg_pb.TuJianPart_GetRewardResponse.decode(buffer);

	if ((msg.Flag != 0)) {
		return;
	}
	var rewardData = null;
	//领取成功，显示奖励，修改状态
	if ((getType == PokedexPanel.HighLightType.Hero)) {
		rewardData = LoadConfig.getConfigData(ConfigName.TuJianJingLingShouJi,getID);
		for (var i = 0; i != TuJianPart.CardRewardState.length; ++i) {
			if ((TuJianPart.CardRewardState[i].ConfID == getID)) {
				TuJianPart.CardRewardState[i].State = 2;
				TuJianPart.CardRewardState[i].SortID = 2;
				break;
			}
		}
	} else if ((getType == PokedexPanel.HighLightType.Equip) ){
		rewardData = LoadConfig.getConfigData(ConfigName.TuJianZhuangBeiShouJi,getID);
		for (var i = 0; i != TuJianPart.EquipRewardState.length; ++i) {
			if ((TuJianPart.EquipRewardState[i].ConfID == getID)) {
				TuJianPart.EquipRewardState[i].State = 2;
				TuJianPart.EquipRewardState[i].SortID = 2;
				break;
			}
		}
	}
	var rewardType = { };
	var rewardID = { };
	var rewardCount = { };
	for (var i = 0; i != rewardData.ResourceType.length; ++i) {
		if ((rewardData.ResourceType[i] == 0)) {
			break;
		}
		rewardType[i] = rewardData.ResourceType[i];
		rewardID[i] = rewardData.ResourceID[i];
		rewardCount[i] = rewardData.ResourceCount[i];
	}
	HuoDeWuPinTiShiPanelData.Show(rewardType, rewardID, rewardCount);
	//刷新界面
	PokedexPanel.RefreshRewardPopPanel();
}
//判断某张卡是否收集
TuJianPart.IsCardCollect = function(cardID) {
	var isCollect = false;
	for (var i = 0; i != TuJianPart.TotalCardIDs.length; ++i) {
		if ((cardID == TuJianPart.TotalCardIDs[i])) {
			isCollect = true;
			break;
		}
	}
	return isCollect;
}
//判断某装备是否收集
TuJianPart.IsEquipCollect = function(equipID) {
	var isCollect = false;
	for (var i = 0; i != TuJianPart.TotalEquipIDs.length; ++i) {
		if ((equipID == TuJianPart.TotalEquipIDs[i])) {
			isCollect = true;
			break;
		}
	}
	return isCollect;
}
//获取当前已收集的所有卡牌数量
TuJianPart.GetCollectedCardCount = function() {
	return TuJianPart.TotalCardIDs.length;
}
//获取当前已收集的所有装备数量
TuJianPart.GetCollectedEquipCount = function() {
	return TuJianPart.TotalEquipIDs.length;
}
//endregion
window.TuJianPart = TuJianPart;