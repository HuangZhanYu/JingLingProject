let active_msg_pb = require('active_msg').msg;
let activity_msg_pb = require('activity_msg').msg;

//活动部件
//活动任务状态
const TaskType = {
	TaskType_No: 0,
	//未完成
	TaskType_CanGet: 1,
	//可领取
	TaskType_HaveGet: 2,
	//已领取

};
let ActivePart = { };

//限时活动类型
ActivePart.LimitType = {
	ActivePart_DataType_Rank: 0,
	//冲榜活动
	ActivePart_DataType_TotalCharge: 1,
	//累充活动
	ActivePart_DataType_LimitRank: 2,
	//钻石消耗排行
	ActivePart_DataType_OnceCharge: 3,
	//单充活动
	ActivePart_DataType_CostReward: 4,
	//消耗奖励活动
	ActivePart_DataType_Partner: 5,
	//限时伙伴
	ActivePart_DataType_Contract: 6,
	//精灵契约
	ActivePaty_DataType_ContractEquip: 7,
	//套装守护
	ActivePart_DataType_PointsShop: 8,
	//积分商店
	ActivePart_DataType_DayRecharge: 9,
	//每日充值
	ActivePart_DataType_DailyWelfare: 10,
	//每日福利
	ActivePart_DataType_DiamondCostReward: 11,
	//钻石消耗奖励

};
ActivePart.ActivePopEnum = {
	ShouChongLiBao: 1,
	//首充礼包
	YueKa: 2,
	//月卡
	JiJin: 3,
	//基金
	MeiRiRenWu: 4,
	//每日任务
	DengLuLiBao: 5,
	//等级礼包
	ShaoKaoPaiDui: 6,
	//烧烤派对
	Max: 7,
	//最大值，用来做循环的遍历

};
ActivePart.SignProgress = 0;
//签到进度 0为第一天
ActivePart.HaveSign = false;
//今天是否已经签到
ActivePart.LimitList = { };
//限时冲榜活动冲榜按钮数据
ActivePart.IsActiveRedPointList = { };
//首次登录精彩活动红点初始化
//接收服务器的活动数据
ActivePart.OnReciveData = function(buffer) {
	var msg = active_msg_pb.SignActiveData.decode(buffer);
	ActivePart.SignProgress = msg.SignProgress;
	ActivePart.HaveSign = msg.HaveSign;
}
//发送签到请求给服务器
ActivePart.SendSignRequest = function() {
	//判断今天是否已经签到过
	if (ActivePart.HaveSign) {
		return;
	}
	//调用服务器签到功能
	var request = new active_msg_pb.SignRequest();
	SocketClient.callServerFun("SignActivePart_SignRequest", request);
	ViewManager.showLoading({});
}
//接收服务器的活动数据
ActivePart.SignReturn = function(buffer) {
	ViewManager.hideLoading();
	var msg = active_msg_pb.SignResponse.decode(buffer);

	ActivePart.SignProgress = msg.SignProgress;
	ActivePart.HaveSign = msg.HaveSign;
	var data = LoadConfig.getConfigData(ConfigName.Huodong_DengLuJiangLi,ActivePart.SignProgress);
	if ((data == null)) {
		console.error(("活动登录数据表为空，此ID：" + (ActivePart.SignProgress)));
		return;
	}
	//刷新登录奖励
	//DengLuHuoDongPanel.SetInfo();
	//DengLuHuoDongPanel.SetState();
	//AuditLog1stPopPanel.SetState();
	//判断主界面按钮是否有登录活动红点
	//MainPanel.IsDengLuHongDian();
	//获得奖励界面
	var resType = { };
	var resID = { };
	var resCount = { };
	for (var i = 0; i != data.ResourceType.length; ++i) {
		if ((data.ResourceType[i] <= 0)) {
			break;
		}
		resType[(resType.length + 1)] = data.ResourceType[i];
		resID[(resID.length + 1)] = data.ResourceID[i];
		resCount[(resCount.length + 1)] = data.ResourceCount[i];
	}
	//HuoDeWuPinTiShiPanelData.Show(resType, resID, resCount);
}
//请求服务器限时活动按钮列表
ActivePart.OnLimitBtnData = function() {
	var msg = new active_msg_pb.ActiveLimitDataRequest();
	SocketClient.callServerFun("SignActivePart_GetLimitListRequest", msg);
}
//接收服务器限时活动按钮列表返回
ActivePart.ReciveLimitList = function(buffer) {
	var msg = active_msg_pb.LimitListResponse.decode(buffer);
	ActivePart.LimitList = { };
	ActivePart.LimitList = msg.List;
}
//限时活动数据请求
ActivePart.OnLimitActiveDataRequest = function(id) {
	var msg = new active_msg_pb.ActiveLimitDataRequest();
	msg.ID = id;
	SocketClient.callServerFun("SignActivePart_GetLimitDataRequest", msg);
	ViewManager.showLoading({});
}
//活动红点相关
ActivePart.OnReciveRedPointData = function(buffer) {
	var msg = activity_msg_pb.ActiveRedPointResponse.decode(buffer);
	for (var i = 0; i != (ActivePart.ActivePopEnum.Max - 1); ++i) {
		ActivePart.IsActiveRedPointList[i] = false;
	}
	for (var i = 0; i != msg.List.length; ++i) {
		var index = msg.List[i];
		ActivePart.IsActiveRedPointList[(index + 1)] = true;
	}
}
//点击活动按钮修改当前红点状态
ActivePart.CurDayActiveRedPointState = function(index) {
	//没有这个状态，或者已经是false的情况下，不修改也不上传
	if (((ActivePart.IsActiveRedPointList[index] == null) || (ActivePart.IsActiveRedPointList[index] == false))) {
		return;
	}
	ActivePart.IsActiveRedPointList[index] = false;
	var msg = new activity_msg_pb.ActiveRedPointChangeRequest();
	msg.Index = (index - 1);
	SocketClient.callServerFun("ActivePart_OnRedPointChange", msg);
}
//获取主界面的精彩活动的另一个红点判断
ActivePart.IsMainIconRed = function() {
	for (var i = 0; i != (ActivePart.ActivePopEnum.Max - 1); ++i) {
		if (ActivePart.IsActiveRedPointList[i]) {
			return true;
		}
	}
	return false;
}
ActivePart.OnRecLevelGiftData = function(buffer)
{	

}
ActivePart.OnFirstRechargeConf  = function(buffer)
{	

}
ActivePart.OnMoonCardConf  = function(buffer)
{	

}
ActivePart.OnFundConf  = function(buffer)
{	

}
ActivePart.OnRecvVipGiftData  = function(buffer)
{	

}
ActivePart.OnRecLevelGiftData  = function(buffer)
{	

}
ActivePart.OnBarbecuePartyConf  = function(buffer)
{	

}
ActivePart.OnDayAllDayTaskInfoData  = function(buffer)
{	

}
// ActivePart.OnRecLevelGiftData  = function(buffer)
// {	

// }
// ActivePart.OnRecLevelGiftData  = function(buffer)
// {	

// }
window.ActivePart = ActivePart;