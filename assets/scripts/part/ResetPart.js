//region *.lua
//Date
//此文件由[BabeLua]插件自动生成
import { ResourceTypeEnum } from '../tool/ResourceTool';

let reset_msg_pb = require('reset_msg').msg;

let ResetPart = { };

ResetPart.Result = {
	Success: 0,
	//时间间隔内不能再次进行灵魂休息
	TimeError: 1,
	//正在进行灵魂休息中
	IsReseting: 2,
	//还没开始灵魂休息
	IsNotReseting: 3,
	//还没开始灵魂休息
	FinishFast: 4,
	//60秒内结束灵魂休息
	CostError: 5,
	//钻石不足
	OpenError: 6,
	//至少需要一次复活

};
ResetPart.LstResetTime = 0;
// 上次灵魂休息结束时间
ResetPart.LstStartTime = 0;
// 上次灵魂休息开始时间
ResetPart.IsReseting = false;
// 是否正在进行灵魂休息
ResetPart.GetPerMin = "0";
// 每分钟获得
ResetPart.TodayBuyCount = 0;
// 今日已经购买的缩短时间次数
//登陆下发
ResetPart.OnLoginResponse = function(buffer) {
	var msg = reset_msg_pb.ResetPart_LoginResponse.decode(buffer);

	ResetPart.LstResetTime = parseInt(msg.LstResetTime);
	ResetPart.LstStartTime = parseInt(msg.LstStartTime);
	ResetPart.IsReseting = msg.IsReseting;
	ResetPart.GetPerMin = msg.GetPerMin;
	ResetPart.TodayBuyCount = msg.TodayCount;
	//清除灵魂休息定时器
	//timerMgr.RemoveTimerEvent("ResetPart.CWrefreshTime");
	//判断当前是否正在灵魂休息，并刷新界面
	if (ResetPart.IsReseting) {
		ResetPart.CountDownTime();
	}
}
//登陆下发
ResetPart.OnUpdateResponse = function(buffer) {
	var msg = reset_msg_pb.ResetPart_LoginResponse.decode(buffer);

	ResetPart.LstResetTime = parseInt(msg.LstResetTime);
	ResetPart.LstStartTime = parseInt(msg.LstStartTime);
	ResetPart.IsReseting = msg.IsReseting;
	ResetPart.GetPerMin = msg.GetPerMin;
	ResetPart.TodayBuyCount = msg.TodayCount;
}
//开始请求
ResetPart.OnStartRequest = function() {
	var request = new  reset_msg_pb.ResetPart_StartRequest();
	SocketClient.callServerFun('ResetPart_Start', request);
}
//开始请求返回
ResetPart.OnStartResponse = function(buffer) {
	var msg = reset_msg_pb.ResetPart_StartResponse.decode(buffer);

	if ((msg.Success != ResetPart.Result.Success)) {
		ResetPart.ShowErrorTips(msg.Success);
		return;
	}
	var data = msg.Data;
	ResetPart.LstResetTime = parseInt(data.LstResetTime);
	ResetPart.LstStartTime = parseInt(data.LstStartTime);
	ResetPart.IsReseting = data.IsReseting;
	ResetPart.GetPerMin = data.GetPerMin;
	ResetPart.TodayBuyCount = data.TodayCount;
	//刷新界面
	MainPanel.SetSoulOnOff(true);
	LingHuningPopPanel.Show();
	//判断当前是否正在灵魂休息，并刷新界面
	if (ResetPart.IsReseting) {
		ResetPart.CountDownTime();
	}
	if ((FightBehaviorManager.mFightType == FightSystemType.GuanQia)) {
		FightBehaviorManager.StopFight();
	}
}
//每分钟刷新一下界面
ResetPart.CountDownTime = function() {
	//timerMgr.RemoveTimerEvent("ResetPart.CWrefreshTime");
	//timerMgr.AddTimerEvent('ResetPart.CWrefreshTime', 62, ResetPart.CWrefreshTime, { }, false);
}
//刷新信息
ResetPart.CWrefreshTime = function() {
	if (LingHuningPopPanel.IsShow()) {
		LingHuningPopPanel.SetInfo();
	}
	//已经休息的时间
	var countDownSec = (TimePart.GetUnix() - ResetPart.LstStartTime);
	if ((countDownSec <= 0)) {
		countDownSec = 0;
	}
	var Integer = Math.ceil(countDownSec);
	if ((parseInt(Integer) >= 60)) {
		MainPanel.UpdateSoulTime(TimeTool_TransformTime(Integer));
	} else {
		MainPanel.UpdateSoulTime(TimeTool_TransformTime(0));
	}
	ResetPart.CountDownTime();
}
//结束请求
//flag 是否双倍，bool
ResetPart.OnFinishRequest = function(flag) {
	var request = new  reset_msg_pb.ResetPart_FinishRequest();
	request.Double = flag;
	SocketClient.callServerFun('ResetPart_Finish', request);
}
//结束请求返回
ResetPart.OnFinishResponse = function(buffer) {
	var msg = reset_msg_pb.ResetPart_FinishResponse.decode(buffer);

	if (((msg.Success != ResetPart.Result.Success) && (msg.Success != ResetPart.Result.FinishFast))) {
		ResetPart.ShowErrorTips(msg.Success);
		return;
	}
	var data = msg.Data;
	ResetPart.LstResetTime = parseInt(data.LstResetTime);
	ResetPart.LstStartTime = parseInt(data.LstStartTime);
	ResetPart.IsReseting = data.IsReseting;
	ResetPart.GetPerMin = data.GetPerMin;
	ResetPart.TodayBuyCount = data.TodayCount;
	if ((msg.Success == ResetPart.Result.FinishFast)) {
		ResetPart.ShowErrorTips(msg.Success);
	} else {
		var reward = msg.Reward;
		//timerMgr.RemoveTimerEvent("ResetPart.CWrefreshTime");
		HuoDeWuPinTiShiPanelData.Show([
				ResourceTypeEnum.ResType_HuoBi
			], [
				3
			], [
				reward
			]);
		
	}
	//刷新界面
	MainPanel.SetSoulOnOff(false);
	//恢复主线战斗
	GuanQiaPart.Resume();
}
//缩短请求
ResetPart.OnReduceTimeRequest = function() {
	var request = new  reset_msg_pb.ResetPart_ReduceTimeRequest();
	SocketClient.callServerFun('ResetPart_ReduceTime', request);
}
//缩短请求返回
ResetPart.OnReduceTimeResponse = function(buffer) {
	var msg = reset_msg_pb.ResetPart_ReduceTimeResponse.decode(buffer);

	if ((msg.Success != ResetPart.Result.Success)) {
		ResetPart.ShowErrorTips(msg.Success);
		return;
	}
	var data = msg.Data;
	ResetPart.LstResetTime = parseInt(data.LstResetTime);
	ResetPart.LstStartTime = parseInt(data.LstStartTime);
	ResetPart.IsReseting = data.IsReseting;
	ResetPart.GetPerMin = data.GetPerMin;
	ResetPart.TodayBuyCount = data.TodayCount;
	//刷新界面
	LingHunXiuXiPopPanel.Show();
}
// 显示错误信息
ResetPart.ShowErrorTips = function(err) {
	if ((err == ResetPart.Result.TimeError)) {
		ToolTipPopPanel.Show(277);
	} else if ((err == ResetPart.Result.IsReseting) ){
		ToolTipPopPanel.Show(278);
	} else if ((err == ResetPart.Result.IsNotReseting) ){
		ToolTipPopPanel.Show(279);
	} else if ((err == ResetPart.Result.FinishFast) ){
		ToolTipPopPanel.Show(280);
	} else if ((err == ResetPart.Result.CostError) ){
		ToolTipPopPanel.Show(281);
	} else if ((err == ResetPart.Result.OpenError) ){
		ToolTipPopPanel.Show(282);
	}
}
//endregion
window.ResetPart = ResetPart;