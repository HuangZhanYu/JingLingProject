

let question_msg_pb = require('question_msg').msg;
//问卷系统部件
//功能描述：用于收集玩家的游戏反馈
//定义部件
let QuestionPart = { };

//QuestionPart.Max_Quistion_Num = LoadConfig.getConfig(ConfigName.WenJuan).length;
//最大问题数量
QuestionPart.CureQuestion = 1;
//当前问题ID
//接收服务器问题数据
QuestionPart.OnRecvQuestionData = function(buffer) {
	//解析消息
	var msg = question_msg_pb.PlayerQuestionData.decode(buffer);

	QuestionPart.CureQuestion = msg.CureQuestion;
}
//提交问题答案
QuestionPart.SendAnswer = function(answer) {
	//发送消息给服务器
	var request = new  question_msg_pb.PlayerAnswerRequest();
	request.QuestionID = QuestionPart.CureQuestion;
	for (var i = 0; i != answer.length; ++i) {
		request.Answer.push(answer[i]);
	}
	ViewManager.showLoading({});
	SocketClient.callServerFun("PlayerQuestionPart_AnswerRequest", request);
}

//提交答案完毕
QuestionPart.OnRecvAnswer = function(buffer) {
	//解析消息
	var msg = question_msg_pb.PlayerQuestionData();

	ViewManager.hideLoading();
	QuestionPart.CureQuestion = msg.CureQuestion;
	//刷新界面
	QuestionPanel.AfterInfo();
	var resTyp = { };
	var resID = { };
	var resCount = { };
	let Conf_WenJuan = LoadConfig.getConfig(ConfigName.WenJuan);
	//判断当前是否回答全部问题
	if ((QuestionPart.CureQuestion == (Conf_WenJuan.length + 1))) {
		var data = LoadConfig.getConfigData(ConfigName.WenJuan,Conf_WenJuan.length);
		for (var i = 0; i != data.RewardsType.length; ++i) {
			if ((data.RewardsType[i] <= 0)) {
				break;
			}
			resTyp[(resTyp.length + 1)] = data.RewardsType[i];
			resID[(resID.length + 1)] = data.RewardsID[i];
			resCount[(resCount.length + 1)] = data.RewardsCount[i];
		}
		HuoDeWuPinTiShiPanelData.Show(resTyp, resID, resCount);
		return;
	}
}
window.QuestionPart = QuestionPart;