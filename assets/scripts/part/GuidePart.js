//region GuidePart.lua
//Date 20170701
//GuidePart 引导部件
let guide_msg_pb = require('guide_msg').msg;
let GuidePart = { };

GuidePart.GuideFight = false;
//触发一个引导
GuidePart.OnTriggerGuide = function(buffer) {
	var msg = guide_msg_pb.GuidePart_TriggerResponse.decode(buffer);

	//清除前端的当前引导
	// GuideManager.CurGuideID = 0;
	// GuideManager.CurGuideStepID = 0;
	// var guideID = msg.GuideID;
	// GuideManager.OnGuideTrigger(guideID);
}
//完成一个引导步骤
//参数1，guideID 引导ID
//参数2，stepID 步骤ID
GuidePart.OnPass = function(guideID, stepID) {
	var request = new  guide_msg_pb.GuidePart_PassRequest();
	request.GuideID = guideID;
	request.StepID = stepID;
	SocketClient.callServerFun("GuidePart_OnPassRequest", request);
}
//完成一个引导
//参数1，guideID 引导ID
GuidePart.OnFinish = function(guideID, stepID) {
	var request = new  guide_msg_pb.GuidePart_FinishRequest();
	request.GuideID = guideID;
	request.StepID = stepID;
	SocketClient.callServerFun("GuidePart_OnFinishRequest", request);
}
//触发一个引导
GuidePart.OnUpdateGuideFight = function(buffer) {
	var msg = guide_msg_pb.GuidePart_UpdateGuideFightResponse.decode(buffer);

	GuidePart.GuideFight = msg.GuideFight;
}
//完成一个引导步骤
//参数1，guideID 引导ID
//参数2，stepID 步骤ID
GuidePart.OnFinishGuideFight = function() {
	GuidePart.GuideFight = true;
	var request = new  guide_msg_pb.GuidePart_FinishGuideFightRequest();
	SocketClient.callServerFun("GuidePart_OnFinishGuideFight", request);
}
//endregion
window.GuidePart = GuidePart;