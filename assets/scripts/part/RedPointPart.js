let hongdian_pb = require('hongdian').msg;

import GOTool from '../tool/GOTool';

let RedPointPart = { };

RedPointPart.Enum = //主界面枚举
{
	Main_Friend: 1,
	//主界面好友
	Main_LoginActivity: 2,
	//主界面登录活动
	Main_Mail: 3,
	//主界面邮件
	Main_Fetters: 4,
	//主界面羁绊
	Main_Adventure: 5,
	//主界面冒险
	//好友枚举
	Friend_Request: 6,
	//好友申请
	Friend_NewMessage: 7,
	//好友有新消息
	//羁绊相关
	Fetters_PageOne: 8,
	//页签1红点
	Fetters_PageTwo: 9,
	//页签2红点
	Fetters_PageThree: 10,
	//页签3红点
	Fetters_PageFour: 11,
	//页签4红点
	Fetters_BoxOne: 12,
	//宝箱1红点
	Fetters_BoxTwo: 13,
	//宝箱2红点
	Fetters_BoxThree: 14,
	//宝箱3红点
	Fetters_BoxFour: 15,
	//宝箱4红点
	//活动相关
	RedPoint_Type_Activity_MoonCard: 16,
	//月卡红点
	RedPoint_Type_Activity_FirstRecharge: 17,
	//首充红点
	RedPoint_Type_Activity_Fund: 18,
	//基金红点
	RedPoint_Type_Activity_NormalFund: 19,
	//普通基金红点
	RedPoint_Type_Activity_LuxuryFund: 20,
	//豪华基金红点
	RedPoint_Type_Activity_VipGift: 21,
	//vip礼包红点
	//商店红点
	Shop_FreshFull: 22,
	//商店红点
	//公会红点
	RedPoint_Type_Guild_Sign: 23,
	//签到	
	RedPoint_Type_Guild_FightBoss: 24,
	//打boss
	RedPoint_Type_Guild_CardAwards: 25,
	//训练卡奖励
	RedPoint_Type_Guild_NoticeChanged: 26,
	//公告
	RedPoint_Type_Guild_NewApply: 27,
	//新的入会申请
	RedPoint_Type_Guild: 28,
	//公会主界面
	Main_LimitActivity: 29,
	//主界面限时活动
	Activity_OnceRecharge: 30,
	//单充红点
	Main_OffenUse: 31,
	//主界面常用
	Main_Chat: 32,
	//主界面聊天
	//图鉴相关
	RedPoint_Type_TuJian: 33,
	//图鉴按钮红点
	RedPoint_Type_TuJian_Card: 34,
	//图鉴卡牌奖励红点
	RedPoint_Type_TuJian_Equip: 35,
	//图鉴装备奖励红点
	Activity_DailyWelfare: 36,
	//每日福利
	Activity_CostReward: 37,
	//消耗奖励
	RedPoint_Type_Adventure_MegaEvo: 38,
	//冒险界面百万进化
	//道馆战红点
	RedPoint_Type_Landgrave_IdleOrAward: 39,
	//有空闲领地 或者可领取奖励
	RedPoint_Type_Landgrave_Revive: 40,
	//复活队伍红点
	RedPoint_Type_Landgrave_FightRecord: 41,
	//战报红点
	RedPoint_Type_Landgrave: 42,
	//全局红点
	Main_Sign: 43,
	//主界面签到
	RedPoint_Type_DayTask: 44,
	//每日任务红点
	RedPoint_Type_LineUp_TeamAttr: 45,
	//队伍界面的训练大师按钮红点
	RedPoint_Type_Main_Revive_Reward: 46,
	//主界面重置红点，只要有通关奖励可领取的时候就显示
	RedPoint_Type_MainBtn_Plateau: 47,
	//精通塔红点（也叫精灵高原)，只要有挑战次数就显示
	RedPoint_Type_Activity_Elf_Party: 48,
	//精彩活动烧烤派对红点，没进去过或在活动时间内有奖励可领取显示红点

};
class RedPointData{
	constructor(enumType)
	{
		this.Enum = enumType;
		this.Active = { };
	}
	Set(enumType, active)
	{
		this.Active[enumType] = active;
	}
	GetActive()
	{
		for (var active in this.Active) {
			
			if (active) {
				return true;
			}
			
		}
		return false;
	}
};

RedPointPart.TotalHongDianObj = { };
RedPointPart.TotalHongDianRelateData = { };
RedPointPart.OnRelateReturn = function(buffer) {
	var msg = hongdian_pb.HongDianRelateResponse.decode(buffer);

	for (let i = 0; i != msg.Data.length; ++i) {
		var data = msg.Data[i];
		var enumType = data.Enum;
		// 转换成前端数据
		var rpData = new RedPointData(enumType);
		for (let j = 0; j < data.Active.length; ++j) {
			var active = data.Active[j];
			var en = data.RelateEnum[j];
			rpData.Set(en, active);
		}
		RedPointPart.TotalHongDianRelateData[enumType] = rpData;
		var acitve = RedPointPart.GetActive(enumType);
		RedPointPart.SetObjActive(enumType, acitve);
	}
}
//获取红点信息的显隐信息
//参数1 RedPointPart.Enum
RedPointPart.GetActive = function(enumType) {
	var rpData = RedPointPart.TotalHongDianRelateData[enumType];
	if ((rpData == null)) {
		return false;
	}
	return rpData.GetActive();
}
//设置红点显隐
RedPointPart.SetObjActive = function(enumType, active) {
	var objs = RedPointPart.TotalHongDianObj[enumType];
	if ((objs != null)) {
		for (var i = 0; i != objs.length; ++i) {
			var obj = objs[i];
			if ((obj != null) && (!GOTool.IsDestroy(obj))) {
				if (typeof(active) == 'boolean') {
					obj.SetActive(active);
				} else {
					obj.SetActive(false);
				}
			}
		}
	}
}
//注册红点Obj
//参数1，RedPointPart.Enum
//参数2，红点Obj
RedPointPart.Register = function(enumType, obj) {
	var objs = RedPointPart.TotalHongDianObj[enumType];
	if ((objs == null)) {
		var newObjs = { };
		newObjs[(newObjs.length + 1)] = obj;
		RedPointPart.TotalHongDianObj[enumType] = newObjs;
	} else {
		objs[(objs.length + 1)] = obj;
		RedPointPart.TotalHongDianObj[enumType] = objs;
	}
	//注册后立即对红点进行设置
	var active = RedPointPart.GetActive(enumType);
	RedPointPart.SetObjActive(enumType, active);
}
//前端设置某个红点枚举是否激活
RedPointPart.SetEnumActive = function(enumType, active, relateEnum1, relateEnum2, relateEnum3, relateEnum4, relateEnum5, relateEnum6) {
	var data = RedPointPart.TotalHongDianRelateData[enumType];
	if ((data == null)) {
		data = new RedPointData(enumType);
	}
	data.Set(enumType, active);
	RedPointPart.TotalHongDianRelateData[enumType] = data;
	//关联的红点
	RedPointPart.SetRelateEnumActive(relateEnum1, active, enumType);
	RedPointPart.SetRelateEnumActive(relateEnum2, active, enumType);
	RedPointPart.SetRelateEnumActive(relateEnum3, active, enumType);
	RedPointPart.SetRelateEnumActive(relateEnum4, active, enumType);
	RedPointPart.SetRelateEnumActive(relateEnum5, active, enumType);
	RedPointPart.SetRelateEnumActive(relateEnum6, active, enumType);
	var acitve = RedPointPart.GetActive(enumType);
	RedPointPart.SetObjActive(enumType, active);
}
//设置关联的红点信息
RedPointPart.SetRelateEnumActive = function(relateEnum, active, typ) {
	if ((relateEnum == null)) {
		return;
	}
	var data = RedPointPart.TotalHongDianRelateData[relateEnum];
	if ((data == null)) {
		data = new RedPointData();
	}
	data.Set(relateEnum, active);
	data.Set(typ, active);
	RedPointPart.TotalHongDianRelateData[relateEnum] = data;
	var realacitve = RedPointPart.GetActive(relateEnum);
	RedPointPart.SetObjActive(relateEnum, realacitve);
}
window.RedPointPart = RedPointPart;