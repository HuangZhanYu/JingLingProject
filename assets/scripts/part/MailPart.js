//文件定义：客户端邮件系统Part
//设计要点：
//1、储存服务器下发的邮件列表
//负责人：周游
//评分：
//移交记录：
let mail_msg_pb = require('mail_msg').msg;
let MailPart = { };

MailPart.Result = {
	Success: 0,
	// 成功
	IDError: 1,
	// ID错误

};
MailPart.mTotalRewardData = { };
//邮件列表
MailPart.mTotalCount = 0;
//邮件的数量
//创建一个邮件数据结构
MailPart.CreateMail = function(data) {
	var mail = { };
	mail.ObjID = data.ObjID;
	mail.State = data.State;
	mail.DeleteTime = data.DeleteTime;
	mail.Content = data.Content;
	mail.Rewards = data.Rewards;
	mail.Sign = data.Sign;
	mail.Index = data.Index;
	return mail;
}
//请求所有邮件数据
MailPart.OnGetTotalData = function() {
	var request = new  mail_msg_pb.MailGetDataRequest();
	SocketClient.callServerFun("MailPart_OnGetTotal", request);
	//ViewManager.showLoading({});
}
//所有邮件数据返回
MailPart.OnGetTotalDataReturn = function(buffer) {
	//ViewManager.hideLoading();
	var msg = mail_msg_pb.MailGetDataResponse.decode(buffer);

	//清空邮件
	MailPart.mTotalRewardData = { };
	//获取服务器下发的邮件数据
	for (var i = 0; i != msg.Data.length; ++i) {
		var mail = MailPart.CreateMail(msg.Data[i]);
		var index = (MailPart.mTotalRewardData.length + 1);
		MailPart.mTotalRewardData[index] = { };
		MailPart.mTotalRewardData[index] = mail;
	}
	//主界面邮件红点检测
	MailPart.CheckRedPoint();
	//是否显示界面UI
	if ((msg.IsShowPanel == 1)) {

		//刷新界面
		if ((GOTool.IsDestroy(MailPopPanel.GetObj()) || (MailPopPanel.GetObj().activeSelf == false))) {
			MailPopPanel.ShowAfterLoad();
			return;
		}
	}
	//邮件系统更新UI
	//MailPopPanel.LoadUIData();
	
}
//接收一封邮件数据
MailPart.OnReciveOneMail = function(buffer) {
	//ViewManager.hideLoading();
	var msg = mail_msg_pb.MailGetOneDataResponse.decode(buffer);

	//更新的邮件
	var mail = MailPart.CreateMail(msg.Data);
	//是否已经存在此邮件
	var isHas = false;
	//将这封邮件更新到全局邮件列表中
	for (var i = 0; i != MailPart.mTotalRewardData.length; ++i) {
		if ((MailPart.mTotalRewardData[i].ObjID == mail.ObjID)) {
			MailPart.mTotalRewardData[i] = mail;
			isHas = true;
			break;
		}
	}
	//不存在此邮件，添加到邮件列表里(放到第一位，界面需求显示排前)
	if (!isHas) {
		table.insert(MailPart.mTotalRewardData, 1, mail);
	}
	//主界面邮件红点检测
	MailPart.CheckRedPoint();
	//邮件系统更新UI
	//MailPopPanel.LoadUIData();
}
// 领某个奖励请求
MailPart.OnGet = function(index) {
	var request = new  mail_msg_pb.MailGetRequest();
	//服务器邮件列表索引
	request.Index = index;
	SocketClient.callServerFun("MailPart_OnGet", request);
	//ViewManager.showLoading({});
}
// 领某个奖励返回
MailPart.OnGetReturn = function(buffer) {
	//ViewManager.hideLoading();
	var msg = mail_msg_pb.MailGetOneResponse.decode(buffer);

	//返回不成功弹出提示
	if ((msg.Success != MailPart.Result.Success)) {
		ToolTipPopPanel.Show(33);
		return;
	}
	//当前领取获得的奖励
	var totalResource = msg.Rewards;
	if ((totalResource.length < 1)) {
		return;
	}
	var rewardType = { };
	var rewardID = { };
	var rewardCount = { };
	for (var i = 0; i != msg.Rewards.length; ++i) {
		//设置道具的图标和数量
		rewardType[i] = msg.Rewards[i].Type;
		rewardID[i] = msg.Rewards[i].ID;
		rewardCount[i] = msg.Rewards[i].Count;
	}
	//界面显示获得的奖励
	HuoDeWuPinTiShiPanelData.Show(rewardType, rewardID, rewardCount);
	//清空邮件右边的数据
	//MailPopPanel.ResetRightUI();
	//MailPopPanel.SetGetBtnState(MailPopPanel.MailGetBtnState.Hide);
}
// 领所有奖励请求
MailPart.OnGetAll = function() {
	var request = new  mail_msg_pb.MailGetTotalRequest();
	SocketClient.callServerFun("MailPart_OnGetAll", request);
	//ViewManager.showLoading({});
}
// 领所有奖励返回
MailPart.OnGetAllReturn = function(buffer) {
	//ViewManager.hideLoading();
	var msg = mail_msg_pb.MailGetResponse.decode(buffer);

	//返回不成功弹出提示
	if ((msg.Success != MailPart.Result.Success)) {
		ToolTipPopPanel.Show(33);
		return;
	}
	//当前领取获得的奖励
	var totalResource = msg.Rewards;
	if ((totalResource.length < 1)) {
		return;
	}
	var rewardType = { };
	var rewardID = { };
	var rewardCount = { };
	for (var i = 0; i != msg.Rewards.length; ++i) {
		//设置道具的图标和数量
		rewardType[i] = msg.Rewards[i].Type;
		rewardID[i] = msg.Rewards[i].ID;
		rewardCount[i] = msg.Rewards[i].Count;
	}
	//界面显示获得的奖励
	HuoDeWuPinTiShiPanelData.Show(rewardType, rewardID, rewardCount);
	//清空邮件右边的数据
	//MailPopPanel.ResetRightUI();
	//MailPopPanel.SetGetBtnState(MailPopPanel.MailGetBtnState.Hide);
}
//阅读邮件请求
MailPart.ReadRequest = function(index) {
	var request = new  mail_msg_pb.MailReadRequest();
	//服务器邮件列表索引
	request.Index = index;
	SocketClient.callServerFun("MailPart_ReadMail", request);
	//ViewManager.showLoading({});
}
//阅读邮件返回
MailPart.ReadResponse = function(buffer) {
	//ViewManager.hideLoading();
	var msg = mail_msg_pb.MailReadResponse.decode(buffer);

	//MailPopPanel.ReadMailCallBack();
}
//删除邮件请求
MailPart.DeleteRequest = function(index) {
	var request = new  mail_msg_pb.MailDeleteRequest();
	//服务器邮件列表索引
	request.Index = index;
	SocketClient.callServerFun("MailPart_DeleteMail", request);
	//ViewManager.showLoading({});
}
//删除邮件请求
MailPart.DeleteResponse = function(buffer) {
	//ViewManager.hideLoading();
	var msg = mail_msg_pb.MailDeleteResponse.decode(buffer);

	//清空邮件右边的数据
	//MailPopPanel.ResetRightUI();
	//MailPopPanel.SetGetBtnState(MailPopPanel.MailGetBtnState.Hide);
}
//一键删除邮件请求
MailPart.OneKeyDeleteRequest = function() {
	var request = new  mail_msg_pb.MailOneKeyDeleteRequest();
	SocketClient.callServerFun("MailPart_OneKeyDeleteMail", request);
	//ViewManager.showLoading({});
}
//一键删除邮件返回
MailPart.OneKeyDeleteResponse = function(buffer) {
	//ViewManager.hideLoading();
	var msg = mail_msg_pb.MailOneKeyDeleteResponse.decode(buffer);

	//清空邮件右边的数据
	//MailPopPanel.ResetRightUI();
	//MailPopPanel.SetGetBtnState(MailPopPanel.MailGetBtnState.Hide);
}
//检测邮件红点
MailPart.CheckRedPoint = function() {
	//默认红点设为关闭
	RedPointPart.SetEnumActive(RedPointPart.Enum.Main_Mail, false, RedPointPart.Enum.Main_OffenUse);
	//遍历邮件列表，未读和未领取邮件显示红点
	// for (var i = 0; i != MailPart.mTotalRewardData.length; ++i) {
	// 	if (((MailPart.mTotalRewardData[i].State == MailPopPanel.MailState.NotRead) || (MailPart.mTotalRewardData[i].State == MailPopPanel.MailState.NotGet))) {
	// 		RedPointPart.SetEnumActive(RedPointPart.Enum.Main_Mail, true, RedPointPart.Enum.Main_OffenUse);
	// 		break;
	// 	}
	// }
}
window.MailPart = MailPart;