let pvp_msg_pb = require('pvp_msg').msg;
let PVPPart = { };

PVPPart.Room = null;
PVPPart.Records = null;
PVPPart.Point = 0;
PVPPart.MatchTimes = 0;
PVPPart.WinTimesToday = 0;
PVPPart.CWinTimesToday = 0;
PVPPart.GotDailyRecord = { };
PVPPart.GotSeasonRecord = { };
PVPPart.IsMatching = false;
//是否正在匹配中
const PVPDaily = {
	MatchTimes: 1,
	//匹配次数
	TotalWinTImes: 2,
	//今日胜利次数
	WinTimes: 3,
	//今日最大连胜

};
//获取每日任务对应值
PVPPart.GetDailyValue = function(typ) {
	if ((typ == PVPDaily.MatchTimes)) {
		return PVPPart.MatchTimes;
	} else if ((typ == PVPDaily.TotalWinTImes) ){
		return PVPPart.WinTimesToday;
	} else if ((typ == PVPDaily.TotalWinTImes) ){
		return PVPPart.CWinTimesToday;
	}
	return 0;
}
//获取pvp状态
PVPPart.GetStat = function() {
	var req = new  pvp_msg_pb.PVPPart_StatRequest();
	SocketClient.callServerFun("PVPPart_GetStat", req);
}
PVPPart.StatResp = function(buffer) {
	var ans = pvp_msg_pb.PVPPart_StatResponse.decode(buffer);

}
//获取pvp相关信息
PVPPart.GetInfo = function() {
	var req = new  pvp_msg_pb.PVPPart_InfoRequest();
	SocketClient.callServerFun("PVPPart_GetInfo", req);
}
//获取pvp相关信息回调
PVPPart.InfoResp = function(buffer) {
	var ans = pvp_msg_pb.PVPPart_InfoResponse.decode(buffer);

	PVPPart.Point = ans.Point;
	PVPPart.MatchTimes = ans.MatchTimes;
	PVPPart.WinTimesToday = ans.WinTimesToday;
	PVPPart.CWinTimesToday = ans.CWinTimesToday;
	PVPPart.GotDailyRecord = ans.GotDailyRecord;
	PVPPart.GotSeasonRecord = ans.GotSeasonRecord;
}
//匹配对手
PVPPart.Match = function() {
	var req = new  pvp_msg_pb.PVPPart_MatchRequest();
	SocketClient.callServerFun("PVPPart_Match", req);
	ViewManager.showLoading({});
}
//匹配结果
PVPPart.MatchRet = function(buffer) {
	ViewManager.hideLoading();
	var ans = pvp_msg_pb.PVPPart_MatchRet();

	if ((ans.Ret != 0)) {
		//
		ToolTipPopPanel.Show(ans.Ret);
		return;
	}
	//匹配中
	PVPPart.Room = null;
	PVPPart.IsMatching = true;
	PeakArenaPanel.ServerMatching();
}
//匹配成功
PVPPart.MatchAns = function(buffer) {
	var ans = pvp_msg_pb.PVPPart_MatchResponse.decode(buffer);

	var pvpplayers = { };
	for (var forinvar in ipairs(msg.Players)) {
		
		var player = forinvar.player;
		{
			table.insert(pvpplayers, new PVPPlayerplayer());
		}
	}
	PVPPart.Room = new PVPRoompvpplayers();
	PVPPart.Records = null;
	PVPPart.IsMatching = false;
	//拿到匹配房间数据，更新界面
	PeakArenaPanel.MatchSuccess();
}
//房间状态转换
PVPPart.RoomStateChange = function(buffer) {
	var msg = pvp_msg_pb.PVPPart_RoomStateChange();

	if (!PVPPart.Room) {
		return;
	}
	PVPPart.Room.ChangeState(msg.State);
}
//禁用属性
PVPPart.BanRace = function(races) {
	if (!PVPPart.Room) {
		return;
	}
	PVPPart.Room.Operator(PlayerBasePart.ObjID, OPERATORCODE.BANRACE, races);
}
//禁用属性的返回
PVPPart.BanRaceRet = function(buffer) {
	var ans = pvp_msg_pb.PVPPart_BanRaceResponse.decode(buffer);

	if (!PVPPart.Room) {
		return;
	}
	PVPPart.Room.OperatorRet(ans.ObjID, ans.Races);
}
//禁用卡牌
PVPPart.BanCards = function(cards) {
	if (!PVPPart.Room) {
		return;
	}
	PVPPart.Room.Operator(PlayerBasePart.ObjID, OPERATORCODE.BANCARD, cards);
}
//禁用卡牌回调
PVPPart.BanCardsRet = function(buffer) {
	var ans = pvp_msg_pb.PVPPart_BanCardsResponse.decode(buffer);

	if (!PVPPart.Room) {
		return;
	}
	PVPPart.Room.OperatorRet(ans.ObjID, ans.Cards);
}
//布阵
PVPPart.ClothArray = function(cards) {
	if (!PVPPart.Room) {
		return;
	}
	PVPPart.Room.Operator(PlayerBasePart.ObjID, OPERATORCODE.CLOTHARRAY, cards);
}
//布阵回调
PVPPart.ClothArrayRet = function(buffer) {
	var ans = pvp_msg_pb.PVPPart_BanCardsResponse.decode(buffer);

	if (!PVPPart.Room) {
		return;
	}
	PVPPart.Room.OperatorRet(ans.ObjID, ans.Cards);
}
//战斗过程
PVPPart.FightRecord = function(buffer) {
	var msg = pvp_msg_pb.PVPPart_FightRecord();

	if (!PVPPart.Records) {
		PVPPart.Records = new FightRecords();
		PVPPart.Records.Decode(msg.Content);
	} else {
		PVPPart.Records.AppendFollow(msg.Content);
	}
	if (msg.Done) {
		PVPPart.Room.Operator("", 0, PVPPart.Records);
	}
}
//战斗结果
PVPPart.FightRet = function(buffer) {
	var msg = pvp_msg_pb.PVPPart_FightRet();

	PVPPart.Room.OperatorRet("", msg);
}
//领取每日奖励
PVPPart.GetDailyAward = function(id) {
	var req = new  pvp_msg_pb.PVPPart_DailyAwardRequest();
	req.ID = id;
	SocketClient.callServerFun("PVPPart_DailyAward", req);
}
PVPPart.DailyAwardResp = function(buffer) {
	var ans = pvp_msg_pb.PVPPart_DailyAwardResponse.decode(buffer);

	if ((ans.Ret != 0)) {
		//
		ToolTipPopPanel.Show(ans.Ret);
		return;
	}
	table.insert(PVPPart.GotDailyRecord, ans.ID);
}
//领取赛季奖励
PVPPart.GetSeasonAward = function(id) {
	var req = new  pvp_msg_pb.PVPPart_SeasonAwardRequest();
	req.ID = id;
	SocketClient.callServerFun("PVPPart_SeasonAward", req);
}
PVPPart.SeasonAwardResp = function(buffer) {
	var ans = pvp_msg_pb.PVPPart_SeasonAwardResponse.decode(buffer);

	if ((ans.Ret != 0)) {
		//
		ToolTipPopPanel.Show(ans.Ret);
		return;
	}
	table.insert(PVPPart.GotSeasonRecord, ans.ID);
}
window.PVPPart = PVPPart;