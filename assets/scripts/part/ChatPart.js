//文件定义：客户端聊天系统Part
//设计要点：
//1、常规客户端和服务器回调
//负责人：周游
//评分：
//移交记录：
let chat_msg_pb = require('chat_msg').msg;

let ChatPart = { };

//聊天类型枚举
ChatPart.ChatType = {
	ChatPartype_Total: 0,
	//世界
	ConChatType_Guild: 1,
	//公会

};
//聊天返回类型枚举
ChatPart.ResultType = {
	Success: 0,
	//成功
	NotLive: 1,
	//玩家不在线
	Error: 2,
	//错误
	BanSpeech: 3,
	//禁言

};
ChatPart.FlauntList = { };
//炫耀消息列表
//获取聊天历史记录
ChatPart.GetMsgRecord = function(chattype) {
	var req = new  chat_msg_pb.ChatPart_MsgRecordRequest();
	req.ChatType = chattype;
	SocketClient.callServerFun("ChatPart_GetMsgRecord", req);
	ViewManager.showLoading({});
}
//获取聊天历史记录回调
ChatPart.MsgRecordResp = function(buffer) {
	ViewManager.hideLoading();
	var msg = chat_msg_pb.ChatPart_MsgRecordResponse.decode(buffer);

	//设置为只更新数据不更新UI标记
	ChatPopPanel.SetNotShowPanel();
	for (var forinvar in ipairs(msg.MsgList)) {
		
		var message = forinvar.message;
		{
			if ((message.ChatType >= 10)) {
				message.ChatType = (message.ChatType - 10);
			}
			ChatPopPanel.AddMessage(message);
		}
	}
}
//增加一条留言
ChatPart.AddMsg = function(buffer) {
	var msg = chat_msg_pb.ChatPart_Message();

	//设置为只更新数据不更新UI标记
	ChatPopPanel.SetNotShowPanel();
	var isShow = true;
	//是否显示弹幕
	if (((msg.Name != "") && (msg.Name != "SystemRobot"))) {
		if ((msg.ChatType >= 10)) {
			isShow = false;
			msg.ChatType = (msg.ChatType - 10);
		}
		//UI界面增加一条聊天信息
		ChatPopPanel.AddMessage(msg);
		//设置聊天红点  --如果当前界面不是聊天界面 则设置
		var curPanel = UIManager.GetCurPanelData();
		var isChatPanel = (((curPanel.Type == PanelType.PopPanel) && (curPanel.Enum == PopPanelEnum.ChatPop)));
		if (!isChatPanel) {
			RedPointPart.SetEnumActive(RedPointPart.Enum.Main_Chat, true);
		}
	}
	//聊天记录不弹弹幕
	if (!isShow) {
		return;
	}
	//显示弹幕
	if ((ChatPopPanel_IsOpenDanMu && GOTool.IsDestroy(DanMuPopPanel.GetObj()))) {
		DanMuPopPanel.Show(msg);
	} else if (ChatPopPanel_IsOpenDanMu && (!GOTool.IsDestroy(DanMuPopPanel.GetObj())) ){
		DanMuPopPanel.AddMsg(msg);
	}
}
//增加一条留言请求
ChatPart.SpeakRequest = function(ChatType, Name, Content) {
	var request = new  chat_msg_pb.ChatPart_SpeakRequest();
	//发送频道
	request.ChatType = ChatType;
	//发送者名字
	request.Name = Name;
	//屏蔽词工具过滤
	var newContent = BannedWordTool_CheckString(Content);
	//内容
	request.Content = newContent;
	SocketClient.callServerFun('ChatPart_Speak', request);
	ViewManager.showLoading({});
}
//增加一条留言回应
ChatPart.SpeakResponse = function(buffer) {
	ViewManager.hideLoading();
	var msg = chat_msg_pb.ChatPart_SpeakResponse.decode(buffer);

	//msg.Flag Flag为1这说明发送的人物不存在   玩家不在线或不存在
	if ((msg.Flag == ChatPart.ResultType.NotLive)) {
	} else if ((msg.Flag == ChatPart.ResultType.Error) ){
		ToolTipPopPanel.Show(202);
	} else if ((msg.Flag == ChatPart.ResultType.BanSpeech) ){
		ToolTipPopPanel.Show(201);
	}
}
//增加一个炫耀公告
ChatPart.AddFlauntNotice = function(buffer) {
	var msg = chat_msg_pb.ChatPart_FlauntNotice();

	ChatPart.FlauntList[(ChatPart.FlauntList.length + 1)] = msg;
	DanMuPopPanel.ShowFlauntNotice();
}
window.ChatPart = ChatPart;