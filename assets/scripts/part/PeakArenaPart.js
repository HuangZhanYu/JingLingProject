//巅峰竞技场部件
//负责人：张洋凡
let peak_arena_msg_pb = require('peak_arena_msg').msg;
let PeakArenaPart = { };

PeakArenaPart.AccumulatedPoint = 0;
PeakArenaPart.MyPosition = 0;
PeakArenaPart.DefendLines = { };
PeakArenaPart.FightLines = { };
PeakArenaPart.mRivals = { };
PeakArenaPart.mPosChanges = { };
//排名改变记录
PeakArenaPart.mDefends = { };
//对战记录
//请求巅峰数据
PeakArenaPart.PeakDataRequest = function() {
	var request = new  peak_arena_msg_pb.PeakArenaPart_PeakDataRequest();
	ViewManager.showLoading({});
	SocketClient.callServerFun('PeakArenaPart_SendPeakDataRequest', request);
}
//巅峰数据返回
PeakArenaPart.PeakDataResponse = function(buffer) {
	var msg = peak_arena_msg_pb.PeakArenaPart_PeakDataResponse.decode(buffer);

	ViewManager.hideLoading();
	PeakArenaPart.AccumulatedPoint = msg.AccumulatedPoint;
	PeakArenaPart.MyPosition = msg.MyPosition;
	PeakArenaPart.DefendLines = { };
	PeakArenaPart.DefendLines = msg.DefendLines;
	PeakArenaPart.FightLines = { };
	PeakArenaPart.FightLines = msg.FightLines;
	PeakArenaPart.mRivals = { };
	PeakArenaPart.mRivals = msg.Rivals;
	PeakArenaPanel.RequestSuccess();
}
//接受点数信息
PeakArenaPart.UpdatePointData = function(buffer) {
	var msg = peak_arena_msg_pb.PeakArenaPart_BattlePointData();

	PeakArenaPart.AccumulatedPoint = msg.AccumulatedPoint;
}
var beforeGet = 0;
//客户端请求领取奖励
PeakArenaPart.GetPointRequest = function() {
	if ((PeakArenaPart.AccumulatedPoint <= 0)) {
		return;
	}
	beforeGet = PeakArenaPart.AccumulatedPoint;
	var request = new  peak_arena_msg_pb.PeakArenaPart_GetPointRequest();
	ViewManager.showLoading({});
	SocketClient.callServerFun('PeakArenaPart_GetPointRequest', request);
}
//领取返回
PeakArenaPart.GetPointResponse = function(buffer) {
	var msg = peak_arena_msg_pb.PeakArenaPart_GetPointResponse.decode(buffer);

	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		console.error(("领取失败" + msg.Flag));
		return;
	}
	PeakArenaPart.AccumulatedPoint = msg.AccumulatedPoint;
	//刷新界面
	PeakArenaPanel.UpdateBattlePoint();
	//弹窗奖励
	HuoDeWuPinTiShiPanelData.Show([
			ResourceTypeEnum.ResType_HuoBi
		], [
			HuoBiTypeEnum.HuoBiType_JingJiDian
		], [
			beforeGet
		]);
}
//请求保存阵容
PeakArenaPart.SaveLinesRequest = function() {
	var request = new  peak_arena_msg_pb.PeakArenaPart_SaveLinesRequest();
	for (var i = 0; i != PeakArenaPart.DefendLines.length; ++i) {
		if ((i == 1)) {
			for (let j = 0; j != PeakArenaPart.DefendLines[i].CardList.length; ++j) {
				request.DefendLinesOne.push(PeakArenaPart.DefendLines[i].CardList[j]);
			}
		}
		if ((i == 2)) {
			for (let j = 0; j != PeakArenaPart.DefendLines[i].CardList.length; ++j) {
				request.DefendLinesTwo.push(PeakArenaPart.DefendLines[i].CardList[j]);
			}
		}
		if ((i == 3)) {
			for (let j = 0; j != PeakArenaPart.DefendLines[i].CardList.length; ++j) {
				request.DefendLinesThree.push(PeakArenaPart.DefendLines[i].CardList[j]);
			}
		}
	}
	ViewManager.showLoading({});
	SocketClient.callServerFun('PeakArenaPart_SaveDefendLines', request);
}
PeakArenaPart.SaveLinesResponse = function(buffer) {
	var msg = peak_arena_msg_pb.PeakArenaPart_SaveLinesResponse.decode(buffer);

	ViewManager.hideLoading();
	if ((msg.Flag != 0)) {
		console.error("保存失败，不合法的阵容");
		return;
	}
	//刷新界面
	PeakChangePopPanel.OnSaveSuccess();
}
//防守阵容上阵请求 
PeakArenaPart.DefendAddLineRequest = function(index, cardID) {
	//判断是否已经上满
	var count = 0;
	var addPos = -1;
	for (var i = 0; i != PeakArenaPart.DefendLines[index].CardList.length; ++i) {
		if ((PeakArenaPart.DefendLines[index].CardList[i] != 0)) {
			count = (count + 1);
		} else {
			addPos = i;
			break;
		}
	}
	if ((count == 12)) {
		return;
	}
	PeakArenaPart.DefendLines[index].CardList[addPos] = cardID;
	//刷新界面
	PeakChangePopPanel.SetLineData(index);
	PeakChangePopPanel.UpdatePlayerCards();
}
//防守阵容下阵请求
PeakArenaPart.DefendSubLineRequest = function(index, cardIndex) {
	PeakArenaPart.DefendLines[index].CardList[cardIndex] = 0;
	//刷新界面
	PeakChangePopPanel.SetLineData(index);
	PeakChangePopPanel.UpdatePlayerCards();
}
//一键下阵请求
PeakArenaPart.DefendOneKeyDownRequest = function(index) {
	for (var i = 0; i != 12; ++i) {
		PeakArenaPart.DefendLines[index].CardList[i] = 0;
	}
	//刷新界面
	PeakChangePopPanel.SetLineData(index);
	PeakChangePopPanel.UpdatePlayerCards();
}
//挑战阵容上阵请求
PeakArenaPart.FightAddLineRequest = function(index, cardID) {
	//判断是否已经上满
	var count = 0;
	var addPos = -1;
	for (var i = 0; i != PeakArenaPart.FightLines[index].CardList.length; ++i) {
		if ((PeakArenaPart.FightLines[index].CardList[i] != 0)) {
			count = (count + 1);
		} else {
			addPos = i;
			break;
		}
	}
	if ((count == 12)) {
		return;
	}
	PeakArenaPart.FightLines[index].CardList[addPos] = cardID;
	//刷新界面
	PeakEnemyPopPanel.SetLineData(index);
	PeakEnemyPopPanel.UpdatePlayerCards();
}
//挑战阵容下阵请求
PeakArenaPart.FightSubLineRequest = function(index, cardIndex) {
	PeakArenaPart.FightLines[index].CardList[cardIndex] = 0;
	//刷新界面
	PeakEnemyPopPanel.SetLineData(index);
	PeakEnemyPopPanel.UpdatePlayerCards();
}
//挑战阵容一键下阵
PeakArenaPart.FightOneKeyDownRequest = function(index) {
	for (var i = 0; i != 12; ++i) {
		PeakArenaPart.FightLines[index].CardList[i] = 0;
	}
	//刷新界面
	PeakEnemyPopPanel.SetLineData(index);
	PeakEnemyPopPanel.UpdatePlayerCards();
}
var rivalPosition = null;
var rivalObjID = null;
//获取对手阵容信息
PeakArenaPart.OnGetRivalRequest = function(position, objID) {
	var request = new  peak_arena_msg_pb.PeakArenaPart_GetRivalRequest();
	request.Position = position;
	request.ObjID = objID;
	rivalPosition = position;
	rivalObjID = objID;
	SocketClient.callServerFun('PeakArenaPart_OnGetRivalRequest', request);
	ViewManager.showLoading({});
}
//竞技场对手信息返回
PeakArenaPart.OnGetRivalResponse = function(buffer) {
	var msg = peak_arena_msg_pb.PeakArenaPart_GetRivalResponse.decode(buffer);

	ViewManager.hideLoading();
	var err = msg.Success;
	//错误，显示提示后返回
	if ((err != SingleArenaPart.Result.Success)) {
		return;
	}
	var datas = { };
	datas.RivalDefendLines = msg.DefendLines;
	datas.Position = rivalPosition;
	datas.ObjID = rivalObjID;
	PeakPKBeauPopPanel.GetRivalSuccess(datas);
}
//请求挑战巅峰竞技场
PeakArenaPart.OnFightRequest = function(position, objID) {
	//设置战斗标记
	FightBehaviorManager.SetFightingSystem(FightSystemType.PeakArena);
	var request = new  peak_arena_msg_pb.PeakArenaPart_FightRequest();
	request.Position = position;
	request.ObjID = objID;
	for (var i = 0; i != PeakArenaPart.FightLines.length; ++i) {
		if ((i == 1)) {
			for (let j = 0; j != PeakArenaPart.FightLines[i].CardList.length; ++j) {
				request.FightLinesOne.push(PeakArenaPart.FightLines[i].CardList[j]);
			}
		}
		if ((i == 2)) {
			for (let j = 0; j != PeakArenaPart.FightLines[i].CardList.length; ++j) {
				request.FightLinesTwo.push(PeakArenaPart.FightLines[i].CardList[j]);
			}
		}
		if ((i == 3)) {
			for (let j = 0; j != PeakArenaPart.FightLines[i].CardList.length; ++j) {
				request.FightLinesThree.push(PeakArenaPart.FightLines[i].CardList[j]);
			}
		}
	}
	ViewManager.showLoading({});
	SocketClient.callServerFun('PeakArenaPart_OnFightRequest', request);
}
var fightRound = 1;
var mRecords = null;
var winList = null;
var resultmsg = null;
//竞技场战斗结算信息以及战斗记录
PeakArenaPart.OnFightResultResponse = function(buffer) {
	ViewManager.hideLoading();
	var msg = peak_arena_msg_pb.PeakArenaPart_FightResultResponse.decode(buffer);

	var err = msg.Success;
	//错误，显示提示后返回
	if ((err != 0)) {
		console.error(("挑战失败" + err));
		FightBehaviorManager.SetFightingSystem(null);
		return;
	}
	PeakArenaPart.MyPosition = msg.NewPosition;
	//开启战斗
	var conf_ZhanDouPeiZhi_Data = LoadConfig.getConfigData(ConfigName.ZhanDouPeiZhi,11);
	if ((conf_ZhanDouPeiZhi_Data == null)) {
		return;
	}
	fightRound = 1;
	mRecords = msg.FightRecord;
	winList = msg.IsWin;
	var isrealWin = false;
	var wincount = 0;
	for (var i = 0; i != 3; ++i) {
		if (winList[i]) {
			wincount = (wincount + 1);
		}
	}
	if ((wincount >= 2)) {
		isrealWin = true;
	}
	resultmsg = { };
	resultmsg.IsWin = isrealWin;
	resultmsg.RivalName = msg.RivalName;
	resultmsg.OldPosition = msg.OldPosition;
	resultmsg.NewPosition = msg.NewPosition;
	resultmsg.RivalName = msg.RivalName;
	resultmsg.Rewards = msg.Rewards;
	var fightRecord = new FightRecords();
	fightRecord.Decode(msg.FightRecord[fightRound]);
	CommonFightPanel.Show(CommonFightPanel.FightType.TopArena);
	//开启第一场战斗 
	GFightManager.Playback(conf_ZhanDouPeiZhi_Data.Scene, FightSystemType.PeakArena, fightRecord, function() {
			PeakResultPopPanel.Show(winList[fightRound], fightRound, resultmsg);
		});
}
PeakArenaPart.StartNextFight = function() {
	FightBehaviorManager.SetFightingSystem(FightSystemType.PeakArena);
	fightRound = (fightRound + 1);
	var fightRecord = new FightRecords();
	fightRecord.Decode(mRecords[fightRound]);
	var conf_ZhanDouPeiZhi_Data = LoadConfig.getConfigData(ConfigName.ZhanDouPeiZhi,11);
	if ((conf_ZhanDouPeiZhi_Data == null)) {
		return;
	}
	//开启下场战斗 
	GFightManager.Playback(conf_ZhanDouPeiZhi_Data.Scene, FightSystemType.PeakArena, fightRecord, function() {
			PeakResultPopPanel.Show(winList[fightRound], fightRound, resultmsg);
		});
}
//请求竞技场对战记录信息
PeakArenaPart.OnRecordRequest = function() {
	var request = new  peak_arena_msg_pb.PeakArenaPart_RecordRequest();
	SocketClient.callServerFun('PeakArenaPart_RecordRequest', request);
	ViewManager.showLoading({});
}
//竞技场对战记录信息返回
PeakArenaPart.OnRecordResponse = function(buffer) {
	ViewManager.hideLoading();
	var msg = peak_arena_msg_pb.PeakArenaPart_RecordResponse.decode(buffer);

	PeakArenaPart.mPosChanges = msg.PosChanges;
	//竞技场排名变化记录数组
	PeakArenaPart.mDefends = msg.Defends;
	//竞技场防守战报记录
	//打开对战记录界面
	PKRecordPopPanel.Show(PKRecordPopPanel.EnterEnum.PeakArena);
}
//请求购买挑战次数
PeakArenaPart.BuyChallengeCountRequest = function(count) {
	var request = new  peak_arena_msg_pb.PeakArenaPart_BuyChallengeCountRequest();
	request.Count = count;
	SocketClient.callServerFun('PeakArenaPart_BuyChallengeCount', request);
	ViewManager.showLoading({});
}
//回应购买挑战次数
PeakArenaPart.BuyChallengeCountResponse = function(buffer) {
	ViewManager.hideLoading();
	var msg = peak_arena_msg_pb.PeakArenaPart_BuyChallengeCountResponse.decode(buffer);

	if ((msg.Flag != 0)) {
		return;
	}
	PeakArenaPanel.UpdateChallengeCount();
}
window.PeakArenaPart = PeakArenaPart;