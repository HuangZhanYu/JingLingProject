@echo off

setlocal EnableDelayedExpansion

::使用说明，将该批处理文件放到proto 文件的文件夹里，且该文件夹又protojs子文件夹。

set currPath=!cd!

echo %currPath%

for /r %currPath% %%i in (*.proto
) do (
	pbjs -t commonjs %%i>%currPath%\protojs\%%~ni.js
	::@echo %%i
	::@echo %%~ni
)

@echo 转换完成！
pause 转换完成！
