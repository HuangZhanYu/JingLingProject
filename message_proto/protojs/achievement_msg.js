module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "AchievementData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "Progress",
                    "id": 1
                }
            ]
        },
        {
            "name": "AchievementGetRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Type",
                    "id": 1
                }
            ]
        },
        {
            "name": "AchievementGetResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "Susseed",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Type",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Progress",
                    "id": 3
                }
            ]
        }
    ],
    "isNamespace": true
}).build();