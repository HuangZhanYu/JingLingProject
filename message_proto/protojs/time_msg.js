module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "ServerTimeMessage",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int64",
                    "name": "TimeNow",
                    "id": 1
                }
            ]
        }
    ],
    "isNamespace": true
}).build();