module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "BackPackPart_CardInfo",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "CardID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Count",
                    "id": 2
                }
            ]
        },
        {
            "name": "BackPackPart_Reward",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RewardType",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RewardID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "RewardCount",
                    "id": 3
                }
            ]
        },
        {
            "name": "BackPackPart_UseItemRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "DaoJuID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Count",
                    "id": 2
                }
            ]
        },
        {
            "name": "BackPackPart_UseItemResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "BackPackPart_Reward",
                    "name": "RewardList",
                    "id": 2
                }
            ]
        },
        {
            "name": "BackPackPart_BuyItemRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "DaoJuID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Count",
                    "id": 2
                }
            ]
        },
        {
            "name": "BackPackPart_BuyItemResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                }
            ]
        },
        {
            "name": "BackPackPart_CardListResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "BackPackPart_CardInfo",
                    "name": "CardList",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Group",
                    "id": 2
                }
            ]
        },
        {
            "name": "BackPackPart_UpdataCardInfo",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "CardID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Count",
                    "id": 2
                }
            ]
        },
        {
            "name": "BackPackPart_DecomposEquipRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Type",
                    "id": 2
                }
            ]
        },
        {
            "name": "BackPackPart_DecomposEquipResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "BackPackPart_Reward",
                    "name": "RewardList",
                    "id": 2
                }
            ]
        },
        {
            "name": "BackPackPart_EvolutionCardRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "CardID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "EvoType",
                    "id": 2
                }
            ]
        },
        {
            "name": "BackPackPart_EvolutionCardResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "EvoID",
                    "id": 2
                }
            ]
        },
        {
            "name": "BackPackPart_MultipleRecycleRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "string",
                    "name": "ObjIDArr",
                    "id": 1
                }
            ]
        },
        {
            "name": "BackPackPart_MultipleRecycleResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "BackPackPart_Reward",
                    "name": "RewardList",
                    "id": 2
                }
            ]
        },
        {
            "name": "BackPackPart_MultipleDestroyRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "string",
                    "name": "ObjIDArr",
                    "id": 1
                }
            ]
        },
        {
            "name": "BackPackPart_MultipleDestroyResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Flag",
                    "id": 1
                }
            ]
        },
        {
            "name": "BackPackPart_ComposeRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ItemType",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ItemID",
                    "id": 2
                }
            ]
        },
        {
            "name": "BackPackPart_ComposeResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Ret",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "BackPackPart_Reward",
                    "name": "Rewards",
                    "id": 2
                }
            ]
        },
        {
            "name": "BackPackPart_OptPackageRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ID",
                    "id": 1
                }
            ]
        },
        {
            "name": "BackPackPart_OptPackageResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "BackPackPart_Reward",
                    "name": "Rewards",
                    "id": 1
                }
            ]
        },
        {
            "name": "BackPackPart_OptPkgAwardRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Index",
                    "id": 2
                }
            ]
        },
        {
            "name": "BackPackPart_OptPkgAwardResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "BackPackPart_Reward",
                    "name": "Rewards",
                    "id": 1
                }
            ]
        }
    ],
    "isNamespace": true
}).build();