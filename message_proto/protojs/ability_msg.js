module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "AbilityPart_GrooveData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Position",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "SkillID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int64",
                    "name": "LastUsingTime",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "IsOpen",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "CDTime",
                    "id": 5
                }
            ]
        },
        {
            "name": "AbilityPart_UpdateDataResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "AbilityPart_GrooveData",
                    "name": "SkillGrooves",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "AllSkills",
                    "id": 2
                }
            ]
        },
        {
            "name": "AbilityPart_UpdateTotalSkillIDResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "AllSkills",
                    "id": 1
                }
            ]
        },
        {
            "name": "AbilityPart_SetGrooveRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "SkillID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Position",
                    "id": 2
                }
            ]
        },
        {
            "name": "AbilityPart_UseSkillRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Position",
                    "id": 1
                }
            ]
        },
        {
            "name": "AbilityPart_UpdateGrooveResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Success",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "AbilityPart_GrooveData",
                    "name": "Data",
                    "id": 2
                }
            ]
        },
        {
            "name": "AbilityPart_BuyGrooveRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Position",
                    "id": 1
                }
            ]
        },
        {
            "name": "AbilityPart_BuySkillRequest",
            "syntax": "proto2",
            "fields": []
        },
        {
            "name": "AbilityPart_BuySkillResponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Success",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "bool",
                    "name": "Exist",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "SkillID",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "ResourceType",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "ResourceID",
                    "id": 5
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "ResourceCount",
                    "id": 6
                }
            ]
        }
    ],
    "isNamespace": true
}).build();