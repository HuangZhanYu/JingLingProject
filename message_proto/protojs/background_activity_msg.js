module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "SimpleActivityData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ActivityName",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ActivityId",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ActivityIcon",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "IsHadRedPoint",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "IconPos",
                    "id": 5
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ActivityTplType",
                    "id": 6
                }
            ]
        },
        {
            "name": "SyncAllActivitys",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "SimpleActivityData",
                    "name": "SimpleActivityList",
                    "id": 1
                }
            ]
        },
        {
            "name": "DetailActivityRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ActivityId",
                    "id": 1
                }
            ]
        },
        {
            "name": "ActivityReward",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ResourceType",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ResourceID",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "ResourceCount",
                    "id": 3
                }
            ]
        },
        {
            "name": "ConditionAndReward",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "No",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "ValueList",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "HadGetTimes",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "ExtraInfo",
                    "id": 4
                },
                {
                    "rule": "repeated",
                    "type": "ActivityReward",
                    "name": "RewardList",
                    "id": 5
                }
            ]
        },
        {
            "name": "BackgroundActivityTab",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "TabName",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "bool",
                    "name": "IsHadRedPoint",
                    "id": 2
                },
                {
                    "rule": "repeated",
                    "type": "ConditionAndReward",
                    "name": "ConditionAndRewardList",
                    "id": 3
                }
            ]
        },
        {
            "name": "DetailActivityReponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "MsgCode",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "ActivityId",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "ActivityTplType",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "ActivityName",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "ActivityIcon",
                    "id": 5
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "ActivityDescribe",
                    "id": 6
                },
                {
                    "rule": "repeated",
                    "type": "int32",
                    "name": "ReachValueList",
                    "id": 7
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "EndTime",
                    "id": 8
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "ShowEndTime",
                    "id": 9
                },
                {
                    "rule": "repeated",
                    "type": "BackgroundActivityTab",
                    "name": "BackgroundActivityTabList",
                    "id": 10
                }
            ]
        },
        {
            "name": "GetRewardRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ActivityId",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "TabIndex",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RewardNo",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "Count",
                    "id": 4
                }
            ]
        },
        {
            "name": "GetRewardReponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ActivityId",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "TabIndex",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "RewardNo",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "MsgCode",
                    "id": 4
                },
                {
                    "rule": "repeated",
                    "type": "ActivityReward",
                    "name": "RewardList",
                    "id": 5
                }
            ]
        },
        {
            "name": "WebCommentData",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 1
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ActivityName",
                    "id": 2
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ActivityIcon",
                    "id": 3
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "Path",
                    "id": 4
                },
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ActivityDescribe",
                    "id": 5
                },
                {
                    "rule": "repeated",
                    "type": "ActivityReward",
                    "name": "RewardList",
                    "id": 6
                }
            ]
        },
        {
            "name": "SyncWebCommentDataList",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "repeated",
                    "type": "WebCommentData",
                    "name": "WebCommentDataList",
                    "id": 1
                }
            ]
        },
        {
            "name": "WebCommentRewardRequest",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "string",
                    "name": "ObjID",
                    "id": 1
                }
            ]
        },
        {
            "name": "WebCommentRewardReponse",
            "syntax": "proto2",
            "fields": [
                {
                    "rule": "required",
                    "type": "int32",
                    "name": "MsgCode",
                    "id": 1
                },
                {
                    "rule": "repeated",
                    "type": "ActivityReward",
                    "name": "RewardList",
                    "id": 2
                }
            ]
        }
    ],
    "isNamespace": true
}).build();