module.exports = require("protobufjs").newBuilder({})['import']({
    "package": "msg",
    "syntax": "proto2",
    "messages": [
        {
            "name": "LoginRequest",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "Account",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "PassWord",
                    "id": 2
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "ServerId",
                    "id": 3
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "DeviceId",
                    "id": 4
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "ChannelId",
                    "id": 5
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "Reconnect",
                    "id": 6
                },
                {
                    "rule": "optional",
                    "type": "string",
                    "name": "PackageName",
                    "id": 7
                }
            ]
        },
        {
            "name": "LoginResponse",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "Susseed",
                    "id": 1
                },
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "NewPlayer",
                    "id": 2
                }
            ]
        },
        {
            "name": "LoginErrorResponse",
            "syntax": "proto3",
            "fields": [
                {
                    "rule": "optional",
                    "type": "int32",
                    "name": "Susseed",
                    "id": 1
                }
            ]
        }
    ],
    "isNamespace": true
}).build();